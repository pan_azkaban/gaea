
package com.anjiplus.template.gaea.business.modules.deviceinfo.dao.entity;

import lombok.Data;
import io.swagger.annotations.ApiModelProperty;

import com.anji.plus.gaea.curd.entity.GaeaBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.List;
import com.baomidou.mybatisplus.annotation.TableField;

import com.anjiplus.template.gaea.business.modules.devicelogdetail.dao.entity.DeviceLogDetail;
import com.anjiplus.template.gaea.business.modules.devicemodel.dao.entity.DeviceModel;

/**
* @description 设备信息 entity
* @author WongBin
* @date 2021-04-07 17:44:28.598
**/
@TableName(value="t_device_info")
@Data
public class DeviceInfo extends GaeaBaseEntity {
    @ApiModelProperty(value = "设备型号ID")
    private Long deviceModelId;

    @ApiModelProperty(value = "设备名称")
    private String deviceName;

    @ApiModelProperty(value = "机构代码")
    private String orgCode;

    @ApiModelProperty(value = "所属机构")
    private String orgName;

    @ApiModelProperty(value = "设备ip")
    private String deviceIp;

    @ApiModelProperty(value = "设备mac地址")
    private String macAddress;

    @ApiModelProperty(value = "日志级别")
    private String logLevel;

    @ApiModelProperty(value = "启用标志")
    private Integer enableFlag;

    @ApiModelProperty(value = "删除标志")
    private Integer deleteFlag;

    @TableField(exist = false)
    private List<DeviceLogDetail> deviceLogDetails;

    @TableField(exist = false)
    private List<DeviceModel> deviceModels;


}