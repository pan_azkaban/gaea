package com.anji.plus.gaea.job.core.util;

import java.util.*;

/**
 * @desc 对象工具类
 * @author 木子李*de
 * @date 2023-05-17 15:25:35.961
 **/
public class ObjectUtil {

    public static List array2List(String[] arrs){
        List<String> listB = new ArrayList<>();
        if(arrs != null && arrs.length > 0){
            Collections.addAll(listB, arrs);
        }
        return listB;
    }

    public static List mergerList(List listA, List listB){
        Set set = new HashSet();
        if(listA != null && listA.size() >0){
            set.addAll(listA);
        }
        if(listB != null && listB.size() >0){
            set.addAll(listB);
        }
        List list = new ArrayList(set);
        return list;
    }

    /**
     * 判断字符串是null、空字符、空格字符、'null'，返回true
     * @param str
     * @return
     */
    public static boolean isBlank(String str){
        if( str == null ) {
            return true;
        }
        if( str.trim().length() == 0 ) {
            return true;
        }
        if("null".equals(str.trim().toLowerCase())){
            return true;
        }
        return false;
    }

    public static boolean isNotBlank(String str){
        return isBlank(str) == false;
    }
}
