package com.anji.plus.gaea.push.type.param;

import com.anji.plus.gaea.push.type.dingding.enums.MesOperation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.StringUtils;

import java.io.InputStream;
import java.io.Serializable;
import java.util.Map;

/**
 * @author anji gaea teams
 * @Date: 2020/10/27
 * @Description:
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PushParamVO implements Serializable {
    private static final long serialVersionUID = 1141241214L;

    /** PushEnum
     * mail:邮件; dingtalk:钉钉 sms:短信；miniapp: 微信小程序
     */
    String pushType;
    /**
     * 模板code
     */
    String templateCode;

    /**
     * 扩展信息，短信第三方配置，小程序的配置
     */
    String templateInfo;

    /**
     * 模板
     */
    String template;

    /**
     * 模板参数
     */
    String templateParam;

    /**
     * 标题
     */
    String subject;
    /**
     * 发送者：email： 邮箱 以,分开   其他可以不填
     */
    String from;
    /**
     * 接收者 邮箱或手机号或openid 以,分开
     */
    String to;
    /**
     * 抄送
     */
    String copy;
    /**
     * 密送
     */
    String Bcc;
    /**
     * 发送内容
     */
    Map<String, Object> paramMap;
    /**
     * 邮件发送文件
     */
    Map<String, InputStream> fileMap;

    /**
     * 发送内容json转字符串
     */
    String param;
    /**
     * 钉钉 推送信息格式
     */
    String msgType;

    /**
     * 用于解析用户相关的邮箱、openid等
     */
    String username;


    public String getMsgType() {
        if (StringUtils.isEmpty(msgType)) {
            return MesOperation.text;
        }
        return msgType;
    }
}


