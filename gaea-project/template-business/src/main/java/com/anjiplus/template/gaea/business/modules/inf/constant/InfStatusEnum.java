package com.anjiplus.template.gaea.business.modules.inf.constant;

import lombok.Getter;

/**
 * @author ultrajiaming
 * @since 2021/3/19 16:53
 */
@Getter
public enum InfStatusEnum {

    /**
     * 编辑中
     */
    EDIT(0, "编辑中"),
    /**
     * 测试通过
     */
    TEST_PASS(1, "测试通过"),

    /**
     * 审核通过
     **/
    EXAMINE(5, "审核通过"),
    /**
     * 上线
     */
    PUBLISHED(2, "已上线"),
    /**
     * 下线
     */
    OFFLINE(3, "已下线");

    private int value;
    private String title;

    InfStatusEnum(int value, String title){
        this.value = value;
        this.title = title;
    }

    public static String match(int value) {
        for (InfStatusEnum infStatusEnum : InfStatusEnum.values()) {
            if (infStatusEnum.getValue() == value) {
                return infStatusEnum.getTitle();
            }
        }
        return null;
    }

}
