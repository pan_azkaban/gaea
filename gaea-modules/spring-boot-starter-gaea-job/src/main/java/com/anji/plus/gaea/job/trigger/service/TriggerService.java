package com.anji.plus.gaea.job.trigger.service;

import com.anji.plus.gaea.job.core.dto.ReturnT;
import com.anji.plus.gaea.job.core.param.HandleCallbackParam;
import com.anji.plus.gaea.job.core.param.RegistryParam;
import com.anji.plus.gaea.job.trigger.thread.JobCompleteHelper;
import com.anji.plus.gaea.job.trigger.thread.JobRegistryHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @desc 执行器处理服务
 * @author 木子李*de
 * @date 2023-05-17 15:25:35.961
 **/
@Service
public class TriggerService {
    private static Logger logger = LoggerFactory.getLogger(TriggerService.class);

    /**
     * 提供给执行器注册心跳
     * @param registryParam
     * @return
     */
    public ReturnT<String> registryUp(RegistryParam registryParam){
        if(registryParam == null || registryParam.isValid() == false){
            return ReturnT.fail("Argument registryParam valid fail");
        }
        return JobRegistryHelper.getInstance().registryUp(registryParam);
    }

    /**
     * 提供给执行器取消注册心跳
     * @param registryParam
     * @return
     */
    public ReturnT<String> registryDown(RegistryParam registryParam){
        if(registryParam == null || registryParam.isValid() == false){
            return ReturnT.fail("Argument registryParam valid fail");
        }
        return JobRegistryHelper.getInstance().registryDown(registryParam);
    }

    /**
     * 执行器运行完回调
     * @param callbackParamList
     * @return
     */
    public ReturnT<String> callback(List<HandleCallbackParam> callbackParamList){
        return JobCompleteHelper.getInstance().callback(callbackParamList);
    }
}
