package com.anjiplus.template.gaea.gateway.config;

import com.anjiplus.template.gaea.gateway.filter.CorsResponseHeaderFilter;
import com.anjiplus.template.gaea.gateway.filter.GlobalSecurityAuthenticationFilter;
import com.anjiplus.template.gaea.gateway.filter.GlobalSecurityAccessFilter;
import com.anjiplus.template.gaea.gateway.filter.SkipAuthenticationGatewayFilterFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.cors.reactive.CorsUtils;
import org.springframework.web.server.WebFilter;
import reactor.core.publisher.Mono;

/**
 * 网关配置
 * @author lr
 * @since 2021-02-01
 */
@Configuration
public class GaeaGatewayConfig {

//    private static final String ALLOWED_HEADERS = "x-requested-with, Authorization, Content-Type, credential, X-XSRF-TOKEN";
    private static final String ALLOWED_HEADERS = "*";
    private static final String ALLOWED_METHODS = "*";
    private static final String ALLOWED_ORIGIN = "*";
    private static final String ALLOWED_EXPOSE = "*";
    private static final String MAX_AGE = "18000L";


    /**
     * 判断是否登录
     * @return
     */
    @Bean
    public GlobalSecurityAuthenticationFilter globalSecurityAuthenticationFilter() {
        return new GlobalSecurityAuthenticationFilter();
    }

    /**
     * 判断是否有权限
     * @return
     */
    @Bean
    public GlobalSecurityAccessFilter globalSecurityAccessFilter() {
        return new GlobalSecurityAccessFilter();
    }

    /**
     * 设置跳过校验标识
     * @return
     */
    @Bean
    public SkipAuthenticationGatewayFilterFactory skipAuthenticationGatewayFilterFactory() {
        return new SkipAuthenticationGatewayFilterFactory();
    }

    @Bean
    public CorsResponseHeaderFilter getCorsResponseHeaderFilter(){
        return new CorsResponseHeaderFilter();
    }
    /**
     * 跨域配置
     * @return
     */
    @Bean
    public WebFilter webFilter() {
        return (exchange, chain) -> {
            ServerHttpRequest request = exchange.getRequest();
            if (CorsUtils.isCorsRequest(request)) {
                ServerHttpResponse response = exchange.getResponse();
                HttpHeaders headers = response.getHeaders();
                headers.add("Access-Control-Allow-Origin", request.getHeaders().getOrigin());
                headers.add("Access-Control-Allow-Methods", ALLOWED_METHODS);
                headers.add("Access-Control-Max-Age", MAX_AGE);
                headers.add("Access-Control-Allow-Headers", ALLOWED_HEADERS);
                headers.add("Access-Control-Expose-Headers", ALLOWED_EXPOSE);
                headers.add("Access-Control-Allow-Credentials", "true");

                if (request.getMethod() == HttpMethod.OPTIONS) {
                    response.setStatusCode(HttpStatus.OK);
                    return Mono.empty();
                }
            }
            return chain.filter(exchange);
        };
    }
}
