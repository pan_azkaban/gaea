import request from '@/utils/request'

export function getAllTable() {
  return request({
    url: '/business/api/generator/tables/all',
    method: 'get',
  })
}

export function generator(project, tableName, type,dbSchema) {
  return request({
    url: '/business/api/generator/' + project + '/' + tableName + '/' + type + '/'+dbSchema,
    method: 'post',
    responseType: type === 2 ? 'blob' : '',
  })
}

export function save(data,dbSchema) {
  return request({
    url: '/business/api/generator/saveColumnCfg/'+dbSchema,
    data,
    method: 'post',
  })
}

export function sync(tables,dbSchema) {
  return request({
    url: '/business/api/generator/sync/'+dbSchema,
    method: 'post',
    data: tables,
  })
}
