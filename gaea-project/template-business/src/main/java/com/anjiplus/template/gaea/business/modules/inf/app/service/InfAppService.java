package com.anjiplus.template.gaea.business.modules.inf.app.service;

import com.anji.plus.gaea.bean.KeyValue;
import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anjiplus.template.gaea.business.modules.inf.app.controller.dto.InfAppDTO;
import com.anjiplus.template.gaea.business.modules.inf.app.controller.params.InfAppParams;
import com.anjiplus.template.gaea.business.modules.inf.app.dao.entity.InfAppEntity;

import java.util.List;

/**
 * (InfApp)表服务接口
 *
 * @author ultrajiaming
 * @since 2021-03-18 23:27:42
 */
public interface InfAppService extends GaeaBaseService<InfAppParams, InfAppEntity> {

    /**
     * 更新秘钥
     **/
    void updateSecret(Long id);

    /**
     * 下拉 appId——appName
     **/
    List<KeyValue> dropDown();

    /**
     * 通过AppId查询
     **/
    InfAppDTO selectByAppId(String appId);

}
