
package com.anjiplus.template.gaea.business.modules.alipayconfig.controller.dto;

import java.io.Serializable;

import com.anji.plus.gaea.curd.dto.GaeaBaseDTO;
import com.anji.plus.gaea.annotation.Formatter;
import lombok.Data;
import java.sql.Timestamp;


/**
*
* @description 支付配置 dto
* @author dev-user
* @date 2021-04-07 14:58:28.131
**/
@Data
public class AlipayConfigDto extends GaeaBaseDTO implements Serializable {
    /** 应用ID */
    private String appId;

    /** 编码 */
    @Formatter(dictCode = "charset",targetField = "charsetCn")
    private String charset;
    private String charsetCn;

    /** 类型  */
    @Formatter(dictCode = "responseType",targetField = "formatCn")
    private String format;
    private String formatCn;

    /** 网关地址 */
    private String gatewayUrl;

    /** 异步回调 */
    private String notifyUrl;

    /** 私钥 */
    private String privateKey;

    /** 公钥 */
    private String publicKey;

    /** 回调地址 */
    private String returnUrl;

    /** 签名方式 */
    private String signType;

    /** 商户号 */
    private String sysServiceProviderId;

}