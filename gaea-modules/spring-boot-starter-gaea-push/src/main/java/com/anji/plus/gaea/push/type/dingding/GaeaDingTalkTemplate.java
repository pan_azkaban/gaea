package com.anji.plus.gaea.push.type.dingding;

import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.push.event.SendMessageEvent;
import com.anji.plus.gaea.push.support.AbstractPushSender;
import com.anji.plus.gaea.push.type.base.BasePushDetails;
import com.anji.plus.gaea.push.type.dingding.template.DingDingTemplate;
import com.anji.plus.gaea.push.type.param.PushParamVO;
import com.anji.plus.gaea.utils.ApplicationContextUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @ClassName DingDingPushSender
 * @Description: 发送钉钉
 * @Author dingkaiqiang
 * @Date 2021-04-07
 * @Version V1.0
 **/
public class GaeaDingTalkTemplate extends AbstractPushSender<DingDingPushDetails> {

    @Autowired
    private  DingDingTemplate dingDingTemplate;

    /**
     * 发送
     * @param message
     * @return
     * @throws Exception
     */
    @Override
    public ResponseBean doSend(DingDingPushDetails message) throws Exception {
        boolean sendSuccess = true;
        try {
            sendSuccess = dingDingTemplate.doSendDingMsg(message);
        } catch (Exception e){
            e.printStackTrace();
            //异常，设置发送失败
            sendSuccess = false;
        } finally {
            ApplicationContextUtils.publishEvent(new SendMessageEvent(message, sendSuccess));

        }
        return ResponseBean.builder().build();
    }

    /**
     * 这个转换 实际上也是需要根据不同的类型来处理
     * @param pushParamVO
     * @return
     */
    @Override
    public BasePushDetails convert(PushParamVO pushParamVO) {

        DingDingPushDetails pushDetails = dingDingTemplate.convertParam(pushParamVO);
        return pushDetails;
    }

}
