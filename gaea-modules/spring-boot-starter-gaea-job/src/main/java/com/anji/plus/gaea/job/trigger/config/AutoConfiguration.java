package com.anji.plus.gaea.job.trigger.config;

import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * @desc 定时任务--调度器装配
 * @author 木子李*De
 * @date 2023-05-12 15:25:35.961
 **/
@Configuration
@EnableConfigurationProperties(TriggerProperties.class)
@ComponentScan(value = {"com.anji.plus.gaea.job.trigger"})
@ConditionalOnProperty(name= "spring.gaea.subscribes.trigger.enabled", havingValue="true")
public class AutoConfiguration {
    private static Logger logger = LoggerFactory.getLogger(AutoConfiguration.class);

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private TriggerProperties triggerProperties;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private RedissonClient redissonClient;

    @Bean
    @ConditionalOnMissingBean
    public TriggerConfiguration triggerConfiguration() {
        // 调度器初始化
        TriggerConfiguration triggerConfiguration = new TriggerConfiguration();
        triggerConfiguration.setApplicationContext(applicationContext);
        triggerConfiguration.setJdbcTemplate(jdbcTemplate);
        triggerConfiguration.setRedissonClient(redissonClient);
        triggerConfiguration.setTriggerProperties(triggerProperties);
        return triggerConfiguration;
    }

}
