/*
 * @Author: zyk
 * @Date: 2020-07-13 15:13:34
 * @Last Modified by: zyk
 * @Last Modified time: 2021-03-15 13:33:24
 */

import request from '@/utils/request'
// 系统设置模块内所有页面相关接口

// 列表
export function getAnnotList(params) {
  return request({
    url: '/auth/annot/pageList',
    method: 'GET',
    params,
  })
}

// 新增
export function annotAdd(data) {
  return request({
    url: '/auth/annot',
    method: 'POST',
    data,
  })
}
// 新增
export function annotEdit(data) {
  return request({
    url: '/auth/annot',
    method: 'PUT',
    data,
  })
}

// 保存并发布公告
export function saveAndPublishAnnot(data) {
  return request({
    url: '/auth/annot/saveAndPublishAnnot',
    method: 'POST',
    data,
  })
}

// 批量发布
export function publishAnnotBatch(data) {
  return request({
    url: `/auth/annot/publishAnnotBatch`,
    method: 'POST',
    data,
  })
}

// 撤销
export function cancelAnnot(id) {
  return request({
    url: `auth/annot/cancelAnnot/` + id,
    method: 'GET',
  })
}
