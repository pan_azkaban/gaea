
package com.anjiplus.template.gaea.business.modules.devicemodel.service.impl;

import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.anjiplus.template.gaea.business.modules.devicemodel.dao.entity.DeviceModel;
import com.anjiplus.template.gaea.business.modules.devicemodel.service.DeviceModelService;
import com.anjiplus.template.gaea.business.modules.devicemodel.dao.DeviceModelMapper;
/**
* @desc DeviceModel 设备类型服务实现
* @author dev-user
* @date 2021-04-07 17:27:35.130
**/
@Service
public class DeviceModelServiceImpl implements DeviceModelService {

    @Autowired
    private DeviceModelMapper deviceModelMapper;

    @Override
    public GaeaBaseMapper<DeviceModel> getMapper() {
      return deviceModelMapper;
    }

    @Override
    public DeviceModel getDetail(Long id) {
        DeviceModel deviceModel = this.selectOne(id);
        return deviceModel;
    }
}