
package ${package}.service;

import ${package}.dao.entity.${className};
import ${package}.controller.param.${className}Param;
import com.anji.plus.gaea.curd.service.GaeaBaseService;

/**
* @desc ${className} ${apiAlias}服务接口
* @author ${author}
* @date ${date}
**/
public interface ${className}Service extends GaeaBaseService<${className}Param, ${className}> {

    /***
     * 查询详情
     *
     * @param id
     * @return
     */
    ${className} getDetail(Long id);
}