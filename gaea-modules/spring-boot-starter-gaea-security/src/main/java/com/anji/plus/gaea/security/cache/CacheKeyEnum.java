package com.anji.plus.gaea.security.cache;

/**
 * 缓存对象key.
 *
 * @author lr
 * @since 2019-07-30 09:28
 */
public enum CacheKeyEnum {

    /**
     * 保存5分钟签名，防止恶意重复请求
     */
    REPEAT_REQUEST_SIGN("system:repeat:sign:","请求签名"),


    /**
     * 保存用户可权限路径
     */
    USER_AUTH("system:user:auth:","用户权限"),

    /**
     * 用户权限url
     */
    USER_URL("system:user:url:","用户权限url"),

    /**
     * 保存用户角色
     */
    USER_ROLE("system:user:role:","用户角色"),

    /**
     * 登录token
     */
    TOKEN_JWT_USER("system:user:token:", "存放用户名及对应的token"),

    /**
     * 密码错误次数
     */
    USER_PASSWORD_ERROR_NUMBER("system:user:password:errorNumber:", "密码错误次数"),

    /**
     * 保存用户对应的权限
     */
    USER_AUTHORITIES("system:user:authorities:", "保存用户对应的权限");


    private String key;

    private String value;

    CacheKeyEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }}
