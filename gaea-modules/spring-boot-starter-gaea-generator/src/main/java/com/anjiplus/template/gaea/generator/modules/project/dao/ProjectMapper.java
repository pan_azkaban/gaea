package com.anjiplus.template.gaea.generator.modules.project.dao;

import com.anjiplus.template.gaea.generator.modules.project.dao.entity.Project;
import org.apache.ibatis.annotations.Mapper;

import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;

/**
 * (Project)Mapper
 *
 * @author makejava
 * @since 2021-03-03 09:54:37
 */
@Mapper
public interface ProjectMapper extends GaeaBaseMapper<Project> {


}
