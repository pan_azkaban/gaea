insert into gaea_menu(menu_code, menu_name, sys_code, parent_code, path,
                      menu_icon,sort,enabled,delete_flag, redirect_url, component,
                      create_by, update_by, create_time, update_time, version)values
('DeviceInfo','设备信息','PC','SystemSet','deviceInfo','',3060,1,	0,'','system-set/deviceInfo/index','admin','管理员',now(),now(),1);

insert into gaea_authority(application_name, scan_annotation, auth_code, auth_name, parent_code, path,
                           create_time, update_time, version) values
('gaea-business', 0, 'DeviceInfoController#pageList', 'uiI18n查询', 'DeviceInfoController', 'GET#/deviceInfo/pageList', now(), now(), 1),
('gaea-business', 0, 'DeviceInfoController#detail', 'uiI18n详情', 'DeviceInfoController', 'GET#/deviceInfo/**', now(), now(), 1),
('gaea-business', 0, 'DeviceInfoController#update', 'uiI18n修改', 'DeviceInfoController', 'PUT#/deviceInfo/', now(), now(), 1),
('gaea-business', 0, 'DeviceInfoController#insert', 'uiI18n新增', 'DeviceInfoController', 'POST#/deviceInfo/', now(), now(), 1),
('gaea-business', 0, 'DeviceInfoController#deleteById', 'uiI18n删除', 'DeviceInfoController', 'DELETE#/deviceInfo/**', now(), now(), 1)
;

insert into gaea_menu_authority(org_code, menu_code, auth_code, create_by,
                                update_by, create_time, update_time, version)VALUES
('3000004','DeviceInfo','DeviceInfoController#pageList','','',now(),now(),1),
('3000004','DeviceInfo','DeviceInfoController#detail','','',now(),now(),1),
('3000004','DeviceInfo','DeviceInfoController#update','','',now(),now(),1),
('3000004','DeviceInfo','DeviceInfoController#insert','','',now(),now(),1),
('3000004','DeviceInfo','DeviceInfoController#deleteById','','',now(),now(),1);