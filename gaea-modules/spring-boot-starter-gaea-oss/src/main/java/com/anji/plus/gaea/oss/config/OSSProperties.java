package com.anji.plus.gaea.oss.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.Serializable;

/**
 * oss组件配置项
 *
 * @author 木子李·De
 * @since 2022/3/22 14:16
 */
@ConfigurationProperties(prefix = "spring.gaea.subscribes.oss")
public class OSSProperties implements Serializable {

    /** 默认激活oss组件 */
    private boolean enabled = true;

    /** 允许上传的文件后缀，为空不限制，例如：.png|.jpg|.gif|.icon|.pdf|.xlsx|.xls|.csv|.mp4|.avi */
    private String fileTypeWhiteList="";

    /** 防盗链，允许访问的域名，为空不限制，例如：sili.anji-plus.com,www.anji-plus.com */
    private String refererWhiteList="";

    /** minio组件配置项 */
    private OSSMinioProperties minio;

    /** AmazonS3组件配置项 */
    private OSSAmazonS3Properties amazonS3;

    /** 默认使用服务器本地文件夹，多节点时配合nfs */
    private OSSNFSProperties nfs;

    private OSSBaseProperties huaweiObs;

    public OSSBaseProperties getHuaweiObs() {
        return huaweiObs;
    }

    public void setHuaweiObs(OSSBaseProperties huaweiObs) {
        this.huaweiObs = huaweiObs;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getFileTypeWhiteList() {
        return fileTypeWhiteList;
    }

    public void setFileTypeWhiteList(String fileTypeWhiteList) {
        this.fileTypeWhiteList = fileTypeWhiteList;
    }

    public String getRefererWhiteList() {
        return refererWhiteList;
    }

    public void setRefererWhiteList(String refererWhiteList) {
        this.refererWhiteList = refererWhiteList;
    }

    public OSSMinioProperties getMinio() {
        return minio;
    }

    public void setMinio(OSSMinioProperties minio) {
        this.minio = minio;
    }

    public OSSAmazonS3Properties getAmazonS3() {
        return amazonS3;
    }

    public void setAmazonS3(OSSAmazonS3Properties amazonS3) {
        this.amazonS3 = amazonS3;
    }

    public OSSNFSProperties getNfs() {
        return nfs;
    }

    public void setNfs(OSSNFSProperties nfs) {
        this.nfs = nfs;
    }
}
