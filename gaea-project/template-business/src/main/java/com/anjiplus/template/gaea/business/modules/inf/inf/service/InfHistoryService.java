package com.anjiplus.template.gaea.business.modules.inf.inf.service;


import com.anjiplus.template.gaea.business.modules.inf.inf.dao.entity.InfHistoryEntity;
import com.baomidou.mybatisplus.extension.service.IService;

public interface InfHistoryService extends IService<InfHistoryEntity> {
}
