package com.anji.plus.gaea.security;

import com.anji.plus.gaea.constant.GaeaConstant;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author lr
 * @since 2021-01-20
 */
@ConfigurationProperties(prefix = GaeaConstant.COMPONENT_PREFIX + GaeaSecurityProperties.COMPONENT_NAME)
public class GaeaSecurityProperties {

    /**
     * 组件名称
     */
    public final static String COMPONENT_NAME = "security";


    /**
     * 显示验证码，默认三次
     */
    private int captchaTimes = 3;

    /**
     * 锁定次数
     */
    private int lockTimes = 5;


    /**
     * 密码过期时长,默认90天
     */
    private int credentialsExpiredLength = 90;

    /**
     * 离密码过期还有一天时，提醒
     */
    private int credentialsExpiredRemind = 1;

    /**
     * 关闭校验
     */
    private boolean authDisabled;

    /**
     * 默认密码
     */
    private String defaultPassword = "123456";

    /**
     * 解决单体服务跨域请求
     */
    private boolean corsEnabled = false;

    public int getCaptchaTimes() {
        return captchaTimes;
    }

    public void setCaptchaTimes(int captchaTimes) {
        this.captchaTimes = captchaTimes;
    }

    public int getLockTimes() {
        return lockTimes;
    }

    public void setLockTimes(int lockTimes) {
        this.lockTimes = lockTimes;
    }

    public boolean getAuthDisabled() {
        return authDisabled;
    }

    public void setAuthDisabled(boolean authDisabled) {
        this.authDisabled = authDisabled;
    }

    public int getCredentialsExpiredLength() {
        return credentialsExpiredLength;
    }

    public void setCredentialsExpiredLength(int credentialsExpiredLength) {
        this.credentialsExpiredLength = credentialsExpiredLength;
    }

    public int getCredentialsExpiredRemind() {
        return credentialsExpiredRemind;
    }

    public void setCredentialsExpiredRemind(int credentialsExpiredRemind) {
        this.credentialsExpiredRemind = credentialsExpiredRemind;
    }

    public String getDefaultPassword() {
        return defaultPassword;
    }

    public void setDefaultPassword(String defaultPassword) {
        this.defaultPassword = defaultPassword;
    }

    public boolean isCorsEnabled() {
        return corsEnabled;
    }

    public void setCorsEnabled(boolean corsEnabled) {
        this.corsEnabled = corsEnabled;
    }
}
