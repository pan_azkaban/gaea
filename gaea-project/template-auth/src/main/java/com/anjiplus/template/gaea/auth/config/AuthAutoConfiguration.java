package com.anjiplus.template.gaea.auth.config;

import com.anji.plus.gaea.security.plus.runner.ApplicationInitRunner;
import com.anjiplus.template.gaea.auth.listener.UserEventListener;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * 权限模块配置类
 * @author lr
 * @since 2021-03-01
 */
@Configuration
public class AuthAutoConfiguration {

    /**
     * restful客户端
     * @return
     */
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    /**
     * 需调用第三方服务时使用
     * @return
     */
    @Bean(name = "authRestTemplate")
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    /**
     * 应用启动后执行，用于初始化请求信息到权限表中
     * @return
     */
    @Bean
    public ApplicationInitRunner applicationInitRunner() {
        return new ApplicationInitRunner();
    }

    @Bean
    public UserEventListener accountLockListener() {
        return new UserEventListener();
    }


}
