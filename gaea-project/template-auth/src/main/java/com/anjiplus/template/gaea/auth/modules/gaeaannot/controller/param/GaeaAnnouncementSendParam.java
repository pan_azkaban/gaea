package com.anjiplus.template.gaea.auth.modules.gaeaannot.controller.param;

import com.anji.plus.gaea.curd.params.PageParam;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 功能描述：
 *
 * @Author: peiyanni
 * @Date: 2021/3/29 16:24
 */
@Getter
@Setter
public class GaeaAnnouncementSendParam extends PageParam implements Serializable {
    /**
     * 标题
     */
    private String title;
    /**
     * 发布人
     */
    private String publisher;
    /**
     * 当前用户
     */
    private String username;
}
