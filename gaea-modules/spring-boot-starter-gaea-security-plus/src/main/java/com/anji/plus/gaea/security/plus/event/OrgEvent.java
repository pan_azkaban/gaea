package com.anji.plus.gaea.security.plus.event;

import com.anji.plus.gaea.constant.BaseOperationEnum;
import com.anji.plus.gaea.security.plus.modules.org.dao.entity.GaeaOrg;
import org.springframework.context.ApplicationEvent;

/**
 * @author lr
 * @since 2021-05-19
 */
public class OrgEvent extends ApplicationEvent {

    private GaeaOrg gaeaOrg;

    private BaseOperationEnum baseOperationEnum;

    public OrgEvent(GaeaOrg gaeaOrg, BaseOperationEnum baseOperationEnum) {
        super(gaeaOrg);
        this.gaeaOrg = gaeaOrg;
        this.baseOperationEnum = baseOperationEnum;
    }

    public GaeaOrg getGaeaOrg() {
        return gaeaOrg;
    }

    public void setGaeaOrg(GaeaOrg gaeaOrg) {
        this.gaeaOrg = gaeaOrg;
    }

    public BaseOperationEnum getBaseOperationEnum() {
        return baseOperationEnum;
    }

    public void setBaseOperationEnum(BaseOperationEnum baseOperationEnum) {
        this.baseOperationEnum = baseOperationEnum;
    }
}
