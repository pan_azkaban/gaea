package com.anji.plus.gaea.curd.controller;

import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.code.ResponseCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.anji.plus.gaea.bean.ResponseBean.builder;

/**
 * 通用
 * @author lr
 * @since 2021-04-29
 */
public abstract class BaseResponse {

    /**
     * 记录日志
     */
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 构建成功响应实例
     *
     * @return
     */
    public ResponseBean responseSuccess() {
        return builder().build();
    }


    /**
     * 构建成功响应实例
     *
     * @param code 响应编码
     * @param args 响应参数
     * @return
     */
    public ResponseBean responseSuccess(String code, Object... args) {
        return builder().code(code).args(args).build();
    }

    /**
     * 构建成功响应实例
     *
     * @param data
     * @return
     */
    public ResponseBean responseSuccessWithData(Object data) {
        return builder().data(data).build();
    }

    /**
     * 构建成功响应实例
     * @param code 响应编码
     * @param data 响应数据
     * @param args 响应参数
     * @return
     */
    public ResponseBean responseSuccessWithData(String code, Object data, Object... args) {
        return builder().code(code).data(data).args(args).build();
    }

    /***
     * short for responseSuccessWithData
     * @param code
     * @param data
     * @param args
     * @return
     */
    public ResponseBean successWithData(String code, Object data, Object... args) {
        return responseSuccessWithData(code,data,args);
    }

    /**
     * 构建成功响应实例,short for responseSuccessWithData
     *
     * @param data
     * @return
     */
    public ResponseBean successWithData(Object data) {
        return builder().data(data).build();
    }

    /**
     * 构建失败响应实例
     *
     * @return
     */
    public ResponseBean failure() {
        return builder().code(ResponseCode.FAIL_CODE).build();
    }

    /**
     * 构建失败响应实例
     *
     * @param code 响应编码
     * @param args 响应参数
     * @return
     */
    public ResponseBean failure(String code, Object... args) {
        return builder().code(code).args(args).build();
    }

    /**
     * 构建失败响应实例,包含响应数据
     * @param code 响应编码
     * @param data 响应数据
     * @param args 响应参数
     * @return
     */
    public ResponseBean failureWithData(String code, Object data, Object... args) {
        return builder().code(code).args(args).data(data).build();
    }

}
