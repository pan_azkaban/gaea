/*
 *  Copyright 2019-2020 Zheng Jie
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.anjiplus.template.gaea.generator.modules.table.dao;

import java.io.Serializable;

import org.springframework.data.annotation.Transient;

import com.anji.plus.gaea.curd.entity.GaeaBaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 代码生成配置
 * @author Zheng Jie
 * @date 2019-01-03
 */
@Data
@NoArgsConstructor
@TableName("gaea_generator_table")
public class GeneratorTable extends GaeaBaseEntity implements Serializable {

    public GeneratorTable(String tableName) {
        this.tableName = tableName;
    }

    @Transient
    @TableField(exist = false)
    private String workspace;
    /**
     * project和moduleName前缀一致
     */
    private String project;
    /***
     * 项目根目录名字
     */
    private String projectRoot;

    @ApiModelProperty(value = "schema")
    private String schemaName;

    @ApiModelProperty(value = "表名")
    private String tableName;

    @ApiModelProperty(value = "接口名称")
    private String apiAlias;

    @ApiModelProperty(value = "包路径")
    private String pack;

    @ApiModelProperty(value = "模块名")
    private String moduleName;

    @ApiModelProperty(value = "前端文件路径")
    private String path;

    @ApiModelProperty(value = "前端js文件路径")
    private String apiPath;

    @ApiModelProperty(value = "作者")
    private String author;

    @ApiModelProperty(value = "表前缀")
    private String prefix;

    @ApiModelProperty(value = "是否覆盖")
    private Boolean cover = false;
    /**
     * 模板类型
     */
    private String catatype = "Mybatis";
    /**
     * 生成页面
     */
    private Boolean genUi = true;
    /**
     * 页面生成类型：zh,i81n
     */
    private String genType = "i18n";
}
