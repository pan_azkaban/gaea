
package com.anjiplus.template.gaea.business.modules.deviceinfo.service;

import com.anjiplus.template.gaea.business.base.BaseService;
import com.anjiplus.template.gaea.business.modules.deviceinfo.dao.entity.DeviceInfo;
import com.anjiplus.template.gaea.business.modules.deviceinfo.controller.param.DeviceInfoParam;

/**
* @desc DeviceInfo 设备信息服务接口
* @author WongBin
* @date 2021-04-07 17:44:28.598
**/
public interface DeviceInfoService extends BaseService<DeviceInfoParam, DeviceInfo> {

    /***
     * 查询详情
     *
     * @param id
     * @return
     */
    DeviceInfo getDetail(Long id);
}