package com.anji.plus.gaea.security.plus.modules.org.dao;

import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import com.anji.plus.gaea.security.plus.modules.org.dao.entity.GaeaOrg;
import org.apache.ibatis.annotations.Mapper;

/**
 * 组织(GaeaOrg)Mapper
 *
 * @author lr
 * @since 2021-02-02 13:37:33
 */
@Mapper
public interface GaeaOrgMapper extends GaeaBaseMapper<GaeaOrg> {


}
