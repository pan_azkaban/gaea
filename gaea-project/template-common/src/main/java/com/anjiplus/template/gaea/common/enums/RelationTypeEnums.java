package com.anjiplus.template.gaea.common.enums;

/**
 * 功能描述：
 * 消息关联的业务类型枚举
 *
 * @Author: peiyanni
 * @Date: 2021/4/15 12:50
 */
public enum RelationTypeEnums {
    SYSTEM("EVENT_TYPE_SYSTEM_MSG","系统公告");
    //TODO add other type

    private String value;
    private String name;

    RelationTypeEnums(String value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
