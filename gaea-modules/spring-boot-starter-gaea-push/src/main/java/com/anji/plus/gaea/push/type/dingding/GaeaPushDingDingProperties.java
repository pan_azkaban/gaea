package com.anji.plus.gaea.push.type.dingding;

import com.anji.plus.gaea.push.GaeaPushProperties;
import com.anji.plus.gaea.push.type.sms.GaeaPushSmsProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static com.anji.plus.gaea.constant.GaeaConstant.COMPONENT_PREFIX;

/**
 * @ClassName GaeaPushDingDingProperties
 * @Description: TODO
 * @Author dingkaiqiang
 * @Date 2021-04-07
 * @Version V1.0
 **/

@ConfigurationProperties(prefix = GaeaPushDingDingProperties.COMPONENT_DINGDING_NAME)
public class GaeaPushDingDingProperties {

    /**
     * 组件名称
     */
    public final static String COMPONENT_DINGDING_NAME = COMPONENT_PREFIX + GaeaPushProperties.COMPONENT_NAME + ".dingding";

    /** 应用代理ID*/
    public Long agentId;
    /** 应用key*/
    public String appkey;
    /** 应用秘钥*/
    public  String appSecret;


    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public String getAppkey() {
        return appkey;
    }

    public void setAppkey(String appkey) {
        this.appkey = appkey;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }
}
