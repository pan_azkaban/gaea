import request from '@/utils/request'
// 工作流模型查询
export function processModelPageList(params) {
  return request({
    url: '/business/actDeModel/pageList',
    method: 'get',
    params,
  })
}

// 新增模型
export function addProcessModel(data) {
  return request({
    url: '/flowable/app/rest/models',
    method: 'POST',
    data,
  })
}

// 删除
export function deleteProcessModel(data) {
  return request({
    url: `/business/actDeModel/delete/batch`,
    method: 'POST',
    data,
  })
}

// 部署模型
export function deployProcessModel(modelId) {
  return request({
    url: `/business/actDeModel/publish/${modelId}`,
    method: 'GET',
  })
}

// 模型部署流程定义
export function processPageList(params) {
  return request({
    url: '/business/process/pageList',
    method: 'get',
    params,
  })
}

// 流程删除
export function deleteProcess(data) {
  return request({
    url: `/business/process/delete/batch`,
    method: 'POST',
    data,
  })
}

// 发起流程
export function startProcess(key, data) {
  return request({
    url: `/business/process/start/process/${key}`,
    method: 'POST',
    data,
  })
}

// 获取表单数据
export function getFormData(processId) {
  return request({
    url: `/business/process/form/data/${processId}`,
    method: 'GET',
  })
}

// 待办任务
export function runtimeTaskPage(params) {
  return request({
    url: '/business/process/my/running/task',
    method: 'get',
    params,
  })
}

// 已办任务
export function historyTaskPage(params) {
  return request({
    url: '/business/process/my/history/task',
    method: 'get',
    params,
  })
}

// 完成任务
export function completeTask(key, data) {
  return request({
    url: `/business/process/complete/task/${key}`,
    method: 'POST',
    data,
  })
}

// 任务详情
export function taskDetail(taskId) {
  return request({
    url: `/business/process/task/detail/${taskId}`,
    method: 'GET',
  })
}

// 实例详情
export function processInstanceDetail(processInstanceId) {
  return request({
    url: `/business/process/processInstance/detail/${processInstanceId}`,
    method: 'GET',
  })
}
