package com.anjiplus.template.gaea.business.modules.inf.app.service.impl;

import com.anjiplus.template.gaea.business.modules.inf.app.controller.dto.GaeaInfAppRelationDTO;
import com.anjiplus.template.gaea.business.modules.inf.app.dao.entity.GaeaInfAppRelation;
import com.anjiplus.template.gaea.business.modules.inf.app.dao.GaeaInfAppRelationMapper;
import com.anjiplus.template.gaea.business.modules.inf.app.service.GaeaInfAppRelationService;
import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 接口应用关系表(GaeaInfAppRelation)ServiceImpl
 *
 * @author ultrajiaming
 * @since 2021-04-08 17:37:08
 */
@Service
public class GaeaInfAppRelationServiceImpl implements GaeaInfAppRelationService {
    @Autowired
    private GaeaInfAppRelationMapper  gaeaInfAppRelationMapper;

    @Override
    public GaeaBaseMapper<GaeaInfAppRelation> getMapper() {
        return  gaeaInfAppRelationMapper;
    }

    @Override
    public List<GaeaInfAppRelationDTO> listInfApp(GaeaInfAppRelationDTO gaeaInfAppRelationDTO) {
        GaeaInfAppRelation gaeaInfAppRelation = new GaeaInfAppRelation();
        BeanUtils.copyProperties(gaeaInfAppRelationDTO, gaeaInfAppRelation);
        List<GaeaInfAppRelation> gaeaInfAppRelationList = list(Wrappers.lambdaQuery(gaeaInfAppRelation));
        List<GaeaInfAppRelationDTO> results = gaeaInfAppRelationList.stream().map(infAppRelation -> {
            GaeaInfAppRelationDTO infAppRelationDTO = new GaeaInfAppRelationDTO();
            BeanUtils.copyProperties(infAppRelation, infAppRelationDTO);
            return infAppRelationDTO;
        }).collect(Collectors.toList());
        return results;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateTimes(List<GaeaInfAppRelationDTO> dtoList) {
        if (CollectionUtils.isEmpty(dtoList)) {
            return;
        }
        for (GaeaInfAppRelationDTO dto : dtoList) {
            GaeaInfAppRelation entity = new GaeaInfAppRelation();
            BeanUtils.copyProperties(dto, entity);
            gaeaInfAppRelationMapper.updateById(entity);
        }
    }
}
