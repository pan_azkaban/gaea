# 项目介绍
盖亚目前微服务版本已完成，后续会继续更新新的功能。
- 微服务
源码地址：<https://gitee.com/anji-plus/gaea/tree/master/gaea-project-template>；
数据库脚本：[数据库脚本](https://gitee.com/anji-plus/gaea/tree/master/gaea-project-template/gaea-refer-doc/gaea-db)
系统配置：[配置文件](https://gitee.com/anji-plus/gaea/tree/master/gaea-project-template/gaea-refer-doc/gaea-nacos)



## 文件结构
### 系统目录
- 
```
├── doc                                           文档源码
│   ├── docs
│   ├── package.json
│   └── README.md
├── gaea-modules                                  盖亚组件库
│   ├── pom.xml                                   gaea父pom，jar版本管理
│   ├── README.md
│   ├── spring-boot-gaea                          gaea底层库，包含crud、国际化等
│   ├── spring-boot-starter-gaea-archiver         数据库归档组件
│   ├── spring-boot-starter-gaea-archiver-test    数据库归档组件测试示例
│   ├── spring-boot-starter-gaea-export           导出excel、pdf组件
│   ├── spring-boot-starter-gaea-flowable         flowable工作流组件
│   ├── spring-boot-starter-gaea-log              操作日志
│   ├── spring-boot-starter-gaea-push             邮件、钉钉、阿里|极光|安吉短信推送
│   └── spring-boot-starter-gaea-security         权限组件
├── LICENSE
├── README.md
├── gaea-project-template                         spring-cloud工程模板
│   ├── gaea-refer-doc
│   ├── template-auth                             权限服务
│   ├── template-business                         业务服务
│   ├── template-common                           基础工具包
│   ├── template-gateway                          网关
│   ├── template-generator
│   ├── template-ui                               前端vue工程
│   ├── pom.xml
│   └── README.md                                 如何改成spring-boot工程模板
```

### 前端
```
├── build                      # 构建相关
├── mock                       # 项目mock 模拟数据
├── public                     # 静态资源
│   │── favicon.ico            # favicon图标
│   └── index.html             # html模板
├── src                        # 源代码
│   ├── api                    # 所有请求
│   ├── assets                 # 主题 字体等静态资源
│   ├── components             # 全局公用组件
│   ├── directive              # 全局指令
│   ├── icons                  # 项目所有 svg icons
│   ├── lang                   # 国际化 language
│   ├── layout                 # 全局 layout
│   ├── router                 # 路由
│   ├── store                  # 全局 store管理
│   ├── styles                 # 全局样式
│   ├── utils                  # 全局公用方法
│   ├── views                  # views 所有页面
│   ├── App.vue                # 入口页面
│   ├── main.js                # 入口文件 加载组件 初始化等
│   └── permission.js          # 权限管理
├── .env.xxx                   # 环境变量配置
├── .eslintrc.js               # eslint 配置项
├── .babelrc                   # babel-loader 配置
├── .travis.yml                # 自动化CI配置
├── vue.config.js              # vue-cli 配置
├── postcss.config.js          # postcss 配置
└── package.json               # package.json
```
## 核心技术
### 后端
- [Spring Boot](https://spring.io/projects/spring-boot/)：Spring Boot是一款开箱即用框架，让我们的Spring应用变的更轻量化、更快的入门。 在主程序执行main函数就可以运行。你也可以打包你的应用为jar并通过使用java -jar来运行你的Web应用；
- [Spring Cloud](https://spring.io/projects/spring-cloud): Spring Cloud是一系列框架的集合，如：配置管理、服务注册与发现、断路器、数据监控等。
- [Mybatic-plus](https://mp.baomidou.com/)：Mybatic-plus（简称 MP）是一个 MyBatis (opens new window) 的增强工具。
### 前端
- [npm](https://www.npmjs.com/)：node.js的包管理工具，用于统一管理我们前端项目中需要用到的包、插件、工具、命令等，便于开发和维护。
- [webpack](https://webpack.docschina.org/)：用于现代 JavaScript 应用程序的_静态模块打包工具
- [ES6](https://es6.ruanyifeng.com/)：Javascript的新版本，ECMAScript6的简称。利用ES6我们可以简化我们的JS代码，同时利用其提供的强大功能来快速实现JS逻辑。
- [vue-cli](https://cli.vuejs.org/)：Vue的脚手架工具，用于自动生成Vue项目的目录及文件。
- [vue-router](https://router.vuejs.org/)： Vue提供的前端路由工具，利用其我们实现页面的路由控制，局部刷新及按需加载，构建单页应用，实现前后端分离。
- [vuex](https://vuex.vuejs.org/)：Vue提供的状态管理工具，用于统一管理我们项目中各种数据的交互和重用，存储我们需要用到数据对象。
- [element-ui](https://element.eleme.cn/#/zh-CN)：基于MVVM框架Vue开源出来的一套前端ui组件。
- [vue-element-admin](https://panjiachen.gitee.io/vue-element-admin-site/)：后台前端解决方案,它基于 vue 和 element-ui实现
