package com.anjiplus.template.gaea.generator.modules.generator.controller.param;

import com.anji.plus.gaea.curd.params.PageParam;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class GeneratorParam extends PageParam implements Serializable {

    private String schema;

    private String tableName;

}
