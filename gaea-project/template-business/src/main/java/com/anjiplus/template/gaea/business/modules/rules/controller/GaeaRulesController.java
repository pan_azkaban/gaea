package com.anjiplus.template.gaea.business.modules.rules.controller;

import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.curd.controller.GaeaBaseController;
import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anjiplus.template.gaea.business.modules.rules.controller.dto.GaeaRulesDTO;
import com.anjiplus.template.gaea.business.modules.rules.controller.param.GaeaRulesParam;
import com.anjiplus.template.gaea.business.modules.rules.dao.entity.GaeaRules;
import com.anjiplus.template.gaea.business.modules.rules.service.GaeaRulesService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * (GaeaRules)实体类
 *
 * @author lirui
 * @since 2021-03-12 10:38:19
 */
@RestController
@RequestMapping("/gaeaRules")
@Api(value = "/gaeaRules", tags = "")
public class GaeaRulesController extends GaeaBaseController<GaeaRulesParam, GaeaRules, GaeaRulesDTO> {
    @Autowired
    private GaeaRulesService gaeaRulesService;

    @Override
    public GaeaBaseService<GaeaRulesParam, GaeaRules> getService() {
        return gaeaRulesService;
    }

    @Override
    public GaeaRules getEntity() {
        return new GaeaRules();
    }

    @Override
    public GaeaRulesDTO getDTO() {
        return new GaeaRulesDTO();
    }

    /**
     *  执行规则
     * @param dto
     * @return
     */
    @PostMapping("/executeRules")
    public ResponseBean executeRules(@RequestBody GaeaRulesDTO dto) {
        return gaeaRulesService.executeRules(dto.getRuleCode(), dto.getRuleContent(), dto.getRuleDemo());
    }
}
