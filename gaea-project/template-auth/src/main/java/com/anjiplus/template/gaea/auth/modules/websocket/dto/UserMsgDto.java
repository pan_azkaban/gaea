package com.anjiplus.template.gaea.auth.modules.websocket.dto;

import com.anjiplus.template.gaea.common.enums.RelationTypeEnums;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 功能描述：
 *
 * @Author: peiyanni
 * @Date: 2021/4/9 13:43
 */
@Getter
@Setter
public class UserMsgDto implements Serializable {
    /**
     * 消息唯一标识
     */
    private String id;
    /**
     * 消息内容
     */
    private String message;
    /**
     * 消息接收用户
     */
    private String username;
    /**
     * 关联业务Id，如果没有则不传
     */
    private String eventData;
    /**
     * 关联业务类型，如果没有则不传
     */
    private RelationTypeEnums eventType;

    public UserMsgDto(String id, String message, String username, RelationTypeEnums eventType) {
        this.id = id;
        this.message = message;
        this.username = username;
        this.eventType = eventType;
    }

    public UserMsgDto(String id, String message, String username, String eventData, RelationTypeEnums eventType) {
        this.id = id;
        this.message = message;
        this.username = username;
        this.eventData = eventData;
        this.eventType = eventType;
    }
}
