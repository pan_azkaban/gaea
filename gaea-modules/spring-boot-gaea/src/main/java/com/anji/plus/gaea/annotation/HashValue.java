package com.anji.plus.gaea.annotation;

import java.lang.annotation.*;

/**
 * 缓存，将对应key,value加入缓存
 * @author lr
 * @since 2021-04-19
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface HashValue {

    /**
     * HashValue对应的缓存key
     */
    String key();
}
