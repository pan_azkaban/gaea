/*
Navicat MySQL Data Transfer

Source Server Version : 50728
Source Database       : gaea_auth

Target Server Type    : MYSQL
Target Server Version : 50728
File Encoding         : 65001

Date: 2021-04-13 11:10:19
*/

SET FOREIGN_KEY_CHECKS=0;


-- ----------------------------
-- Table structure for gaea_announcement
-- ----------------------------
DROP TABLE IF EXISTS `gaea_announcement`;
CREATE TABLE `gaea_announcement` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL COMMENT '标题',
  `msg_content` longtext COMMENT '内容',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `priority` varchar(10) DEFAULT NULL COMMENT '优先级 （优先级（L低，M中，H高））',
  `msg_category` varchar(10) DEFAULT NULL COMMENT '消息类型1:通知公告2:系统消息',
  `msg_type` varchar(10) DEFAULT NULL COMMENT '通告对象类型（USER:指定用户，ALL:全体用户）',
  `send_status` int(11) DEFAULT '0' COMMENT '发布状态（0未发布，1已发布，2已撤销）',
  `publisher` varchar(64) DEFAULT NULL COMMENT '发布人',
  `send_time` datetime DEFAULT NULL COMMENT '发布时间',
  `cancel_time` datetime DEFAULT NULL COMMENT '撤销时间',
  `user_names` longtext COMMENT '指定用户，多个逗号隔开',
  `relation_info` longtext COMMENT '关联的业务数据（唯一标识等）',
  `relation_type` varchar(30) DEFAULT NULL COMMENT '关联业务类型',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COMMENT='系统公告信息表';


-- ----------------------------
-- Table structure for gaea_announcement_send
-- ----------------------------
DROP TABLE IF EXISTS `gaea_announcement_send`;
CREATE TABLE `gaea_announcement_send` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `annt_id` bigint(20) DEFAULT NULL COMMENT '系统公告id',
  `user_name` varchar(64) DEFAULT NULL COMMENT '用户登录名',
  `read_flag` int(11) DEFAULT '0' COMMENT '阅读状态（0未读，1已读）',
  `read_time` datetime DEFAULT NULL COMMENT '阅读时间',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `annot_index_01` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8 COMMENT='系统公告发送阅读记录表';

-- ----------------------------
-- Table structure for gaea_authority
-- ----------------------------
DROP TABLE IF EXISTS `gaea_authority`;
CREATE TABLE `gaea_authority` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `application_name` varchar(64) DEFAULT NULL COMMENT '应用名称',
  `scan_annotation` varchar(64) DEFAULT NULL COMMENT '扫描类型：0：不扫描注解j即扫描所有，1：扫描注解',
  `auth_code` varchar(64) NOT NULL COMMENT '权限代码',
  `auth_name` varchar(64) DEFAULT NULL COMMENT '权限名称',
  `parent_code` varchar(64) DEFAULT NULL COMMENT '父级代码',
  `path` varchar(256) DEFAULT NULL COMMENT '请求路径',
  `sort` int(8) DEFAULT '0' COMMENT '排序，升序',
  `enabled` int(1) DEFAULT '1' COMMENT '0--禁用 1--启用',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(64) DEFAULT NULL COMMENT '修改人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `version` int(8) DEFAULT NULL COMMENT '版本号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13039 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='权限表';

-- ----------------------------
-- Table structure for gaea_query_user_condition
-- ----------------------------
DROP TABLE IF EXISTS `gaea_query_user_condition`;
CREATE TABLE `gaea_query_user_condition` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `menu_code` varchar(64) DEFAULT NULL COMMENT '菜单ID',
  `comm_sql` text COMMENT '查询sql',
  `label` varchar(1024) DEFAULT NULL COMMENT '查询条件名称label',
  `is_disabled` int(11) DEFAULT '0' COMMENT '0:可用,1:已作废',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人ID',
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人ID',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `table_code` varchar(20) DEFAULT NULL COMMENT '表code',
  `search_name` varchar(50) DEFAULT NULL COMMENT '常用查询名称',
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_gcc_01` (`menu_code`,`create_by`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gaea_log
-- ----------------------------
DROP TABLE IF EXISTS `gaea_log`;
CREATE TABLE `gaea_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_name` varchar(64) DEFAULT NULL COMMENT '用户名称',
  `request_url` varchar(512) DEFAULT NULL COMMENT '请求路径',
  `page_title` varchar(512) DEFAULT NULL COMMENT '页面或按钮标题',
  `request_param` mediumtext COMMENT '请求参数',
  `response_param` longtext COMMENT '响应参数',
  `source_ip` varchar(16) DEFAULT NULL COMMENT '来源IP',
  `request_time` datetime DEFAULT NULL COMMENT '访问时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_by` varchar(64) CHARACTER SET utf8mb4 DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=43465 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for gaea_menu
-- ----------------------------
DROP TABLE IF EXISTS `gaea_menu`;
CREATE TABLE `gaea_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `menu_code` varchar(64) NOT NULL COMMENT '菜单代码',
  `menu_name` varchar(64) NOT NULL COMMENT '菜单名称',
  `sys_code` varchar(64) DEFAULT NULL COMMENT '系统代码，参考数据字典SYSTEM_CODE',
  `parent_code` varchar(64) DEFAULT NULL COMMENT '父级id',
  `path` varchar(256) DEFAULT NULL COMMENT '菜单路径',
  `menu_icon` varchar(64) DEFAULT NULL COMMENT 'base64图标或者iconfont字体',
  `sort` int(8) NOT NULL DEFAULT '0' COMMENT '排序，升序',
  `enabled` int(1) NOT NULL DEFAULT '1' COMMENT '0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG',
  `delete_flag` int(1) NOT NULL DEFAULT '0' COMMENT ' 0--未删除 1--已删除 DIC_NAME=DEL_FLAG',
  `redirect_url` varchar(128) DEFAULT NULL COMMENT '重定向地址',
  `component` varchar(128) DEFAULT NULL COMMENT '该路由需要加载的页面组件的文件路径',
  `hidden` int(11) DEFAULT '0' COMMENT '是否隐藏菜单 0为未隐藏 默认0',
  `always_show` int(11) DEFAULT '1' COMMENT '是否一直显示 0 否 1.是',
  `create_by` varchar(64) NOT NULL COMMENT '创建人',
  `update_by` varchar(64) DEFAULT NULL COMMENT '修改人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `version` int(8) DEFAULT NULL COMMENT '版本号',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `IDX1` (`menu_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1187 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='菜单表';

-- ----------------------------
-- Table structure for gaea_menu_authority
-- ----------------------------
DROP TABLE IF EXISTS `gaea_menu_authority`;
CREATE TABLE `gaea_menu_authority` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `org_code` varchar(64) DEFAULT NULL COMMENT '机构编码',
  `menu_code` varchar(64) NOT NULL COMMENT '菜单id',
  `auth_code` varchar(64) NOT NULL COMMENT '操作id，如按钮',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(64) DEFAULT NULL COMMENT '修改人',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `version` tinyint(8) DEFAULT NULL COMMENT '版本号',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_ara_authority_id` (`auth_code`) USING BTREE,
  KEY `idx_ara_role_id` (`menu_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=601 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='菜单按钮对应关系';

-- ----------------------------
-- Table structure for gaea_menu_extension
-- ----------------------------
DROP TABLE IF EXISTS `gaea_menu_extension`;
CREATE TABLE `gaea_menu_extension` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `table_code` varchar(255) DEFAULT NULL COMMENT '表格code',
  `menu_code` varchar(64) DEFAULT NULL COMMENT '菜单id',
  `name` varchar(100) DEFAULT NULL COMMENT '列中文名',
  `code` varchar(100) DEFAULT NULL COMMENT '列编码-和返回的表格数据里的key一致',
  `sortable` int(11) DEFAULT '0' COMMENT '是否可排序,0-不可排序，1-可排序',
  `sort_no` int(11) DEFAULT NULL COMMENT '排序号',
  `visible` int(11) DEFAULT '1' COMMENT '0:不可见；1:可见',
  `width` int(11) DEFAULT NULL COMMENT '宽度',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人ID',
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人ID',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_disabled` int(11) DEFAULT '0' COMMENT '删除标志',
  `sort_code` varchar(200) DEFAULT NULL COMMENT '排序字段',
  `sort_order` int(11) DEFAULT '1' COMMENT '1升序，0降序，默认1',
  `group_name` varchar(100) DEFAULT NULL COMMENT '组名',
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_gme_01` (`table_code`,`menu_code`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gaea_menu_user_extension
-- ----------------------------
DROP TABLE IF EXISTS `gaea_menu_user_extension`;
CREATE TABLE `gaea_menu_user_extension` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `table_code` varchar(255) DEFAULT NULL COMMENT '表格code',
  `menu_code` varchar(64) DEFAULT NULL COMMENT '菜单id',
  `username` varchar(64) DEFAULT NULL COMMENT '用户ID',
  `name` varchar(100) DEFAULT NULL COMMENT '列中文名',
  `code` varchar(100) DEFAULT NULL COMMENT '列编码-和返回的表格数据里的key一致',
  `sortable` int(11) DEFAULT '0' COMMENT '是否可排序,0-不可排序，1-可排序',
  `sort_no` int(11) DEFAULT NULL COMMENT '排序号',
  `visible` int(11) DEFAULT '1' COMMENT '0:不可见；1:可见',
  `width` int(11) DEFAULT NULL COMMENT '宽度',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人ID',
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人ID',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_disabled` int(11) DEFAULT '0' COMMENT '删除标志',
  `sort_code` varchar(200) DEFAULT NULL COMMENT '排序字段',
  `sort_order` int(11) DEFAULT '1' COMMENT '1升序，0降序，默认1',
  `group_name` varchar(100) DEFAULT NULL COMMENT '组名',
  `version` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_gmue_01` (`table_code`,`menu_code`,`username`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gaea_org
-- ----------------------------
DROP TABLE IF EXISTS `gaea_org`;
CREATE TABLE `gaea_org` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `org_code` varchar(32) NOT NULL COMMENT '机构代码',
  `org_name` varchar(64) NOT NULL COMMENT '机构名称',
  `org_parent_code` varchar(64) DEFAULT NULL COMMENT '上级组织code',
  `org_parent_name` varchar(64) DEFAULT NULL COMMENT '上级部门名字',
  `out_org_code` varchar(64) DEFAULT NULL COMMENT '外部机构代码（从外系统同步过来得编码）',
  `out_org_parent_code` varchar(64) DEFAULT NULL COMMENT '外部机构父级编码（从外系统同步过来得父级编码）',
  `org_level` varchar(32) DEFAULT NULL COMMENT '机构级别',
  `org_type` varchar(32) DEFAULT NULL COMMENT '组织类型',
  `linkman` varchar(32) DEFAULT NULL COMMENT '联系人',
  `mobile_phone` varchar(32) DEFAULT NULL COMMENT '手机号',
  `telephone` varchar(32) DEFAULT NULL COMMENT '固定电话',
  `enabled` int(1) NOT NULL DEFAULT '1' COMMENT '0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG',
  `delete_flag` int(1) DEFAULT '0' COMMENT ' 0--未删除 1--已删除 DIC_NAME=DEL_FLAG',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述信息',
  `create_by` varchar(64) NOT NULL COMMENT '创建用户编码',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '修改用户编码',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  `version` tinyint(8) DEFAULT NULL COMMENT '版本',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_code` (`org_code`,`enabled`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='组织';

-- ----------------------------
-- Table structure for gaea_query_condition
-- ----------------------------
DROP TABLE IF EXISTS `gaea_query_condition`;
CREATE TABLE `gaea_query_condition` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `table_code` varchar(20) DEFAULT NULL COMMENT '表格code',
  `menu_code` varchar(64) DEFAULT NULL COMMENT '菜单ID',
  `name` varchar(64) DEFAULT NULL COMMENT '条件名称-key',
  `name_value` varchar(64) DEFAULT NULL COMMENT '条件名称-value',
  `value_type` int(11) DEFAULT '1' COMMENT '条件值类型(1:字符串,2:数字,3:日期)',
  `query_condition` int(11) DEFAULT '1' COMMENT '查询条件(1:>、2:<、3:>=、4:<=、5:LIKE)(暂时不使用此字段)',
  `type` int(11) DEFAULT '1' COMMENT '条件类型(1:文本框、2:下拉框、3:联想控件、4:日期控件、5:数字、6:多记录文本)(1:文本框、2:下拉框、3:日期控件、4:多记录文本、5:数字、6:联想控件)（此处只取前四种类型，下拉框和联想控件合并，数字类型归为文本）',
  `place` int(11) DEFAULT '1' COMMENT '排列序号',
  `data_source` int(11) DEFAULT '2' COMMENT '数据源(1:接口、2:固定内容)(联想控件接口统一格式,code,name)',
  `data_source_value` varchar(100) DEFAULT NULL COMMENT '数据源值',
  `is_disabled` int(11) DEFAULT '0' COMMENT '0:可用,1:已作废',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人ID',
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人ID',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `date_precision` varchar(20) DEFAULT NULL COMMENT '日期精度',
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_gqc_01` (`table_code`,`menu_code`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gaea_role
-- ----------------------------
DROP TABLE IF EXISTS `gaea_role`;
CREATE TABLE `gaea_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `org_code` varchar(64) DEFAULT NULL COMMENT '组织编号',
  `role_code` varchar(64) DEFAULT NULL COMMENT '角色编码',
  `role_name` varchar(64) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL COMMENT '1：可用 0：禁用',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述',
  `create_by` varchar(64) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `version` tinyint(8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4 COMMENT='角色';

-- ----------------------------
-- Table structure for gaea_role_menu_authority
-- ----------------------------
DROP TABLE IF EXISTS `gaea_role_menu_authority`;
CREATE TABLE `gaea_role_menu_authority` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `org_code` varchar(64) DEFAULT NULL COMMENT '机构编码',
  `role_code` varchar(64) DEFAULT NULL,
  `menu_code` varchar(64) DEFAULT NULL,
  `auth_code` varchar(64) DEFAULT NULL COMMENT '权限编码',
  `auth_path` varchar(128) DEFAULT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(64) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `version` int(8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11646 DEFAULT CHARSET=utf8 COMMENT='角色与权限对应关系';

-- ----------------------------
-- Table structure for gaea_role_org
-- ----------------------------
DROP TABLE IF EXISTS `gaea_role_org`;
CREATE TABLE `gaea_role_org` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_code` varchar(64) NOT NULL COMMENT 'roleid',
  `org_code` varchar(64) NOT NULL COMMENT '组织id',
  `create_by` varchar(64) CHARACTER SET utf8mb4 DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_by` varchar(64) CHARACTER SET utf8mb4 DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `version` tinyint(8) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1250 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色与组织';

-- ----------------------------
-- Table structure for gaea_setting
-- ----------------------------
DROP TABLE IF EXISTS `gaea_setting`;
CREATE TABLE `gaea_setting` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `setting_name` varchar(64) NOT NULL COMMENT '参数名称',
  `setting_label` varchar(128) NOT NULL COMMENT '参数描述',
  `setting_type` varchar(16) NOT NULL COMMENT '参数值类型，input input-number json-array',
  `setting_form` varchar(10240) NOT NULL COMMENT '参数表单',
  `setting_value` varchar(4096) DEFAULT NULL COMMENT '表单保存的值，int\\String\\Json',
  `enable` int(1) NOT NULL DEFAULT '1' COMMENT '0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG',
  `remark` varchar(512) DEFAULT NULL COMMENT '备注',
  `create_by` varchar(64) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `version` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `IDX1` (`setting_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for gaea_user
-- ----------------------------
DROP TABLE IF EXISTS `gaea_user`;
CREATE TABLE `gaea_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `password_update_time` datetime DEFAULT NULL COMMENT '密码更新时间',
  `nickname` varchar(64) DEFAULT NULL COMMENT '真实姓名',
  `email` varchar(64) DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '1' COMMENT '1：可用 0：禁用',
  `multi_login` tinyint(1) DEFAULT NULL COMMENT '设置多终端登录，0：不允许，1：允许',
  `account_locked` tinyint(1) DEFAULT '0' COMMENT '0：未锁定，1：是，锁定',
  `account_non_expired` tinyint(1) DEFAULT '1' COMMENT '0：否，过期，1：是，未过期',
  `credentials_non_expired` tinyint(1) DEFAULT '1' COMMENT '0：否，过期，1：是，未过期',
  `create_by` varchar(64) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `device_id` varchar(64) DEFAULT NULL COMMENT 'app端用户的设备号',
  `version` tinyint(8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1093 DEFAULT CHARSET=utf8mb4 COMMENT='用户表';
-- ----------------------------
-- Table structure for gaea_user_role_org
-- ----------------------------
DROP TABLE IF EXISTS `gaea_user_role_org`;
CREATE TABLE `gaea_user_role_org` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) DEFAULT NULL COMMENT '用户名',
  `role_code` varchar(64) DEFAULT NULL COMMENT '角色code',
  `org_code` varchar(64) DEFAULT NULL COMMENT '所属机构code',
  `create_by` varchar(64) CHARACTER SET utf8mb4 DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_by` varchar(64) CHARACTER SET utf8mb4 DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `version` tinyint(8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=utf8;



ALTER TABLE `gaea_menu_extension`
ADD COLUMN `filterable`  int NULL DEFAULT 0 COMMENT '是否可以过滤 0.不可过滤 1.可过滤' AFTER `sortable`;

ALTER TABLE `gaea_menu_user_extension`
ADD COLUMN `filterable`  int NULL DEFAULT 0 COMMENT '是否可以过滤 0.不可过滤 1.可过滤' AFTER `sortable`;

