﻿# 该组件基于 https://github.com/xuxueli/xxl-job 改写

## 1.pom.xml中增加依赖
```xml
		<dependency>
			<groupId>com.anji-plus</groupId>
			<artifactId>spring-boot-starter-gaea-job</artifactId>
			<version>2.9.1-SNAPSHOT</version>
		</dependency>
```

## 2.增加配置
```xml
spring:
  gaea:
    subscribes:
      #执行器，JobHandler所在的服务启用
      executor:
        enabled: true
        #多个逗号相隔
        triggerAddresses: http://10.108.12.22:9097/
        #logHome: ./logs/job-executor/
        #日志保留天数，默认30天
        #logMaxDays: 30
        #执行器注册心路间隔默认30秒
        #beatTimeoutSeconds: 30
      #调度器，只要一个服务启用就行
      trigger:
        enabled: true
```

## 3.编写任务
```java
@Component
public class PushOrderToERPJobHandler implements JobHandler<OrderParam, Order> {

    @Override
    public List<OrderParam> getParamList(JobInfo jobInfo) throws Exception{
        // 查询所有仓库
    }

    @Override
    public Integer getTotalCount(JobInfo jobInfo, OrderParam param) throws Exception{
        // 一个仓库有多少个订单，按JobInfo.jobBatch页大小分页
    }

    @Override
    public List<Entity> getPageList(JobInfo jobInfo, OrderParam param, int pageNum, int pageSize) throws Exception{
        // 获取一个仓库一页数据
    }

    // 必须实现，其它方法不重写时不分页处理
    @Override
    public void execute(JobInfo jobInfo, OrderParam param, List<Entity> pageList) throws Exception{
        // 处理一个仓库一页数据
        // XxlJobHelper.log("----------- -------");
    }

    @Override
    public void afterExecute(JobInfo jobInfo, OrderParam param) throws Exception{
        // 一个仓库处理完后回调
    }
}
```

## 4.在meta_job_info中配置Job的定时信息及策略

## 5.在meta_job_executor中查看所有执行器注册信息，在meta_jog_log中查看任务执行日志
