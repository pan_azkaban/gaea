insert into gaea_menu(menu_code, menu_name, sys_code, parent_code, path,
                      menu_icon,sort,enabled,delete_flag, redirect_url, component,
                      create_by, update_by, create_time, update_time, version)values
('DeviceLogDetail','设备日志明细','PC','SystemSet','deviceLogDetail','',3060,1,	0,'','system-set/deviceLogDetail/index','admin','管理员',now(),now(),1);

insert into gaea_authority(application_name, scan_annotation, auth_code, auth_name, parent_code, path,
                           create_time, update_time, version) values
('gaea-business', 0, 'DeviceLogDetailController#pageList', 'uiI18n查询', 'DeviceLogDetailController', 'GET#/deviceLogDetail/pageList', now(), now(), 1),
('gaea-business', 0, 'DeviceLogDetailController#detail', 'uiI18n详情', 'DeviceLogDetailController', 'GET#/deviceLogDetail/**', now(), now(), 1),
('gaea-business', 0, 'DeviceLogDetailController#update', 'uiI18n修改', 'DeviceLogDetailController', 'PUT#/deviceLogDetail/', now(), now(), 1),
('gaea-business', 0, 'DeviceLogDetailController#insert', 'uiI18n新增', 'DeviceLogDetailController', 'POST#/deviceLogDetail/', now(), now(), 1),
('gaea-business', 0, 'DeviceLogDetailController#deleteById', 'uiI18n删除', 'DeviceLogDetailController', 'DELETE#/deviceLogDetail/**', now(), now(), 1)
;

insert into gaea_menu_authority(org_code, menu_code, auth_code, create_by,
                                update_by, create_time, update_time, version)VALUES
('3000004','DeviceLogDetail','DeviceLogDetailController#pageList','','',now(),now(),1),
('3000004','DeviceLogDetail','DeviceLogDetailController#detail','','',now(),now(),1),
('3000004','DeviceLogDetail','DeviceLogDetailController#update','','',now(),now(),1),
('3000004','DeviceLogDetail','DeviceLogDetailController#insert','','',now(),now(),1),
('3000004','DeviceLogDetail','DeviceLogDetailController#deleteById','','',now(),now(),1);