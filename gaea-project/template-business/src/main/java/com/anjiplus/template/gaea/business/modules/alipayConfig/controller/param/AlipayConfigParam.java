/**/
package com.anjiplus.template.gaea.business.modules.alipayconfig.controller.param;

import lombok.Data;
import java.io.Serializable;
import com.anji.plus.gaea.annotation.Query;
import com.anji.plus.gaea.constant.QueryEnum;
import com.anji.plus.gaea.curd.params.PageParam;

import java.util.List;


/**
* @desc AlipayConfig 支付配置查询输入类
* @author dev-user
* @date 2021-04-07 14:58:28.131
**/
@Data
public class AlipayConfigParam extends PageParam implements Serializable{

    /** 精确查询 */
    @Query
    private String appId;

    /** 精确查询 */
    @Query
    private String charset;

    /** 精确查询 */
    @Query
    private String format;

    /** 模糊查询 */
    @Query(value = QueryEnum.LIKE)
    private String gatewayUrl;

    /** 精确查询 */
    @Query
    private String notifyUrl;
}