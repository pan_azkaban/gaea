
-- ----------------------------
-- Records of gaea_menu
-- ----------------------------
INSERT INTO `gaea_menu` VALUES ('1111', 'Org', '组织机构菜单', 'PC', '', '/organization', 'organization', '10', '1', '0', '/organization/index', '', '0', '0', 'admin', 'admin', '2021-02-05 17:09:49', '2021-03-08 09:00:04', '7');
INSERT INTO `gaea_menu` VALUES ('1113', 'Organization', '组织机构', 'PC', 'Org', 'index', 'organization', '1010', '1', '0', '', 'organization/index', '0', '1', 'admin', 'admin', '2021-02-07 15:07:30', '2021-03-03 14:46:41', '3');
INSERT INTO `gaea_menu` VALUES ('1114', 'Authority', '权限管理', 'PC', '', '/authority', 'authority', '20', '1', '0', '/authority/permission', '', '0', '1', 'admin', 'admin', '2021-02-07 16:31:02', '2021-03-11 13:10:43', '3');
INSERT INTO `gaea_menu` VALUES ('1116', 'MenuConfig', '菜单配置', 'PC', 'Authority', 'menu', '', '2020', '1', '0', '', 'authority/menu/index', '0', '1', 'admin', 'admin', '2021-02-07 16:32:56', '2021-03-04 14:52:50', '5');
INSERT INTO `gaea_menu` VALUES ('1117', 'Role', '角色管理', 'PC', 'Authority', 'role', '', '2040', '1', '0', '', 'authority/role/index', '0', '1', 'admin', 'admin', '2021-02-07 16:33:25', '2021-03-03 14:18:42', '3');
INSERT INTO `gaea_menu` VALUES ('1118', 'User', '用户管理', 'PC', 'Authority', 'user', '', '2050', '1', '0', '', 'authority/user/index', '0', '1', 'admin', 'admin', '2021-02-07 16:34:01', '2021-03-03 14:18:34', '2');
INSERT INTO `gaea_menu` VALUES ('1120', 'Permission', '权限列表', 'PC', 'Authority', 'permission', null, '2010', '1', '0', null, 'authority/permission', '0', '1', 'admin', 'admin', '2021-03-03 14:01:41', '2021-03-03 14:11:48', '2');
INSERT INTO `gaea_menu` VALUES ('1121', 'MenuDetail', '设定动态列', 'PC', 'Authority', 'menu-detail', '', '2030', '1', '0', '', 'authority/menu/menu-detail', '1', '1', 'admin', 'admin', '2021-03-03 14:12:26', '2021-03-13 14:19:26', '4');
INSERT INTO `gaea_menu` VALUES ('1122', 'SystemSet', '系统设置', 'PC', '', '/system-set', 'systemSet', '30', '1', '0', '', '', '0', '1', 'admin', 'admin', '2021-03-03 14:26:50', '2021-03-03 14:31:58', '2');
INSERT INTO `gaea_menu` VALUES ('1123', 'DataDictionary', '字典管理', 'PC', 'SystemSet', 'data-dictionary', '', '3010', '1', '0', '', 'system-set/data-dictionary/index', '0', '1', 'admin', 'admin', '2021-03-03 14:27:59', '2021-03-03 14:31:51', '2');
INSERT INTO `gaea_menu` VALUES ('1124', 'Parameter', '参数管理', 'PC', 'SystemSet', 'parameter', '', '3020', '1', '0', '', 'system-set/parameter/index', '0', '1', 'admin', 'admin', '2021-03-03 14:28:47', '2021-03-03 14:28:47', '1');
INSERT INTO `gaea_menu` VALUES ('1125', 'ParameterEdit', '参数管理编辑', 'PC', 'SystemSet', '/system-set/parameter/edit', '', '3030', '1', '0', '', 'system-set/parameter/component/edit', '1', '1', 'admin', 'admin', '2021-03-03 14:30:56', '2021-03-03 14:31:33', '2');
INSERT INTO `gaea_menu` VALUES ('1126', 'Support', '帮助中心', 'PC', 'SystemSet', 'support', '', '3040', '1', '0', '', 'system-set/support/index', '0', '1', 'admin', 'admin', '2021-03-03 14:32:53', '2021-03-23 10:39:24', '3');
INSERT INTO `gaea_menu` VALUES ('1127', 'OperationLog', '操作日志', 'PC', 'SystemSet', 'operation-log', '', '3050', '1', '0', '', 'system-set/operation-log', '0', '1', 'admin', 'admin', '2021-03-03 14:34:01', '2021-03-03 14:34:01', '1');
INSERT INTO `gaea_menu` VALUES ('1128', 'PushNotify', '消息管理', 'PC', '', '/push-notify', 'pushNotify', '40', '1', '0', '/push-notify/history', '', '0', '1', 'admin', 'admin', '2021-03-03 14:35:40', '2021-03-03 14:35:40', '1');
INSERT INTO `gaea_menu` VALUES ('1129', 'Situation', '收发概况', 'PC', 'PushNotify', 'situation', '', '4010', '1', '0', '', 'push-notify/situation', '0', '1', 'admin', 'admin', '2021-03-03 14:36:14', '2021-03-03 14:36:14', '1');
INSERT INTO `gaea_menu` VALUES ('1130', 'Template', '推送模板', 'PC', 'PushNotify', 'template', '', '4020', '1', '0', '', 'push-notify/template/index', '0', '1', 'admin', 'admin', '2021-03-03 14:36:58', '2021-03-03 14:36:58', '1');
INSERT INTO `gaea_menu` VALUES ('1131', 'History', '推送历史', 'PC', 'PushNotify', 'history', '', '4030', '1', '0', '', 'push-notify/history/index', '0', '1', 'admin', 'admin', '2021-03-03 14:37:53', '2021-03-03 14:37:53', '1');
INSERT INTO `gaea_menu` VALUES ('1132', 'ComponentCenter', '组件中心', 'PC', '', '/component-center', 'componentCenter', '50', '1', '0', '', '/component-center/advanced-list', '0', '1', 'admin', 'admin', '2021-03-03 14:40:04', '2021-03-03 14:40:04', '1');
INSERT INTO `gaea_menu` VALUES ('1133', 'AdvancedList', '高级查询/动态列', 'PC', 'ComponentCenter', 'advanced-list', '', '5010', '1', '0', '', 'component-center/advanced-list', '0', '1', 'admin', 'admin', '2021-03-03 14:41:56', '2021-03-03 14:41:56', '1');
INSERT INTO `gaea_menu` VALUES ('1134', 'Demo', 'CRUD组件化案例', 'PC', 'ComponentCenter', 'demo', '', '5020', '1', '0', '', 'component-center/demo', '0', '1', 'admin', 'admin', '2021-03-03 14:43:09', '2021-03-03 14:43:09', '1');
INSERT INTO `gaea_menu` VALUES ('1135', 'DownloadMenu', '导出中心菜单', 'PC', '', '/download', 'download', '140', '1', '0', '/download/index', '', '0', '0', 'admin', 'admin', '2021-03-03 14:45:51', '2021-03-08 10:29:20', '4');
INSERT INTO `gaea_menu` VALUES ('1136', 'Download', '导出中心', 'CTS-PC', 'DownloadMenu', 'index', null, '6010', '1', '0', '', 'download/index', '0', '1', 'admin', 'admin', '2021-03-03 14:47:26', '2021-03-09 13:14:44', '2');
INSERT INTO `gaea_menu` VALUES ('1139', 'DictItem', '字典项', 'PC', 'SystemSet', 'dict-item', '', '3060', '1', '0', '', 'system-set/data-dictionary/dict-item', '1', '1', 'admin', 'admin', '2021-03-10 16:43:55', '2021-03-11 10:24:39', '2');
INSERT INTO `gaea_menu` VALUES ('1140', 'Rules', '规则引擎', 'PC', 'SystemSet', 'rules', '', '1000', '1', '0', '', 'system-set/rules/index', '0', '1', 'admin', 'admin', '2021-03-12 16:15:03', '2021-03-12 16:16:50', '4');
INSERT INTO `gaea_menu` VALUES ('1143', 'List', 'mock数据(开发者使用)', 'PC', 'ComponentCenter', 'list', '', '5030', '1', '0', '', 'list/index', '0', '1', 'admin', 'admin', '2021-03-15 13:44:19', '2021-03-19 10:39:36', '5');
INSERT INTO `gaea_menu` VALUES ('1144', 'RulesDetail', '规则引擎编辑', 'PC', 'SystemSet', '/system-set/rules/edit', '', '1000', '1', '0', '', 'system-set/rules/edit', '1', '1', 'admin', 'admin', '2021-03-12 16:15:03', '2021-03-17 10:56:17', '5');
INSERT INTO `gaea_menu` VALUES ('1145', 'Flowable', '工作流管理', 'PC', '', '/flowable', 'componentCenter', '90', '1', '0', '', '', '0', '1', 'admin', 'admin', '2021-03-17 16:21:46', '2021-03-17 17:21:21', '3');
INSERT INTO `gaea_menu` VALUES ('1146', 'ProcessModel', '流程模板', 'PC', 'Flowable', '/process-model', '', '91', '1', '0', '', 'flowable/process-model/index', '0', '1', 'admin', 'admin', '2021-03-17 16:22:22', '2021-03-17 17:21:10', '2');
INSERT INTO `gaea_menu` VALUES ('1147', 'ProcessDef', '流程定义', 'PC', 'Flowable', '/process', '', '92', '1', '0', '', 'flowable/process/index', '0', '1', 'admin', 'admin', '2021-03-17 16:22:22', '2021-03-17 17:21:10', '2');
INSERT INTO `gaea_menu` VALUES ('1149', 'ProcessRunning', '我的待办', 'PC', 'Flowable', '/process/running', '', '92', '1', '0', '', 'flowable/running/index', '0', '1', 'admin', 'admin', '2021-03-17 16:22:22', '2021-03-17 17:21:10', '2');
INSERT INTO `gaea_menu` VALUES ('1150', 'ProcessHistory', '我的已办', 'PC', 'Flowable', '/process/history', '', '92', '1', '0', '', 'flowable/history/index', '0', '1', 'admin', 'admin', '2021-03-17 16:22:22', '2021-03-17 17:21:10', '2');
INSERT INTO `gaea_menu` VALUES ('1153', 'ProcessForm', '表单模板', 'PC', 'Flowable', '/process/form', '', '90', '1', '0', '', 'flowable/form/index', '0', '1', 'admin', 'admin', '2021-03-17 16:22:22', '2021-03-17 17:21:10', '2');
INSERT INTO `gaea_menu` VALUES ('1154', 'ReportManager', '报表管理', 'PC', '', '/reportManager', 'report', '110', '1', '0', '', '', '0', '1', 'admin', 'admin', '2021-03-17 16:21:46', '2021-03-17 17:21:21', '3');
INSERT INTO `gaea_menu` VALUES ('1155', 'Datasource', '数据源', 'PC', 'ReportManager', '/datasource', '', '11010', '1', '0', '', 'report/datasource/index', '0', '1', 'admin', 'admin', '2021-03-17 16:22:22', '2021-03-17 17:21:10', '2');
INSERT INTO `gaea_menu` VALUES ('1156', 'Resultset', '数据集', 'PC', 'ReportManager', '/resultset', '', '11020', '1', '0', '', 'report/resultset/index', '0', '1', 'admin', 'admin', '2021-03-17 16:22:22', '2021-03-17 17:21:10', '2');
INSERT INTO `gaea_menu` VALUES ('1157', 'Report', '报表管理', 'PC', 'ReportManager', '/report', '', '11030', '1', '0', '', 'report/report/index', '0', '1', 'admin', 'admin', '2021-03-17 16:22:22', '2021-03-17 17:21:10', '2');
INSERT INTO `gaea_menu` VALUES ('1158', 'Bigscreen', '大屏报表', 'PC', 'ReportManager', '/bigscreen', '', '11040', '1', '0', '', 'report/bigscreen/index', '0', '1', 'admin', 'admin', '2021-03-17 16:22:22', '2021-03-17 17:21:10', '2');
INSERT INTO `gaea_menu` VALUES ('1159', 'Excelreport', '表格报表', 'PC', 'ReportManager', '/excelreport', '', '11050', '1', '0', '', 'report/excelreport/index', '0', '1', 'admin', 'admin', '2021-03-17 16:22:22', '2021-03-17 17:21:10', '2');
INSERT INTO `gaea_menu` VALUES ('1165', 'GaeaUiI18n', 'UI国际化设置', 'PC', 'SystemSet', 'gaeaUiI18n/index', '', '3070', '1', '0', '', 'system-set/gaeaUiI18n/index', '0', '1', 'admin', 'admin', '2021-03-25 14:16:26', '2021-03-30 10:35:58', '9');
INSERT INTO `gaea_menu` VALUES ('1169', 'INF', '接口中心', 'PC', '', '', 'componentCenter', '130', '1', '0', '', '', '0', '1', 'admin', 'admin', '2021-03-26 15:55:42', '2021-04-16 16:13:34', '3');
INSERT INTO `gaea_menu` VALUES ('1170', 'ParameterAdd', '参数管理新增', 'PC', 'SystemSet', '/system-set/parameter/add', '', '3031', '1', '0', '', 'system-set/parameter/component/edit', '1', '1', 'admin', 'admin', '2021-03-19 16:35:00', '2021-03-19 16:35:31', '2');
INSERT INTO `gaea_menu` VALUES ('1171', 'RulesAdd', '引擎规则新增', 'PC', 'SystemSet', '/system-set/rules/add', '', '1000', '1', '0', '', 'system-set/rules/edit', '1', '1', 'admin', 'admin', '2021-03-22 13:14:49', '2021-03-22 13:15:08', '2');
INSERT INTO `gaea_menu` VALUES ('1172', 'INF_MYAPP', '我的应用', 'PC', 'INF', '/system-set/my-application', '', '0', '1', '0', '', 'system-set/my-application/index', '0', '1', 'admin', 'admin', '2021-03-26 16:00:17', '2021-03-26 16:00:17', '1');
INSERT INTO `gaea_menu` VALUES ('1173', 'INF_MGT', '接口列表', 'PC', 'INF', '/system-set/interface', '', '0', '1', '0', '', 'system-set/interface/index', '0', '1', 'admin', 'admin', '2021-03-26 16:01:03', '2021-04-06 17:45:19', '6');
INSERT INTO `gaea_menu` VALUES ('1175', 'FlowableTaskHandler', '任务办理', 'PC', 'Flowable', '/flowable/running/handler', '', '1000', '1', '0', '', 'flowable/running/handler', '1', '1', 'admin', 'admin', '2021-03-22 13:14:49', '2021-03-22 13:15:08', '1');
INSERT INTO `gaea_menu` VALUES ('1180', 'interfaceEdit', '接口编辑', 'PC', 'INF', '/system-set/interface/edit', '', '0', '1', '0', '', 'system-set/interface/interfaceEdit/index', '1', '1', 'admin', 'admin', '2021-03-31 10:11:39', '2021-03-31 11:59:40', '3');
INSERT INTO `gaea_menu` VALUES ('1181', 'FlowableHisDetail', '工作流历史详情', 'PC', 'Flowable', '/flowable/history/detail', '', '1001', '1', '0', '', 'flowable/history/detail', '1', '1', 'admin', 'admin', '2021-03-22 13:14:49', '2021-03-22 13:15:08', '1');
INSERT INTO `gaea_menu` VALUES ('1187', 'Generator', '代码生成', 'PC', '', '/generator', 'clipboard', '120', '1', '0', '', '', '0', '1', 'admin', 'admin', '2021-04-16 13:23:27', '2021-04-16 13:23:27', '1');
INSERT INTO `gaea_menu` VALUES ('1188', 'Project', '项目列表', 'PC', 'Generator', 'project', '', '121', '1', '0', '', 'generator/project', '0', '1', 'admin', 'admin', '2021-04-16 13:28:40', '2021-04-16 13:31:03', '3');
INSERT INTO `gaea_menu` VALUES ('1189', 'Index', '项目数据源', 'PC', 'Generator', 'index', '', '122', '1', '0', '', 'generator/index', '1', '1', 'admin', 'admin', '2021-04-16 14:46:57', '2021-04-16 15:06:50', '3');
INSERT INTO `gaea_menu` VALUES ('1190', 'Config', '生成配置', 'PC', 'Generator', 'config', '', '123', '1', '0', '', 'generator/config', '1', '1', 'admin', 'admin', '2021-04-16 15:07:31', '2021-04-16 15:12:38', '2');
INSERT INTO `gaea_menu` VALUES ('1191', 'Init', '项目初始化', 'PC', 'Generator', 'init', '', '124', '1', '0', '', 'generator/init', '1', '1', 'admin', 'admin', '2021-04-16 15:18:26', '2021-04-16 15:18:26', '1');
INSERT INTO `gaea_menu` VALUES ('1192', 'Preview', '代码预览', 'PC', 'Generator', 'preview', '', '125', '1', '0', '', 'generator/preview', '1', '1', 'admin', 'admin', '2021-04-16 15:19:07', '2021-04-16 15:19:07', '1');
INSERT INTO `gaea_menu` VALUES ('1193', 'AlipayConfig', '生成示例-单表', 'PC', 'Generator', 'alipayConfig', '', '126', '1', '0', '', 'alipayConfig/index', '0', '1', 'admin', 'admin', '2021-04-16 15:19:58', '2021-04-16 15:19:58', '1');
INSERT INTO `gaea_menu` VALUES ('1194', 'AlipayConfigDetail', '生成示例-单表详情', 'PC', 'Generator', 'alipayConfigDetail', '', '127', '1', '0', '', 'alipayConfig/component/detail', '1', '1', 'admin', 'admin', '2021-04-16 15:20:45', '2021-04-16 15:20:45', '1');
INSERT INTO `gaea_menu` VALUES ('1195', 'DeviceInfo', '设备信息-主表', 'PC', 'Generator', 'deviceInfo', '', '128', '1', '0', '', 'deviceInfo/index', '0', '1', 'admin', 'admin', '2021-04-16 15:21:34', '2021-04-16 15:21:34', '1');
INSERT INTO `gaea_menu` VALUES ('1196', 'DeviceModel', '设备类型-子表', 'PC', 'Generator', 'deviceModel', '', '129', '1', '0', '', 'deviceModel/index', '0', '1', 'admin', 'admin', '2021-04-16 15:22:06', '2021-04-16 15:22:06', '1');
INSERT INTO `gaea_menu` VALUES ('1197', 'DeviceLogDetail', '设备日志-子表', 'PC', 'Generator', 'deviceLogDetail', '', '130', '1', '0', '', 'deviceLogDetail/index', '0', '1', 'admin', 'admin', '2021-04-16 15:22:56', '2021-04-16 15:22:56', '1');
INSERT INTO `gaea_menu` VALUES ('1198', 'DeviceInfoDetail', '设备信息-详情', 'PC', 'Generator', 'deviceInfoDetail', '', '0', '1', '0', '', 'deviceInfo/component/detail', '1', '1', 'admin', 'admin', '2021-04-16 15:23:46', '2021-04-16 15:23:46', '1');
INSERT INTO `gaea_menu` VALUES ('1199', 'DeviceModelDetail', '设备类型-详情', 'PC', 'Generator', 'deviceModelDetail', '', '131', '1', '0', '', 'deviceModel/component/detail', '1', '1', 'admin', 'admin', '2021-04-16 15:24:24', '2021-04-16 15:24:24', '1');
INSERT INTO `gaea_menu` VALUES ('1200', 'DeviceLogDetailInfo', '设备日志-详情', 'PC', 'Generator', 'deviceLogDetailInfo', '', '0', '1', '0', '', 'deviceLogDetail/component/detail', '1', '1', 'admin', 'admin', '2021-04-16 15:24:57', '2021-04-16 15:24:57', '1');
INSERT INTO `gaea_menu` VALUES ('1201', 'Announcement', '系统通告', 'PC', 'SystemSet', 'announcement', '', '3091', '1', '0', '', 'announcement/index', '0', '1', 'admin', 'admin', '2021-04-16 15:35:37', '2021-04-16 15:35:37', '1');
INSERT INTO `gaea_menu` VALUES ('1202', 'MyMassage', '我的消息', 'PC', 'SystemSet', 'my-massage', '', '3092', '1', '0', '', 'my-massage/index', '1', '1', 'admin', 'admin', '2021-04-16 15:45:05', '2021-04-16 15:45:05', '1');
INSERT INTO `gaea_menu` VALUES ('1203', 'MyMassageDetails', '我的消息详情', 'PC', 'Announcement', 'my-massageDetails', '', '3093', '1', '0', '', 'my-massage/component/index', '1', '1', 'admin', 'admin', '2021-04-16 15:45:51', '2021-04-16 15:45:51', '1');
INSERT INTO `gaea_menu` VALUES ('1204', 'BigscreenDesigner', '大屏报表设计器', 'PC', 'Bigscreen', 'report/bigscreen/designer', '', '11060', '1', '0', '', 'report/bigscreen/designer/index', '1', '1', 'admin', 'admin', '2021-04-16 16:00:33', '2021-04-16 16:24:30', '4');
INSERT INTO `gaea_menu` VALUES ('1205', 'BigscreenViewer', '大屏报表查看器', 'PC', 'Bigscreen', 'report/bigscreen/viewer', '', '11062', '1', '0', '', 'report/bigscreen/viewer/index', '1', '1', 'admin', 'admin', '2021-04-16 16:02:37', '2021-04-16 16:24:42', '5');
INSERT INTO `gaea_menu` VALUES ('1206', 'ExcelreportDesigner', '表格报表设计器', 'PC', 'Excelreport', 'report/excelreport/designer', '', '0', '1', '0', '', 'report/excelreport/designer/index', '1', '1', 'admin', 'admin', '2021-04-16 16:07:29', '2021-04-16 16:24:20', '2');
INSERT INTO `gaea_menu` VALUES ('1207', 'ExcelreportViewer', '表格报表查看器', 'PC', 'Excelreport', 'report/excelreport/viewer', '', '0', '1', '0', '', 'report/excelreport/viewer/index', '1', '1', 'admin', 'admin', '2021-04-16 16:08:10', '2021-04-16 16:23:59', '2');
INSERT INTO `gaea_menu` VALUES ('1208', 'API', 'API文档', 'PC', 'INF', '/open/view/api', '', '0', '1', '0', '', 'apiList/main', '0', '1', 'admin', 'admin', '2021-04-16 16:13:02', '2021-04-16 16:13:02', '1');
INSERT INTO `gaea_menu` VALUES ('1209', 'OpenApiList', 'API列表', 'PC', 'API', 'index/:id', '', '1', '1', '0', '', 'apiList/list', '1', '1', 'admin', 'admin', '2021-04-16 16:14:34', '2021-04-16 16:15:27', '2');
INSERT INTO `gaea_menu` VALUES ('1210', 'OpenApiDetail', '接口明细', 'PC', 'API', 'detail/:id', '', '1', '1', '0', '', 'apiList/detail', '1', '1', 'admin', 'admin', '2021-04-16 16:19:49', '2021-04-16 16:19:49', '1');
INSERT INTO `gaea_menu` VALUES ('1211', 'OpenApiAccess', '服务调用方', 'PC', 'API', 'access', '', '1', '1', '0', '', 'apiList/portal/access', '1', '1', 'admin', 'admin', '2021-04-16 16:20:14', '2021-04-16 16:20:14', '1');
INSERT INTO `gaea_menu` VALUES ('1212', 'OpenApiGuide', '服务提供方', 'PC', 'API', 'guide', '', '0', '1', '0', '', 'apiList/portal/guide', '1', '1', 'admin', 'admin', '2021-04-16 16:20:43', '2021-04-16 16:20:43', '1');
INSERT INTO `gaea_menu` VALUES ('1213', 'OpenApiParam', '接口参数', 'PC', 'API', 'param', '', '1', '1', '0', '', 'apiList/portal/param', '1', '1', 'admin', 'admin', '2021-04-16 16:21:16', '2021-04-16 16:21:16', '1');
INSERT INTO `gaea_menu` VALUES ('1214', 'OpenApiSign', '安全方式', 'PC', 'API', 'sign', '', '1', '1', '0', '', 'apiList/portal/sign', '1', '1', 'admin', 'admin', '2021-04-16 16:21:44', '2021-04-16 16:21:44', '1');
INSERT INTO `gaea_menu` VALUES ('1215', 'OpenApiCode', '响应码查询', 'PC', 'API', 'code', '', '1', '1', '0', '', 'apiList/portal/code', '1', '1', 'admin', 'admin', '2021-04-16 16:22:16', '2021-04-16 16:22:16', '1');
INSERT INTO `gaea_menu` VALUES ('1216', 'SvgDemo', '图标库', 'PC', 'ComponentCenter', 'gallery', '', '0', '1', '0', '', 'component-center/gallery', '0', '1', 'admin', 'admin', '2021-04-16 16:26:39', '2021-04-16 16:53:23', '4');

-- ----------------------------
-- Records of gaea_menu_authority
-- ----------------------------
INSERT INTO `gaea_menu_authority` VALUES ('1', null, 'Permission', 'gaeaAuthorityController#roleAuthority', 'admin', 'admin', '2021-03-03 16:56:06', '2021-03-03 16:56:06', '1');
INSERT INTO `gaea_menu_authority` VALUES ('2', null, 'Permission', 'gaeaAuthorityController#update', 'admin', 'admin', '2021-03-03 16:56:06', '2021-03-03 16:56:06', '1');
INSERT INTO `gaea_menu_authority` VALUES ('167', '3000004', 'MenuConfig', 'gaeaMenuController#userInfo', 'admin', 'admin', '2021-03-05 17:26:36', '2021-03-05 17:26:36', '1');
INSERT INTO `gaea_menu_authority` VALUES ('168', '3000004', 'MenuConfig', 'gaeaMenuController#getTree', 'admin', 'admin', '2021-03-05 17:26:36', '2021-03-05 17:26:36', '1');
INSERT INTO `gaea_menu_authority` VALUES ('169', '3000004', 'MenuConfig', 'gaeaMenuController#getMenuInfoByOrg', 'admin', 'admin', '2021-03-05 17:26:36', '2021-03-05 17:26:36', '1');
INSERT INTO `gaea_menu_authority` VALUES ('170', '3000004', 'MenuConfig', 'gaeaMenuController#menuAuthority', 'admin', 'admin', '2021-03-05 17:26:36', '2021-03-05 17:26:36', '1');
INSERT INTO `gaea_menu_authority` VALUES ('171', '3000004', 'MenuConfig', 'gaeaMenuController#update', 'admin', 'admin', '2021-03-05 17:26:36', '2021-03-05 17:26:36', '1');
INSERT INTO `gaea_menu_authority` VALUES ('172', '3000004', 'MenuConfig', 'gaeaMenuController#insert', 'admin', 'admin', '2021-03-05 17:26:36', '2021-03-05 17:26:36', '1');
INSERT INTO `gaea_menu_authority` VALUES ('173', '3000004', 'MenuConfig', 'gaeaMenuController#detail', 'admin', 'admin', '2021-03-05 17:26:36', '2021-03-05 17:26:36', '1');
INSERT INTO `gaea_menu_authority` VALUES ('174', '3000004', 'MenuConfig', 'gaeaMenuController#deleteBatchIds', 'admin', 'admin', '2021-03-05 17:26:36', '2021-03-05 17:26:36', '1');
INSERT INTO `gaea_menu_authority` VALUES ('175', '3000004', 'MenuConfig', 'gaeaMenuController#deleteById', 'admin', 'admin', '2021-03-05 17:26:36', '2021-03-05 17:26:36', '1');
INSERT INTO `gaea_menu_authority` VALUES ('176', '3000004', 'MenuConfig', 'gaeaMenuController#pageList', 'admin', 'admin', '2021-03-05 17:26:36', '2021-03-05 17:26:36', '1');
INSERT INTO `gaea_menu_authority` VALUES ('177', '3000004', 'MenuConfig', 'gaeaMenuController#authorityTree', 'admin', 'admin', '2021-03-05 17:26:36', '2021-03-05 17:26:36', '1');
INSERT INTO `gaea_menu_authority` VALUES ('178', '3000004', 'Role', 'gaeaRoleController#update', 'admin', 'admin', '2021-03-05 17:26:48', '2021-03-05 17:26:48', '1');
INSERT INTO `gaea_menu_authority` VALUES ('179', '3000004', 'Role', 'gaeaRoleController#insert', 'admin', 'admin', '2021-03-05 17:26:48', '2021-03-05 17:26:48', '1');
INSERT INTO `gaea_menu_authority` VALUES ('180', '3000004', 'Role', 'gaeaRoleController#detail', 'admin', 'admin', '2021-03-05 17:26:48', '2021-03-05 17:26:48', '1');
INSERT INTO `gaea_menu_authority` VALUES ('181', '3000004', 'Role', 'gaeaRoleController#deleteBatchIds', 'admin', 'admin', '2021-03-05 17:26:48', '2021-03-05 17:26:48', '1');
INSERT INTO `gaea_menu_authority` VALUES ('182', '3000004', 'Role', 'gaeaRoleController#deleteById', 'admin', 'admin', '2021-03-05 17:26:48', '2021-03-05 17:26:48', '1');
INSERT INTO `gaea_menu_authority` VALUES ('183', '3000004', 'Role', 'gaeaRoleController#pageList', 'admin', 'admin', '2021-03-05 17:26:48', '2021-03-05 17:26:48', '1');
INSERT INTO `gaea_menu_authority` VALUES ('184', '3000004', 'Role', 'gaeaRoleController#roleAuthorities', 'admin', 'admin', '2021-03-05 17:26:48', '2021-03-05 17:26:48', '1');
INSERT INTO `gaea_menu_authority` VALUES ('185', '3000004', 'Role', 'gaeaRoleController#saveOrgTreeForRole', 'admin', 'admin', '2021-03-05 17:26:48', '2021-03-05 17:26:48', '1');
INSERT INTO `gaea_menu_authority` VALUES ('186', '3000004', 'Role', 'gaeaRoleController#queryOrgTreeForRole', 'admin', 'admin', '2021-03-05 17:26:48', '2021-03-05 17:26:48', '1');
INSERT INTO `gaea_menu_authority` VALUES ('187', '3000004', 'Role', 'gaeaRoleController#saveMenuActionTreeForRole', 'admin', 'admin', '2021-03-05 17:26:48', '2021-03-05 17:26:48', '1');
INSERT INTO `gaea_menu_authority` VALUES ('214', '3000004', 'Support', 'gaeaHelpController#listAll', 'admin', 'admin', '2021-03-05 17:36:14', '2021-03-05 17:36:14', '1');
INSERT INTO `gaea_menu_authority` VALUES ('215', '3000004', 'Support', 'gaeaHelpController#demo', 'admin', 'admin', '2021-03-05 17:36:14', '2021-03-05 17:36:14', '1');
INSERT INTO `gaea_menu_authority` VALUES ('216', '3000004', 'Support', 'gaeaHelpController#update', 'admin', 'admin', '2021-03-05 17:36:14', '2021-03-05 17:36:14', '1');
INSERT INTO `gaea_menu_authority` VALUES ('217', '3000004', 'Support', 'gaeaHelpController#insert', 'admin', 'admin', '2021-03-05 17:36:14', '2021-03-05 17:36:14', '1');
INSERT INTO `gaea_menu_authority` VALUES ('218', '3000004', 'Support', 'gaeaHelpController#detail', 'admin', 'admin', '2021-03-05 17:36:14', '2021-03-05 17:36:14', '1');
INSERT INTO `gaea_menu_authority` VALUES ('219', '3000004', 'Support', 'gaeaHelpController#deleteBatchIds', 'admin', 'admin', '2021-03-05 17:36:14', '2021-03-05 17:36:14', '1');
INSERT INTO `gaea_menu_authority` VALUES ('220', '3000004', 'Support', 'gaeaHelpController#deleteById', 'admin', 'admin', '2021-03-05 17:36:14', '2021-03-05 17:36:14', '1');
INSERT INTO `gaea_menu_authority` VALUES ('221', '3000004', 'Support', 'gaeaHelpController#pageList', 'admin', 'admin', '2021-03-05 17:36:14', '2021-03-05 17:36:14', '1');
INSERT INTO `gaea_menu_authority` VALUES ('243', '3000004', 'History', 'gaeaPushHistoryController#getPushStatistics', 'admin', 'admin', '2021-03-05 17:37:59', '2021-03-05 17:37:59', '1');
INSERT INTO `gaea_menu_authority` VALUES ('244', '3000004', 'History', 'gaeaPushHistoryController#update', 'admin', 'admin', '2021-03-05 17:37:59', '2021-03-05 17:37:59', '1');
INSERT INTO `gaea_menu_authority` VALUES ('245', '3000004', 'History', 'gaeaPushHistoryController#insert', 'admin', 'admin', '2021-03-05 17:37:59', '2021-03-05 17:37:59', '1');
INSERT INTO `gaea_menu_authority` VALUES ('246', '3000004', 'History', 'gaeaPushHistoryController#detail', 'admin', 'admin', '2021-03-05 17:37:59', '2021-03-05 17:37:59', '1');
INSERT INTO `gaea_menu_authority` VALUES ('247', '3000004', 'History', 'gaeaPushHistoryController#deleteBatchIds', 'admin', 'admin', '2021-03-05 17:37:59', '2021-03-05 17:37:59', '1');
INSERT INTO `gaea_menu_authority` VALUES ('248', '3000004', 'History', 'gaeaPushHistoryController#deleteById', 'admin', 'admin', '2021-03-05 17:37:59', '2021-03-05 17:37:59', '1');
INSERT INTO `gaea_menu_authority` VALUES ('249', '3000004', 'History', 'gaeaPushHistoryController#pageList', 'admin', 'admin', '2021-03-05 17:37:59', '2021-03-05 17:37:59', '1');
INSERT INTO `gaea_menu_authority` VALUES ('250', '3000004', 'Template', 'gaeaPushTemplateController#testSendPush', 'admin', 'admin', '2021-03-05 17:38:15', '2021-03-05 17:38:15', '1');
INSERT INTO `gaea_menu_authority` VALUES ('251', '3000004', 'Template', 'gaeaPushTemplateController#preview', 'admin', 'admin', '2021-03-05 17:38:15', '2021-03-05 17:38:15', '1');
INSERT INTO `gaea_menu_authority` VALUES ('252', '3000004', 'Template', 'gaeaPushTemplateController#update', 'admin', 'admin', '2021-03-05 17:38:15', '2021-03-05 17:38:15', '1');
INSERT INTO `gaea_menu_authority` VALUES ('253', '3000004', 'Template', 'gaeaPushTemplateController#insert', 'admin', 'admin', '2021-03-05 17:38:15', '2021-03-05 17:38:15', '1');
INSERT INTO `gaea_menu_authority` VALUES ('254', '3000004', 'Template', 'gaeaPushTemplateController#detail', 'admin', 'admin', '2021-03-05 17:38:15', '2021-03-05 17:38:15', '1');
INSERT INTO `gaea_menu_authority` VALUES ('255', '3000004', 'Template', 'gaeaPushTemplateController#deleteBatchIds', 'admin', 'admin', '2021-03-05 17:38:15', '2021-03-05 17:38:15', '1');
INSERT INTO `gaea_menu_authority` VALUES ('256', '3000004', 'Template', 'gaeaPushTemplateController#deleteById', 'admin', 'admin', '2021-03-05 17:38:15', '2021-03-05 17:38:15', '1');
INSERT INTO `gaea_menu_authority` VALUES ('257', '3000004', 'Template', 'gaeaPushTemplateController#pageList', 'admin', 'admin', '2021-03-05 17:38:15', '2021-03-05 17:38:15', '1');
INSERT INTO `gaea_menu_authority` VALUES ('258', '3000004', 'Download', 'gaeaExportController#export', 'admin', 'admin', '2021-03-05 17:38:41', '2021-03-05 17:38:41', '1');
INSERT INTO `gaea_menu_authority` VALUES ('259', '3000004', 'Download', 'gaeaExportController#queryExportInfo', 'admin', 'admin', '2021-03-05 17:38:41', '2021-03-05 17:38:41', '1');
INSERT INTO `gaea_menu_authority` VALUES ('260', '3000004', 'Download', 'gaeaExportController#update', 'admin', 'admin', '2021-03-05 17:38:41', '2021-03-05 17:38:41', '1');
INSERT INTO `gaea_menu_authority` VALUES ('261', '3000004', 'Download', 'gaeaExportController#insert', 'admin', 'admin', '2021-03-05 17:38:41', '2021-03-05 17:38:41', '1');
INSERT INTO `gaea_menu_authority` VALUES ('262', '3000004', 'Download', 'gaeaExportController#detail', 'admin', 'admin', '2021-03-05 17:38:41', '2021-03-05 17:38:41', '1');
INSERT INTO `gaea_menu_authority` VALUES ('263', '3000004', 'Download', 'gaeaExportController#deleteBatchIds', 'admin', 'admin', '2021-03-05 17:38:41', '2021-03-05 17:38:41', '1');
INSERT INTO `gaea_menu_authority` VALUES ('264', '3000004', 'Download', 'gaeaExportController#deleteById', 'admin', 'admin', '2021-03-05 17:38:41', '2021-03-05 17:38:41', '1');
INSERT INTO `gaea_menu_authority` VALUES ('265', '3000004', 'Download', 'gaeaExportController#pageList', 'admin', 'admin', '2021-03-05 17:38:41', '2021-03-05 17:38:41', '1');
INSERT INTO `gaea_menu_authority` VALUES ('488', '3000004', 'MenuDetail', 'gaeaMenuExtensionController#queryMenuExtension', 'admin', 'admin', '2021-03-13 14:24:38', '2021-03-13 14:24:38', '1');
INSERT INTO `gaea_menu_authority` VALUES ('489', '3000004', 'DataDictionary', 'gaeaDictController#refresh', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', '1');
INSERT INTO `gaea_menu_authority` VALUES ('490', '3000004', 'DataDictionary', 'gaeaDictController#select', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', '1');
INSERT INTO `gaea_menu_authority` VALUES ('491', '3000004', 'DataDictionary', 'gaeaDictController#detail', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', '1');
INSERT INTO `gaea_menu_authority` VALUES ('492', '3000004', 'DataDictionary', 'gaeaDictController#pageList', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', '1');
INSERT INTO `gaea_menu_authority` VALUES ('493', '3000004', 'DataDictionary', 'gaeaDictController#deleteBatchIds', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', '1');
INSERT INTO `gaea_menu_authority` VALUES ('494', '3000004', 'DataDictionary', 'gaeaDictController#deleteById', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', '1');
INSERT INTO `gaea_menu_authority` VALUES ('495', '3000004', 'DataDictionary', 'gaeaDictController#update', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', '1');
INSERT INTO `gaea_menu_authority` VALUES ('496', '3000004', 'DataDictionary', 'gaeaDictController#insert', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', '1');
INSERT INTO `gaea_menu_authority` VALUES ('497', '3000004', 'DataDictionary', 'gaeaDictItemController#detail', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', '1');
INSERT INTO `gaea_menu_authority` VALUES ('498', '3000004', 'DataDictionary', 'gaeaDictItemController#pageList', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', '1');
INSERT INTO `gaea_menu_authority` VALUES ('499', '3000004', 'DataDictionary', 'gaeaDictItemController#deleteBatchIds', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', '1');
INSERT INTO `gaea_menu_authority` VALUES ('500', '3000004', 'DataDictionary', 'gaeaDictItemController#deleteById', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', '1');
INSERT INTO `gaea_menu_authority` VALUES ('501', '3000004', 'DataDictionary', 'gaeaDictItemController#update', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', '1');
INSERT INTO `gaea_menu_authority` VALUES ('502', '3000004', 'DataDictionary', 'gaeaDictItemController#insert', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', '1');
INSERT INTO `gaea_menu_authority` VALUES ('507', '3000004', 'OperationLog', 'gaeaLogController#update', 'admin', 'admin', '2021-03-15 15:54:59', '2021-03-15 15:54:59', '1');
INSERT INTO `gaea_menu_authority` VALUES ('508', '3000004', 'OperationLog', 'gaeaLogController#insert', 'admin', 'admin', '2021-03-15 15:54:59', '2021-03-15 15:54:59', '1');
INSERT INTO `gaea_menu_authority` VALUES ('509', '3000004', 'OperationLog', 'gaeaLogController#detail', 'admin', 'admin', '2021-03-15 15:54:59', '2021-03-15 15:54:59', '1');
INSERT INTO `gaea_menu_authority` VALUES ('510', '3000004', 'OperationLog', 'gaeaLogController#deleteBatchIds', 'admin', 'admin', '2021-03-15 15:54:59', '2021-03-15 15:54:59', '1');
INSERT INTO `gaea_menu_authority` VALUES ('511', '3000004', 'OperationLog', 'gaeaLogController#deleteById', 'admin', 'admin', '2021-03-15 15:54:59', '2021-03-15 15:54:59', '1');
INSERT INTO `gaea_menu_authority` VALUES ('512', '3000004', 'OperationLog', 'gaeaLogController#pageList', 'admin', 'admin', '2021-03-15 15:54:59', '2021-03-15 15:54:59', '1');
INSERT INTO `gaea_menu_authority` VALUES ('513', '3000004', 'OperationLog', 'gaeaLogController#exportLogToFile', 'admin', 'admin', '2021-03-15 15:54:59', '2021-03-15 15:54:59', '1');
INSERT INTO `gaea_menu_authority` VALUES ('514', '3000004', 'Rules', 'gaeaRulesController#executeRules', 'admin', 'admin', '2021-03-16 12:33:57', '2021-03-16 12:33:57', '1');
INSERT INTO `gaea_menu_authority` VALUES ('515', '3000004', 'Rules', 'gaeaRulesController#update', 'admin', 'admin', '2021-03-16 12:33:57', '2021-03-16 12:33:57', '1');
INSERT INTO `gaea_menu_authority` VALUES ('516', '3000004', 'Rules', 'gaeaRulesController#insert', 'admin', 'admin', '2021-03-16 12:33:57', '2021-03-16 12:33:57', '1');
INSERT INTO `gaea_menu_authority` VALUES ('517', '3000004', 'Rules', 'gaeaRulesController#detail', 'admin', 'admin', '2021-03-16 12:33:57', '2021-03-16 12:33:57', '1');
INSERT INTO `gaea_menu_authority` VALUES ('518', '3000004', 'Rules', 'gaeaRulesController#pageList', 'admin', 'admin', '2021-03-16 12:33:57', '2021-03-16 12:33:57', '1');
INSERT INTO `gaea_menu_authority` VALUES ('519', '3000004', 'Rules', 'gaeaRulesController#deleteById', 'admin', 'admin', '2021-03-16 12:33:57', '2021-03-16 12:33:57', '1');
INSERT INTO `gaea_menu_authority` VALUES ('520', '3000004', 'Rules', 'gaeaRulesController#deleteBatchIds', 'admin', 'admin', '2021-03-16 12:33:57', '2021-03-16 12:33:57', '1');
INSERT INTO `gaea_menu_authority` VALUES ('521', '3000004', 'Authority', 'gaeaAuthorityController#authorityTree', 'admin', 'admin', '2021-03-16 13:21:43', '2021-03-16 13:21:43', '1');
INSERT INTO `gaea_menu_authority` VALUES ('522', '3000004', 'Authority', 'gaeaAuthorityController#roleAuthority', 'admin', 'admin', '2021-03-16 13:21:43', '2021-03-16 13:21:43', '1');
INSERT INTO `gaea_menu_authority` VALUES ('523', '3000004', 'Authority', 'gaeaAuthorityController#update', 'admin', 'admin', '2021-03-16 13:21:43', '2021-03-16 13:21:43', '1');
INSERT INTO `gaea_menu_authority` VALUES ('545', '3000004', 'Parameter', 'gaeaSettingController#update', 'zouya', 'zouya', '2021-03-18 13:50:07', '2021-03-18 13:50:07', '1');
INSERT INTO `gaea_menu_authority` VALUES ('546', '3000004', 'Parameter', 'gaeaSettingController#insert', 'zouya', 'zouya', '2021-03-18 13:50:07', '2021-03-18 13:50:07', '1');
INSERT INTO `gaea_menu_authority` VALUES ('547', '3000004', 'Parameter', 'gaeaSettingController#detail', 'zouya', 'zouya', '2021-03-18 13:50:07', '2021-03-18 13:50:07', '1');
INSERT INTO `gaea_menu_authority` VALUES ('548', '3000004', 'Parameter', 'gaeaSettingController#deleteBatchIds', 'zouya', 'zouya', '2021-03-18 13:50:07', '2021-03-18 13:50:07', '1');
INSERT INTO `gaea_menu_authority` VALUES ('549', '3000004', 'Parameter', 'gaeaSettingController#deleteById', 'zouya', 'zouya', '2021-03-18 13:50:07', '2021-03-18 13:50:07', '1');
INSERT INTO `gaea_menu_authority` VALUES ('550', '3000004', 'Parameter', 'gaeaSettingController#pageList', 'zouya', 'zouya', '2021-03-18 13:50:07', '2021-03-18 13:50:07', '1');
INSERT INTO `gaea_menu_authority` VALUES ('551', '3000004', 'Organization', 'gaeaOrgController#queryAllOrg', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', '1');
INSERT INTO `gaea_menu_authority` VALUES ('552', '3000004', 'Organization', 'gaeaOrgController#orgRoleTree', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', '1');
INSERT INTO `gaea_menu_authority` VALUES ('553', '3000004', 'Organization', 'gaeaOrgController#orgSelect', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', '1');
INSERT INTO `gaea_menu_authority` VALUES ('554', '3000004', 'Organization', 'gaeaOrgController#orgTree', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', '1');
INSERT INTO `gaea_menu_authority` VALUES ('555', '3000004', 'Organization', 'gaeaOrgController#saveOrg', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', '1');
INSERT INTO `gaea_menu_authority` VALUES ('556', '3000004', 'Organization', 'gaeaOrgController#updateOrg', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', '1');
INSERT INTO `gaea_menu_authority` VALUES ('557', '3000004', 'Organization', 'gaeaOrgController#orgRoleTreeSelected', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', '1');
INSERT INTO `gaea_menu_authority` VALUES ('558', '3000004', 'Organization', 'gaeaOrgController#update', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', '1');
INSERT INTO `gaea_menu_authority` VALUES ('559', '3000004', 'Organization', 'gaeaOrgController#insert', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', '1');
INSERT INTO `gaea_menu_authority` VALUES ('560', '3000004', 'Organization', 'gaeaOrgController#detail', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', '1');
INSERT INTO `gaea_menu_authority` VALUES ('561', '3000004', 'Organization', 'gaeaOrgController#deleteBatchIds', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', '1');
INSERT INTO `gaea_menu_authority` VALUES ('562', '3000004', 'Organization', 'gaeaOrgController#deleteById', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', '1');
INSERT INTO `gaea_menu_authority` VALUES ('563', '3000004', 'Organization', 'gaeaOrgController#pageList', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', '1');
INSERT INTO `gaea_menu_authority` VALUES ('580', '3000004', 'GaeaUiI18n', 'gaeaUiI18nController#detail', 'admin', 'admin', '2021-04-02 10:02:56', '2021-04-02 10:02:56', '1');
INSERT INTO `gaea_menu_authority` VALUES ('581', '3000004', 'GaeaUiI18n', 'gaeaUiI18nController#update', 'admin', 'admin', '2021-04-02 10:02:56', '2021-04-02 10:02:56', '1');
INSERT INTO `gaea_menu_authority` VALUES ('582', '3000004', 'GaeaUiI18n', 'gaeaUiI18nController#insert', 'admin', 'admin', '2021-04-02 10:02:56', '2021-04-02 10:02:56', '1');
INSERT INTO `gaea_menu_authority` VALUES ('583', '3000004', 'GaeaUiI18n', 'gaeaUiI18nController#pageList', 'admin', 'admin', '2021-04-02 10:02:56', '2021-04-02 10:02:56', '1');
INSERT INTO `gaea_menu_authority` VALUES ('584', '3000004', 'GaeaUiI18n', 'gaeaUiI18nController#deleteById', 'admin', 'admin', '2021-04-02 10:02:56', '2021-04-02 10:02:56', '1');
INSERT INTO `gaea_menu_authority` VALUES ('585', '3000004', 'GaeaUiI18n', 'gaeaUiI18nController#deleteBatchIds', 'admin', 'admin', '2021-04-02 10:02:56', '2021-04-02 10:02:56', '1');
INSERT INTO `gaea_menu_authority` VALUES ('586', '3000004', 'GaeaUiI18n', 'gaeaUiI18nController#scan', 'admin', 'admin', '2021-04-02 10:02:56', '2021-04-02 10:02:56', '1');
INSERT INTO `gaea_menu_authority` VALUES ('587', '3000004', 'GaeaUiI18n', 'gaeaUiI18nController#getI18nFields', 'admin', 'admin', '2021-04-02 10:02:56', '2021-04-02 10:02:56', '1');
INSERT INTO `gaea_menu_authority` VALUES ('588', '3000004', 'GaeaUiI18n', 'gaeaUiI18nController#getTables', 'admin', 'admin', '2021-04-02 10:02:56', '2021-04-02 10:02:56', '1');
INSERT INTO `gaea_menu_authority` VALUES ('589', '3000004', 'User', 'gaeaUserController#queryRoleTree', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', '1');
INSERT INTO `gaea_menu_authority` VALUES ('590', '3000004', 'User', 'gaeaUserController#resetPassword', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', '1');
INSERT INTO `gaea_menu_authority` VALUES ('591', '3000004', 'User', 'gaeaUserController#saveRoleTree', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', '1');
INSERT INTO `gaea_menu_authority` VALUES ('592', '3000004', 'User', 'gaeaUserController#refreshCache', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', '1');
INSERT INTO `gaea_menu_authority` VALUES ('593', '3000004', 'User', 'gaeaUserController#updatePassword', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', '1');
INSERT INTO `gaea_menu_authority` VALUES ('594', '3000004', 'User', 'gaeaUserController#update', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', '1');
INSERT INTO `gaea_menu_authority` VALUES ('595', '3000004', 'User', 'gaeaUserController#insert', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', '1');
INSERT INTO `gaea_menu_authority` VALUES ('596', '3000004', 'User', 'gaeaUserController#detail', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', '1');
INSERT INTO `gaea_menu_authority` VALUES ('597', '3000004', 'User', 'gaeaUserController#deleteBatchIds', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', '1');
INSERT INTO `gaea_menu_authority` VALUES ('598', '3000004', 'User', 'gaeaUserController#deleteById', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', '1');
INSERT INTO `gaea_menu_authority` VALUES ('599', '3000004', 'User', 'gaeaUserController#pageList', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', '1');
INSERT INTO `gaea_menu_authority` VALUES ('600', '3000004', 'User', 'gaeaUserController#unLock', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', '1');



-- ----------------------------
-- Records of gaea_menu_extension
-- ----------------------------
INSERT INTO `gaea_menu_extension` VALUES ('11', 'advanceTable', 'AdvancedList', '用户名', 'username', '0', '0', '0', '1', '150', 'admin', '2021-03-15 17:56:34', 'pyn', '2021-03-15 17:56:34', '0', '', '1', '', '3');
INSERT INTO `gaea_menu_extension` VALUES ('12', 'advanceTable', 'AdvancedList', '用户真实姓名', 'nickname', '0', '0', '0', '1', '150', 'admin', '2021-03-15 17:56:34', 'pyn', '2021-03-15 17:56:34', '0', '', '1', '', '2');
INSERT INTO `gaea_menu_extension` VALUES ('13', 'advanceTable', 'AdvancedList', '用户手机号', 'phone', '0', '0', '0', '1', '200', 'admin', '2021-03-15 17:56:34', 'pyn', '2021-03-15 17:56:34', '0', '', '1', '', '2');
INSERT INTO `gaea_menu_extension` VALUES ('14', 'advanceTable', 'AdvancedList', '账号状态', 'enable', '0', '0', '0', '1', '200', 'admin', '2021-04-13 11:13:51', 'admin', '2021-04-13 11:13:51', '0', '', '1', '', '3');
INSERT INTO `gaea_menu_extension` VALUES ('15', 'advanceTable', 'AdvancedList', '用户邮箱', 'email', '0', '0', '0', '1', '100', 'admin', '2021-03-15 17:56:34', 'pyn', '2021-03-15 17:56:34', '0', '', '1', '', '2');
INSERT INTO `gaea_menu_extension` VALUES ('16', 'advanceTable', 'AdvancedList', '创建时间', 'createTime', '1', '0', '0', '1', '200', 'admin', '2021-03-15 17:56:34', 'pyn', '2021-03-15 17:56:34', '0', 'gu.create_time', '1', '', '4');
INSERT INTO `gaea_menu_extension` VALUES ('17', 'advanceTable', 'AdvancedList', '创建人', 'createBy', '0', '0', '0', '1', '100', 'admin', '2021-03-15 17:56:34', 'pyn', '2021-03-15 17:56:34', '0', '', '1', '', '2');
INSERT INTO `gaea_menu_extension` VALUES ('18', 'advanceTable', 'AdvancedList', '修改时间', 'updateTime', '1', '0', '0', '1', '200', 'admin', '2021-03-15 17:56:34', 'pyn', '2021-03-15 17:56:34', '0', 'gu.update_time', '1', '', '4');
INSERT INTO `gaea_menu_extension` VALUES ('19', 'advanceTable', 'AdvancedList', '修改人', 'updateBy', '0', '0', '0', '1', '100', 'admin', '2021-03-15 17:56:34', 'pyn', '2021-03-15 17:56:34', '0', '', '1', '', '2');
INSERT INTO `gaea_menu_extension` VALUES ('20', 'advanceTable', 'AdvancedList', '所属机构', 'orgNames', '0', '0', '0', '1', '200', 'pyn', '2021-03-15 17:56:34', 'pyn', '2021-03-15 17:56:34', '0', '', '1', '', '1');
INSERT INTO `gaea_menu_extension` VALUES ('21', 'advanceTable', 'AdvancedList', '所属角色', 'roleNames', '0', '0', '0', '1', '150', 'pyn', '2021-03-15 17:56:34', 'pyn', '2021-03-15 17:56:34', '0', '', '1', '', '1');


-- ----------------------------
-- Records of gaea_query_condition
-- ----------------------------
INSERT INTO `gaea_query_condition` VALUES ('15', 'advanceTable', 'AdvancedList', 'gu.username', '用户登录名', '1', '1', '1', '1', '2', '', '0', 'admin', '2021-03-15 17:57:38', 'pyn', '2021-03-15 17:57:38', '', '3');
INSERT INTO `gaea_query_condition` VALUES ('16', 'advanceTable', 'AdvancedList', 'date_format(gu.create_time,\'%Y-%m-%d\')', '创建时间', '1', '1', '3', '1', '2', '', '0', 'admin', '2021-03-15 17:57:38', 'pyn', '2021-03-15 17:57:38', 'yyyy-MM-dd', '2');
INSERT INTO `gaea_query_condition` VALUES ('17', 'advanceTable', 'AdvancedList', 'gu.enabled', '账号状态', '1', '1', '2', '1', '1', '/business/gaeaDict/select/ENABLE_FLAG', '0', 'admin', '2021-03-15 17:57:38', 'pyn', '2021-03-15 17:57:38', null, '2');
INSERT INTO `gaea_query_condition` VALUES ('18', 'advanceTable', 'AdvancedList', 'org.org_code', '所属机构', '1', '1', '2', '1', '1', '/auth/org/orgSelect', '0', 'pyn', '2021-03-15 17:57:38', 'pyn', '2021-03-15 17:57:38', null, '1');
INSERT INTO `gaea_query_condition` VALUES ('19', 'advanceTable', 'AdvancedList', 'gu.phone', '用户手机号', '1', '1', '1', '1', '2', '', '0', 'pyn', '2021-03-15 17:57:38', 'pyn', '2021-03-15 17:57:38', '', '1');


-- ----------------------------
-- Records of gaea_common_condition
-- ----------------------------
INSERT INTO `gaea_common_condition` VALUES ('23', 'AdvancedList', '[{\"dynamicQueryOperatorType\":\"NE\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"org.org_code\",\"operator\":\"NE\",\"value\":\"3000002\",\"valueType\":1}]', '[\"武汉事业部1\"]', '0', 'admin', '2021-03-22 09:56:41', 'admin', '2021-03-22 09:56:41', 'advanceTable', '机构非武汉事业部1', '1');
INSERT INTO `gaea_common_condition` VALUES ('24', 'AdvancedList', '[{\"dynamicQueryOperatorType\":\"EQ\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"org.org_code\",\"operator\":\"EQ\",\"value\":\"2000002\",\"valueType\":1}]', '[\"武汉分公司\"]', '0', 'admin', '2021-03-22 09:57:05', 'admin', '2021-03-22 09:57:05', 'advanceTable', '机构为武汉事业部', '1');
INSERT INTO `gaea_common_condition` VALUES ('29', 'AdvancedList', '[{\"datePrecision\":\"\",\"dynamicQueryOperatorType\":\"IN\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"gu.username\",\"operator\":\"IN\",\"value\":\"aimee\",\"valueType\":1}]', '[\"aimee\"]', '0', 'admin', '2021-03-22 10:13:28', 'admin', '2021-03-22 10:13:28', 'advanceTable', '登录名', '1');
INSERT INTO `gaea_common_condition` VALUES ('30', 'AdvancedList', '[{\"datePrecision\":\"\",\"dynamicQueryOperatorType\":\"LIKE\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"gu.username\",\"operator\":\"LIKE\",\"value\":\"aimee\",\"valueType\":1}]', '[\"aimee\"]', '0', 'admin', '2021-03-22 10:14:44', 'admin', '2021-03-22 10:14:44', 'advanceTable', '登录名类似', '1');
INSERT INTO `gaea_common_condition` VALUES ('31', 'AdvancedList', '[{\"datePrecision\":\"\",\"dynamicQueryOperatorType\":\"LIKE\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"gu.phone\",\"operator\":\"LIKE\",\"value\":\"13816966003\",\"valueType\":1}]', '[\"13816966003\"]', '0', 'admin', '2021-03-22 10:15:48', 'admin', '2021-03-22 10:15:48', 'advanceTable', '手机号', '1');
INSERT INTO `gaea_common_condition` VALUES ('33', 'AdvancedList', '[{\"datePrecision\":\"\",\"dynamicQueryOperatorType\":\"LIKE\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"gu.phone\",\"operator\":\"LIKE\",\"value\":\"13816966003\",\"valueType\":1},{\"datePrecision\":\"\",\"dynamicQueryOperatorType\":\"IN\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"gu.username\",\"operator\":\"IN\",\"value\":\"aimee\",\"valueType\":1}]', '[\"13816966003\",\"aimee\"]', '0', 'admin', '2021-03-22 10:18:24', 'admin', '2021-03-22 10:18:24', 'advanceTable', '登录名和手机号', '1');
INSERT INTO `gaea_common_condition` VALUES ('36', 'AdvancedList', '[{\"datePrecision\":\"\",\"dynamicQueryOperatorType\":\"EQ\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"gu.username\",\"operator\":\"EQ\",\"value\":\"pyn\",\"valueType\":1}]', '[\"pyn\"]', '0', 'admin', '2021-03-22 13:08:24', 'admin', '2021-03-22 13:08:24', 'advanceTable', 'pyn', '1');
INSERT INTO `gaea_common_condition` VALUES ('37', 'AdvancedList', '[{\"datePrecision\":\"\",\"dynamicQueryOperatorType\":\"EQ\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"gu.username\",\"operator\":\"EQ\",\"value\":\"pyn\",\"valueType\":1}]', '[\"pyn\"]', '0', 'pyn', '2021-03-22 13:09:24', 'pyn', '2021-03-22 13:09:24', 'advanceTable', 'pyn', '1');
INSERT INTO `gaea_common_condition` VALUES ('38', 'AdvancedList', '[{\"datePrecision\":\"\",\"dynamicQueryOperatorType\":\"EQ\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"gu.username\",\"operator\":\"EQ\",\"value\":\"admin\",\"valueType\":1}]', '[\"admin\"]', '0', 'pyn', '2021-03-22 13:10:55', 'pyn', '2021-03-22 13:10:55', 'advanceTable', 'admin', '1');


-- ----------------------------
-- Records of gaea_org
-- ----------------------------
INSERT INTO `gaea_org` VALUES ('1', '1000001', '总公司', null, '无', null, null, '1', '1', null, '', null, '1', '0', '', 'admin', '2020-11-25 15:01:16', 'admin', '2020-11-25 15:01:16', null);
INSERT INTO `gaea_org` VALUES ('2', '2000001', '上海分公司', '1000001', '总公司', null, null, '2', '2', null, null, null, '1', '0', null, 'admin', '2020-11-25 15:01:16', 'admin', '2020-11-25 15:01:16', null);
INSERT INTO `gaea_org` VALUES ('3', '2000002', '武汉分公司', '1000001', '总公司', null, null, '2', '2', null, null, null, '1', '0', null, 'admin', '2020-11-25 15:01:16', 'admin', '2020-11-25 15:01:16', null);
INSERT INTO `gaea_org` VALUES ('4', '2000003', '北京分公司', '1000001', '总公司', null, null, '2', '2', null, null, null, '1', '0', null, 'admin', '2020-11-25 15:01:16', 'admin', '2020-11-25 15:01:16', null);
INSERT INTO `gaea_org` VALUES ('101', '3000001', '上海事业部', '2000001', '上海分公司', null, null, '3', '3', null, null, null, '1', '1', null, 'admin', '2020-11-25 15:01:16', 'admin', '2020-11-25 15:01:16', null);
INSERT INTO `gaea_org` VALUES ('102', '3000002', '武汉事业部', '2000002', '武汉分公司', null, null, '3', '3', null, null, null, '1', '1', null, 'admin', '2020-11-25 15:01:16', 'admin', '2020-11-25 15:01:16', null);
INSERT INTO `gaea_org` VALUES ('103', '3000003', '北京事业部', '2000003', '北京分公司', null, null, '3', '3', null, null, null, '1', '1', null, 'admin', '2020-11-25 15:01:16', 'admin', '2020-11-25 15:01:16', null);
INSERT INTO `gaea_org` VALUES ('104', '3000004', '上海IT部', '2000001', '上海分公司', null, null, '3', '3', null, null, null, '1', '0', null, 'admin', '2020-11-25 15:01:16', 'admin', '2020-11-25 15:01:16', null);

-- ----------------------------
-- Records of gaea_role
-- ----------------------------
INSERT INTO `gaea_role` VALUES ('1', '3000004', 'admin', '管理员', '1', '管理员1111', '管理员', '2020-09-15 12:30:48', 'admin', '2021-03-08 17:43:00', '6');
INSERT INTO `gaea_role` VALUES ('25', null, null, null, '1', null, 'admin', '2021-03-16 15:58:40', 'admin', '2021-03-16 15:58:40', '1');
INSERT INTO `gaea_role` VALUES ('26', null, null, null, '1', null, 'admin', '2021-03-16 16:12:59', 'admin', '2021-03-16 16:12:59', '1');



INSERT INTO `gaea_role_menu_authority` VALUES ('12555', '3000004', 'admin', 'Organization', 'gaeaOrgController#queryAllOrg', 'GET#/org/queryAllOrg', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12556', '3000004', 'admin', 'Organization', 'gaeaOrgController#orgTree', 'GET#/org/tree', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12557', '3000004', 'admin', 'Organization', 'gaeaOrgController#saveOrg', 'POST#/org/saveOrg', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12558', '3000004', 'admin', 'Organization', 'gaeaOrgController#updateOrg', 'POST#/org/updateOrg', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12559', '3000004', 'admin', 'Organization', 'gaeaOrgController#orgRoleTreeSelected', 'GET#/org/role/tree', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12560', '3000004', 'admin', 'Organization', 'gaeaOrgController#update', 'PUT#/org', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12561', '3000004', 'admin', 'Organization', 'gaeaOrgController#insert', 'POST#/org', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12562', '3000004', 'admin', 'Organization', 'gaeaOrgController#detail', 'GET#/org/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12563', '3000004', 'admin', 'Organization', 'gaeaOrgController#deleteBatchIds', 'POST#/org/delete/batch', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12564', '3000004', 'admin', 'Organization', 'gaeaOrgController#deleteById', 'DELETE#/org/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12565', '3000004', 'admin', 'Organization', 'gaeaOrgController#pageList', 'GET#/org/pageList', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12566', '3000004', 'admin', 'Authority', 'gaeaAuthorityController#authorityTree', 'GET#/gaeaAuthority/authority/tree/**/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12567', '3000004', 'admin', 'Authority', 'gaeaAuthorityController#roleAuthority', 'POST#/gaeaAuthority/role/authority', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12568', '3000004', 'admin', 'Authority', 'gaeaAuthorityController#update', 'PUT#/gaeaAuthority', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12569', '3000004', 'admin', 'Permission', 'gaeaAuthorityController#roleAuthority', 'POST#/gaeaAuthority/role/authority', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12570', '3000004', 'admin', 'Permission', 'gaeaAuthorityController#update', 'PUT#/gaeaAuthority', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12571', '3000004', 'admin', 'MenuConfig', 'gaeaMenuController#userInfo', 'GET#/menu/userInfo', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12572', '3000004', 'admin', 'MenuConfig', 'gaeaMenuController#getTree', 'GET#/menu/tree', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12573', '3000004', 'admin', 'MenuConfig', 'gaeaMenuController#getMenuInfoByOrg', 'POST#/menu/menuUserInfoByOrg', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12574', '3000004', 'admin', 'MenuConfig', 'gaeaMenuController#menuAuthority', 'POST#/menu/mapper/authorities', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12575', '3000004', 'admin', 'MenuConfig', 'gaeaMenuController#update', 'PUT#/menu', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12576', '3000004', 'admin', 'MenuConfig', 'gaeaMenuController#insert', 'POST#/menu', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12577', '3000004', 'admin', 'MenuConfig', 'gaeaMenuController#detail', 'GET#/menu/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12578', '3000004', 'admin', 'MenuConfig', 'gaeaMenuController#deleteBatchIds', 'POST#/menu/delete/batch', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12579', '3000004', 'admin', 'MenuConfig', 'gaeaMenuController#deleteById', 'DELETE#/menu/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12580', '3000004', 'admin', 'MenuConfig', 'gaeaMenuController#pageList', 'GET#/menu/pageList', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12581', '3000004', 'admin', 'MenuConfig', 'gaeaMenuController#authorityTree', 'GET#/menu/authority/tree/**/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12582', '3000004', 'admin', 'MenuDetail', 'gaeaMenuExtensionController#queryMenuExtension', 'GET#/menuextension/queryMenuExtension/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12583', '3000004', 'admin', 'Role', 'gaeaRoleController#update', 'PUT#/role', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12584', '3000004', 'admin', 'Role', 'gaeaRoleController#insert', 'POST#/role', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12585', '3000004', 'admin', 'Role', 'gaeaRoleController#detail', 'GET#/role/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12586', '3000004', 'admin', 'Role', 'gaeaRoleController#deleteBatchIds', 'POST#/role/delete/batch', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12587', '3000004', 'admin', 'Role', 'gaeaRoleController#deleteById', 'DELETE#/role/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12588', '3000004', 'admin', 'Role', 'gaeaRoleController#pageList', 'GET#/role/pageList', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12589', '3000004', 'admin', 'Role', 'gaeaRoleController#roleAuthorities', 'GET#/role/tree/authorities/**/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12590', '3000004', 'admin', 'Role', 'gaeaRoleController#saveOrgTreeForRole', 'POST#/role/mapping/org', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12591', '3000004', 'admin', 'Role', 'gaeaRoleController#queryOrgTreeForRole', 'GET#/role/org/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12592', '3000004', 'admin', 'Role', 'gaeaRoleController#saveMenuActionTreeForRole', 'POST#/role/roleMenuAuthorities', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12593', '3000004', 'admin', 'User', 'gaeaUserController#queryRoleTree', 'GET#/user/queryRoleTree/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12594', '3000004', 'admin', 'User', 'gaeaUserController#resetPassword', 'POST#/user/resetPwd', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12595', '3000004', 'admin', 'User', 'gaeaUserController#saveRoleTree', 'POST#/user/saveRoleTree', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12596', '3000004', 'admin', 'User', 'gaeaUserController#refreshCache', 'POST#/user/refresh/username', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12597', '3000004', 'admin', 'User', 'gaeaUserController#updatePassword', 'POST#/user/updatePassword', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12598', '3000004', 'admin', 'User', 'gaeaUserController#update', 'PUT#/user', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12599', '3000004', 'admin', 'User', 'gaeaUserController#insert', 'POST#/user', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12600', '3000004', 'admin', 'User', 'gaeaUserController#detail', 'GET#/user/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12601', '3000004', 'admin', 'User', 'gaeaUserController#deleteBatchIds', 'POST#/user/delete/batch', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12602', '3000004', 'admin', 'User', 'gaeaUserController#deleteById', 'DELETE#/user/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12603', '3000004', 'admin', 'User', 'gaeaUserController#pageList', 'GET#/user/pageList', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12604', '3000004', 'admin', 'User', 'gaeaUserController#unLock', 'POST#/user/unLock', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12605', '3000004', 'admin', 'Rules', 'gaeaRulesController#executeRules', 'POST#/gaeaRules/executeRules', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12606', '3000004', 'admin', 'Rules', 'gaeaRulesController#update', 'PUT#/gaeaRules', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12607', '3000004', 'admin', 'Rules', 'gaeaRulesController#insert', 'POST#/gaeaRules', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12608', '3000004', 'admin', 'Rules', 'gaeaRulesController#detail', 'GET#/gaeaRules/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12609', '3000004', 'admin', 'Rules', 'gaeaRulesController#pageList', 'GET#/gaeaRules/pageList', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12610', '3000004', 'admin', 'Rules', 'gaeaRulesController#deleteById', 'DELETE#/gaeaRules/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12611', '3000004', 'admin', 'Rules', 'gaeaRulesController#deleteBatchIds', 'POST#/gaeaRules/delete/batch', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12612', '3000004', 'admin', 'RulesDetail', 'RulesDetail', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12613', '3000004', 'admin', 'RulesAdd', 'RulesAdd', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12614', '3000004', 'admin', 'DataDictionary', 'gaeaDictController#select', 'GET#/gaeaDict/select/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12615', '3000004', 'admin', 'DataDictionary', 'gaeaDictController#detail', 'GET#/gaeaDict/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12616', '3000004', 'admin', 'DataDictionary', 'gaeaDictController#pageList', 'GET#/gaeaDict/pageList', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12617', '3000004', 'admin', 'DataDictionary', 'gaeaDictController#deleteBatchIds', 'POST#/gaeaDict/delete/batch', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12618', '3000004', 'admin', 'DataDictionary', 'gaeaDictController#deleteById', 'DELETE#/gaeaDict/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12619', '3000004', 'admin', 'DataDictionary', 'gaeaDictController#update', 'PUT#/gaeaDict', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12620', '3000004', 'admin', 'DataDictionary', 'gaeaDictController#insert', 'POST#/gaeaDict', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12621', '3000004', 'admin', 'DataDictionary', 'gaeaDictItemController#detail', 'GET#/gaeaDictItem/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12622', '3000004', 'admin', 'DataDictionary', 'gaeaDictItemController#pageList', 'GET#/gaeaDictItem/pageList', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12623', '3000004', 'admin', 'DataDictionary', 'gaeaDictItemController#deleteBatchIds', 'POST#/gaeaDictItem/delete/batch', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12624', '3000004', 'admin', 'DataDictionary', 'gaeaDictItemController#deleteById', 'DELETE#/gaeaDictItem/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12625', '3000004', 'admin', 'DataDictionary', 'gaeaDictItemController#update', 'PUT#/gaeaDictItem', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12626', '3000004', 'admin', 'DataDictionary', 'gaeaDictItemController#insert', 'POST#/gaeaDictItem', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12627', '3000004', 'admin', 'Parameter', 'gaeaSettingController#update', 'PUT#/setting', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12628', '3000004', 'admin', 'Parameter', 'gaeaSettingController#insert', 'POST#/setting', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12629', '3000004', 'admin', 'Parameter', 'gaeaSettingController#detail', 'GET#/setting/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12630', '3000004', 'admin', 'Parameter', 'gaeaSettingController#deleteBatchIds', 'POST#/setting/delete/batch', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12631', '3000004', 'admin', 'Parameter', 'gaeaSettingController#deleteById', 'DELETE#/setting/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12632', '3000004', 'admin', 'Parameter', 'gaeaSettingController#pageList', 'GET#/setting/pageList', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12633', '3000004', 'admin', 'ParameterEdit', 'ParameterEdit', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12634', '3000004', 'admin', 'ParameterAdd', 'ParameterAdd', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12635', '3000004', 'admin', 'Support', 'gaeaHelpController#listAll', 'GET#/gaeaHelp/list', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12636', '3000004', 'admin', 'Support', 'gaeaHelpController#demo', 'GET#/gaeaHelp/demo', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12637', '3000004', 'admin', 'Support', 'gaeaHelpController#update', 'PUT#/gaeaHelp', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12638', '3000004', 'admin', 'Support', 'gaeaHelpController#insert', 'POST#/gaeaHelp', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12639', '3000004', 'admin', 'Support', 'gaeaHelpController#detail', 'GET#/gaeaHelp/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12640', '3000004', 'admin', 'Support', 'gaeaHelpController#deleteBatchIds', 'POST#/gaeaHelp/delete/batch', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12641', '3000004', 'admin', 'Support', 'gaeaHelpController#deleteById', 'DELETE#/gaeaHelp/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12642', '3000004', 'admin', 'Support', 'gaeaHelpController#pageList', 'GET#/gaeaHelp/pageList', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12643', '3000004', 'admin', 'OperationLog', 'gaeaLogController#update', 'PUT#/log', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12644', '3000004', 'admin', 'OperationLog', 'gaeaLogController#insert', 'POST#/log', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12645', '3000004', 'admin', 'OperationLog', 'gaeaLogController#detail', 'GET#/log/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12646', '3000004', 'admin', 'OperationLog', 'gaeaLogController#deleteBatchIds', 'POST#/log/delete/batch', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12647', '3000004', 'admin', 'OperationLog', 'gaeaLogController#deleteById', 'DELETE#/log/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12648', '3000004', 'admin', 'OperationLog', 'gaeaLogController#pageList', 'GET#/log/pageList', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12649', '3000004', 'admin', 'OperationLog', 'gaeaLogController#exportLogToFile', 'POST#/log/exportLogToFile', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12650', '3000004', 'admin', 'DictItem', 'DictItem', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12651', '3000004', 'admin', 'GaeaUiI18n', 'gaeaUiI18nController#detail', 'GET#/gaeaUiI18n/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12652', '3000004', 'admin', 'GaeaUiI18n', 'gaeaUiI18nController#update', 'PUT#/gaeaUiI18n', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12653', '3000004', 'admin', 'GaeaUiI18n', 'gaeaUiI18nController#insert', 'POST#/gaeaUiI18n', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12654', '3000004', 'admin', 'GaeaUiI18n', 'gaeaUiI18nController#pageList', 'GET#/gaeaUiI18n/pageList', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12655', '3000004', 'admin', 'GaeaUiI18n', 'gaeaUiI18nController#deleteById', 'DELETE#/gaeaUiI18n/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12656', '3000004', 'admin', 'GaeaUiI18n', 'gaeaUiI18nController#deleteBatchIds', 'POST#/gaeaUiI18n/delete/batch', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12657', '3000004', 'admin', 'Announcement', 'Announcement', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12658', '3000004', 'admin', 'MyMassageDetails', 'MyMassageDetails', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12659', '3000004', 'admin', 'Situation', 'Situation', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12660', '3000004', 'admin', 'Template', 'gaeaPushTemplateController#testSendPush', 'POST#/gaeaPushTemplate/testSendPush', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12661', '3000004', 'admin', 'Template', 'gaeaPushTemplateController#preview', 'POST#/gaeaPushTemplate/preview', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12662', '3000004', 'admin', 'Template', 'gaeaPushTemplateController#update', 'PUT#/gaeaPushTemplate', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12663', '3000004', 'admin', 'Template', 'gaeaPushTemplateController#insert', 'POST#/gaeaPushTemplate', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12664', '3000004', 'admin', 'Template', 'gaeaPushTemplateController#detail', 'GET#/gaeaPushTemplate/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12665', '3000004', 'admin', 'Template', 'gaeaPushTemplateController#deleteBatchIds', 'POST#/gaeaPushTemplate/delete/batch', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12666', '3000004', 'admin', 'Template', 'gaeaPushTemplateController#deleteById', 'DELETE#/gaeaPushTemplate/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12667', '3000004', 'admin', 'Template', 'gaeaPushTemplateController#pageList', 'GET#/gaeaPushTemplate/pageList', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12668', '3000004', 'admin', 'History', 'gaeaPushHistoryController#getPushStatistics', 'POST#/gaeaPushHistory/getPushStatistics', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12669', '3000004', 'admin', 'History', 'gaeaPushHistoryController#update', 'PUT#/gaeaPushHistory', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12670', '3000004', 'admin', 'History', 'gaeaPushHistoryController#insert', 'POST#/gaeaPushHistory', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12671', '3000004', 'admin', 'History', 'gaeaPushHistoryController#detail', 'GET#/gaeaPushHistory/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12672', '3000004', 'admin', 'History', 'gaeaPushHistoryController#deleteBatchIds', 'POST#/gaeaPushHistory/delete/batch', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12673', '3000004', 'admin', 'History', 'gaeaPushHistoryController#deleteById', 'DELETE#/gaeaPushHistory/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12674', '3000004', 'admin', 'History', 'gaeaPushHistoryController#pageList', 'GET#/gaeaPushHistory/pageList', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12675', '3000004', 'admin', 'SvgDemo', 'SvgDemo', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12676', '3000004', 'admin', 'AdvancedList', 'AdvancedList', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12677', '3000004', 'admin', 'Demo', 'Demo', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12678', '3000004', 'admin', 'ProcessForm', 'ProcessForm', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12679', '3000004', 'admin', 'ProcessModel', 'ProcessModel', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12680', '3000004', 'admin', 'ProcessDef', 'ProcessDef', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12681', '3000004', 'admin', 'ProcessRunning', 'ProcessRunning', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12682', '3000004', 'admin', 'ProcessHistory', 'ProcessHistory', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12683', '3000004', 'admin', 'FlowableTaskHandler', 'FlowableTaskHandler', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12684', '3000004', 'admin', 'FlowableHisDetail', 'FlowableHisDetail', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12685', '3000004', 'admin', 'Datasource', 'Datasource', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12686', '3000004', 'admin', 'Resultset', 'Resultset', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12687', '3000004', 'admin', 'Report', 'Report', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12688', '3000004', 'admin', 'BigscreenDesigner', 'BigscreenDesigner', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12689', '3000004', 'admin', 'BigscreenViewer', 'BigscreenViewer', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12690', '3000004', 'admin', 'ExcelreportDesigner', 'ExcelreportDesigner', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12691', '3000004', 'admin', 'ExcelreportViewer', 'ExcelreportViewer', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12692', '3000004', 'admin', 'DeviceInfoDetail', 'DeviceInfoDetail', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12693', '3000004', 'admin', 'DeviceLogDetailInfo', 'DeviceLogDetailInfo', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12695', '3000004', 'admin', 'Index', 'Index', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12696', '3000004', 'admin', 'Config', 'Config', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12697', '3000004', 'admin', 'Init', 'Init', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12698', '3000004', 'admin', 'Preview', 'Preview', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12699', '3000004', 'admin', 'AlipayConfig', 'AlipayConfig', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12700', '3000004', 'admin', 'AlipayConfigDetail', 'AlipayConfigDetail', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12701', '3000004', 'admin', 'DeviceInfo', 'DeviceInfo', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12702', '3000004', 'admin', 'DeviceModel', 'DeviceModel', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12703', '3000004', 'admin', 'DeviceLogDetail', 'DeviceLogDetail', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12704', '3000004', 'admin', 'DeviceModelDetail', 'DeviceModelDetail', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12705', '3000004', 'admin', 'INF_MYAPP', 'INF_MYAPP', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12706', '3000004', 'admin', 'INF_MGT', 'INF_MGT', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12707', '3000004', 'admin', 'interfaceEdit', 'interfaceEdit', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12708', '3000004', 'admin', 'OpenApiGuide', 'OpenApiGuide', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12709', '3000004', 'admin', 'OpenApiList', 'OpenApiList', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12710', '3000004', 'admin', 'OpenApiDetail', 'OpenApiDetail', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12711', '3000004', 'admin', 'OpenApiAccess', 'OpenApiAccess', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12712', '3000004', 'admin', 'OpenApiParam', 'OpenApiParam', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12713', '3000004', 'admin', 'OpenApiSign', 'OpenApiSign', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12714', '3000004', 'admin', 'OpenApiCode', 'OpenApiCode', null, 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12715', '3000004', 'admin', 'Download', 'gaeaExportController#export', 'POST#/export/saveExportLog', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12716', '3000004', 'admin', 'Download', 'gaeaExportController#detail', 'GET#/export/**', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');
INSERT INTO `gaea_role_menu_authority` VALUES ('12717', '3000004', 'admin', 'Download', 'gaeaExportController#pageList', 'GET#/export/pageList', 'admin', '2021-04-16 17:13:22', 'admin', '2021-04-16 17:13:22', '1');

-- ----------------------------
-- Records of gaea_role_org
-- ----------------------------
INSERT INTO `gaea_role_org` VALUES ('1238', 'admin', '3000004', 'admin', '2021-02-07 16:36:30', 'admin', '2021-02-07 16:36:30', '1');

-- ----------------------------
-- Records of gaea_user
-- ----------------------------
INSERT INTO `gaea_user` VALUES ('1', 'admin', '$2a$10$XW4/DayBqZ6oFJ4FgoASduWOcC6FKgL3iK0rWh8QyVPljz4TFxWD6', null,'管理员', 'admin@163.com', '', '1','1', '0', '1', '1', 'admin', '2020-07-14 14:17:01', 'admin', '2021-02-07 13:51:28',null, '4');


-- ----------------------------
-- Records of gaea_user_role_org
-- ----------------------------
INSERT INTO `gaea_user_role_org` VALUES ('77', 'admin', 'admin', '3000004', 'admin', '2021-03-11 09:38:27', 'admin', '2021-03-11 09:38:27', '1');
