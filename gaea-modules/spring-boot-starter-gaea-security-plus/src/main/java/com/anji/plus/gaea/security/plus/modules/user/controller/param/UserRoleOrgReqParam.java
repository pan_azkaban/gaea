package com.anji.plus.gaea.security.plus.modules.user.controller.param;

import java.io.Serializable;
import java.util.List;

/**
 * 功能描述：
 *
 * @Author: peiyanni
 * @Date: 2021/2/3 17:46
 */
public class UserRoleOrgReqParam implements Serializable {
    private String username;
    private List<String> roleOrgCodes;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<String> getRoleOrgCodes() {
        return roleOrgCodes;
    }

    public void setRoleOrgCodes(List<String> roleOrgCodes) {
        this.roleOrgCodes = roleOrgCodes;
    }
}
