/*
 * @Author: zyk
 * @Date: 2020-07-13 15:13:34
 * @Last Modified by: zyk
 * @Last Modified time: 2021-03-15 13:33:24
 */

import request from '@/utils/request'
// 系统设置模块内所有页面相关接口

// 列表

// 更多
export function myAnnotInfo(params) {
  return request({
    url: '/auth/annot/myAnnotInfo',
    method: 'GET',
    params,
  })
}

// 全部标记已读
export function tagAllRead(data) {
  return request({
    url: '/auth/annot/tagAllRead',
    method: 'POST',
    data,
  })
}
// 查看详情
export function readDetail(id) {
  return request({
    url: '/auth/annot/readDetail/' + id,
    method: 'GET',
  })
}
