package com.anjiplus.gaea.oss.file;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;

public interface FileService {

    /**
     * 文件上传
     *
     * @param file
     * @return 文件访问路径
     */
    String upload(MultipartFile file);

    /**
     * 根据fileId显示图片或者下载文件
     *
     * @param request
     * @param response
     * @param fileName
     * @return
     */
    ResponseEntity<byte[]> download(HttpServletRequest request, HttpServletResponse response, String fileName);

}
