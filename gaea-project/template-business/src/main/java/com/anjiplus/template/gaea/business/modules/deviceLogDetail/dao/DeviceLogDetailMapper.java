package com.anjiplus.template.gaea.business.modules.devicelogdetail.dao;

import org.apache.ibatis.annotations.Mapper;

import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import com.anjiplus.template.gaea.business.modules.devicelogdetail.dao.entity.DeviceLogDetail;

/**
* DeviceLogDetail Mapper
* @author dev-user
* @date 2021-04-07 17:06:42.920
**/
@Mapper
public interface DeviceLogDetailMapper extends GaeaBaseMapper<DeviceLogDetail> {

}