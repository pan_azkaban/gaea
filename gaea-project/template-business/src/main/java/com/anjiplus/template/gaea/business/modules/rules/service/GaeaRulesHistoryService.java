package com.anjiplus.template.gaea.business.modules.rules.service;

import com.anjiplus.template.gaea.business.modules.rules.dao.entity.GaeaRulesHistory;
import com.anjiplus.template.gaea.business.modules.rules.controller.param.GaeaRulesHistoryParam;
import com.anji.plus.gaea.curd.service.GaeaBaseService;

/**
 * (GaeaRulesHistory)Service
 *
 * @author why
 * @since 2021-03-17 13:52:55
 */
public interface GaeaRulesHistoryService extends GaeaBaseService<GaeaRulesHistoryParam, GaeaRulesHistory> {

}