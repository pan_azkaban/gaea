package com.anji.plus.gaea.security.plus.event;

import org.springframework.context.ApplicationListener;

/**
 * 组织监听
 * @author lr
 * @since 2021-05-19
 */
public class OrgEventApplicationListener implements ApplicationListener<OrgEvent> {

    @Override
    public void onApplicationEvent(OrgEvent event) {
    }
}
