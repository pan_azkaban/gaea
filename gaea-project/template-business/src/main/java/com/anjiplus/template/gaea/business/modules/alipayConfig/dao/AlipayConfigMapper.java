package com.anjiplus.template.gaea.business.modules.alipayconfig.dao;

import org.apache.ibatis.annotations.Mapper;

import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import com.anjiplus.template.gaea.business.modules.alipayconfig.dao.entity.AlipayConfig;

/**
* AlipayConfig Mapper
* @author dev-user
* @date 2021-04-07 14:58:28.131
**/
@Mapper
public interface AlipayConfigMapper extends GaeaBaseMapper<AlipayConfig> {

}