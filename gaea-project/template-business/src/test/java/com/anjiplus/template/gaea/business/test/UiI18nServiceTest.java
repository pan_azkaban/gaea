package com.anjiplus.template.gaea.business.test;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.anjiplus.template.gaea.business.modules.gaeaUiI18n.dao.entity.GaeaUiI18n;
import com.anjiplus.template.gaea.business.modules.gaeaUiI18n.service.GaeaUiI18nService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author WongBin
 * @date 2021/3/26
 */

public class UiI18nServiceTest extends ServiceTestBase{

    @Autowired
    GaeaUiI18nService gaeaUiI18nService;
    private GaeaUiI18n qry = new GaeaUiI18n();

    @Before
    public void init(){
        // 菜单初始化 查询参数
        qry.setLocale("zh");
        qry.setCataType("1001");
        qry.setRefer("t_device_info,t_device_log_detail:d,t_device_model:m");//:
        qry.setModule("deviceInfo");
    }

    @Test
    public void testScan(){
        gaeaUiI18nService.scan("t_device_info");
        gaeaUiI18nService.scan("t_device_log_detail");
    }

    @Test
    public void testInitAll(){
        List<String> tables = gaeaUiI18nService.getUi18nTables();
        for (String item : tables) {
            try {
                gaeaUiI18nService.scan(item.split(":")[1]);
            }catch (Exception ex){

            }
        };
    }

    @Test
    public void testInitFields(){
        Map ret = new HashMap();
        for(String table : qry.getRefer().split(",")) {
            qry.setRefer(table);
            Map t = gaeaUiI18nService.getI18nFields(qry);
            // 合并module根节点
            ret.putIfAbsent(qry.getModule(),new HashMap<>());
            // 合并module子节点
            ((Map)ret.get(qry.getModule())).putAll((Map)t.get(qry.getModule()));
        }
        System.out.println(JSON.toJSONString(ret));
    }
}
