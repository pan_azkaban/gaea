package com.anjiplus.template.gaea.auth.modules.role.service.impl;

import com.anji.plus.gaea.bean.TreeNode;
import com.anji.plus.gaea.cache.CacheHelper;
import com.anji.plus.gaea.constant.Enabled;
import com.anji.plus.gaea.constant.GaeaConstant;
import com.anji.plus.gaea.constant.GaeaKeyConstant;
import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import com.anjiplus.template.gaea.auth.modules.authority.dao.GaeaAuthorityMapper;
import com.anjiplus.template.gaea.auth.modules.authority.dao.entity.GaeaAuthority;
import com.anjiplus.template.gaea.auth.modules.menu.controller.dto.TreeDTO;
import com.anjiplus.template.gaea.auth.modules.org.dao.GaeaOrgMapper;
import com.anjiplus.template.gaea.auth.modules.org.dao.entity.GaeaOrg;
import com.anjiplus.template.gaea.auth.modules.role.controller.dto.RoleMenuAuthorityDTO;
import com.anjiplus.template.gaea.auth.modules.role.controller.param.GaeaRoleParam;
import com.anjiplus.template.gaea.auth.modules.role.controller.param.RoleOrgReqParam;
import com.anjiplus.template.gaea.auth.modules.role.dao.GaeaRoleMapper;
import com.anjiplus.template.gaea.auth.modules.role.dao.GaeaRoleMenuAuthorityMapper;
import com.anjiplus.template.gaea.auth.modules.role.dao.GaeaRoleOrgMapper;
import com.anjiplus.template.gaea.auth.modules.role.dao.entity.GaeaRole;
import com.anjiplus.template.gaea.auth.modules.role.dao.entity.GaeaRoleMenuAuthority;
import com.anjiplus.template.gaea.auth.modules.role.dao.entity.GaeaRoleOrg;
import com.anjiplus.template.gaea.auth.modules.role.service.GaeaRoleService;
import com.anjiplus.template.gaea.auth.modules.user.dao.GaeaUserRoleMapper;
import com.anjiplus.template.gaea.auth.modules.user.dao.GaeaUserRoleOrgMapper;
import com.anjiplus.template.gaea.auth.modules.user.dao.entity.GaeaUserRole;
import com.anjiplus.template.gaea.auth.modules.user.dao.entity.GaeaUserRoleOrg;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 角色(GaeaRole)ServiceImpl
 *
 * @author lr
 * @since 2021-02-02 13:37:54
 */
@Service
public class GaeaRoleServiceImpl implements GaeaRoleService {

    @Autowired
    private GaeaRoleMapper gaeaRoleMapper;

    @Autowired
    private GaeaUserRoleMapper gaeaUserRoleMapper;
    @Autowired
    private GaeaRoleOrgMapper gaeaRoleOrgMapper;
    @Autowired
    private GaeaOrgMapper gaeaOrgMapper;
    @Autowired
    private GaeaUserRoleOrgMapper gaeaUserRoleOrgMapper;
    @Autowired
    private GaeaRoleMenuAuthorityMapper gaeaRoleMenuAuthorityMapper;

    @Autowired
    private GaeaAuthorityMapper gaeaAuthorityMapper;

    @Autowired
    private CacheHelper cacheHelper;

    @Override
    public GaeaBaseMapper<GaeaRole> getMapper() {
        return gaeaRoleMapper;
    }


    /**
     * 查询机构下的角色，当传的是值包含":"时，说明是角色编号
     *
     * @param param        查询参数
     * @param queryWrapper 基本查询条件
     * @return
     */
    @Override
    public Wrapper<GaeaRole> extensionWrapper(GaeaRoleParam param, QueryWrapper<GaeaRole> queryWrapper) {
        if (StringUtils.isNotBlank(param.getCode())) {
            if (param.getCode().contains(GaeaConstant.REDIS_SPLIT)) {
                queryWrapper.and(wrapper -> wrapper.or().lambda().eq(GaeaRole::getRoleCode, param.getCode().split(GaeaConstant.REDIS_SPLIT)[1]));
            } else {
                queryWrapper.and(wrapper -> wrapper.or().lambda().eq(GaeaRole::getOrgCode, param.getCode()));
            }
        }
        return queryWrapper;
    }


    @Override
    public List<String> getUserRoleCodes(String username) {

        LambdaQueryWrapper<GaeaUserRole> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(GaeaUserRole::getUsername, username);

        List<GaeaUserRole> gaeaUserRoles = gaeaUserRoleMapper.selectList(wrapper);
        return gaeaUserRoles.stream().map(GaeaUserRole::getRoleCode).collect(Collectors.toList());
    }

    @Override
    public TreeDTO queryRoleOrgTree(String roleCode) {
        //该角色已经关联的组织
        QueryWrapper<GaeaRoleOrg> roleOrgQueryWrapper = new QueryWrapper<GaeaRoleOrg>();
        roleOrgQueryWrapper.lambda().select(GaeaRoleOrg::getId, GaeaRoleOrg::getOrgCode).eq(GaeaRoleOrg::getRoleCode, roleCode);
        //组装出选中的id
        List<GaeaRoleOrg> roleOrgList = gaeaRoleOrgMapper.selectList(roleOrgQueryWrapper);
        List<String> checkedCodes = roleOrgList.stream().map(GaeaRoleOrg::getOrgCode).collect(Collectors.toList());

        //所有的组织
        QueryWrapper<GaeaOrg> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().select(GaeaOrg::getId, GaeaOrg::getOrgCode, GaeaOrg::getOrgName, GaeaOrg::getOrgParentCode)
                .and(e -> e.eq(GaeaOrg::getEnabled, Enabled.YES.getValue()));
        List<GaeaOrg> orgList = gaeaOrgMapper.selectList(queryWrapper);

        List<TreeNode> treeList = buildOrgTree(orgList, "0");
        //返回结果
        TreeDTO result = new TreeDTO();
        result.setTreeDatas(treeList);
        result.setCheckedCodes(checkedCodes);
        return result;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveOrgTreeForRole(RoleOrgReqParam requestModel) {

        //清除菜单的旧关联按钮
        QueryWrapper<GaeaRoleOrg> menuQueryWrapper = new QueryWrapper<>();
        menuQueryWrapper.lambda().eq(GaeaRoleOrg::getRoleCode, requestModel.getRoleCode());
        gaeaRoleOrgMapper.delete(menuQueryWrapper);
        List<String> checkedCodes = requestModel.getOrgCodes();
        List<GaeaRoleOrg> roleOrgList = new ArrayList<>(checkedCodes.size());
        //保存新的
        checkedCodes.stream().forEach(e -> {
            GaeaRoleOrg roleOrgPO = new GaeaRoleOrg();
            roleOrgPO.setOrgCode(e);
            roleOrgPO.setRoleCode(requestModel.getRoleCode());
            roleOrgList.add(roleOrgPO);
        });
        gaeaRoleOrgMapper.insertBatch(roleOrgList);
        //根据roleId清除gaea_user_role_org的已勾选的orgIds
        QueryWrapper<GaeaUserRoleOrg> userRoleOrgQueryWrapper = new QueryWrapper<>();
        LambdaQueryWrapper<GaeaUserRoleOrg> lambdaQueryWrapper = userRoleOrgQueryWrapper.lambda();
        lambdaQueryWrapper.eq(GaeaUserRoleOrg::getRoleCode, requestModel.getRoleCode());
        if (checkedCodes.size() > 0) {
            lambdaQueryWrapper.notIn(GaeaUserRoleOrg::getOrgCode, checkedCodes);
        }
        gaeaUserRoleOrgMapper.delete(userRoleOrgQueryWrapper);
        return true;

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveMenuAuthority(RoleMenuAuthorityDTO dto) {
        String roleCode = dto.getRoleCode();
        String orgCode = dto.getOrgCode();
        List<String> authCodes = dto.getCodes();

        //清除菜单的旧关联按钮
        LambdaQueryWrapper<GaeaRoleMenuAuthority> queryWrapper = Wrappers.lambdaQuery();

        queryWrapper.eq(GaeaRoleMenuAuthority::getRoleCode, roleCode);
        queryWrapper.eq(GaeaRoleMenuAuthority::getOrgCode, orgCode);

        gaeaRoleMenuAuthorityMapper.delete(queryWrapper);

        List<GaeaAuthority> gaeaMenuAuthorities = gaeaAuthorityMapper.selectList(Wrappers.emptyWrapper());

        Map<String, String> authorityPathMap = gaeaMenuAuthorities.stream().collect(Collectors.toMap(GaeaAuthority::getAuthCode, GaeaAuthority::getPath, (value1, value2) -> value2));

        Set<String> newAuthPaths = new HashSet<>();
        if (!CollectionUtils.isEmpty(authCodes)) {
            List<GaeaRoleMenuAuthority> checkList = new ArrayList(authCodes.size());
            //保存新的关联
            authCodes.forEach(s -> {

                GaeaRoleMenuAuthority gaeaRoleMenuAuthority = new GaeaRoleMenuAuthority();
                gaeaRoleMenuAuthority.setRoleCode(roleCode);
                gaeaRoleMenuAuthority.setOrgCode(orgCode);
                if (s.contains(GaeaConstant.REDIS_SPLIT)) {
                    String[] menuAuth = s.split(GaeaConstant.REDIS_SPLIT);
                    gaeaRoleMenuAuthority.setMenuCode(menuAuth[0]);
                    gaeaRoleMenuAuthority.setAuthCode(menuAuth[1]);
                    String path = authorityPathMap.get(menuAuth[1]);
                    gaeaRoleMenuAuthority.setAuthPath(path);
                    newAuthPaths.add(path);

                } else {
                    gaeaRoleMenuAuthority.setMenuCode(s);
                    gaeaRoleMenuAuthority.setAuthCode(s);
                }
                checkList.add(gaeaRoleMenuAuthority);
            });
            gaeaRoleMenuAuthorityMapper.insertBatch(checkList);
        }

        //刷新机构、角色、权限的缓存
        refreshRoleAuthorities(orgCode);
        return true;
    }


    /**
     * 组织树
     *
     * @param orgList
     * @param pCode
     * @return
     */
    private List<TreeNode> buildOrgTree(List<GaeaOrg> orgList, String pCode) {
        List<TreeNode> childList = new ArrayList<>();
        orgList.forEach(orgPO -> {
            if (StringUtils.isNotEmpty(orgPO.getOrgParentCode()) && orgPO.getOrgParentCode().equals(pCode)) {
                TreeNode treeVO = new TreeNode();
                treeVO.setId(orgPO.getOrgCode());
                treeVO.setLabel(orgPO.getOrgName());
                childList.add(treeVO);
            }
        });
        childList.forEach(treeVO -> {
            List<TreeNode> treeList = buildOrgTree(orgList, treeVO.getId());
            if (!treeList.isEmpty()) {
                treeVO.setChildren(treeList);
            }
        });
        return childList;
    }

    /**
     * 获取当前用户拥有的按钮权限
     *
     * @param roleCode
     * @return
     */
    @Override
    public List<String> getSelectAuthorities(String orgCode, String roleCode) {
        LambdaQueryWrapper<GaeaRoleMenuAuthority> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(GaeaRoleMenuAuthority::getOrgCode, orgCode);
        queryWrapper.eq(GaeaRoleMenuAuthority::getRoleCode, roleCode);

        List<GaeaRoleMenuAuthority> gaeaRoleMenuAuthorities = gaeaRoleMenuAuthorityMapper.selectList(queryWrapper);
        return gaeaRoleMenuAuthorities.stream()
                .map(authority -> {
                    String authCode = authority.getAuthCode();
                    if (StringUtils.isNotBlank(authority.getAuthPath())) {
                        authCode = authority.getMenuCode() + GaeaConstant.REDIS_SPLIT + authCode;
                    }
                    return authCode;
                })
                .collect(Collectors.toList());
    }

    @Override
    public void refreshRoleAuthorities(String orgCode) {

        LambdaQueryWrapper<GaeaRoleMenuAuthority> wrapper = Wrappers.lambdaQuery();
        if (StringUtils.isNotBlank(orgCode)) {
            wrapper.eq(GaeaRoleMenuAuthority::getOrgCode, orgCode);
        }
        List<GaeaRoleMenuAuthority> gaeaRoleMenuAuthorities = gaeaRoleMenuAuthorityMapper.selectList(wrapper);

        Map<String, Map<String, String>> orgPathRoleMap = gaeaRoleMenuAuthorities.stream()
                .filter(gaeaRoleMenuAuthority -> StringUtils.isNoneBlank(gaeaRoleMenuAuthority.getAuthPath(), gaeaRoleMenuAuthority.getRoleCode()))
                .collect(Collectors.groupingBy(GaeaRoleMenuAuthority::getOrgCode, Collectors.
                        groupingBy(GaeaRoleMenuAuthority::getAuthPath,
                                Collectors.mapping(GaeaRoleMenuAuthority::getRoleCode, Collectors.joining(GaeaConstant.SPLIT)))));


        orgPathRoleMap.entrySet().stream().forEach(entry -> {
            String key = GaeaKeyConstant.HASH_URL_ROLE_KEY + entry.getKey();

            cacheHelper.delete(key);
            cacheHelper.hashSet(key, entry.getValue());
        });
    }
}
