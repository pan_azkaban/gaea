package com.anjiplus.template.gaea.business.modules.inf.inf.controller.params;

import com.anji.plus.gaea.annotation.Query;
import com.anji.plus.gaea.constant.QueryEnum;
import com.anji.plus.gaea.curd.params.PageParam;
import lombok.Data;


/**
 * InfParams
 *  请求参数
 * @author ultrajiaming
 * @since 2021-03-17 10:17:23
 */
@Data
public class InfParams extends PageParam {

    //接口标识
    @Query(QueryEnum.LIKE)
    private String infName;
    //接口名称
    @Query(QueryEnum.LIKE)
    private String infNameName;
    //接口描述
    @Query(QueryEnum.LIKE)
    private String infDescription;
    //状态 1：启用 0：禁用
    @Query
    private Integer status;
    //所属分组
    @Query
    private String withGroup;
    //交互类型
    @Query
    private String interactionType;
    //创建人
    @Query(QueryEnum.LIKE)
    private String createBy;

}
