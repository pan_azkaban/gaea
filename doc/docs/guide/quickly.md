# 快速了解
---
## 项目简介
- 项目源代码地址: <https://gitee.com/anji-plus/gaea>
- 在线提问: <https://gitee.com/anji-plus/gaea/issues/I3BLNG>
- 在线文档: 

盖亚（Gaea）是一个JavaEE微服务架构平台，采用经典组合（SpringBoot，SpringCloud，Vue，ElementUI）。
内置的基础功能包括组织机构，权限管理（*用户*，*角色*，*菜单*等）；系统管理（*数组字典*，*参数维护*，*操作日志*等）；消息管理，组件中心（*高级查询/自定义列示例*），导出中心；


## 技术选型

**1.环境**
-  JavaEE 8
-  Apache Maven 3

**2.主框架**
-  Spring Boot 2.3.x
-  Spring Cloud Hoxton.SR9
-  Spring Security 5.3.x

**3.持久层**
-  Apache MyBatis 3.5.x

**4.视图层**
-  Vue 2.6.10+
-  ElementUI 2.13.0+


## 系统特性
1. 最新最稳定的技术栈；
2. 高效率的开发。代码生成器可一键生成前后端代码，可快速构建管理界面CRUD接口；
3. 丰富的组件。权限，归档，日志，导出，消息推送等组件功能；
4. 强扩展性。支持用户高级查询，自定义列界面；

## 内置功能
1. 组织机构：可配置系统组织机构；
2. 权限管理：包含权限，菜单，角色，用户相关功能；
3. 系统管理：包含字典管理，参数管理，帮助中心，操作日志功能；
4. 消息管理：包含收发概况，推送相关功能；
5. 组件中心：主要提供高级查询/自定义列示例，前后端CRUD组件示例；
6. 导出中心：系统中导出文件的汇总信息；
7. 提供项目初始化maven archetype及操作手册
8. 代码生成：一键生成前后端代码，支持单表增、删、改、查、详情及子母表详情页功能自动生成，UI支持国际化版本
9. UI界面提供国际化维护功能

## 内置相关组件
内置的组件具体使用规范见*组件文档*，

**1. 盖亚底盘**

包含CRUD封装，国际化，异常处理，敏感信息加密，数据脱敏，自定义注解等基础功能。
相关依赖如下：
```xml
<dependency>
	<groupId>com.anji-plus</groupId>
	<artifactId>spring-boot-gaea</artifactId>
</dependency>
```

**2. 权限组件**

权限是spring security 框架，用户的登录，token校验等功能。
相关依赖如下：
```xml
<dependency>
    <groupId>com.anji-plus</groupId>
    <artifactId>spring-boot-starter-gaea-security</artifactId>
</dependency>
```
**3. 推送组件**

推送消息，目前包含发送邮件和短信两种模式。
相关依赖如下：
```xml
<dependency>
     <groupId>com.anji-plus</groupId>
     <artifactId>spring-boot-starter-gaea-push</artifactId>
</dependency>
```
**4. 日志组件**

日志组件通过注解方式实现，需要打印接口日志或者保存接口日志的地方使用@GaeaAuditLog。
相关依赖如下：
```xml
<dependency>
     <groupId>com.anji-plus</groupId>
     <artifactId>spring-boot-starter-gaea-log</artifactId>
</dependency>
```
**5. 归档组件**

归档组件目前支持Mysql数据库，只需要配置对应的表和字段，就可以实现数据自动归档的功能。
相关依赖如下：
```xml
<dependency>
	<groupId>com.anji-plus</groupId>
	<artifactId>spring-boot-starter-gaea-archiver</artifactId>
</dependency>

```
**6. 导出组件**

导出目前支持简单的导出和模板导出功能，具体的规范见对应的文档。
相关依赖如下：
```xml
 <dependency>
      <groupId>com.anji-plus</groupId>
      <artifactId>spring-boot-starter-gaea-export</artifactId>
 </dependency>
```

## 7. 新项目初始化

gaea-project-template项目是**项目模板**，实际使用时参考如下步骤产生新项目： 

1、clone项目源码，然后本地制作maven archetype模板
  
- 注意事项：
  - 生成maven archetype项目时，先删除template-gaea-ui/node_modules下的内容。
  - maven archetype的相关资料参考[官方文档](http://maven.apache.org/archetype/maven-archetype-plugin/index.html) 
  
2、执行mvn archetype:generate产生新的项目
详细操作步骤参考[mvn-project-init](https://gitee.com/anji-plus/gaea/blob/master/gaea-project-template/gaea-refer-doc/mvn-project-init.md)

3、对于公司内部项目，已制作template-gaea-archetype模板并上传maven私服，忽略以上2步，比如新项目名称为ctms,可以使用如下命令生成新项目：

```bash
mvn archetype:generate 
-DarchetypeCatalog=remote 
-DarchetypeReposity=http://nexus.anji-plus.com:8081/repository/maven-snapshots/archetype-catalog.xml
-DgroupId=com.anjiplus.ctms 
-DartifactId=ctms 
-Dpackage=com.anjiplus.ctms 
-Dversion=1.0.0-SNAPSHOT 
-DarchetypeGroupId=com.anjiplus.template.gaea 
-DarchetypeArtifactId=template-gaea-archetype 
-DarchetypeVersion=1.0.0-SNAPSHOT 
-DinteractiveMode=false
```
新生成的项目,目录结构示例如下
```
ctms            
├── ctms-auth                 权限服务
├── ctms-business             业务服务
├── ctms-common               基础工具包
├── ctms-gateway              网关
├── ctms-generator            代码生成器
├── ctms-ui                   前端vue
├── gaea-refer-doc            初始化dbscript及配置 
├── pom.xml                       
├── README.md                  
```