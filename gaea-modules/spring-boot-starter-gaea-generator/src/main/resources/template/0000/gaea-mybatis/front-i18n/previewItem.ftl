<#--noinspection ALL-->
<!-- **i18n版本-->
<template>
  <div class="">
      <header class="header">{{ tff('moduleName') }}</header>
      <el-table ref="table" :data="this.detail" size="small" style="width: 100%;" >
        <#--<el-table-column type="selection" width="55" />-->
  <#if columns??>
     <#list columns as column>
        <#if column.columnShow>
          <#if (column.dictName)?? && (column.dictName)!="">
        <el-table-column prop="${column.changeColumnName}Cn" :label="tff('${column.changeColumnName}')">
         <#-- <template slot-scope="scope">
            {{ dict.label.${column.dictName}[scope.row.${column.changeColumnName}] }}
          </template>-->
        </el-table-column>
          <#else>
        <el-table-column prop="${column.changeColumnName}" :label="tff('${column.changeColumnName}')" />
          </#if>
        </#if>
     </#list>
  </#if>
        <!-- v-if="checkPer(['admin','${changeClassName}:edit','${changeClassName}:del'])"-->
        <el-table-column  :label="$t('btn.operationCol')" width="200px" align="center">
          <#--<template slot-scope="scope">

          </template>-->
        </el-table-column>
      </el-table>
  </div>
</template>

<script>

import i18nHandler from '@/mixins/i18nHandler'

export default {
  name: '${className}PreviewItem',
  mixins:[ i18nHandler ],
  props:{
      module:String,
      alias:String
  },
  data() {
      return {
          detail: {},i18nQry:{}
      }
  },
  created() {
      this.i18nQry.module = this.module
  },
  methods: {
      tff(code){
          return this.tf(this.alias+'.'+code)
      }
  },
}
</script>

<style scoped>

</style>
