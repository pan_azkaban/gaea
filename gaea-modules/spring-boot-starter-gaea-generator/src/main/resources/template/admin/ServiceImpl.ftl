
package ${package}.service.impl;

import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ${package}.dao.entity.${className};
import ${package}.service.${className}Service;
import ${package}.dao.${className}Mapper;
<#if subEntity??>
    <#list subEntity as s>
import ${basePackage}.${s.instance?lower_case}.service.${s.className}Service;
    </#list>
</#if>
/**
* @desc ${className} ${apiAlias}服务实现
* @author ${author}
* @date ${date}
**/
@Service
public class ${className}ServiceImpl implements ${className}Service {

    @Autowired
    private ${className}Mapper ${changeClassName}Mapper;

    @Override
    public GaeaBaseMapper<${className}> getMapper() {
      return ${changeClassName}Mapper;
    }

<#if hasSubEntity>
    <#if subEntity??>
        <#list subEntity as s>
    @Autowired
    private ${s.className}Service ${s.instance}Service;

        </#list>
    </#if>
</#if>
    @Override
    public ${className} getDetail(Long id) {
        ${className} ${changeClassName} = this.selectOne(id);
<#if hasSubEntity>
    <#if subEntity??>
        <#list subEntity as s>
        ${changeClassName}.set${s.className}s(${s.instance}Service.list("${s.dbFieldName}",${changeClassName}.get${s.pFieldName}()));
        </#list>
    </#if>
</#if>
        return ${changeClassName};
    }
}