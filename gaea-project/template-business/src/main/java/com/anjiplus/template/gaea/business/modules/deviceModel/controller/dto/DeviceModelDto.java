
package com.anjiplus.template.gaea.business.modules.devicemodel.controller.dto;

import java.io.Serializable;

import com.anji.plus.gaea.curd.dto.GaeaBaseDTO;
import com.anji.plus.gaea.annotation.Formatter;
import lombok.Data;


/**
*
* @description 设备类型 dto
* @author dev-user
* @date 2021-04-07 17:27:35.130
**/
@Data
public class DeviceModelDto extends GaeaBaseDTO implements Serializable {
    /** 设备类型 */
    @Formatter(dictCode = "DEVICE_TYPE",targetField = "deviceTypeCn")
    private String deviceType;
    private String deviceTypeCn;

    /** 厂商名称 */
    @Formatter(dictCode = "MANUFACTURER",targetField = "manufacturerCn")
    private String manufacturer;
    private String manufacturerCn;

    /** 设备型号 */
    @Formatter(dictCode = "device_cataType",targetField = "deviceModelCn")
    private String deviceModel;
    private String deviceModelCn;

    /** 启用标志 */
    @Formatter(dictCode = "FILTER_FLAG",targetField = "enableFlagCn")
    private Integer enableFlag;
    private String enableFlagCn;

    /** 删除标志 */
    @Formatter(dictCode = "DELETE_FLAG",targetField = "deleteFlagCn")
    private Integer deleteFlag;
    private String deleteFlagCn;

}