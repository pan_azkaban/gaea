package com.anji.plus.gaea.archiver.service;

import com.anji.plus.gaea.archiver.config.ArchiverTable;
import com.anji.plus.gaea.archiver.consant.DBType;
import com.anji.plus.gaea.archiver.sqlbuilder.ISQLBuilder;
import com.anji.plus.gaea.archiver.sqlbuilder.SQLBuilderFactory;
import com.anji.plus.gaea.archiver.utils.DateUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 数据库操作实现类
 *
 * @author 木子李·De
 * @since 2021/2/3 14:16
 */
@Service
public class DBOperatorService implements IDBOperatorService {

    private static DBType dbType;
    private static Logger logger = LoggerFactory.getLogger(DBOperatorService.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private DBType getDBType() throws SQLException {
        if(dbType != null){
            return dbType;
        }
        String databaseProductName=jdbcTemplate.getDataSource().getConnection().getMetaData().getDatabaseProductName();
        if(databaseProductName == null || databaseProductName.trim().length() == 0){
            throw new RuntimeException(String.format("%s database not supported", databaseProductName));
        }
        dbType=DBType.getDbType(databaseProductName);
        return Optional.ofNullable(dbType).orElseThrow(()->
            new RuntimeException(String.format("%s database not supported", databaseProductName))
        );
    }

    private ISQLBuilder getSQLBuilder(){
        try{
            return SQLBuilderFactory.getSQLBuilder(getDBType());
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public boolean existTable(String tableName) {
        try{
            String sql = getSQLBuilder().existTable(tableName);
            logger.info("exist-table-sql={}",sql);
            jdbcTemplate.queryForRowSet(sql);
            return true;
        }catch (Exception e){
            logger.error("exist-err:{}",tableName,e);
            return false;
        }
    }

    @Override
    public boolean existTableField(String tableName, String fieldName) {
        try{
            String sql = getSQLBuilder().existTableField(tableName, fieldName);
            logger.info("get-table-metadata-sql={}",sql);
            SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet(sql);

            boolean existField = false;
            while (sqlRowSet.next()){
                String fileName = sqlRowSet.getString("Field");
                if(fileName.toLowerCase().trim().equals(fieldName.trim())){
                    existField = true;
                    break;
                }
            }
            return existField;
        }catch (Exception e){
            logger.error("exist-err:{}:{}",tableName,fieldName,e);
            return false;
        }
    }

    @Override
    public Date getNow() {
        String sql = getSQLBuilder().getNow();
        Map<String, Object> map = jdbcTemplate.queryForMap(sql);
        Date now = (Date)map.get("now");
        return null;
    }

    @Override
    public Long countRows(String sourceTableName) {
        return null;
    }

    @Override
    public List<String> getCrossMonthList(String tablename, String timefield, Integer maxDaysBeforeArchive) {
        String sql = getSQLBuilder().getCrossMonthList(tablename, timefield, maxDaysBeforeArchive);
        logger.info("get-month-sql={}",sql);
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);

        //yyyy-MM
        List<String> monthList = new ArrayList<String>();
        if(list == null ||list.isEmpty()){
            logger.warn("get-month-sql-ret-is-empty");
            return monthList;
        }

        for(Map<String, Object> item: list){
            String monthStr = (String) item.get("month");
            monthList.add(monthStr);
        }
        logger.warn("get-month-sql-ret:{}",monthList);
        return monthList;
    }

    @Override
    public String createArchiverTableIfNotExist(String sourceTableName, String monthStr){
        monthStr = monthStr.replace("-","");
        String archiveTableName = String.format("%s_%s", sourceTableName, monthStr);
        if(existTable(archiveTableName) == false){
            String sql = getSQLBuilder().createArchiverTableIfNotExist(archiveTableName, sourceTableName);
            logger.info("create-table-sql={}",sql);
            jdbcTemplate.execute(sql);
        }
        return archiveTableName;
    }

    @Override
    public String createArchiverTableIfNotExist(String tableName, String monthStr, ArchiverTable archiverTable) {
        monthStr = monthStr.replace("-","");
        // table_202312,table_202212,table_202112
        List<String> archiveTables = scanArchiver(tableName,archiverTable.getDbName());
        if(!CollectionUtils.isEmpty(archiveTables)) {
            int curr = Integer.valueOf(monthStr);
            // 归档表
            List<String> list = archiveTables.stream().sorted().collect(Collectors.toList());
            // 最新的归档表
            String latest = list.get(list.size()-1);
            latest = latest.replace(tableName, "");
            latest = latest.replace("_","");
            int lastArchive = Integer.valueOf(latest);

            if(curr <=lastArchive ){
                for(int i=0;i<list.size()-1;i++){
                    String m = list.get(i).replace(tableName,"").replace("_","");
                    // 历史数据 找到最合适的归档表
                    if(curr <= Integer.valueOf(m)){
                        return list.get(i);
                    }
                }
                // 最近的一个归档表
                return list.get(list.size()-1);
            }else{
                // 没找到归档表，新建一个归档表
                String fmt = "yyyyMM";
                String fmtDay = "yyyyMMdd";
                long nextArchive = DateUtil.addDays(DateUtil.parse(latest+"01",fmtDay),
                        archiverTable.getMaxDaysBeforeArchive()).getTime();
                monthStr = DateUtil.format(new Date(nextArchive),fmt);
            }
        }
        String archiveTableName = String.format("%s_%s", tableName, monthStr);
        if(!existTable(archiveTableName)){
            String sql = getSQLBuilder().createArchiverTableIfNotExist(archiveTableName, tableName);
            logger.info("create-table-sql={}",sql);
            jdbcTemplate.execute(sql);
        }
        return archiveTableName;
    }

    @Override
    public boolean doArchiver(String sourceTableName, Date startTime, Date endTime) {
        return false;
    }

    /**
     * 扫描以表名为前缀的所有归档表
     *
     * @param sourceTableName
     * @return list
     */
    @Override
    public List<String> scanArchiver(String sourceTableName, String dbName) {
        String sql = getSQLBuilder().scanArchive(sourceTableName, dbName,"_20");
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql);
        List<String> tableNameList = new ArrayList<String>();
        maps.forEach(stringObjectMap -> {
            String tableName = (String) stringObjectMap.get("table_name");
            tableNameList.add(tableName);
        });
        logger.info("scan-table-sql={},tables:{}",sql,tableNameList);
        return tableNameList;
    }

    /**
     * 执行dump命令
     *
     * @param archiverTable
     * @return
     */
    @Override
    public String executeDump(ArchiverTable archiverTable) {
        return getSQLBuilder().executeDump(archiverTable);
    }

    /**
     * drop 过期的数据库表
     *
     * @param archiverTable
     * @return
     */
    @Override
    public String dropTable(ArchiverTable archiverTable) {
        String sql = getSQLBuilder().dropTable(archiverTable);
        logger.info("drop-table-sql={}",sql);
        jdbcTemplate.execute(sql);
        return null;
    }
}
