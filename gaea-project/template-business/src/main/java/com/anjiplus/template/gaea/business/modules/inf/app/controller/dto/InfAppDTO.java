package com.anjiplus.template.gaea.business.modules.inf.app.controller.dto;

import com.anji.plus.gaea.annotation.Formatter;
import com.anji.plus.gaea.curd.dto.GaeaBaseDTO;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * InfAppDTO
 *  数据传输封装
 * @author ultrajiaming
 * @since 2021-03-18 23:27:36
 */
@Data
public class InfAppDTO extends GaeaBaseDTO {
    //应用名称
    @NotBlank
    private String appName;
    //应用ID
    @NotBlank
    private String appId;
    //应用秘钥
    private String appSecret;
    //1：有效 0：无效
    @NotNull
    @Formatter(dictCode = "ENABLE_FLAG",targetField = "statusName")
    private Integer status;
    private String statusName;
    //描述
    private String remark;
}
