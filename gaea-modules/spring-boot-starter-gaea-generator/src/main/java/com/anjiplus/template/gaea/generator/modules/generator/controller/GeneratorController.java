/*
 *  Copyright 2019-2020 Zheng Jie
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.anjiplus.template.gaea.generator.modules.generator.controller;

import com.anji.plus.gaea.bean.KeyValue;
import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.curd.controller.BaseResponse;
import com.anjiplus.template.gaea.generator.modules.column.dao.GeneratorColumn;
import com.anjiplus.template.gaea.generator.modules.generator.controller.param.GeneratorParam;
import com.anjiplus.template.gaea.generator.modules.generator.service.GeneratorService;
import com.anjiplus.template.gaea.generator.modules.table.dao.GeneratorTable;
import com.anjiplus.template.gaea.generator.modules.table.service.TableService;
import com.anjiplus.template.gaea.generator.utils.PageUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @author Zheng Jie
 * @date 2019-01-02
 */
@RestController
@RequestMapping("/api/generator")
@Api(tags = "系统：代码生成管理")
@RefreshScope
public class GeneratorController extends BaseResponse {

    @Autowired(required = false)
    private GeneratorService generatorService;

    @Autowired(required = false)
    private TableService tableService;

    @ApiOperation("查询数据库schemas")
    @GetMapping(value = "/schemas")
    public ResponseBean querySchemas(){
        List<KeyValue> keyValueList = generatorService.getSchemas();
        return responseSuccessWithData(keyValueList);
    }

    @ApiOperation("查询数据库数据")
    @GetMapping(value = "/tables/all")
    public ResponseBean queryTables(@RequestParam(required = false) String schema){
        return responseSuccessWithData(generatorService.getTables(schema));
    }

    @ApiOperation("查询数据库数据")
    @GetMapping(value = "/tables")
    public ResponseBean queryTables(GeneratorParam generatorParam){
        Page page = generatorService.getTables(generatorParam);
        return responseSuccessWithData(page);
    }

    @ApiOperation("查询字段数据")
    @GetMapping(value = "/columns")
    public ResponseBean queryColumns(@RequestParam String tableName,
                                               @RequestParam(required = false) String schema){
        List<GeneratorColumn> generatorColumnInfos = generatorService.getColumns(schema,tableName);
        Page pages = PageUtil.toPage(generatorColumnInfos, generatorColumnInfos.size());
        return responseSuccessWithData(pages);
    }

    @ApiOperation("保存字段数据")
    @PostMapping(value = "/saveColumnCfg/{schema}")
    public ResponseBean save(@RequestBody List<GeneratorColumn> generatorColumnInfos){
        generatorService.saveColumnGenConfig(generatorColumnInfos);
        return responseSuccess();
    }

    @ApiOperation("同步字段数据")
    @PostMapping(value = "sync/{schema}")
    public ResponseBean sync(@RequestBody List<String> tables,
                                           @PathVariable String schema){
        for (String table : tables) {
            generatorService.sync(generatorService.getColumns(schema,table),
                    generatorService.queryColumns(schema,table));
        }
        return responseSuccess();
    }

    @ApiOperation("生成代码")
    @PostMapping(value = "/{project}/{tableName}/{type}/{schema}")
    public ResponseBean generator(
            @PathVariable String project,
            @PathVariable String tableName,
            @PathVariable Integer type,
            @PathVariable String schema,
            HttpServletRequest request, HttpServletResponse response){

        GeneratorTable config = tableService.find(schema,tableName);
        List<GeneratorColumn> generatorColumns =  generatorService.getColumns(schema,tableName);
        switch (type){
            // 生成代码
            case 0:
                generatorService.generator(config, generatorColumns);
                break;
            // 预览
            case 1:
                List list = generatorService.preview(config, generatorColumns);
                return responseSuccessWithData(list);
            // 打包
            case 2:
                generatorService.download(config, generatorColumns, request, response);
                break;
            default: throw new RuntimeException("没有这个选项");
        }
        return responseSuccess();
    }
}
