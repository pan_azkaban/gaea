package com.anjiplus.template.gaea.auth.modules.menuextension.dao;

import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import com.anjiplus.template.gaea.auth.modules.menuextension.entity.GaeaMenuExtension;
import org.apache.ibatis.annotations.Mapper;


/**
 * (GaeaMenuExtension)Mapper
 *
 * @author peiyanni
 * @since 2021-02-04 17:14:14
 */
@Mapper
public interface GaeaMenuExtensionMapper extends GaeaBaseMapper<GaeaMenuExtension> {

}
