package com.anjiplus.template.gaea.auth.modules.gaeaannot.service;

import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.controller.dto.GaeaAnnouncementDTO;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.controller.param.GaeaAnnouncementSendParam;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.dao.entity.GaeaAnnouncementSend;

import java.util.List;

/**
 * 系统公告发送阅读记录表(GaeaAnnouncementSend)Service
 *
 * @author makejava
 * @since 2021-03-29 15:31:29
 */
public interface GaeaAnnouncementSendService extends GaeaBaseService<GaeaAnnouncementSendParam, GaeaAnnouncementSend> {

    /**
     * 查看我的消息详情，
     * 如果是未读的，需要标记为已读
     * @param id
     * @return
     */
    GaeaAnnouncementDTO getAnnotReadDetailInfo(Long id);
    /**
     * 标记已读，如果没有ids，则是当前用户下所有未读进行标记
     * @param ids
     * @return
     */
    Boolean tagAllRead(List<Long> ids);
}