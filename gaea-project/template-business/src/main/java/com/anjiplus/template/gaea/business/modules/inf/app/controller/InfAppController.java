package com.anjiplus.template.gaea.business.modules.inf.app.controller;

import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.curd.controller.GaeaBaseController;
import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anjiplus.template.gaea.business.modules.inf.app.controller.dto.InfAppDTO;
import com.anjiplus.template.gaea.business.modules.inf.app.controller.params.InfAppParams;
import com.anjiplus.template.gaea.business.modules.inf.app.dao.entity.InfAppEntity;
import com.anjiplus.template.gaea.business.modules.inf.app.service.InfAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * (InfApp)表控制层
 *
 * @author ultrajiaming
 * @since 2021-03-18 23:27:42
 */
@RestController
@RequestMapping("inf/app")
public class InfAppController extends GaeaBaseController<InfAppParams, InfAppEntity, InfAppDTO> {

    @Autowired
    private InfAppService infAppService;

    @Override
    public GaeaBaseService<InfAppParams, InfAppEntity> getService() {
        return infAppService;
    }

    @Override
    public InfAppEntity getEntity() {
        return new InfAppEntity();
    }

    @Override
    public InfAppDTO getDTO() {
        return new InfAppDTO();
    }

    /**
     * 插入
     */
    @PostMapping("add")
    public ResponseBean add(@Validated @RequestBody InfAppDTO appDto) {
        return this.insert(appDto);
    }

    /**
     * 修改
     */
    @PostMapping("edit")
    public ResponseBean edit(@Validated @RequestBody InfAppDTO appDto) {
        return this.update(appDto);
    }


    /**
     * 更新秘钥
     */
    @GetMapping("appSecret/{id}")
    public ResponseBean updateSecret(@PathVariable Long id) {
        infAppService.updateSecret(id);
        return responseSuccess();
    }

    /**
     * 下拉应用列表，供授权使用
     */
    @GetMapping("select/dropDown")
    public ResponseBean dropDown() {
        return responseSuccessWithData(infAppService.dropDown());
    }

}
