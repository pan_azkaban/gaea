package com.anji.plus.gaea.job.trigger.controller;

import com.anji.plus.gaea.job.core.dto.ReturnT;
import com.anji.plus.gaea.job.core.param.HandleCallbackParam;
import com.anji.plus.gaea.job.core.param.RegistryParam;
import com.anji.plus.gaea.job.trigger.service.TriggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @desc 调度器提供给执行器远程调用接口 controller
 * @author 木子李*de
 * @date 2023-05-12 15:25:35.961
 **/
@RestController
@RequestMapping("/jobserver")
public class TriggerController {

    @Autowired
    private TriggerService triggerService;

    @ResponseBody
    @RequestMapping("/registryUp")
    public ReturnT<String> executorRegistryUp(@RequestBody RegistryParam registryParam){
        return triggerService.registryUp(registryParam);
    }

    @ResponseBody
    @RequestMapping("/registryDown")
    public ReturnT<String> executorRegistryDown(@RequestBody RegistryParam registryParam){
        return triggerService.registryDown(registryParam);
    }

    @ResponseBody
    @RequestMapping("/callback")
    public ReturnT<String> executorCallback(@RequestBody List<HandleCallbackParam> callbackParamList){
        return triggerService.callback(callbackParamList);
    }
}
