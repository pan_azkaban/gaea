package com.anjiplus.template.gaea.auth.modules.thirdapis.controller.param;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 功能描述：
 * appsp推送测试
 * @Author: peiyanni
 * @Date: 2021/4/12 13:31
 */
@Getter
@Setter
public class AppspPushParam {
    /**
     * 推送标题
     */
    @NotBlank(message = "The title cannot be empty")
    private String title;
    /**
     * 推送内容
     */
    @NotBlank(message = "The content cannot be empty")
    private String content;
    /**
     * 推送类型 1透传消息 0 普通消息（默认0）
     */
    private String pushType;

    /**
     * 推送用户
     */
    @NotEmpty(message="The recipient cannot be empty")
    private List<String> usernames;
}
