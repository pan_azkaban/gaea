package com.anji.plus.gaea.security.plus.modules.menu.controller.param;

import java.io.Serializable;
import java.util.List;

/**
 * 功能描述：
 * 菜单按钮新增请求实体
 * @Author: peiyanni
 * @Date: 2021/2/3 15:27
 */
public class MenuActionReqParam implements Serializable {
    /**
     * 菜单code
     */
    private String menuCode;
    /**
     * 所选按钮code集合
     */
    private List<String> actionCodes;

    public String getMenuCode() {
        return menuCode;
    }

    public void setMenuCode(String menuCode) {
        this.menuCode = menuCode;
    }

    public List<String> getActionCodes() {
        return actionCodes;
    }

    public void setActionCodes(List<String> actionCodes) {
        this.actionCodes = actionCodes;
    }
}
