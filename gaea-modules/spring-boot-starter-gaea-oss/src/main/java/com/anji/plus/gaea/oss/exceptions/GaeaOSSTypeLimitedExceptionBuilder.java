package com.anji.plus.gaea.oss.exceptions;

public class GaeaOSSTypeLimitedExceptionBuilder {
    public GaeaOSSTypeLimitedExceptionBuilder() {
    }

    public static GaeaOSSTypeLimitedException build(String code) {
        return new GaeaOSSTypeLimitedException(code);
    }

    public static GaeaOSSTypeLimitedException build(Throwable throwable) {
        return new GaeaOSSTypeLimitedException(throwable);
    }

    public static GaeaOSSTypeLimitedException build(String code, Throwable throwable) {
        return new GaeaOSSTypeLimitedException(code, throwable);
    }

}
