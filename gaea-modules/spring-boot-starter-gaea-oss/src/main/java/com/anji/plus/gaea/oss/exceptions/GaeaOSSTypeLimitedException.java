package com.anji.plus.gaea.oss.exceptions;

public class GaeaOSSTypeLimitedException extends GaeaOSSException{

    public GaeaOSSTypeLimitedException(String message) {
        super(message);
    }

    public GaeaOSSTypeLimitedException(Throwable throwable) {
        super(throwable);
    }

    public GaeaOSSTypeLimitedException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
