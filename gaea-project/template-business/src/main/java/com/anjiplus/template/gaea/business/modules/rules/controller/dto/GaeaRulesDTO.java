package com.anjiplus.template.gaea.business.modules.rules.controller.dto;

import com.anji.plus.gaea.curd.dto.GaeaBaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
/**
 * (GaeaRules)实体类
 *
 * @author lirui
 * @since 2021-03-12 10:38:19
 */
@ApiModel(value = "")
public class GaeaRulesDTO extends GaeaBaseDTO implements Serializable {

    @ApiModelProperty(value = "规则名称")
    private String ruleName;

    @ApiModelProperty(value = "规则编码")
    private String ruleCode;

    @ApiModelProperty(value = "规则字段值")
    private String ruleFields;

    @ApiModelProperty(value = "规则内容")
    private String ruleContent;

    @ApiModelProperty(value = "规则测试值")
    private String ruleDemo;

    private Integer enabled;

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(String ruleCode) {
        this.ruleCode = ruleCode;
    }

    public String getRuleFields() {
        return ruleFields;
    }

    public void setRuleFields(String ruleFields) {
        this.ruleFields = ruleFields;
    }

    public String getRuleContent() {
        return ruleContent;
    }

    public void setRuleContent(String ruleContent) {
        this.ruleContent = ruleContent;
    }

    public String getRuleDemo() {
        return ruleDemo;
    }

    public void setRuleDemo(String ruleDemo) {
        this.ruleDemo = ruleDemo;
    }


}
