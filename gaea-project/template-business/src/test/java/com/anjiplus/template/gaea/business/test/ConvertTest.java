package com.anjiplus.template.gaea.business.test;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.anji.plus.gaea.cache.CacheHelper;
import com.anji.plus.gaea.constant.GaeaKeyConstant;
import com.anjiplus.template.gaea.business.modules.deviceinfo.controller.DeviceInfoController;
import com.anjiplus.template.gaea.business.modules.deviceinfo.service.DeviceInfoService;

/**
 * @author WongBin
 * @date 2021/4/8
 */
public class ConvertTest extends ServiceTestBase{

    @Autowired
    private DeviceInfoService deviceInfoService;
    @Autowired
    private DeviceInfoController api;
    private Long id = 1L;

    @Test
    public void testQueryDetail(){
        Object ret = api.detail(id);
        System.out.println(JSON.toJSONString(ret));
    }

    @Test
    public void testGetKeys(){
        Object ret = cacheHelper.keys(GaeaKeyConstant.DICT_PREFIX+"*");
        assert ret !=null;
        System.out.println(JSON.toJSONString(ret));
    }

    @Autowired
    CacheHelper cacheHelper;
}
