package com.anjiplus.template.gaea.business.modules.inf.app.controller.params;

import com.anji.plus.gaea.annotation.Query;
import com.anji.plus.gaea.constant.QueryEnum;
import com.anji.plus.gaea.curd.params.PageParam;
import lombok.Data;


/**
 * InfAppParams
 *  请求参数
 * @author ultrajiaming
 * @since 2021-03-18 23:27:41
 */
@Data
public class InfAppParams extends PageParam {

    //应用名称
    @Query(QueryEnum.LIKE)
    private String appName;
    //应用ID
    @Query(QueryEnum.LIKE)
    private String appId;
    //1：有效 0：无效
    @Query
    private Integer status;

}
