package com.anjiplus.template.gaea.auth.modules.role.controller.param;


import com.anji.plus.gaea.annotation.Query;
import com.anji.plus.gaea.constant.QueryEnum;
import com.anji.plus.gaea.curd.params.PageParam;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 角色(GaeaRole)param
 *
 * @author lr
 * @since 2021-02-02 13:37:54
 */
@Getter
@Setter
public class GaeaRoleParam extends PageParam implements Serializable {

    /**
     * 机构编号
     */
    private String orgCode;

    /**
     * 角色名称
     */
    @Query(QueryEnum.LIKE)
    private String roleName;
    /**
     * 1：可用 0：禁用
     */
    private Integer enabled;
    /**
     * 描述
     */
    private String remark;


    /**
     * 左边树编码
     */
    @Query(where = false)
    private String code;
}
