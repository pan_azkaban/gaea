package com.anji.plus.gaea.security.plus.modules.org.service;

import com.anji.plus.gaea.bean.TreeNode;
import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anji.plus.gaea.security.plus.modules.org.controller.dto.GaeaOrgDTO;
import com.anji.plus.gaea.security.plus.modules.org.controller.param.GaeaOrgParam;
import com.anji.plus.gaea.security.plus.modules.org.dao.entity.GaeaOrg;

import java.util.List;

/**
 * 组织(GaeaOrg)Service
 *
 * @author lr
 * @since 2021-02-02 13:37:33
 */
public interface GaeaOrgService extends GaeaBaseService<GaeaOrgParam, GaeaOrg> {

    /**
     * 查询所有可用机构信息
     * @return
     */
    List<GaeaOrg> queryAllOrg(GaeaOrgParam gaeaOrgParam);

    /**
     * 组织树
     * @param hasRole 是否带角色
     * @return
     */
    List<TreeNode> tree(boolean hasRole);

    /** 根据orgCode返回详情
     * @param orgCode
     * @return
     */
    GaeaOrgDTO queryByOrgCode(String orgCode);

    /**根据组织code更新名称
     * @param gaeaOrgDTO
     * @return
     */
    Integer updateNameStatusByCode(GaeaOrgDTO gaeaOrgDTO);

    /** 根据orgCode删除机构
     * @param orgCode
     * @return
     */
    Integer deleteByOrgCode(String orgCode);


    /**
     * 获取当前用户的机构，包含下级机构
     * @return
     */
    List<String> getCurrentUserOrgCodes();
}
