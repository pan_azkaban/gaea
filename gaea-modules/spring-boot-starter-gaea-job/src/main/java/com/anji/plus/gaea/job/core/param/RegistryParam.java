package com.anji.plus.gaea.job.core.param;

import org.springframework.util.StringUtils;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @desc MetaJobExecutor 定时任务执行器注册表查询输入类
 * @author 木子李*de
 * @date 2023-05-12 15:25:35.961
 **/
public class RegistryParam implements Serializable {

    /**  执行器的标识，取自执行器springboot的spring.application.name */
    private String executorCode;

    /**  执行器的访问前缀，http://ip:port/contextpath/ */
    private String executorAddress;

    /**  执行器的下所有的JobHandler实现 */
    private String[] jobHandlerBeanNames;

    /**  执行器的注册时间，yyyy-MM-dd HH:mm:ss */
    private Date registoryTime;

    /**  执行器的过期时间，从1970年1月1日开始计算的总毫秒数 */
    private Long expireTimemillis;

    public RegistryParam() {}
    public RegistryParam(String executorCode, String executorAddress, String[] jobHandlerBeanNames) {
        this.executorCode = executorCode;
        this.executorAddress = executorAddress;
        this.jobHandlerBeanNames = jobHandlerBeanNames;
    }
    public RegistryParam(String executorCode, String executorAddress) {
        this.executorCode = executorCode;
        this.executorAddress = executorAddress;
        this.jobHandlerBeanNames = new String[0];
    }

    public String getExecutorCode() {
        return executorCode;
    }

    public void setExecutorCode(String executorCode) {
        this.executorCode = executorCode;
    }

    public String getExecutorAddress() {
        return executorAddress;
    }

    public void setExecutorAddress(String executorAddress) {
        this.executorAddress = executorAddress;
    }

    public String[] getJobHandlerBeanNames() {
        return jobHandlerBeanNames;
    }

    public void setJobHandlerBeanNames(String[] jobHandlerBeanNames) {
        this.jobHandlerBeanNames = jobHandlerBeanNames;
    }

    public Date getRegistoryTime() {
        return registoryTime;
    }

    public void setRegistoryTime(Date registoryTime) {
        this.registoryTime = registoryTime;
    }

    public Long getExpireTimemillis() {
        return expireTimemillis;
    }

    public void setExpireTimemillis(Long expireTimemillis) {
        this.expireTimemillis = expireTimemillis;
    }

    public boolean isValid(){
        if(StringUtils.isEmpty(executorCode) || StringUtils.isEmpty(executorAddress) ){
            return false;
        }
        if(executorAddress.startsWith("http") == false){
            return false;
        }
        return true;
    }
}