package com.anjiplus.template.gaea.generator.modules.project.controller.param;


import java.io.Serializable;

import com.anji.plus.gaea.curd.params.PageParam;

import lombok.Data;

/**
 * (Project)param
 *
 * @author makejava
 * @since 2021-03-03 09:54:53
 */
@Data
public class ProjectParam extends PageParam implements Serializable {
    private String name;

    private String code;
    private String gitRepo;
}
