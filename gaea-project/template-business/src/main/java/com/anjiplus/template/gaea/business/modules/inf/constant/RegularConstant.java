package com.anjiplus.template.gaea.business.modules.inf.constant;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author weijiaming
 * @since 2021/3/26 12:56
 */
public interface RegularConstant {



    /**
     * 正则匹配诸如：${xxx}的不安全SQL
     **/
    Pattern STATIC_SQL_PATTERN = Pattern.compile("[\\s\\S]*\\$\\{[\\S]*\\}[\\s\\S]*");

    /**
     * 正则匹配诸如：create xxx
     **/
    Pattern SQL_PATTERN_START_CREATE = Pattern.compile("^create\\s[\\s\\S]*");

    /**
     * 正则匹配诸如：xxx create xxx
     **/
    Pattern SQL_PATTERN_CONTAINS_CREATE = Pattern.compile("[\\s\\S]*\\screate\\s[\\s\\S]*");

    /**
     * 正则匹配诸如：insert xxx
     **/
    Pattern SQL_PATTERN_START_INSERT = Pattern.compile("^insert\\s[\\s\\S]*");

    /**
     * 正则匹配诸如：xxx insert xxx
     **/
    Pattern SQL_PATTERN_CONTAINS_INSERT = Pattern.compile("[\\s\\S]*\\sinsert\\s[\\s\\S]*");

    /**
     * 正则匹配诸如：update xxx
     **/
    Pattern SQL_PATTERN_START_UPDATE = Pattern.compile("^update\\s[\\s\\S]*");

    /**
     * 正则匹配诸如：xxx update xxx
     **/
    Pattern SQL_PATTERN_CONTAINS_UPDATE = Pattern.compile("[\\s\\S]*\\supdate\\s[\\s\\S]*");

    /**
     * 正则匹配诸如：delete xxx
     **/
    Pattern SQL_PATTERN_START_DELETE = Pattern.compile("^delete\\s[\\s\\S]*");

    /**
     * 正则匹配诸如：xxx delete xxx
     **/
    Pattern SQL_PATTERN_CONTAINS_DELETE = Pattern.compile("[\\s\\S]*\\sdelete\\s[\\s\\S]*");

    /**
     * 正则匹配诸如：drop xxx
     **/
    Pattern SQL_PATTERN_START_DROP = Pattern.compile("^drop\\s[\\s\\S]*");

    /**
     * 正则匹配诸如：xxx drop xxx
     **/
    Pattern SQL_PATTERN_CONTAINS_DROP = Pattern.compile("[\\s\\S]*\\sdrop\\s[\\s\\S]*");

    /**
     * 正则匹配诸如：alter xxx
     **/
    Pattern SQL_PATTERN_START_ALTER = Pattern.compile("^alter\\s[\\s\\S]*");

    /**
     * 正则匹配诸如：xxx alter xxx
     **/
    Pattern SQL_PATTERN_CONTAINS_ALTER = Pattern.compile("[\\s\\S]*\\salter\\s[\\s\\S]*");

    /**
     * 正则匹配诸如：prepare xxx
     **/
    Pattern SQL_PATTERN_START_PREPARE = Pattern.compile("^prepare\\s[\\s\\S]*");

    /**
     * 正则匹配诸如：xxx prepare xxx
     **/
    Pattern SQL_PATTERN_CONTAINS_PREPARE = Pattern.compile("[\\s\\S]*\\sprepare\\s[\\s\\S]*");

    /**
     * 正则匹配诸如：declare xxx
     **/
    Pattern SQL_PATTERN_START_DECLARE = Pattern.compile("^declare\\s[\\s\\S]*");

    /**
     * 正则匹配诸如：xxx declare xxx
     **/
    Pattern SQL_PATTERN_CONTAINS_DECLARE = Pattern.compile("[\\s\\S]*\\sdeclare\\s[\\s\\S]*");

    /**
     * 不允许SQL中出现的关键
     **/
    List<Pattern> SQL_EXCLUDE_KEYWORDS = Arrays.asList(
//            SQL_PATTERN_START_CREATE,
//            SQL_PATTERN_START_INSERT,
//            SQL_PATTERN_START_UPDATE,
//            SQL_PATTERN_START_DELETE,
//            SQL_PATTERN_START_DROP,
//            SQL_PATTERN_START_ALTER,

            SQL_PATTERN_CONTAINS_CREATE,
            SQL_PATTERN_CONTAINS_INSERT,
            SQL_PATTERN_CONTAINS_UPDATE,
            SQL_PATTERN_CONTAINS_DELETE,
            SQL_PATTERN_CONTAINS_DROP,
            SQL_PATTERN_CONTAINS_ALTER,
            SQL_PATTERN_CONTAINS_PREPARE,
            SQL_PATTERN_CONTAINS_DECLARE
    );

}
