
# 1.在线体验
**在线体验暂时下线**

# 2.目录说明
```
.
├── app
│   ├── flutter
│   └── uni-app
├── doc                                           文档源码
│   ├── docs
│   ├── package.json
│   └── README.md
├── gaea-modules                                  盖亚组件库
│   ├── pom.xml                                   gaea父pom，jar版本管理
│   ├── README.md
│   ├── spring-boot-gaea                          gaea底层库，包含crud、国际化等
│   ├── spring-boot-starter-gaea-archiver         数据库归档组件
│   ├── spring-boot-starter-gaea-archiver-test    数据库归档组件测试示例
│   ├── spring-boot-starter-gaea-export           导出excel、pdf组件
│   ├── spring-boot-starter-gaea-flowable         flowable工作流组件
│   ├── spring-boot-starter-gaea-log              操作日志
│   ├── spring-boot-starter-gaea-push             邮件、钉钉、阿里|极光|安吉短信推送
│   └── spring-boot-starter-gaea-security         权限组件
├── LICENSE
├── README.md
├── gaea-project-template                         spring-cloud工程模板
│   ├── gaea-refer-doc                                 项目初始化配置及说明
│   ├── template-gaea-auth                             权限服务
│   ├── template-gaea-business                         业务服务
│   ├── template-gaea-common                           基础工具包
│   ├── template-gaea-gateway                          网关
│   ├── template-gaea-generator                        前后端代码生成
│   ├── template-gaea-ui                               前端vue工程
│   ├── pom.xml
│   └── README.md                                 

```
# 3.功能介绍
![功能介绍](./gaea-project/template-ui/public/introduce/introduce.jpg)

# 4.后端架构
![后端架构](./gaea-project/template-ui/public/introduce/backkend.png)

# 5.前端架构
![前端架构](./gaea-project/template-ui/public/introduce/frontkend.png)

# 6.工程模板
![前端架构](./gaea-project/template-ui/public/introduce/template.png)

# 7.开发手册
[开发手册](./doc/docs/guide/devdoc.md)
