# 介绍
---
**名字的由来**

盖亚--是古希腊神话中的大地女神，众神之母。具有无穷的创造力。

**盖亚介绍**

盖亚（Gaea）是一个JavaEE微服务架构平台，采用经典组合（SpringBoot，SpringCloud，Vue，ElementUI）。目前提供两种架构，单体架构和微服务架构。
内置的基础功能包括组织机构，权限管理（*用户*，*角色*，*菜单*等），系统管理（*数组字典*，*参数维护*，*操作日志*等）,消息管理，组件中心（*高级查询/自定义列示例*），导出中心

**功能架构**
- 前端
![前端架构](https://gaea.anji-plus.com/introduce/frontkend.png)

- 后端
![后端架构](https://gaea.anji-plus.com/introduce/backkend.png)

**在线体验**
- 访问地址
<https://gaea.anji-plus.com>
- 默认账号密码
admin/p@ss1234

**联系我们**



