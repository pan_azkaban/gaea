
package com.anjiplus.template.gaea.business.modules.alipayconfig.service.impl;

import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.anjiplus.template.gaea.business.modules.alipayconfig.dao.entity.AlipayConfig;
import com.anjiplus.template.gaea.business.modules.alipayconfig.service.AlipayConfigService;
import com.anjiplus.template.gaea.business.modules.alipayconfig.dao.AlipayConfigMapper;
/**
* @desc AlipayConfig 支付配置服务实现
* @author dev-user
* @date 2021-04-07 14:58:28.131
**/
@Service
public class AlipayConfigServiceImpl implements AlipayConfigService {

    @Autowired
    private AlipayConfigMapper alipayConfigMapper;

    @Override
    public GaeaBaseMapper<AlipayConfig> getMapper() {
      return alipayConfigMapper;
    }

    @Override
    public AlipayConfig getDetail(Long id) {
        AlipayConfig alipayConfig = this.selectOne(id);
        return alipayConfig;
    }
}