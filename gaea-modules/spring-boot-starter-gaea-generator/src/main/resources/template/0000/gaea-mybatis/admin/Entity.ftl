
package ${package}.dao.entity;

import lombok.Data;
import io.swagger.annotations.ApiModelProperty;

import com.anji.plus.gaea.curd.entity.GaeaBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;

<#if isNotNullColumns??>
import javax.validation.constraints.*;
</#if>
<#if hasDateAnnotation>
</#if>
<#if hasTimestamp>
import java.sql.Timestamp;
</#if>
<#if hasBigDecimal>
import java.math.BigDecimal;
</#if>

<#if hasSubEntity>
import java.util.List;
import com.baomidou.mybatisplus.annotation.TableField;

<#if subEntity??>
<#list subEntity as s>
import ${basePackage}.${s.instance?lower_case}.dao.entity.${s.className};
</#list>
</#if>

</#if>
/**
* @description ${apiAlias} entity
* @author ${author}
* @date ${date}
**/
@TableName(value="${tableName}")
@Data
public class ${className} extends GaeaBaseEntity {
<#if columns??>
    <#list columns as column>
    <#if "id,createTime,createBy,updateBy,updateTime,version"?index_of('${column.changeColumnName}')<0>
    <#if column.remark != ''>
    @ApiModelProperty(value = "${column.remark}")
    <#else>
    @ApiModelProperty(value = "${column.changeColumnName}")
    </#if>
    private ${column.columnType} ${column.changeColumnName};

    </#if>
    </#list>
    <#if hasSubEntity>
        <#if subEntity??>
            <#list subEntity as s>
    @TableField(exist = false)
    private List<${s.className}> ${s.instance}s;

            </#list>
        </#if>
    </#if>
</#if>

<#--    public void copy(${className} source){-->
<#--        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));-->
<#--    }-->
}