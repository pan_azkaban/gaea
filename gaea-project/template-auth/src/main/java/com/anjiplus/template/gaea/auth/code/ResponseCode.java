package com.anjiplus.template.gaea.auth.code;

/**
 * 系统错误码常量
 *
 * @Author: lide
 * @Date: 2022/4/8 10:52
 */
public class ResponseCode {
    // #### javax.validation 校验注解对应的错误码信息 start ####
    String NULL = "Null";
    String NOT_NULL = "NotNull";
    String NOT_BLANK = "NotBlank";
    String NOT_EMPTY = "NotEmpty";
    String ASSERT_TRUE = "AssertTrue";
    String ASSERT_FALSE = "AssertFalse";
    String POSITIVE = "Positive";
    String POSITIVE_OR_ZERO = "PositiveOrZero";
    String NEGATIVE = "Negative";
    String NEGATIVE_OR_ZERO = "NegativeOrZero";
    String EMAIL = "Email";
    String PAST = "Past";
    String FUTURE = "Future";
    String PAST_OR_PRESENT = "PastOrPresent";
    String FUTURE_OR_PRESENT = "FutureOrPresent";
    String DECIMAL_MAX = "DecimalMax";
    String DECIMAL_MIN = "DecimalMin";
    String MAX = "Max";
    String MIN = "Min";
    String SIZE = "Size";
    String DIGITS = "Digits";
    String PATTERN = "Pattern";
    // #### javax.validation 校验注解对应的错误码信息  end  ####
}
