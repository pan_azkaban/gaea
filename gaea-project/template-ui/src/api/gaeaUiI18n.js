import request from '@/utils/request'
import i18n from '@/lang'
const $i18n = i18n
export function add(data) {
  return request({
    url: 'business/gaeaUiI18n',
    method: 'post',
    data,
  })
}

export function del(ids) {
  return request({
    url: 'business/gaeaUiI18n/' + ids,
    method: 'delete',
    data: ids,
  })
}

export function edit(data) {
  return request({
    url: 'business/gaeaUiI18n',
    method: 'put',
    data,
  })
}

export function preview(data) {
  return request({
    url: 'business/gaeaUiI18n/' + data.id,
    method: 'get',
    params: data,
  })
}

export function listI18nFields(data) {
  if (!data.hasOwnProperty('locale')) {
    data.locale = $i18n.locale
    console.log('getI18nFields-req:' + JSON.stringify(data))
  }
  return request({
    url: 'business/gaeaUiI18n/listI18nFields',
    method: 'post',
    data,
  })
}

/** *
 * 单表扫描 国际化配置
 * @param data
 */
export function scanI18n(data) {
  return request({
    url: 'business/gaeaUiI18n/scan',
    method: 'post',
    params: data,
  })
}

export function getTables() {
  return request({
    url: 'business/gaeaUiI18n/getTables',
    method: 'get',
    params: {},
  })
}

export default { add, edit, del, preview }
