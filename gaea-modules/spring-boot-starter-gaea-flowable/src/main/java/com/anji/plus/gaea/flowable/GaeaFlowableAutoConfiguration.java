package com.anji.plus.gaea.flowable;

import com.anji.plus.gaea.constant.GaeaConstant;
import org.flowable.idm.api.User;
import org.flowable.idm.engine.impl.persistence.entity.UserEntityImpl;
import org.flowable.ui.common.security.SecurityUtils;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 工作流组件组件
 *
 * @author lr
 * @since 2021-01-15
 */
@Configuration
@ComponentScan(value = {"com.anji.plus.gaea.flowable"})
public class GaeaFlowableAutoConfiguration {

    /**
     * 注册用户名的security上下文里
     * @return
     */
    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();

        filterRegistrationBean.setName("addFlowableUserFilter");
        filterRegistrationBean.setFilter((request, response, chain) -> {

            User user = new UserEntityImpl();
            user.setId(GaeaConstant.SUPER_USER_NAME);
            SecurityUtils.assumeUser(user);
            chain.doFilter(request, response);
        });

        filterRegistrationBean.addUrlPatterns("/*");
        filterRegistrationBean.setOrder(Integer.MIN_VALUE + 1);

        return filterRegistrationBean;
    }
}
