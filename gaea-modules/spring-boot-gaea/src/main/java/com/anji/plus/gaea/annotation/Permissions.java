package com.anji.plus.gaea.annotation;


import java.lang.annotation.*;


/**
 * 配置权限
 * @author lr
 * @since 2021-01-12
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Permissions {

    /**
     * 权限
     * @return
     */
    Permission[] value();
}
