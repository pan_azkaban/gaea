package com.anji.plus.gaea.push.type.wechat;

import cn.binarywang.wx.miniapp.config.impl.WxMaDefaultConfigImpl;
import com.anji.plus.gaea.push.GaeaPushProperties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import static com.anji.plus.gaea.constant.GaeaConstant.COMPONENT_PREFIX;

/**
 * 短信属性配置
 *
 * @author lr
 * @since 2021-02-07
 */
@ConfigurationProperties(prefix = GaeaPushWeChatMiniAppProperties.COMPONENT_MAIL_NAME)
@Data
public class GaeaPushWeChatMiniAppProperties extends WxMaDefaultConfigImpl {
    /**
     * 组件名称
     */
    public final static String COMPONENT_MAIL_NAME = COMPONENT_PREFIX + GaeaPushProperties.COMPONENT_NAME + ".miniapp";

    private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;
}
