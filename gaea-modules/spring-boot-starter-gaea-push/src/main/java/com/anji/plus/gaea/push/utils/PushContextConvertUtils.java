package com.anji.plus.gaea.push.utils;

import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @ClassName PushContextConvertUtils
 * @Description: TODO
 * @Author dingkaiqiang
 * @Date 2021-04-06
 * @Version V1.0
 **/
public class PushContextConvertUtils {
    private static final Pattern NAMES_PATTERN = Pattern.compile("\\{([^/]+?)\\}");
    private static final Pattern LIST_PATTERN = Pattern.compile("\\[([^/]+?)\\]");
    private static final Pattern MAP_KEY_PATTERN = Pattern.compile("\\: ([^/]+?)\\ :");

    public static String convert(String context, Map<String, Object> paramMap){
        Matcher matcher = NAMES_PATTERN.matcher(context);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            String match = matcher.group(1);
            String varName = getVariableName(match);
            Object varValue = paramMap.get(varName);
            String formatted = getVariableValueAsString(varValue);
            formatted =  Matcher.quoteReplacement(formatted);
            matcher.appendReplacement(sb, formatted);
        }
        matcher.appendTail(sb);
        return sb.toString();
    }
    public static String convertList(String context, Map<String, Object> paramMap){
        Matcher matcher = LIST_PATTERN.matcher(context);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            String match = matcher.group(1);
            String varName = getListVariableName(match);

            Object varValue = paramMap.get(varName);
            String formatted;
            if (varValue instanceof List) {
                formatted = getVariableListValueAsString((List<Map<String, String>>) varValue);
            } else {
                formatted = getVariableValueAsString(varValue);
            }
            formatted =  Matcher.quoteReplacement(formatted);
            matcher.appendReplacement(sb, formatted);
        }
        matcher.appendTail(sb);
        String s = sb.toString();
        String convert = convert(s, paramMap);
        System.out.println(convert);
        return convert;

    }

    private static String getVariableName(String match) {
        int colonIdx = match.indexOf(':');
        return (colonIdx != -1 ? match.substring(0, colonIdx) : match);
    }

    private static String getListVariableName(String match) {

        Matcher m = MAP_KEY_PATTERN.matcher(match);
        String res = "";
        while(m.find()) {
            res =  m.group(1);
            break;
        }
        return res;
    }
    private static String getVariableValueAsString(@Nullable Object variableValue) {
        return (variableValue != null ? variableValue.toString() : "");
    }


    public static String context = "### 告警时间：{sendTime}\n" +
            "### 告警名称：{alertName}\n" +
            "### 告警等级：{alertlevel}\n" +
            "### 告警项目：{projectName}\n" +
            "### 告警信息\n" +
            "[listbegin : alertInfoList : alertInfo }\n" +
            "#### 时间: {alertInfo.alertTime}\n" +
            "#### 维度: {alertInfo.alertInfoDmensions}\n" +
            "#### 指标: {alertInfo.alertInfo}\n" +
            " listend ]\n\n" +
            "本邮件由推送系统发送，请勿回复";
//    public static void main(String[] args) {
//
//        Map<String, Object> paramMap = getMap();
//        convertList(context, paramMap);
//
//    }

    private static String getVariableListValueAsString(List<Map<String, String>> list) {
        StringBuffer sb = new StringBuffer();

        for (Map<String, String> map : list) {
            map.forEach((key, value) -> {
                sb.append(key).append(": ").append(value).append("\n");
            });
        }
        sb.delete(sb.length() - 1, sb.length());
        return sb.toString();
    }

    private static Map<String, Object> getMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("projectName", "测试001");
        map.put("alertName", "阈值告警");
        map.put("alertlevel", "一级警告");
        map.put("sendTime", "2021-04-08");
        List<Map<String, String>> list = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            Map<String, String> maps = new HashMap<>();
            maps.put("####指标", "阈值200" + i);
            maps.put("####维度", "智能报警" + i);
            maps.put("####时间", "2021-04-08 ：0" + i);
            list.add(maps);
        }
        map.put("alertInfoList", list);
        return map;
    }

}
