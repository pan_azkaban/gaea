package com.anji.plus.gaea.annotation;


import java.lang.annotation.*;


/**
 * 配置权限
 * @author lr
 * @since 2021-01-12
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Repeatable(Permissions.class)
public @interface Permission {

    /**
     * 权限名称
     * @return
     */
    String name() default "";

    /**
     * 权限编号
     * @return
     */
    String code() default "";

    /**
     * 父编码
     * @return
     */
    String superCode() default "";

    /***
     * 权限码默认启用，某些通用权限码 扫描初始化时禁用
     * @return
     */
    boolean enabled() default true;

    /***
     * 依赖的api清单，如GET#/ab/c,POST#/de/f/c,用户授权后，对应api清单自动授权
     * @return
     */
    String dependsOn() default "";
}
