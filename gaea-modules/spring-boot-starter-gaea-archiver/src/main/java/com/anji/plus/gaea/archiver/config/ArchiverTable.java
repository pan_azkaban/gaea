package com.anji.plus.gaea.archiver.config;

import java.util.List;

/**
 * 归档组件配置项
 *
 * @author 木子李·De
 * @since 2021/2/3 14:16
 */
public class ArchiverTable {

    /** 要归档的表名 */
    private String tablename;

    /** 归档依据的时间列名 */
    private String timefield;

    /** 归档多久之前的数据，将n天前的数据，移动到归档表 */
    private Integer maxDaysBeforeArchive;

    /** 删除多久之前的历史数据，归档表中，已归档的数据保留期限，超过期限的数据将删除。*/
    private Integer maxDaysBeforeDelete;

    /** 要归档的库名 */
    private String dbName;

    /** dump 归档host */
    private String dumpHost;

    /** dump 归档port */
    private String dumpPort;

    /** dump 归档用户 */
    private String dumpUser;

    /** dump 归档密码 */
    private String dumpPasswd;

    /** dump 归档本地路径 */
    private String dumpFile;

    /** dump 归档本地路径 */
    private List<String> dumpTableList;

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public List<String> getDumpTableList() {
        return dumpTableList;
    }

    public void setDumpTableList(List<String> dumpTableList) {
        this.dumpTableList = dumpTableList;
    }

    public String getDumpHost() {
        return dumpHost;
    }

    public void setDumpHost(String dumpHost) {
        this.dumpHost = dumpHost;
    }

    public String getDumpPort() {
        return dumpPort;
    }

    public void setDumpPort(String dumpPort) {
        this.dumpPort = dumpPort;
    }

    public String getDumpUser() {
        return dumpUser;
    }

    public void setDumpUser(String dumpUser) {
        this.dumpUser = dumpUser;
    }

    public String getDumpPasswd() {
        return dumpPasswd;
    }

    public void setDumpPasswd(String dumpPasswd) {
        this.dumpPasswd = dumpPasswd;
    }

    public String getDumpFile() {
        return dumpFile;
    }

    public void setDumpFile(String dumpFile) {
        this.dumpFile = dumpFile;
    }

    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

    public String getTimefield() {
        return timefield;
    }

    public void setTimefield(String timefield) {
        this.timefield = timefield;
    }

    public Integer getMaxDaysBeforeArchive() {
        return maxDaysBeforeArchive;
    }

    public void setMaxDaysBeforeArchive(Integer maxDaysBeforeArchive) {
        this.maxDaysBeforeArchive = maxDaysBeforeArchive;
    }

    public Integer getMaxDaysBeforeDelete() {
        return maxDaysBeforeDelete;
    }

    public void setMaxDaysBeforeDelete(Integer maxDaysBeforeDelete) {
        this.maxDaysBeforeDelete = maxDaysBeforeDelete;
    }
}
