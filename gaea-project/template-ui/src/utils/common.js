/*
 * @Author: zyk
 * @Date: 2021-03-09 10:55:35
 * @Last Modified by: zyk
 * @Last Modified time: 2021-03-15 14:58:41
 *
 * 全局公用变量
 */
import Vue from 'vue'

// // 引入 Avue
// import Avue from '@smallwei/avue'
// import '@smallwei/avue/lib/index.css'
// Vue.use(Avue)

// import AvueFormDesign from '@sscfaith/avue-form-design'
// Vue.use(AvueFormDesign)

// // 如果要使用富文本字段
// import AvueUeditor from 'avue-plugin-ueditor'
// Vue.use(AvueUeditor)

// 删除按钮组件
import DeleteBtn from '@/components/BtnCommon/DeleteBtn'
Vue.component('DeleteBtn', DeleteBtn)

// 有权限的按钮组件
import PermissionBtn from '@/components/PermissionBtn/index'
Vue.component('PermissionBtn', PermissionBtn)

import UploadExcel from '@/components/UploadExcel'
Vue.component('UploadExcel', UploadExcel)

// 启用禁用抽成全局变量
import { dataDictionary } from '@/api/common'
import i18n from '@/lang' // 国际化
async function getEnableDict() {
  Vue.prototype.$enableDict = [
    { id: 0, text: i18n.t('businessGlossary.enabled0') },
    { id: 1, text: i18n.t('businessGlossary.enabled1') },
  ]
  // try {
  //   const { code, data } = await dataDictionary('ENABLE_FLAG')
  //   if (code != '200') return
  //   // 字符串类型的id转为数字类型，解决回显问题
  //   Vue.prototype.$enableDict = data.map((item) => {
  //     return { id: Number(item.id), text: item.text }
  //   })
  // } catch (err) {
  //   console.error(err)
  // }
}
getEnableDict()

// 分页的全局size配置;
Vue.prototype.$pageSizeAll = [10, 50, 100, 200, 500]
