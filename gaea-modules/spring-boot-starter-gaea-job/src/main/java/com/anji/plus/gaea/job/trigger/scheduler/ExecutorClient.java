package com.anji.plus.gaea.job.trigger.scheduler;

import com.anji.plus.gaea.job.core.dto.LogResult;
import com.anji.plus.gaea.job.core.dto.ReturnT;
import com.anji.plus.gaea.job.core.param.KillParam;
import com.anji.plus.gaea.job.core.param.LogParam;
import com.anji.plus.gaea.job.core.param.TriggerParam;
import com.anji.plus.gaea.job.core.util.XxlJobRemotingUtil;

/**
 * Created by xuxueli on 17/3/10.
 *
 * Borrowed from xxljob v2.4.0
 */
public class ExecutorClient {

    /** 执行器地址，http://ip:port/contextpath/ */
    private String executorAddress;

    /** 执行器访问令牌 */
    private String accessToken;

    private int timeout = 3;

    public ExecutorClient(String executorAddress, String accessToken) {
        if(executorAddress.endsWith("/")){
            this.executorAddress = executorAddress + "jobexecutor/";
        }else{
            this.executorAddress = executorAddress + "/jobexecutor/";
        }
        this.accessToken = accessToken;
    }


    public ReturnT<String> beat() {
        return XxlJobRemotingUtil.postBody(executorAddress+"beat", accessToken, timeout, "");
    }

    public ReturnT<String> idleBeat(Long jobId){
        return XxlJobRemotingUtil.postBody(executorAddress+"idleBeat", accessToken, timeout, jobId);
    }

    public ReturnT<String> run(TriggerParam triggerParam) {
        return XxlJobRemotingUtil.postBody(executorAddress + "run", accessToken, timeout, triggerParam);
    }

    public ReturnT<String> kill(KillParam killParam) {
        return XxlJobRemotingUtil.postBody(executorAddress + "kill", accessToken, timeout, killParam);
    }

    public ReturnT<LogResult> log(LogParam logParam) {
        return XxlJobRemotingUtil.postBody(executorAddress + "log", accessToken, timeout, logParam, LogResult.class);
    }
}
