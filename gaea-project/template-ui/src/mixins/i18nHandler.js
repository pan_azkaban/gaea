import { listI18nFields } from '@/api/gaeaUiI18n'
import { getLanguage } from '@/lang/index'
import i18n from '@/lang'
const $i18n = i18n
export default {
  data() {
    return {
      i18nQry: {
        cataType: null,
        system: null,
        module: null,
        refer: null,
      },
    }
  },
  methods: {
    /** 菜单加载-初始化接口*/
    moduleI18nInit(req) {
      req.locale = getLanguage()
      this.i18nQry = req
      listI18nFields(req).then((d) => {
        if (d.code == '200') {
          // console.log("当前module-i18n:"+JSON.stringify(req)+"\n" + JSON.stringify(d.data))
          $i18n.mergeLocaleMessage(d.data.locale, d.data)
          // console.log(JSON.stringify($i18n.messages[req.locale]))
        }
      })
    },
    /**
     * 字段翻译入口
     * 加载国际化，菜单级别的配置通过tf('key')转换，全局的页面配置通过$t('key')方式转换
     */
    tf(key, args) {
      return $i18n.t(this.i18nQry.module + '.' + key, args)
    },
  },
}
