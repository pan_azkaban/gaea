package com.anji.plus.gaea.annotation;

import java.lang.annotation.*;

/**
 * 详情标记
 * @author wongbin
 * @date 2021/12/16
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD,ElementType.TYPE_PARAMETER,ElementType.METHOD})
@Documented
public @interface DetailView {

}
