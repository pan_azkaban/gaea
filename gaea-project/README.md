
## 目录结构说明
```
gaea-project                      spring-cloud工程模板
├── template-auth                 权限服务
├── template-business             业务服务
├── template-common               基础工具包
├── template-gateway              网关
├── template-generator            代码生成器
├── template-generator-example    代码生成示例
├── template-ui                   前端vue
├── doc                           mysql脚本、nacos配置、项目创建文档
├── pom.xml                       
├── README.md                  
```
- [快速了解](https://gitee.com/anji-plus/gaea/blob/master/doc/docs/guide/quickly.md)
- [开发手册](https://gitee.com/anji-plus/gaea/blob/master/doc/docs/guide/devdoc.md)

- [生成项目模板-操作步骤](./doc/mvn-project-init.md)

- [生成项目模板-命令](./create-gaea-archetype.sh)

- [新项目框架代码初始化](https://gitee.com/anji-plus/gaea/blob/master/doc/docs/guide/quickly.md#7-%E6%96%B0%E9%A1%B9%E7%9B%AE%E5%88%9D%E5%A7%8B%E5%8C%96)

- [代码生成示例菜单-db配置](doc/gaea-db/gaea_business/gaea_generator-DDL-DML.sql)

- [ui国际化db配置](./doc/gaea-db/gaea_business/gaea_business.gaea_ui_i18n-DML.sql)