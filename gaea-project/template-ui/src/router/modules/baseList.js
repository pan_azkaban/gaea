/*
 * @Author: zyk
 * @Date: 2020-07-13 15:34:51
 * @Last Modified by: zyk
 * @Last Modified time: 2021-03-15 18:06:57
 */

/** 脚手架基础页面路由 **/
export const baseList = [
  // 组织机构
  {
    name: 'org', // 菜单代码 && 路由名称              （必填）
    sort: '10', // 菜单排序
    parentCode: '', // 父级菜单代码               （有父级时必填）
    path: '/organization', // 路由                    （必填）
    component: '', // 组件地址从相对view文件夹的路径  (页面级时必填)
    redirect: '/organization/index', // 默认的重定向路径
    alwaysShow: 0, // 默认情况下只有一个子级菜单的时候不展示父级菜单
    meta: {
      title: '组织机构', // 菜单名
      icon: 'organization', // svg图标
    },
    children: [
      {
        sort: '1010',
        path: 'index',
        component: 'organization/index',
        name: 'organization',
        meta: {
          title: '组织机构',
          icon: 'organization',
        },
      },
    ],
  },
  // 权限管理
  {
    sort: '20',
    path: '/authority',
    redirect: '/authority/role',
    name: 'authority',
    alwaysShow: true,
    meta: {
      title: '权限管理',
      icon: 'authority',
    },
    children: [
      // {
      //   sort: '2010',
      //   path: 'btn-config',
      //   component: 'authority/btn-config',
      //   name: 'btnConfig',
      //   meta: {
      //     title: 'btnConfig',
      //     icon: '',
      //   },
      // },
      {
        sort: '2010',
        path: 'permission',
        component: 'authority/permission',
        name: 'permission',
        meta: {
          title: '权限列表',
          icon: '',
        },
      },
      {
        sort: '2020',
        path: 'menu',
        component: 'authority/menu/index',
        name: 'menuConfig',
        meta: {
          title: '菜单配置',
          icon: '',
        },
      },
      {
        sort: '2030',
        path: 'menu-detail',
        hidden: 1,
        component: 'authority/menu/menu-detail',
        name: 'menuDetail',
        meta: {
          title: '页面配置',
          icon: '',
        },
      },
      {
        sort: '2040',
        path: 'role',
        component: 'authority/role/index',
        name: 'role',
        meta: {
          title: '角色管理',
          icon: '',
        },
      },
      {
        sort: '2050',
        path: 'user',
        component: 'authority/user/index',
        name: 'user',
        meta: {
          title: '用户管理',
          icon: '',
        },
      },
    ],
  },
]
