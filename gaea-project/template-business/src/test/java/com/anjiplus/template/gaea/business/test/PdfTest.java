package com.anjiplus.template.gaea.business.test;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.anji.plus.gaea.export.vo.ExportOperation;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;


public class PdfTest {

    @Test
    public void testCreatePdf() {

        try {
            // 1. new Document
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("D:\\aaa.pdf"));
            // 2. 打开document
            document.open();
            // 3. 添加内容
            document.add(new Paragraph("hello world！"));
            // 4. 关闭 (如果未关闭则会生成无效的pdf文件)
            document.close();
        } catch (DocumentException ex) {
            ex.printStackTrace();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testChineseFontPdf() {
        try {
            //1. new document
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("D:\\aaa.pdf"));
            //2. open document
            document.open();
            BaseFont bf = BaseFont.createFont(path() + "fonts/SIMKAI.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            //3. 注册字体
            Font font = new Font(bf, 30);
            //3. 添加段落,并设置字体
            document.add(new Paragraph("hello world(中文,)", font));
            //4. close document
            document.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (DocumentException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 获取资源的路径
     *
     * @return
     */
    private String path() {
        String path = this.getClass().getResource("/").getPath();
        return path;
    }

    /**
     * 生成表格
     */
    @Test
    public void testTablePdf() {
        try {
            //1.new document
            Document document = new Document(PageSize.A0);
            PdfWriter.getInstance(document, new FileOutputStream("D:\\aaa.pdf"));
            //2 open document
            document.open();
            //3.添加pdf tables 3表示列数，
            PdfPTable pdfPTable = new PdfPTable(3);
            // cell表示单元格,(12表示12个单元格,3列，12个单元格就形成来一个4行3列的表格)
            for (int i = 0; i < 12; i++) {
                pdfPTable.addCell("cell" + i);
            }
            document.add(pdfPTable);
            //4. 关闭document
            document.close();
        } catch (DocumentException ex) {
            ex.printStackTrace();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 生成image
     */
    @Test
    public void testImagePdf() {
        try {
            //1.new document
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("D:\\aaa.pdf"));
            //2 open document
            document.open();
            document.add(new Paragraph("Hello world!"));
            //3.添加image
            Image image = Image.getInstance("D:\\原图.png");
            image.scaleAbsolute(PageSize.A4.rotate());
            document.add(image);
            //4. 关闭document
            document.close();
        } catch (DocumentException ex) {
            ex.printStackTrace();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    /**
     * 生成带样式的表格
     */
    @Test
    public void testTableStylePdf() {
        try {
            JSONObject json = getData2();

            BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
            //1.new document
            Document document = new Document(PageSize.A3);
            PdfWriter.getInstance(document, new FileOutputStream("D:\\aaa.pdf"));
            //2 open document
            document.open();
            //3.添加pdf tables 3表示列数，
            // cell表示单元格,(12表示12个单元格,3列，12个单元格就形成来一个4行3列的表格)
            JSONObject rows = json.getJSONObject("rows");
            JSONArray styles = json.getJSONArray("styles");
            JSONObject cols = json.getJSONObject("cols");

            Integer defaultWidth = rows.getInteger("len");
            Integer defaultHeigth = 25;
            rows.remove("len");
            //需要得到最多的列数
            AtomicInteger colSize = new AtomicInteger();
            rows.forEach((row, col) -> {
                JSONObject colJson = JSONObject.parseObject(col.toString(), Feature.OrderedField);
                JSONObject cells = colJson.getJSONObject("cells");
                int size = cells.size();
                if (colSize.get() < size) {
                    colSize.set(size);
                }
            });

            AtomicReference<String> colNumber = new AtomicReference<>("0");
            rows.forEach((row, col) -> colNumber.set(row));
            PdfPTable pdfPTable = new PdfPTable(colSize.get());
            float[] tableWidthArray = new float[colSize.get()];
            for (int i = 0; i < colSize.get(); i++) {
                if (cols.containsKey(String.valueOf(i))) {
                    Integer width = cols.getJSONObject(String.valueOf(i)).getInteger("width");
                    tableWidthArray[i] = width;
                } else {
                    tableWidthArray[i] = defaultWidth;
                }
            }
            try {
                pdfPTable.setWidths(tableWidthArray);
            } catch (DocumentException e) {
                e.printStackTrace();
            }
            Map<Integer, Set<Integer>> colspanMap = new HashMap<>();
            for (int j = 0; j < Integer.parseInt(colNumber.get()); j++) {
                if (rows.containsKey(String.valueOf(j))) {
                    JSONObject colJson = rows.getJSONObject(String.valueOf(j));
                    JSONObject cells = colJson.getJSONObject("cells");
                    Integer height = colJson.getInteger("height");

                    for (int i = 0; i < colSize.get(); i++) {
                        boolean flag = true;
                        if (colspanMap.containsKey(j)) {
                            Set<Integer> integer = colspanMap.get(j);
                            if (integer.contains(i)) {
                                flag = false;
                            }
                        }
                        if (flag) {
                            if (cells.containsKey(String.valueOf(i))) {
                                if (cells.containsKey(String.valueOf(i))) {

                                    JSONObject cellDetail = cells.getJSONObject(String.valueOf(i));
                                    String text = cellDetail.getString("text");
                                    Integer style = cellDetail.getInteger("style");
                                    JSONArray merge = cellDetail.getJSONArray("merge");
                                    PdfPCell pdfPCell = new PdfPCell();
                                    //行高
                                    pdfPCell.setFixedHeight(null == height ? defaultHeigth : height);
                                    String color = null;
                                    Integer fontSize = 10;
                                    boolean italic = false;
                                    boolean bold = false;
                                    boolean underline = false;
                                    boolean strike = false;
                                    if (null != style) {
                                        JSONObject styleJson = styles.getJSONObject(style);
                                        if (styleJson.containsKey("align")) {
                                            String align = styleJson.getString("align");
                                            pdfPCell.setHorizontalAlignment(getHorizontalAlignment(align));
                                        }
                                        if (styleJson.containsKey("color")) {
                                            //设置单元格字体颜色
                                            color = styleJson.getString("color");
                                        }

                                        if (styleJson.containsKey("bgcolor")) {
                                            //设置单元格背景颜色
                                            String bgcolor = styleJson.getString("bgcolor");
                                            pdfPCell.setBackgroundColor(toColorFromString(bgcolor));
                                        }
                                        if (styleJson.containsKey("border")) {
                                            //设置单元格的边框
                                            JSONObject border = styleJson.getJSONObject("border");
                                            if (border.containsKey("bottom")) {
                                                JSONArray type = border.getJSONArray("bottom");
                                                pdfPCell.setBorderWidthBottom(getBorderwidth(type.getString(0)));
                                                pdfPCell.setBorderColorBottom(toColorFromString(type.getString(1)));
                                            } else {
                                                pdfPCell.disableBorderSide(Rectangle.BOTTOM);
                                            }
                                            if (border.containsKey("top")) {
                                                JSONArray type = border.getJSONArray("top");
                                                pdfPCell.setBorderWidthTop(getBorderwidth(type.getString(0)));
                                                pdfPCell.setBorderColorTop(toColorFromString(type.getString(1)));
                                            } else {
                                                pdfPCell.disableBorderSide(Rectangle.TOP);
                                            }

                                            if (border.containsKey("left")) {
                                                JSONArray type = border.getJSONArray("left");
                                                pdfPCell.setBorderWidthLeft(getBorderwidth(type.getString(0)));
                                                pdfPCell.setBorderColorLeft(toColorFromString(type.getString(1)));
                                            } else {
                                                pdfPCell.disableBorderSide(Rectangle.LEFT);
                                            }

                                            if (border.containsKey("right")) {
                                                JSONArray type = border.getJSONArray("right");
                                                pdfPCell.setBorderWidthRight(getBorderwidth(type.getString(0)));
                                                pdfPCell.setBorderColorRight(toColorFromString(type.getString(1)));
                                            } else {
                                                pdfPCell.disableBorderSide(Rectangle.RIGHT);
                                            }
                                        } else {
                                            //不包含边框，默认全部隐藏
                                            pdfPCell.disableBorderSide(Rectangle.LISTITEM);
                                        }
                                        if (styleJson.containsKey("font")) {
                                            fontSize = styleJson.getJSONObject("font").getInteger("size");
                                            italic = styleJson.getJSONObject("font").getBoolean("italic");
                                            bold = styleJson.getJSONObject("font").getBoolean("bold");
                                        }
                                        if (styleJson.containsKey("underline")) {
                                            //下划线
                                            underline = styleJson.getBoolean("underline");
                                        }
                                        if (styleJson.containsKey("strike")) {
                                            //删除线
                                            strike = styleJson.getBoolean("strike");

                                        }

                                    }
                                    if (StringUtils.isNotBlank(text)) {
                                        Font font = new Font(bfChinese);
                                        if (fontSize > 0) {
                                            font.setSize(fontSize);
                                        }
                                        if (underline) {
                                            font.setStyle(Font.UNDERLINE);
                                        }
                                        if (strike) {
                                            font.setStyle(Font.STRIKETHRU);
                                        }

                                        if (italic && bold) {
                                            font.setStyle(Font.BOLDITALIC);
                                        } else if (bold) {
                                            font.setStyle(Font.BOLD);
                                        } else if (italic) {
                                            font.setStyle(Font.ITALIC);
                                        }else {
                                            font.setStyle(Font.NORMAL);
                                        }


                                        if (StringUtils.isNotBlank(color)) {
                                            font.setColor(toColorFromString(color));
                                        } else {
                                            font.setColor(BaseColor.BLACK);
                                        }
                                        Phrase elements = new Phrase(text, font);
                                        pdfPCell.setPhrase(elements);
                                    } else {
                                        pdfPCell.setPhrase(new Phrase(""));
                                    }
                                    //合并单元格
                                    if (null != merge) {
                                        int colspan = merge.getInteger(1) + 1;
                                        int rowspan = merge.getInteger(0);
                                        pdfPCell.setRowspan(rowspan + 1);
                                        pdfPCell.setColspan(colspan);

                                        if (colspan > 0) {
                                            Set<Integer> set;
                                            if (colspanMap.containsKey(j)) {
                                                set = colspanMap.get(j);
                                            } else {
                                                set = new HashSet<>();
                                            }
                                            for (int i1 = 0; i1 < colspan; i1++) {
                                                set.add(i + i1);
                                            }
                                            colspanMap.put(j, set);
                                        }
                                        if (rowspan > 0) {
                                            for (int i1 = 1; i1 <= rowspan; i1++) {
                                                for (int i2 = 0; i2 < colspan; i2++) {
                                                    Set<Integer> set;
                                                    if (colspanMap.containsKey(j + i1)) {
                                                        set = colspanMap.get(j + i1);
                                                    } else {
                                                        set = new HashSet<>();
                                                    }
                                                    set.add(i + i2);
                                                    colspanMap.put(j + i1, set);
                                                }
                                            }
                                        }
                                    }
                                    pdfPTable.addCell(pdfPCell);

                                } else {
                                    pdfPTable.addCell(createEmptyCell());
                                }
                            } else {
                                pdfPTable.addCell(createEmptyCell());
                            }
                        }

                    }

                } else {
                    pdfPTable.addCell(createEmptyCell());

                }
            }
            document.add(pdfPTable);
            //4. 关闭document
            document.close();
            System.out.println("导出成功！！！");
        } catch (DocumentException ex) {
            ex.printStackTrace();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void pdf(){
        ExportOperation exportOperation = new ExportOperation();
        exportOperation.setJsonStr(getData1());
        exportOperation.setFilePath("D:\\aa.pdf");
        //GaeaReportPdfUtil.pdfExportByFilePath(exportOperation);

    }


    /**
     * 字符串转换成Color对象
     *
     * @param colorStr 16进制颜色字符串
     * @return Color对象
     */
    public BaseColor toColorFromString(String colorStr) {
        if (colorStr.length() < 7) {
            return BaseColor.BLACK;
        }
        int[] rgb = new int[3];
        rgb[0] = Integer.valueOf(colorStr.substring(1, 3), 16);
        rgb[1] = Integer.valueOf(colorStr.substring(3, 5), 16);
        rgb[2] = Integer.valueOf(colorStr.substring(5, 7), 16);
        return new BaseColor(rgb[0], rgb[1], rgb[2]);
    }

    /**
     * 获取表格边框宽度
     * thin      薄
     * medium    中等的
     * thick     厚的
     * dotted    星罗棋布的   暂未处理
     * dashed    虚线  暂未处理
     *
     * @param type
     * @return
     */
    public float getBorderwidth(String type) {
        if ("thin".equals(type)) {
            return 0.5f;
        }
        if ("medium".equals(type)) {
            return 1f;
        }
        if ("thick".equals(type)) {
            return 1.5f;
        }
        if ("dotted".equals(type)) {
            return 1f;
        }
        if ("dashed".equals(type)) {
            return 1f;
        }
        return 0f;
    }

    @Test
    public void testGetData() {
        JSONObject data2 = getData2();
        System.out.println("------------------");
        System.out.println(data2);
    }

    public PdfPCell createEmptyCell() {
        PdfPCell pdfPCell = new PdfPCell();
        //不包含边框，默认全部隐藏
        pdfPCell.disableBorderSide(Rectangle.LISTITEM);
        return pdfPCell;
    }

    public String getData1() {
        String json = "{\"name\":\"盖亚用户信息\",\"freeze\":\"A1\",\"styles\":[{\"align\":\"center\"},{\"border\":{\"top\":[\"thin\",\"#000\"],\"bottom\":[\"thin\",\"#000\"],\"left\":[\"thin\",\"#000\"]}},{\"border\":{\"top\":[\"thin\",\"#000\"],\"bottom\":[\"thin\",\"#000\"]}},{\"border\":{\"top\":[\"thin\",\"#000\"],\"bottom\":[\"thin\",\"#000\"],\"right\":[\"thin\",\"#000\"]}},{\"border\":{\"top\":[\"thin\",\"#000\"],\"left\":[\"thin\",\"#000\"]}},{\"border\":{\"top\":[\"thin\",\"#000\"],\"right\":[\"thin\",\"#000\"]}},{\"border\":{\"bottom\":[\"thin\",\"#000\"],\"left\":[\"thin\",\"#000\"]}},{\"border\":{\"bottom\":[\"thin\",\"#000\"],\"right\":[\"thin\",\"#000\"]}},{\"border\":{\"top\":[\"thin\",\"#000\"],\"left\":[\"thin\",\"#000\"],\"bottom\":[\"thin\",\"#000\"],\"right\":[\"thin\",\"#000\"]}},{\"border\":{\"top\":[\"thin\",\"#000\"],\"left\":[\"thin\",\"#000\"],\"bottom\":[\"thin\",\"#000\"],\"right\":[\"thin\",\"#000\"]},\"align\":\"center\"},{\"border\":{\"top\":[\"thin\",\"#000\"],\"left\":[\"thin\",\"#000\"],\"bottom\":[\"thin\",\"#000\"],\"right\":[\"thin\",\"#000\"]},\"textwrap\":true},{\"textwrap\":true},{\"align\":\"right\"},{\"align\":\"center\",\"font\":{\"size\":14}},{\"font\":{\"size\":14}},{\"border\":{\"right\":[\"thin\",\"#000\"]}},{\"border\":{\"top\":[\"thin\",\"#000\"],\"left\":[\"thin\",\"#000\"],\"bottom\":[\"thin\",\"#000\"],\"right\":[\"thin\",\"#000\"]},\"align\":\"center\",\"valign\":\"top\"},{\"align\":\"center\",\"valign\":\"top\"},{\"border\":{\"top\":[\"thin\",\"#000\"],\"left\":[\"thin\",\"#000\"],\"bottom\":[\"thin\",\"#000\"],\"right\":[\"thin\",\"#000\"]},\"align\":\"center\",\"valign\":\"bottom\"},{\"align\":\"center\",\"valign\":\"bottom\"},{\"border\":{\"top\":[\"thin\",\"#000\"],\"left\":[\"thin\",\"#000\"],\"bottom\":[\"thin\",\"#000\"],\"right\":[\"thin\",\"#000\"]},\"align\":\"center\",\"valign\":\"middle\"},{\"align\":\"center\",\"valign\":\"middle\"},{\"align\":\"center\",\"font\":{\"size\":14},\"valign\":\"middle\"},{\"font\":{\"size\":14},\"valign\":\"middle\"},{\"valign\":\"bottom\"},{\"align\":\"center\",\"font\":{\"size\":14},\"valign\":\"bottom\"},{\"font\":{\"size\":14},\"valign\":\"bottom\"},{\"align\":\"center\",\"font\":{\"size\":14},\"valign\":\"top\"},{\"font\":{\"size\":14},\"valign\":\"top\"},{\"border\":{\"top\":[\"thin\",\"#000\"],\"left\":[\"thin\",\"#000\"],\"bottom\":[\"thin\",\"#000\"],\"right\":[\"thin\",\"#000\"]},\"valign\":\"middle\"},{\"valign\":\"middle\"},{\"border\":{\"top\":[\"thin\",\"#000\"],\"left\":[\"thin\",\"#000\"],\"bottom\":[\"thin\",\"#000\"],\"right\":[\"thin\",\"#000\"]},\"textwrap\":true,\"valign\":\"middle\"},{\"border\":{\"right\":[\"thin\",\"#000\"]},\"valign\":\"middle\"},{\"textwrap\":true,\"valign\":\"middle\"},{\"align\":\"right\",\"valign\":\"middle\"}],\"merges\":[\"A1:D1\",\"B4:D4\",\"B5:D5\",\"A7:A13\",\"A14:A19\",\"A20:A23\",\"A24:A26\",\"B24:D26\",\"A27:D28\",\"B20:D23\",\"B18:D19\"],\"rows\":{\"0\":{\"cells\":{\"0\":{\"merge\":[0,3],\"style\":27,\"text\":\"职工信息表\"},\"1\":{\"style\":28},\"2\":{\"style\":28},\"3\":{\"style\":28},\"4\":{\"style\":0}},\"height\":75},\"1\":{\"cells\":{\"0\":{\"style\":20,\"text\":\"用户名\"},\"1\":{\"style\":29,\"text\":\"管理员\"},\"2\":{\"style\":20,\"text\":\"曾用名\"},\"3\":{\"style\":29,\"text\":\"admin\"}}},\"2\":{\"cells\":{\"0\":{\"style\":16,\"text\":\"邮箱\"},\"1\":{\"style\":29,\"text\":\"admin@163.com\"},\"2\":{\"style\":20,\"text\":\"手机号码\"},\"3\":{\"style\":29,\"text\":\"13324345678\"}},\"height\":53},\"3\":{\"cells\":{\"0\":{\"style\":20,\"text\":\"现居住地\"},\"1\":{\"merge\":[0,2],\"style\":29,\"text\":\"上海市杨浦区江浦路1000号\"},\"2\":{\"style\":30},\"3\":{\"style\":30}}},\"4\":{\"cells\":{\"0\":{\"style\":20,\"text\":\"户口所在地\"},\"1\":{\"merge\":[0,2],\"style\":29,\"text\":\"上海市杨浦区江浦路1000号\"},\"2\":{\"style\":30},\"3\":{\"style\":30}}},\"5\":{\"cells\":{\"0\":{\"style\":20,\"text\":\"所属街道\"},\"1\":{\"style\":29,\"text\":\"xx路xx街道\"},\"2\":{\"style\":20,\"text\":\"身份证号\"},\"3\":{\"style\":29,\"text\":\"3451904234242352566\"}}},\"6\":{\"cells\":{\"0\":{\"merge\":[6,0],\"style\":16,\"text\":\"工作经历\"},\"1\":{\"style\":20,\"text\":\"开始结束年月\"},\"2\":{\"style\":20,\"text\":\"公司名称\"},\"3\":{\"style\":20,\"text\":\"部门及职务\"},\"5\":{\"style\":24}}},\"7\":{\"cells\":{\"0\":{\"style\":17},\"1\":{\"style\":29,\"text\":\"2020.1-至今\"},\"2\":{\"style\":29,\"text\":\"安吉加加\"},\"3\":{\"style\":29,\"text\":\"工程研发部\"}}},\"8\":{\"cells\":{\"0\":{\"style\":17},\"1\":{\"style\":29,\"text\":\"2020.1-至今\"},\"2\":{\"style\":29,\"text\":\"安吉加加\"},\"3\":{\"style\":29,\"text\":\"工程研发部\"}}},\"9\":{\"cells\":{\"0\":{\"style\":17},\"1\":{\"style\":29,\"text\":\"2020.1-至今\"},\"2\":{\"style\":29,\"text\":\"安吉加加\"},\"3\":{\"style\":29,\"text\":\"工程研发部\"}}},\"10\":{\"cells\":{\"0\":{\"style\":17},\"1\":{\"style\":29,\"text\":\"2020.1-至今\"},\"2\":{\"style\":29,\"text\":\"安吉加加\"},\"3\":{\"style\":29,\"text\":\"工程研发部\"}}},\"11\":{\"cells\":{\"0\":{\"style\":17},\"1\":{\"style\":29},\"2\":{\"style\":29},\"3\":{\"style\":29}}},\"12\":{\"cells\":{\"0\":{\"style\":17},\"1\":{\"style\":29},\"2\":{\"style\":29},\"3\":{\"style\":29}}},\"13\":{\"cells\":{\"0\":{\"merge\":[5,0],\"style\":18,\"text\":\"教育经历\"},\"1\":{\"style\":20,\"text\":\"开始结束时间\"},\"2\":{\"style\":20,\"text\":\"院校\"},\"3\":{\"style\":20,\"text\":\"学士学位\"}}},\"14\":{\"cells\":{\"0\":{\"style\":19},\"1\":{\"style\":29,\"text\":\"2020.1-2024.9\"},\"2\":{\"style\":29,\"text\":\"哈佛\"},\"3\":{\"style\":29,\"text\":\"博士\"}}},\"15\":{\"cells\":{\"0\":{\"style\":19},\"1\":{\"style\":29},\"2\":{\"style\":29},\"3\":{\"style\":29}}},\"16\":{\"cells\":{\"0\":{\"style\":19},\"1\":{\"style\":29},\"2\":{\"style\":29},\"3\":{\"style\":29}}},\"17\":{\"cells\":{\"0\":{\"style\":19},\"1\":{\"merge\":[1,2],\"style\":29},\"2\":{\"style\":30},\"3\":{\"style\":30}}},\"18\":{\"cells\":{\"0\":{\"style\":19},\"1\":{\"style\":30},\"2\":{\"style\":30},\"3\":{\"style\":30}}},\"19\":{\"cells\":{\"0\":{\"merge\":[3,0],\"style\":31,\"text\":\"您是否具有、并愿意提供市场化业务渠道\"},\"1\":{\"merge\":[3,2],\"style\":29},\"2\":{\"style\":30},\"3\":{\"style\":32}}},\"20\":{\"cells\":{\"0\":{\"style\":33},\"1\":{\"style\":30},\"2\":{\"style\":30},\"3\":{\"style\":32}}},\"21\":{\"cells\":{\"0\":{\"style\":33},\"1\":{\"style\":30},\"2\":{\"style\":30},\"3\":{\"style\":32}}},\"22\":{\"cells\":{\"0\":{\"style\":33},\"1\":{\"style\":30},\"2\":{\"style\":30},\"3\":{\"style\":32}}},\"23\":{\"cells\":{\"0\":{\"merge\":[2,0],\"style\":31,\"text\":\"希望您提供的信息\"},\"1\":{\"merge\":[2,2],\"style\":29},\"2\":{\"style\":30},\"3\":{\"style\":30}}},\"24\":{\"cells\":{\"0\":{\"style\":33},\"1\":{\"style\":29},\"2\":{\"style\":29},\"3\":{\"style\":29}}},\"25\":{\"cells\":{\"0\":{\"style\":33},\"1\":{\"style\":29},\"2\":{\"style\":29},\"3\":{\"style\":29}}},\"26\":{\"cells\":{\"0\":{\"merge\":[1,3],\"style\":30,\"text\":\"本人承诺，以上信息均属事实，若有不实，本人自愿承担相应后果。\"},\"1\":{\"style\":30},\"2\":{\"style\":30},\"3\":{\"style\":30}}},\"27\":{\"cells\":{\"0\":{\"style\":30},\"1\":{\"style\":30},\"2\":{\"style\":30},\"3\":{\"style\":30}}},\"28\":{\"cells\":{\"0\":{\"style\":30},\"1\":{\"style\":30},\"2\":{\"style\":34,\"text\":\"签名：\"},\"3\":{\"style\":30}}},\"29\":{\"cells\":{\"0\":{\"style\":30},\"1\":{\"style\":30},\"2\":{\"style\":34,\"text\":\"日期：\"},\"3\":{\"style\":30}}},\"len\":60},\"cols\":{\"0\":{\"width\":98},\"1\":{\"width\":164},\"2\":{\"width\":73},\"3\":{\"width\":234},\"len\":52},\"validations\":[],\"autofilter\":{}}";
        // 取第一个sheet

        JSONObject jsonObject = JSONObject.parseObject(json, Feature.OrderedField);
        return json;
    }


    public JSONObject getData2() {
        String json = "{\"name\":\"sheet1\",\"freeze\":\"A1\",\"styles\":[{\"align\":\"center\"},{\"align\":\"center\",\"font\":{\"size\":16}},{\"align\":\"center\",\"format\":\"datetime\"},{\"align\":\"center\",\"format\":\"text\"},{\"format\":\"date\"},{\"format\":\"datetime\"},{\"align\":\"center\",\"format\":\"time\"},{\"format\":\"time\"},{\"align\":\"center\",\"font\":{\"size\":16},\"border\":{\"bottom\":[\"thin\",\"#000\"],\"top\":[\"thin\",\"#000\"],\"left\":[\"thin\",\"#000\"],\"right\":[\"thin\",\"#000\"]}},{\"align\":\"center\",\"border\":{\"bottom\":[\"thin\",\"#000\"],\"top\":[\"thin\",\"#000\"],\"left\":[\"thin\",\"#000\"],\"right\":[\"thin\",\"#000\"]}},{\"align\":\"center\",\"format\":\"time\",\"border\":{\"bottom\":[\"thin\",\"#000\"],\"top\":[\"thin\",\"#000\"],\"left\":[\"thin\",\"#000\"],\"right\":[\"thin\",\"#000\"]}},{\"align\":\"center\",\"border\":{\"bottom\":[\"thin\",\"#000\"],\"top\":[\"thin\",\"#000\"],\"left\":[\"thin\",\"#000\"],\"right\":[\"thin\",\"#000\"]},\"bgcolor\":\"#a7d08c\"},{\"align\":\"center\",\"border\":{\"bottom\":[\"thin\",\"#000\"],\"top\":[\"thin\",\"#000\"],\"left\":[\"thin\",\"#000\"],\"right\":[\"thin\",\"#000\"]},\"bgcolor\":\"#c5e0b3\"},{\"align\":\"center\",\"format\":\"time\",\"border\":{\"bottom\":[\"thin\",\"#000\"],\"top\":[\"thin\",\"#000\"],\"left\":[\"thin\",\"#000\"],\"right\":[\"thin\",\"#000\"]},\"bgcolor\":\"#c5e0b3\"},{\"align\":\"center\",\"font\":{\"size\":16,\"name\":\"Helvetica\"},\"border\":{\"bottom\":[\"thin\",\"#000\"],\"top\":[\"thin\",\"#000\"],\"left\":[\"thin\",\"#000\"],\"right\":[\"thin\",\"#000\"]}},{\"font\":{\"name\":\"Helvetica\"}},{\"format\":\"time\",\"font\":{\"name\":\"Source Sans Pro\"}},{\"align\":\"center\",\"font\":{\"size\":16,\"name\":\"Lato\"},\"border\":{\"bottom\":[\"thin\",\"#000\"],\"top\":[\"thin\",\"#000\"],\"left\":[\"thin\",\"#000\"],\"right\":[\"thin\",\"#000\"]}},{\"font\":{\"name\":\"Lato\"}},{\"align\":\"center\",\"font\":{\"size\":16,\"name\":\"Arial\"},\"border\":{\"bottom\":[\"thin\",\"#000\"],\"top\":[\"thin\",\"#000\"],\"left\":[\"thin\",\"#000\"],\"right\":[\"thin\",\"#000\"]}},{\"font\":{\"name\":\"Arial\"}},{\"format\":\"duration\"},{\"format\":\"eur\"},{\"format\":\"rmb\"},{\"align\":\"center\",\"format\":\"datetime\",\"border\":{\"bottom\":[\"thin\",\"#000\"],\"top\":[\"thin\",\"#000\"],\"left\":[\"thin\",\"#000\"],\"right\":[\"thin\",\"#000\"]},\"bgcolor\":\"#c5e0b3\"},{\"align\":\"left\",\"border\":{\"bottom\":[\"thin\",\"#000\"],\"top\":[\"thin\",\"#000\"],\"left\":[\"thin\",\"#000\"],\"right\":[\"thin\",\"#000\"]},\"bgcolor\":\"#c5e0b3\"}],\"merges\":[\"A1:G1\"],\"rows\":{\"0\":{\"cells\":{\"0\":{\"text\":\"设备信息列表\",\"style\":19,\"merge\":[0,6]},\"1\":{\"style\":20},\"2\":{\"style\":20},\"3\":{\"style\":20},\"4\":{\"style\":20},\"5\":{\"style\":20}}},\"1\":{\"cells\":{\"0\":{\"text\":\"设备名称\",\"style\":11},\"1\":{\"text\":\"设备ip\",\"style\":11},\"2\":{\"text\":\"组织名称\",\"style\":11},\"3\":{\"text\":\"组织编码\",\"style\":11},\"4\":{\"text\":\"状态\",\"style\":11},\"5\":{\"text\":\"创建者\",\"style\":11},\"6\":{\"text\":\"创建时间\",\"style\":11},\"7\":{\"style\":5}}},\"2\":{\"cells\":{\"0\":{\"style\":25,\"text\":\"#{gaea_set_device_info.device_name}\"},\"1\":{\"text\":\"#{gaea_set_device_info.device_ip}\",\"style\":12},\"2\":{\"text\":\"#{gaea_set_device_info.org_name}\",\"style\":12},\"3\":{\"style\":12,\"text\":\"#{gaea_set_device_info.org_code}\"},\"4\":{\"text\":\"#{gaea_set_device_info.enable_flag}\",\"style\":12},\"5\":{\"text\":\"#{gaea_set_device_info.create_by}\",\"style\":12},\"6\":{\"text\":\"#{gaea_set_device_info.create_time}\",\"style\":24}}},\"5\":{\"cells\":{\"5\":{\"style\":5}}},\"6\":{\"cells\":{\"2\":{\"style\":16},\"4\":{\"style\":4},\"5\":{}}},\"8\":{\"cells\":{\"-1\":{\"text\":\"#{gaea_set_device_info.create_by}\"}}},\"10\":{\"cells\":{\"0\":{}}},\"len\":100},\"cols\":{\"0\":{\"width\":193},\"1\":{\"width\":132},\"2\":{\"width\":235},\"3\":{\"width\":141},\"4\":{\"width\":142},\"5\":{\"width\":130},\"6\":{\"width\":228},\"7\":{\"width\":154},\"len\":52},\"validations\":[{\"refs\":[\"H2\"],\"mode\":\"cell\",\"type\":\"date\",\"required\":false,\"operator\":\"be\",\"value\":[\"2020-11-10\",\"2020-11-12\"]},{\"refs\":[\"H2\"],\"mode\":\"cell\",\"type\":\"date\",\"required\":false,\"operator\":\"be\",\"value\":[\"2020-11-10\",\"2020-11-10\"]}],\"autofilter\":{}}";
        // 取第一个sheet
        JSONObject jsonObject = JSONObject.parseObject(json, Feature.OrderedField);
        return jsonObject;
    }

    public JSONObject getData3() {
        String json = "{\"name\":\"sheet1\",\"freeze\":\"A1\",\"styles\":[{\"border\":{\"top\":[\"thin\",\"#000\"]}},{\"border\":{\"top\":[\"thin\",\"#000\"],\"bottom\":[\"thin\",\"#000\"]}},{\"border\":{\"bottom\":[\"thin\",\"#000\"]}},{\"border\":{\"top\":[\"thin\",\"#000\"],\"bottom\":[\"thin\",\"#000\"]},\"bgcolor\":\"#f4b184\"},{\"border\":{\"bottom\":[\"thin\",\"#000\"]},\"color\":\"#fe0000\"},{\"border\":{\"bottom\":[\"thin\",\"#000\"]},\"align\":\"center\"},{\"border\":{\"bottom\":[\"thin\",\"#000\"]},\"valign\":\"top\"},{\"valign\":\"top\"},{\"border\":{\"bottom\":[\"thin\",\"#000\"]},\"valign\":\"bottom\"},{\"valign\":\"bottom\"},{\"border\":{\"bottom\":[\"thin\",\"#000\"]},\"valign\":\"top\",\"textwrap\":true},{\"valign\":\"top\",\"textwrap\":true},{\"border\":{\"bottom\":[\"thin\",\"#000\"],\"top\":[\"thin\",\"#000\"],\"left\":[\"thin\",\"#000\"],\"right\":[\"thin\",\"#000\"]},\"color\":\"#fe0000\"},{\"border\":{\"bottom\":[\"medium\",\"#000\"],\"top\":[\"medium\",\"#000\"],\"left\":[\"medium\",\"#000\"],\"right\":[\"medium\",\"#000\"]},\"valign\":\"top\",\"textwrap\":true},{\"border\":{\"bottom\":[\"thick\",\"#000\"],\"top\":[\"thick\",\"#000\"],\"left\":[\"thick\",\"#000\"],\"right\":[\"thick\",\"#000\"]}},{\"border\":{\"bottom\":[\"dashed\",\"#000\"],\"top\":[\"dashed\",\"#000\"],\"left\":[\"dashed\",\"#000\"],\"right\":[\"dashed\",\"#000\"]}},{\"border\":{\"bottom\":[\"dotted\",\"#000\"],\"top\":[\"dotted\",\"#000\"],\"left\":[\"dotted\",\"#000\"],\"right\":[\"dotted\",\"#000\"]}}],\"merges\":[\"C2:D3\",\"A7:D8\"],\"rows\":{\"0\":{\"cells\":{\"0\":{\"style\":3,\"text\":\"n好\"},\"1\":{\"style\":12,\"text\":\"w欧豪\"},\"2\":{\"style\":5,\"text\":\"w我\"},\"3\":{\"style\":2,\"text\":\"嗯嗯\"}}},\"1\":{\"cells\":{\"0\":{\"style\":13,\"text\":\"你好问\"},\"1\":{\"style\":14},\"2\":{\"style\":14,\"merge\":[1,1]}}},\"2\":{\"cells\":{\"1\":{\"style\":14,\"text\":\"e额\"}}},\"3\":{\"cells\":{\"0\":{\"style\":15},\"1\":{\"style\":16},\"2\":{\"style\":2,\"text\":\"h好\"},\"3\":{\"style\":2}}},\"4\":{\"cells\":{\"1\":{\"style\":2},\"2\":{\"style\":2},\"3\":{\"style\":2}}},\"5\":{\"cells\":{\"0\":{\"style\":2},\"1\":{\"style\":2},\"2\":{\"style\":2},\"3\":{\"style\":2},\"4\":{\"style\":16}}},\"6\":{\"cells\":{\"0\":{\"style\":8,\"merge\":[1,3],\"text\":\"哈哈\"},\"1\":{\"style\":9},\"2\":{\"style\":9},\"3\":{\"style\":9}}},\"7\":{\"cells\":{\"0\":{\"style\":9},\"1\":{\"style\":9},\"2\":{\"style\":9},\"3\":{\"style\":9}},\"height\":94},\"8\":{\"cells\":{\"0\":{\"style\":2},\"1\":{\"style\":2},\"2\":{\"style\":2},\"3\":{\"style\":2}}},\"9\":{\"cells\":{\"0\":{\"style\":2},\"1\":{\"style\":2,\"text\":\"e额\"},\"2\":{\"style\":2},\"3\":{\"style\":2}}},\"10\":{\"cells\":{\"0\":{\"style\":2},\"1\":{\"style\":2},\"2\":{\"style\":2},\"3\":{\"style\":2}}},\"11\":{\"cells\":{\"0\":{\"style\":2,\"text\":\"d的\"},\"1\":{\"style\":2},\"2\":{\"style\":2},\"3\":{\"style\":2}}},\"12\":{\"cells\":{\"0\":{\"style\":2},\"1\":{\"style\":2},\"2\":{\"style\":2},\"3\":{\"style\":2}}},\"13\":{\"cells\":{\"0\":{\"style\":2},\"1\":{\"style\":2},\"2\":{\"style\":2},\"3\":{\"style\":2}}},\"len\":100},\"cols\":{\"len\":52},\"validations\":[],\"autofilter\":{}}";
        return JSONObject.parseObject(json, Feature.OrderedField);
    }

    public String getData4(){
        String json = "{\"name\":\"sheet2\",\"freeze\":\"A1\",\"styles\":[{\"valign\":\"top\"}],\"merges\":[],\"rows\":{\"0\":{\"cells\":{\"0\":{\"text\":{\"type\":\"QRCode\",\"content\":\"http://gaea.anji-plus.com\",\"width\":125,\"height\":125,\"ri\":0,\"ci\":0},\"style\":0}},\"height\":163},\"len\":100},\"cols\":{\"len\":20},\"validations\":[],\"autofilter\":{}}";
        return json;
    }

    public int getHorizontalAlignment(String align) {
        if (align.equals("rigth")) {
            return Element.ALIGN_RIGHT;
        } else if (align.equals("left")) {
            return Element.ALIGN_LEFT;
        } else {
            return Element.ALIGN_CENTER;
        }
    }
}
