package com.anji.plus.gaea.security.event;

/**
 * 用户事件类型
 * @author lr
 * @since 2021-04-14
 */
public enum EventEnum {

    LOCKED,
    CERDENTIALS_EXPIRE
}
