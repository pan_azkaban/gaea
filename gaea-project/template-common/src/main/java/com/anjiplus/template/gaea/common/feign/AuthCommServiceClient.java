package com.anjiplus.template.gaea.common.feign;

import com.anji.plus.gaea.bean.ResponseBean;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * <pre>
 * AuthServiceClient
 * </pre>
 * , fallback = CommonBaseServiceFallback.class
 *
 * @author peiyanni
 * @version AuthServiceClient.java
 */
@FeignClient(name = "gaea-auth", fallback = AuthCommClientFallback.class)
public interface AuthCommServiceClient {

    /**
     * 根据ID查询获取常用查询sql集合
     *
     * @param commonId
     * @return List<DynamicQueryBo>
     */
    @RequestMapping(value = "/commonCondition/getDynamicQueryBoListById", method = RequestMethod.GET)
    ResponseBean getDynamicQueryBoListById(@RequestParam("commonId") Long commonId);

}
