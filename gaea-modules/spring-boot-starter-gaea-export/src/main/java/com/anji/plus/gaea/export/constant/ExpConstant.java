package com.anji.plus.gaea.export.constant;


import java.util.ArrayList;
import java.util.List;

public class ExpConstant {
    public static final String LIST_EXP = "${";
    public static final String GROUP_RIGHT = "groupRight(";
    public static final String GROUP = "group(";
    public static final String SUM = "=SUM(";
    public static final String[] FUNCTION = new String[]{"=SUM(", "=AVERAGE(", "=MAX(", "=MIN(", "=IF(", "=AND(", "=OR(", "=CONCAT("};
    public static final String[] FREEMARKER_FUNCTION = new String[]{"max(", "min(", "average(", "sum("};
    public static final String DYNAMIC = "dynamic(";
    public static final String MAX = "max(";
    public static final String EACH_TITLE = "eachTitle(";

    public static List<Integer> getExpFunction(String e) {
        List<Integer> counts = new ArrayList<>();
        for (int i = 0; i < FUNCTION.length; i++) {
            if(e.contains(FUNCTION[i])){
                counts.add(i);
            }
        }

        return counts;
    }

    public static void main(String[] args) {
        System.out.println(getExpFunction("da").toString());
    }
}
