package com.anji.plus.gaea.job.executor.handler;

import com.anji.plus.gaea.job.executor.handler.param.JobInfo;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

/**
 * @desc 任务接口
 *
 * 示例：分贝读取目录下的A、B、C、D四个订单文件为例，分页200条一页。
 * getParamList 方法返回文件路径列表 List{A、B、C、D}
 * getTotalCount 方法返回计算每个文件共多少行
 * getPageList 方法返回每个文件分页后一页大小，从第一页串，直到处理完
 * execute 方法处理getPageList方法返回的一页数据
 * afterExecute 当该文件处理完后回调
 *
 * @param <Param> 查询参数
 * @param <Entity> getPageList对应的分页数据结构
 *
 * @author 木子李*de
 * @date 2023-05-12 15:25:35.961
 */
public interface JobHandler<Param, Entity> {

    default Param newParam() throws InstantiationException, IllegalAccessException {
        ParameterizedType parameterizedType = (ParameterizedType)getClass().getGenericInterfaces()[0];
        //Class<P> pClass = (Class<P>)parameterizedType.getActualTypeArguments()[0];
        Class<Param> tClass = (Class<Param>)parameterizedType.getActualTypeArguments()[0];
        return tClass.newInstance();
    }
    default Param newEntity() throws InstantiationException, IllegalAccessException {
        ParameterizedType parameterizedType = (ParameterizedType)getClass().getGenericInterfaces()[0];
        //Class<P> pClass = (Class<P>)parameterizedType.getActualTypeArguments()[0];
        Class<Param> tClass = (Class<Param>)parameterizedType.getActualTypeArguments()[1];
        return tClass.newInstance();
    }
    /**
     * 一个job，需要处理多个对象时，覆盖此方法
     * @param jobInfo jogId、tenantCode、orgCode、jobName、job扩展参数
     * @return
     * @throws Exception
     */
    default List<Param> getParamList(JobInfo jobInfo) throws Exception{
        List list = new ArrayList();
        list.add(newParam());
        return list;
    }


    /**
     * 查询总共多少条
     * @param jobInfo
     * @param param
     * @return
     * @throws Exception
     */
    default Integer getTotalCount(JobInfo jobInfo, Param param) throws Exception{
        return 1;
    }

    /**
     * 查询一页数据
     * @param jobInfo
     * @param param
     * @param pageNum
     * @param pageSize
     * @return
     * @throws Exception
     */
    default List<Entity> getPageList(JobInfo jobInfo, Param param, int pageNum, int pageSize) throws Exception{
        List list = new ArrayList();
        list.add(newEntity());
        return list;
    }

    /**
     * 如果重写了getTotalCount、getPageList，就是分页处理
     * 如果只重写了execute，就不分页处理
     *
     * @param jobInfo
     * @param param
     * @param pageList
     * @throws Exception
     */
    void execute(JobInfo jobInfo, Param param, List<Entity> pageList) throws Exception;

    /**
     * 一个Param对象处理完后回调
     * @param jobInfo
     * @param param
     * @throws Exception
     */
    default void afterExecute(JobInfo jobInfo, Param param) throws Exception{

    }
}
