package com.anjiplus.template.gaea.auth.modules.user.controller.param;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 用户表(GaeaUser)param
 *
 * @author peiyanni
 * @since 2021-02-07 13:38:12
 */
@Getter
@Setter
public class GaeaUserPasswordParam implements Serializable {
    private String username;
    /**
     * 新密码
     */
    @NotBlank(message = "password not empty")
    @Size(min = 8,message ="Password at least 8 characters" )
    private String password;
    /**修改密码时旧密码*/
    @NotBlank(message = "oldPassword not empty")
    private String oldPassword;

    /**修改密码时确认密码*/
    @NotBlank(message = "confirmPassword not empty")
    private String confirmPassword;
}
