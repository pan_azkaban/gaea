
package com.anjiplus.template.gaea.business.modules.deviceinfo.service.impl;

import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.anjiplus.template.gaea.business.modules.deviceinfo.dao.entity.DeviceInfo;
import com.anjiplus.template.gaea.business.modules.deviceinfo.service.DeviceInfoService;
import com.anjiplus.template.gaea.business.modules.deviceinfo.dao.DeviceInfoMapper;
import com.anjiplus.template.gaea.business.modules.devicelogdetail.service.DeviceLogDetailService;
import com.anjiplus.template.gaea.business.modules.devicemodel.service.DeviceModelService;
/**
* @desc DeviceInfo 设备信息服务实现
* @author WongBin
* @date 2021-04-07 17:44:28.598
**/
@Service
public class DeviceInfoServiceImpl implements DeviceInfoService {

    @Autowired
    private DeviceInfoMapper deviceInfoMapper;

    @Override
    public GaeaBaseMapper<DeviceInfo> getMapper() {
      return deviceInfoMapper;
    }

    @Autowired
    private DeviceLogDetailService deviceLogDetailService;

    @Autowired
    private DeviceModelService deviceModelService;

    @Override
    public DeviceInfo getDetail(Long id) {
        DeviceInfo deviceInfo = this.selectOne(id);
        deviceInfo.setDeviceLogDetails(deviceLogDetailService.list("device_id",deviceInfo.getId()));
        deviceInfo.setDeviceModels(deviceModelService.list("id",deviceInfo.getDeviceModelId()));
        return deviceInfo;
    }
}