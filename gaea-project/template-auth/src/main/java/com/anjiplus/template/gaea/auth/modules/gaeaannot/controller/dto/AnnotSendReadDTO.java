package com.anjiplus.template.gaea.auth.modules.gaeaannot.controller.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * 功能描述：
 * 我的消息
 * @Author: peiyanni
 * @Date: 2021/3/30 9:54
 */
@Getter
@Setter
public class AnnotSendReadDTO implements Serializable {
    private Long id;
    private Long annotId;
    @ApiModelProperty(value = "标题")
    private String title;
    @ApiModelProperty(value = "消息类型1:通知公告2:系统消息")
    private String msgCategory;
    @ApiModelProperty(value = "通告对象类型（USER:指定用户，ALL:全体用户）")
    private String msgType;
    @ApiModelProperty(value = "发布人")
    private String publisher;
    @ApiModelProperty(value = "发布时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date sendTime;
    @ApiModelProperty(value = "优先级 （优先级（L低，M中，H高））")
    private String priority;
    @ApiModelProperty(value = "阅读状态（0未读，1已读）")
    private Integer readFlag;
}
