package com.anjiplus.template.gaea.auth.modules.gaeaannot.service;

import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.controller.dto.AnnotSendReadDTO;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.controller.dto.GaeaAnnouncementDTO;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.controller.param.GaeaAnnouncementParam;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.controller.param.GaeaAnnouncementSendParam;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.controller.param.ServiceMsgRequest;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.dao.entity.GaeaAnnouncement;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.dao.entity.GaeaAnnouncementSend;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;

/**
 * 系统公告信息表(GaeaAnnouncement)Service
 *
 * @author peiyanni
 * @since 2021-03-29 15:17:21
 */
public interface GaeaAnnouncementService extends GaeaBaseService<GaeaAnnouncementParam, GaeaAnnouncement> {
    /**
     * 系统公告保存并发布
     * @param annotDto
     * @return
     */
    Boolean saveAndPublishAnnotOne(GaeaAnnouncementDTO annotDto);

    /**
     * 批量发布系统公告
     * @param ids
     * @return
     */
    Boolean publishAnnotBatch(List<Long> ids);

    /**
     * 查看我的消息
     * @param param
     * @return
     */
    Page<AnnotSendReadDTO> queryMyAnnotList(GaeaAnnouncementSendParam param);

    /**
     * 撤销已发布公告
     * @param id
     * @return
     */
    Boolean cancelAnnot(Long id);

    /**
     * 发送业务消息给用户
     * @param msgRequest
     * @return
     */
    Boolean sendServiceMsg(ServiceMsgRequest msgRequest);

}