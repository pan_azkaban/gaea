package com.anjiplus.template.gaea.auth.listener;

import com.anji.plus.gaea.security.event.UserLoginEvent;
import org.springframework.context.ApplicationListener;

/**
 * 监听用户登录事件
 * @author lr
 * @since 2021-04-08
 */
public class UserLoginListener implements ApplicationListener<UserLoginEvent> {

    @Override
    public void onApplicationEvent(UserLoginEvent event) {

    }
}
