package com.anji.plus.gaea.curd.entity;

/**
 * 最上层实体类
 * @author lr
 * @since 2021-03-17
 */
public interface BaseEntity extends Converter {

}
