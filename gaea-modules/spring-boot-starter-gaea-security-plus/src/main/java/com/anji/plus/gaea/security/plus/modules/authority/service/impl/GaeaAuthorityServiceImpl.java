package com.anji.plus.gaea.security.plus.modules.authority.service.impl;

import com.anji.plus.gaea.GaeaProperties;
import com.anji.plus.gaea.bean.TreeNode;
import com.anji.plus.gaea.constant.Enabled;
import com.anji.plus.gaea.constant.GaeaConstant;
import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import com.anji.plus.gaea.security.plus.modules.authority.dao.GaeaAuthorityMapper;
import com.anji.plus.gaea.security.plus.modules.authority.dao.entity.GaeaAuthority;
import com.anji.plus.gaea.security.plus.modules.authority.service.GaeaAuthorityService;
import com.anji.plus.gaea.security.plus.modules.role.dao.GaeaRoleMenuAuthorityMapper;
import com.anji.plus.gaea.security.plus.modules.role.dao.entity.GaeaRoleMenuAuthority;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 菜单表(GaeaAuthority)ServiceImpl
 *
 * @author lirui
 * @since 2021-03-01 10:03:51
 */
@Service
public class GaeaAuthorityServiceImpl implements GaeaAuthorityService {

    @Autowired
    private GaeaAuthorityMapper gaeaAuthorityMapper;

    @Autowired
    private GaeaRoleMenuAuthorityMapper gaeaRoleMenuAuthorityMapper;

    @Autowired
    private GaeaProperties gaeaProperties;

    @Override
    public GaeaBaseMapper<GaeaAuthority> getMapper() {
        return  gaeaAuthorityMapper;
    }


    @Override
    public List<TreeNode> authorityTree() {
        LambdaQueryWrapper<GaeaAuthority> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(GaeaAuthority::getScanAnnotation, gaeaProperties.getSecurity().isScanAnnotation()? Enabled.YES.getValue():Enabled.NO.getValue());
        List<GaeaAuthority> gaeaAuthorities = gaeaAuthorityMapper.selectList(wrapper);

        Map<String, String> codeNameMap = gaeaAuthorities.stream()
                .filter(gaeaAuthority -> StringUtils.isNotBlank(gaeaAuthority.getAuthName()))
                .collect(Collectors.toMap(GaeaAuthority::getAuthCode, GaeaAuthority::getAuthName,(v1,v2) -> v2));

        Map<String, List<GaeaAuthority>> applicationTree = gaeaAuthorities.stream()
                .collect(Collectors.groupingBy(GaeaAuthority::getApplicationName));

        List<TreeNode> result = new ArrayList<>();

        applicationTree.entrySet().stream().forEach(entry -> {
            TreeNode treeNode = new TreeNode();
            result.add(treeNode);
            treeNode.setId(entry.getKey());
            if (codeNameMap.containsKey(entry.getKey())) {
                treeNode.setLabel(codeNameMap.get(entry.getKey()));
            } else {
                treeNode.setLabel(entry.getKey());
            }

            List<TreeNode> children = new ArrayList<>();
            treeNode.setChildren(children);
            //controller的Bean名称与方法的
            Map<String, List<GaeaAuthority>> beanMap = entry.getValue().stream().collect(Collectors.groupingBy(GaeaAuthority::getParentCode));

            beanMap.entrySet().stream().forEach(e -> {
                TreeNode node = new TreeNode();
                children.add(node);
                node.setId(e.getKey());
                if (codeNameMap.containsKey(e.getKey())) {
                    node.setLabel(codeNameMap.get(e.getKey()));
                } else {
                    node.setLabel(e.getKey());
                }
                List<TreeNode> beanChildren = new ArrayList<>();
                node.setChildren(beanChildren);

                List<GaeaAuthority> beanAuthorities = e.getValue();
                beanAuthorities.stream().forEach(beanAuthority -> {
                    TreeNode nodeMethod = new TreeNode();
                    beanChildren.add(nodeMethod);
                    nodeMethod.setId(beanAuthority.getAuthCode());
                    if (codeNameMap.containsKey(beanAuthority.getAuthCode())) {
                        nodeMethod.setLabel(beanAuthority.getAuthName());
                    } else {
                        nodeMethod.setLabel(beanAuthority.getAuthCode());
                    }
                });
            });


        });
        return result;
    }

    /**
     * 获取指定角色的权限
     * @param org
     * @param roles
     * @return
     */
    @Override
    public Set<String> userAuthorities(String org, List<String> roles) {

        if (CollectionUtils.isEmpty(roles)) {
            return new HashSet<>();
        }

        //当有超级角色的时候，拥有所有权限
        if (roles.indexOf(GaeaConstant.SUPER_ADMIN_ROLE) != -1) {
            return  gaeaAuthorityMapper.selectList(Wrappers.emptyWrapper()).stream().map(GaeaAuthority::getAuthCode).collect(Collectors.toSet());
        }

        LambdaQueryWrapper<GaeaRoleMenuAuthority> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(GaeaRoleMenuAuthority::getOrgCode, org);
        wrapper.in(GaeaRoleMenuAuthority::getRoleCode, roles);
        wrapper.eq(GaeaRoleMenuAuthority::getIsMenu, Enabled.NO.getValue());
        return gaeaRoleMenuAuthorityMapper.selectList(wrapper)
                .stream().map(GaeaRoleMenuAuthority::getAuthCode).collect(Collectors.toSet());

    }
}
