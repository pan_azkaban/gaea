﻿# 该目录为盖亚[AJ-Gaea]的组件库
```
gaea-modules
	├── pom.xml                                   gaea父pom，jar版本管理
	├── README.md
	├── spring-boot-gaea                          gaea底层库，包含crud、国际化等              
	├── spring-boot-starter-gaea-archiver         数据库归档组件
	├── spring-boot-starter-gaea-archiver-test    数据库归档组件测试示例
	├── spring-boot-starter-gaea-export           导出excel、pdf组件
	├── spring-boot-starter-gaea-flowable         flowable工作流组件
	├── spring-boot-starter-gaea-log              操作日志
	├── spring-boot-starter-gaea-push             邮件、钉钉、阿里|极光|安吉短信推送
	└── spring-boot-starter-gaea-security         权限组件
```

# 查看版本
公网中央仓库https://mvnrepository.com/search?q=com.anji-plus.captcha