/*
 * @Author: zyk
 * @Date: 2020-07-13 15:13:34
 * @Last Modified by: zyk
 * @Last Modified time: 2021-03-15 13:33:24
 */

import request from '@/utils/request'
// 系统设置模块内所有页面相关接口

// 字典页面

// 字典管理查询
export function getDictList(params) {
  return request({
    url: '/business/gaeaDict/pageList',
    method: 'GET',
    params,
  })
}

// 字典管理新增
export function dictAdd(data) {
  return request({
    url: '/business/gaeaDict',
    method: 'POST',
    data,
  })
}

// 字典管理编辑
export function dictEdit(data) {
  return request({
    url: '/business/gaeaDict',
    method: 'PUT',
    data,
  })
}

// 字典管理删除
export function dictDelect(id) {
  return request({
    url: `/business/gaeaDict/${id}`,
    method: 'DELETE',
  })
}

// 字典管理批量删除
export function dictsDelect(data) {
  return request({
    url: `/business/gaeaDict/delete/batch`,
    method: 'POST',
    data,
  })
}

// 字典项页面

// 字典项管理查询
export function getDictItemList(params) {
  return request({
    url: '/business/gaeaDictItem/pageList',
    method: 'GET',
    params,
  })
}

// 字典项管理新增
export function dictItemAdd(data) {
  return request({
    url: '/business/gaeaDictItem',
    method: 'POST',
    data,
  })
}

// 字典项管理编辑
export function dictItemEdit(data) {
  return request({
    url: '/business/gaeaDictItem',
    method: 'PUT',
    data,
  })
}

// 字典项管理删除
export function dictItemDelect(id) {
  return request({
    url: `/business/gaeaDictItem/${id}`,
    method: 'DELETE',
  })
}

// 字典项管理批量删除
export function dictsItemDelect(data) {
  return request({
    url: `/business/gaeaDictItem/delete/batch`,
    method: 'POST',
    data,
  })
}

// 修改用户密码
export function changePassword(data) {
  return request({
    url: '/auth/user/updatePassword',
    method: 'POST',
    data,
  })
}

// 操作日志
export function log(params) {
  return request({
    url: `/v1/login/log`,
    method: 'GET',
    params,
  })
}

// 参数管理查询
export function settingPageList(params) {
  return request({
    url: '/auth/setting/pageList',
    method: 'GET',
    params,
  })
}
// 参数管理编辑接口
export function queryById(data) {
  return request({
    url: `/auth/setting/${data.id}?accessKey=${data.accessKey}`,
    method: 'GET',
    data,
  })
}
// 参数管理新增
export function authSetting(data) {
  return request({
    url: '/auth/setting',
    method: 'POST',
    data,
  })
}
// 参数管理编辑接口
export function authSettingEdit(data) {
  return request({
    url: `/auth/setting`,
    method: 'PUT',
    data,
  })
}

// 操作日志查询接口
export function logPageList(params) {
  return request({
    url: '/auth/log/pageList',
    method: 'GET',
    params,
  })
}

// 帮助中心查询
export function gaeaHelpPageList(params) {
  return request({
    url: '/business/gaeaHelp/pageList',
    method: 'GET',
    params,
  })
}

// 帮助中心新增
export function gaeaHelpAdd(data) {
  return request({
    url: '/business/gaeaHelp',
    method: 'POST',
    data,
  })
}

// 帮助中心编辑
export function gaeaHelpEdit(data) {
  return request({
    url: '/business/gaeaHelp',
    method: 'PUT',
    data,
  })
}

// 上传
export function uploadImg(data) {
  return request({
    url: '/business/file/upload',
    method: 'POST',
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    data,
  })
}

// 帮助中心删除
export function gaeaHelpDelect(id) {
  return request({
    url: `/business/gaeaHelp/${id}`,
    method: 'DELETE',
  })
}

// 多删接口
export function gaeaHelpsDelect(data) {
  return request({
    url: `/business/gaeaHelp/delete/batch`,
    method: 'POST',
    data,
  })
}

// 操作日志导出
export function exportLogToFile(data) {
  return request({
    url: '/auth/log/exportLogToFile',
    method: 'POST',
    data,
  })
}

// 规则查询
export function rulesPageList(params) {
  return request({
    url: '/business/gaeaRules/pageList',
    method: 'get',
    params,
  })
}

// 规则保存
export function rulesSave(data) {
  return request({
    url: '/business/gaeaRules',
    method: 'POST',
    data,
  })
}

// 规则更新
export function rulesUpdate(data) {
  return request({
    url: '/business/gaeaRules',
    method: 'PUT',
    data,
  })
}

// 规则手动执行
export function rulesExecute(data) {
  return request({
    url: '/business/gaeaRules/executeRules',
    method: 'POST',
    data,
  })
}

// 规则删除
export function deleteRules(data) {
  return request({
    url: `/business/gaeaRules/delete/batch`,
    method: 'post',
    data,
  })
}

// 规则详情
export function rulesDetail(id, accessKey) {
  return request({
    url: `/business/gaeaRules/${id}?accessKey=${accessKey}`,
    method: 'GET',
  })
}

/**
 * 刷新数据字典
 * @param data
 */
export function freshDict(data) {
  return request({
    url: '/business/gaeaDict/freshDict',
    method: 'POST',
    data,
  })
}

/**
 * 获取指定语言的字典项
 * @param data
 * @param lang
 */
export function getDictItems(dictCode) {
  return request({
    url: `/business/gaeaDict/map/${dictCode}`,
    method: 'GET',
  })
}
