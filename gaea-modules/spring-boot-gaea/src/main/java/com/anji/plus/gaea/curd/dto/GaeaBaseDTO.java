package com.anji.plus.gaea.curd.dto;


import com.alibaba.fastjson.JSON;
import com.anji.plus.gaea.annotation.Formatter;
import com.anji.plus.gaea.constant.GaeaKeyConstant;
import com.anji.plus.gaea.utils.GaeaUtils;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * 基础传输对象
 * @author lr
 * @since 2021-01-12
 */
public class GaeaBaseDTO implements BaseDTO{

    @Override
    public String toString(){
        return JSON.toJSONString(this);
    }

    private Long id;

    /**
     * 创建人
     */
    @Formatter(key = GaeaKeyConstant.USER_NICKNAME_KEY, replace = {"tenantCode"},targetField = "createByView")
    private String createBy;

    /**
     * 前端展示
     */
    private String createByView;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    /**
     * 修改人
     */
    @Formatter(key = GaeaKeyConstant.USER_NICKNAME_KEY, replace = {"tenantCode"},targetField = "updateByView")
    private String updateBy;


    /**
     * 前端展示
     */
    private String updateByView;

    /**
     * 修改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime;

    /**
     * 版本号
     */
    private Integer version;

    /**
     * accessKey用于校验单条记录权限，防止越权
     * @return
     */
    public String getAccessKey() {
        if (id == null) {
            return null;
        }
        return GaeaUtils.getPassKey(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getCreateByView() {
        return createByView;
    }

    public void setCreateByView(String createByView) {
        this.createByView = createByView;
    }

    public String getUpdateByView() {
        return updateByView;
    }

    public void setUpdateByView(String updateByView) {
        this.updateByView = updateByView;
    }
}
