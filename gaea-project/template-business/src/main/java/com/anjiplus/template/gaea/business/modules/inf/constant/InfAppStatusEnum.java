package com.anjiplus.template.gaea.business.modules.inf.constant;

import lombok.Getter;

/**
 * @author ultrajiaming
 * @since 2021/3/19 16:53
 */
@Getter
public enum InfAppStatusEnum {

    USEFUL(1, "有效"),
    UNUSEFUL(0, "无效");

    private int value;
    private String title;

    InfAppStatusEnum(int value, String title){
        this.value = value;
        this.title = title;
    }

}
