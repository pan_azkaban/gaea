package com.anjiplus.template.gaea.business.modules.inf.inf.service.impl;

import com.anjiplus.template.gaea.business.modules.inf.inf.dao.InfHistoryMapper;
import com.anjiplus.template.gaea.business.modules.inf.inf.dao.entity.InfHistoryEntity;
import com.anjiplus.template.gaea.business.modules.inf.inf.service.InfHistoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class InfHistoryServiceImpl extends ServiceImpl<InfHistoryMapper, InfHistoryEntity> implements InfHistoryService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

}
