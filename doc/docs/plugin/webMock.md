# 介绍
Mock 数据是前端开发过程中必不可少的一环，是分离前后端开发的关键链路。通过预先跟服务器端约定好的接口，模拟请求数据甚至逻辑，能够让前端开发更加独立自主，不会被服务端的开发所阻塞。详见[Mock Data](https://panjiachen.gitee.io/vue-element-admin-site/zh/guide/essentials/mock-api.html)
# 应用
## 启用纯前端 Mock
在src/main.js中：
```js
import { mockXHR } from '../mock'
mockXHR()
```

#本地 Mock 数据与线上数据切换
有很多时候我们会遇到本地使用 mock 数据，线上环境使用真实数据，或者说不同环境使用不同的数据。

Easy-Mock 的形式
你需要保证你本地模拟 api 除了根路径其它的地址是一致的。 比如：
```js
https://api-dev/login   // 本地请求

https://api-prod/login  // 线上请求
```
我们可以通过之后会介绍的环境变量来做到不同环境下，请求不同的 api 地址。
```js
# .env.development
VUE_APP_BASE_API = '/dev-api' #注入本地 api 的根路径
# .env.production
VUE_APP_BASE_API = '/prod-api' #注入线上 api 的根路径
之后根据环境变量创建axios实例，让它具有不同的baseURL。 @/utils/request.js

// create an axios instance
const service = axios.create({
  baseURL: process.env.BASE_API, // api 的 base_url
  timeout: 5000 // request timeout
})
```
这样我们就做到了自动根据环境变量切换本地和线上 api。

Mock.js 的切换
当我们本地使用 Mock.js 模拟本地数据，线上使用真实环境 api 方法。这与上面的 easy-mock 的方法是差不多的。我们主要是判断：是线上环境的时候，不引入 mock 数据就可以了，只有在本地引入 Mock.js。
```js
// main.js
// 通过环境变量来判断是否需要加载启用
if (process.env.NODE_ENV === 'development') {
  require('./mock') // simulation data
}
```
只有在本地环境之中才会引入 mock 数据。

## 组件应用示例
demo地址：[AJ-Gaea\web-template\src\views\component-center\demo.vue](https://gitee.com/anji-plus/gaea/blob/master/web-template/src/views/list/index.vue)
