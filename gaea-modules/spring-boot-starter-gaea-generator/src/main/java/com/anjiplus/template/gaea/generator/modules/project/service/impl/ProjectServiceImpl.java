package com.anjiplus.template.gaea.generator.modules.project.service.impl;

import com.anjiplus.template.gaea.generator.modules.project.dao.entity.Project;
import com.anjiplus.template.gaea.generator.modules.project.dao.ProjectMapper;
import com.anjiplus.template.gaea.generator.modules.project.service.ProjectService;
import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * (Project)ServiceImpl
 *
 * @author makejava
 * @since 2021-03-03 09:54:47
 */
@Service
public class ProjectServiceImpl implements ProjectService {
    @Autowired
    private ProjectMapper projectMapper;

    @Override
    public GaeaBaseMapper<Project> getMapper() {
        return projectMapper;
    }

}
