package com.anjiplus.template.gaea.generator.config;

import com.anji.plus.gaea.constant.GaeaConstant;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

@RefreshScope
@ConfigurationProperties(prefix = GaeaConstant.COMPONENT_PREFIX + GaeaGeneratorProperties.COMPONENT_NAME)
public class GaeaGeneratorProperties {
    /**
     * 组件名称
     */
    public final static String COMPONENT_NAME = "generator";

    /**
     * 在/src/main/resource下存放前后端代码模板的路径，默认template
     */
    private String templatePath = "template";

    /**
     * 所在的服务，路由前缀，对应contextpath或者gateway中predicates.Path前缀
     */
    private String serverRouteName = "";

    /**
     * 查询数据源下所有表时，排除哪些表的sql，比如AND table_name not like 'ACT_%'
     */
    private String excludeTableSql="";

    /**
     * 生成代码时的存放目录，默认/app/disk/workspace
     */
    private String workspace = "/app/disk/workspace";

    /**
     * 指定哪些数据库可以生成代码，多个逗号相隔，格式参考: "'数据库名1','数据库名2'"
     */
    private String schemas = "";

    public String getTemplatePath() {
        return templatePath;
    }

    public void setTemplatePath(String templatePath) {
        this.templatePath = templatePath;
    }

    public String getServerRouteName() {
        return serverRouteName;
    }

    public void setServerRouteName(String serverRouteName) {
        this.serverRouteName = serverRouteName;
    }

    public String getExcludeTableSql() {
        return excludeTableSql;
    }

    public void setExcludeTableSql(String excludeTableSql) {
        this.excludeTableSql = excludeTableSql;
    }

    public String getWorkspace() {
        return workspace;
    }

    public void setWorkspace(String workspace) {
        this.workspace = workspace;
    }

    public String getSchemas() {
        return schemas;
    }

    public void setSchemas(String schemas) {
        this.schemas = schemas;
    }
}
