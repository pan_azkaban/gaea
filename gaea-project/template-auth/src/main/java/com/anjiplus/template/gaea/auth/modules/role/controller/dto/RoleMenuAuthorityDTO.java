package com.anjiplus.template.gaea.auth.modules.role.controller.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * 功能描述：
 * 角色，菜单权限
 * @Author: peiyanni
 * @Date: 2021/2/3 17:46
 */
@Getter
@Setter
public class RoleMenuAuthorityDTO implements Serializable {

    /**
     * 角色
     */
    private String roleCode;

    /**
     * 机构编码
     */
    private String orgCode;

    /**
     * 菜单权限编号
     */
    private List<String> codes;
}
