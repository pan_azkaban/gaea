import request from '@/utils/request'

export function pageList(params) {
  return request({
    url: '/business/inf/app/pageList',
    method: 'get',
    params,
  })
}
export function businessApplication(params) {
  return request({
    url: '/business/inf/app/edit',
    method: 'post',
    data: params,
  })
}
export function addApplication(params) {
  return request({
    url: '/business/inf/app/add',
    method: 'post',
    data: params,
  })
}
export function deleteApplication(id) {
  return request({
    url: '/business/inf/app/' + id,
    method: 'delete',
  })
}
export function appSecret(id) {
  return request({
    url: '/business/inf/app/appSecret/' + id,
    method: 'get',
  })
}
export function queryListInfApp(params) {
  return request({
    url: '/business/inf/app/relation/listInfApp',
    method: 'post',
    data: params,
  })
}
export function updateTimes(params) {
  return request({
    url: '/business//inf/app/relation/times',
    method: 'put',
    data: params,
  })
}
