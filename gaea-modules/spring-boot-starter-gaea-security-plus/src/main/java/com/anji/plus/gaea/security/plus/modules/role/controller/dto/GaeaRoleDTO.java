package com.anji.plus.gaea.security.plus.modules.role.controller.dto;

import com.anji.plus.gaea.annotation.Formatter;
import com.anji.plus.gaea.curd.dto.GaeaBaseDTO;
import com.anji.plus.gaea.security.plus.constant.AuthCacheKey;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 角色(GaeaRole)实体类
 *
 * @author lr
 * @since 2021-02-02 13:37:54
 */
@ApiModel(value = "角色")
public class GaeaRoleDTO extends GaeaBaseDTO {

    /**
     * 机构编码
     */
    @Formatter(key = AuthCacheKey.ORG_CODE_NAME_KEY,targetField = "orgName")
    private String orgCode;

    private String orgName;
    /**
     * 角色编码
     */
    @ApiModelProperty(value = "角色编码")
    private String roleCode;

    private String roleName;
    /**
     * 1：可用 0：禁用
     */
    @ApiModelProperty(value = "1：可用 0：禁用")
    @Formatter(dictCode = "ENABLE_FLAG", targetField = "enabledStr")
    private Integer enabled;

    private String enabledStr;
    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String remark;

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getEnabledStr() {
        return enabledStr;
    }

    public void setEnabledStr(String enabledStr) {
        this.enabledStr = enabledStr;
    }
}
