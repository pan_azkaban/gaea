package com.anji.plus.gaea.security.plus.modules.menu.controller.dto;

import com.anji.plus.gaea.bean.TreeNode;

import java.io.Serializable;
import java.util.List;

/**
 * 树
 */
public class TreeDTO implements Serializable {
    private List<TreeNode> treeDatas;
    private List<String> checkedCodes;

    public List<TreeNode> getTreeDatas() {
        return treeDatas;
    }

    public void setTreeDatas(List<TreeNode> treeDatas) {
        this.treeDatas = treeDatas;
    }

    public List<String> getCheckedCodes() {
        return checkedCodes;
    }

    public void setCheckedCodes(List<String> checkedCodes) {
        this.checkedCodes = checkedCodes;
    }
}
