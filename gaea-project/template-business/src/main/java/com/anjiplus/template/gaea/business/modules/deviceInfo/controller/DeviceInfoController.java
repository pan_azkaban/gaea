
package com.anjiplus.template.gaea.business.modules.deviceinfo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.anji.plus.gaea.annotation.AccessKey;
import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.curd.service.GaeaBaseService;

import com.anji.plus.gaea.utils.GaeaBeanUtils;
import com.anji.plus.gaea.utils.GaeaUtils;

import com.anjiplus.template.gaea.business.base.BaseController;
import com.anjiplus.template.gaea.business.modules.deviceinfo.dao.entity.DeviceInfo;
import com.anjiplus.template.gaea.business.modules.deviceinfo.service.DeviceInfoService;
import com.anjiplus.template.gaea.business.modules.deviceinfo.controller.dto.DeviceInfoDto;
import com.anjiplus.template.gaea.business.modules.deviceinfo.controller.param.DeviceInfoParam;

import io.swagger.annotations.Api;

/**
* @desc 设备信息 controller
* @website https://gitee.com/anji-plus/gaea
* @author WongBin
* @date 2021-04-07 17:44:28.598
**/
@RestController
@Api(tags = "设备信息管理")
@RequestMapping("/deviceInfo")
public class DeviceInfoController extends BaseController<DeviceInfoParam, DeviceInfo, DeviceInfoDto> {

    @Autowired
    private DeviceInfoService deviceInfoService;

    @Override
    public GaeaBaseService<DeviceInfoParam, DeviceInfo> getService() {
        return deviceInfoService;
    }

    @Override
    public DeviceInfo getEntity() {
        return new DeviceInfo();
    }

    @Override
    public DeviceInfoDto getDTO() {
        return new DeviceInfoDto();
    }


    @GetMapping({"/{id}"})
    @AccessKey
    @Override
    public ResponseBean detail(@PathVariable("id") Long id) {
        this.logger.info("{}根据ID查询服务开始，id为：{}", this.getClass().getSimpleName(), id);
        DeviceInfo result = deviceInfoService.getDetail(id);
        DeviceInfoDto dto = this.getDTO();
        GaeaBeanUtils.copyAndFormatter(result, dto);
        ResponseBean responseBean = this.responseSuccessWithData(this.resultDtoHandle(dto));
        this.logger.info("{}根据ID查询结束，结果：{}", this.getClass().getSimpleName(), GaeaUtils.toJSONString(responseBean));
        return responseBean;
    }
}