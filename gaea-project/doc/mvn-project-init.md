项目模板生成命令：
```
cd ./gaea-project/
mvn clean

指定自己maven配置的位置
mvn -s "D:\App\apache-maven-3.5.3\conf\settings.xml" archetype:create-from-project -Darchetype.properties=./doc/archetype.properties

替换target\generated-sources\archetype\src\main\resources\META-INF\maven\archetype-metadata.xml

cd  target\generated-sources\archetype & mvn install
测试生成的模板文件是否正确。

需要deploy时，修改如下文件：
pom.xml根节点追加如下内容，地址做相应修改
  <distributionManagement>
      <repository>
          <id>nexus-releases</id>
          <url>http://ip:port/repository/maven-releases/</url>
      </repository>
      <snapshotRepository>
          <id>nexus-snapshots</id>
          <url>http://ip:port/repository/maven-snapshots/</url>
      </snapshotRepository>
  </distributionManagement>

mvn deploy
```

=========================================================
客户端修改如下文件：.m2/archetype-catalog.xml，追加repository，地址做相应修改。
```
  <archetypes>
    <archetype>
      <groupId>com.anji-plus.gaea</groupId>
      <artifactId>template-archetype</artifactId>
      <version>1.0.0-SNAPSHOT</version>
      <description>gaea-template-archetype</description>
      <repository>
		http://ip:port/nexus/content/repositories/snapshots
	  </repository>
    </archetype>
  </archetypes>
```

执行如下命令，生成新项目
```
mvn archetype:generate -DarchetypeCatalog=local -DgroupId=com.anjiplus.新项目名 -DartifactId=新项目名 -Dpackage=com.anjiplus.新项目名 -Dversion=1.0.0-SNAPSHOT -DarchetypeGroupId=com.anji-plus.gaea -DarchetypeArtifactId=template-archetype -DarchetypeVersion=1.0.0-SNAPSHOT -DinteractiveMode=false
```
