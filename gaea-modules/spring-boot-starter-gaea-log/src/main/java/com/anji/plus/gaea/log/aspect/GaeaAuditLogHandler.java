package com.anji.plus.gaea.log.aspect;

import javax.servlet.http.HttpServletRequest;

import com.anji.plus.gaea.annotation.log.GaeaAuditLog;

/***
 * @author wongbin
 * @date 2022/06/15
 */
public interface GaeaAuditLogHandler {
    /***
     * 客户端推送日志数据接口
     * @param url
     * @param data
     */
    void push(String url,Object data);

    /**
     * 日志填充扩展点
     * @param request
     * @param operLog
     */
    default void initLogInfo(HttpServletRequest request,
                LogOperation operLog, Object result, Exception ex, GaeaAuditLog tag){

    };
}
