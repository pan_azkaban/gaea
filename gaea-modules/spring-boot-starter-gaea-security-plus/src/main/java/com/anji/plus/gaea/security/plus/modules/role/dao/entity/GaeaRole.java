package com.anji.plus.gaea.security.plus.modules.role.dao.entity;

import com.anji.plus.gaea.annotation.UnionUnique;
import com.anji.plus.gaea.annotation.UnionUniqueCode;
import com.anji.plus.gaea.curd.entity.GaeaBaseEntity;
import com.anji.plus.gaea.security.plus.code.RespCommonCode;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * 角色(GaeaRole)实体类
 *
 * @author lr
 * @since 2021-02-02 13:37:54
 */
@TableName("gaea_role")
@UnionUniqueCode(group = RespCommonCode.ROLE_CODE_EXIST_GROUP  ,code = RespCommonCode.ROLE_CODE_EXIST)
public class GaeaRole extends GaeaBaseEntity implements Serializable {

    /**
     * 机构编码
     */
    @UnionUnique(group = RespCommonCode.ROLE_CODE_EXIST_GROUP)
    private String orgCode;
    /**
     * 角色编码
     */
    @UnionUnique(group = RespCommonCode.ROLE_CODE_EXIST_GROUP)
    private String roleCode;

    private String roleName;
    /**
     * 1：可用 0：禁用
     */
    private Integer enabled;
    /**
     * 描述
     */
    private String remark;

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


}
