package com.anji.plus.gaea.security.plus.modules.role.controller.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 功能描述：
 * 角色，菜单权限
 * @Author: peiyanni
 * @Date: 2021/2/3 17:46
 */
public class RoleMenuAuthorityDTO implements Serializable {

    /**
     * 角色
     */
    private String roleCode;

    /**
     * 机构编码
     */
    private String orgCode;

    /**
     * 菜单权限编号
     */
    private List<String> codes;

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public List<String> getCodes() {
        return codes;
    }

    public void setCodes(List<String> codes) {
        this.codes = codes;
    }
}
