/*
 *  Copyright 2019-2020 Zheng Jie
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.anjiplus.template.gaea.generator.modules.table.controller;

import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.curd.controller.BaseResponse;
import com.anjiplus.template.gaea.generator.modules.table.dao.GeneratorTable;
import com.anjiplus.template.gaea.generator.modules.table.service.TableService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author Zheng Jie
 * @date 2019-01-14
 */
@RestController
@RequestMapping("/api/genConfig")
@Api(tags = "代码生成器配置管理")
public class TableController extends BaseResponse {

    @Autowired(required = false)
    private TableService tableService;

    @ApiOperation("查询")
    @GetMapping(value = "/{schemaName}/{tableName}")
    public ResponseBean query(
            @PathVariable String schemaName,
            @PathVariable String tableName){
        GeneratorTable generatorTable = tableService.find(schemaName,tableName);
        return responseSuccessWithData(generatorTable);
    }

    @ApiOperation("修改")
    @PutMapping
    public ResponseBean update(@Validated @RequestBody GeneratorTable c){
        GeneratorTable generatorTable = tableService.update(c.getSchemaName(),c.getTableName(), c);
        return responseSuccessWithData(generatorTable);
    }
}
