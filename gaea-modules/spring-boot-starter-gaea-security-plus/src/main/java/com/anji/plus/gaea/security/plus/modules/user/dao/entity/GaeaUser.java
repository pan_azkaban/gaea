package com.anji.plus.gaea.security.plus.modules.user.dao.entity;

import com.anji.plus.gaea.annotation.Unique;
import com.anji.plus.gaea.constant.Enabled;
import com.anji.plus.gaea.curd.entity.GaeaBaseEntity;
import com.anji.plus.gaea.security.plus.code.RespCommonCode;
import com.anji.plus.gaea.security.security.extension.GaeaUserDetail;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Collection;
import java.util.Date;

import org.springframework.security.core.GrantedAuthority;

/**
 * 用户表(GaeaUser)实体类
 *
 * @author lr
 * @since 2021-02-02 13:38:12
 */
@TableName("gaea_user")
public class GaeaUser extends GaeaBaseEntity implements GaeaUserDetail {

    /**
     * 用户名
     */
    @Unique(code = RespCommonCode.USER_CODE_ISEXIST)
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 密码更新时间
     */
    private Date passwordUpdateTime;
    /**
     * 真实姓名
     */
    private String nickname;

    private String email;

    private String phone;
    /**
     * 1：可用 0：禁用
     */
    private Integer enabled;

    /**
     * 设置多终端登录，0：不允许，1：允许
     */
    private Integer multiLogin;
    /**
     * 0：否，锁定，1：是，未锁定
     */
    private Integer accountLocked;
    /**
     * 0：否，过期，1：是，未过期
     */
    private Integer accountNonExpired;
    /**
     * 0：否，过期，1：是，未过期
     */
    private Integer credentialsNonExpired;

    /**
     * 角色
     */
    @TableField(exist = false)
    private String roleOrgCode;

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public Integer getAccountLocked() {
        return accountLocked;
    }

    public void setAccountLocked(Integer accountLocked) {
        this.accountLocked = accountLocked;
    }

    public Integer getAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonExpired(Integer accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    public Integer getCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(Integer credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }


    public Integer getMultiLogin() {
        return multiLogin;
    }

    public void setMultiLogin(Integer multiLogin) {
        this.multiLogin = multiLogin;
    }

    @Override
    public Date getPasswordUpdateTime() {
        return passwordUpdateTime;
    }

    public void setPasswordUpdateTime(Date passwordUpdateTime) {
        this.passwordUpdateTime = passwordUpdateTime;
    }

    public String getRoleOrgCode() {
        return roleOrgCode;
    }

    public void setRoleOrgCode(String roleOrgCode) {
        this.roleOrgCode = roleOrgCode;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return Enabled.YES.getValue().equals(accountNonExpired);
    }

    @Override
    public boolean isAccountNonLocked() {
        return !Enabled.YES.getValue().equals(accountLocked);
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return Enabled.YES.getValue().equals(credentialsNonExpired);
    }

    @Override
    public boolean enable() {
        return Enabled.YES.getValue().equals(enabled);
    }
}
