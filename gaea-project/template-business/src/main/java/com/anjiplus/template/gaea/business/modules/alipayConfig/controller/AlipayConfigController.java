
package com.anjiplus.template.gaea.business.modules.alipayconfig.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.anji.plus.gaea.annotation.AccessKey;
import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.curd.controller.GaeaBaseController;
import com.anji.plus.gaea.curd.service.GaeaBaseService;

import com.anji.plus.gaea.utils.GaeaBeanUtils;
import com.anji.plus.gaea.utils.GaeaUtils;

import com.anjiplus.template.gaea.business.modules.alipayconfig.dao.entity.AlipayConfig;
import com.anjiplus.template.gaea.business.modules.alipayconfig.service.AlipayConfigService;
import com.anjiplus.template.gaea.business.modules.alipayconfig.controller.dto.AlipayConfigDto;
import com.anjiplus.template.gaea.business.modules.alipayconfig.controller.param.AlipayConfigParam;

import io.swagger.annotations.Api;

/**
* @desc 支付配置 controller
* @website https://gitee.com/anji-plus/gaea
* @author dev-user
* @date 2021-04-07 14:58:28.131
**/
@RestController
@Api(tags = "支付配置管理")
@RequestMapping("/alipayConfig")
public class AlipayConfigController extends GaeaBaseController<AlipayConfigParam, AlipayConfig, AlipayConfigDto> {

    @Autowired
    private AlipayConfigService alipayConfigService;

    @Override
    public GaeaBaseService<AlipayConfigParam, AlipayConfig> getService() {
        return alipayConfigService;
    }

    @Override
    public AlipayConfig getEntity() {
        return new AlipayConfig();
    }

    @Override
    public AlipayConfigDto getDTO() {
        return new AlipayConfigDto();
    }


    @GetMapping({"/{id}"})
    @AccessKey
    @Override
    public ResponseBean detail(@PathVariable("id") Long id) {
        this.logger.info("{}根据ID查询服务开始，id为：{}", this.getClass().getSimpleName(), id);
        AlipayConfig result = alipayConfigService.getDetail(id);
        AlipayConfigDto dto = this.getDTO();
        GaeaBeanUtils.copyAndFormatter(result, dto);
        ResponseBean responseBean = this.responseSuccessWithData(this.resultDtoHandle(dto));
        this.logger.info("{}根据ID查询结束，结果：{}", this.getClass().getSimpleName(), GaeaUtils.toJSONString(responseBean));
        return responseBean;
    }
}