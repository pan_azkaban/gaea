# 高级查询和自定义列

## 介绍
高级查询：通过自定义sql来获取相关查询数据，开发人员提前进行相关配置，前端用户在查询时传入配置好的查询条件，后端根据相关规则生成相关sql，组装好QueryWrapper。

自定义列：开发人员提前配置好菜单查询返回的列信息，前端用户可以在界面上进行个性化配置，显示或者隐藏列，每列的宽度，是否可以排序，以及调整列的顺序。

## 高级查询配置(开发人员)
1. 首先开发人员开发了一个高级查询的接口供前端调用，需要注意的地方有三个
- 接口的输入参数需要继承父类**com.anji.plus.dto.BaseQueryBO**.例如：
```java
import com.anjiplus.template.gaea.common.dto.BaseQueryBO;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class GaeaUserQueryParam extends BaseQueryBO implements Serializable {
    private String username;

}
```
- 在对应service层的实现方法上添加注解**com.anji.plus.aop.GaeaQuery**.例如：
```java
/**
 * 用户信息高级查询
 * 高级查询在实现类中使用注解@GaeaQuery，方法后面的参数新增一个QueryWrapper动态参数，
 * 方法内部可以直接使用QueryWrapper
 * @param param
 * @param wrappers
 * @return
 */
 @Override
 @GaeaQuery
 public Page<GaeaUserQueryDto> queryAdvanceUserInfo(GaeaUserQueryParam param, QueryWrapper... wrappers) {
     Page<GaeaUserQueryDto> page = new Page<>(param.getPageNumber(), param.getPageSize());
     QueryWrapper queryWrapper = (null != wrappers && wrappers.length > 0) ? wrappers[0] : null;
     List<GaeaUserQueryDto> list= gaeaUserMapper.queryUserAdvance(page,param,queryWrapper);
     page.setRecords(list);
     return page;
 }

```
  - 方法的输入参数需要新增一个QueryWrapper的动态参数，用于接收自定义sql。具体可以以参考菜单【高级查询/动态列】的查询接口。如上；
2. 其次通过【菜单配置】，找到对应菜单，点击【配置菜单】进入高级查询/自定义列列表界面，新增高级查询条件，具体可以参考菜单【高级查询/动态列】相关配置。需要注意如下:
  - 新增的表编码需要前端开发人员分配，其中的编码是sql语句中的查询列名，如果sql中使用了别名，也需要带上别名;
 - 如果查询条件类型选的日期，则编码需要加上对应的转换函数，列如mysql数据库中是**date_format(gu.create_time,'%Y-%m-%d')**,Oracle 数据库中是**to_char(gu.create_time,'yyyy-mm-dd')**,并配置上对应的精度;
 - 如果查询条件是下拉框，则需要输入对应的接口地址，接口返回参数需要一致，具体参考接口**gaeaDict/select/{dictCode}** 返回的数据格式;

3. 配置示例：
![高级查询配置](https://gaea.anji-plus.com/business/file/download/b1a28dbc-d968-44d4-89ec-673655b9fb92)
## 自定义列配置(开发人员)
1. 自定义列配置好后，后端开发人员不需要做其他处理。
2. 通过【菜单配置】，找到对应的菜单，点击【配置菜单】进入高级查询/自定义列列表界面，新增自定列，具体可以参考菜单【高级查询/动态列】相关配置。需要注意如下:
	- 新增的表编码需要前端开发人员分配，列编码是返回的对应实体的字段名;
	- 如果配置的可以排序，则排序字段是对应sql中的字段名，如果有别名，需要带上别名;
3. 配置示例：
![自定义列配置](https://gaea.anji-plus.com/business/file/download/3d1de533-73f9-4fff-85e7-6a4d64c2ce00)

