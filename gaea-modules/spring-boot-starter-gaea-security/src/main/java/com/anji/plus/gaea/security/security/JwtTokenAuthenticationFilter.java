package com.anji.plus.gaea.security.security;

import com.anji.plus.gaea.GaeaProperties;
import com.anji.plus.gaea.cache.CacheHelper;
import com.anji.plus.gaea.constant.GaeaConstant;
import com.anji.plus.gaea.constant.GaeaKeyConstant;
import com.anji.plus.gaea.holder.UserContentHolder;
import com.anji.plus.gaea.holder.UserContext;
import com.anji.plus.gaea.security.GaeaSecurityProperties;
import com.anji.plus.gaea.security.code.UserResponseCode;
import com.anji.plus.gaea.security.handler.GaeaFilterExceptionHandler;
import com.anji.plus.gaea.utils.GaeaUtils;
import com.anji.plus.gaea.utils.JwtBean;
import com.auth0.jwt.exceptions.SignatureGenerationException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 分布式环境中构造本地Security Authentication
 *
 * @author lr
 * @since 2021-01-26
 */
public class JwtTokenAuthenticationFilter extends OncePerRequestFilter {

    /**
     * 缓存帮助类
     */
    @Autowired
    private CacheHelper cacheHelper;

    @Autowired
    private JwtBean jwtBean;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private GaeaSecurityProperties gaeaSecurityProperties;

    @Autowired
    private GaeaProperties gaeaProperties;

    @Autowired
    private GaeaFilterExceptionHandler gaeaFilterExceptionHandler;

    protected AuthenticationDetailsSource<HttpServletRequest, ?> authenticationDetailsSource = new WebAuthenticationDetailsSource();

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        try {
            //白名单
            if (GaeaUtils.matchPath(gaeaProperties.getSecurity().getWhiteList(), request.getServletPath())) {
                //当符合白名单时,直接跳过
                filterChain.doFilter(request, response);
                return;
            }

            //当Security中存在登录标识时，直接跳过
            if (SecurityContextHolder.getContext().getAuthentication() != null || gaeaSecurityProperties.getAuthDisabled()) {
                filterChain.doFilter(request, response);
                return;
            }

            //获取请求头中token
            String token = request.getHeader(GaeaConstant.Authorization);

            //当token为空或过期时，未登录
            if (StringUtils.isBlank(token)) {
                throw new TokenExpiredException(UserResponseCode.USER_TOKEN_EXPIRED);
            }

            String username = null;
            try {
                username = jwtBean.getUsername(token);
            } catch (Exception e) {

            }
            if (StringUtils.isBlank(username)) {
                throw new TokenExpiredException(String.format("The Token has expired on %s.", new Date()));
            }

            String tokenKey = GaeaKeyConstant.USER_LOGIN_TOKEN + username + GaeaConstant.REDIS_SPLIT + jwtBean.getUUID(token);

            //验证token是否有效
            if (!cacheHelper.exist(tokenKey) || !StringUtils.equals(token,cacheHelper.stringGet(tokenKey))) {
                throw new TokenExpiredException(String.format("The Token has expired on %s.", new Date()));
            }

            //刷新token时间
            cacheHelper.expire(tokenKey, TimeUnit.MINUTES, jwtBean.getGaeaProperties().getSecurity().getJwtTokenTimeout());

            UserDetails userDetails = userDetailsService.loadUserByUsername(username);
            SecurityContext context = SecurityContextHolder.createEmptyContext();
            context.setAuthentication(createSuccessfulAuthentication(request, userDetails));
            SecurityContextHolder.setContext(context);

            UserContext userContext = UserContentHolder.getContext();
            userContext.setAuthorities(userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toSet()));
            filterChain.doFilter(request, response);
        } catch (TokenExpiredException | SignatureGenerationException | SignatureVerificationException exception) {
            gaeaFilterExceptionHandler.handler(request, response, exception);
            return;
        } catch (Exception e) {
            gaeaFilterExceptionHandler.handler(request, response, e);
            return;
        } finally {
            UserContentHolder.clearContext();
        }

    }

    /**
     * 构建成功的AuthenticationToken
     *
     * @param request
     * @param user
     * @return
     */
    private Authentication createSuccessfulAuthentication(HttpServletRequest request,
                                                          UserDetails user) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user, user.getPassword(), user.getAuthorities());

        authenticationToken.setDetails(this.authenticationDetailsSource.buildDetails(request));

        return authenticationToken;
    }

}
