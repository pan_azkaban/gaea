package com.anjiplus.template.gaea.business.feign;

import com.anji.plus.gaea.bean.ResponseBean;
import com.anjiplus.template.gaea.business.feign.dto.AppspPushParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


/**
 * 功能描述：
 * auth服务fallback
 * @Author: peiyanni
 * @Date: 2021/2/19 14:32
 */
@Service
@Slf4j
public class AuthServiceClientFallback implements AuthServiceClient {

    @Override
    public ResponseBean testAppspPush(AppspPushParam pushParam) {
        log.info("----AuthServiceClientFallback----");
        return null;
    }
}
