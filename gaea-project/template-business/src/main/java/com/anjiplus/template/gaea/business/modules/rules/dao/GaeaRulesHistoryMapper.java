package com.anjiplus.template.gaea.business.modules.rules.dao;

import com.anjiplus.template.gaea.business.modules.rules.dao.entity.GaeaRulesHistory;
import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * (GaeaRulesHistory)Mapper
 *
 * @author why
 * @since 2021-03-17 13:52:55
 */
@Mapper
public interface GaeaRulesHistoryMapper extends GaeaBaseMapper<GaeaRulesHistory> {


}