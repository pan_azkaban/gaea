/**/
package com.anjiplus.template.gaea.business.modules.devicelogdetail.controller.param;

import lombok.Data;
import java.io.Serializable;
import com.anji.plus.gaea.annotation.Query;
import com.anji.plus.gaea.constant.QueryEnum;
import com.anji.plus.gaea.curd.params.PageParam;

import java.util.List;


/**
* @desc DeviceLogDetail 设备日志明细查询输入类
* @author dev-user
* @date 2021-04-07 17:06:42.920
**/
@Data
public class DeviceLogDetailParam extends PageParam implements Serializable{

    /** 模糊查询 */
    @Query(value = QueryEnum.LIKE)
    private Long deviceId;

    /** 模糊查询 */
    @Query(value = QueryEnum.LIKE)
    private String log;
}