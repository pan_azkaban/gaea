package com.anji.plus.gaea.push.type.email;

import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.push.support.AbstractPushSender;
import com.anji.plus.gaea.push.type.base.BasePushDetails;
import com.anji.plus.gaea.push.type.param.PushParamVO;
import com.anji.plus.gaea.push.utils.TemplateAnalysisUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

/**
 * 发送邮件
 * @author lr
 * @since 2021-02-08
 */
public class GaeaMailTemplate extends AbstractPushSender<MailPushDetails> {

    /**
     * 发送者
     */
    private JavaMailSender mailSender;

    @Autowired
    private GaeaPushMailProperties gaeaPushMailProperties;

    private Object lock = new Object();

    /**
     * 发送邮件
     * @param details
     * @throws MessagingException
     */
    @Override
    public ResponseBean doSend(MailPushDetails details) throws MessagingException {
        MimeMessage mimeMessage = getJavaMailSender().createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);

        MimeMessageBuilder builder = new MimeMessageBuilder(mimeMessageHelper);

        //发送人
        builder.from(details.getFrom())
                .to(details.getTo())
                .subject(details.getSubject())
                .cc(details.getCc())
                .bcc(details.getBcc())
                .text(details.getText(), details.isHtml())
                .sentDate(details.getSentDate())
                .addAttachment(details.getFileMap());

        getJavaMailSender().send(mimeMessage);
        return ResponseBean.builder().build();
    }

    /**
     * 简单邮件
     * @param from
     * @param to
     * @param subject
     * @param text
     */
    public void simpleEmail(String from, String to, String subject, String text) {
        //创建简单邮件消息
        SimpleMailMessage message = new SimpleMailMessage();
        //谁发的
        message.setFrom(from);
        //谁要接收
        message.setTo(to);
        //邮件标题
        message.setSubject(subject);
        //邮件内容
        message.setText(text);
        getJavaMailSender().send(message);
    }

    @Override
    public BasePushDetails convert(PushParamVO pushParamVO) {
        String html = TemplateAnalysisUtil.buildHTML(pushParamVO.getTemplate(), pushParamVO.getParamMap(), pushParamVO.getTemplateParam(), true);
        pushParamVO.setParam(html);
        MailPushDetails mailPushDetails = new MailPushDetails();
        BeanUtils.copyProperties(pushParamVO, mailPushDetails);
        mailPushDetails.setSentDate(new Date());
        mailPushDetails.setText(pushParamVO.getParam());
        if (getJavaMailSender() instanceof JavaMailSenderImpl) {
            JavaMailSenderImpl javaMailSender = (JavaMailSenderImpl) getJavaMailSender();
            mailPushDetails.setFrom(javaMailSender.getJavaMailProperties().getProperty("from", javaMailSender.getUsername()));
            mailPushDetails.setHtml(Boolean.parseBoolean(javaMailSender.getJavaMailProperties().getProperty("html", "true")));
        }

        return mailPushDetails;
    }

    public JavaMailSender getJavaMailSender(){
        if (null != mailSender) {
            return mailSender;
        }
        synchronized (lock) {
            JavaMailSenderImpl jms = new JavaMailSenderImpl();
            jms.setHost(gaeaPushMailProperties.getHost());
            jms.setPort(gaeaPushMailProperties.getPort());
            jms.setUsername(gaeaPushMailProperties.getUsername());
            jms.setPassword(gaeaPushMailProperties.getPassword());
            jms.setDefaultEncoding(gaeaPushMailProperties.getDefaultEncoding().name());
            Properties p = new Properties();
            p.putAll(gaeaPushMailProperties.getProperties());
            jms.setJavaMailProperties(p);
            mailSender = jms;
        }
        return mailSender;
    }
}
