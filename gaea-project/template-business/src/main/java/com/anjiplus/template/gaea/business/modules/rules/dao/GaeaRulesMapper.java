package com.anjiplus.template.gaea.business.modules.rules.dao;

import com.anjiplus.template.gaea.business.modules.rules.dao.entity.GaeaRules;
import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * (GaeaRules)Mapper
 *
 * @author lirui
 * @since 2021-03-12 10:38:19
 */
@Mapper
public interface GaeaRulesMapper extends GaeaBaseMapper<GaeaRules> {


}