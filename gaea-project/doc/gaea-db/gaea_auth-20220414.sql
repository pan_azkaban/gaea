/*
 Navicat Premium Data Transfer

 Source Server         : gaea-dev-26.197
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : 10.108.26.197:3306
 Source Schema         : gaea_auth

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 14/04/2022 14:46:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gaea_announcement
-- ----------------------------
DROP TABLE IF EXISTS `gaea_announcement`;
CREATE TABLE `gaea_announcement`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `msg_content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `start_time` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `priority` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '优先级 （优先级（L低，M中，H高））',
  `msg_category` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息类型1:通知公告2:系统消息',
  `msg_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '通告对象类型（USER:指定用户，ALL:全体用户）',
  `send_status` int(11) NULL DEFAULT 0 COMMENT '发布状态（0未发布，1已发布，2已撤销）',
  `publisher` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发布人',
  `send_time` datetime(0) NULL DEFAULT NULL COMMENT '发布时间',
  `cancel_time` datetime(0) NULL DEFAULT NULL COMMENT '撤销时间',
  `user_names` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '指定用户，多个逗号隔开',
  `relation_info` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '关联的业务数据（唯一标识等）',
  `relation_type` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联业务类型',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `version` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 62 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统公告信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gaea_announcement
-- ----------------------------
INSERT INTO `gaea_announcement` VALUES (1, '系统测试', '公告测试', '2021-04-07 11:09:34', '2021-04-07 11:09:36', 'M', '2', 'USER', 1, 'lixiaoyan', '2021-04-08 13:23:50', NULL, 'pyn', NULL, NULL, 'pyn', '2021-04-07 11:10:32', 'lixiaoyan', '2021-04-08 13:23:50', 3);

-- ----------------------------
-- Table structure for gaea_announcement_send
-- ----------------------------
DROP TABLE IF EXISTS `gaea_announcement_send`;
CREATE TABLE `gaea_announcement_send`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `annt_id` bigint(20) NULL DEFAULT NULL COMMENT '系统公告id',
  `user_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户登录名',
  `read_flag` int(11) NULL DEFAULT 0 COMMENT '阅读状态（0未读，1已读）',
  `read_time` datetime(0) NULL DEFAULT NULL COMMENT '阅读时间',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `version` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `annot_index_01`(`user_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统公告发送阅读记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gaea_announcement_send
-- ----------------------------

-- ----------------------------
-- Table structure for gaea_authority
-- ----------------------------
DROP TABLE IF EXISTS `gaea_authority`;
CREATE TABLE `gaea_authority`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `application_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应用名称',
  `scan_annotation` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扫描类型：0：不扫描注解j即扫描所有，1：扫描注解',
  `auth_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限代码',
  `auth_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限名称',
  `parent_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父级代码',
  `path` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求路径',
  `sort` int(8) NULL DEFAULT 0 COMMENT '排序，升序',
  `enabled` int(1) NULL DEFAULT 1 COMMENT '0--禁用 1--启用',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `version` int(8) NULL DEFAULT NULL COMMENT '版本号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13352 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gaea_authority
-- ----------------------------
INSERT INTO `gaea_authority` VALUES (11069, 'gaea-auth', '0', 'gaeaAuthorityController#authorityTree', '权限树', 'gaeaAuthorityController', 'GET#/gaeaAuthority/authority/tree/**/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11070, 'gaea-auth', '0', 'gaeaAuthorityController#roleAuthority', '角色分配权限', 'gaeaAuthorityController', 'POST#/gaeaAuthority/role/authority', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11071, 'gaea-auth', '0', 'gaeaAuthorityController#update', '权限更新', 'gaeaAuthorityController', 'PUT#/gaeaAuthority', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11072, 'gaea-auth', '0', 'gaeaAuthorityController#insert', '权限新增', 'gaeaAuthorityController', 'POST#/gaeaAuthority', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11073, 'gaea-auth', '0', 'gaeaAuthorityController#detail', '权限详情', 'gaeaAuthorityController', 'GET#/gaeaAuthority/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11074, 'gaea-auth', '0', 'gaeaAuthorityController#deleteBatchIds', '权限批量删除', 'gaeaAuthorityController', 'POST#/gaeaAuthority/delete/batch', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11075, 'gaea-auth', '0', 'gaeaAuthorityController#deleteById', '权限删除', 'gaeaAuthorityController', 'DELETE#/gaeaAuthority/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11076, 'gaea-auth', '0', 'gaeaAuthorityController#pageList', '权限分页', 'gaeaAuthorityController', 'GET#/gaeaAuthority/pageList', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11077, 'gaea-auth', '0', 'gaeaLogController#update', '日志更新', 'gaeaLogController', 'PUT#/log', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11078, 'gaea-auth', '0', 'gaeaLogController#insert', '日志新增', 'gaeaLogController', 'POST#/log', NULL, NULL, NULL, 'lixiaoyan', '2021-03-12 17:09:13', '2021-04-13 17:42:09', 2);
INSERT INTO `gaea_authority` VALUES (11079, 'gaea-auth', '0', 'gaeaLogController#detail', '日志详情', 'gaeaLogController', 'GET#/log/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11080, 'gaea-auth', '0', 'gaeaLogController#deleteBatchIds', '日志批量删除', 'gaeaLogController', 'POST#/log/delete/batch', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11081, 'gaea-auth', '0', 'gaeaLogController#deleteById', '日志删除', 'gaeaLogController', 'DELETE#/log/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11082, 'gaea-auth', '0', 'gaeaLogController#pageList', '日志分页', 'gaeaLogController', 'GET#/log/pageList', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11083, 'gaea-auth', '0', 'gaeaMenuController#userInfo', '用户信息', 'gaeaMenuController', 'GET#/menu/userInfo', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11084, 'gaea-auth', '0', 'gaeaMenuController#getTree', '菜单数', 'gaeaMenuController', 'GET#/menu/tree', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11085, 'gaea-auth', '0', 'gaeaMenuController#getMenuInfoByOrg', '用户信息', 'gaeaMenuController', 'POST#/menu/menuUserInfoByOrg', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11086, 'gaea-auth', '0', 'gaeaMenuController#menuAuthority', '菜单绑定权限', 'gaeaMenuController', 'POST#/menu/mapper/authorities', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11087, 'gaea-auth', '0', 'gaeaMenuController#getRoleTree', '角色树', 'gaeaMenuController', 'GET#/menu/role/tree', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11088, 'gaea-auth', '0', 'gaeaMenuController#menuSelect', '菜单下来', 'gaeaMenuController', 'GET#/menu/menuSelect', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11089, 'gaea-auth', '0', 'gaeaMenuController#authorityTree', '设置权限', 'gaeaMenuController', 'GET#/menu/authority/tree/**/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11090, 'gaea-auth', '0', 'gaeaMenuController#update', '菜单更新', 'gaeaMenuController', 'PUT#/menu', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11091, 'gaea-auth', '0', 'gaeaMenuController#insert', '菜单新增', 'gaeaMenuController', 'POST#/menu', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11092, 'gaea-auth', '0', 'gaeaMenuController#detail', '菜单详情', 'gaeaMenuController', 'GET#/menu/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11093, 'gaea-auth', '0', 'gaeaMenuController#deleteBatchIds', '菜单批量删除', 'gaeaMenuController', 'POST#/menu/delete/batch', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11094, 'gaea-auth', '0', 'gaeaMenuController#deleteById', '菜单删除', 'gaeaMenuController', 'DELETE#/menu/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11095, 'gaea-auth', '0', 'gaeaMenuController#pageList', '菜单分页', 'gaeaMenuController', 'GET#/menu/pageList', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11127, 'gaea-auth', '0', 'gaeaOrgController#queryAllOrg', '查询所有组织', 'gaeaOrgController', 'GET#/org/queryAllOrg', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11128, 'gaea-auth', '0', 'gaeaOrgController#orgRoleTree', NULL, 'gaeaOrgController', 'GET#/org/user/role/tree/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11129, 'gaea-auth', '0', 'gaeaOrgController#orgSelect', '组织下拉', 'gaeaOrgController', 'GET#/org/orgSelect', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11130, 'gaea-auth', '0', 'gaeaOrgController#orgTree', '组织树', 'gaeaOrgController', 'GET#/org/tree', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11131, 'gaea-auth', '0', 'gaeaOrgController#saveOrg', '组织保存', 'gaeaOrgController', 'POST#/org/saveOrg', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11132, 'gaea-auth', '0', 'gaeaOrgController#updateOrg', '组织更新', 'gaeaOrgController', 'POST#/org/updateOrg', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11133, 'gaea-auth', '0', 'gaeaOrgController#orgRoleTreeSelected', '组织角色树', 'gaeaOrgController', 'GET#/org/role/tree', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11134, 'gaea-auth', '0', 'gaeaOrgController#update', '组织更新', 'gaeaOrgController', 'PUT#/org', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11135, 'gaea-auth', '0', 'gaeaOrgController#insert', '组织新增', 'gaeaOrgController', 'POST#/org', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11136, 'gaea-auth', '0', 'gaeaOrgController#detail', '组织详情', 'gaeaOrgController', 'GET#/org/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11137, 'gaea-auth', '0', 'gaeaOrgController#deleteBatchIds', '组织批量删除', 'gaeaOrgController', 'POST#/org/delete/batch', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11138, 'gaea-auth', '0', 'gaeaOrgController#deleteById', '组织删除', 'gaeaOrgController', 'DELETE#/org/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11139, 'gaea-auth', '0', 'gaeaOrgController#pageList', '组织分页', 'gaeaOrgController', 'GET#/org/pageList', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11143, 'gaea-auth', '0', 'gaeaRoleController#saveMenuActionTreeForRole', '保存角色权限', 'gaeaRoleController', 'POST#/role/roleMenuAuthorities', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11144, 'gaea-auth', '0', 'gaeaRoleController#update', '角色更新', 'gaeaRoleController', 'PUT#/role', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11145, 'gaea-auth', '0', 'gaeaRoleController#insert', '角色新增', 'gaeaRoleController', 'POST#/role', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11146, 'gaea-auth', '0', 'gaeaRoleController#detail', '角色详情', 'gaeaRoleController', 'GET#/role/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11147, 'gaea-auth', '0', 'gaeaRoleController#deleteBatchIds', '角色批量删除', 'gaeaRoleController', 'POST#/role/delete/batch', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11148, 'gaea-auth', '0', 'gaeaRoleController#deleteById', '角色删除', 'gaeaRoleController', 'DELETE#/role/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11149, 'gaea-auth', '0', 'gaeaRoleController#pageList', '角色分页', 'gaeaRoleController', 'GET#/role/pageList', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11158, 'gaea-auth', '0', 'gaeaUserController#resetPassword', '重置密码', 'gaeaUserController', 'POST#/user/resetPwd', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11159, 'gaea-auth', '0', 'gaeaUserController#saveRoleTree', '保存用户角色', 'gaeaUserController', 'POST#/user/saveRoleTree', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11161, 'gaea-auth', '0', 'gaeaUserController#updatePassword', '更新密码', 'gaeaUserController', 'POST#/user/updatePassword', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11162, 'gaea-auth', '0', 'gaeaUserController#update', '用户更新', 'gaeaUserController', 'PUT#/user', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11163, 'gaea-auth', '0', 'gaeaUserController#insert', '用户新增', 'gaeaUserController', 'POST#/user', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11164, 'gaea-auth', '0', 'gaeaUserController#detail', '用户详情', 'gaeaUserController', 'GET#/user/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11165, 'gaea-auth', '0', 'gaeaUserController#deleteBatchIds', '用户批量删除', 'gaeaUserController', 'POST#/user/delete/batch', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11166, 'gaea-auth', '0', 'gaeaUserController#deleteById', '用户删除', 'gaeaUserController', 'DELETE#/user/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11167, 'gaea-auth', '0', 'gaeaUserController#pageList', '用户分页', 'gaeaUserController', 'GET#/user/pageList', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11168, 'gaea-auth', '0', 'captchaController#get', NULL, 'captchaController', 'POST#/captcha/get', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11169, 'gaea-auth', '0', 'captchaController#check', NULL, 'captchaController', 'POST#/captcha/check', NULL, NULL, NULL, NULL, '2021-03-12 17:09:13', '2021-03-12 17:09:13', 1);
INSERT INTO `gaea_authority` VALUES (11171, 'gaea-business', '0', 'gaeaFileController#upload', '文件上传', 'gaeaFileController', 'POST#/file/upload', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11172, 'gaea-business', '0', 'gaeaFileController#export', '文件下载', 'gaeaFileController', 'GET#/file/download/**', NULL, NULL, NULL, 'admin', '2021-03-12 17:09:15', '2021-05-06 13:53:36', 2);
INSERT INTO `gaea_authority` VALUES (11173, 'gaea-business', '0', 'gaeaExportController#queryExportInfo', NULL, 'gaeaExportController', 'POST#/export/queryAdvanceExport', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11174, 'gaea-business', '0', 'gaeaExportController#export', '导出中心导出', 'gaeaExportController', 'POST#/export/saveExportLog', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11175, 'gaea-business', '0', 'gaeaExportController#detail', '导出中心详情', 'gaeaExportController', 'GET#/export/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11176, 'gaea-business', '0', 'gaeaExportController#pageList', '导出中心分页', 'gaeaExportController', 'GET#/export/pageList', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11177, 'gaea-business', '0', 'gaeaExportController#deleteBatchIds', '导出中心批量删除', 'gaeaExportController', 'POST#/export/delete/batch', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11178, 'gaea-business', '0', 'gaeaExportController#deleteById', '导出中心删除', 'gaeaExportController', 'DELETE#/export/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11179, 'gaea-business', '0', 'gaeaExportController#update', '导出中心更新', 'gaeaExportController', 'PUT#/export', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11180, 'gaea-business', '0', 'gaeaExportController#insert', '导出中心新增', 'gaeaExportController', 'POST#/export', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11181, 'gaea-business', '0', 'gaeaPushTemplateController#preview', '推送模板预览', 'gaeaPushTemplateController', 'POST#/gaeaPushTemplate/preview', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11182, 'gaea-business', '0', 'gaeaPushTemplateController#testSendPush', '推送模板测试发送', 'gaeaPushTemplateController', 'POST#/gaeaPushTemplate/testSendPush', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11183, 'gaea-business', '0', 'gaeaPushTemplateController#detail', '推送模板详情', 'gaeaPushTemplateController', 'GET#/gaeaPushTemplate/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11184, 'gaea-business', '0', 'gaeaPushTemplateController#pageList', '推送模板分页', 'gaeaPushTemplateController', 'GET#/gaeaPushTemplate/pageList', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11185, 'gaea-business', '0', 'gaeaPushTemplateController#deleteBatchIds', '推送模板批量删除', 'gaeaPushTemplateController', 'POST#/gaeaPushTemplate/delete/batch', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11186, 'gaea-business', '0', 'gaeaPushTemplateController#deleteById', '推送模板删除', 'gaeaPushTemplateController', 'DELETE#/gaeaPushTemplate/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11187, 'gaea-business', '0', 'gaeaPushTemplateController#update', '推送模板更新', 'gaeaPushTemplateController', 'PUT#/gaeaPushTemplate', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11188, 'gaea-business', '0', 'gaeaPushTemplateController#insert', '推送模板新增', 'gaeaPushTemplateController', 'POST#/gaeaPushTemplate', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11189, 'gaea-business', '0', 'gaeaPushHistoryController#getPushStatistics', '推送历史报表', 'gaeaPushHistoryController', 'POST#/gaeaPushHistory/getPushStatistics', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11190, 'gaea-business', '0', 'gaeaPushHistoryController#detail', '推送历史详情', 'gaeaPushHistoryController', 'GET#/gaeaPushHistory/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11191, 'gaea-business', '0', 'gaeaPushHistoryController#pageList', '推送历史分页', 'gaeaPushHistoryController', 'GET#/gaeaPushHistory/pageList', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11192, 'gaea-business', '0', 'gaeaPushHistoryController#deleteBatchIds', '推送历史批量删除', 'gaeaPushHistoryController', 'POST#/gaeaPushHistory/delete/batch', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11193, 'gaea-business', '0', 'gaeaPushHistoryController#deleteById', '推送历史删除', 'gaeaPushHistoryController', 'DELETE#/gaeaPushHistory/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11194, 'gaea-business', '0', 'gaeaPushHistoryController#update', '推送历史更新', 'gaeaPushHistoryController', 'PUT#/gaeaPushHistory', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11195, 'gaea-business', '0', 'gaeaPushHistoryController#insert', '推送历史新增', 'gaeaPushHistoryController', 'POST#/gaeaPushHistory', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11196, 'gaea-business', '0', 'gaeaDictItemController#detail', '数据字典项详情', 'gaeaDictItemController', 'GET#/gaeaDictItem/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11197, 'gaea-business', '0', 'gaeaDictItemController#pageList', '数据字典项分页', 'gaeaDictItemController', 'GET#/gaeaDictItem/pageList', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11198, 'gaea-business', '0', 'gaeaDictItemController#deleteBatchIds', '数据字典项批量删除', 'gaeaDictItemController', 'POST#/gaeaDictItem/delete/batch', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11199, 'gaea-business', '0', 'gaeaDictItemController#deleteById', '数据字典项删除', 'gaeaDictItemController', 'DELETE#/gaeaDictItem/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11200, 'gaea-business', '0', 'gaeaDictItemController#update', '数据字典项更新', 'gaeaDictItemController', 'PUT#/gaeaDictItem', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11201, 'gaea-business', '0', 'gaeaDictItemController#insert', '数据字典项新增', 'gaeaDictItemController', 'POST#/gaeaDictItem', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11203, 'gaea-business', '0', 'gaeaDictController#select', '数据字典下拉', 'gaeaDictController', 'GET#/gaeaDict/select/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11204, 'gaea-business', '0', 'gaeaDictController#detail', '数据字典详情', 'gaeaDictController', 'GET#/gaeaDict/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11205, 'gaea-business', '0', 'gaeaDictController#pageList', '数据字典分页', 'gaeaDictController', 'GET#/gaeaDict/pageList', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11206, 'gaea-business', '0', 'gaeaDictController#deleteBatchIds', '数据字典批量删除', 'gaeaDictController', 'POST#/gaeaDict/delete/batch', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11207, 'gaea-business', '0', 'gaeaDictController#deleteById', '数据字典删除', 'gaeaDictController', 'DELETE#/gaeaDict/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11208, 'gaea-business', '0', 'gaeaDictController#update', '数据字典更新', 'gaeaDictController', 'PUT#/gaeaDict', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11209, 'gaea-business', '0', 'gaeaDictController#insert', '数据字典新增', 'gaeaDictController', 'POST#/gaeaDict', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11210, 'gaea-business', '0', 'gaeaHelpController#demo', NULL, 'gaeaHelpController', 'GET#/gaeaHelp/demo', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11211, 'gaea-business', '0', 'gaeaHelpController#listAll', '帮助中心查询所有', 'gaeaHelpController', 'GET#/gaeaHelp/list', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11212, 'gaea-business', '0', 'gaeaHelpController#detail', '帮助中心详情', 'gaeaHelpController', 'GET#/gaeaHelp/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11213, 'gaea-business', '0', 'gaeaHelpController#pageList', '帮助中心分页', 'gaeaHelpController', 'GET#/gaeaHelp/pageList', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11214, 'gaea-business', '0', 'gaeaHelpController#deleteBatchIds', '帮助中心批量删除', 'gaeaHelpController', 'POST#/gaeaHelp/delete/batch', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11215, 'gaea-business', '0', 'gaeaHelpController#deleteById', '帮助中心删除', 'gaeaHelpController', 'DELETE#/gaeaHelp/**', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11216, 'gaea-business', '0', 'gaeaHelpController#update', '帮助中心更新', 'gaeaHelpController', 'PUT#/gaeaHelp', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11217, 'gaea-business', '0', 'gaeaHelpController#insert', '帮助中心新增', 'gaeaHelpController', 'POST#/gaeaHelp', NULL, NULL, NULL, NULL, '2021-03-12 17:09:15', '2021-03-12 17:09:15', 1);
INSERT INTO `gaea_authority` VALUES (11218, 'gaea-auth', '0', 'gaeaLogController#exportLogToFile', '操作日志导出', 'gaeaLogController', 'POST#/log/exportLogToFile', NULL, NULL, NULL, NULL, '2021-03-15 10:24:36', '2021-03-15 10:24:36', 1);
INSERT INTO `gaea_authority` VALUES (11219, 'gaea-business', '0', 'gaeaRulesController#executeRules', '规则执行', 'gaeaRulesController', 'POST#/gaeaRules/executeRules', NULL, NULL, NULL, NULL, '2021-03-16 12:32:47', '2021-03-16 12:32:47', 1);
INSERT INTO `gaea_authority` VALUES (11220, 'gaea-business', '0', 'gaeaRulesController#update', '规则引擎更新', 'gaeaRulesController', 'PUT#/gaeaRules', NULL, NULL, NULL, NULL, '2021-03-16 12:32:47', '2021-03-16 12:32:47', 1);
INSERT INTO `gaea_authority` VALUES (11221, 'gaea-business', '0', 'gaeaRulesController#insert', '规则引擎新增', 'gaeaRulesController', 'POST#/gaeaRules', NULL, NULL, NULL, NULL, '2021-03-16 12:32:47', '2021-03-16 12:32:47', 1);
INSERT INTO `gaea_authority` VALUES (11222, 'gaea-business', '0', 'gaeaRulesController#detail', '规则引擎详情', 'gaeaRulesController', 'GET#/gaeaRules/**', NULL, NULL, NULL, NULL, '2021-03-16 12:32:47', '2021-03-16 12:32:47', 1);
INSERT INTO `gaea_authority` VALUES (11223, 'gaea-business', '0', 'gaeaRulesController#pageList', '规则引擎分页', 'gaeaRulesController', 'GET#/gaeaRules/pageList', NULL, NULL, NULL, NULL, '2021-03-16 12:32:47', '2021-03-16 12:32:47', 1);
INSERT INTO `gaea_authority` VALUES (11224, 'gaea-business', '0', 'gaeaRulesController#deleteById', '规则引擎删除', 'gaeaRulesController', 'DELETE#/gaeaRules/**', NULL, NULL, NULL, NULL, '2021-03-16 12:32:47', '2021-03-16 12:32:47', 1);
INSERT INTO `gaea_authority` VALUES (11225, 'gaea-business', '0', 'gaeaRulesController#deleteBatchIds', '规则引擎批量删除', 'gaeaRulesController', 'POST#/gaeaRules/delete/batch', NULL, NULL, NULL, NULL, '2021-03-16 12:32:47', '2021-03-16 12:32:47', 1);
INSERT INTO `gaea_authority` VALUES (11629, 'gaea-business', '0', 'gaeaRulesHistoryController#detail', NULL, 'gaeaRulesHistoryController', 'GET#/gaeaRulesHistory/**', NULL, NULL, NULL, NULL, '2021-03-19 21:55:30', '2021-03-19 21:55:30', 1);
INSERT INTO `gaea_authority` VALUES (11630, 'gaea-business', '0', 'gaeaRulesHistoryController#deleteById', NULL, 'gaeaRulesHistoryController', 'DELETE#/gaeaRulesHistory/**', NULL, NULL, NULL, NULL, '2021-03-19 21:55:30', '2021-03-19 21:55:30', 1);
INSERT INTO `gaea_authority` VALUES (11631, 'gaea-business', '0', 'gaeaRulesHistoryController#deleteBatchIds', NULL, 'gaeaRulesHistoryController', 'POST#/gaeaRulesHistory/delete/batch', NULL, NULL, NULL, NULL, '2021-03-19 21:55:30', '2021-03-19 21:55:30', 1);
INSERT INTO `gaea_authority` VALUES (11632, 'gaea-business', '0', 'gaeaRulesHistoryController#pageList', NULL, 'gaeaRulesHistoryController', 'GET#/gaeaRulesHistory/pageList', NULL, NULL, NULL, NULL, '2021-03-19 21:55:30', '2021-03-19 21:55:30', 1);
INSERT INTO `gaea_authority` VALUES (11633, 'gaea-business', '0', 'gaeaRulesHistoryController#update', NULL, 'gaeaRulesHistoryController', 'PUT#/gaeaRulesHistory', NULL, NULL, NULL, NULL, '2021-03-19 21:55:30', '2021-03-19 21:55:30', 1);
INSERT INTO `gaea_authority` VALUES (11634, 'gaea-business', '0', 'gaeaRulesHistoryController#insert', NULL, 'gaeaRulesHistoryController', 'POST#/gaeaRulesHistory', NULL, NULL, NULL, NULL, '2021-03-19 21:55:30', '2021-03-19 21:55:30', 1);
INSERT INTO `gaea_authority` VALUES (11635, 'gaea-business', '0', 'baseController#get', NULL, 'baseController', 'GET#/my/rest/account', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11636, 'gaea-business', '0', 'dataSetTransformController#deleteById', NULL, 'dataSetTransformController', 'DELETE#/dataSetTransform/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11637, 'gaea-business', '0', 'dataSetTransformController#detail', NULL, 'dataSetTransformController', 'GET#/dataSetTransform/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11638, 'gaea-business', '0', 'dataSetTransformController#deleteBatchIds', NULL, 'dataSetTransformController', 'POST#/dataSetTransform/delete/batch', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11639, 'gaea-business', '0', 'dataSetTransformController#pageList', NULL, 'dataSetTransformController', 'GET#/dataSetTransform/pageList', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11640, 'gaea-business', '0', 'dataSetTransformController#update', NULL, 'dataSetTransformController', 'PUT#/dataSetTransform', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11641, 'gaea-business', '0', 'dataSetTransformController#insert', NULL, 'dataSetTransformController', 'POST#/dataSetTransform', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11642, 'gaea-business', '0', 'dataSourceController#testConnection', NULL, 'dataSourceController', 'POST#/dataSource/testConnection', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11643, 'gaea-business', '0', 'dataSourceController#deleteById', NULL, 'dataSourceController', 'DELETE#/dataSource/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11644, 'gaea-business', '0', 'dataSourceController#detail', NULL, 'dataSourceController', 'GET#/dataSource/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11645, 'gaea-business', '0', 'dataSourceController#deleteBatchIds', NULL, 'dataSourceController', 'POST#/dataSource/delete/batch', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11646, 'gaea-business', '0', 'dataSourceController#pageList', NULL, 'dataSourceController', 'GET#/dataSource/pageList', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11647, 'gaea-business', '0', 'dataSourceController#update', NULL, 'dataSourceController', 'PUT#/dataSource', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11648, 'gaea-business', '0', 'dataSourceController#insert', NULL, 'dataSourceController', 'POST#/dataSource', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11649, 'gaea-business', '0', 'dataSetController#deleteById', NULL, 'dataSetController', 'DELETE#/dataSet/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11651, 'gaea-business', '0', 'dataSetController#deleteBatchIds', NULL, 'dataSetController', 'POST#/dataSet/delete/batch', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11652, 'gaea-business', '0', 'dataSetController#pageList', NULL, 'dataSetController', 'GET#/dataSet/pageList', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11653, 'gaea-business', '0', 'dataSetController#update', NULL, 'dataSetController', 'PUT#/dataSet', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11654, 'gaea-business', '0', 'dataSetController#insert', NULL, 'dataSetController', 'POST#/dataSet', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11655, 'gaea-business', '0', 'dataSetParamController#deleteById', NULL, 'dataSetParamController', 'DELETE#/dataSetParam/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11656, 'gaea-business', '0', 'dataSetParamController#detail', NULL, 'dataSetParamController', 'GET#/dataSetParam/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11657, 'gaea-business', '0', 'dataSetParamController#deleteBatchIds', NULL, 'dataSetParamController', 'POST#/dataSetParam/delete/batch', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11658, 'gaea-business', '0', 'dataSetParamController#pageList', NULL, 'dataSetParamController', 'GET#/dataSetParam/pageList', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11659, 'gaea-business', '0', 'dataSetParamController#update', NULL, 'dataSetParamController', 'PUT#/dataSetParam', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11660, 'gaea-business', '0', 'dataSetParamController#insert', NULL, 'dataSetParamController', 'POST#/dataSetParam', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11661, 'gaea-business', '0', 'deviceInfoController#detail', NULL, 'deviceInfoController', 'GET#/deviceInfo/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11662, 'gaea-business', '0', 'deviceInfoController#deleteById', NULL, 'deviceInfoController', 'DELETE#/deviceInfo/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11663, 'gaea-business', '0', 'deviceInfoController#deleteBatchIds', NULL, 'deviceInfoController', 'POST#/deviceInfo/delete/batch', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11664, 'gaea-business', '0', 'deviceInfoController#pageList', NULL, 'deviceInfoController', 'GET#/deviceInfo/pageList', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11665, 'gaea-business', '0', 'deviceInfoController#update', NULL, 'deviceInfoController', 'PUT#/deviceInfo', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11666, 'gaea-business', '0', 'deviceInfoController#insert', NULL, 'deviceInfoController', 'POST#/deviceInfo', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11667, 'gaea-business', '0', 'deviceLogDetailController#detail', NULL, 'deviceLogDetailController', 'GET#/deviceLogDetail/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11668, 'gaea-business', '0', 'deviceLogDetailController#deleteById', NULL, 'deviceLogDetailController', 'DELETE#/deviceLogDetail/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11669, 'gaea-business', '0', 'deviceLogDetailController#deleteBatchIds', NULL, 'deviceLogDetailController', 'POST#/deviceLogDetail/delete/batch', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11670, 'gaea-business', '0', 'deviceLogDetailController#pageList', NULL, 'deviceLogDetailController', 'GET#/deviceLogDetail/pageList', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11671, 'gaea-business', '0', 'deviceLogDetailController#update', NULL, 'deviceLogDetailController', 'PUT#/deviceLogDetail', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11672, 'gaea-business', '0', 'deviceLogDetailController#insert', NULL, 'deviceLogDetailController', 'POST#/deviceLogDetail', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11673, 'gaea-business', '0', 'deviceModelController#detail', NULL, 'deviceModelController', 'GET#/deviceModel/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11674, 'gaea-business', '0', 'deviceModelController#deleteById', NULL, 'deviceModelController', 'DELETE#/deviceModel/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11675, 'gaea-business', '0', 'deviceModelController#deleteBatchIds', NULL, 'deviceModelController', 'POST#/deviceModel/delete/batch', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11676, 'gaea-business', '0', 'deviceModelController#pageList', NULL, 'deviceModelController', 'GET#/deviceModel/pageList', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11677, 'gaea-business', '0', 'deviceModelController#update', NULL, 'deviceModelController', 'PUT#/deviceModel', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11678, 'gaea-business', '0', 'deviceModelController#insert', NULL, 'deviceModelController', 'POST#/deviceModel', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11679, 'gaea-business', '0', 'alipayConfigController#detail', NULL, 'alipayConfigController', 'GET#/alipayConfig/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11680, 'gaea-business', '0', 'alipayConfigController#deleteById', NULL, 'alipayConfigController', 'DELETE#/alipayConfig/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11681, 'gaea-business', '0', 'alipayConfigController#deleteBatchIds', NULL, 'alipayConfigController', 'POST#/alipayConfig/delete/batch', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11682, 'gaea-business', '0', 'alipayConfigController#pageList', NULL, 'alipayConfigController', 'GET#/alipayConfig/pageList', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11683, 'gaea-business', '0', 'alipayConfigController#update', NULL, 'alipayConfigController', 'PUT#/alipayConfig', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11684, 'gaea-business', '0', 'alipayConfigController#insert', NULL, 'alipayConfigController', 'POST#/alipayConfig', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11685, 'gaea-business', '0', 'gaeaDictController#selectTypecodes', NULL, 'gaeaDictController', 'GET#/gaeaDict/selectAll/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11686, 'gaea-business', '0', 'actDeModelController#publishModel', NULL, 'actDeModelController', 'GET#/actDeModel/publish/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11687, 'gaea-business', '0', 'actDeModelController#deleteById', NULL, 'actDeModelController', 'DELETE#/actDeModel/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11688, 'gaea-business', '0', 'actDeModelController#detail', NULL, 'actDeModelController', 'GET#/actDeModel/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11689, 'gaea-business', '0', 'actDeModelController#deleteBatchIds', NULL, 'actDeModelController', 'POST#/actDeModel/delete/batch', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11690, 'gaea-business', '0', 'actDeModelController#pageList', NULL, 'actDeModelController', 'GET#/actDeModel/pageList', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11691, 'gaea-business', '0', 'actDeModelController#update', NULL, 'actDeModelController', 'PUT#/actDeModel', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11692, 'gaea-business', '0', 'actDeModelController#insert', NULL, 'actDeModelController', 'POST#/actDeModel', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11715, 'gaea-business', '0', 'appDefinitionResource#importAppDefinition', NULL, 'appDefinitionResource', 'POST#/app/rest/app-definitions/**/import', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11716, 'gaea-business', '0', 'appDefinitionResource#importAppDefinition', NULL, 'appDefinitionResource', 'POST#/app/rest/app-definitions/import', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11717, 'gaea-business', '0', 'appDefinitionResource#publishAppDefinition', NULL, 'appDefinitionResource', 'POST#/app/rest/app-definitions/**/publish', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11718, 'gaea-business', '0', 'appDefinitionResource#getAppDefinition', NULL, 'appDefinitionResource', 'GET#/app/rest/app-definitions/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11719, 'gaea-business', '0', 'appDefinitionResource#getAppDefinitionHistory', NULL, 'appDefinitionResource', 'GET#/app/rest/app-definitions/**/history/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11720, 'gaea-business', '0', 'appDefinitionResource#updateAppDefinition', NULL, 'appDefinitionResource', 'PUT#/app/rest/app-definitions/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11721, 'gaea-business', '0', 'appDefinitionResource#exportAppDefinition', NULL, 'appDefinitionResource', 'GET#/app/rest/app-definitions/**/export', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11722, 'gaea-business', '0', 'appDefinitionResource#exportDeployableAppDefinition', NULL, 'appDefinitionResource', 'GET#/app/rest/app-definitions/**/export-bar', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11723, 'gaea-business', '0', 'appDefinitionResource#importAppDefinitionText', NULL, 'appDefinitionResource', 'POST#/app/rest/app-definitions/text/import', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11724, 'gaea-business', '0', 'appDefinitionResource#importAppDefinitionText', NULL, 'appDefinitionResource', 'POST#/app/rest/app-definitions/**/text/import', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11725, 'gaea-business', '0', 'decisionTableResource#getDecisionTables', NULL, 'decisionTableResource', 'GET#/app/rest/decision-table-models/values', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11726, 'gaea-business', '0', 'decisionTableResource#exportDecisionTable', NULL, 'decisionTableResource', 'GET#/app/rest/decision-table-models/**/export', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11727, 'gaea-business', '0', 'decisionTableResource#exportHistoricDecisionTable', NULL, 'decisionTableResource', 'GET#/app/rest/decision-table-models/history/**/export', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11728, 'gaea-business', '0', 'decisionTableResource#importDecisionTable', NULL, 'decisionTableResource', 'POST#/app/rest/decision-table-models/import-decision-table', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11729, 'gaea-business', '0', 'decisionTableResource#getDecisionTable', NULL, 'decisionTableResource', 'GET#/app/rest/decision-table-models/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11730, 'gaea-business', '0', 'decisionTableResource#getHistoricDecisionTable', NULL, 'decisionTableResource', 'GET#/app/rest/decision-table-models/**/history/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11731, 'gaea-business', '0', 'decisionTableResource#getHistoricDecisionTable', NULL, 'decisionTableResource', 'GET#/app/rest/decision-table-models/history/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11732, 'gaea-business', '0', 'decisionTableResource#saveDecisionTable', NULL, 'decisionTableResource', 'PUT#/app/rest/decision-table-models/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11733, 'gaea-business', '0', 'decisionTableResource#importDecisionTableText', NULL, 'decisionTableResource', 'POST#/app/rest/decision-table-models/import-decision-table-text', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11734, 'gaea-business', '0', 'formResource#getForms', NULL, 'formResource', 'GET#/app/rest/form-models/values', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11735, 'gaea-business', '0', 'formResource#saveForm', NULL, 'formResource', 'PUT#/app/rest/form-models/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11736, 'gaea-business', '0', 'formResource#getFormHistory', NULL, 'formResource', 'GET#/app/rest/form-models/**/history/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11737, 'gaea-business', '0', 'formResource#getForm', NULL, 'formResource', 'GET#/app/rest/form-models/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11738, 'gaea-business', '0', 'modelResource#getModel', NULL, 'modelResource', 'GET#/app/rest/models/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11739, 'gaea-business', '0', 'modelResource#saveModel', NULL, 'modelResource', 'POST#/app/rest/models/**/editor/json', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11740, 'gaea-business', '0', 'modelResource#importNewVersion', NULL, 'modelResource', 'POST#/app/rest/models/**/newversion', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11741, 'gaea-business', '0', 'modelResource#deleteModel', NULL, 'modelResource', 'DELETE#/app/rest/models/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11742, 'gaea-business', '0', 'modelResource#getModelThumbnail', NULL, 'modelResource', 'GET#/app/rest/models/**/thumbnail', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11743, 'gaea-business', '0', 'modelResource#updateModel', NULL, 'modelResource', 'PUT#/app/rest/models/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11744, 'gaea-business', '0', 'modelResource#getModelJSON', NULL, 'modelResource', 'GET#/app/rest/models/**/editor/json', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11745, 'gaea-business', '0', 'modelValidationRestResource#validate', NULL, 'modelValidationRestResource', 'POST#/app/rest/model/validate', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11746, 'gaea-business', '0', 'caseModelsResource#getDecisionTables', NULL, 'caseModelsResource', 'GET#/app/rest/case-models', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11747, 'gaea-business', '0', 'modelRelationResource#getModelRelations', NULL, 'modelRelationResource', 'GET#/app/rest/models/**/parent-relations', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11748, 'gaea-business', '0', 'modelBpmnResource#getProcessModelBpmn20Xml', NULL, 'modelBpmnResource', 'GET#/app/rest/models/**/bpmn20', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11749, 'gaea-business', '0', 'modelBpmnResource#getHistoricProcessModelBpmn20Xml', NULL, 'modelBpmnResource', 'GET#/app/rest/models/**/history/**/bpmn20', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11750, 'gaea-business', '0', 'editorDisplayJsonClientResource#getModelJSON', NULL, 'editorDisplayJsonClientResource', 'GET#/app/rest/models/**/model-json', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11751, 'gaea-business', '0', 'editorDisplayJsonClientResource#getModelHistoryJSON', NULL, 'editorDisplayJsonClientResource', 'GET#/app/rest/models/**/history/**/model-json', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11752, 'gaea-business', '0', 'formsResource#getForms', NULL, 'formsResource', 'GET#/app/rest/form-models', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11753, 'gaea-business', '0', 'stencilSetResource#getStencilSetForEditor', NULL, 'stencilSetResource', 'GET#/app/rest/stencil-sets/editor', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11754, 'gaea-business', '0', 'stencilSetResource#getCmmnStencilSetForEditor', NULL, 'stencilSetResource', 'GET#/app/rest/stencil-sets/cmmneditor', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11755, 'gaea-business', '0', 'modelHistoryResource#getModelHistoryCollection', NULL, 'modelHistoryResource', 'GET#/app/rest/models/**/history', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11756, 'gaea-business', '0', 'modelHistoryResource#getProcessModelHistory', NULL, 'modelHistoryResource', 'GET#/app/rest/models/**/history/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11757, 'gaea-business', '0', 'modelHistoryResource#executeProcessModelHistoryAction', NULL, 'modelHistoryResource', 'POST#/app/rest/models/**/history/**', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11758, 'gaea-business', '0', 'modelsResource#getModelsToIncludeInAppDefinition', NULL, 'modelsResource', 'GET#/app/rest/models-for-app-definition', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11759, 'gaea-business', '0', 'modelsResource#getCmmnModelsToIncludeInAppDefinition', NULL, 'modelsResource', 'GET#/app/rest/cmmn-models-for-app-definition', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11760, 'gaea-business', '0', 'modelsResource#importProcessModel', NULL, 'modelsResource', 'POST#/app/rest/import-process-model', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11761, 'gaea-business', '0', 'modelsResource#importCaseModel', NULL, 'modelsResource', 'POST#/app/rest/import-case-model', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11762, 'gaea-business', '0', 'modelsResource#getModels', NULL, 'modelsResource', 'GET#/app/rest/models', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11763, 'gaea-business', '0', 'modelsResource#createModel', NULL, 'modelsResource', 'POST#/app/rest/models', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11764, 'gaea-business', '0', 'modelsResource#duplicateModel', NULL, 'modelsResource', 'POST#/app/rest/models/**/clone', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11765, 'gaea-business', '0', 'modelsResource#importProcessModelText', NULL, 'modelsResource', 'POST#/app/rest/import-process-model/text', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11766, 'gaea-business', '0', 'modelsResource#importCaseModelText', NULL, 'modelsResource', 'POST#/app/rest/import-case-model/text', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11767, 'gaea-business', '0', 'modelCmmnResource#getProcessModelCmmnXml', NULL, 'modelCmmnResource', 'GET#/app/rest/models/**/cmmn', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11768, 'gaea-business', '0', 'modelCmmnResource#getHistoricProcessModelCmmnXml', NULL, 'modelCmmnResource', 'GET#/app/rest/models/**/history/**/cmmn', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11769, 'gaea-business', '0', 'decisionTablesResource#getDecisionTables', NULL, 'decisionTablesResource', 'GET#/app/rest/decision-table-models', NULL, NULL, NULL, NULL, '2021-03-22 11:19:34', '2021-03-22 11:19:34', 1);
INSERT INTO `gaea_authority` VALUES (11787, 'gaea-business', '0', 'dataSetController#testTransform', NULL, 'dataSetController', 'POST#/dataSet/testTransform', NULL, NULL, NULL, NULL, '2021-03-24 09:58:20', '2021-03-24 09:58:20', 1);
INSERT INTO `gaea_authority` VALUES (11788, 'gaea-business', '0', 'dataSetParamController#verification', NULL, 'dataSetParamController', 'POST#/dataSetParam/verification', NULL, NULL, NULL, NULL, '2021-03-24 09:58:20', '2021-03-24 09:58:20', 1);
INSERT INTO `gaea_authority` VALUES (11834, 'gaea-business', '0', 'dataSetController#queryAllDataSet', NULL, 'dataSetController', 'GET#/dataSet/queryAllDataSet', NULL, NULL, NULL, NULL, '2021-03-26 15:33:36', '2021-03-26 15:33:36', 1);
INSERT INTO `gaea_authority` VALUES (11835, 'gaea-business', '0', 'dataSourceController#queryAllDataSource', NULL, 'dataSourceController', 'GET#/dataSource/queryAllDataSource', NULL, NULL, NULL, NULL, '2021-03-26 15:33:36', '2021-03-26 15:33:36', 1);
INSERT INTO `gaea_authority` VALUES (11846, 'gaea-business', '0', 'gaeaUiI18nController#detail', NULL, 'gaeaUiI18nController', 'GET#/gaeaUiI18n/**', NULL, NULL, NULL, NULL, '2021-03-26 15:33:36', '2021-03-26 15:33:36', 1);
INSERT INTO `gaea_authority` VALUES (11847, 'gaea-business', '0', 'gaeaUiI18nController#update', NULL, 'gaeaUiI18nController', 'PUT#/gaeaUiI18n', NULL, NULL, NULL, NULL, '2021-03-26 15:33:36', '2021-03-26 15:33:36', 1);
INSERT INTO `gaea_authority` VALUES (11848, 'gaea-business', '0', 'gaeaUiI18nController#insert', NULL, 'gaeaUiI18nController', 'POST#/gaeaUiI18n', NULL, NULL, NULL, NULL, '2021-03-26 15:33:36', '2021-03-26 15:33:36', 1);
INSERT INTO `gaea_authority` VALUES (11849, 'gaea-business', '0', 'gaeaUiI18nController#pageList', NULL, 'gaeaUiI18nController', 'GET#/gaeaUiI18n/pageList', NULL, NULL, NULL, NULL, '2021-03-26 15:33:36', '2021-03-26 15:33:36', 1);
INSERT INTO `gaea_authority` VALUES (11850, 'gaea-business', '0', 'gaeaUiI18nController#deleteById', NULL, 'gaeaUiI18nController', 'DELETE#/gaeaUiI18n/**', NULL, NULL, NULL, NULL, '2021-03-26 15:33:36', '2021-03-26 15:33:36', 1);
INSERT INTO `gaea_authority` VALUES (11851, 'gaea-business', '0', 'gaeaUiI18nController#deleteBatchIds', NULL, 'gaeaUiI18nController', 'POST#/gaeaUiI18n/delete/batch', NULL, NULL, NULL, NULL, '2021-03-26 15:33:36', '2021-03-26 15:33:36', 1);
INSERT INTO `gaea_authority` VALUES (11888, 'gaea-business', '0', 'dynamicController#execute', NULL, 'dynamicController', 'POST#/custom/dynamic/execute', NULL, NULL, NULL, NULL, '2021-03-29 14:14:24', '2021-03-29 14:14:24', 1);
INSERT INTO `gaea_authority` VALUES (11889, 'gaea-business', '0', 'dynamicController#runTest', NULL, 'dynamicController', 'POST#/custom/dynamic/run/test', NULL, NULL, NULL, NULL, '2021-03-29 14:14:24', '2021-03-29 14:14:24', 1);
INSERT INTO `gaea_authority` VALUES (11898, 'gaea-business', '0', 'infAppController#add', NULL, 'infAppController', 'POST#/inf/app/add', NULL, NULL, NULL, NULL, '2021-03-29 14:14:24', '2021-03-29 14:14:24', 1);
INSERT INTO `gaea_authority` VALUES (11899, 'gaea-business', '0', 'infAppController#edit', NULL, 'infAppController', 'POST#/inf/app/edit', NULL, NULL, NULL, NULL, '2021-03-29 14:14:24', '2021-03-29 14:14:24', 1);
INSERT INTO `gaea_authority` VALUES (11900, 'gaea-business', '0', 'infAppController#dropDown', NULL, 'infAppController', 'GET#/inf/app/select/dropDown', NULL, NULL, NULL, NULL, '2021-03-29 14:14:24', '2021-03-29 14:14:24', 1);
INSERT INTO `gaea_authority` VALUES (11901, 'gaea-business', '0', 'infAppController#updateSecret', NULL, 'infAppController', 'GET#/inf/app/appSecret/**', NULL, NULL, NULL, NULL, '2021-03-29 14:14:24', '2021-03-29 14:14:24', 1);
INSERT INTO `gaea_authority` VALUES (11902, 'gaea-business', '0', 'infAppController#update', NULL, 'infAppController', 'PUT#/inf/app', NULL, NULL, NULL, NULL, '2021-03-29 14:14:24', '2021-03-29 14:14:24', 1);
INSERT INTO `gaea_authority` VALUES (11903, 'gaea-business', '0', 'infAppController#insert', NULL, 'infAppController', 'POST#/inf/app', NULL, NULL, NULL, NULL, '2021-03-29 14:14:24', '2021-03-29 14:14:24', 1);
INSERT INTO `gaea_authority` VALUES (11904, 'gaea-business', '0', 'infAppController#detail', NULL, 'infAppController', 'GET#/inf/app/**', NULL, NULL, NULL, NULL, '2021-03-29 14:14:24', '2021-03-29 14:14:24', 1);
INSERT INTO `gaea_authority` VALUES (11905, 'gaea-business', '0', 'infAppController#deleteById', NULL, 'infAppController', 'DELETE#/inf/app/**', NULL, NULL, NULL, NULL, '2021-03-29 14:14:24', '2021-03-29 14:14:24', 1);
INSERT INTO `gaea_authority` VALUES (11906, 'gaea-business', '0', 'infAppController#deleteBatchIds', NULL, 'infAppController', 'POST#/inf/app/delete/batch', NULL, NULL, NULL, NULL, '2021-03-29 14:14:24', '2021-03-29 14:14:24', 1);
INSERT INTO `gaea_authority` VALUES (11907, 'gaea-business', '0', 'infAppController#pageList', NULL, 'infAppController', 'GET#/inf/app/pageList', NULL, NULL, NULL, NULL, '2021-03-29 14:14:24', '2021-03-29 14:14:24', 1);
INSERT INTO `gaea_authority` VALUES (11908, 'gaea-business', '0', 'infController#add', NULL, 'infController', 'POST#/inf/add', NULL, NULL, NULL, NULL, '2021-03-29 14:14:24', '2021-03-29 14:14:24', 1);
INSERT INTO `gaea_authority` VALUES (11910, 'gaea-business', '0', 'infController#authorizeApps', NULL, 'infController', 'POST#/inf/authorize/app/**', NULL, NULL, NULL, NULL, '2021-03-29 14:14:24', '2021-03-29 14:14:24', 1);
INSERT INTO `gaea_authority` VALUES (11911, 'gaea-business', '0', 'infController#queryById', NULL, 'infController', 'GET#/inf/id/**', NULL, NULL, NULL, NULL, '2021-03-29 14:14:24', '2021-03-29 14:14:24', 1);
INSERT INTO `gaea_authority` VALUES (11912, 'gaea-business', '0', 'infController#edit', NULL, 'infController', 'POST#/inf/edit', NULL, NULL, NULL, NULL, '2021-03-29 14:14:24', '2021-03-29 14:14:24', 1);
INSERT INTO `gaea_authority` VALUES (11913, 'gaea-business', '0', 'infController#online', NULL, 'infController', 'GET#/inf/online/**', NULL, NULL, NULL, NULL, '2021-03-29 14:14:24', '2021-03-29 14:14:24', 1);
INSERT INTO `gaea_authority` VALUES (11914, 'gaea-business', '0', 'infController#offline', NULL, 'infController', 'GET#/inf/offline/**', NULL, NULL, NULL, NULL, '2021-03-29 14:14:24', '2021-03-29 14:14:24', 1);
INSERT INTO `gaea_authority` VALUES (11915, 'gaea-business', '0', 'infController#update', NULL, 'infController', 'PUT#/inf', NULL, NULL, NULL, NULL, '2021-03-29 14:14:24', '2021-03-29 14:14:24', 1);
INSERT INTO `gaea_authority` VALUES (11916, 'gaea-business', '0', 'infController#insert', NULL, 'infController', 'POST#/inf', NULL, NULL, NULL, NULL, '2021-03-29 14:14:24', '2021-03-29 14:14:24', 1);
INSERT INTO `gaea_authority` VALUES (11917, 'gaea-business', '0', 'infController#deleteById', NULL, 'infController', 'DELETE#/inf/**', NULL, NULL, NULL, NULL, '2021-03-29 14:14:24', '2021-03-29 14:14:24', 1);
INSERT INTO `gaea_authority` VALUES (11918, 'gaea-business', '0', 'infController#deleteBatchIds', NULL, 'infController', 'POST#/inf/delete/batch', NULL, NULL, NULL, NULL, '2021-03-29 14:14:24', '2021-03-29 14:14:24', 1);
INSERT INTO `gaea_authority` VALUES (11919, 'gaea-business', '0', 'infController#pageList', NULL, 'infController', 'GET#/inf/pageList', NULL, NULL, NULL, NULL, '2021-03-29 14:14:24', '2021-03-29 14:14:24', 1);
INSERT INTO `gaea_authority` VALUES (12029, 'gaea-business', '0', 'reportController#update', NULL, 'reportController', 'PUT#/report', NULL, NULL, NULL, NULL, '2021-03-30 17:34:08', '2021-03-30 17:34:08', 1);
INSERT INTO `gaea_authority` VALUES (12030, 'gaea-business', '0', 'reportController#detail', NULL, 'reportController', 'GET#/report/**', NULL, NULL, NULL, NULL, '2021-03-30 17:34:08', '2021-03-30 17:34:08', 1);
INSERT INTO `gaea_authority` VALUES (12031, 'gaea-business', '0', 'reportController#deleteById', NULL, 'reportController', 'DELETE#/report/**', NULL, NULL, NULL, NULL, '2021-03-30 17:34:08', '2021-03-30 17:34:08', 1);
INSERT INTO `gaea_authority` VALUES (12032, 'gaea-business', '0', 'reportController#pageList', NULL, 'reportController', 'GET#/report/pageList', NULL, NULL, NULL, NULL, '2021-03-30 17:34:08', '2021-03-30 17:34:08', 1);
INSERT INTO `gaea_authority` VALUES (12033, 'gaea-business', '0', 'reportController#deleteBatchIds', NULL, 'reportController', 'POST#/report/delete/batch', NULL, NULL, NULL, NULL, '2021-03-30 17:34:08', '2021-03-30 17:34:08', 1);
INSERT INTO `gaea_authority` VALUES (12042, 'gaea-business', '0', 'gaeaUiI18nController#scan', NULL, 'gaeaUiI18nController', 'POST#/gaeaUiI18n/scan', NULL, NULL, NULL, NULL, '2021-03-30 17:34:08', '2021-03-30 17:34:08', 1);
INSERT INTO `gaea_authority` VALUES (12043, 'gaea-business', '0', 'gaeaUiI18nController#getI18nFields', NULL, 'gaeaUiI18nController', 'POST#/gaeaUiI18n/listI18nFields', NULL, NULL, NULL, NULL, '2021-03-30 17:34:08', '2021-03-30 17:34:08', 1);
INSERT INTO `gaea_authority` VALUES (12044, 'gaea-business', '0', 'gaeaUiI18nController#getTables', NULL, 'gaeaUiI18nController', 'GET#/gaeaUiI18n/getTables', NULL, NULL, NULL, NULL, '2021-03-30 17:34:08', '2021-03-30 17:34:08', 1);
INSERT INTO `gaea_authority` VALUES (12045, 'gaea-business', '0', 'infController#detail', NULL, 'infController', 'GET#/inf/**', NULL, NULL, NULL, NULL, '2021-03-30 17:34:08', '2021-03-30 17:34:08', 1);
INSERT INTO `gaea_authority` VALUES (12058, 'gaea-business', '0', 'processController#processInstanceDetail', NULL, 'processController', 'GET#/process/processInstance/detail/**', NULL, NULL, NULL, NULL, '2021-03-31 14:36:40', '2021-03-31 14:36:40', 1);
INSERT INTO `gaea_authority` VALUES (12059, 'gaea-business', '0', 'processController#myRunningTask', NULL, 'processController', 'GET#/process/my/running/task', NULL, NULL, NULL, NULL, '2021-03-31 14:36:40', '2021-03-31 14:36:40', 1);
INSERT INTO `gaea_authority` VALUES (12060, 'gaea-business', '0', 'processController#getFormData', NULL, 'processController', 'GET#/process/form/data/**', NULL, NULL, NULL, NULL, '2021-03-31 14:36:40', '2021-03-31 14:36:40', 1);
INSERT INTO `gaea_authority` VALUES (12061, 'gaea-business', '0', 'processController#startProcess', NULL, 'processController', 'POST#/process/start/process/**', NULL, NULL, NULL, NULL, '2021-03-31 14:36:40', '2021-03-31 14:36:40', 1);
INSERT INTO `gaea_authority` VALUES (12062, 'gaea-business', '0', 'processController#myHistoryTask', NULL, 'processController', 'GET#/process/my/history/task', NULL, NULL, NULL, NULL, '2021-03-31 14:36:40', '2021-03-31 14:36:40', 1);
INSERT INTO `gaea_authority` VALUES (12063, 'gaea-business', '0', 'processController#handlerTask', NULL, 'processController', 'POST#/process/complete/task/**', NULL, NULL, NULL, NULL, '2021-03-31 14:36:40', '2021-03-31 14:36:40', 1);
INSERT INTO `gaea_authority` VALUES (12064, 'gaea-business', '0', 'processController#taskDetail', NULL, 'processController', 'GET#/process/task/detail/**', NULL, NULL, NULL, NULL, '2021-03-31 14:36:40', '2021-03-31 14:36:40', 1);
INSERT INTO `gaea_authority` VALUES (12065, 'gaea-business', '0', 'processController#update', NULL, 'processController', 'PUT#/process', NULL, NULL, NULL, NULL, '2021-03-31 14:36:40', '2021-03-31 14:36:40', 1);
INSERT INTO `gaea_authority` VALUES (12066, 'gaea-business', '0', 'processController#insert', NULL, 'processController', 'POST#/process', NULL, NULL, NULL, NULL, '2021-03-31 14:36:40', '2021-03-31 14:36:40', 1);
INSERT INTO `gaea_authority` VALUES (12067, 'gaea-business', '0', 'processController#detail', NULL, 'processController', 'GET#/process/**', NULL, NULL, NULL, NULL, '2021-03-31 14:36:40', '2021-03-31 14:36:40', 1);
INSERT INTO `gaea_authority` VALUES (12068, 'gaea-business', '0', 'processController#deleteById', NULL, 'processController', 'DELETE#/process/**', NULL, NULL, NULL, NULL, '2021-03-31 14:36:40', '2021-03-31 14:36:40', 1);
INSERT INTO `gaea_authority` VALUES (12069, 'gaea-business', '0', 'processController#pageList', NULL, 'processController', 'GET#/process/pageList', NULL, NULL, NULL, NULL, '2021-03-31 14:36:40', '2021-03-31 14:36:40', 1);
INSERT INTO `gaea_authority` VALUES (12070, 'gaea-business', '0', 'processController#deleteBatchIds', NULL, 'processController', 'POST#/process/delete/batch', NULL, NULL, NULL, NULL, '2021-03-31 14:36:40', '2021-03-31 14:36:40', 1);
INSERT INTO `gaea_authority` VALUES (12085, 'gaea-business', '0', 'dataSetController#detailBysetCode', NULL, 'dataSetController', 'GET#/dataSet/detailBysetCode/**', NULL, NULL, NULL, NULL, '2021-04-01 09:25:27', '2021-04-01 09:25:27', 1);
INSERT INTO `gaea_authority` VALUES (12280, 'gaea-business', '0', 'dynamicController#testDemo', NULL, 'dynamicController', 'POST#/custom/dynamic/testDemo', NULL, NULL, NULL, NULL, '2021-04-06 15:39:54', '2021-04-06 15:39:54', 1);
INSERT INTO `gaea_authority` VALUES (12282, 'gaea-business', '0', 'reportController#insert', NULL, 'reportController', 'POST#/report', NULL, NULL, NULL, NULL, '2021-04-06 15:39:54', '2021-04-06 15:39:54', 1);
INSERT INTO `gaea_authority` VALUES (12358, 'gaea-business', '0', 'infController#examine', NULL, 'infController', 'GET#/inf/examine/**', NULL, NULL, NULL, NULL, '2021-04-07 16:28:49', '2021-04-07 16:28:49', 1);
INSERT INTO `gaea_authority` VALUES (12376, 'gaea-business', '0', 'gaeaDictController#refreshDict', NULL, 'gaeaDictController', 'POST#/gaeaDict/freshDict', NULL, NULL, NULL, NULL, '2021-04-08 13:22:10', '2021-04-08 13:22:10', 1);
INSERT INTO `gaea_authority` VALUES (12488, 'gaea-business', '0', 'gaeaInfAppRelationController#update', NULL, 'gaeaInfAppRelationController', 'PUT#/inf/app/relation/times', NULL, NULL, NULL, NULL, '2021-04-09 17:37:10', '2021-04-09 17:37:10', 1);
INSERT INTO `gaea_authority` VALUES (12489, 'gaea-business', '0', 'gaeaInfAppRelationController#listInfApp', NULL, 'gaeaInfAppRelationController', 'POST#/inf/app/relation/listInfApp', NULL, NULL, NULL, NULL, '2021-04-09 17:37:10', '2021-04-09 17:37:10', 1);
INSERT INTO `gaea_authority` VALUES (12490, 'gaea-business', '0', 'gaeaInfAppRelationController#update', NULL, 'gaeaInfAppRelationController', 'PUT#/inf/app/relation', NULL, NULL, NULL, NULL, '2021-04-09 17:37:10', '2021-04-09 17:37:10', 1);
INSERT INTO `gaea_authority` VALUES (12491, 'gaea-business', '0', 'gaeaInfAppRelationController#insert', NULL, 'gaeaInfAppRelationController', 'POST#/inf/app/relation', NULL, NULL, NULL, NULL, '2021-04-09 17:37:10', '2021-04-09 17:37:10', 1);
INSERT INTO `gaea_authority` VALUES (12492, 'gaea-business', '0', 'gaeaInfAppRelationController#detail', NULL, 'gaeaInfAppRelationController', 'GET#/inf/app/relation/**', NULL, NULL, NULL, NULL, '2021-04-09 17:37:10', '2021-04-09 17:37:10', 1);
INSERT INTO `gaea_authority` VALUES (12493, 'gaea-business', '0', 'gaeaInfAppRelationController#deleteById', NULL, 'gaeaInfAppRelationController', 'DELETE#/inf/app/relation/**', NULL, NULL, NULL, NULL, '2021-04-09 17:37:10', '2021-04-09 17:37:10', 1);
INSERT INTO `gaea_authority` VALUES (12494, 'gaea-business', '0', 'gaeaInfAppRelationController#pageList', NULL, 'gaeaInfAppRelationController', 'GET#/inf/app/relation/pageList', NULL, NULL, NULL, NULL, '2021-04-09 17:37:10', '2021-04-09 17:37:10', 1);
INSERT INTO `gaea_authority` VALUES (12495, 'gaea-business', '0', 'gaeaInfAppRelationController#deleteBatchIds', NULL, 'gaeaInfAppRelationController', 'POST#/inf/app/relation/delete/batch', NULL, NULL, NULL, NULL, '2021-04-09 17:37:10', '2021-04-09 17:37:10', 1);
INSERT INTO `gaea_authority` VALUES (12497, 'gaea-business', '0', 'dataSetController#detail', NULL, 'dataSetController', 'GET#/dataSet/detailBysetId/**', NULL, NULL, NULL, NULL, '2021-04-12 14:20:28', '2021-04-12 14:20:28', 1);
INSERT INTO `gaea_authority` VALUES (12498, 'gaea-business', '0', 'gaeaDictController#dictItemByLang', NULL, 'gaeaDictController', 'GET#/gaeaDict/map/**', NULL, NULL, NULL, NULL, '2021-04-12 14:20:28', '2021-04-12 14:20:28', 1);
INSERT INTO `gaea_authority` VALUES (12767, 'gaea-business', '0', 'dataSetController#detail', NULL, 'dataSetController', 'GET#/dataSet/**', NULL, NULL, NULL, NULL, '2021-04-12 16:29:17', '2021-04-12 16:29:17', 1);
INSERT INTO `gaea_authority` VALUES (13052, 'gaea-business', '0', 'reportDashboardController#insert', NULL, 'reportDashboardController', 'POST#/reportDashboard', NULL, NULL, NULL, NULL, '2021-04-13 15:01:18', '2021-04-13 15:01:18', 1);
INSERT INTO `gaea_authority` VALUES (13053, 'gaea-business', '0', 'reportDashboardController#detail', NULL, 'reportDashboardController', 'GET#/reportDashboard/**', NULL, NULL, NULL, NULL, '2021-04-13 15:01:18', '2021-04-13 15:01:18', 1);
INSERT INTO `gaea_authority` VALUES (13056, 'gaea-business', '0', 'reportExcelController#exportExcel', NULL, 'reportExcelController', 'POST#/reportExcel/exportExcel', NULL, NULL, NULL, NULL, '2021-04-14 15:00:40', '2021-04-14 15:00:40', 1);
INSERT INTO `gaea_authority` VALUES (13057, 'gaea-business', '0', 'reportExcelController#detailByReportCode', NULL, 'reportExcelController', 'GET#/reportExcel/detailByReportCode/**', NULL, NULL, NULL, NULL, '2021-04-14 15:00:40', '2021-04-14 15:00:40', 1);
INSERT INTO `gaea_authority` VALUES (13058, 'gaea-business', '0', 'reportExcelController#preview', NULL, 'reportExcelController', 'POST#/reportExcel/preview', NULL, NULL, NULL, NULL, '2021-04-14 15:00:40', '2021-04-14 15:00:40', 1);
INSERT INTO `gaea_authority` VALUES (13059, 'gaea-business', '0', 'reportExcelController#deleteById', NULL, 'reportExcelController', 'DELETE#/reportExcel/**', NULL, NULL, NULL, NULL, '2021-04-14 15:00:40', '2021-04-14 15:00:40', 1);
INSERT INTO `gaea_authority` VALUES (13060, 'gaea-business', '0', 'reportExcelController#pageList', NULL, 'reportExcelController', 'GET#/reportExcel/pageList', NULL, NULL, NULL, NULL, '2021-04-14 15:00:40', '2021-04-14 15:00:40', 1);
INSERT INTO `gaea_authority` VALUES (13061, 'gaea-business', '0', 'reportExcelController#detail', NULL, 'reportExcelController', 'GET#/reportExcel/**', NULL, NULL, NULL, NULL, '2021-04-14 15:00:40', '2021-04-14 15:00:40', 1);
INSERT INTO `gaea_authority` VALUES (13062, 'gaea-business', '0', 'reportExcelController#deleteBatchIds', NULL, 'reportExcelController', 'POST#/reportExcel/delete/batch', NULL, NULL, NULL, NULL, '2021-04-14 15:00:40', '2021-04-14 15:00:40', 1);
INSERT INTO `gaea_authority` VALUES (13063, 'gaea-business', '0', 'reportExcelController#update', NULL, 'reportExcelController', 'PUT#/reportExcel', NULL, NULL, NULL, NULL, '2021-04-14 15:00:40', '2021-04-14 15:00:40', 1);
INSERT INTO `gaea_authority` VALUES (13064, 'gaea-business', '0', 'reportExcelController#insert', NULL, 'reportExcelController', 'POST#/reportExcel', NULL, NULL, NULL, NULL, '2021-04-14 15:00:40', '2021-04-14 15:00:40', 1);
INSERT INTO `gaea_authority` VALUES (13077, 'gaea-auth', '0', 'gaeaCommonConditionController#getDynamicQueryBoListById', NULL, 'gaeaCommonConditionController', 'GET#/commonCondition/getDynamicQueryBoListById', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13078, 'gaea-auth', '0', 'gaeaCommonConditionController#saveCommonCondition', NULL, 'gaeaCommonConditionController', 'POST#/commonCondition/saveCondition', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13079, 'gaea-auth', '0', 'gaeaCommonConditionController#queryByCondition', NULL, 'gaeaCommonConditionController', 'POST#/commonCondition/queryByCondition', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13080, 'gaea-auth', '0', 'gaeaCommonConditionController#update', NULL, 'gaeaCommonConditionController', 'PUT#/commonCondition', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13081, 'gaea-auth', '0', 'gaeaCommonConditionController#insert', NULL, 'gaeaCommonConditionController', 'POST#/commonCondition', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13082, 'gaea-auth', '0', 'gaeaCommonConditionController#detail', NULL, 'gaeaCommonConditionController', 'GET#/commonCondition/**', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13083, 'gaea-auth', '0', 'gaeaCommonConditionController#pageList', NULL, 'gaeaCommonConditionController', 'GET#/commonCondition/pageList', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13084, 'gaea-auth', '0', 'gaeaCommonConditionController#deleteById', NULL, 'gaeaCommonConditionController', 'DELETE#/commonCondition/**', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13085, 'gaea-auth', '0', 'gaeaCommonConditionController#deleteBatchIds', NULL, 'gaeaCommonConditionController', 'POST#/commonCondition/delete/batch', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13086, 'gaea-auth', '0', 'gaeaMenuExtensionController#queryMenuExtension', NULL, 'gaeaMenuExtensionController', 'GET#/menuextension/queryMenuExtension/**', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13087, 'gaea-auth', '0', 'gaeaMenuExtensionController#update', NULL, 'gaeaMenuExtensionController', 'PUT#/menuextension', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13088, 'gaea-auth', '0', 'gaeaMenuExtensionController#insert', NULL, 'gaeaMenuExtensionController', 'POST#/menuextension', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13089, 'gaea-auth', '0', 'gaeaMenuExtensionController#detail', NULL, 'gaeaMenuExtensionController', 'GET#/menuextension/**', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13090, 'gaea-auth', '0', 'gaeaMenuExtensionController#pageList', NULL, 'gaeaMenuExtensionController', 'GET#/menuextension/pageList', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13091, 'gaea-auth', '0', 'gaeaMenuExtensionController#deleteById', NULL, 'gaeaMenuExtensionController', 'DELETE#/menuextension/**', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13092, 'gaea-auth', '0', 'gaeaMenuExtensionController#deleteBatchIds', NULL, 'gaeaMenuExtensionController', 'POST#/menuextension/delete/batch', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13093, 'gaea-auth', '0', 'gaeaMenuUserExtensionController#batchSave', NULL, 'gaeaMenuUserExtensionController', 'POST#/userextension/userMenuExtensionsBatchSave', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13094, 'gaea-auth', '0', 'gaeaMenuUserExtensionController#getUserMenuList', NULL, 'gaeaMenuUserExtensionController', 'GET#/userextension/userMenuExtensions', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13095, 'gaea-auth', '0', 'gaeaMenuUserExtensionController#update', NULL, 'gaeaMenuUserExtensionController', 'PUT#/userextension', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13096, 'gaea-auth', '0', 'gaeaMenuUserExtensionController#insert', NULL, 'gaeaMenuUserExtensionController', 'POST#/userextension', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13097, 'gaea-auth', '0', 'gaeaMenuUserExtensionController#detail', NULL, 'gaeaMenuUserExtensionController', 'GET#/userextension/**', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13098, 'gaea-auth', '0', 'gaeaMenuUserExtensionController#pageList', NULL, 'gaeaMenuUserExtensionController', 'GET#/userextension/pageList', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13099, 'gaea-auth', '0', 'gaeaMenuUserExtensionController#deleteById', NULL, 'gaeaMenuUserExtensionController', 'DELETE#/userextension/**', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13100, 'gaea-auth', '0', 'gaeaMenuUserExtensionController#deleteBatchIds', NULL, 'gaeaMenuUserExtensionController', 'POST#/userextension/delete/batch', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13101, 'gaea-auth', '0', 'gaeaQueryConditionController#queryCondition', NULL, 'gaeaQueryConditionController', 'POST#/querycondition/list', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13102, 'gaea-auth', '0', 'gaeaQueryConditionController#update', NULL, 'gaeaQueryConditionController', 'PUT#/querycondition', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13103, 'gaea-auth', '0', 'gaeaQueryConditionController#insert', NULL, 'gaeaQueryConditionController', 'POST#/querycondition', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13104, 'gaea-auth', '0', 'gaeaQueryConditionController#detail', NULL, 'gaeaQueryConditionController', 'GET#/querycondition/**', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13105, 'gaea-auth', '0', 'gaeaQueryConditionController#pageList', NULL, 'gaeaQueryConditionController', 'GET#/querycondition/pageList', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13106, 'gaea-auth', '0', 'gaeaQueryConditionController#deleteById', NULL, 'gaeaQueryConditionController', 'DELETE#/querycondition/**', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13107, 'gaea-auth', '0', 'gaeaQueryConditionController#deleteBatchIds', NULL, 'gaeaQueryConditionController', 'POST#/querycondition/delete/batch', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13108, 'gaea-auth', '0', 'gaeaRoleController#saveOrgTreeForRole', NULL, 'gaeaRoleController', 'POST#/role/mapping/org', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13109, 'gaea-auth', '0', 'gaeaRoleController#queryOrgTreeForRole', NULL, 'gaeaRoleController', 'GET#/role/org/**', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13110, 'gaea-auth', '0', 'gaeaSettingController#update', NULL, 'gaeaSettingController', 'PUT#/setting', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13111, 'gaea-auth', '0', 'gaeaSettingController#insert', NULL, 'gaeaSettingController', 'POST#/setting', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13112, 'gaea-auth', '0', 'gaeaSettingController#detail', NULL, 'gaeaSettingController', 'GET#/setting/**', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13113, 'gaea-auth', '0', 'gaeaSettingController#pageList', NULL, 'gaeaSettingController', 'GET#/setting/pageList', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13114, 'gaea-auth', '0', 'gaeaSettingController#deleteById', NULL, 'gaeaSettingController', 'DELETE#/setting/**', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13115, 'gaea-auth', '0', 'gaeaSettingController#deleteBatchIds', NULL, 'gaeaSettingController', 'POST#/setting/delete/batch', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13117, 'gaea-auth', '0', 'gaeaUserController#queryRoleTree', NULL, 'gaeaUserController', 'GET#/user/queryRoleTree/**', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13118, 'gaea-auth', '0', 'gaeaUserController#userAdvanceQuery', NULL, 'gaeaUserController', 'POST#/user/queryAdvanceUser', NULL, NULL, NULL, NULL, '2021-04-15 09:26:23', '2021-04-15 09:26:23', 1);
INSERT INTO `gaea_authority` VALUES (13132, 'gaea-business', '0', 'projectController#deleteById', NULL, 'projectController', 'DELETE#/project/**', NULL, NULL, NULL, NULL, '2021-04-16 09:25:21', '2021-04-16 09:25:21', 1);
INSERT INTO `gaea_authority` VALUES (13133, 'gaea-business', '0', 'projectController#detail', NULL, 'projectController', 'GET#/project/**', NULL, NULL, NULL, NULL, '2021-04-16 09:25:21', '2021-04-16 09:25:21', 1);
INSERT INTO `gaea_authority` VALUES (13134, 'gaea-business', '0', 'projectController#pageList', NULL, 'projectController', 'GET#/project/pageList', NULL, NULL, NULL, NULL, '2021-04-16 09:25:21', '2021-04-16 09:25:21', 1);
INSERT INTO `gaea_authority` VALUES (13135, 'gaea-business', '0', 'projectController#deleteBatchIds', NULL, 'projectController', 'POST#/project/delete/batch', NULL, NULL, NULL, NULL, '2021-04-16 09:25:21', '2021-04-16 09:25:21', 1);
INSERT INTO `gaea_authority` VALUES (13136, 'gaea-business', '0', 'projectController#update', NULL, 'projectController', 'PUT#/project', NULL, NULL, NULL, NULL, '2021-04-16 09:25:21', '2021-04-16 09:25:21', 1);
INSERT INTO `gaea_authority` VALUES (13137, 'gaea-business', '0', 'projectController#insert', NULL, 'projectController', 'POST#/project', NULL, NULL, NULL, NULL, '2021-04-16 09:25:21', '2021-04-16 09:25:21', 1);
INSERT INTO `gaea_authority` VALUES (13138, 'gaea-business', '0', 'genConfigController#update', NULL, 'genConfigController', 'PUT#/api/genConfig', NULL, NULL, NULL, NULL, '2021-04-16 09:25:21', '2021-04-16 09:25:21', 1);
INSERT INTO `gaea_authority` VALUES (13139, 'gaea-business', '0', 'genConfigController#query', NULL, 'genConfigController', 'GET#/api/genConfig/**/**', NULL, NULL, NULL, NULL, '2021-04-16 09:25:21', '2021-04-16 09:25:21', 1);
INSERT INTO `gaea_authority` VALUES (13140, 'gaea-business', '0', 'generatorController#generator', NULL, 'generatorController', 'POST#/api/generator/**/**/**', NULL, NULL, NULL, NULL, '2021-04-16 09:25:21', '2021-04-16 09:25:21', 1);
INSERT INTO `gaea_authority` VALUES (13141, 'gaea-business', '0', 'generatorController#queryColumns', NULL, 'generatorController', 'GET#/api/generator/columns', NULL, NULL, NULL, NULL, '2021-04-16 09:25:21', '2021-04-16 09:25:21', 1);
INSERT INTO `gaea_authority` VALUES (13142, 'gaea-business', '0', 'generatorController#queryTables', NULL, 'generatorController', 'GET#/api/generator/tables', NULL, NULL, NULL, NULL, '2021-04-16 09:25:21', '2021-04-16 09:25:21', 1);
INSERT INTO `gaea_authority` VALUES (13143, 'gaea-business', '0', 'generatorController#queryTables', NULL, 'generatorController', 'GET#/api/generator/tables/all', NULL, NULL, NULL, NULL, '2021-04-16 09:25:21', '2021-04-16 09:25:21', 1);
INSERT INTO `gaea_authority` VALUES (13144, 'gaea-business', '0', 'generatorController#save', NULL, 'generatorController', 'POST#/api/generator/saveColumnCfg', NULL, NULL, NULL, NULL, '2021-04-16 09:25:21', '2021-04-16 09:25:21', 1);
INSERT INTO `gaea_authority` VALUES (13145, 'gaea-business', '0', 'generatorController#sync', NULL, 'generatorController', 'POST#/api/generator/sync', NULL, NULL, NULL, NULL, '2021-04-16 09:25:21', '2021-04-16 09:25:21', 1);
INSERT INTO `gaea_authority` VALUES (13194, 'gaea-auth', '1', 'annot:UPDATE', '系统公告信息表更新', 'gaeaAnnouncementController', 'PUT#/annot', NULL, NULL, NULL, NULL, '2021-04-19 14:50:38', '2021-04-19 14:50:38', 1);
INSERT INTO `gaea_authority` VALUES (13195, 'gaea-auth', '1', 'annot:INSERT', '系统公告信息表新增', 'gaeaAnnouncementController', 'POST#/annot', NULL, NULL, NULL, NULL, '2021-04-19 14:50:38', '2021-04-19 14:50:38', 1);
INSERT INTO `gaea_authority` VALUES (13196, 'gaea-auth', '1', 'annot:DETAIL', '系统公告信息表明细', 'gaeaAnnouncementController', 'GET#/annot/**', NULL, NULL, NULL, NULL, '2021-04-19 14:50:38', '2021-04-19 14:50:38', 1);
INSERT INTO `gaea_authority` VALUES (13197, 'gaea-auth', '1', 'annot:DELETE', '系统公告信息表删除', 'gaeaAnnouncementController', 'DELETE#/annot/**', NULL, NULL, NULL, NULL, '2021-04-19 14:50:38', '2021-04-19 14:50:38', 1);
INSERT INTO `gaea_authority` VALUES (13198, 'gaea-auth', '1', 'annot:BATCH_DELETE', '系统公告信息表批量删除', 'gaeaAnnouncementController', 'POST#/annot/delete/batch', NULL, NULL, NULL, NULL, '2021-04-19 14:50:38', '2021-04-19 14:50:38', 1);
INSERT INTO `gaea_authority` VALUES (13199, 'gaea-auth', '1', 'annot:PAGE', '系统公告信息表分页', 'gaeaAnnouncementController', 'GET#/annot/pageList', NULL, NULL, NULL, NULL, '2021-04-19 14:50:38', '2021-04-19 14:50:38', 1);
INSERT INTO `gaea_authority` VALUES (13200, 'gaea-auth', '1', 'menu:UPDATE', '菜单更新', 'gaeaMenuController', 'PUT#/menu', NULL, NULL, NULL, NULL, '2021-04-19 14:50:38', '2021-04-19 14:50:38', 1);
INSERT INTO `gaea_authority` VALUES (13201, 'gaea-auth', '1', 'menu:INSERT', '菜单新增', 'gaeaMenuController', 'POST#/menu', NULL, NULL, NULL, NULL, '2021-04-19 14:50:38', '2021-04-19 14:50:38', 1);
INSERT INTO `gaea_authority` VALUES (13202, 'gaea-auth', '1', 'menu:DETAIL', '菜单明细', 'gaeaMenuController', 'GET#/menu/**', NULL, NULL, NULL, NULL, '2021-04-19 14:50:38', '2021-04-19 14:50:38', 1);
INSERT INTO `gaea_authority` VALUES (13203, 'gaea-auth', '1', 'menu:DELETE', '菜单删除', 'gaeaMenuController', 'DELETE#/menu/**', NULL, NULL, NULL, NULL, '2021-04-19 14:50:38', '2021-04-19 14:50:38', 1);
INSERT INTO `gaea_authority` VALUES (13204, 'gaea-auth', '1', 'menu:BATCH_DELETE', '菜单批量删除', 'gaeaMenuController', 'POST#/menu/delete/batch', NULL, NULL, NULL, NULL, '2021-04-19 14:50:38', '2021-04-19 14:50:38', 1);
INSERT INTO `gaea_authority` VALUES (13205, 'gaea-auth', '1', 'menu:PAGE', '菜单分页', 'gaeaMenuController', 'GET#/menu/pageList', NULL, NULL, NULL, NULL, '2021-04-19 14:50:38', '2021-04-19 14:50:38', 1);
INSERT INTO `gaea_authority` VALUES (13206, 'gaea-business', '0', 'gaeaFileController#deleteById', NULL, 'gaeaFileController', 'DELETE#/file/**', NULL, NULL, NULL, NULL, '2021-04-23 13:24:33', '2021-04-23 13:24:33', 1);
INSERT INTO `gaea_authority` VALUES (13207, 'gaea-business', '0', 'gaeaFileController#detail', NULL, 'gaeaFileController', 'GET#/file/**', NULL, NULL, NULL, NULL, '2021-04-23 13:24:33', '2021-04-23 13:24:33', 1);
INSERT INTO `gaea_authority` VALUES (13208, 'gaea-business', '0', 'gaeaFileController#pageList', NULL, 'gaeaFileController', 'GET#/file/pageList', NULL, NULL, NULL, NULL, '2021-04-23 13:24:33', '2021-04-23 13:24:33', 1);
INSERT INTO `gaea_authority` VALUES (13209, 'gaea-business', '0', 'gaeaFileController#deleteBatchIds', NULL, 'gaeaFileController', 'POST#/file/delete/batch', NULL, NULL, NULL, NULL, '2021-04-23 13:24:33', '2021-04-23 13:24:33', 1);
INSERT INTO `gaea_authority` VALUES (13210, 'gaea-business', '0', 'gaeaFileController#update', NULL, 'gaeaFileController', 'PUT#/file', NULL, NULL, NULL, NULL, '2021-04-23 13:24:33', '2021-04-23 13:24:33', 1);
INSERT INTO `gaea_authority` VALUES (13211, 'gaea-business', '0', 'gaeaFileController#insert', NULL, 'gaeaFileController', 'POST#/file', NULL, NULL, NULL, NULL, '2021-04-23 13:24:33', '2021-04-23 13:24:33', 1);
INSERT INTO `gaea_authority` VALUES (13212, 'gaea-business', '0', 'reportController#delReport', NULL, 'reportController', 'DELETE#/report/delReport', NULL, NULL, NULL, NULL, '2021-04-26 09:37:06', '2021-04-26 09:37:06', 1);
INSERT INTO `gaea_authority` VALUES (13213, 'gaea-business', '0', 'reportDashboardController#getData', NULL, 'reportDashboardController', 'POST#/reportDashboard/getData', NULL, NULL, NULL, NULL, '2021-04-28 11:10:59', '2021-04-28 11:10:59', 1);
INSERT INTO `gaea_authority` VALUES (13216, 'gaea-auth', '0', 'gaeaAnnouncementController#saveAndPublishAnnot', NULL, 'gaeaAnnouncementController', 'POST#/annot/publishAnnotBatch', NULL, NULL, 'admin', 'admin', '2021-06-22 13:32:21', '2021-06-22 13:32:21', 1);
INSERT INTO `gaea_authority` VALUES (13217, 'gaea-auth', '0', 'gaeaAnnouncementController#saveAndPublishAnnot', NULL, 'gaeaAnnouncementController', 'POST#/annot/saveAndPublishAnnot', NULL, NULL, 'admin', 'admin', '2021-06-22 13:32:21', '2021-06-22 13:32:21', 1);
INSERT INTO `gaea_authority` VALUES (13218, 'gaea-auth', '0', 'gaeaAnnouncementController#myAnnotInfo', NULL, 'gaeaAnnouncementController', 'GET#/annot/myAnnotInfo', NULL, NULL, 'admin', 'admin', '2021-06-22 13:32:21', '2021-06-22 13:32:21', 1);
INSERT INTO `gaea_authority` VALUES (13219, 'gaea-auth', '0', 'gaeaAnnouncementController#sendServiceMsg', NULL, 'gaeaAnnouncementController', 'POST#/annot/sendServiceMsg', NULL, NULL, 'admin', 'admin', '2021-06-22 13:32:21', '2021-06-22 13:32:21', 1);
INSERT INTO `gaea_authority` VALUES (13220, 'gaea-auth', '0', 'gaeaAnnouncementController#cancelAnnot', NULL, 'gaeaAnnouncementController', 'GET#/annot/cancelAnnot/**', NULL, NULL, 'admin', 'admin', '2021-06-22 13:32:21', '2021-06-22 13:32:21', 1);
INSERT INTO `gaea_authority` VALUES (13221, 'gaea-auth', '0', 'gaeaAnnouncementController#tagAllRead', NULL, 'gaeaAnnouncementController', 'POST#/annot/tagAllRead', NULL, NULL, 'admin', 'admin', '2021-06-22 13:32:21', '2021-06-22 13:32:21', 1);
INSERT INTO `gaea_authority` VALUES (13222, 'gaea-auth', '0', 'gaeaAnnouncementController#readDetail', NULL, 'gaeaAnnouncementController', 'GET#/annot/readDetail/**', NULL, NULL, 'admin', 'admin', '2021-06-22 13:32:21', '2021-06-22 13:32:21', 1);
INSERT INTO `gaea_authority` VALUES (13223, 'gaea-auth', '0', 'gaeaAnnouncementController#update', NULL, 'gaeaAnnouncementController', 'PUT#/annot', NULL, NULL, 'admin', 'admin', '2021-06-22 13:32:21', '2021-06-22 13:32:21', 1);
INSERT INTO `gaea_authority` VALUES (13224, 'gaea-auth', '0', 'gaeaAnnouncementController#insert', NULL, 'gaeaAnnouncementController', 'POST#/annot', NULL, NULL, 'admin', 'admin', '2021-06-22 13:32:21', '2021-06-22 13:32:21', 1);
INSERT INTO `gaea_authority` VALUES (13225, 'gaea-auth', '0', 'gaeaAnnouncementController#detail', NULL, 'gaeaAnnouncementController', 'GET#/annot/**', NULL, NULL, 'admin', 'admin', '2021-06-22 13:32:21', '2021-06-22 13:32:21', 1);
INSERT INTO `gaea_authority` VALUES (13226, 'gaea-auth', '0', 'gaeaAnnouncementController#pageList', NULL, 'gaeaAnnouncementController', 'GET#/annot/pageList', NULL, NULL, 'admin', 'admin', '2021-06-22 13:32:21', '2021-06-22 13:32:21', 1);
INSERT INTO `gaea_authority` VALUES (13227, 'gaea-auth', '0', 'gaeaAnnouncementController#deleteBatchIds', NULL, 'gaeaAnnouncementController', 'POST#/annot/delete/batch', NULL, NULL, 'admin', 'admin', '2021-06-22 13:32:21', '2021-06-22 13:32:21', 1);
INSERT INTO `gaea_authority` VALUES (13228, 'gaea-auth', '0', 'gaeaAnnouncementController#deleteById', NULL, 'gaeaAnnouncementController', 'DELETE#/annot/**', NULL, NULL, 'admin', 'admin', '2021-06-22 13:32:21', '2021-06-22 13:32:21', 1);
INSERT INTO `gaea_authority` VALUES (13229, 'gaea-auth', '0', 'gaeaRoleController#roleAuthorities', NULL, 'gaeaRoleController', 'GET#/role/tree/authorities/**/**', NULL, NULL, 'admin', 'admin', '2021-06-22 13:32:21', '2021-06-22 13:32:21', 1);
INSERT INTO `gaea_authority` VALUES (13230, 'gaea-auth', '0', 'appspPushController#testAppspPush', NULL, 'appspPushController', 'POST#/apppush/testpush', NULL, NULL, 'admin', 'admin', '2021-06-22 13:32:21', '2021-06-22 13:32:21', 1);
INSERT INTO `gaea_authority` VALUES (13231, 'gaea-auth', '0', 'gaeaUserController#userSelect', NULL, 'gaeaUserController', 'GET#/user/select', NULL, NULL, 'admin', 'admin', '2021-06-22 13:32:21', '2021-06-22 13:32:21', 1);
INSERT INTO `gaea_authority` VALUES (13232, 'gaea-auth', '0', 'gaeaUserController#unLock', NULL, 'gaeaUserController', 'POST#/user/unLock', NULL, NULL, 'admin', 'admin', '2021-06-22 13:32:21', '2021-06-22 13:32:21', 1);
INSERT INTO `gaea_authority` VALUES (13233, 'gaea-auth', '0', 'gaeaUserController#refreshCache', NULL, 'gaeaUserController', 'POST#/user/refresh/username', NULL, NULL, 'admin', 'admin', '2021-06-22 13:32:21', '2021-06-22 13:32:21', 1);
INSERT INTO `gaea_authority` VALUES (13234, 'javaserver-auth', '0', 'gaeaAuthorityController#authorityTree', NULL, 'gaeaAuthorityController', 'GET#/gaeaAuthority/authority/tree/**/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13235, 'javaserver-auth', '0', 'gaeaAuthorityController#roleAuthority', NULL, 'gaeaAuthorityController', 'POST#/gaeaAuthority/role/authority', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13236, 'javaserver-auth', '0', 'gaeaAuthorityController#update', NULL, 'gaeaAuthorityController', 'PUT#/gaeaAuthority', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13237, 'javaserver-auth', '0', 'gaeaAuthorityController#insert', NULL, 'gaeaAuthorityController', 'POST#/gaeaAuthority', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13238, 'javaserver-auth', '0', 'gaeaAuthorityController#detail', NULL, 'gaeaAuthorityController', 'GET#/gaeaAuthority/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13239, 'javaserver-auth', '0', 'gaeaAuthorityController#pageList', NULL, 'gaeaAuthorityController', 'GET#/gaeaAuthority/pageList', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13240, 'javaserver-auth', '0', 'gaeaAuthorityController#deleteById', NULL, 'gaeaAuthorityController', 'DELETE#/gaeaAuthority/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13241, 'javaserver-auth', '0', 'gaeaAuthorityController#deleteBatchIds', NULL, 'gaeaAuthorityController', 'POST#/gaeaAuthority/delete/batch', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13242, 'javaserver-auth', '0', 'gaeaAnnouncementController#myAnnotInfo', NULL, 'gaeaAnnouncementController', 'GET#/annot/myAnnotInfo', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13243, 'javaserver-auth', '0', 'gaeaAnnouncementController#cancelAnnot', NULL, 'gaeaAnnouncementController', 'GET#/annot/cancelAnnot/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13244, 'javaserver-auth', '0', 'gaeaAnnouncementController#tagAllRead', NULL, 'gaeaAnnouncementController', 'POST#/annot/tagAllRead', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13245, 'javaserver-auth', '0', 'gaeaAnnouncementController#readDetail', NULL, 'gaeaAnnouncementController', 'GET#/annot/readDetail/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13246, 'javaserver-auth', '0', 'gaeaAnnouncementController#sendServiceMsg', NULL, 'gaeaAnnouncementController', 'POST#/annot/sendServiceMsg', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13247, 'javaserver-auth', '0', 'gaeaAnnouncementController#saveAndPublishAnnot', NULL, 'gaeaAnnouncementController', 'POST#/annot/publishAnnotBatch', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13248, 'javaserver-auth', '0', 'gaeaAnnouncementController#saveAndPublishAnnot', NULL, 'gaeaAnnouncementController', 'POST#/annot/saveAndPublishAnnot', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13249, 'javaserver-auth', '0', 'gaeaAnnouncementController#update', NULL, 'gaeaAnnouncementController', 'PUT#/annot', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13250, 'javaserver-auth', '0', 'gaeaAnnouncementController#insert', NULL, 'gaeaAnnouncementController', 'POST#/annot', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13251, 'javaserver-auth', '0', 'gaeaAnnouncementController#detail', NULL, 'gaeaAnnouncementController', 'GET#/annot/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13252, 'javaserver-auth', '0', 'gaeaAnnouncementController#pageList', NULL, 'gaeaAnnouncementController', 'GET#/annot/pageList', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13253, 'javaserver-auth', '0', 'gaeaAnnouncementController#deleteById', NULL, 'gaeaAnnouncementController', 'DELETE#/annot/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13254, 'javaserver-auth', '0', 'gaeaAnnouncementController#deleteBatchIds', NULL, 'gaeaAnnouncementController', 'POST#/annot/delete/batch', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13255, 'javaserver-auth', '0', 'gaeaLogController#exportLogToFile', NULL, 'gaeaLogController', 'POST#/log/exportLogToFile', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13256, 'javaserver-auth', '0', 'gaeaLogController#update', NULL, 'gaeaLogController', 'PUT#/log', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13257, 'javaserver-auth', '0', 'gaeaLogController#insert', NULL, 'gaeaLogController', 'POST#/log', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13258, 'javaserver-auth', '0', 'gaeaLogController#detail', NULL, 'gaeaLogController', 'GET#/log/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13259, 'javaserver-auth', '0', 'gaeaLogController#pageList', NULL, 'gaeaLogController', 'GET#/log/pageList', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13260, 'javaserver-auth', '0', 'gaeaLogController#deleteById', NULL, 'gaeaLogController', 'DELETE#/log/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13261, 'javaserver-auth', '0', 'gaeaLogController#deleteBatchIds', NULL, 'gaeaLogController', 'POST#/log/delete/batch', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13262, 'javaserver-auth', '0', 'gaeaMenuController#userInfo', NULL, 'gaeaMenuController', 'GET#/menu/userInfo', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13263, 'javaserver-auth', '0', 'gaeaMenuController#getTree', NULL, 'gaeaMenuController', 'GET#/menu/tree', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13264, 'javaserver-auth', '0', 'gaeaMenuController#authorityTree', NULL, 'gaeaMenuController', 'GET#/menu/authority/tree/**/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13265, 'javaserver-auth', '0', 'gaeaMenuController#menuSelect', NULL, 'gaeaMenuController', 'GET#/menu/menuSelect', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13266, 'javaserver-auth', '0', 'gaeaMenuController#menuAuthority', NULL, 'gaeaMenuController', 'POST#/menu/mapper/authorities', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13267, 'javaserver-auth', '0', 'gaeaMenuController#getMenuInfoByOrg', NULL, 'gaeaMenuController', 'POST#/menu/menuUserInfoByOrg', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13268, 'javaserver-auth', '0', 'gaeaMenuController#getRoleTree', NULL, 'gaeaMenuController', 'GET#/menu/role/tree', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13269, 'javaserver-auth', '0', 'gaeaMenuController#update', NULL, 'gaeaMenuController', 'PUT#/menu', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13270, 'javaserver-auth', '0', 'gaeaMenuController#insert', NULL, 'gaeaMenuController', 'POST#/menu', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13271, 'javaserver-auth', '0', 'gaeaMenuController#detail', NULL, 'gaeaMenuController', 'GET#/menu/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13272, 'javaserver-auth', '0', 'gaeaMenuController#pageList', NULL, 'gaeaMenuController', 'GET#/menu/pageList', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13273, 'javaserver-auth', '0', 'gaeaMenuController#deleteById', NULL, 'gaeaMenuController', 'DELETE#/menu/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13274, 'javaserver-auth', '0', 'gaeaMenuController#deleteBatchIds', NULL, 'gaeaMenuController', 'POST#/menu/delete/batch', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13275, 'javaserver-auth', '0', 'gaeaCommonConditionController#queryByCondition', NULL, 'gaeaCommonConditionController', 'POST#/commonCondition/queryByCondition', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13276, 'javaserver-auth', '0', 'gaeaCommonConditionController#getDynamicQueryBoListById', NULL, 'gaeaCommonConditionController', 'GET#/commonCondition/getDynamicQueryBoListById', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13277, 'javaserver-auth', '0', 'gaeaCommonConditionController#saveCommonCondition', NULL, 'gaeaCommonConditionController', 'POST#/commonCondition/saveCondition', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13278, 'javaserver-auth', '0', 'gaeaCommonConditionController#update', NULL, 'gaeaCommonConditionController', 'PUT#/commonCondition', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13279, 'javaserver-auth', '0', 'gaeaCommonConditionController#insert', NULL, 'gaeaCommonConditionController', 'POST#/commonCondition', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13280, 'javaserver-auth', '0', 'gaeaCommonConditionController#detail', NULL, 'gaeaCommonConditionController', 'GET#/commonCondition/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13281, 'javaserver-auth', '0', 'gaeaCommonConditionController#pageList', NULL, 'gaeaCommonConditionController', 'GET#/commonCondition/pageList', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13282, 'javaserver-auth', '0', 'gaeaCommonConditionController#deleteById', NULL, 'gaeaCommonConditionController', 'DELETE#/commonCondition/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13283, 'javaserver-auth', '0', 'gaeaCommonConditionController#deleteBatchIds', NULL, 'gaeaCommonConditionController', 'POST#/commonCondition/delete/batch', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13284, 'javaserver-auth', '0', 'gaeaMenuExtensionController#queryMenuExtension', NULL, 'gaeaMenuExtensionController', 'GET#/menuextension/queryMenuExtension/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13285, 'javaserver-auth', '0', 'gaeaMenuExtensionController#update', NULL, 'gaeaMenuExtensionController', 'PUT#/menuextension', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13286, 'javaserver-auth', '0', 'gaeaMenuExtensionController#insert', NULL, 'gaeaMenuExtensionController', 'POST#/menuextension', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13287, 'javaserver-auth', '0', 'gaeaMenuExtensionController#detail', NULL, 'gaeaMenuExtensionController', 'GET#/menuextension/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13288, 'javaserver-auth', '0', 'gaeaMenuExtensionController#pageList', NULL, 'gaeaMenuExtensionController', 'GET#/menuextension/pageList', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13289, 'javaserver-auth', '0', 'gaeaMenuExtensionController#deleteById', NULL, 'gaeaMenuExtensionController', 'DELETE#/menuextension/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13290, 'javaserver-auth', '0', 'gaeaMenuExtensionController#deleteBatchIds', NULL, 'gaeaMenuExtensionController', 'POST#/menuextension/delete/batch', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13291, 'javaserver-auth', '0', 'gaeaMenuUserExtensionController#getUserMenuList', NULL, 'gaeaMenuUserExtensionController', 'GET#/userextension/userMenuExtensions', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13292, 'javaserver-auth', '0', 'gaeaMenuUserExtensionController#batchSave', NULL, 'gaeaMenuUserExtensionController', 'POST#/userextension/userMenuExtensionsBatchSave', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13293, 'javaserver-auth', '0', 'gaeaMenuUserExtensionController#update', NULL, 'gaeaMenuUserExtensionController', 'PUT#/userextension', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13294, 'javaserver-auth', '0', 'gaeaMenuUserExtensionController#insert', NULL, 'gaeaMenuUserExtensionController', 'POST#/userextension', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13295, 'javaserver-auth', '0', 'gaeaMenuUserExtensionController#detail', NULL, 'gaeaMenuUserExtensionController', 'GET#/userextension/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13296, 'javaserver-auth', '0', 'gaeaMenuUserExtensionController#pageList', NULL, 'gaeaMenuUserExtensionController', 'GET#/userextension/pageList', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13297, 'javaserver-auth', '0', 'gaeaMenuUserExtensionController#deleteById', NULL, 'gaeaMenuUserExtensionController', 'DELETE#/userextension/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13298, 'javaserver-auth', '0', 'gaeaMenuUserExtensionController#deleteBatchIds', NULL, 'gaeaMenuUserExtensionController', 'POST#/userextension/delete/batch', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13299, 'javaserver-auth', '0', 'gaeaQueryConditionController#queryCondition', NULL, 'gaeaQueryConditionController', 'POST#/querycondition/list', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13300, 'javaserver-auth', '0', 'gaeaQueryConditionController#update', NULL, 'gaeaQueryConditionController', 'PUT#/querycondition', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13301, 'javaserver-auth', '0', 'gaeaQueryConditionController#insert', NULL, 'gaeaQueryConditionController', 'POST#/querycondition', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13302, 'javaserver-auth', '0', 'gaeaQueryConditionController#detail', NULL, 'gaeaQueryConditionController', 'GET#/querycondition/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13303, 'javaserver-auth', '0', 'gaeaQueryConditionController#pageList', NULL, 'gaeaQueryConditionController', 'GET#/querycondition/pageList', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13304, 'javaserver-auth', '0', 'gaeaQueryConditionController#deleteById', NULL, 'gaeaQueryConditionController', 'DELETE#/querycondition/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13305, 'javaserver-auth', '0', 'gaeaQueryConditionController#deleteBatchIds', NULL, 'gaeaQueryConditionController', 'POST#/querycondition/delete/batch', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13306, 'javaserver-auth', '0', 'gaeaOrgController#orgSelect', NULL, 'gaeaOrgController', 'GET#/org/orgSelect', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13307, 'javaserver-auth', '0', 'gaeaOrgController#updateOrg', NULL, 'gaeaOrgController', 'POST#/org/updateOrg', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13308, 'javaserver-auth', '0', 'gaeaOrgController#saveOrg', NULL, 'gaeaOrgController', 'POST#/org/saveOrg', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13309, 'javaserver-auth', '0', 'gaeaOrgController#queryAllOrg', NULL, 'gaeaOrgController', 'GET#/org/queryAllOrg', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13310, 'javaserver-auth', '0', 'gaeaOrgController#orgRoleTree', NULL, 'gaeaOrgController', 'GET#/org/user/role/tree/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13311, 'javaserver-auth', '0', 'gaeaOrgController#orgTree', NULL, 'gaeaOrgController', 'GET#/org/tree', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13312, 'javaserver-auth', '0', 'gaeaOrgController#orgRoleTreeSelected', NULL, 'gaeaOrgController', 'GET#/org/role/tree', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13313, 'javaserver-auth', '0', 'gaeaOrgController#update', NULL, 'gaeaOrgController', 'PUT#/org', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13314, 'javaserver-auth', '0', 'gaeaOrgController#insert', NULL, 'gaeaOrgController', 'POST#/org', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13315, 'javaserver-auth', '0', 'gaeaOrgController#detail', NULL, 'gaeaOrgController', 'GET#/org/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13316, 'javaserver-auth', '0', 'gaeaOrgController#pageList', NULL, 'gaeaOrgController', 'GET#/org/pageList', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13317, 'javaserver-auth', '0', 'gaeaOrgController#deleteById', NULL, 'gaeaOrgController', 'DELETE#/org/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13318, 'javaserver-auth', '0', 'gaeaOrgController#deleteBatchIds', NULL, 'gaeaOrgController', 'POST#/org/delete/batch', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13319, 'javaserver-auth', '0', 'gaeaRoleController#roleAuthorities', NULL, 'gaeaRoleController', 'GET#/role/tree/authorities/**/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13320, 'javaserver-auth', '0', 'gaeaRoleController#saveMenuActionTreeForRole', NULL, 'gaeaRoleController', 'POST#/role/roleMenuAuthorities', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13321, 'javaserver-auth', '0', 'gaeaRoleController#queryOrgTreeForRole', NULL, 'gaeaRoleController', 'GET#/role/org/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13322, 'javaserver-auth', '0', 'gaeaRoleController#saveOrgTreeForRole', NULL, 'gaeaRoleController', 'POST#/role/mapping/org', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13323, 'javaserver-auth', '0', 'gaeaRoleController#update', NULL, 'gaeaRoleController', 'PUT#/role', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13324, 'javaserver-auth', '0', 'gaeaRoleController#insert', NULL, 'gaeaRoleController', 'POST#/role', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13325, 'javaserver-auth', '0', 'gaeaRoleController#detail', NULL, 'gaeaRoleController', 'GET#/role/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13326, 'javaserver-auth', '0', 'gaeaRoleController#pageList', NULL, 'gaeaRoleController', 'GET#/role/pageList', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13327, 'javaserver-auth', '0', 'gaeaRoleController#deleteById', NULL, 'gaeaRoleController', 'DELETE#/role/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13328, 'javaserver-auth', '0', 'gaeaRoleController#deleteBatchIds', NULL, 'gaeaRoleController', 'POST#/role/delete/batch', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13329, 'javaserver-auth', '0', 'gaeaSettingController#update', NULL, 'gaeaSettingController', 'PUT#/setting', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13330, 'javaserver-auth', '0', 'gaeaSettingController#insert', NULL, 'gaeaSettingController', 'POST#/setting', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13331, 'javaserver-auth', '0', 'gaeaSettingController#detail', NULL, 'gaeaSettingController', 'GET#/setting/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13332, 'javaserver-auth', '0', 'gaeaSettingController#pageList', NULL, 'gaeaSettingController', 'GET#/setting/pageList', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13333, 'javaserver-auth', '0', 'gaeaSettingController#deleteById', NULL, 'gaeaSettingController', 'DELETE#/setting/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13334, 'javaserver-auth', '0', 'gaeaSettingController#deleteBatchIds', NULL, 'gaeaSettingController', 'POST#/setting/delete/batch', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13335, 'javaserver-auth', '0', 'appspPushController#testAppspPush', NULL, 'appspPushController', 'POST#/apppush/testpush', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13336, 'javaserver-auth', '0', 'gaeaUserController#queryRoleTree', NULL, 'gaeaUserController', 'GET#/user/queryRoleTree/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13337, 'javaserver-auth', '0', 'gaeaUserController#saveRoleTree', NULL, 'gaeaUserController', 'POST#/user/saveRoleTree', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13338, 'javaserver-auth', '0', 'gaeaUserController#updatePassword', NULL, 'gaeaUserController', 'POST#/user/updatePassword', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13339, 'javaserver-auth', '0', 'gaeaUserController#unLock', NULL, 'gaeaUserController', 'POST#/user/unLock', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13340, 'javaserver-auth', '0', 'gaeaUserController#userSelect', NULL, 'gaeaUserController', 'GET#/user/select', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13341, 'javaserver-auth', '0', 'gaeaUserController#userAdvanceQuery', NULL, 'gaeaUserController', 'POST#/user/queryAdvanceUser', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13342, 'javaserver-auth', '0', 'gaeaUserController#refreshCache', NULL, 'gaeaUserController', 'POST#/user/refresh/username', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13343, 'javaserver-auth', '0', 'gaeaUserController#resetPassword', NULL, 'gaeaUserController', 'POST#/user/resetPwd', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13344, 'javaserver-auth', '0', 'gaeaUserController#update', NULL, 'gaeaUserController', 'PUT#/user', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13345, 'javaserver-auth', '0', 'gaeaUserController#insert', NULL, 'gaeaUserController', 'POST#/user', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13346, 'javaserver-auth', '0', 'gaeaUserController#detail', NULL, 'gaeaUserController', 'GET#/user/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13347, 'javaserver-auth', '0', 'gaeaUserController#pageList', NULL, 'gaeaUserController', 'GET#/user/pageList', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13348, 'javaserver-auth', '0', 'gaeaUserController#deleteById', NULL, 'gaeaUserController', 'DELETE#/user/**', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13349, 'javaserver-auth', '0', 'gaeaUserController#deleteBatchIds', NULL, 'gaeaUserController', 'POST#/user/delete/batch', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13350, 'javaserver-auth', '0', 'captchaController#get', NULL, 'captchaController', 'POST#/captcha/get', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);
INSERT INTO `gaea_authority` VALUES (13351, 'javaserver-auth', '0', 'captchaController#check', NULL, 'captchaController', 'POST#/captcha/check', NULL, NULL, NULL, NULL, '2022-02-28 15:02:28', '2022-02-28 15:02:28', 1);

-- ----------------------------
-- Table structure for gaea_common_condition
-- ----------------------------
DROP TABLE IF EXISTS `gaea_common_condition`;
CREATE TABLE `gaea_common_condition`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `menu_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单ID',
  `comm_sql` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '查询sql',
  `label` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询条件名称label',
  `is_disabled` int(11) NULL DEFAULT 0 COMMENT '0:可用,1:已作废',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
  `create_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人ID',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `table_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表code',
  `search_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '常用查询名称',
  `version` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_gcc_01`(`menu_code`, `create_by`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gaea_common_condition
-- ----------------------------

-- ----------------------------
-- Table structure for gaea_log
-- ----------------------------
DROP TABLE IF EXISTS `gaea_log`;
CREATE TABLE `gaea_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名称',
  `request_url` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求路径',
  `page_title` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '页面或按钮标题',
  `request_param` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '请求参数',
  `response_param` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '响应参数',
  `source_ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '来源IP',
  `request_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` timestamp(0) NULL DEFAULT NULL,
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `version` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gaea_log
-- ----------------------------

-- ----------------------------
-- Table structure for gaea_menu
-- ----------------------------
DROP TABLE IF EXISTS `gaea_menu`;
CREATE TABLE `gaea_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `menu_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单代码',
  `menu_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `sys_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统代码，参考数据字典SYSTEM_CODE',
  `parent_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父级id',
  `path` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单路径',
  `menu_icon` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'base64图标或者iconfont字体',
  `sort` int(8) NOT NULL DEFAULT 0 COMMENT '排序，升序',
  `enabled` int(1) NOT NULL DEFAULT 1 COMMENT '0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG',
  `delete_flag` int(1) NOT NULL DEFAULT 0 COMMENT ' 0--未删除 1--已删除 DIC_NAME=DEL_FLAG',
  `redirect_url` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '重定向地址',
  `component` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '该路由需要加载的页面组件的文件路径',
  `hidden` int(11) NULL DEFAULT 0 COMMENT '是否隐藏菜单 0为未隐藏 默认0',
  `always_show` int(11) NULL DEFAULT 1 COMMENT '是否一直显示 0 否 1.是',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `version` int(8) NULL DEFAULT NULL COMMENT '版本号',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `IDX1`(`menu_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1219 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gaea_menu
-- ----------------------------
INSERT INTO `gaea_menu` VALUES (1111, 'Org', '组织机构菜单', 'PC', '', '/organization', 'organization', 10, 1, 0, '/organization/index', '', 0, 0, 'admin', 'admin', '2021-02-05 17:09:49', '2021-03-08 09:00:04', 7);
INSERT INTO `gaea_menu` VALUES (1113, 'Organization', '组织机构', 'PC', 'Org', 'index', 'organization', 1010, 1, 0, '', 'organization/index', 0, 1, 'admin', 'admin', '2021-02-07 15:07:30', '2021-03-03 14:46:41', 3);
INSERT INTO `gaea_menu` VALUES (1114, 'Authority', '权限管理', 'PC', '', '/authority', 'authority', 20, 1, 0, '/authority/permission', '', 0, 1, 'admin', 'admin', '2021-02-07 16:31:02', '2021-03-11 13:10:43', 3);
INSERT INTO `gaea_menu` VALUES (1116, 'MenuConfig', '菜单配置', 'PC', 'Authority', 'menu', '', 2020, 1, 0, '', 'authority/menu/index', 0, 1, 'admin', 'admin', '2021-02-07 16:32:56', '2021-03-04 14:52:50', 5);
INSERT INTO `gaea_menu` VALUES (1117, 'Role', '角色管理', 'PC', 'Authority', 'role', '', 2040, 1, 0, '', 'authority/role/index', 0, 1, 'admin', 'admin', '2021-02-07 16:33:25', '2021-03-03 14:18:42', 3);
INSERT INTO `gaea_menu` VALUES (1118, 'User', '用户管理', 'PC', 'Authority', 'user', '', 2050, 1, 0, '', 'authority/user/index', 0, 1, 'admin', 'admin', '2021-02-07 16:34:01', '2021-03-03 14:18:34', 2);
INSERT INTO `gaea_menu` VALUES (1120, 'Permission', '权限列表', 'PC', 'Authority', 'permission', NULL, 2010, 1, 0, NULL, 'authority/permission', 0, 1, 'admin', 'admin', '2021-03-03 14:01:41', '2021-03-03 14:11:48', 2);
INSERT INTO `gaea_menu` VALUES (1121, 'MenuDetail', '设定动态列', 'PC', 'Authority', 'menu-detail', '', 2030, 1, 0, '', 'authority/menu/menu-detail', 1, 1, 'admin', 'admin', '2021-03-03 14:12:26', '2021-03-13 14:19:26', 4);
INSERT INTO `gaea_menu` VALUES (1122, 'SystemSet', '系统设置', 'PC', '', '/system-set', 'systemSet', 30, 1, 0, '', '', 0, 1, 'admin', 'admin', '2021-03-03 14:26:50', '2021-03-03 14:31:58', 2);
INSERT INTO `gaea_menu` VALUES (1123, 'DataDictionary', '字典管理', 'PC', 'SystemSet', 'data-dictionary', '', 3010, 1, 0, '', 'system-set/data-dictionary/index', 0, 1, 'admin', 'admin', '2021-03-03 14:27:59', '2021-03-03 14:31:51', 2);
INSERT INTO `gaea_menu` VALUES (1124, 'Parameter', '参数管理', 'PC', 'SystemSet', 'parameter', '', 3020, 1, 0, '', 'system-set/parameter/index', 0, 1, 'admin', 'admin', '2021-03-03 14:28:47', '2021-03-03 14:28:47', 1);
INSERT INTO `gaea_menu` VALUES (1125, 'ParameterEdit', '参数管理编辑', 'PC', 'SystemSet', '/system-set/parameter/edit', '', 3030, 1, 0, '', 'system-set/parameter/component/edit', 1, 1, 'admin', 'admin', '2021-03-03 14:30:56', '2021-03-03 14:31:33', 2);
INSERT INTO `gaea_menu` VALUES (1126, 'Support', '帮助中心', 'PC', 'SystemSet', 'support', '', 3040, 1, 0, '', 'system-set/support/index', 0, 1, 'admin', 'admin', '2021-03-03 14:32:53', '2021-03-23 10:39:24', 3);
INSERT INTO `gaea_menu` VALUES (1127, 'OperationLog', '操作日志', 'PC', 'SystemSet', 'operation-log', '', 3050, 1, 0, '', 'system-set/operation-log', 0, 1, 'admin', 'admin', '2021-03-03 14:34:01', '2021-03-03 14:34:01', 1);
INSERT INTO `gaea_menu` VALUES (1128, 'PushNotify', '消息管理', 'PC', '', '/push-notify', 'pushNotify', 40, 1, 0, '/push-notify/history', '', 0, 1, 'admin', 'admin', '2021-03-03 14:35:40', '2021-03-03 14:35:40', 1);
INSERT INTO `gaea_menu` VALUES (1129, 'Situation', '收发概况', 'PC', 'PushNotify', 'situation', '', 4010, 1, 0, '', 'push-notify/situation', 0, 1, 'admin', 'admin', '2021-03-03 14:36:14', '2021-03-03 14:36:14', 1);
INSERT INTO `gaea_menu` VALUES (1130, 'Template', '推送模板', 'PC', 'PushNotify', 'template', '', 4020, 1, 0, '', 'push-notify/template/index', 0, 1, 'admin', 'admin', '2021-03-03 14:36:58', '2021-03-03 14:36:58', 1);
INSERT INTO `gaea_menu` VALUES (1131, 'History', '推送历史', 'PC', 'PushNotify', 'history', '', 4030, 1, 0, '', 'push-notify/history/index', 0, 1, 'admin', 'admin', '2021-03-03 14:37:53', '2021-03-03 14:37:53', 1);
INSERT INTO `gaea_menu` VALUES (1132, 'ComponentCenter', '组件中心', 'PC', '', '/component-center', 'componentCenter', 50, 1, 0, '', '/component-center/advanced-list', 0, 1, 'admin', 'admin', '2021-03-03 14:40:04', '2021-03-03 14:40:04', 1);
INSERT INTO `gaea_menu` VALUES (1133, 'AdvancedList', '高级查询/动态列', 'PC', 'ComponentCenter', 'advanced-list', '', 5010, 1, 0, '', 'component-center/advanced-list', 0, 1, 'admin', 'admin', '2021-03-03 14:41:56', '2021-04-25 09:28:01', 3);
INSERT INTO `gaea_menu` VALUES (1134, 'crudDemo', 'CRUD组件化案例', 'PC', 'ComponentCenter', 'crudDemo', '', 5020, 1, 0, '', 'component-center/anji-crud-demo', 0, 1, 'admin', 'admin', '2021-03-03 14:43:09', '2021-03-03 14:43:09', 1);
INSERT INTO `gaea_menu` VALUES (1135, 'DownloadMenu', '导出中心菜单', 'PC', '', '/download', 'download', 140, 1, 0, '/download/index', '', 0, 0, 'admin', 'admin', '2021-03-03 14:45:51', '2021-03-08 10:29:20', 4);
INSERT INTO `gaea_menu` VALUES (1136, 'Download', '导出中心', 'CTS-PC', 'DownloadMenu', 'index', NULL, 6010, 1, 0, '', 'download/index', 0, 1, 'admin', 'admin', '2021-03-03 14:47:26', '2021-03-09 13:14:44', 2);
INSERT INTO `gaea_menu` VALUES (1139, 'DictItem', '字典项', 'PC', 'SystemSet', 'dict-item', '', 3060, 1, 0, '', 'system-set/data-dictionary/dict-item', 1, 1, 'admin', 'admin', '2021-03-10 16:43:55', '2021-03-11 10:24:39', 2);
INSERT INTO `gaea_menu` VALUES (1140, 'Rules', '规则引擎', 'PC', 'SystemSet', 'rules', '', 1000, 1, 0, '', 'system-set/rules/index', 0, 1, 'admin', 'admin', '2021-03-12 16:15:03', '2021-03-12 16:16:50', 4);
INSERT INTO `gaea_menu` VALUES (1143, 'List', 'mock数据(开发者使用)', 'PC', 'ComponentCenter', 'list', '', 5030, 1, 0, '', 'list/index', 0, 1, 'admin', 'admin', '2021-03-15 13:44:19', '2021-03-19 10:39:36', 5);
INSERT INTO `gaea_menu` VALUES (1144, 'RulesDetail', '规则引擎编辑', 'PC', 'SystemSet', '/system-set/rules/edit', '', 1000, 1, 0, '', 'system-set/rules/edit', 1, 1, 'admin', 'admin', '2021-03-12 16:15:03', '2021-03-17 10:56:17', 5);
INSERT INTO `gaea_menu` VALUES (1145, 'Flowable', '工作流管理', 'PC', '', '/flowable', 'componentCenter', 90, 1, 0, '', '', 0, 1, 'admin', 'admin', '2021-03-17 16:21:46', '2021-03-17 17:21:21', 3);
INSERT INTO `gaea_menu` VALUES (1146, 'ProcessModel', '流程模板', 'PC', 'Flowable', '/process-model', '', 91, 1, 0, '', 'flowable/process-model/index', 0, 1, 'admin', 'admin', '2021-03-17 16:22:22', '2021-03-17 17:21:10', 2);
INSERT INTO `gaea_menu` VALUES (1147, 'ProcessDef', '流程定义', 'PC', 'Flowable', '/process', '', 92, 1, 0, '', 'flowable/process/index', 0, 1, 'admin', 'admin', '2021-03-17 16:22:22', '2021-03-17 17:21:10', 2);
INSERT INTO `gaea_menu` VALUES (1149, 'ProcessRunning', '我的待办', 'PC', 'Flowable', '/process/running', '', 92, 1, 0, '', 'flowable/running/index', 0, 1, 'admin', 'admin', '2021-03-17 16:22:22', '2021-03-17 17:21:10', 2);
INSERT INTO `gaea_menu` VALUES (1150, 'ProcessHistory', '我的已办', 'PC', 'Flowable', '/process/history', '', 92, 1, 0, '', 'flowable/history/index', 0, 1, 'admin', 'admin', '2021-03-17 16:22:22', '2021-03-17 17:21:10', 2);
INSERT INTO `gaea_menu` VALUES (1153, 'ProcessForm', '表单模板', 'PC', 'Flowable', '/process/form', '', 90, 1, 0, '', 'flowable/form/index', 0, 1, 'admin', 'admin', '2021-03-17 16:22:22', '2021-03-17 17:21:10', 2);
INSERT INTO `gaea_menu` VALUES (1154, 'ReportManager', '报表管理', 'PC', '', '/reportManager', 'report', 110, 1, 0, '', '', 0, 1, 'admin', 'admin', '2021-03-17 16:21:46', '2021-03-17 17:21:21', 3);
INSERT INTO `gaea_menu` VALUES (1155, 'Datasource', '数据源', 'PC', 'ReportManager', '/datasource', '', 11010, 1, 0, '', 'report/datasource/index', 0, 1, 'admin', 'admin', '2021-03-17 16:22:22', '2021-03-17 17:21:10', 2);
INSERT INTO `gaea_menu` VALUES (1156, 'Resultset', '数据集', 'PC', 'ReportManager', '/resultset', '', 11020, 1, 0, '', 'report/resultset/index', 0, 1, 'admin', 'admin', '2021-03-17 16:22:22', '2021-03-17 17:21:10', 2);
INSERT INTO `gaea_menu` VALUES (1157, 'Report', '报表管理', 'PC', 'ReportManager', '/report', '', 11030, 1, 0, '', 'report/report/index', 0, 1, 'admin', 'admin', '2021-03-17 16:22:22', '2021-03-17 17:21:10', 2);
INSERT INTO `gaea_menu` VALUES (1158, 'Bigscreen', '大屏报表', 'PC', 'ReportManager', '/bigscreen', '', 11040, 1, 0, '', 'report/bigscreen/index', 0, 1, 'admin', 'admin', '2021-03-17 16:22:22', '2021-03-17 17:21:10', 2);
INSERT INTO `gaea_menu` VALUES (1159, 'Excelreport', '表格报表', 'PC', 'ReportManager', '/excelreport', '', 11050, 1, 0, '', 'report/excelreport/index', 0, 1, 'admin', 'admin', '2021-03-17 16:22:22', '2021-03-17 17:21:10', 2);
INSERT INTO `gaea_menu` VALUES (1165, 'GaeaUiI18n', 'UI国际化设置', 'PC', 'SystemSet', 'gaeaUiI18n/index', '', 3070, 1, 0, '', 'system-set/gaeaUiI18n/index', 0, 1, 'admin', 'admin', '2021-03-25 14:16:26', '2021-03-30 10:35:58', 9);
INSERT INTO `gaea_menu` VALUES (1169, 'INF', '接口中心', 'PC', '', '', 'componentCenter', 130, 1, 0, '', '', 0, 1, 'admin', 'admin', '2021-03-26 15:55:42', '2021-07-15 15:58:44', 4);
INSERT INTO `gaea_menu` VALUES (1170, 'ParameterAdd', '参数管理新增', 'PC', 'SystemSet', '/system-set/parameter/add', '', 3031, 1, 0, '', 'system-set/parameter/component/edit', 1, 1, 'admin', 'admin', '2021-03-19 16:35:00', '2021-03-19 16:35:31', 2);
INSERT INTO `gaea_menu` VALUES (1171, 'RulesAdd', '引擎规则新增', 'PC', 'SystemSet', '/system-set/rules/add', '', 1000, 1, 0, '', 'system-set/rules/edit', 1, 1, 'admin', 'admin', '2021-03-22 13:14:49', '2021-03-22 13:15:08', 2);
INSERT INTO `gaea_menu` VALUES (1172, 'INF_MYAPP', '我的应用', 'PC', 'INF', '/system-set/my-application', '', 0, 1, 0, '', 'system-set/my-application/index', 0, 1, 'admin', 'admin', '2021-03-26 16:00:17', '2021-03-26 16:00:17', 1);
INSERT INTO `gaea_menu` VALUES (1173, 'INF_MGT', '接口列表', 'PC', 'INF', '/system-set/interface', '', 0, 1, 0, '', 'system-set/interface/index', 0, 1, 'admin', 'admin', '2021-03-26 16:01:03', '2021-04-06 17:45:19', 6);
INSERT INTO `gaea_menu` VALUES (1175, 'FlowableTaskHandler', '任务办理', 'PC', 'Flowable', '/flowable/running/handler', '', 1000, 1, 0, '', 'flowable/running/handler', 1, 1, 'admin', 'admin', '2021-03-22 13:14:49', '2021-03-22 13:15:08', 1);
INSERT INTO `gaea_menu` VALUES (1180, 'interfaceEdit', '接口编辑', 'PC', 'INF', '/system-set/interface/edit', '', 0, 1, 0, '', 'system-set/interface/interfaceEdit/index', 1, 1, 'admin', 'admin', '2021-03-31 10:11:39', '2021-03-31 11:59:40', 3);
INSERT INTO `gaea_menu` VALUES (1181, 'FlowableHisDetail', '工作流历史详情', 'PC', 'Flowable', '/flowable/history/detail', '', 1001, 1, 0, '', 'flowable/history/detail', 1, 1, 'admin', 'admin', '2021-03-22 13:14:49', '2021-03-22 13:15:08', 1);
INSERT INTO `gaea_menu` VALUES (1187, 'Generator', '代码生成', 'PC', '', '/generator', 'clipboard', 120, 1, 0, '', '', 0, 1, 'admin', 'admin', '2021-04-16 13:23:27', '2021-04-16 13:23:27', 1);
INSERT INTO `gaea_menu` VALUES (1188, 'Project', '项目列表', 'PC', 'Generator', 'project', '', 121, 1, 0, '', 'generator/project', 0, 1, 'admin', 'admin', '2021-04-16 13:28:40', '2021-04-16 13:31:03', 3);
INSERT INTO `gaea_menu` VALUES (1189, 'Index', '项目数据源', 'PC', 'Generator', 'index', '', 122, 1, 0, '', 'generator/index', 1, 1, 'admin', 'admin', '2021-04-16 14:46:57', '2021-04-16 15:06:50', 3);
INSERT INTO `gaea_menu` VALUES (1190, 'Config', '生成配置', 'PC', 'Generator', 'config', '', 123, 1, 0, '', 'generator/config', 1, 1, 'admin', 'admin', '2021-04-16 15:07:31', '2021-04-16 15:12:38', 2);
INSERT INTO `gaea_menu` VALUES (1191, 'Init', '项目初始化', 'PC', 'Generator', 'init', '', 124, 1, 0, '', 'generator/init', 1, 1, 'admin', 'admin', '2021-04-16 15:18:26', '2021-04-16 15:18:26', 1);
INSERT INTO `gaea_menu` VALUES (1192, 'Preview', '代码预览', 'PC', 'Generator', 'preview', '', 125, 1, 0, '', 'generator/preview', 1, 1, 'admin', 'admin', '2021-04-16 15:19:07', '2021-04-16 15:19:07', 1);
INSERT INTO `gaea_menu` VALUES (1193, 'AlipayConfig', '生成示例-单表', 'PC', 'Generator', 'alipayConfig', '', 126, 1, 0, '', 'alipayConfig/index', 0, 1, 'admin', 'admin', '2021-04-16 15:19:58', '2021-04-16 15:19:58', 1);
INSERT INTO `gaea_menu` VALUES (1194, 'AlipayConfigDetail', '生成示例-单表详情', 'PC', 'Generator', 'alipayConfigDetail', '', 127, 1, 0, '', 'alipayConfig/component/detail', 1, 1, 'admin', 'admin', '2021-04-16 15:20:45', '2021-04-16 15:20:45', 1);
INSERT INTO `gaea_menu` VALUES (1195, 'DeviceInfo', '设备信息-主表', 'PC', 'Generator', 'deviceInfo', '', 128, 1, 0, '', 'deviceInfo/index', 0, 1, 'admin', 'admin', '2021-04-16 15:21:34', '2021-04-16 15:21:34', 1);
INSERT INTO `gaea_menu` VALUES (1196, 'DeviceModel', '设备类型-子表', 'PC', 'Generator', 'deviceModel', '', 129, 1, 0, '', 'deviceModel/index', 0, 1, 'admin', 'admin', '2021-04-16 15:22:06', '2021-04-16 15:22:06', 1);
INSERT INTO `gaea_menu` VALUES (1197, 'DeviceLogDetail', '设备日志-子表', 'PC', 'Generator', 'deviceLogDetail', '', 130, 1, 0, '', 'deviceLogDetail/index', 0, 1, 'admin', 'admin', '2021-04-16 15:22:56', '2021-04-16 15:22:56', 1);
INSERT INTO `gaea_menu` VALUES (1198, 'DeviceInfoDetail', '设备信息-详情', 'PC', 'Generator', 'deviceInfoDetail', '', 0, 1, 0, '', 'deviceInfo/component/detail', 1, 1, 'admin', 'admin', '2021-04-16 15:23:46', '2021-04-16 15:23:46', 1);
INSERT INTO `gaea_menu` VALUES (1199, 'DeviceModelDetail', '设备类型-详情', 'PC', 'Generator', 'deviceModelDetail', '', 131, 1, 0, '', 'deviceModel/component/detail', 1, 1, 'admin', 'admin', '2021-04-16 15:24:24', '2021-04-16 15:24:24', 1);
INSERT INTO `gaea_menu` VALUES (1200, 'DeviceLogDetailInfo', '设备日志-详情', 'PC', 'Generator', 'deviceLogDetailInfo', '', 0, 1, 0, '', 'deviceLogDetail/component/detail', 1, 1, 'admin', 'admin', '2021-04-16 15:24:57', '2021-04-16 15:24:57', 1);
INSERT INTO `gaea_menu` VALUES (1201, 'Announcement', '系统通告', 'PC', 'SystemSet', 'announcement', '', 3091, 1, 0, '', 'announcement/index', 0, 1, 'admin', 'admin', '2021-04-16 15:35:37', '2021-04-16 17:08:45', 2);
INSERT INTO `gaea_menu` VALUES (1202, 'MyMassage', '我的消息', 'PC', 'SystemSet', 'my-massage', '', 3092, 1, 0, '', 'my-massage/index', 1, 1, 'admin', 'admin', '2021-04-16 15:45:05', '2021-04-16 15:45:05', 1);
INSERT INTO `gaea_menu` VALUES (1203, 'MyMassageDetails', '我的消息详情', 'PC', 'SystemSet', 'my-massageDetails', '', 3093, 1, 0, '', 'my-massage/component/index', 1, 1, 'admin', 'admin', '2021-04-16 15:45:51', '2021-04-21 09:38:24', 3);
INSERT INTO `gaea_menu` VALUES (1208, 'API', 'API文档', 'PC', 'INF', '/open/view/api', '', 0, 1, 0, '', 'apiList/main', 0, 1, 'admin', 'admin', '2021-04-16 16:13:02', '2021-04-16 17:09:37', 2);
INSERT INTO `gaea_menu` VALUES (1209, 'OpenApiList', 'API列表', 'PC', 'INF', 'index/:id', '', 1, 1, 0, '', 'apiList/list', 1, 1, 'admin', 'admin', '2021-04-16 16:14:34', '2021-04-16 16:15:27', 2);
INSERT INTO `gaea_menu` VALUES (1210, 'OpenApiDetail', '接口明细', 'PC', 'INF', 'detail/:id', '', 1, 1, 0, '', 'apiList/detail', 1, 1, 'admin', 'admin', '2021-04-16 16:19:49', '2021-04-16 16:19:49', 1);
INSERT INTO `gaea_menu` VALUES (1211, 'OpenApiAccess', '服务调用方', 'PC', 'INF', 'access', '', 1, 1, 0, '', 'apiList/portal/access', 1, 1, 'admin', 'admin', '2021-04-16 16:20:14', '2021-04-16 16:20:14', 1);
INSERT INTO `gaea_menu` VALUES (1212, 'OpenApiGuide', '服务提供方', 'PC', 'INF', 'guide', '', 0, 1, 0, '', 'apiList/portal/guide', 1, 1, 'admin', 'admin', '2021-04-16 16:20:43', '2021-04-16 16:20:43', 1);
INSERT INTO `gaea_menu` VALUES (1213, 'OpenApiParam', '接口参数', 'PC', 'INF', 'param', '', 1, 1, 0, '', 'apiList/portal/param', 1, 1, 'admin', 'admin', '2021-04-16 16:21:16', '2021-04-16 16:21:16', 1);
INSERT INTO `gaea_menu` VALUES (1214, 'OpenApiSign', '安全方式', 'PC', 'INF', 'sign', '', 1, 1, 0, '', 'apiList/portal/sign', 1, 1, 'admin', 'admin', '2021-04-16 16:21:44', '2021-04-16 16:21:44', 1);
INSERT INTO `gaea_menu` VALUES (1215, 'OpenApiCode', '响应码查询', 'PC', 'INF', 'code', '', 1, 1, 0, '', 'apiList/portal/code', 1, 1, 'admin', 'admin', '2021-04-16 16:22:16', '2021-04-16 16:22:16', 1);
INSERT INTO `gaea_menu` VALUES (1216, 'SvgDemo', '图标库', 'PC', 'ComponentCenter', 'gallery', '', 0, 1, 0, '', 'component-center/gallery', 0, 1, 'admin', 'admin', '2021-04-16 16:26:39', '2021-04-16 16:53:23', 4);
INSERT INTO `gaea_menu` VALUES (1218, 'FileManagement', '文件管理', 'PC', 'SystemSet', 'file-management', '', 3100, 1, 0, '', 'system-set/file-management/index', 0, 1, 'admin', 'admin', '2021-04-27 13:06:55', '2021-04-27 13:45:55', 6);

-- ----------------------------
-- Table structure for gaea_menu_authority
-- ----------------------------
DROP TABLE IF EXISTS `gaea_menu_authority`;
CREATE TABLE `gaea_menu_authority`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `org_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '机构编码',
  `menu_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单id',
  `auth_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '操作id，如按钮',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `version` tinyint(8) NULL DEFAULT NULL COMMENT '版本号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_ara_authority_id`(`auth_code`) USING BTREE,
  INDEX `idx_ara_role_id`(`menu_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 633 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单按钮对应关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gaea_menu_authority
-- ----------------------------
INSERT INTO `gaea_menu_authority` VALUES (1, NULL, 'Permission', 'gaeaAuthorityController#roleAuthority', 'admin', 'admin', '2021-03-03 16:56:06', '2021-03-03 16:56:06', 1);
INSERT INTO `gaea_menu_authority` VALUES (2, NULL, 'Permission', 'gaeaAuthorityController#update', 'admin', 'admin', '2021-03-03 16:56:06', '2021-03-03 16:56:06', 1);
INSERT INTO `gaea_menu_authority` VALUES (167, '3000004', 'MenuConfig', 'gaeaMenuController#userInfo', 'admin', 'admin', '2021-03-05 17:26:36', '2021-03-05 17:26:36', 1);
INSERT INTO `gaea_menu_authority` VALUES (168, '3000004', 'MenuConfig', 'gaeaMenuController#getTree', 'admin', 'admin', '2021-03-05 17:26:36', '2021-03-05 17:26:36', 1);
INSERT INTO `gaea_menu_authority` VALUES (169, '3000004', 'MenuConfig', 'gaeaMenuController#getMenuInfoByOrg', 'admin', 'admin', '2021-03-05 17:26:36', '2021-03-05 17:26:36', 1);
INSERT INTO `gaea_menu_authority` VALUES (170, '3000004', 'MenuConfig', 'gaeaMenuController#menuAuthority', 'admin', 'admin', '2021-03-05 17:26:36', '2021-03-05 17:26:36', 1);
INSERT INTO `gaea_menu_authority` VALUES (171, '3000004', 'MenuConfig', 'gaeaMenuController#update', 'admin', 'admin', '2021-03-05 17:26:36', '2021-03-05 17:26:36', 1);
INSERT INTO `gaea_menu_authority` VALUES (172, '3000004', 'MenuConfig', 'gaeaMenuController#insert', 'admin', 'admin', '2021-03-05 17:26:36', '2021-03-05 17:26:36', 1);
INSERT INTO `gaea_menu_authority` VALUES (173, '3000004', 'MenuConfig', 'gaeaMenuController#detail', 'admin', 'admin', '2021-03-05 17:26:36', '2021-03-05 17:26:36', 1);
INSERT INTO `gaea_menu_authority` VALUES (174, '3000004', 'MenuConfig', 'gaeaMenuController#deleteBatchIds', 'admin', 'admin', '2021-03-05 17:26:36', '2021-03-05 17:26:36', 1);
INSERT INTO `gaea_menu_authority` VALUES (175, '3000004', 'MenuConfig', 'gaeaMenuController#deleteById', 'admin', 'admin', '2021-03-05 17:26:36', '2021-03-05 17:26:36', 1);
INSERT INTO `gaea_menu_authority` VALUES (176, '3000004', 'MenuConfig', 'gaeaMenuController#pageList', 'admin', 'admin', '2021-03-05 17:26:36', '2021-03-05 17:26:36', 1);
INSERT INTO `gaea_menu_authority` VALUES (177, '3000004', 'MenuConfig', 'gaeaMenuController#authorityTree', 'admin', 'admin', '2021-03-05 17:26:36', '2021-03-05 17:26:36', 1);
INSERT INTO `gaea_menu_authority` VALUES (178, '3000004', 'Role', 'gaeaRoleController#update', 'admin', 'admin', '2021-03-05 17:26:48', '2021-03-05 17:26:48', 1);
INSERT INTO `gaea_menu_authority` VALUES (179, '3000004', 'Role', 'gaeaRoleController#insert', 'admin', 'admin', '2021-03-05 17:26:48', '2021-03-05 17:26:48', 1);
INSERT INTO `gaea_menu_authority` VALUES (180, '3000004', 'Role', 'gaeaRoleController#detail', 'admin', 'admin', '2021-03-05 17:26:48', '2021-03-05 17:26:48', 1);
INSERT INTO `gaea_menu_authority` VALUES (181, '3000004', 'Role', 'gaeaRoleController#deleteBatchIds', 'admin', 'admin', '2021-03-05 17:26:48', '2021-03-05 17:26:48', 1);
INSERT INTO `gaea_menu_authority` VALUES (182, '3000004', 'Role', 'gaeaRoleController#deleteById', 'admin', 'admin', '2021-03-05 17:26:48', '2021-03-05 17:26:48', 1);
INSERT INTO `gaea_menu_authority` VALUES (183, '3000004', 'Role', 'gaeaRoleController#pageList', 'admin', 'admin', '2021-03-05 17:26:48', '2021-03-05 17:26:48', 1);
INSERT INTO `gaea_menu_authority` VALUES (184, '3000004', 'Role', 'gaeaRoleController#roleAuthorities', 'admin', 'admin', '2021-03-05 17:26:48', '2021-03-05 17:26:48', 1);
INSERT INTO `gaea_menu_authority` VALUES (185, '3000004', 'Role', 'gaeaRoleController#saveOrgTreeForRole', 'admin', 'admin', '2021-03-05 17:26:48', '2021-03-05 17:26:48', 1);
INSERT INTO `gaea_menu_authority` VALUES (186, '3000004', 'Role', 'gaeaRoleController#queryOrgTreeForRole', 'admin', 'admin', '2021-03-05 17:26:48', '2021-03-05 17:26:48', 1);
INSERT INTO `gaea_menu_authority` VALUES (187, '3000004', 'Role', 'gaeaRoleController#saveMenuActionTreeForRole', 'admin', 'admin', '2021-03-05 17:26:48', '2021-03-05 17:26:48', 1);
INSERT INTO `gaea_menu_authority` VALUES (214, '3000004', 'Support', 'gaeaHelpController#listAll', 'admin', 'admin', '2021-03-05 17:36:14', '2021-03-05 17:36:14', 1);
INSERT INTO `gaea_menu_authority` VALUES (215, '3000004', 'Support', 'gaeaHelpController#demo', 'admin', 'admin', '2021-03-05 17:36:14', '2021-03-05 17:36:14', 1);
INSERT INTO `gaea_menu_authority` VALUES (216, '3000004', 'Support', 'gaeaHelpController#update', 'admin', 'admin', '2021-03-05 17:36:14', '2021-03-05 17:36:14', 1);
INSERT INTO `gaea_menu_authority` VALUES (217, '3000004', 'Support', 'gaeaHelpController#insert', 'admin', 'admin', '2021-03-05 17:36:14', '2021-03-05 17:36:14', 1);
INSERT INTO `gaea_menu_authority` VALUES (218, '3000004', 'Support', 'gaeaHelpController#detail', 'admin', 'admin', '2021-03-05 17:36:14', '2021-03-05 17:36:14', 1);
INSERT INTO `gaea_menu_authority` VALUES (219, '3000004', 'Support', 'gaeaHelpController#deleteBatchIds', 'admin', 'admin', '2021-03-05 17:36:14', '2021-03-05 17:36:14', 1);
INSERT INTO `gaea_menu_authority` VALUES (220, '3000004', 'Support', 'gaeaHelpController#deleteById', 'admin', 'admin', '2021-03-05 17:36:14', '2021-03-05 17:36:14', 1);
INSERT INTO `gaea_menu_authority` VALUES (221, '3000004', 'Support', 'gaeaHelpController#pageList', 'admin', 'admin', '2021-03-05 17:36:14', '2021-03-05 17:36:14', 1);
INSERT INTO `gaea_menu_authority` VALUES (243, '3000004', 'History', 'gaeaPushHistoryController#getPushStatistics', 'admin', 'admin', '2021-03-05 17:37:59', '2021-03-05 17:37:59', 1);
INSERT INTO `gaea_menu_authority` VALUES (244, '3000004', 'History', 'gaeaPushHistoryController#update', 'admin', 'admin', '2021-03-05 17:37:59', '2021-03-05 17:37:59', 1);
INSERT INTO `gaea_menu_authority` VALUES (245, '3000004', 'History', 'gaeaPushHistoryController#insert', 'admin', 'admin', '2021-03-05 17:37:59', '2021-03-05 17:37:59', 1);
INSERT INTO `gaea_menu_authority` VALUES (246, '3000004', 'History', 'gaeaPushHistoryController#detail', 'admin', 'admin', '2021-03-05 17:37:59', '2021-03-05 17:37:59', 1);
INSERT INTO `gaea_menu_authority` VALUES (247, '3000004', 'History', 'gaeaPushHistoryController#deleteBatchIds', 'admin', 'admin', '2021-03-05 17:37:59', '2021-03-05 17:37:59', 1);
INSERT INTO `gaea_menu_authority` VALUES (248, '3000004', 'History', 'gaeaPushHistoryController#deleteById', 'admin', 'admin', '2021-03-05 17:37:59', '2021-03-05 17:37:59', 1);
INSERT INTO `gaea_menu_authority` VALUES (249, '3000004', 'History', 'gaeaPushHistoryController#pageList', 'admin', 'admin', '2021-03-05 17:37:59', '2021-03-05 17:37:59', 1);
INSERT INTO `gaea_menu_authority` VALUES (250, '3000004', 'Template', 'gaeaPushTemplateController#testSendPush', 'admin', 'admin', '2021-03-05 17:38:15', '2021-03-05 17:38:15', 1);
INSERT INTO `gaea_menu_authority` VALUES (251, '3000004', 'Template', 'gaeaPushTemplateController#preview', 'admin', 'admin', '2021-03-05 17:38:15', '2021-03-05 17:38:15', 1);
INSERT INTO `gaea_menu_authority` VALUES (252, '3000004', 'Template', 'gaeaPushTemplateController#update', 'admin', 'admin', '2021-03-05 17:38:15', '2021-03-05 17:38:15', 1);
INSERT INTO `gaea_menu_authority` VALUES (253, '3000004', 'Template', 'gaeaPushTemplateController#insert', 'admin', 'admin', '2021-03-05 17:38:15', '2021-03-05 17:38:15', 1);
INSERT INTO `gaea_menu_authority` VALUES (254, '3000004', 'Template', 'gaeaPushTemplateController#detail', 'admin', 'admin', '2021-03-05 17:38:15', '2021-03-05 17:38:15', 1);
INSERT INTO `gaea_menu_authority` VALUES (255, '3000004', 'Template', 'gaeaPushTemplateController#deleteBatchIds', 'admin', 'admin', '2021-03-05 17:38:15', '2021-03-05 17:38:15', 1);
INSERT INTO `gaea_menu_authority` VALUES (256, '3000004', 'Template', 'gaeaPushTemplateController#deleteById', 'admin', 'admin', '2021-03-05 17:38:15', '2021-03-05 17:38:15', 1);
INSERT INTO `gaea_menu_authority` VALUES (257, '3000004', 'Template', 'gaeaPushTemplateController#pageList', 'admin', 'admin', '2021-03-05 17:38:15', '2021-03-05 17:38:15', 1);
INSERT INTO `gaea_menu_authority` VALUES (258, '3000004', 'Download', 'gaeaExportController#export', 'admin', 'admin', '2021-03-05 17:38:41', '2021-03-05 17:38:41', 1);
INSERT INTO `gaea_menu_authority` VALUES (259, '3000004', 'Download', 'gaeaExportController#queryExportInfo', 'admin', 'admin', '2021-03-05 17:38:41', '2021-03-05 17:38:41', 1);
INSERT INTO `gaea_menu_authority` VALUES (260, '3000004', 'Download', 'gaeaExportController#update', 'admin', 'admin', '2021-03-05 17:38:41', '2021-03-05 17:38:41', 1);
INSERT INTO `gaea_menu_authority` VALUES (261, '3000004', 'Download', 'gaeaExportController#insert', 'admin', 'admin', '2021-03-05 17:38:41', '2021-03-05 17:38:41', 1);
INSERT INTO `gaea_menu_authority` VALUES (262, '3000004', 'Download', 'gaeaExportController#detail', 'admin', 'admin', '2021-03-05 17:38:41', '2021-03-05 17:38:41', 1);
INSERT INTO `gaea_menu_authority` VALUES (263, '3000004', 'Download', 'gaeaExportController#deleteBatchIds', 'admin', 'admin', '2021-03-05 17:38:41', '2021-03-05 17:38:41', 1);
INSERT INTO `gaea_menu_authority` VALUES (264, '3000004', 'Download', 'gaeaExportController#deleteById', 'admin', 'admin', '2021-03-05 17:38:41', '2021-03-05 17:38:41', 1);
INSERT INTO `gaea_menu_authority` VALUES (265, '3000004', 'Download', 'gaeaExportController#pageList', 'admin', 'admin', '2021-03-05 17:38:41', '2021-03-05 17:38:41', 1);
INSERT INTO `gaea_menu_authority` VALUES (488, '3000004', 'MenuDetail', 'gaeaMenuExtensionController#queryMenuExtension', 'admin', 'admin', '2021-03-13 14:24:38', '2021-03-13 14:24:38', 1);
INSERT INTO `gaea_menu_authority` VALUES (489, '3000004', 'DataDictionary', 'gaeaDictController#refresh', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', 1);
INSERT INTO `gaea_menu_authority` VALUES (490, '3000004', 'DataDictionary', 'gaeaDictController#select', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', 1);
INSERT INTO `gaea_menu_authority` VALUES (491, '3000004', 'DataDictionary', 'gaeaDictController#detail', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', 1);
INSERT INTO `gaea_menu_authority` VALUES (492, '3000004', 'DataDictionary', 'gaeaDictController#pageList', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', 1);
INSERT INTO `gaea_menu_authority` VALUES (493, '3000004', 'DataDictionary', 'gaeaDictController#deleteBatchIds', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', 1);
INSERT INTO `gaea_menu_authority` VALUES (494, '3000004', 'DataDictionary', 'gaeaDictController#deleteById', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', 1);
INSERT INTO `gaea_menu_authority` VALUES (495, '3000004', 'DataDictionary', 'gaeaDictController#update', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', 1);
INSERT INTO `gaea_menu_authority` VALUES (496, '3000004', 'DataDictionary', 'gaeaDictController#insert', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', 1);
INSERT INTO `gaea_menu_authority` VALUES (497, '3000004', 'DataDictionary', 'gaeaDictItemController#detail', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', 1);
INSERT INTO `gaea_menu_authority` VALUES (498, '3000004', 'DataDictionary', 'gaeaDictItemController#pageList', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', 1);
INSERT INTO `gaea_menu_authority` VALUES (499, '3000004', 'DataDictionary', 'gaeaDictItemController#deleteBatchIds', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', 1);
INSERT INTO `gaea_menu_authority` VALUES (500, '3000004', 'DataDictionary', 'gaeaDictItemController#deleteById', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', 1);
INSERT INTO `gaea_menu_authority` VALUES (501, '3000004', 'DataDictionary', 'gaeaDictItemController#update', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', 1);
INSERT INTO `gaea_menu_authority` VALUES (502, '3000004', 'DataDictionary', 'gaeaDictItemController#insert', 'admin', 'admin', '2021-03-15 14:38:44', '2021-03-15 14:38:44', 1);
INSERT INTO `gaea_menu_authority` VALUES (507, '3000004', 'OperationLog', 'gaeaLogController#update', 'admin', 'admin', '2021-03-15 15:54:59', '2021-03-15 15:54:59', 1);
INSERT INTO `gaea_menu_authority` VALUES (508, '3000004', 'OperationLog', 'gaeaLogController#insert', 'admin', 'admin', '2021-03-15 15:54:59', '2021-03-15 15:54:59', 1);
INSERT INTO `gaea_menu_authority` VALUES (509, '3000004', 'OperationLog', 'gaeaLogController#detail', 'admin', 'admin', '2021-03-15 15:54:59', '2021-03-15 15:54:59', 1);
INSERT INTO `gaea_menu_authority` VALUES (510, '3000004', 'OperationLog', 'gaeaLogController#deleteBatchIds', 'admin', 'admin', '2021-03-15 15:54:59', '2021-03-15 15:54:59', 1);
INSERT INTO `gaea_menu_authority` VALUES (511, '3000004', 'OperationLog', 'gaeaLogController#deleteById', 'admin', 'admin', '2021-03-15 15:54:59', '2021-03-15 15:54:59', 1);
INSERT INTO `gaea_menu_authority` VALUES (512, '3000004', 'OperationLog', 'gaeaLogController#pageList', 'admin', 'admin', '2021-03-15 15:54:59', '2021-03-15 15:54:59', 1);
INSERT INTO `gaea_menu_authority` VALUES (513, '3000004', 'OperationLog', 'gaeaLogController#exportLogToFile', 'admin', 'admin', '2021-03-15 15:54:59', '2021-03-15 15:54:59', 1);
INSERT INTO `gaea_menu_authority` VALUES (514, '3000004', 'Rules', 'gaeaRulesController#executeRules', 'admin', 'admin', '2021-03-16 12:33:57', '2021-03-16 12:33:57', 1);
INSERT INTO `gaea_menu_authority` VALUES (515, '3000004', 'Rules', 'gaeaRulesController#update', 'admin', 'admin', '2021-03-16 12:33:57', '2021-03-16 12:33:57', 1);
INSERT INTO `gaea_menu_authority` VALUES (516, '3000004', 'Rules', 'gaeaRulesController#insert', 'admin', 'admin', '2021-03-16 12:33:57', '2021-03-16 12:33:57', 1);
INSERT INTO `gaea_menu_authority` VALUES (517, '3000004', 'Rules', 'gaeaRulesController#detail', 'admin', 'admin', '2021-03-16 12:33:57', '2021-03-16 12:33:57', 1);
INSERT INTO `gaea_menu_authority` VALUES (518, '3000004', 'Rules', 'gaeaRulesController#pageList', 'admin', 'admin', '2021-03-16 12:33:57', '2021-03-16 12:33:57', 1);
INSERT INTO `gaea_menu_authority` VALUES (519, '3000004', 'Rules', 'gaeaRulesController#deleteById', 'admin', 'admin', '2021-03-16 12:33:57', '2021-03-16 12:33:57', 1);
INSERT INTO `gaea_menu_authority` VALUES (520, '3000004', 'Rules', 'gaeaRulesController#deleteBatchIds', 'admin', 'admin', '2021-03-16 12:33:57', '2021-03-16 12:33:57', 1);
INSERT INTO `gaea_menu_authority` VALUES (521, '3000004', 'Authority', 'gaeaAuthorityController#authorityTree', 'admin', 'admin', '2021-03-16 13:21:43', '2021-03-16 13:21:43', 1);
INSERT INTO `gaea_menu_authority` VALUES (522, '3000004', 'Authority', 'gaeaAuthorityController#roleAuthority', 'admin', 'admin', '2021-03-16 13:21:43', '2021-03-16 13:21:43', 1);
INSERT INTO `gaea_menu_authority` VALUES (523, '3000004', 'Authority', 'gaeaAuthorityController#update', 'admin', 'admin', '2021-03-16 13:21:43', '2021-03-16 13:21:43', 1);
INSERT INTO `gaea_menu_authority` VALUES (545, '3000004', 'Parameter', 'gaeaSettingController#update', 'zouya', 'zouya', '2021-03-18 13:50:07', '2021-03-18 13:50:07', 1);
INSERT INTO `gaea_menu_authority` VALUES (546, '3000004', 'Parameter', 'gaeaSettingController#insert', 'zouya', 'zouya', '2021-03-18 13:50:07', '2021-03-18 13:50:07', 1);
INSERT INTO `gaea_menu_authority` VALUES (547, '3000004', 'Parameter', 'gaeaSettingController#detail', 'zouya', 'zouya', '2021-03-18 13:50:07', '2021-03-18 13:50:07', 1);
INSERT INTO `gaea_menu_authority` VALUES (548, '3000004', 'Parameter', 'gaeaSettingController#deleteBatchIds', 'zouya', 'zouya', '2021-03-18 13:50:07', '2021-03-18 13:50:07', 1);
INSERT INTO `gaea_menu_authority` VALUES (549, '3000004', 'Parameter', 'gaeaSettingController#deleteById', 'zouya', 'zouya', '2021-03-18 13:50:07', '2021-03-18 13:50:07', 1);
INSERT INTO `gaea_menu_authority` VALUES (550, '3000004', 'Parameter', 'gaeaSettingController#pageList', 'zouya', 'zouya', '2021-03-18 13:50:07', '2021-03-18 13:50:07', 1);
INSERT INTO `gaea_menu_authority` VALUES (551, '3000004', 'Organization', 'gaeaOrgController#queryAllOrg', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', 1);
INSERT INTO `gaea_menu_authority` VALUES (552, '3000004', 'Organization', 'gaeaOrgController#orgRoleTree', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', 1);
INSERT INTO `gaea_menu_authority` VALUES (553, '3000004', 'Organization', 'gaeaOrgController#orgSelect', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', 1);
INSERT INTO `gaea_menu_authority` VALUES (554, '3000004', 'Organization', 'gaeaOrgController#orgTree', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', 1);
INSERT INTO `gaea_menu_authority` VALUES (555, '3000004', 'Organization', 'gaeaOrgController#saveOrg', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', 1);
INSERT INTO `gaea_menu_authority` VALUES (556, '3000004', 'Organization', 'gaeaOrgController#updateOrg', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', 1);
INSERT INTO `gaea_menu_authority` VALUES (557, '3000004', 'Organization', 'gaeaOrgController#orgRoleTreeSelected', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', 1);
INSERT INTO `gaea_menu_authority` VALUES (558, '3000004', 'Organization', 'gaeaOrgController#update', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', 1);
INSERT INTO `gaea_menu_authority` VALUES (559, '3000004', 'Organization', 'gaeaOrgController#insert', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', 1);
INSERT INTO `gaea_menu_authority` VALUES (560, '3000004', 'Organization', 'gaeaOrgController#detail', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', 1);
INSERT INTO `gaea_menu_authority` VALUES (561, '3000004', 'Organization', 'gaeaOrgController#deleteBatchIds', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', 1);
INSERT INTO `gaea_menu_authority` VALUES (562, '3000004', 'Organization', 'gaeaOrgController#deleteById', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', 1);
INSERT INTO `gaea_menu_authority` VALUES (563, '3000004', 'Organization', 'gaeaOrgController#pageList', 'zouya', 'zouya', '2021-03-23 10:24:50', '2021-03-23 10:24:50', 1);
INSERT INTO `gaea_menu_authority` VALUES (580, '3000004', 'GaeaUiI18n', 'gaeaUiI18nController#detail', 'admin', 'admin', '2021-04-02 10:02:56', '2021-04-02 10:02:56', 1);
INSERT INTO `gaea_menu_authority` VALUES (581, '3000004', 'GaeaUiI18n', 'gaeaUiI18nController#update', 'admin', 'admin', '2021-04-02 10:02:56', '2021-04-02 10:02:56', 1);
INSERT INTO `gaea_menu_authority` VALUES (582, '3000004', 'GaeaUiI18n', 'gaeaUiI18nController#insert', 'admin', 'admin', '2021-04-02 10:02:56', '2021-04-02 10:02:56', 1);
INSERT INTO `gaea_menu_authority` VALUES (583, '3000004', 'GaeaUiI18n', 'gaeaUiI18nController#pageList', 'admin', 'admin', '2021-04-02 10:02:56', '2021-04-02 10:02:56', 1);
INSERT INTO `gaea_menu_authority` VALUES (584, '3000004', 'GaeaUiI18n', 'gaeaUiI18nController#deleteById', 'admin', 'admin', '2021-04-02 10:02:56', '2021-04-02 10:02:56', 1);
INSERT INTO `gaea_menu_authority` VALUES (585, '3000004', 'GaeaUiI18n', 'gaeaUiI18nController#deleteBatchIds', 'admin', 'admin', '2021-04-02 10:02:56', '2021-04-02 10:02:56', 1);
INSERT INTO `gaea_menu_authority` VALUES (586, '3000004', 'GaeaUiI18n', 'gaeaUiI18nController#scan', 'admin', 'admin', '2021-04-02 10:02:56', '2021-04-02 10:02:56', 1);
INSERT INTO `gaea_menu_authority` VALUES (587, '3000004', 'GaeaUiI18n', 'gaeaUiI18nController#getI18nFields', 'admin', 'admin', '2021-04-02 10:02:56', '2021-04-02 10:02:56', 1);
INSERT INTO `gaea_menu_authority` VALUES (588, '3000004', 'GaeaUiI18n', 'gaeaUiI18nController#getTables', 'admin', 'admin', '2021-04-02 10:02:56', '2021-04-02 10:02:56', 1);
INSERT INTO `gaea_menu_authority` VALUES (589, '3000004', 'User', 'gaeaUserController#queryRoleTree', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', 1);
INSERT INTO `gaea_menu_authority` VALUES (590, '3000004', 'User', 'gaeaUserController#resetPassword', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', 1);
INSERT INTO `gaea_menu_authority` VALUES (591, '3000004', 'User', 'gaeaUserController#saveRoleTree', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', 1);
INSERT INTO `gaea_menu_authority` VALUES (592, '3000004', 'User', 'gaeaUserController#refreshCache', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', 1);
INSERT INTO `gaea_menu_authority` VALUES (593, '3000004', 'User', 'gaeaUserController#updatePassword', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', 1);
INSERT INTO `gaea_menu_authority` VALUES (594, '3000004', 'User', 'gaeaUserController#update', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', 1);
INSERT INTO `gaea_menu_authority` VALUES (595, '3000004', 'User', 'gaeaUserController#insert', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', 1);
INSERT INTO `gaea_menu_authority` VALUES (596, '3000004', 'User', 'gaeaUserController#detail', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', 1);
INSERT INTO `gaea_menu_authority` VALUES (597, '3000004', 'User', 'gaeaUserController#deleteBatchIds', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', 1);
INSERT INTO `gaea_menu_authority` VALUES (598, '3000004', 'User', 'gaeaUserController#deleteById', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', 1);
INSERT INTO `gaea_menu_authority` VALUES (599, '3000004', 'User', 'gaeaUserController#pageList', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', 1);
INSERT INTO `gaea_menu_authority` VALUES (600, '3000004', 'User', 'gaeaUserController#unLock', 'admin', 'admin', '2021-04-07 15:17:08', '2021-04-07 15:17:08', 1);
INSERT INTO `gaea_menu_authority` VALUES (625, '3000004', 'FileManagement', 'gaeaFileController#upload', 'admin', 'admin', '2021-05-06 13:54:38', '2021-05-06 13:54:38', 1);
INSERT INTO `gaea_menu_authority` VALUES (626, '3000004', 'FileManagement', 'gaeaFileController#export', 'admin', 'admin', '2021-05-06 13:54:38', '2021-05-06 13:54:38', 1);
INSERT INTO `gaea_menu_authority` VALUES (627, '3000004', 'FileManagement', 'gaeaFileController#deleteById', 'admin', 'admin', '2021-05-06 13:54:38', '2021-05-06 13:54:38', 1);
INSERT INTO `gaea_menu_authority` VALUES (628, '3000004', 'FileManagement', 'gaeaFileController#detail', 'admin', 'admin', '2021-05-06 13:54:38', '2021-05-06 13:54:38', 1);
INSERT INTO `gaea_menu_authority` VALUES (629, '3000004', 'FileManagement', 'gaeaFileController#pageList', 'admin', 'admin', '2021-05-06 13:54:38', '2021-05-06 13:54:38', 1);
INSERT INTO `gaea_menu_authority` VALUES (630, '3000004', 'FileManagement', 'gaeaFileController#deleteBatchIds', 'admin', 'admin', '2021-05-06 13:54:38', '2021-05-06 13:54:38', 1);
INSERT INTO `gaea_menu_authority` VALUES (631, '3000004', 'FileManagement', 'gaeaFileController#update', 'admin', 'admin', '2021-05-06 13:54:38', '2021-05-06 13:54:38', 1);
INSERT INTO `gaea_menu_authority` VALUES (632, '3000004', 'FileManagement', 'gaeaFileController#insert', 'admin', 'admin', '2021-05-06 13:54:38', '2021-05-06 13:54:38', 1);

-- ----------------------------
-- Table structure for gaea_menu_extension
-- ----------------------------
DROP TABLE IF EXISTS `gaea_menu_extension`;
CREATE TABLE `gaea_menu_extension`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `table_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格code',
  `menu_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单id',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列中文名',
  `code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列编码-和返回的表格数据里的key一致',
  `sortable` int(11) NULL DEFAULT 0 COMMENT '是否可排序,0-不可排序，1-可排序',
  `filterable` int(11) NULL DEFAULT 0 COMMENT '是否可以过滤 0.不可过滤 1.可过滤',
  `sort_no` int(11) NULL DEFAULT NULL COMMENT '排序号',
  `visible` int(11) NULL DEFAULT 1 COMMENT '0:不可见；1:可见',
  `width` int(11) NULL DEFAULT NULL COMMENT '宽度',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
  `create_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人ID',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_disabled` int(11) NULL DEFAULT 0 COMMENT '删除标志',
  `sort_code` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '排序字段',
  `sort_order` int(11) NULL DEFAULT 1 COMMENT '1升序，0降序，默认1',
  `group_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组名',
  `version` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_gme_01`(`table_code`, `menu_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gaea_menu_extension
-- ----------------------------
INSERT INTO `gaea_menu_extension` VALUES (11, 'advanceTable', 'AdvancedList', '用户名', 'username', 0, 0, 0, 1, 150, 'admin', '2021-03-15 17:56:34', 'pyn', '2021-03-15 17:56:34', 0, '', 1, '', 3);
INSERT INTO `gaea_menu_extension` VALUES (12, 'advanceTable', 'AdvancedList', '用户真实姓名', 'nickname', 0, 0, 0, 1, 150, 'admin', '2021-03-15 17:56:34', 'pyn', '2021-03-15 17:56:34', 0, '', 1, '', 2);
INSERT INTO `gaea_menu_extension` VALUES (13, 'advanceTable', 'AdvancedList', '用户手机号', 'phone', 0, 0, 0, 1, 200, 'admin', '2021-03-15 17:56:34', 'pyn', '2021-03-15 17:56:34', 0, '', 1, '', 2);
INSERT INTO `gaea_menu_extension` VALUES (14, 'advanceTable', 'AdvancedList', '账号状态', 'enable', 0, 0, 0, 1, 200, 'admin', '2021-04-13 11:13:51', 'admin', '2021-04-13 11:13:51', 0, '', 1, '', 3);
INSERT INTO `gaea_menu_extension` VALUES (15, 'advanceTable', 'AdvancedList', '用户邮箱', 'email', 0, 0, 0, 1, 100, 'admin', '2021-03-15 17:56:34', 'pyn', '2021-03-15 17:56:34', 0, '', 1, '', 2);
INSERT INTO `gaea_menu_extension` VALUES (16, 'advanceTable', 'AdvancedList', '创建时间', 'createTime', 1, 0, 0, 1, 200, 'admin', '2021-03-15 17:56:34', 'pyn', '2021-03-15 17:56:34', 0, 'gu.create_time', 1, '', 4);
INSERT INTO `gaea_menu_extension` VALUES (17, 'advanceTable', 'AdvancedList', '创建人', 'createBy', 0, 0, 0, 1, 100, 'admin', '2021-03-15 17:56:34', 'pyn', '2021-03-15 17:56:34', 0, '', 1, '', 2);
INSERT INTO `gaea_menu_extension` VALUES (18, 'advanceTable', 'AdvancedList', '修改时间', 'updateTime', 0, 1, 0, 1, 200, 'admin', '2021-04-25 16:49:44', 'admin', '2021-04-25 16:49:44', 0, 'gu.update_time', 1, '', 6);
INSERT INTO `gaea_menu_extension` VALUES (19, 'advanceTable', 'AdvancedList', '修改人', 'updateBy', 0, 0, 0, 1, 100, 'admin', '2021-03-15 17:56:34', 'pyn', '2021-03-15 17:56:34', 0, '', 1, '', 2);
INSERT INTO `gaea_menu_extension` VALUES (20, 'advanceTable', 'AdvancedList', '所属机构', 'orgNames', 0, 0, 0, 1, 200, 'pyn', '2021-03-15 17:56:34', 'pyn', '2021-03-15 17:56:34', 0, '', 1, '', 1);
INSERT INTO `gaea_menu_extension` VALUES (21, 'advanceTable', 'AdvancedList', '所属角色', 'roleNames', 0, 0, 0, 1, 150, 'pyn', '2021-03-15 17:56:34', 'pyn', '2021-03-15 17:56:34', 0, '', 1, '', 1);

-- ----------------------------
-- Table structure for gaea_menu_user_extension
-- ----------------------------
DROP TABLE IF EXISTS `gaea_menu_user_extension`;
CREATE TABLE `gaea_menu_user_extension`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `table_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格code',
  `menu_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单id',
  `username` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户ID',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列中文名',
  `code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列编码-和返回的表格数据里的key一致',
  `sortable` int(11) NULL DEFAULT 0 COMMENT '是否可排序,0-不可排序，1-可排序',
  `filterable` int(11) NULL DEFAULT 0 COMMENT '是否可以过滤 0.不可过滤 1.可过滤',
  `sort_no` int(11) NULL DEFAULT NULL COMMENT '排序号',
  `visible` int(11) NULL DEFAULT 1 COMMENT '0:不可见；1:可见',
  `width` int(11) NULL DEFAULT NULL COMMENT '宽度',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
  `create_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人ID',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_disabled` int(11) NULL DEFAULT 0 COMMENT '删除标志',
  `sort_code` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '排序字段',
  `sort_order` int(11) NULL DEFAULT 1 COMMENT '1升序，0降序，默认1',
  `group_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组名',
  `version` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_gmue_01`(`table_code`, `menu_code`, `username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 106 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gaea_menu_user_extension
-- ----------------------------
INSERT INTO `gaea_menu_user_extension` VALUES (51, 'advanceTable', 'AdvancedList', 'pyn', '用户名', 'username', 0, 0, 0, 1, 150, 'pyn', '2021-03-22 10:50:43', 'pyn', '2021-03-22 10:50:43', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (52, 'advanceTable', 'AdvancedList', 'pyn', '用户真实姓名', 'nickname', 0, 0, 0, 1, 150, 'pyn', '2021-03-22 10:50:43', 'pyn', '2021-03-22 10:50:43', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (53, 'advanceTable', 'AdvancedList', 'pyn', '用户手机号', 'phone', 0, 0, 0, 1, 200, 'pyn', '2021-03-22 10:50:43', 'pyn', '2021-03-22 10:50:43', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (54, 'advanceTable', 'AdvancedList', 'pyn', '账号状态', 'enableName', 0, 0, 0, 1, 200, 'pyn', '2021-03-22 10:50:43', 'pyn', '2021-03-22 10:50:43', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (55, 'advanceTable', 'AdvancedList', 'pyn', '用户邮箱', 'email', 0, 0, 0, 1, 100, 'pyn', '2021-03-22 10:50:43', 'pyn', '2021-03-22 10:50:43', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (56, 'advanceTable', 'AdvancedList', 'pyn', '创建时间', 'createTime', 1, 0, 0, 1, 200, 'pyn', '2021-03-22 10:50:43', 'pyn', '2021-03-22 10:50:43', 0, 'gu.create_time', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (57, 'advanceTable', 'AdvancedList', 'pyn', '创建人', 'createBy', 0, 0, 0, 1, 100, 'pyn', '2021-03-22 10:50:43', 'pyn', '2021-03-22 10:50:43', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (58, 'advanceTable', 'AdvancedList', 'pyn', '修改时间', 'updateTime', 1, 0, 0, 1, 200, 'pyn', '2021-03-22 10:50:43', 'pyn', '2021-03-22 10:50:43', 0, 'gu.update_time', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (59, 'advanceTable', 'AdvancedList', 'pyn', '修改人', 'updateBy', 0, 0, 0, 1, 100, 'pyn', '2021-03-22 10:50:43', 'pyn', '2021-03-22 10:50:43', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (60, 'advanceTable', 'AdvancedList', 'pyn', '所属机构', 'orgNames', 0, 0, 0, 1, 200, 'pyn', '2021-03-22 10:50:43', 'pyn', '2021-03-22 10:50:43', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (61, 'advanceTable', 'AdvancedList', 'pyn', '所属角色', 'roleNames', 0, 0, 0, 1, 150, 'pyn', '2021-03-22 10:50:43', 'pyn', '2021-03-22 10:50:43', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (62, 'advanceTable', 'AdvancedList', 'lixiaoyan', '用户名', 'username', 0, 0, 0, 1, 150, 'lixiaoyan', '2021-03-25 09:57:31', 'lixiaoyan', '2021-03-25 09:57:31', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (63, 'advanceTable', 'AdvancedList', 'lixiaoyan', '用户真实姓名', 'nickname', 0, 0, 0, 1, 150, 'lixiaoyan', '2021-03-25 09:57:31', 'lixiaoyan', '2021-03-25 09:57:31', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (64, 'advanceTable', 'AdvancedList', 'lixiaoyan', '用户手机号', 'phone', 0, 0, 0, 1, 200, 'lixiaoyan', '2021-03-25 09:57:31', 'lixiaoyan', '2021-03-25 09:57:31', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (65, 'advanceTable', 'AdvancedList', 'lixiaoyan', '账号状态', 'enableName', 0, 0, 0, 1, 200, 'lixiaoyan', '2021-03-25 09:57:31', 'lixiaoyan', '2021-03-25 09:57:31', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (66, 'advanceTable', 'AdvancedList', 'lixiaoyan', '用户邮箱', 'email', 0, 0, 0, 1, 100, 'lixiaoyan', '2021-03-25 09:57:31', 'lixiaoyan', '2021-03-25 09:57:31', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (67, 'advanceTable', 'AdvancedList', 'lixiaoyan', '创建时间', 'createTime', 1, 0, 0, 1, 200, 'lixiaoyan', '2021-03-25 09:57:31', 'lixiaoyan', '2021-03-25 09:57:31', 0, 'gu.create_time', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (68, 'advanceTable', 'AdvancedList', 'lixiaoyan', '创建人', 'createBy', 0, 0, 0, 1, 100, 'lixiaoyan', '2021-03-25 09:57:31', 'lixiaoyan', '2021-03-25 09:57:31', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (69, 'advanceTable', 'AdvancedList', 'lixiaoyan', '修改时间', 'updateTime', 1, 0, 0, 1, 200, 'lixiaoyan', '2021-03-25 09:57:31', 'lixiaoyan', '2021-03-25 09:57:31', 0, 'gu.update_time', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (70, 'advanceTable', 'AdvancedList', 'lixiaoyan', '修改人', 'updateBy', 0, 0, 0, 1, 100, 'lixiaoyan', '2021-03-25 09:57:31', 'lixiaoyan', '2021-03-25 09:57:31', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (71, 'advanceTable', 'AdvancedList', 'lixiaoyan', '所属机构', 'orgNames', 0, 0, 0, 1, 200, 'lixiaoyan', '2021-03-25 09:57:31', 'lixiaoyan', '2021-03-25 09:57:31', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (72, 'advanceTable', 'AdvancedList', 'lixiaoyan', '所属角色', 'roleNames', 0, 0, 0, 1, 150, 'lixiaoyan', '2021-03-25 09:57:31', 'lixiaoyan', '2021-03-25 09:57:31', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (73, 'advanceTable', 'AdvancedList', 'lide', '用户名', 'username', 0, 0, 0, 1, 150, 'admin', '2021-04-07 10:07:33', 'pyn', '2021-04-07 10:07:33', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (74, 'advanceTable', 'AdvancedList', 'lide', '用户真实姓名', 'nickname', 0, 0, 0, 1, 150, 'admin', '2021-04-07 10:07:33', 'pyn', '2021-04-07 10:07:33', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (75, 'advanceTable', 'AdvancedList', 'lide', '用户手机号', 'phone', 0, 0, 0, 1, 200, 'admin', '2021-04-07 10:07:33', 'pyn', '2021-04-07 10:07:33', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (76, 'advanceTable', 'AdvancedList', 'lide', '账号状态', 'enableName', 0, 0, 0, 1, 200, 'admin', '2021-04-07 10:07:33', 'admin', '2021-04-07 10:07:33', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (77, 'advanceTable', 'AdvancedList', 'lide', '用户邮箱', 'email', 0, 0, 0, 1, 100, 'admin', '2021-04-07 10:07:33', 'pyn', '2021-04-07 10:07:33', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (78, 'advanceTable', 'AdvancedList', 'lide', '创建时间', 'createTime', 1, 0, 0, 1, 200, 'admin', '2021-04-07 10:07:33', 'pyn', '2021-04-07 10:07:33', 0, 'gu.create_time', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (79, 'advanceTable', 'AdvancedList', 'lide', '创建人', 'createBy', 0, 0, 0, 1, 100, 'admin', '2021-04-07 10:07:33', 'pyn', '2021-04-07 10:07:33', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (80, 'advanceTable', 'AdvancedList', 'lide', '修改时间', 'updateTime', 1, 0, 0, 1, 200, 'admin', '2021-04-07 10:07:33', 'pyn', '2021-04-07 10:07:33', 0, 'gu.update_time', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (81, 'advanceTable', 'AdvancedList', 'lide', '修改人', 'updateBy', 0, 0, 0, 1, 100, 'admin', '2021-04-07 10:07:33', 'pyn', '2021-04-07 10:07:33', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (82, 'advanceTable', 'AdvancedList', 'lide', '所属机构', 'orgNames', 0, 0, 0, 1, 200, 'pyn', '2021-04-07 10:07:33', 'pyn', '2021-04-07 10:07:33', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (83, 'advanceTable', 'AdvancedList', 'lide', '所属角色', 'roleNames', 0, 0, 0, 1, 150, 'pyn', '2021-04-07 10:07:33', 'pyn', '2021-04-07 10:07:33', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (84, 'advanceTable', 'AdvancedList', 'zouya', '用户名', 'username', 0, 0, 0, 1, 150, 'admin', '2021-04-09 16:53:13', 'pyn', '2021-04-09 16:53:13', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (85, 'advanceTable', 'AdvancedList', 'zouya', '用户真实姓名', 'nickname', 0, 0, 0, 1, 150, 'admin', '2021-04-09 16:53:13', 'pyn', '2021-04-09 16:53:13', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (86, 'advanceTable', 'AdvancedList', 'zouya', '用户手机号', 'phone', 0, 0, 0, 1, 200, 'admin', '2021-04-09 16:53:13', 'pyn', '2021-04-09 16:53:13', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (87, 'advanceTable', 'AdvancedList', 'zouya', '账号状态', 'enableName', 0, 0, 0, 1, 200, 'admin', '2021-04-09 16:53:13', 'admin', '2021-04-09 16:53:13', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (88, 'advanceTable', 'AdvancedList', 'zouya', '用户邮箱', 'email', 0, 0, 0, 1, 100, 'admin', '2021-04-09 16:53:13', 'pyn', '2021-04-09 16:53:13', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (89, 'advanceTable', 'AdvancedList', 'zouya', '创建时间', 'createTime', 1, 0, 0, 1, 200, 'admin', '2021-04-09 16:53:13', 'pyn', '2021-04-09 16:53:13', 0, 'gu.create_time', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (90, 'advanceTable', 'AdvancedList', 'zouya', '创建人', 'createBy', 0, 0, 0, 1, 100, 'admin', '2021-04-09 16:53:13', 'pyn', '2021-04-09 16:53:13', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (91, 'advanceTable', 'AdvancedList', 'zouya', '修改时间', 'updateTime', 1, 0, 0, 1, 200, 'admin', '2021-04-09 16:53:13', 'pyn', '2021-04-09 16:53:13', 0, 'gu.update_time', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (92, 'advanceTable', 'AdvancedList', 'zouya', '修改人', 'updateBy', 0, 0, 0, 1, 100, 'admin', '2021-04-09 16:53:13', 'pyn', '2021-04-09 16:53:13', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (93, 'advanceTable', 'AdvancedList', 'zouya', '所属机构', 'orgNames', 0, 0, 0, 1, 200, 'pyn', '2021-04-09 16:53:13', 'pyn', '2021-04-09 16:53:13', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (94, 'advanceTable', 'AdvancedList', 'zouya', '所属角色', 'roleNames', 0, 0, 0, 1, 150, 'pyn', '2021-04-09 16:53:13', 'pyn', '2021-04-09 16:53:13', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (95, 'advanceTable', 'AdvancedList', 'admin', '用户名', 'username', 0, 0, 0, 1, 150, 'admin', '2021-04-25 16:31:56', 'pyn', '2021-04-25 16:31:56', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (96, 'advanceTable', 'AdvancedList', 'admin', '用户真实姓名', 'nickname', 0, 0, 0, 1, 150, 'admin', '2021-04-25 16:31:56', 'pyn', '2021-04-25 16:31:56', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (97, 'advanceTable', 'AdvancedList', 'admin', '用户手机号', 'phone', 0, 0, 0, 1, 200, 'admin', '2021-04-25 16:31:56', 'pyn', '2021-04-25 16:31:56', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (98, 'advanceTable', 'AdvancedList', 'admin', '账号状态', 'enable', 0, 0, 0, 1, 200, 'admin', '2021-04-25 16:31:56', 'admin', '2021-04-25 16:31:56', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (99, 'advanceTable', 'AdvancedList', 'admin', '用户邮箱', 'email', 0, 0, 0, 1, 100, 'admin', '2021-04-25 16:31:56', 'pyn', '2021-04-25 16:31:56', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (100, 'advanceTable', 'AdvancedList', 'admin', '创建时间', 'createTime', 1, 0, 0, 1, 200, 'admin', '2021-04-25 16:31:56', 'pyn', '2021-04-25 16:31:56', 0, 'gu.create_time', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (101, 'advanceTable', 'AdvancedList', 'admin', '创建人', 'createBy', 0, 0, 0, 1, 100, 'admin', '2021-04-25 16:31:56', 'pyn', '2021-04-25 16:31:56', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (102, 'advanceTable', 'AdvancedList', 'admin', '修改时间', 'updateTime', 1, 1, 0, 1, 200, 'admin', '2021-04-25 16:31:56', 'admin', '2021-04-25 16:31:56', 0, 'gu.update_time', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (103, 'advanceTable', 'AdvancedList', 'admin', '修改人', 'updateBy', 0, 0, 0, 1, 100, 'admin', '2021-04-25 16:31:56', 'pyn', '2021-04-25 16:31:56', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (104, 'advanceTable', 'AdvancedList', 'admin', '所属机构', 'orgNames', 0, 0, 0, 1, 200, 'pyn', '2021-04-25 16:31:56', 'pyn', '2021-04-25 16:31:56', 0, '', 1, '', '1');
INSERT INTO `gaea_menu_user_extension` VALUES (105, 'advanceTable', 'AdvancedList', 'admin', '所属角色', 'roleNames', 0, 0, 0, 1, 150, 'pyn', '2021-04-25 16:31:56', 'pyn', '2021-04-25 16:31:56', 0, '', 1, '', '1');

-- ----------------------------
-- Table structure for gaea_org
-- ----------------------------
DROP TABLE IF EXISTS `gaea_org`;
CREATE TABLE `gaea_org`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `org_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '机构代码',
  `org_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '机构名称',
  `org_parent_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上级组织code',
  `org_parent_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上级部门名字',
  `out_org_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '外部机构代码（从外系统同步过来得编码）',
  `out_org_parent_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '外部机构父级编码（从外系统同步过来得父级编码）',
  `org_level` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '机构级别',
  `org_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组织类型',
  `linkman` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系人',
  `mobile_phone` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `telephone` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '固定电话',
  `enabled` int(1) NOT NULL DEFAULT 1 COMMENT '0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG',
  `delete_flag` int(1) NULL DEFAULT 0 COMMENT ' 0--未删除 1--已删除 DIC_NAME=DEL_FLAG',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述信息',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建用户编码',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '修改用户编码',
  `update_time` datetime(0) NOT NULL COMMENT '修改时间',
  `version` tinyint(8) NULL DEFAULT NULL COMMENT '版本',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_code`(`org_code`, `enabled`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 129 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '组织' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gaea_org
-- ----------------------------
INSERT INTO `gaea_org` VALUES (1, '1000001', '总公司', NULL, '无', NULL, NULL, '1', '1', NULL, '', NULL, 1, 0, '', 'admin', '2020-11-25 15:01:16', 'admin', '2020-11-25 15:01:16', NULL);
INSERT INTO `gaea_org` VALUES (2, '2000001', '上海分公司', '1000001', '总公司', NULL, NULL, '2', '2', NULL, NULL, NULL, 1, 0, NULL, 'admin', '2020-11-25 15:01:16', 'admin', '2020-11-25 15:01:16', NULL);
INSERT INTO `gaea_org` VALUES (3, '2000002', '武汉分公司', '1000001', '总公司', NULL, NULL, '2', '2', NULL, NULL, NULL, 1, 0, NULL, 'admin', '2020-11-25 15:01:16', 'admin', '2020-11-25 15:01:16', NULL);
INSERT INTO `gaea_org` VALUES (4, '2000003', '北京分公司', '1000001', '总公司', NULL, NULL, '2', '2', NULL, NULL, NULL, 1, 0, NULL, 'admin', '2020-11-25 15:01:16', 'admin', '2020-11-25 15:01:16', NULL);
INSERT INTO `gaea_org` VALUES (101, '3000001', '上海事业部', '2000001', '上海分公司', NULL, NULL, '3', '3', NULL, NULL, NULL, 1, 0, NULL, 'admin', '2020-11-25 15:01:16', 'admin', '2020-11-25 15:01:16', NULL);
INSERT INTO `gaea_org` VALUES (102, '3000002', '武汉事业部1', '2000002', '武汉分公司', NULL, NULL, '', '3', NULL, NULL, NULL, 1, 0, NULL, '管理员', '2020-11-25 15:01:16', 'zouya', '2021-03-18 10:52:27', NULL);
INSERT INTO `gaea_org` VALUES (103, '3000003', '北京事业部1', '2000003', '北京分公司', NULL, NULL, '3', '3', NULL, NULL, NULL, 1, 0, NULL, '管理员', '2020-11-25 15:01:16', 'zouya', '2021-03-17 15:59:09', NULL);
INSERT INTO `gaea_org` VALUES (104, '3000004', '上海IT部1', '2000001', '上海分公司', NULL, NULL, '3', '3', NULL, NULL, NULL, 1, 0, NULL, '管理员', '2020-11-25 15:01:16', 'zouya', '2021-03-22 16:47:52', NULL);
INSERT INTO `gaea_org` VALUES (123, '4000002', '上海IT部2', '2000001', '上海分公司', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'zouya', '2021-03-19 10:06:43', 'zouya', '2021-03-23 10:18:55', 2);
INSERT INTO `gaea_org` VALUES (124, '5000001', '上海IT部3', '2000001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 'zouya', '2021-03-22 14:10:19', 'zouya', '2021-03-22 14:12:05', 2);
INSERT INTO `gaea_org` VALUES (125, '6000001', '上海IT部4', '5000001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 'zouya', '2021-03-22 14:19:25', 'zouya', '2021-03-22 14:19:25', 1);

-- ----------------------------
-- Table structure for gaea_query_condition
-- ----------------------------
DROP TABLE IF EXISTS `gaea_query_condition`;
CREATE TABLE `gaea_query_condition`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `table_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格code',
  `menu_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单ID',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '条件名称-key',
  `name_value` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '条件名称-value',
  `value_type` int(11) NULL DEFAULT 1 COMMENT '条件值类型(1:字符串,2:数字,3:日期)',
  `query_condition` int(11) NULL DEFAULT 1 COMMENT '查询条件(1:>、2:<、3:>=、4:<=、5:LIKE)(暂时不使用此字段)',
  `type` int(11) NULL DEFAULT 1 COMMENT '条件类型(1:文本框、2:下拉框、3:联想控件、4:日期控件、5:数字、6:多记录文本)(1:文本框、2:下拉框、3:日期控件、4:多记录文本、5:数字、6:联想控件)（此处只取前四种类型，下拉框和联想控件合并，数字类型归为文本）',
  `place` int(11) NULL DEFAULT 1 COMMENT '排列序号',
  `data_source` int(11) NULL DEFAULT 2 COMMENT '数据源(1:接口、2:固定内容)(联想控件接口统一格式,code,name)',
  `data_source_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据源值',
  `is_disabled` int(11) NULL DEFAULT 0 COMMENT '0:可用,1:已作废',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
  `create_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人ID',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `date_precision` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日期精度',
  `version` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_gqc_01`(`table_code`, `menu_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gaea_query_condition
-- ----------------------------
INSERT INTO `gaea_query_condition` VALUES (15, 'advanceTable', 'AdvancedList', 'gu.username', '用户登录名', 1, 1, 1, 1, 2, '', 0, 'admin', '2021-03-15 17:57:38', 'pyn', '2021-03-15 17:57:38', '', 3);
INSERT INTO `gaea_query_condition` VALUES (16, 'advanceTable', 'AdvancedList', 'date_format(gu.create_time,\'%Y-%m-%d\')', '创建时间', 1, 1, 3, 1, 2, '', 0, 'admin', '2021-03-15 17:57:38', 'pyn', '2021-03-15 17:57:38', 'yyyy-MM-dd', 2);
INSERT INTO `gaea_query_condition` VALUES (17, 'advanceTable', 'AdvancedList', 'gu.enabled', '账号状态', 1, 1, 2, 1, 1, '/business/gaeaDict/select/ENABLE_FLAG', 0, 'admin', '2021-03-15 17:57:38', 'pyn', '2021-03-15 17:57:38', NULL, 2);
INSERT INTO `gaea_query_condition` VALUES (18, 'advanceTable', 'AdvancedList', 'org.org_code', '所属机构', 1, 1, 2, 1, 1, '/auth/org/orgSelect', 0, 'pyn', '2021-03-15 17:57:38', 'pyn', '2021-03-15 17:57:38', NULL, 1);
INSERT INTO `gaea_query_condition` VALUES (19, 'advanceTable', 'AdvancedList', 'gu.phone', '用户手机号', 1, 1, 1, 1, 2, '', 0, 'pyn', '2021-03-15 17:57:38', 'pyn', '2021-03-15 17:57:38', '', 1);

-- ----------------------------
-- Table structure for gaea_query_user_condition
-- ----------------------------
DROP TABLE IF EXISTS `gaea_query_user_condition`;
CREATE TABLE `gaea_query_user_condition`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `menu_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单ID',
  `comm_sql` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '查询sql',
  `label` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询条件名称label',
  `is_disabled` int(11) NULL DEFAULT 0 COMMENT '0:可用,1:已作废',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
  `create_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人ID',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `table_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表code',
  `search_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '常用查询名称',
  `type` int(11) NULL DEFAULT 1 COMMENT '条件类型(1:文本框、2:下拉框、3:联想控件、4:日期控件、5:数字、6:多记录文本)(1:文本框、2:下拉框、3:日期控件、4:多记录文本、5:数字、6:联想控件)（此处只取前四种类型，下拉框和联想控件合并，数字类型归为文本）',
  `data_source` int(11) NULL DEFAULT 2 COMMENT '数据源(1:接口、2:固定内容)(联想控件接口统一格式,code,name)',
  `data_source_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据源值',
  `version` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_gcc_01`(`menu_code`, `create_by`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 48 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gaea_query_user_condition
-- ----------------------------
INSERT INTO `gaea_query_user_condition` VALUES (23, 'AdvancedList', '[{\"dynamicQueryOperatorType\":\"NE\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"org.org_code\",\"operator\":\"NE\",\"value\":\"3000002\",\"valueType\":1}]', '[\"武汉事业部1\"]', 0, 'admin', '2021-03-22 09:56:41', 'admin', '2021-03-22 09:56:41', 'advanceTable', '机构非武汉事业部1', 1, 2, NULL, 1);
INSERT INTO `gaea_query_user_condition` VALUES (24, 'AdvancedList', '[{\"dynamicQueryOperatorType\":\"EQ\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"org.org_code\",\"operator\":\"EQ\",\"value\":\"2000002\",\"valueType\":1}]', '[\"武汉分公司\"]', 0, 'admin', '2021-03-22 09:57:05', 'admin', '2021-03-22 09:57:05', 'advanceTable', '机构为武汉事业部', 1, 2, NULL, 1);
INSERT INTO `gaea_query_user_condition` VALUES (29, 'AdvancedList', '[{\"datePrecision\":\"\",\"dynamicQueryOperatorType\":\"IN\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"gu.username\",\"operator\":\"IN\",\"value\":\"aimee\",\"valueType\":1}]', '[\"aimee\"]', 0, 'admin', '2021-03-22 10:13:28', 'admin', '2021-03-22 10:13:28', 'advanceTable', '登录名', 1, 2, NULL, 1);
INSERT INTO `gaea_query_user_condition` VALUES (30, 'AdvancedList', '[{\"datePrecision\":\"\",\"dynamicQueryOperatorType\":\"LIKE\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"gu.username\",\"operator\":\"LIKE\",\"value\":\"aimee\",\"valueType\":1}]', '[\"aimee\"]', 0, 'admin', '2021-03-22 10:14:44', 'admin', '2021-03-22 10:14:44', 'advanceTable', '登录名类似', 1, 2, NULL, 1);
INSERT INTO `gaea_query_user_condition` VALUES (31, 'AdvancedList', '[{\"datePrecision\":\"\",\"dynamicQueryOperatorType\":\"LIKE\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"gu.phone\",\"operator\":\"LIKE\",\"value\":\"13816966003\",\"valueType\":1}]', '[\"13816966003\"]', 0, 'admin', '2021-03-22 10:15:48', 'admin', '2021-03-22 10:15:48', 'advanceTable', '手机号', 1, 2, NULL, 1);
INSERT INTO `gaea_query_user_condition` VALUES (33, 'AdvancedList', '[{\"datePrecision\":\"\",\"dynamicQueryOperatorType\":\"LIKE\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"gu.phone\",\"operator\":\"LIKE\",\"value\":\"13816966003\",\"valueType\":1},{\"datePrecision\":\"\",\"dynamicQueryOperatorType\":\"IN\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"gu.username\",\"operator\":\"IN\",\"value\":\"aimee\",\"valueType\":1}]', '[\"13816966003\",\"aimee\"]', 0, 'admin', '2021-03-22 10:18:24', 'admin', '2021-03-22 10:18:24', 'advanceTable', '登录名和手机号', 1, 2, NULL, 1);
INSERT INTO `gaea_query_user_condition` VALUES (36, 'AdvancedList', '[{\"datePrecision\":\"\",\"dynamicQueryOperatorType\":\"EQ\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"gu.username\",\"operator\":\"EQ\",\"value\":\"pyn\",\"valueType\":1}]', '[\"pyn\"]', 0, 'admin', '2021-03-22 13:08:24', 'admin', '2021-03-22 13:08:24', 'advanceTable', 'pyn', 1, 2, NULL, 1);
INSERT INTO `gaea_query_user_condition` VALUES (37, 'AdvancedList', '[{\"datePrecision\":\"\",\"dynamicQueryOperatorType\":\"EQ\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"gu.username\",\"operator\":\"EQ\",\"value\":\"pyn\",\"valueType\":1}]', '[\"pyn\"]', 0, 'pyn', '2021-03-22 13:09:24', 'pyn', '2021-03-22 13:09:24', 'advanceTable', 'pyn', 1, 2, NULL, 1);
INSERT INTO `gaea_query_user_condition` VALUES (38, 'AdvancedList', '[{\"datePrecision\":\"\",\"dynamicQueryOperatorType\":\"EQ\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"gu.username\",\"operator\":\"EQ\",\"value\":\"admin\",\"valueType\":1}]', '[\"admin\"]', 0, 'pyn', '2021-03-22 13:10:55', 'pyn', '2021-03-22 13:10:55', 'advanceTable', 'admin', 1, 2, NULL, 1);
INSERT INTO `gaea_query_user_condition` VALUES (39, 'AdvancedList', '[{\"datePrecision\":\"\",\"dynamicQueryOperatorType\":\"EQ\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"gu.username\",\"operator\":\"EQ\",\"value\":\"123124\",\"valueType\":1}]', '[\"123124\"]', 0, 'admin', '2021-04-22 14:29:32', 'admin', '2021-04-22 14:29:32', 'advanceTable', '12421412', 1, 2, NULL, 1);
INSERT INTO `gaea_query_user_condition` VALUES (40, 'advancedList', '[]', '[]', 0, 'admin', '2021-04-25 13:45:41', 'admin', '2021-04-25 13:45:41', 'advanceTable', 'asdasd', 1, 2, NULL, 1);
INSERT INTO `gaea_query_user_condition` VALUES (41, 'advancedList', '[{\"datePrecision\":\"yyyy-MM-dd\",\"dynamicQueryOperatorType\":\"EQ\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"date_format(gu.create_time,\'%Y-%m-%d\')\",\"operator\":\"EQ\",\"value\":\"2021-04-13\",\"valueType\":1}]', '[\"2021-04-13\"]', 0, 'admin', '2021-04-25 13:51:42', 'admin', '2021-04-25 13:51:42', 'advanceTable', '0413日创建', 1, 2, NULL, 1);
INSERT INTO `gaea_query_user_condition` VALUES (42, 'AdvancedList', '[{\"datePrecision\":\"yyyy-MM-dd\",\"dynamicQueryOperatorType\":\"EQ\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"date_format(gu.create_time,\'%Y-%m-%d\')\",\"operator\":\"EQ\",\"type\":3,\"value\":\"2021-04-08\",\"valueType\":1}]', '[\"2021-04-08\"]', 0, 'admin', '2021-04-25 17:46:46', 'admin', '2021-04-25 17:46:46', 'advanceTable', '测试', 1, 2, NULL, 1);
INSERT INTO `gaea_query_user_condition` VALUES (43, 'AdvancedList', '[{\"dataSource\":1,\"dataSourceValue\":\"/business/gaeaDict/select/ENABLE_FLAG\",\"dynamicQueryOperatorType\":\"EQ\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"gu.enabled\",\"operator\":\"EQ\",\"type\":2,\"value\":\"0\",\"valueType\":1}]', '[\"禁用\"]', 0, 'admin', '2021-04-25 17:47:48', 'admin', '2021-04-25 17:47:48', 'advanceTable', 'test0001', 1, 2, NULL, 1);
INSERT INTO `gaea_query_user_condition` VALUES (44, 'AdvancedList', '[{\"dataSource\":1,\"dataSourceValue\":\"/business/gaeaDict/select/ENABLE_FLAG\",\"dynamicQueryOperatorType\":\"EQ\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"gu.enabled\",\"operator\":\"EQ\",\"type\":2,\"value\":\"1\",\"valueType\":1}]', '[null]', 0, 'admin', '2021-04-25 17:50:44', 'admin', '2021-04-25 17:50:44', 'advanceTable', 'test0002', 1, 2, NULL, 1);
INSERT INTO `gaea_query_user_condition` VALUES (45, 'AdvancedList', '[{\"dataSource\":1,\"dataSourceValue\":\"/auth/org/orgSelect\",\"dynamicQueryOperatorType\":\"EQ\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"org.org_code\",\"operator\":\"EQ\",\"type\":2,\"value\":\"1000001\",\"valueType\":1}]', '[null]', 0, 'admin', '2021-04-25 18:16:30', 'admin', '2021-04-25 18:16:30', 'advanceTable', '所属机构001', 1, 2, NULL, 1);
INSERT INTO `gaea_query_user_condition` VALUES (46, 'AdvancedList', '[{\"dataSource\":1,\"dataSourceValue\":\"/business/gaeaDict/select/ENABLE_FLAG\",\"dynamicQueryOperatorType\":\"EQ\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"gu.enabled\",\"operator\":\"EQ\",\"type\":2,\"value\":\"0\",\"valueType\":1},{\"dataSource\":1,\"dataSourceValue\":\"/auth/org/orgSelect\",\"dynamicQueryOperatorType\":\"EQ\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"org.org_code\",\"operator\":\"EQ\",\"type\":2,\"value\":\"2000001\",\"valueType\":1},{\"dataSource\":2,\"dataSourceValue\":\"\",\"datePrecision\":\"\",\"dynamicQueryOperatorType\":\"EQ\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"gu.username\",\"operator\":\"EQ\",\"type\":1,\"value\":\"test\",\"valueType\":1}]', '[null,null,\"test\"]', 0, 'admin', '2021-04-25 18:27:33', 'admin', '2021-04-25 18:27:33', 'advanceTable', 'test0003', 1, 2, NULL, 1);
INSERT INTO `gaea_query_user_condition` VALUES (47, 'AdvancedList', '[{\"dataSource\":1,\"dataSourceValue\":\"/auth/org/orgSelect\",\"dynamicQueryOperatorType\":\"NE\",\"dynamicQueryValueType\":\"CHARACTER\",\"name\":\"org.org_code\",\"operator\":\"NE\",\"type\":2,\"value\":\"2000001\",\"valueType\":1}]', '[null]', 0, 'admin', '2021-04-26 14:25:56', 'admin', '2021-04-26 14:25:56', 'advanceTable', '131231', 1, 2, NULL, 1);

-- ----------------------------
-- Table structure for gaea_role
-- ----------------------------
DROP TABLE IF EXISTS `gaea_role`;
CREATE TABLE `gaea_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `org_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组织编号',
  `role_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色编码',
  `role_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `enabled` tinyint(1) NULL DEFAULT NULL COMMENT '1：可用 0：禁用',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `version` tinyint(8) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 70 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gaea_role
-- ----------------------------
INSERT INTO `gaea_role` VALUES (25, '3000004', 'admin', '管理员', 1, '管理员', '管理员', '2021-03-12 23:00:19', 'admin', '2021-05-13 10:36:30', 7);

-- ----------------------------
-- Table structure for gaea_role_menu_authority
-- ----------------------------
DROP TABLE IF EXISTS `gaea_role_menu_authority`;
CREATE TABLE `gaea_role_menu_authority`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `org_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '机构编码',
  `role_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `menu_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `auth_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限编码',
  `auth_path` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `version` int(8) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14530 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色与权限对应关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gaea_role_menu_authority
-- ----------------------------
INSERT INTO `gaea_role_menu_authority` VALUES (14359, '3000004', 'admin', 'Organization', 'gaeaOrgController#queryAllOrg', 'GET#/org/queryAllOrg', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14360, '3000004', 'admin', 'Organization', 'gaeaOrgController#orgRoleTree', 'GET#/org/user/role/tree/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14361, '3000004', 'admin', 'Organization', 'gaeaOrgController#orgSelect', 'GET#/org/orgSelect', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14362, '3000004', 'admin', 'Organization', 'gaeaOrgController#orgTree', 'GET#/org/tree', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14363, '3000004', 'admin', 'Organization', 'gaeaOrgController#saveOrg', 'POST#/org/saveOrg', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14364, '3000004', 'admin', 'Organization', 'gaeaOrgController#updateOrg', 'POST#/org/updateOrg', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14365, '3000004', 'admin', 'Organization', 'gaeaOrgController#orgRoleTreeSelected', 'GET#/org/role/tree', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14366, '3000004', 'admin', 'Organization', 'gaeaOrgController#update', 'PUT#/org', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14367, '3000004', 'admin', 'Organization', 'gaeaOrgController#insert', 'POST#/org', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14368, '3000004', 'admin', 'Organization', 'gaeaOrgController#detail', 'GET#/org/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14369, '3000004', 'admin', 'Organization', 'gaeaOrgController#deleteBatchIds', 'POST#/org/delete/batch', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14370, '3000004', 'admin', 'Organization', 'gaeaOrgController#deleteById', 'DELETE#/org/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14371, '3000004', 'admin', 'Organization', 'gaeaOrgController#pageList', 'GET#/org/pageList', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14372, '3000004', 'admin', 'Authority', 'gaeaAuthorityController#authorityTree', 'GET#/gaeaAuthority/authority/tree/**/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14373, '3000004', 'admin', 'Authority', 'gaeaAuthorityController#roleAuthority', 'POST#/gaeaAuthority/role/authority', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14374, '3000004', 'admin', 'Authority', 'gaeaAuthorityController#update', 'PUT#/gaeaAuthority', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14375, '3000004', 'admin', 'Permission', 'gaeaAuthorityController#roleAuthority', 'POST#/gaeaAuthority/role/authority', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14376, '3000004', 'admin', 'Permission', 'gaeaAuthorityController#update', 'PUT#/gaeaAuthority', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14377, '3000004', 'admin', 'MenuConfig', 'gaeaMenuController#userInfo', 'GET#/menu/userInfo', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14378, '3000004', 'admin', 'MenuConfig', 'gaeaMenuController#getTree', 'GET#/menu/tree', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14379, '3000004', 'admin', 'MenuConfig', 'gaeaMenuController#getMenuInfoByOrg', 'POST#/menu/menuUserInfoByOrg', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14380, '3000004', 'admin', 'MenuConfig', 'gaeaMenuController#menuAuthority', 'POST#/menu/mapper/authorities', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14381, '3000004', 'admin', 'MenuConfig', 'gaeaMenuController#update', 'PUT#/menu', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14382, '3000004', 'admin', 'MenuConfig', 'gaeaMenuController#insert', 'POST#/menu', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14383, '3000004', 'admin', 'MenuConfig', 'gaeaMenuController#detail', 'GET#/menu/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14384, '3000004', 'admin', 'MenuConfig', 'gaeaMenuController#deleteBatchIds', 'POST#/menu/delete/batch', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14385, '3000004', 'admin', 'MenuConfig', 'gaeaMenuController#deleteById', 'DELETE#/menu/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14386, '3000004', 'admin', 'MenuConfig', 'gaeaMenuController#pageList', 'GET#/menu/pageList', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14387, '3000004', 'admin', 'MenuConfig', 'gaeaMenuController#authorityTree', 'GET#/menu/authority/tree/**/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14388, '3000004', 'admin', 'MenuDetail', 'gaeaMenuExtensionController#queryMenuExtension', 'GET#/menuextension/queryMenuExtension/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14389, '3000004', 'admin', 'Role', 'gaeaRoleController#update', 'PUT#/role', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14390, '3000004', 'admin', 'Role', 'gaeaRoleController#insert', 'POST#/role', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14391, '3000004', 'admin', 'Role', 'gaeaRoleController#detail', 'GET#/role/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14392, '3000004', 'admin', 'Role', 'gaeaRoleController#deleteBatchIds', 'POST#/role/delete/batch', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14393, '3000004', 'admin', 'Role', 'gaeaRoleController#deleteById', 'DELETE#/role/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14394, '3000004', 'admin', 'Role', 'gaeaRoleController#pageList', 'GET#/role/pageList', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14395, '3000004', 'admin', 'Role', 'gaeaRoleController#roleAuthorities', 'GET#/role/tree/authorities/**/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14396, '3000004', 'admin', 'Role', 'gaeaRoleController#saveOrgTreeForRole', 'POST#/role/mapping/org', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14397, '3000004', 'admin', 'Role', 'gaeaRoleController#queryOrgTreeForRole', 'GET#/role/org/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14398, '3000004', 'admin', 'Role', 'gaeaRoleController#saveMenuActionTreeForRole', 'POST#/role/roleMenuAuthorities', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14399, '3000004', 'admin', 'User', 'gaeaUserController#queryRoleTree', 'GET#/user/queryRoleTree/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14400, '3000004', 'admin', 'User', 'gaeaUserController#resetPassword', 'POST#/user/resetPwd', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14401, '3000004', 'admin', 'User', 'gaeaUserController#saveRoleTree', 'POST#/user/saveRoleTree', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14402, '3000004', 'admin', 'User', 'gaeaUserController#refreshCache', 'POST#/user/refresh/username', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14403, '3000004', 'admin', 'User', 'gaeaUserController#updatePassword', 'POST#/user/updatePassword', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14404, '3000004', 'admin', 'User', 'gaeaUserController#update', 'PUT#/user', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14405, '3000004', 'admin', 'User', 'gaeaUserController#insert', 'POST#/user', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14406, '3000004', 'admin', 'User', 'gaeaUserController#detail', 'GET#/user/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14407, '3000004', 'admin', 'User', 'gaeaUserController#deleteBatchIds', 'POST#/user/delete/batch', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14408, '3000004', 'admin', 'User', 'gaeaUserController#deleteById', 'DELETE#/user/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14409, '3000004', 'admin', 'User', 'gaeaUserController#pageList', 'GET#/user/pageList', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14410, '3000004', 'admin', 'User', 'gaeaUserController#unLock', 'POST#/user/unLock', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14411, '3000004', 'admin', 'Rules', 'gaeaRulesController#executeRules', 'POST#/gaeaRules/executeRules', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14412, '3000004', 'admin', 'Rules', 'gaeaRulesController#update', 'PUT#/gaeaRules', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14413, '3000004', 'admin', 'Rules', 'gaeaRulesController#insert', 'POST#/gaeaRules', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14414, '3000004', 'admin', 'Rules', 'gaeaRulesController#detail', 'GET#/gaeaRules/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14415, '3000004', 'admin', 'Rules', 'gaeaRulesController#pageList', 'GET#/gaeaRules/pageList', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14416, '3000004', 'admin', 'Rules', 'gaeaRulesController#deleteById', 'DELETE#/gaeaRules/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14417, '3000004', 'admin', 'Rules', 'gaeaRulesController#deleteBatchIds', 'POST#/gaeaRules/delete/batch', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14418, '3000004', 'admin', 'RulesDetail', 'RulesDetail', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14419, '3000004', 'admin', 'RulesAdd', 'RulesAdd', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14420, '3000004', 'admin', 'DataDictionary', 'gaeaDictController#select', 'GET#/gaeaDict/select/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14421, '3000004', 'admin', 'DataDictionary', 'gaeaDictController#detail', 'GET#/gaeaDict/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14422, '3000004', 'admin', 'DataDictionary', 'gaeaDictController#pageList', 'GET#/gaeaDict/pageList', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14423, '3000004', 'admin', 'DataDictionary', 'gaeaDictController#deleteBatchIds', 'POST#/gaeaDict/delete/batch', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14424, '3000004', 'admin', 'DataDictionary', 'gaeaDictController#deleteById', 'DELETE#/gaeaDict/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14425, '3000004', 'admin', 'DataDictionary', 'gaeaDictController#update', 'PUT#/gaeaDict', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14426, '3000004', 'admin', 'DataDictionary', 'gaeaDictController#insert', 'POST#/gaeaDict', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14427, '3000004', 'admin', 'DataDictionary', 'gaeaDictItemController#detail', 'GET#/gaeaDictItem/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14428, '3000004', 'admin', 'DataDictionary', 'gaeaDictItemController#pageList', 'GET#/gaeaDictItem/pageList', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14429, '3000004', 'admin', 'DataDictionary', 'gaeaDictItemController#deleteBatchIds', 'POST#/gaeaDictItem/delete/batch', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14430, '3000004', 'admin', 'DataDictionary', 'gaeaDictItemController#deleteById', 'DELETE#/gaeaDictItem/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14431, '3000004', 'admin', 'DataDictionary', 'gaeaDictItemController#update', 'PUT#/gaeaDictItem', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14432, '3000004', 'admin', 'DataDictionary', 'gaeaDictItemController#insert', 'POST#/gaeaDictItem', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14433, '3000004', 'admin', 'Parameter', 'gaeaSettingController#update', 'PUT#/setting', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14434, '3000004', 'admin', 'Parameter', 'gaeaSettingController#insert', 'POST#/setting', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14435, '3000004', 'admin', 'Parameter', 'gaeaSettingController#detail', 'GET#/setting/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14436, '3000004', 'admin', 'Parameter', 'gaeaSettingController#deleteBatchIds', 'POST#/setting/delete/batch', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14437, '3000004', 'admin', 'Parameter', 'gaeaSettingController#deleteById', 'DELETE#/setting/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14438, '3000004', 'admin', 'Parameter', 'gaeaSettingController#pageList', 'GET#/setting/pageList', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14439, '3000004', 'admin', 'ParameterEdit', 'ParameterEdit', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14440, '3000004', 'admin', 'ParameterAdd', 'ParameterAdd', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14441, '3000004', 'admin', 'Support', 'gaeaHelpController#listAll', 'GET#/gaeaHelp/list', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14442, '3000004', 'admin', 'Support', 'gaeaHelpController#demo', 'GET#/gaeaHelp/demo', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14443, '3000004', 'admin', 'Support', 'gaeaHelpController#update', 'PUT#/gaeaHelp', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14444, '3000004', 'admin', 'Support', 'gaeaHelpController#insert', 'POST#/gaeaHelp', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14445, '3000004', 'admin', 'Support', 'gaeaHelpController#detail', 'GET#/gaeaHelp/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14446, '3000004', 'admin', 'Support', 'gaeaHelpController#deleteBatchIds', 'POST#/gaeaHelp/delete/batch', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14447, '3000004', 'admin', 'Support', 'gaeaHelpController#deleteById', 'DELETE#/gaeaHelp/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14448, '3000004', 'admin', 'Support', 'gaeaHelpController#pageList', 'GET#/gaeaHelp/pageList', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14449, '3000004', 'admin', 'OperationLog', 'gaeaLogController#update', 'PUT#/log', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14450, '3000004', 'admin', 'OperationLog', 'gaeaLogController#insert', 'POST#/log', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14451, '3000004', 'admin', 'OperationLog', 'gaeaLogController#detail', 'GET#/log/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14452, '3000004', 'admin', 'OperationLog', 'gaeaLogController#deleteBatchIds', 'POST#/log/delete/batch', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14453, '3000004', 'admin', 'OperationLog', 'gaeaLogController#deleteById', 'DELETE#/log/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14454, '3000004', 'admin', 'OperationLog', 'gaeaLogController#pageList', 'GET#/log/pageList', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14455, '3000004', 'admin', 'OperationLog', 'gaeaLogController#exportLogToFile', 'POST#/log/exportLogToFile', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14456, '3000004', 'admin', 'DictItem', 'DictItem', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14457, '3000004', 'admin', 'GaeaUiI18n', 'gaeaUiI18nController#detail', 'GET#/gaeaUiI18n/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14458, '3000004', 'admin', 'GaeaUiI18n', 'gaeaUiI18nController#update', 'PUT#/gaeaUiI18n', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14459, '3000004', 'admin', 'GaeaUiI18n', 'gaeaUiI18nController#insert', 'POST#/gaeaUiI18n', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14460, '3000004', 'admin', 'GaeaUiI18n', 'gaeaUiI18nController#pageList', 'GET#/gaeaUiI18n/pageList', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14461, '3000004', 'admin', 'GaeaUiI18n', 'gaeaUiI18nController#deleteById', 'DELETE#/gaeaUiI18n/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14462, '3000004', 'admin', 'GaeaUiI18n', 'gaeaUiI18nController#deleteBatchIds', 'POST#/gaeaUiI18n/delete/batch', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14463, '3000004', 'admin', 'Announcement', 'Announcement', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14464, '3000004', 'admin', 'MyMassageDetails', 'MyMassageDetails', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14465, '3000004', 'admin', 'FileManagement', 'gaeaFileController#upload', 'POST#/file/upload', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14466, '3000004', 'admin', 'FileManagement', 'gaeaFileController#export', 'GET#/file/download/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14467, '3000004', 'admin', 'FileManagement', 'gaeaFileController#deleteById', 'DELETE#/file/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14468, '3000004', 'admin', 'FileManagement', 'gaeaFileController#detail', 'GET#/file/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14469, '3000004', 'admin', 'FileManagement', 'gaeaFileController#pageList', 'GET#/file/pageList', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14470, '3000004', 'admin', 'FileManagement', 'gaeaFileController#deleteBatchIds', 'POST#/file/delete/batch', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14471, '3000004', 'admin', 'FileManagement', 'gaeaFileController#update', 'PUT#/file', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14472, '3000004', 'admin', 'FileManagement', 'gaeaFileController#insert', 'POST#/file', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14473, '3000004', 'admin', 'Situation', 'Situation', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14474, '3000004', 'admin', 'Template', 'gaeaPushTemplateController#testSendPush', 'POST#/gaeaPushTemplate/testSendPush', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14475, '3000004', 'admin', 'Template', 'gaeaPushTemplateController#preview', 'POST#/gaeaPushTemplate/preview', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14476, '3000004', 'admin', 'Template', 'gaeaPushTemplateController#update', 'PUT#/gaeaPushTemplate', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14477, '3000004', 'admin', 'Template', 'gaeaPushTemplateController#insert', 'POST#/gaeaPushTemplate', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14478, '3000004', 'admin', 'Template', 'gaeaPushTemplateController#detail', 'GET#/gaeaPushTemplate/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14479, '3000004', 'admin', 'Template', 'gaeaPushTemplateController#deleteBatchIds', 'POST#/gaeaPushTemplate/delete/batch', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14480, '3000004', 'admin', 'Template', 'gaeaPushTemplateController#deleteById', 'DELETE#/gaeaPushTemplate/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14481, '3000004', 'admin', 'Template', 'gaeaPushTemplateController#pageList', 'GET#/gaeaPushTemplate/pageList', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14482, '3000004', 'admin', 'History', 'gaeaPushHistoryController#getPushStatistics', 'POST#/gaeaPushHistory/getPushStatistics', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14483, '3000004', 'admin', 'History', 'gaeaPushHistoryController#update', 'PUT#/gaeaPushHistory', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14484, '3000004', 'admin', 'History', 'gaeaPushHistoryController#insert', 'POST#/gaeaPushHistory', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14485, '3000004', 'admin', 'History', 'gaeaPushHistoryController#detail', 'GET#/gaeaPushHistory/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14486, '3000004', 'admin', 'History', 'gaeaPushHistoryController#deleteBatchIds', 'POST#/gaeaPushHistory/delete/batch', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14487, '3000004', 'admin', 'History', 'gaeaPushHistoryController#deleteById', 'DELETE#/gaeaPushHistory/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14488, '3000004', 'admin', 'History', 'gaeaPushHistoryController#pageList', 'GET#/gaeaPushHistory/pageList', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14489, '3000004', 'admin', 'SvgDemo', 'SvgDemo', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14490, '3000004', 'admin', 'AdvancedList', 'AdvancedList', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14491, '3000004', 'admin', 'ProcessForm', 'ProcessForm', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14492, '3000004', 'admin', 'ProcessModel', 'ProcessModel', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14493, '3000004', 'admin', 'ProcessDef', 'ProcessDef', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14494, '3000004', 'admin', 'ProcessRunning', 'ProcessRunning', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14495, '3000004', 'admin', 'ProcessHistory', 'ProcessHistory', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14496, '3000004', 'admin', 'FlowableTaskHandler', 'FlowableTaskHandler', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14497, '3000004', 'admin', 'FlowableHisDetail', 'FlowableHisDetail', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14498, '3000004', 'admin', 'Datasource', 'Datasource', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14499, '3000004', 'admin', 'Resultset', 'Resultset', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14500, '3000004', 'admin', 'Report', 'Report', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14501, '3000004', 'admin', 'Bigscreen', 'Bigscreen', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14502, '3000004', 'admin', 'Excelreport', 'Excelreport', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14503, '3000004', 'admin', 'DeviceInfoDetail', 'DeviceInfoDetail', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14504, '3000004', 'admin', 'DeviceLogDetailInfo', 'DeviceLogDetailInfo', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14505, '3000004', 'admin', 'Project', 'Project', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14506, '3000004', 'admin', 'Index', 'Index', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14507, '3000004', 'admin', 'Config', 'Config', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14508, '3000004', 'admin', 'Init', 'Init', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14509, '3000004', 'admin', 'Preview', 'Preview', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14510, '3000004', 'admin', 'AlipayConfig', 'AlipayConfig', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14511, '3000004', 'admin', 'AlipayConfigDetail', 'AlipayConfigDetail', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14512, '3000004', 'admin', 'DeviceInfo', 'DeviceInfo', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14513, '3000004', 'admin', 'DeviceModel', 'DeviceModel', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14514, '3000004', 'admin', 'DeviceLogDetail', 'DeviceLogDetail', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14515, '3000004', 'admin', 'DeviceModelDetail', 'DeviceModelDetail', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14516, '3000004', 'admin', 'INF_MYAPP', 'INF_MYAPP', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14517, '3000004', 'admin', 'INF_MGT', 'INF_MGT', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14518, '3000004', 'admin', 'interfaceEdit', 'interfaceEdit', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14519, '3000004', 'admin', 'API', 'API', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14520, '3000004', 'admin', 'OpenApiGuide', 'OpenApiGuide', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14521, '3000004', 'admin', 'OpenApiList', 'OpenApiList', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14522, '3000004', 'admin', 'OpenApiDetail', 'OpenApiDetail', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14523, '3000004', 'admin', 'OpenApiAccess', 'OpenApiAccess', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14524, '3000004', 'admin', 'OpenApiParam', 'OpenApiParam', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14525, '3000004', 'admin', 'OpenApiSign', 'OpenApiSign', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14526, '3000004', 'admin', 'OpenApiCode', 'OpenApiCode', NULL, 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14527, '3000004', 'admin', 'Download', 'gaeaExportController#export', 'POST#/export/saveExportLog', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14528, '3000004', 'admin', 'Download', 'gaeaExportController#detail', 'GET#/export/**', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);
INSERT INTO `gaea_role_menu_authority` VALUES (14529, '3000004', 'admin', 'Download', 'gaeaExportController#pageList', 'GET#/export/pageList', 'admin', '2021-05-19 12:42:04', 'admin', '2021-05-19 12:42:04', 1);

-- ----------------------------
-- Table structure for gaea_role_org
-- ----------------------------
DROP TABLE IF EXISTS `gaea_role_org`;
CREATE TABLE `gaea_role_org`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'roleid',
  `org_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '组织id',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` timestamp(0) NULL DEFAULT NULL,
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `version` tinyint(8) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1250 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色与组织' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gaea_role_org
-- ----------------------------
INSERT INTO `gaea_role_org` VALUES (1238, 'admin', '3000004', 'admin', '2021-02-07 16:36:30', 'admin', '2021-02-07 16:36:30', 1);

-- ----------------------------
-- Table structure for gaea_setting
-- ----------------------------
DROP TABLE IF EXISTS `gaea_setting`;
CREATE TABLE `gaea_setting`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `setting_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '参数名称',
  `setting_label` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '参数描述',
  `setting_type` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '参数值类型，input input-number json-array',
  `setting_form` varchar(10240) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '参数表单',
  `setting_value` varchar(4096) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单保存的值，int\\String\\Json',
  `enable` int(1) NOT NULL DEFAULT 1 COMMENT '0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG',
  `remark` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `version` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `IDX1`(`setting_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 84 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gaea_setting
-- ----------------------------
INSERT INTO `gaea_setting` VALUES (1, 'demo_config', '配置参数示例', 'custom-form', '[{\r\n	\"type\": \"input\",\r\n	\"label\": \"IP地址\",\r\n	\"name\": \"ip\",\r\n	\"required\": true,\r\n	\"placeholder\": \"\"\r\n}, {\r\n	\"type\": \"input-number\",\r\n	\"label\": \"登录失败几次后锁定用户\",\r\n	\"name\": \"lockAfterLoginFail\",\r\n	\"required\": true,\r\n	\"placeholder\": \"\"\r\n}, {\r\n	\"type\": \"textarea\",\r\n	\"label\": \"执行脚本\",\r\n	\"name\": \"command\",\r\n	\"required\": true,\r\n	\"placeholder\": \"\"\r\n}, {\r\n	\"type\": \"code-select\",\r\n	\"dictname\": \"ENABLE_FLAG\",\r\n	\"label\": \"是否启用\",\r\n	\"name\": \"enableFlag\",\r\n	\"required\": true,\r\n	\"placeholder\": \"\"\r\n}, {\r\n	\"type\": \"select\",\r\n	\"label\": \"支付方式\",\r\n	\"name\": \"payChannel\",\r\n	\"options\": [{\r\n		\"value\": \"alipay\",\r\n		\"label\": \"支付宝\"\r\n	}, {\r\n		\"value\": \"weixin\",\r\n		\"label\": \"微信支付\"\r\n	}, {\r\n		\"value\": \"jdpay\",\r\n		\"label\": \"京东支付\"\r\n	}, {\r\n		\"value\": \"qqpay\",\r\n		\"label\": \"QQ钱包\"\r\n	}, {\r\n		\"value\": \"unipay \",\r\n		\"label\": \"银联支付\"\r\n	}],\r\n	\"required\": true,\r\n	\"placeholder\": \"\"\r\n}, {\r\n	\"type\": \"radio-group\",\r\n	\"options\": [\"上海\", \"北京\", \"广州\", \"深圳\"],\r\n	\"label\": \"活动地点\",\r\n	\"name\": \"position\",\r\n	\"required\": true,\r\n	\"placeholder\": \"\"\r\n}, {\r\n	\"type\": \"checkbox-group\",\r\n	\"options\": [\"现金\", \"网银\", \"代付\"],\r\n	\"label\": \"支付方式\",\r\n	\"name\": \"payType\",\r\n	\"required\": true,\r\n	\"placeholder\": \"\"\r\n}, {\r\n	\"type\": \"upload\",\r\n	\"label\": \"文件上传\",\r\n	\"name\": \"image_file1\",\r\n	\"required\": true,\r\n	\"placeholder\": \"\"\r\n}]', '{\"ip\":\"129.168.1.1\",\"lockAfterLoginFail\":1,\"command\":\"sh /app/startall.sh1\",\"dataCenter\":\"shangqiyun\",\"payChannel\":\"weixin\",\"position\":\"北京\",\"payType\":[\"现金\",\"网银\"],\"image_file1\":\"http://10.108.26.163:8080/auth-service/file/download/b08c940b-c1e0-4c8e-9705-9c02dd6df782\"}', 1, '配置参数示例', '管理员', '2020-11-25 15:01:57', 'admin', '2021-03-31 14:31:59', NULL);
INSERT INTO `gaea_setting` VALUES (2, 'login_config', '登录安全设置', 'custom-form', '[{\r\n	\"type\": \"input-number\",\r\n	\"label\": \"session有效分钟数\",\r\n	\"name\": \"session-expired\",\r\n	\"required\": true,\r\n	\"placeholder\": \"30\"\r\n},\r\n{\r\n	\"type\": \"input-number\",\r\n	\"label\": \"登录连续失败几次后,锁定3分钟\",\r\n	\"name\": \"lock_login_fail_times\",\r\n	\"required\": true,\r\n	\"placeholder\": \"3\"\r\n}]', '{\"session-expired\":30,\"lock_login_fail_times\":2}', 1, '登录安全设置', '管理员', '2020-11-25 15:01:57', 'zouya', '2021-03-22 14:49:12', NULL);
INSERT INTO `gaea_setting` VALUES (4, 'heart_beat_timeout_hours', '设备心跳超时小时数', 'input-number', '', '29', 0, '如果设备超过该值未上报syslog，系统将判定设备离线', '管理员', '2020-11-25 15:01:57', 'admin', '2021-06-18 15:37:57', NULL);
INSERT INTO `gaea_setting` VALUES (5, 'es_log_ttl_day', '日志监控日志保存天数', 'input-number', '', '365', 0, '日志保存天数', 'admin', '2020-11-25 15:01:57', 'admin', '2021-06-18 15:37:56', NULL);
INSERT INTO `gaea_setting` VALUES (6, 'alert_max_item', '最大展示告警事件条数', 'input-number', '', '5', 0, '最大展示告警事件条数', '管理员', '2020-11-25 15:01:57', 'admin', '2021-06-18 15:37:59', NULL);
INSERT INTO `gaea_setting` VALUES (7, 'big_screen_chart_refresh_frequency', '大屏图表刷新频率(秒)', 'input-number', '', '305', 1, '单位(秒)', 'admin', '2020-11-25 15:01:57', 'admin', '2021-02-20 16:45:28', NULL);
INSERT INTO `gaea_setting` VALUES (24, 'device_heartbeat', '设备心跳超时告警配置', 'custom-form', '[\n	{\n		\"type\": \"input-number\",\n		\"label\": \"核心交换机(分钟)\",\n		\"name\": \"coreSwitchHeartbeat\",\n		\"required\": true,\n		\"placeholder\": \"\"\n	},{\n		\"type\": \"input-number\",\n		\"label\": \"VMware(分钟)\",\n		\"name\": \"vmwareHeartbeat\",\n		\"required\": true,\n		\"placeholder\": \"\"\n	},{\n		\"type\": \"input-number\",\n		\"label\": \"外网防火墙(分钟)\",\n		\"name\": \"externalFirewallHeartbeat\",\n		\"required\": true,\n		\"placeholder\": \"\"\n	},{\n		\"type\": \"input-number\",\n		\"label\": \"邮件(分钟)\",\n		\"name\": \"mailHeartbeat\",\n		\"required\": true,\n		\"placeholder\": \"\"\n	},{\n		\"type\": \"input-number\",\n		\"label\": \"服务交换机(分钟)\",\n		\"name\": \"serverSwitchHeartbeat\",\n		\"required\": true,\n		\"placeholder\": \"\"\n	},{\r\n		\"type\": \"input-number\",\r\n		\"label\": \"71网段交换机(分钟)\",\r\n		\"name\": \"71networkSwitchHeartbeat\",\r\n		\"required\": true,\r\n		\"placeholder\": \"\"\r\n	},{\r\n		\"type\": \"input-number\",\r\n		\"label\": \"网盘(分钟)\",\r\n		\"name\": \"netdiscHeartbeat\",\r\n		\"required\": true,\r\n		\"placeholder\": \"\"\r\n	},{\r\n		\"type\": \"input-number\",\r\n		\"label\": \"无线设备(分钟)\",\r\n		\"name\": \"wifiHeartbeat\",\r\n		\"required\": true,\r\n		\"placeholder\": \"\"\r\n	},{\r\n		\"type\": \"input-number\",\r\n		\"label\": \"堡垒机(分钟)\",\r\n		\"name\": \"fortressMachineHeartbeat\",\r\n		\"required\": true,\r\n		\"placeholder\": \"\"\r\n	},{\r\n		\"type\": \"input-number\",\r\n		\"label\": \"内网防火墙(分钟)\",\r\n		\"name\": \"intranetFirewallHeartbeat\",\r\n		\"required\": true,\r\n		\"placeholder\": \"\"\r\n	}\n]', '{\n	\"coreSwitchHeartbeat\": 240,\n	\"vmwareHeartbeat\": 5,\r\n	\"externalFirewallHeartbeat\": 5,\r\n	\"mailHeartbeat\": 60,\r\n	\"serverSwitchHeartbeat\": 240,\r\n	\"71networkSwitchHeartbeat\": 1440,\r\n	\"netdiscHeartbeat\": 60,\r\n	\"fortressMachineHeartbeat\": 720,\r\n	\"wifiHeartbeat\": 60,\r\n	\"intranetFirewallHeartbeat\": 60\n}', 1, '设备心跳超时触发告警配置，高级等级为高危告警。配置名和设备信息表相匹配，包含关键字匹配。', 'admin', '2020-12-04 10:41:04', 'admin', '2020-12-07 15:37:35', NULL);
INSERT INTO `gaea_setting` VALUES (25, 'app_index_config', 'app首页轮播', 'custom-form', '[{\r\n	\"type\": \"upload\",\r\n	\"label\": \"轮播图片1\",\r\n	\"name\": \"image_file1\",\r\n	\"required\": true,\r\n	\"placeholder\": \"\"\r\n}, {\r\n	\"type\": \"upload\",\r\n	\"label\": \"轮播图片2\",\r\n	\"name\": \"image_file2\",\r\n	\"required\": true,\r\n	\"placeholder\": \"\"\r\n}, {\r\n	\"type\": \"upload\",\r\n	\"label\": \"轮播图片3\",\r\n	\"name\": \"image_file3\",\r\n	\"required\": true,\r\n	\"placeholder\": \"\"\r\n},{\r\n	\"type\": \"input-number\",\r\n	\"label\": \"轮播间隔秒\",\r\n	\"name\": \"durationSecond\",\r\n	\"required\": true,\r\n	\"placeholder\": \"\"\r\n}]', '{\"image_file1\":\"http://10.108.26.197/business/file/download/f403919a-7ee8-49b5-991f-bf0655e60401\",\"image_file2\":\"http://haitongnla.test.anji-plus.com/auth-service/file/download/057d7496-c52d-45fc-a624-6101e389deba\",\"image_file3\":\"http://10.108.26.197/business/file/download/7a6c8e86-7fec-49df-accf-d3b2546b0ad6\",\"durationSecond\":5}', 1, 'app首页轮播', '管理员', '2020-11-25 15:01:57', 'admin', '2021-03-16 17:01:56', NULL);

-- ----------------------------
-- Table structure for gaea_user
-- ----------------------------
DROP TABLE IF EXISTS `gaea_user`;
CREATE TABLE `gaea_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password_update_time` datetime(0) NULL DEFAULT NULL COMMENT '密码更新时间',
  `nickname` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '真实姓名',
  `email` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `phone` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `enabled` tinyint(1) NULL DEFAULT 1 COMMENT '1：可用 0：禁用',
  `multi_login` tinyint(1) NULL DEFAULT NULL COMMENT '设置多终端登录，0：不允许，1：允许',
  `account_locked` tinyint(1) NULL DEFAULT 0 COMMENT '0：未锁定，1：是，锁定',
  `account_non_expired` tinyint(1) NULL DEFAULT 1 COMMENT '0：否，过期，1：是，未过期',
  `credentials_non_expired` tinyint(1) NULL DEFAULT 1 COMMENT '0：否，过期，1：是，未过期',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `device_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'app端用户的设备号',
  `version` tinyint(8) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1094 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gaea_user
-- ----------------------------
INSERT INTO `gaea_user` VALUES (1, 'admin', '$2a$10$HW.83NeL8IZGu4rWT7cVKOh6xjrNpzftqx3RmOr5/O8PGdghBTuWW', NULL, '管理员', 'admin@163.com', '', 1, 1, 0, 1, 1, 'admin', '2020-07-14 14:17:01', 'admin', '2021-12-23 14:21:26', '希望提供的信息', 19);

-- ----------------------------
-- Table structure for gaea_user_role_org
-- ----------------------------
DROP TABLE IF EXISTS `gaea_user_role_org`;
CREATE TABLE `gaea_user_role_org`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `role_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色code',
  `org_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属机构code',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `version` tinyint(8) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 145 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gaea_user_role_org
-- ----------------------------
INSERT INTO `gaea_user_role_org` VALUES (142, 'admin', 'admin', '3000004', 'admin', '2021-05-06 13:55:05', 'admin', '2021-05-06 13:55:05', 1);

SET FOREIGN_KEY_CHECKS = 1;
