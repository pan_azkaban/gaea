package com.anji.plus.gaea.job.trigger.route.strategy;

import com.anji.plus.gaea.job.core.dto.ReturnT;
import com.anji.plus.gaea.job.core.param.TriggerParam;
import com.anji.plus.gaea.job.trigger.route.ExecutorRouter;

import java.util.List;

/**
 * Created by xuxueli on 17/3/10.
 *
 * Borrowed from xxljob v2.4.0
 */
public class ExecutorRouteLast extends ExecutorRouter {

    @Override
    public ReturnT<String> route(TriggerParam triggerParam, List<String> addressList) {
        return new ReturnT<String>(addressList.get(addressList.size()-1));
    }

}
