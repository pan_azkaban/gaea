package com.anji.plus.gaea.security.plus.modules.authority.controller;

import com.anji.plus.gaea.annotation.Permission;
import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.bean.TreeNode;
import com.anji.plus.gaea.curd.controller.GaeaBaseController;
import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anji.plus.gaea.security.plus.modules.authority.controller.dto.GaeaAuthorityDTO;
import com.anji.plus.gaea.security.plus.modules.authority.controller.param.GaeaAuthorityParam;
import com.anji.plus.gaea.security.plus.modules.authority.dao.entity.GaeaAuthority;
import com.anji.plus.gaea.security.plus.modules.authority.service.GaeaAuthorityService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * 菜单表(GaeaAuthority)实体类
 *
 * @author lirui
 * @since 2021-03-01 10:03:51
 */
@RestController
@RequestMapping("/gaeaAuthority")
@Api(value = "/gaeaAuthority", tags = "菜单表")
@Permission(code = "authority",name = "权限" )
public class GaeaAuthorityController extends GaeaBaseController<GaeaAuthorityParam, GaeaAuthority, GaeaAuthorityDTO> {
    @Autowired
    private GaeaAuthorityService gaeaAuthorityService;

    @Override
    public GaeaBaseService<GaeaAuthorityParam, GaeaAuthority> getService() {
        return gaeaAuthorityService;
    }

    @Override
    public GaeaAuthority getEntity() {
        return new GaeaAuthority();
    }

    @Override
    public GaeaAuthorityDTO getDTO() {
        return new GaeaAuthorityDTO();
    }


    /**
     * 权限树
     * @return
     */
    @GetMapping("/authority/tree/{org}/{role}")
    public ResponseBean authorityTree(@PathVariable("org") String org,@PathVariable("role") String role) {
        List<TreeNode> treeNodes = gaeaAuthorityService.authorityTree();

        //查询当前用户拥有的权限
        List<String> roles = new ArrayList<>();
        roles.add(role);
        Set<String> authorities = gaeaAuthorityService.userAuthorities(org,roles);

        Map<String, Object> result = new HashMap<>(2);
        result.put("has", authorities);
        result.put("all", treeNodes);
        return responseSuccessWithData(result);
    }
}
