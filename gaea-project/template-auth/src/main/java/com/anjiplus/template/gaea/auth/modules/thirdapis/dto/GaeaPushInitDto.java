package com.anjiplus.template.gaea.auth.modules.thirdapis.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * 功能描述：
 * 推送初始化请求
 * @Author: peiyanni
 * @Date: 2021/4/6 10:17
 */
@Getter
@Setter
public class GaeaPushInitDto extends GaeaPushBaseDto {
    /**
     * 登录设备唯一标识
     */
    private String deviceId;
    /**
     * 设备类型：0:其他手机，1:华为，2:小米，3:oppo，4:vivo，5：ios
     */
    private String deviceType;
    /**
     * 设备品牌（例如：小米6，iPhone 7plus）
     */
    private String brand;
    /**
     * 系统版本例如：7.1.2 13.4.1
     */
    private String osVersion;
    /**
     * 厂商token
     */
    private String manuToken;
    /**
     * 极光用户id
     */
    private String registrationId;
}
