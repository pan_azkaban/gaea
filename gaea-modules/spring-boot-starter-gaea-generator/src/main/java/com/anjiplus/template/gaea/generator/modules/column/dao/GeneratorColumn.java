/*
 *  Copyright 2019-2020 Zheng Jie
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.anjiplus.template.gaea.generator.modules.column.dao;

import java.io.Serializable;

import com.anji.plus.gaea.curd.entity.GaeaBaseEntity;
import com.anjiplus.template.gaea.generator.modules.generator.service.GeneratorUtilServiceImpl;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 列的数据信息
 * @author Zheng Jie
 * @date 2019-01-02
 */
@Data
@NoArgsConstructor
@TableName("gaea_generator_column")
public class GeneratorColumn extends GaeaBaseEntity implements Serializable {
    @ApiModelProperty(value = "schema")
    private String schemaName;

    @ApiModelProperty(value = "表名")
    private String tableName;

    @ApiModelProperty(value = "数据库字段名称")
    private String columnName;

    @ApiModelProperty(value = "数据库字段类型")
    private String columnType;

    @ApiModelProperty(value = "数据库字段键类型")
    private String keyType;

    @ApiModelProperty(value = "字段额外的参数")
    private String extra;

    @ApiModelProperty(value = "数据库字段描述")
    private String remark;

    @ApiModelProperty(value = "是否必填")
    private Boolean notNull;

    @ApiModelProperty(value = "是否在列表显示")
    private Boolean listShow;

    @ApiModelProperty(value = "是否表单显示")
    private Boolean formShow;

    @ApiModelProperty(value = "表单类型")
    private String formType;

    @ApiModelProperty(value = "查询 1:模糊 2：精确")
    private String queryType;

    @ApiModelProperty(value = "字典名称")
    private String dictName;

    @ApiModelProperty(value = "日期注解")
    private String dateAnnotation;

    @ApiModelProperty(value = "子表名称")
    private String subEntity;
    @ApiModelProperty(value = "子表字段")
    private String subEntityField;
    private String maxLength;
    private Object defaultValue;

    public GeneratorColumn(String schemaName, String tableName, String columnName, Boolean notNull,
                           String columnType, String remark, String keyType, String extra) {
        this.schemaName = schemaName;
        this.tableName = tableName;
        this.columnName = columnName;
        this.columnType = columnType;
        this.keyType = keyType;
        this.extra = extra;
        this.notNull = notNull;
        if(GeneratorUtilServiceImpl.PK.equalsIgnoreCase(keyType) && GeneratorUtilServiceImpl.EXTRA.equalsIgnoreCase(extra)){
            this.notNull = false;
        }
        this.remark = remark;
        this.listShow = true;
        this.formShow = true;
        if("id,createtime,createby,version".contains(columnName.toLowerCase())) {
            this.listShow = false;
        }
        if("id,createtime,createby,updateby,updatetime,version".contains(columnName.toLowerCase())){
            this.formShow = false;
        }
    }

    public boolean show(){
        return Boolean.TRUE.equals(formShow) || Boolean.TRUE.equals(listShow);
    }
}
