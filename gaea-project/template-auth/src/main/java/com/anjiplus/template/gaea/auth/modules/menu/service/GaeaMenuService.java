package com.anjiplus.template.gaea.auth.modules.menu.service;

import com.anji.plus.gaea.bean.TreeNode;
import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anjiplus.template.gaea.auth.modules.menu.controller.dto.GaeaLeftMenuDTO;
import com.anjiplus.template.gaea.auth.modules.menu.controller.param.GaeaMenuParam;
import com.anjiplus.template.gaea.auth.modules.menu.dao.entity.GaeaMenu;

import java.util.List;

/**
 * 菜单表(GaeaMenu)Service
 *
 * @author lr
 * @since 2021-02-02 13:36:43
 */
public interface GaeaMenuService extends GaeaBaseService<GaeaMenuParam, GaeaMenu> {

    /**
     * 根据角色查询对应的菜单
     *
     * @param roles
     * @return
     */
    List<GaeaLeftMenuDTO> getMenus(List<String> roles);


    /**
     * 获取所有菜单按钮树,角色分配权限时用
     *
     * @return
     */
    List<TreeNode> getTree();


    /**
     * 设置菜单跟权限对应关系
     *
     * @param orgCode
     * @param menuCode
     * @param authorities
     * @return
     */
    boolean saveMenuAuthorities(String orgCode, String menuCode, List<String> authorities);


    /**
     * 获取指定机构下菜单对应的权限
     *
     * @param org
     * @param menuCode
     * @return
     */
    List<String> menuOrgAuthorities(String org, String menuCode);

    /**
     * 菜单树
     * @return
     */
    List<TreeNode> menuTree();

}
