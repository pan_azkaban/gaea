package com.anji.plus.gaea.config;

import com.anji.plus.gaea.holder.UserContentHolder;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.reflection.MetaObject;

import java.util.Date;

/**
 * 自动补充插入或更新时的值
 *
 * @author lr
 * @since 2021-01-24 18:26
 */
public class MybatisPlusMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        String username = UserContentHolder.getContext().getUsername();

        if (StringUtils.isNotBlank(username)) {
            this.setFieldValByName("createBy", username, metaObject);
            this.setFieldValByName("updateBy", username, metaObject);
        } else {
            Object createBy = this.getFieldValByName("createBy", metaObject);
            Object updateBy = this.getFieldValByName("updateBy", metaObject);
            if (createBy == null) {
                this.setFieldValByName("createBy", StringUtils.EMPTY, metaObject);
            }
            if (updateBy == null) {
                this.setFieldValByName("updateBy", StringUtils.EMPTY, metaObject);
            }
        }

        this.setFieldValByName("createTime", new Date(), metaObject);

        this.setFieldValByName("updateTime", new Date(), metaObject);

        this.setFieldValByName("version", Integer.valueOf(1), metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        String username = UserContentHolder.getContext().getUsername();
        //上下文中，不存在用户名时，重置成空字符串
        if (StringUtils.isBlank(username)) {
            username = "";
        }
        this.setFieldValByName("updateBy", username, metaObject);
        this.setFieldValByName("updateTime", new Date(), metaObject);
        this.setFieldValByName("version", this.getFieldValByName("version", metaObject), metaObject);
    }
}
