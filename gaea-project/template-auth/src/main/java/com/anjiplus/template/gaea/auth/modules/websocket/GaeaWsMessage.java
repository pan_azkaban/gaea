package com.anjiplus.template.gaea.auth.modules.websocket;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 功能描述：
 *
 * @Author: peiyanni
 * @Date: 2021/4/2 14:42
 */
@Getter
@Setter
public class GaeaWsMessage implements Serializable {
    /**
     * 消息主id
     */
    private String id;
    /**
     * 消息标题
     */
    private String title;
    /**
     * 发送时间
     */
    private String time;
    /**
     * 关联的业务主id（需要查看业务详情时使用）
     */
    private String eventData;
    /**
     * 关联的业务类型（需要查看业务详情时使用）
     */
    private String eventType;

    public GaeaWsMessage(String id, String title, String time,String eventType) {
        this.id = id;
        this.title = title;
        this.time = time;
        this.eventType=eventType;
    }

    public GaeaWsMessage(String id, String title, String time, String eventData, String eventType) {
        this.id = id;
        this.title = title;
        this.time = time;
        this.eventData = eventData;
        this.eventType = eventType;
    }

}
