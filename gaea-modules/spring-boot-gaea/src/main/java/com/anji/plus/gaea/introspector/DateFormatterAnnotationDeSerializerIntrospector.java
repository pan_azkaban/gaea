package com.anji.plus.gaea.introspector;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author: Raod
 * @since: 2021-12-20
 */
public class DateFormatterAnnotationDeSerializerIntrospector extends JacksonAnnotationIntrospector {

    private static final long serialVersionUID = 7368707128625539908L;

    @Override
    public Object findDeserializer(Annotated annotated) {
        DateTimeFormat formatter = annotated.getAnnotation(DateTimeFormat.class);
        if (formatter != null) {
            return new DateDeSerializer(formatter.pattern());
        }
        //如果接口未维护DateTimeFormat，取JsonFormat
        JsonFormat jsonFormat = annotated.getAnnotation(JsonFormat.class);
        if (jsonFormat != null) {
            return new DateDeSerializer(jsonFormat.pattern());
        }
        return super.findSerializer(annotated);
    }
}
