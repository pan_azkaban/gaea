package com.anjiplus.template.gaea.business.modules.inf.constant;

/**
 * @author ultrajiaming
 * @since 2021/3/19 13:30
 * 维护缓存的key
 */
public interface InfCacheKey {

    /**
     * 接口授权的应用
     **/
    String AUTHORIZED_APP = "gaea:inf:authorized:app:";

    /**
     * 应用 密钥
     **/
    String APP_SECRET = "gaea:inf:app:secret:";

    /**
     * 初始化标识
     **/
    String INIT_FLAG = "gaea:inf:init:flag";

}
