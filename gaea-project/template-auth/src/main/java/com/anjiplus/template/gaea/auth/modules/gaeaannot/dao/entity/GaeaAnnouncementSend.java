package com.anjiplus.template.gaea.auth.modules.gaeaannot.dao.entity;

import com.anji.plus.gaea.curd.entity.GaeaBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统公告发送阅读记录表(GaeaAnnouncementSend)实体类
 *
 * @author makejava
 * @since 2021-03-29 15:31:28
 */
@TableName("gaea_announcement_send")
public class GaeaAnnouncementSend extends GaeaBaseEntity implements Serializable {
    /**
     * 系统公告id
     */
    private Long anntId;
    /**
     * 用户登录名
     */
    private String userName;
    /**
     * 阅读状态（0未读，1已读）
     */
    private Integer readFlag;
    /**
     * 阅读时间
     */
    private Date readTime;

    public Long getAnntId() {
        return anntId;
    }

    public void setAnntId(Long anntId) {
        this.anntId = anntId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getReadFlag() {
        return readFlag;
    }

    public void setReadFlag(Integer readFlag) {
        this.readFlag = readFlag;
    }

    public Date getReadTime() {
        return readTime;
    }

    public void setReadTime(Date readTime) {
        this.readTime = readTime;
    }


}