package com.anjiplus.template.gaea.business.modules.inf.inf.controller.dto;

import com.anji.plus.gaea.annotation.Formatter;
import com.anji.plus.gaea.curd.dto.GaeaBaseDTO;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * InfDTO
 *  数据传输封装
 * @author ultrajiaming
 * @since 2021-03-17 10:17:23
 */
@Data
public class InfDTO extends GaeaBaseDTO {
    // 接口标识
    @NotEmpty
    private String infName;
    // 接口名称
    @NotEmpty
    private String infNameName;
    // 接口描述
    private String infDescription;
    @NotEmpty
    private String sqlSentence;
    @Formatter(dictCode = "INF_STATUS",targetField = "statusName")
    private Integer status;
    private String statusName;
    @Formatter(dictCode = "INF_WITH_GROUP")
    private String withGroup;
    private String interactionType;
    // 动态条件  逗号隔开
    private String dynamicCondition;
    // 请求参数 Query String Parameters：VO-JSON格式字符串
    private String requestParams;
    // 返回结果示例
    private String responseDemo;
    // 响应参数 Response Parameters：VO-JSON格式字符串
    private String responseParams;
    // 执行SQL接口参数，用于生成文档
    private String sqlRequestParams;
    /**
     * 接口参数dto
     */
    private List<InfParamDTO> paramDTOS;

}
