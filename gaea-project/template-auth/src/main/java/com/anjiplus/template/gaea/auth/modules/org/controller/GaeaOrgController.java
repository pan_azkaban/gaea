package com.anjiplus.template.gaea.auth.modules.org.controller;

import com.anji.plus.gaea.annotation.log.GaeaAuditLog;
import com.anji.plus.gaea.bean.KeyValue;
import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.bean.TreeNode;
import com.anji.plus.gaea.curd.controller.GaeaBaseController;
import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anjiplus.template.gaea.auth.modules.org.controller.dto.GaeaOrgDTO;
import com.anjiplus.template.gaea.auth.modules.org.controller.param.GaeaOrgParam;
import com.anjiplus.template.gaea.auth.modules.org.dao.entity.GaeaOrg;
import com.anjiplus.template.gaea.auth.modules.org.service.GaeaOrgService;
import com.anjiplus.template.gaea.auth.modules.user.service.GaeaUserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 组织(GaeaOrg)实体类
 *
 * @author lr
 * @since 2021-02-02 13:37:33
 */
@RestController
@RequestMapping("/org")
@Api(value = "/org", tags = "组织")
public class GaeaOrgController extends GaeaBaseController<GaeaOrgParam, GaeaOrg, GaeaOrgDTO> {

    @Autowired
    private GaeaOrgService gaeaOrgService;

    @Autowired
    private GaeaUserService gaeaUserService;

    @Override
    public GaeaBaseService<GaeaOrgParam, GaeaOrg> getService() {
        return gaeaOrgService;
    }

    @Override
    public GaeaOrg getEntity() {
        return new GaeaOrg();
    }

    @Override
    public GaeaOrgDTO getDTO() {
        return new GaeaOrgDTO();
    }

    /**
     * 查询所有可用的机构信息
     *
     * @return
     */
    @GetMapping("/queryAllOrg")
    public ResponseBean queryAllOrg() {
        return responseSuccessWithData(gaeaOrgService.queryAllOrg());
    }

    /**
     * 新增机构
     * @param dto
     * @return
     */
    @PostMapping("/saveOrg")
    @GaeaAuditLog(pageTitle="新增机构")
    public ResponseBean saveOrg(@RequestBody GaeaOrgDTO dto){
        return responseSuccessWithData(gaeaOrgService.saveOrUpdateOrg(dto));
    }

    /**
     * 编辑机构
     * @param dto
     * @return
     */
    @PostMapping("/updateOrg")
    @GaeaAuditLog(pageTitle="编辑机构")
    public ResponseBean updateOrg(@RequestBody GaeaOrgDTO dto){
        return responseSuccessWithData(gaeaOrgService.saveOrUpdateOrg(dto));
    }

    /**
     * 组织树
     * @return
     */
    @GetMapping("/tree")
    public ResponseBean orgTree() {
        return responseSuccessWithData(gaeaOrgService.tree(false));
    }


    /**
     * 机构角色树
     * @return
     */
    @GetMapping("/role/tree")
    public ResponseBean orgRoleTreeSelected() {

        List<TreeNode> tree = gaeaOrgService.tree(true);
        return responseSuccessWithData(tree);
    }

    /**
     * 角色机构分配树
     * @return
     */
    @GetMapping("/user/role/tree/{username}")
    public ResponseBean orgRoleTree(@PathVariable("username") String username) {

        List<TreeNode> tree = gaeaOrgService.tree(true);

        List<String> orgRoleMappings = gaeaUserService.getOrgRoleMappings(username);

        Map<String, Object> result = new HashMap(2);
        result.put("tree", tree);
        result.put("roles", orgRoleMappings);
        return responseSuccessWithData(result);
    }



    @GetMapping("/orgSelect")
    public ResponseBean orgSelect(){
        List<GaeaOrg> orgList=gaeaOrgService.queryAllOrg();
        List<KeyValue> data=null;
        if(!CollectionUtils.isEmpty(orgList)){
            data=orgList.stream().map(e->{
                KeyValue keyValue=new KeyValue();
                keyValue.setId(e.getOrgCode());
                keyValue.setText(e.getOrgName());
                return keyValue;
            }).collect(Collectors.toList());
        }
        return responseSuccessWithData(data);
    }
}

