package com.anji.plus.gaea.security.security.extension;

import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 帮助类，留给子类实现扩展
 * @author lr
 * @since 2021-02-01
 */
public interface UserDetailsServiceHelper {

    /**
     * 根据用户名获取用户信息
     * @param username
     * @return
     */
    default UserDetails findByUsername(String username) {
        return null;
    }

    /**
     * 获取用户拥有机构对应的角色
     * @param username
     * @return
     */
    default Map<String, String> getUserRoles(String username) {
        return new HashMap<>(2);
    }


    /**
     * 获取指定用户组织编码列表
     * @param username
     * @return
     */
    default List<String> getUserOrgCodes(String username) {
        return new ArrayList<>();
    }

    /**
     * 是否是多点登录
     * @param username
     * @return
     */
    default Boolean isMultiLogin(String username) {
        return false;
    }

    /**
     * 获取用户密码更新时间，用于密码过期判断
     * @param username
     * @return
     */
    default LocalDateTime getPasswordUpdateTime(String username) {
        return null;
    }


    /**
     * 返回用户类型，账号类型，0：平台，1：租户，2：普通'
     * @param username
     * @return
     */
    default Integer isTenantUser(String username) {
        return -1;
    }

    /**
     * 获取租户编码
     * @param username
     * @return
     */
    default String getTenantCode(String username) {
        return "";
    }


    /**
     * 设置密码过期
     * @param username
     */
    default void setCredentialsExpired(String username){}

    /***
     * 获取登录用户的数据实体
     * @param username
     * @return
     */
    default GaeaUserDetail getUserEntity(String username){
        return null;
    }
}
