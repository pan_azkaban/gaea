package com.anji.plus.gaea.security.plus.modules.role.controller.param;


import com.anji.plus.gaea.annotation.Query;
import com.anji.plus.gaea.constant.QueryEnum;
import com.anji.plus.gaea.curd.params.PageParam;

import java.io.Serializable;

/**
 * 角色(GaeaRole)param
 *
 * @author lr
 * @since 2021-02-02 13:37:54
 */
public class GaeaRoleParam extends PageParam implements Serializable {

    /**
     * 机构编号
     */
    private String orgCode;

    /**
     * 角色名称
     */
    @Query(QueryEnum.LIKE)
    private String roleName;

    /**
     * 角色Code
     */
    @Query(QueryEnum.LIKE)
    private String roleCode;
    /**
     * 1：可用 0：禁用
     */
    private Integer enabled;
    /**
     * 描述
     */
    private String remark;


    /**
     * 左边树编码
     */
    @Query(where = false)
    private String code;

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }
}
