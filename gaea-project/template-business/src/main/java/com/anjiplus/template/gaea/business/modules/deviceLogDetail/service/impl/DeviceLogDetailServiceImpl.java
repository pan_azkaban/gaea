
package com.anjiplus.template.gaea.business.modules.devicelogdetail.service.impl;

import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.anjiplus.template.gaea.business.modules.devicelogdetail.dao.entity.DeviceLogDetail;
import com.anjiplus.template.gaea.business.modules.devicelogdetail.service.DeviceLogDetailService;
import com.anjiplus.template.gaea.business.modules.devicelogdetail.dao.DeviceLogDetailMapper;
/**
* @desc DeviceLogDetail 设备日志明细服务实现
* @author dev-user
* @date 2021-04-07 17:06:42.920
**/
@Service
public class DeviceLogDetailServiceImpl implements DeviceLogDetailService {

    @Autowired
    private DeviceLogDetailMapper deviceLogDetailMapper;

    @Override
    public GaeaBaseMapper<DeviceLogDetail> getMapper() {
      return deviceLogDetailMapper;
    }

    @Override
    public DeviceLogDetail getDetail(Long id) {
        DeviceLogDetail deviceLogDetail = this.selectOne(id);
        return deviceLogDetail;
    }
}