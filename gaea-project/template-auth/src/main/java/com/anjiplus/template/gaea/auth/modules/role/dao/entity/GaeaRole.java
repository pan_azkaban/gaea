package com.anjiplus.template.gaea.auth.modules.role.dao.entity;

import com.anjiplus.template.gaea.common.RespCommonCode;
import com.anji.plus.gaea.annotation.Unique;
import com.anji.plus.gaea.curd.entity.GaeaBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * 角色(GaeaRole)实体类
 *
 * @author lr
 * @since 2021-02-02 13:37:54
 */
@TableName("gaea_role")
public class GaeaRole extends GaeaBaseEntity implements Serializable {

    /**
     * 机构编码
     */
    private String orgCode;
    /**
     * 角色编码
     */
    @Unique(code = RespCommonCode.ROLE_CODE_ISEXIST)
    private String roleCode;

    private String roleName;
    /**
     * 1：可用 0：禁用
     */
    private Integer enabled;
    /**
     * 描述
     */
    private String remark;

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


}
