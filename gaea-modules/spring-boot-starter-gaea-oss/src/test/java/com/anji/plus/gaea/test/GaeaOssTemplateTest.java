package com.anji.plus.gaea.test;

import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;

import com.anji.plus.gaea.oss.config.OSSBaseProperties;
import com.anji.plus.gaea.oss.config.OSSProperties;
import com.anji.plus.gaea.oss.ossbuilder.FileClientFactory;
import com.anji.plus.gaea.oss.ossbuilder.GaeaOSSTemplate;

public class GaeaOssTemplateTest{

    @Test
    public void testFormat(){
        String key = "tms:product:tracknode:${tenantCode}";
    }

    //@Test
    //public void testHuaweiObs()throws Exception{
    public static void main(String[] args)throws Exception{
        OSSProperties config = new OSSProperties();
        config.setHuaweiObs(new OSSBaseProperties());
        config.setFileTypeWhiteList(".doc|.txt");
        config.getHuaweiObs().setAccessKey("xx");
        config.getHuaweiObs().setBucketName("hwy-tms-prod");
        config.getHuaweiObs().setSecretKey("sxc");
        config.getHuaweiObs().setUrl("obs.cn-east-2.myhuaweicloud.com");

        FileClientFactory f = new FileClientFactory(config);

        GaeaOSSTemplate template = f.getObject();
        assert template!=null;

        template.uploadFileByInputStream(new MockMultipartFile(
                "test1","test.txt",
                "txt","test1111111111".getBytes()),"test1");

        byte[] data = template.downloadFile("test1");

        assert "test".equals(new String(data));
    }
}
