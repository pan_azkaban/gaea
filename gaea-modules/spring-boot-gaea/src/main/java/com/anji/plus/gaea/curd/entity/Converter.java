package com.anji.plus.gaea.curd.entity;

import org.springframework.beans.BeanUtils;

import com.anji.plus.gaea.utils.GaeaBeanUtils;

/**
 * @author WongBin
 * @date 2023/2/3
 */
public interface Converter {

    default <T> T copyTo(Class<? extends T> cls){
        try {
            T t = cls.newInstance();
            BeanUtils.copyProperties(this, t);
            return t;
        }catch (Exception ignore){
            return null;
        }
    }

    default <T> T copyFormatTo(Class<? extends T> cls){
        try {
            T t = cls.newInstance();
            GaeaBeanUtils.copyAndFormatter(this, t);
            return t;
        }catch (Exception ignore){
            return null;
        }
    }

}
