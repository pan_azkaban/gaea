package com.anji.plus.gaea.bean;

import com.anji.plus.gaea.annotation.HashKey;

import java.io.Serializable;
import java.util.Objects;

/**
 * hash键值对
 * @author lr
 * @since 2021-01-12
 *
 **/
public class HashKeyValue implements Serializable {

    private String key;
    private String value;

    private HashKey hashKey;

    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HashKeyValue that = (HashKeyValue) o;
        return Objects.equals(key, that.key) &&
                Objects.equals(value, that.value) &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, key, value);
    }

    public boolean nonNull(){
        return key != null && value != null;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public HashKey getHashKey() {
        return hashKey;
    }

    public void setHashKey(HashKey hashKey) {
        this.hashKey = hashKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
