package com.anjiplus.template.gaea.business.modules.inf.app.dao.entity;

import com.anji.plus.gaea.curd.entity.GaeaBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
/**
 * 接口应用关系表(GaeaInfAppRelation)实体类
 *
 * @author ultrajiaming
 * @since 2021-04-08 17:37:07
 */
@TableName("gaea_inf_app_relation")
public class GaeaInfAppRelation extends GaeaBaseEntity implements Serializable {
            /**
    * 应用ID
    */
    private String appId;
        /**
    * 应用名称
    */
    private String appName;
        /**
    * 接口标识
    */
    private String infName;
        /**
    * 接口名称
    */
    private String infNameName;
        /**
    * 调用次数
    */
    private Integer times;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getInfName() {
        return infName;
    }

    public void setInfName(String infName) {
        this.infName = infName;
    }

    public String getInfNameName() {
        return infNameName;
    }

    public void setInfNameName(String infNameName) {
        this.infNameName = infNameName;
    }

    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }


}
