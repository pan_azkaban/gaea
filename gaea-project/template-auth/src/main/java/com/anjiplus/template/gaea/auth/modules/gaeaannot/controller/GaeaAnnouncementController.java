package com.anjiplus.template.gaea.auth.modules.gaeaannot.controller;

import com.anji.plus.gaea.annotation.Permission;
import com.anji.plus.gaea.annotation.log.GaeaAuditLog;
import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.curd.controller.GaeaBaseController;
import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.controller.dto.GaeaAnnouncementDTO;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.controller.param.GaeaAnnouncementParam;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.controller.param.GaeaAnnouncementSendParam;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.controller.param.ServiceMsgRequest;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.dao.entity.GaeaAnnouncement;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.service.GaeaAnnouncementSendService;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.service.GaeaAnnouncementService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 系统公告信息表(GaeaAnnouncement)实体类
 *
 * @author peiyanni
 * @since 2021-03-29 15:17:27
 */
@RestController
@RequestMapping("/annot")
@Api(value = "/annot", tags = "系统公告信息表")
@Permission(code = "annot", name = "系统公告信息表")
public class GaeaAnnouncementController extends GaeaBaseController<GaeaAnnouncementParam, GaeaAnnouncement, GaeaAnnouncementDTO> {
    @Autowired
    private GaeaAnnouncementService gaeaAnnouncementService;
    @Autowired
    private GaeaAnnouncementSendService gaeaAnnouncementSendService;

    @Override
    public GaeaBaseService<GaeaAnnouncementParam, GaeaAnnouncement> getService() {
        return gaeaAnnouncementService;
    }

    @Override
    public GaeaAnnouncement getEntity() {
        return new GaeaAnnouncement();
    }

    @Override
    public GaeaAnnouncementDTO getDTO() {
        return new GaeaAnnouncementDTO();
    }

    @PostMapping("/saveAndPublishAnnot")
    @GaeaAuditLog(pageTitle = "保存并发布公告")
    public ResponseBean saveAndPublishAnnot(@Validated @RequestBody GaeaAnnouncementDTO dto){
        return responseSuccessWithData(gaeaAnnouncementService.saveAndPublishAnnotOne(dto));
    }

    @PostMapping("/publishAnnotBatch")
    @GaeaAuditLog(pageTitle = "批量发布公告")
    public ResponseBean saveAndPublishAnnot(@RequestBody List<Long> ids){
        return responseSuccessWithData(gaeaAnnouncementService.publishAnnotBatch(ids));
    }

    @GetMapping("/myAnnotInfo")
    @GaeaAuditLog(pageTitle = "我的消息")
    public ResponseBean myAnnotInfo(GaeaAnnouncementSendParam param){
        return responseSuccessWithData(gaeaAnnouncementService.queryMyAnnotList(param));
    }

    @PostMapping("/tagAllRead")
    @GaeaAuditLog(pageTitle = "全部标记已读")
    public ResponseBean tagAllRead(@RequestBody List<Long> ids){
        return responseSuccessWithData(gaeaAnnouncementSendService.tagAllRead(ids));
    }

    @GetMapping("/readDetail/{id}")
    @GaeaAuditLog(pageTitle = "查看消息详情")
    public ResponseBean readDetail(@PathVariable("id")Long id ){
        return responseSuccessWithData(gaeaAnnouncementSendService.getAnnotReadDetailInfo(id));
    }

    @GetMapping("/cancelAnnot/{id}")
    @GaeaAuditLog(pageTitle = "撤销系统公告")
    public ResponseBean cancelAnnot(@PathVariable("id")Long id ){
        return responseSuccessWithData(gaeaAnnouncementService.cancelAnnot(id));
    }

    /**
     * 提供内部服务调用，发送业务消息
     * @param request
     * @return
     */
    @PostMapping("/sendServiceMsg")
    public ResponseBean sendServiceMsg(@Validated @RequestBody ServiceMsgRequest request){
        return responseSuccessWithData(gaeaAnnouncementService.sendServiceMsg(request));
    }

}