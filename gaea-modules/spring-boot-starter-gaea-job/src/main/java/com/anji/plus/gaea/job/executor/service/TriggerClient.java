package com.anji.plus.gaea.job.executor.service;

import com.anji.plus.gaea.job.core.dto.ReturnT;
import com.anji.plus.gaea.job.core.param.HandleCallbackParam;
import com.anji.plus.gaea.job.core.param.RegistryParam;
import com.anji.plus.gaea.job.core.util.XxlJobRemotingUtil;

import java.util.List;

public class TriggerClient {

    /** 调度器地址，http://ip:port/contextpath/ */
    private String triggerAddress;

    /** 调度器访问令牌 */
    private String accessToken;

    private int timeout = 3;

    public TriggerClient(String triggerAddress, String accessToken) {
        if(triggerAddress.endsWith("/")){
            this.triggerAddress = triggerAddress;
        }else{
            this.triggerAddress = triggerAddress + "/";
        }
        this.accessToken = accessToken;
    }

    public ReturnT<String> registryUp(RegistryParam registryParam) {
        return XxlJobRemotingUtil.postBody(triggerAddress + "jobserver/registryUp", accessToken, timeout, registryParam);
    }

    public ReturnT<String> registryDown(RegistryParam registryParam) {
        return XxlJobRemotingUtil.postBody(triggerAddress + "jobserver/registryDown", accessToken, timeout, registryParam);
    }

    public ReturnT<String> callback(List<HandleCallbackParam> callbackParamList) {
        return XxlJobRemotingUtil.postBody(triggerAddress + "jobserver/callback", accessToken, timeout, callbackParamList, String.class);
    }
}
