package com.anji.plus.gaea.archiver.sqlbuilder.builders;

import com.anji.plus.gaea.archiver.config.ArchiverTable;
import com.anji.plus.gaea.archiver.sqlbuilder.ISQLBuilder;

import java.util.Date;
/**
 * oracle相关sql语句
 *
 * @author 木子李·De
 * @since 2021/2/3 14:16
 */
public class OracleBuilder implements ISQLBuilder {
    @Override
    public String existTable(String tableName) {
        return null;
    }

    @Override
    public String existTableField(String tableName, String fieldName) {
        return null;
    }

    @Override
    public String getNow() {
        return null;
    }

    @Override
    public String countRows(String sourceTableName) {
        return null;
    }

    @Override
    public String getCrossMonthList(String tablename, String timefield, Integer maxDaysBeforeArchive) {
        return null;
    }

    @Override
    public String createArchiverTableIfNotExist(String archiverTableName, String sourceTableName) {
        return null;
    }

    @Override
    public String doArchiver(String sourceTableName, Date startTime, Date endTime) {
        return null;
    }

    /**
     * 扫描以表名为前缀的所有归档表
     *
     * @param sourceTableName
     * @return
     */
    @Override
    public String scanArchive(String sourceTableName, String dbName) {
        return null;
    }

    /**
     * 执行dump命令
     *
     * @param archiverTable
     * @return
     */
    @Override
    public String executeDump(ArchiverTable archiverTable) {
        return null;
    }

    /**
     * drop 表
     *
     * @param archiverTable
     * @return
     */
    @Override
    public String dropTable(ArchiverTable archiverTable) {
        return null;
    }
}
