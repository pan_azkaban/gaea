package com.anji.plus.gaea.job.trigger.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @desc 定时任务--调度器配置文件
 * @author 木子李*De
 * @date 2023-05-12 15:25:35.961
 **/
@ConfigurationProperties(prefix = "spring.gaea.subscribes.trigger")
public class TriggerProperties {

    /** 默认激活job调度器组件 */
    private boolean enabled = true;

    /**  快调度线程最大线程数 */
    private int fastTriggerPoolMax = 200;

    /** 慢调度线程最大线程数 */
    private int slowTriggerPoolMax = 100;

    /** 日志保留天数 */
    private int logMaxDays = 30;

    /** 注册访问令牌 */
    private String accessToken="AD6319843DDC5D6EDE5384858232BAF2";

    /** 执行器注册心路间隔 */
    private int beatTimeoutSeconds = 30;

    /** 执行器注册心路失效时间 */
    private int beatDeadSeconds = 30 * 3;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getFastTriggerPoolMax() {
        return fastTriggerPoolMax;
    }

    public void setFastTriggerPoolMax(int fastTriggerPoolMax) {
        this.fastTriggerPoolMax = fastTriggerPoolMax;
    }

    public int getSlowTriggerPoolMax() {
        return slowTriggerPoolMax;
    }

    public void setSlowTriggerPoolMax(int slowTriggerPoolMax) {
        this.slowTriggerPoolMax = slowTriggerPoolMax;
    }

    public int getLogMaxDays() {
        return logMaxDays;
    }

    public void setLogMaxDays(int logMaxDays) {
        this.logMaxDays = logMaxDays;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public int getBeatTimeoutSeconds() {
        return beatTimeoutSeconds;
    }

    public void setBeatTimeoutSeconds(int beatTimeoutSeconds) {
        this.beatTimeoutSeconds = beatTimeoutSeconds;
    }

    public int getBeatDeadSeconds() {
        return beatDeadSeconds;
    }

    public void setBeatDeadSeconds(int beatDeadSeconds) {
        this.beatDeadSeconds = beatDeadSeconds;
    }
}
