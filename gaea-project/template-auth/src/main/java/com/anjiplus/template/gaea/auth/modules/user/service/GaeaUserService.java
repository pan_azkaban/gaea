package com.anjiplus.template.gaea.auth.modules.user.service;

import com.anji.plus.gaea.bean.KeyValue;
import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anjiplus.template.gaea.auth.modules.menu.controller.dto.TreeDTO;
import com.anjiplus.template.gaea.auth.modules.org.dao.entity.GaeaOrg;
import com.anjiplus.template.gaea.auth.modules.user.controller.dto.GaeaUserQueryDto;
import com.anjiplus.template.gaea.auth.modules.user.controller.param.GaeaUserParam;
import com.anjiplus.template.gaea.auth.modules.user.controller.param.GaeaUserPasswordParam;
import com.anjiplus.template.gaea.auth.modules.user.controller.param.GaeaUserQueryParam;
import com.anjiplus.template.gaea.auth.modules.user.controller.param.UserRoleOrgReqParam;
import com.anjiplus.template.gaea.auth.modules.user.dao.entity.GaeaUser;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;
import java.util.Map;

/**
 * 用户表(GaeaUser)Service
 *
 * @author lr
 * @since 2021-02-02 13:38:12
 */
public interface GaeaUserService extends GaeaBaseService<GaeaUserParam, GaeaUser> {


    /**
     * 通过用户名获取用户信息
     * @param username
     * @return
     */
    GaeaUser getUserByUsername(String username);

    /**
     * 根据用户获取角色信息
     * @param username
     * @return
     */
    TreeDTO queryRoleTree(String username);

    /**
     * 保存用户角色机构信息
     * @param reqParam
     * @return
     */
    Boolean saveRoleTree(UserRoleOrgReqParam reqParam);

    /**
     * 修改密码操作
     * @param requestParam
     * @return
     */
    Boolean updatePassword(GaeaUserPasswordParam requestParam);

    /**
     * 根据用户查询所属的全部机构信息
     * @param username
     * @return
     */
    List<GaeaOrg> getOrgByUsername(String username);

    /**
     * 根据用户名和机构信息查询角色
     * @param username
     * @param orgCode
     * @return
     */
    List<String> getRoleByUserOrg(String username,String orgCode);

    /**
     * 管理员重置密码操作
     * @param usernames
     * @return
     */
    void setDefaultPwd(List<String> usernames);


    /**
     * 刷新用户缓存
     * @param usernameList 用户名
     */
    void refreshCache(List<String> usernameList);

    /**
     * 获取指定用户的组织列表
     * @param username
     * @return
     */
    List<String> getOrgCodes(String username);

    /**
     * 用户信息高级查询
     * 高级查询在实现类中使用注解@GaeaQuery，方法后面的参数新增一个QueryWrapper动态参数，
     * 方法内部可以直接使用QueryWrapper
     * @param param
     * @param wrappers
     * @return
     */
    Page<GaeaUserQueryDto> queryAdvanceUserInfo(GaeaUserQueryParam param, QueryWrapper ...wrappers);


    /**
     * 获取用户对应的角色
     * @return
     */
    List<String> getOrgRoleMappings(String username);


    /**
     * 用户下拉
     * @return
     */
    List<KeyValue> userSelect();


    /**
     * 解锁
     * @param usernames
     */
    void unLock(List<String> usernames);


    /**
     * 获取指定用户对应的机构角色，多个角色用逗号隔开
     * @param username
     * @return
     */
    Map<String,String> getUserOrgRoleMap(String username);

    /**
     * 根据用户名获取用户信息列表
     * @param usernames
     * @return
     */
    List<GaeaUser> getUserBynams(List<String> usernames);

}
