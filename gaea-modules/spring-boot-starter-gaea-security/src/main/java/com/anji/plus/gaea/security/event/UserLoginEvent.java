package com.anji.plus.gaea.security.event;

import org.springframework.context.ApplicationEvent;

import com.anji.plus.gaea.bean.ResponseBean;

/**
 * 登录事件
 * @author lr
 * @since 2021-01-25
 */
public class UserLoginEvent extends ApplicationEvent {

    private String username;
    private ResponseBean response;

    public UserLoginEvent(String username) {
        super(username);
        this.username = username;
    }

    public UserLoginEvent(String username, ResponseBean response) {
        super(username);
        this.username = username;
        this.response = response;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserLoginEvent{");
        sb.append("username='").append(username).append('\'');
        sb.append(", response=").append(response);
        sb.append('}');
        return sb.toString();
    }
}
