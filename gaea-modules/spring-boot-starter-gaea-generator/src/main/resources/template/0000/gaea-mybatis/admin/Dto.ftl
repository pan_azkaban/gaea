
package ${package}.controller.dto;

import java.io.Serializable;

import com.anji.plus.gaea.curd.dto.GaeaBaseDTO;
<#if hasDict>
import com.anji.plus.gaea.annotation.Formatter;
</#if>
<#if hasSubEntity>
import com.anji.plus.gaea.annotation.FormatterType;
import com.anji.plus.gaea.annotation.resolver.format.FormatterEnum;
</#if>
import lombok.Data;
<#if hasTimestamp>
import java.sql.Timestamp;
</#if>
<#if hasBigDecimal>
import java.math.BigDecimal;
</#if>
<#if !auto && pkColumnType = 'Long'>
import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.ToStringSerializer;
</#if>

<#if hasSubEntity>
import java.util.List;
<#if subEntity??>
<#list subEntity as s>
import ${basePackage}.${s.instance?lower_case}.controller.dto.${s.className}Dto;
</#list>
</#if>
</#if>

/**
*
* @description ${apiAlias} dto
* @author ${author}
* @date ${date}
**/
@Data
public class ${className}Dto extends GaeaBaseDTO implements Serializable {
<#if columns??>
<#list columns as column>
  <#if "id,createTime,createBy,updateBy,updateTime,version"?index_of('${column.changeColumnName}')<0>
    <#if column.remark != ''>
    /** ${column.remark} */
    </#if>
    <#if column.columnKey = 'PRI'>
      <#if !auto && pkColumnType = 'Long'>
    /** 防止精度丢失 */
    @JSONField(serializeUsing = ToStringSerializer.class)
      </#if>
    </#if>
    <#if column.dictName?? && column.dictName!="">
      <#--<#if column.columnType!='String'>-->
    @Formatter(dictCode = "${column.dictName}",targetField = "${column.changeColumnName}Cn")
    private ${column.columnType} ${column.changeColumnName};
    private String ${column.changeColumnName}Cn;
      <#--<#else >
    @Formatter(dictCode = "${column.dictName}")
    private ${column.columnType} ${column.changeColumnName};
      </#if>-->
    <#else >
    private ${column.columnType} ${column.changeColumnName};
    </#if>

  </#if>
</#list>
  <#if hasSubEntity>
    <#if subEntity??>
       <#list subEntity as s>
    @FormatterType(type = FormatterEnum.LIST)
    private List<${s.className}Dto> ${s.instance}s;

       </#list>
    </#if>
  </#if>
</#if>
}