package com.anjiplus.template.gaea.business.modules.inf.inf.dao.entity;

import com.anji.plus.gaea.annotation.Unique;
import com.anji.plus.gaea.curd.entity.GaeaBaseEntity;
import com.anjiplus.template.gaea.business.modules.inf.constant.InfResCode;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 接口维护表(Inf)表实体类
 *
 * @author ultrajiaming
 * @since 2021-03-17 10:17:22
 */
@TableName(value = "gaea_inf")
@Data
public class InfEntity  extends GaeaBaseEntity {
    //接口标识
    @Unique(code = InfResCode.DUPLICATE_INF_NAME)
    private String infName;
    //接口名称
    private String infNameName;
    //接口描述
    private String infDescription;
    //sql语句
    private String sqlSentence;
    //状态 1：启用 0：禁用
    private Integer status;
    //所属分组
    private String withGroup;
    //交互类型
    private String interactionType;
    // 动态条件  逗号隔开
    private String dynamicCondition;
    // 请求参数 Query String Parameters：VO-JSON格式字符串
    private String requestParams;
    // 返回结果示例
    private String responseDemo;
    // 响应参数 Response Parameters：VO-JSON格式字符串
    private String responseParams;


}
