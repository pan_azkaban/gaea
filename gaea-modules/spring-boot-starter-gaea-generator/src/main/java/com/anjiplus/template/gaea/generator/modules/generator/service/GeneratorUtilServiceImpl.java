/*
 *  Copyright 2019-2020 Zheng Jie
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.anjiplus.template.gaea.generator.modules.generator.service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.anjiplus.template.gaea.generator.config.GaeaGeneratorProperties;
import com.anjiplus.template.gaea.generator.modules.column.dao.GeneratorColumn;
import com.anjiplus.template.gaea.generator.modules.table.dao.GeneratorTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.anjiplus.template.gaea.generator.utils.ColUtil;
import com.anjiplus.template.gaea.generator.utils.FileUtil;
import com.anjiplus.template.gaea.generator.utils.StringUtils;

import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.template.Template;
import cn.hutool.extra.template.TemplateConfig;
import cn.hutool.extra.template.TemplateEngine;
import cn.hutool.extra.template.TemplateException;
import cn.hutool.extra.template.TemplateUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * 代码生成
 *
 * @author Zheng Jie
 * @date 2019-01-02
 */
@Slf4j
@SuppressWarnings({"unchecked", "all"})
@Service
public class GeneratorUtilServiceImpl implements GeneratorUtilService {

    private static final String TIMESTAMP = "Timestamp";

    private static final String BIGDECIMAL = "BigDecimal";

    public static final String PK = "PRI";

    public static final String EXTRA = "auto_increment";
    public static final String SYS_TEM_DIR = System.getProperty("java.io.tmpdir") + File.separator;

    @Autowired
    private GaeaGeneratorProperties gaeaGeneratorProperties;

    /**
     * 获取所在的服务，路由前缀，对应contextpath或者gateway中predicates.Path前缀
     * @return
     */
    protected String getServerRouteName(){
        return gaeaGeneratorProperties.getServerRouteName();
    }

    @Override
    public List<Map<String, Object>> preview(List<GeneratorColumn> generatorColumns, GeneratorTable generatorTable) {
        Map<String, Object> genMap = getGenMap(generatorColumns, generatorTable);
        List<Map<String, Object>> genList = new ArrayList<>();
        // 获取后端模版
        List<String> templates = getAdminTemplateNames(generatorTable);
        String templateRoot = getTemplateRoot(generatorTable);
        TemplateEngine engine = getEngine();
        for (String templateName : templates) {
            Map<String, Object> map = new HashMap<>(1);
            Template template = engine.getTemplate(templateRoot + templateName + ".ftl");
            map.put("content", template.render(genMap));
            map.put("name", templateName);
            genList.add(map);
        }
        // 获取前端模版
        templates = getFrontTemplateNames(generatorTable);
        String uiTemplateRoot = getUiTemplateRoot(generatorTable);
        for (String templateName : templates) {
            Map<String, Object> map = new HashMap<>(1);
            Template template = engine.getTemplate(uiTemplateRoot + templateName + ".ftl");
            map.put(templateName, template.render(genMap));
            map.put("content", template.render(genMap));
            map.put("name", templateName);
            genList.add(map);
        }
        return genList;
    }

    @Override
    public void generatorCode(List<GeneratorColumn> generatorColumnInfos, GeneratorTable generatorTable) throws IOException {
        Map<String, Object> genMap = getGenMap(generatorColumnInfos, generatorTable);
        TemplateEngine engine = getEngine();
        // 生成后端代码
        List<String> templates = getAdminTemplateNames(generatorTable);
        String rootPath = generatorTable.getWorkspace() + "/" + generatorTable.getProjectRoot();

        File workspaceDir = new File(rootPath);
        if(workspaceDir.exists() == false){
            workspaceDir.mkdirs();
        }

        String templateRoot = getTemplateRoot(generatorTable);
        for (String templateName : templates) {
            Template template = engine.getTemplate(templateRoot +  templateName + ".ftl");
            //String rootPath = System.getProperty("user.dir");
            String filePath = getAdminFilePath(templateName, generatorTable, genMap.get("className").toString(), rootPath);

            assert filePath != null;
            File file = new File(filePath);

            // 如果非覆盖生成
            if (!generatorTable.getCover() && FileUtil.exist(file)) {
                log.warn("file:{}-exists,ignore", file.getAbsolutePath());
                continue;
            }
            // 生成代码
            genFile(file, template, genMap);
        }
        // 生成前端代码
        templates = getFrontTemplateNames(generatorTable);
        String uiTemplateRoot = getUiTemplateRoot(generatorTable);
        for (String templateName : templates) {
            Template template = engine.getTemplate(uiTemplateRoot + templateName + ".ftl");
            String filePath = getFrontFilePath(rootPath, templateName, generatorTable.getApiPath(),
                    generatorTable.getPath(), genMap.get("changeClassName").toString());
            assert filePath != null;
            File file = new File(filePath);

            // 如果非覆盖生成
            if (!generatorTable.getCover() && FileUtil.exist(file)) {
                log.warn("file:{}-exists,ignore", file.getAbsolutePath());
                continue;
            }
            // 生成代码
            genFile(file, template, genMap);
        }
    }

    @Override
    public String columnToJava(String type) {
        return ColUtil.cloToJava(type);
    }

    @Override
    public String download(List<GeneratorColumn> generatorColumns, GeneratorTable generatorTable) throws IOException {

        String tempPath = SYS_TEM_DIR + "gen-temp" + File.separator + generatorTable.getProjectRoot()
                +File.separator+ generatorTable.getTableName() + File.separator;
        Map<String, Object> genMap = getGenMap(generatorColumns, generatorTable);
        TemplateEngine engine = getEngine();
        // 生成后端代码
        List<String> templates = getAdminTemplateNames(generatorTable);
        String templateRoot = getTemplateRoot(generatorTable);
        //String rootPath = genConfig.getWorkspace() + "/" + genConfig.getProject();
        for (String templateName : templates) {
            Template template = engine.getTemplate(templateRoot  + templateName + ".ftl");
            String filePath = getAdminFilePath(templateName, generatorTable,
                    genMap.get("className").toString(), tempPath + File.separator);
            assert filePath != null;
            File file = new File(filePath);
            // 如果非覆盖生成
            if (!generatorTable.getCover() && FileUtil.exist(file)) {
                log.warn("file:{}-exists,overwrite", file.getAbsolutePath());
            }
            // 生成代码
            genFile(file, template, genMap);
        }
        // 生成前端代码
        templates = getFrontTemplateNames(generatorTable);
        String uiTemplateRoot = getUiTemplateRoot(generatorTable);
        for (String templateName : templates) {
            Template template = engine.getTemplate(uiTemplateRoot  + templateName + ".ftl");
            String path = tempPath + generatorTable.getProject()+"-ui" + File.separator;
            String apiPath = path + "src" + File.separator + "api" + File.separator;
            String srcPath = path + "src" + File.separator + "views"
                    + File.separator + genMap.get("changeClassName").toString() + File.separator;
            String filePath = getFrontFilePath("",
                    templateName, apiPath, srcPath, genMap.get("changeClassName").toString());
            assert filePath != null;
            File file = new File(filePath);
            // 如果非覆盖生成
            if (!generatorTable.getCover() && FileUtil.exist(file)) {
                log.warn("file:{}-exists,overwrite", file.getAbsolutePath());
            }
            // 生成代码
            genFile(file, template, genMap);
        }
        return tempPath;
    }


    /**
     * 获取后端代码模板名称
     *
     * @return List
     */
    protected List<String> getAdminTemplateNames(GeneratorTable cfg) {
        if ("Mybatis".equalsIgnoreCase(cfg.getCatatype())) {
            return Arrays.asList(
                    "Entity", "Dto",
                    "Mapper", "Controller",
                    "QueryCriteria",
                    "Service", "ServiceImpl", "Repository"
            );
        }
        List<String> templateNames = new ArrayList<>();
        templateNames.add("Entity");
        templateNames.add("Dto");
        templateNames.add("Mapper");
        templateNames.add("Controller");
        templateNames.add("QueryCriteria");
        templateNames.add("Service");
        templateNames.add("ServiceImpl");
        templateNames.add("Repository");
        return templateNames;
    }

    /**
     * 获取前端代码模板名称
     *
     * @return List
     */
    protected List<String> getFrontTemplateNames(GeneratorTable cfg) {
        if (!cfg.getGenUi()) {
            return new ArrayList<>(0);
        }
        if ("Mybatis".equalsIgnoreCase(cfg.getCatatype())) {
            return Arrays.asList("index", "detail", "api", "DML-sql","previewItem");
        }
        List<String> templateNames = new ArrayList<>();
        templateNames.add("index");
        templateNames.add("api");
        return templateNames;
    }

    protected TemplateEngine getEngine() {
        String TEMPLATE_PATH = gaeaGeneratorProperties.getTemplatePath();
        return TemplateUtil.createEngine(new TemplateConfig(TEMPLATE_PATH,
                TemplateConfig.ResourceMode.CLASSPATH));
    }

    protected String getTemplateRoot(GeneratorTable config) {
        //if ("Mybatis".equalsIgnoreCase(config.getCatatype())) {
        //    return "gaea-mybatis/admin/";
        //}
        return "admin/";
    }
    protected String getUiTemplateRoot(GeneratorTable config) {
        if ("zh".equalsIgnoreCase(config.getGenType())) {
            //return "gaea-mybatis/front/";
            return "front/";
        }
        if ("i18n".equalsIgnoreCase(config.getGenType())) {
            //return "gaea-mybatis/front-i18n/";
            return "front-i18n/";
        }
        return "front/";
    }

    // 获取模版数据
    protected Map<String, Object> getGenMap(List<GeneratorColumn> generatorColumnInfos, GeneratorTable generatorTable) {
        // 存储模版字段数据
        Map<String, Object> genMap = new HashMap<>(16);
        // 接口别名
        genMap.put("apiAlias", generatorTable.getApiAlias());
        // 包名称
        genMap.put("package", generatorTable.getPack().toLowerCase());
        // 模块名称
        genMap.put("moduleName", generatorTable.getModuleName());
        genMap.put("project", generatorTable.getProject());
        genMap.put("projectRoot", generatorTable.getProjectRoot());
        // 作者
        genMap.put("author", generatorTable.getAuthor());
        // 创建日期
        genMap.put("date", LocalDateTime.now().toString().replace("T"," "));
        // 表名
        genMap.put("tableName", generatorTable.getTableName());
        // 大写开头的类名
        String className = "";
        // 小写开头的类名
        String changeClassName = "";
        // 判断是否去除表前缀
        if (StringUtils.isNotEmpty(generatorTable.getPrefix())) {
            className = StringUtils.toCapitalizeCamelCase(StrUtil.removePrefix(generatorTable.getTableName(), generatorTable.getPrefix()));
            changeClassName = StringUtils.toCamelCase(StrUtil.removePrefix(generatorTable.getTableName(), generatorTable.getPrefix()));
        } else {
            className = StringUtils.toCapitalizeCamelCase(generatorTable.getTableName());
            changeClassName = StringUtils.toCamelCase(generatorTable.getTableName());
        }
        // 保存类名
        genMap.put("className", className);
        // 保存小写开头的类名
        genMap.put("changeClassName", changeClassName);
        // 存在 Timestamp 字段
        genMap.put("hasTimestamp", false);
        // 查询类中存在 Timestamp 字段
        genMap.put("queryHasTimestamp", false);
        // 存在 BigDecimal 字段
        genMap.put("hasBigDecimal", false);
        // 查询类中存在 BigDecimal 字段
        genMap.put("queryHasBigDecimal", false);
        // 是否需要创建查询
        genMap.put("hasQuery", false);
        // 自增主键
        genMap.put("auto", false);
        // 存在字典
        genMap.put("hasDict", false);
        // 存在日期注解
        genMap.put("hasDateAnnotation", false);
        // 保存字段信息
        List<Map<String, Object>> columns = new ArrayList<>();
        // 保存查询字段的信息
        List<Map<String, Object>> queryColumns = new ArrayList<>();
        // 存储字典信息
        List<String> dicts = new ArrayList<>();
        // 存储 between 信息
        List<Map<String, Object>> betweens = new ArrayList<>();
        // 存储不为空的字段信息
        List<Map<String, Object>> isNotNullColumns = new ArrayList<>();

        genMap.put("hasSubEntity",false);
        for (GeneratorColumn generatorColumn : generatorColumnInfos) {
            Map<String, Object> listMap = new HashMap<>(16);
            // 字段描述
            listMap.put("remark", generatorColumn.getRemark());
            // 字段类型
            listMap.put("columnKey", generatorColumn.getKeyType());
            // 主键类型
            String colType = columnToJava(generatorColumn.getColumnType());
            // 小写开头的字段名
            String changeColumnName = StringUtils.toCamelCase(generatorColumn.getColumnName());
            // 大写开头的字段名
            String capitalColumnName = StringUtils.toCapitalizeCamelCase(generatorColumn.getColumnName());
            if (PK.equals(generatorColumn.getKeyType())) {
                // 存储主键类型
                genMap.put("pkColumnType", colType);
                // 存储小写开头的字段名
                genMap.put("pkChangeColName", changeColumnName);
                // 存储大写开头的字段名
                genMap.put("pkCapitalColName", capitalColumnName);
            }
            // 是否存在 Timestamp 类型的字段
            if (TIMESTAMP.equals(colType)) {
                genMap.put("hasTimestamp", true);
            }
            // 是否存在 BigDecimal 类型的字段
            if (BIGDECIMAL.equals(colType)) {
                genMap.put("hasBigDecimal", true);
            }
            // 主键是否自增
            if (EXTRA.equals(generatorColumn.getExtra())) {
                genMap.put("auto", true);
            }
            // 主键存在字典
            if (StringUtils.isNotBlank(generatorColumn.getDictName())) {
                genMap.put("hasDict", true);
                dicts.add(generatorColumn.getDictName());
            }

            // 存储字段类型
            listMap.put("columnType", colType);
            // 存储原始字段名称
            listMap.put("columnName", generatorColumn.getColumnName());
            // 不为空
            listMap.put("isNotNull", generatorColumn.getNotNull());
            // 字段列表显示
            listMap.put("columnShow", generatorColumn.getListShow());
            // 表单显示
            listMap.put("formShow", generatorColumn.getFormShow());
            // 表单组件类型
            listMap.put("formType", StringUtils.isNotBlank(generatorColumn.getFormType()) ? generatorColumn.getFormType() : "Input");
            // 小写开头的字段名称
            listMap.put("changeColumnName", changeColumnName);
            // 大写开头的字段名称
            listMap.put("capitalColumnName", capitalColumnName);
            // 字典名称
            listMap.put("dictName", generatorColumn.getDictName());
            // 日期注解
            listMap.put("dateAnnotation", generatorColumn.getDateAnnotation());
            if (StringUtils.isNotBlank(generatorColumn.getDateAnnotation())) {
                genMap.put("hasDateAnnotation", true);
            }
            // 添加非空字段信息
            if (generatorColumn.getNotNull()) {
                isNotNullColumns.add(listMap);
            }
            // 判断是否有查询，如有则把查询的字段set进columnQuery
            if (!StringUtils.isBlank(generatorColumn.getQueryType())) {
                // 查询类型
                listMap.put("queryType", generatorColumn.getQueryType());
                // 是否存在查询
                genMap.put("hasQuery", true);
                if (TIMESTAMP.equals(colType)) {
                    // 查询中存储 Timestamp 类型
                    genMap.put("queryHasTimestamp", true);
                }
                if (BIGDECIMAL.equals(colType)) {
                    // 查询中存储 BigDecimal 类型
                    genMap.put("queryHasBigDecimal", true);
                }
                if ("between".equalsIgnoreCase(generatorColumn.getQueryType())) {
                    betweens.add(listMap);
                } else {
                    // 添加到查询列表中
                    queryColumns.add(listMap);
                }
            }
            if(StringUtils.isNotBlank(generatorColumn.getSubEntity())){
                genMap.put("hasSubEntity",true);
                genMap.put("basePackage",StringUtils.substring(
                        generatorTable.getPack(),0, generatorTable.getPack().lastIndexOf(".")));
                genMap.put("uiBasePath",StringUtils.substring(
                        generatorTable.getPath(),0, generatorTable.getPath().lastIndexOf("/")));

                genMap.putIfAbsent("subEntity",new ArrayList<HashMap>());
                String subEntityClass = StringUtils.toCapitalizeCamelCase(
                        StrUtil.removePrefix(generatorColumn.getSubEntity(), generatorTable.getPrefix()));
                String subEntityInst = StringUtils.toCamelCase(
                        StrUtil.removePrefix(generatorColumn.getSubEntity(), generatorTable.getPrefix()));
                String subFieldName = StringUtils.toCapitalizeCamelCase(
                        StrUtil.removePrefix(generatorColumn.getSubEntityField(), generatorTable.getPrefix()));

                Map c = new HashMap();
                c.put("pClassName",className);
                c.put("pInstance",changeClassName);
                c.put("pFieldName",capitalColumnName);
                c.put("pFieldInstance",changeColumnName);
                c.put("className",subEntityClass);
                c.put("instance",subEntityInst);
                c.put("fieldName",subFieldName);
                c.put("dbFieldName", generatorColumn.getSubEntityField());
                c.put("dbTableName", generatorColumn.getSubEntity());
                ((List)genMap.get("subEntity")).add(c);
            }
            listMap.put("maxLength", generatorColumn.getMaxLength());
            listMap.put("defaultValue", generatorColumn.getDefaultValue());
            // 添加到字段列表中
            columns.add(listMap);
        }
        // 保存字段列表
        genMap.put("columns", columns);
        // 保存查询列表
        genMap.put("queryColumns", queryColumns);
        // 保存字典列表
        genMap.put("dicts", dicts);
        // 保存查询列表
        genMap.put("betweens", betweens);
        // 保存非空字段信息
        genMap.put("isNotNullColumns", isNotNullColumns);
        return genMap;
    }

    /**
     * 定义后端文件路径以及名称
     */
    protected String getAdminFilePath(String templateName, GeneratorTable generatorTable,
                                      String className, String rootPath) {
        String projectPath = rootPath + File.separator + generatorTable.getModuleName();
        String packagePath = projectPath + File.separator + "src" + File.separator + "main"
                + File.separator + "java" + File.separator;
        String resourcePath = projectPath + File.separator + "src" + File.separator + "main"
                + File.separator + "resources" + File.separator;

        if (!ObjectUtils.isEmpty(generatorTable.getPack())) {
            packagePath += generatorTable.getPack().replace(".", File.separator) + File.separator;
        }

        if ("Entity".equals(templateName)) {
            return packagePath + "dao" + File.separator + "entity" + File.separator + className + ".java";
        }

        if ("Controller".equals(templateName)) {
            return packagePath + "controller" + File.separator + className + "Controller.java";
        }

        if ("Service".equals(templateName)) {
            return packagePath + "service" + File.separator + className + "Service.java";
        }

        if ("ServiceImpl".equals(templateName)) {
            return packagePath + "service" + File.separator + "impl" + File.separator + className + "ServiceImpl.java";
        }

        if ("Dto".equals(templateName)) {
            return packagePath + "controller" + File.separator + "dto" + File.separator + className + "Dto.java";
        }

        if ("QueryCriteria".equals(templateName)) {
            return packagePath + "controller" + File.separator + "param" + File.separator + className + "Param.java";
        }

        if ("Mapper".equals(templateName)) {
            return resourcePath + "mapper" + File.separator + className + "Mapper.xml";
        }

        if ("Repository".equals(templateName)) {
            return packagePath + "dao" + File.separator + className + "Mapper.java";
        }

        return null;
    }

    /**
     * 定义前端文件路径以及名称
     */
    protected String getFrontFilePath(
            String rootPath, String templateName,
            String apiPath, String path, String apiName) {

        if ("api".equals(templateName)) {
            return rootPath + apiPath + File.separator + apiName + ".js";
        }

        if ("index".equals(templateName)) {
            return rootPath + path + File.separator + "index.vue";
        }
        if ("detail".equals(templateName)) {
            return rootPath + path + File.separator + "component/detail.vue";
        }
        if ("previewItem".equals(templateName)) {
            return rootPath + path + File.separator + "component/previewItem.vue";
        }
        if ("DML-sql".equals(templateName)) {
            return rootPath + path + File.separator + "component/menu-insert.sql";
        }
        return null;
    }

    protected void genFile(File file, Template template, Map<String, Object> map) throws IOException {
        // 生成目标文件
        Writer writer = null;
        log.info("genFiles=" + file.getAbsolutePath());
        try {
            FileUtil.touch(file);
            writer = new FileWriter(file);
            template.render(map, writer);
        } catch (TemplateException | IOException e) {
            throw new RuntimeException(e);
        } finally {
            if(writer != null){
                writer.close();
            }
        }
    }
}
