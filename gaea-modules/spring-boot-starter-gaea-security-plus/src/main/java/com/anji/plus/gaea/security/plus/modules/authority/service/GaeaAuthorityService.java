package com.anji.plus.gaea.security.plus.modules.authority.service;

import com.anji.plus.gaea.bean.TreeNode;
import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anji.plus.gaea.security.plus.modules.authority.controller.param.GaeaAuthorityParam;
import com.anji.plus.gaea.security.plus.modules.authority.dao.entity.GaeaAuthority;

import java.util.List;
import java.util.Set;

/**
 * 菜单表(GaeaAuthority)Service
 *
 * @author lirui
 * @since 2021-03-01 10:03:51
 */
public interface GaeaAuthorityService extends GaeaBaseService<GaeaAuthorityParam, GaeaAuthority> {

    /**
     * 权限树
     *
     * @return
     */
    List<TreeNode> authorityTree();


    /**
     * 获取指定角色的权限
     * @param org
     * @param role
     * @return
     */
    Set<String> userAuthorities(String org, List<String> role);

}
