package com.anjiplus.template.gaea.business.modules.rules.controller.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * (GaeaRulesFields)DTO
 * 规则字段值
 *
 * @author why
 * @since 2021-03-17 15:52:55
 */
@Getter
@Setter
public class GaeaRulesFieldsDTO implements Serializable {

    /**
     * 字段名
     */
    private String name;
    /**
     * 字段类型
     */
    private String type;
    /**
     * 是否必填
     */
    private Boolean require;
    /**
     * 字段描述
     */
    private String desc;
}
