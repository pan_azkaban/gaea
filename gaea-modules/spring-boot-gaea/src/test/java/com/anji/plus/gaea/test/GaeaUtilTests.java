package com.anji.plus.gaea.test;

import java.util.Arrays;

import com.anji.plus.gaea.utils.GaeaBeanUtils;
import com.anji.plus.gaea.utils.GaeaUtils;
import org.junit.Test;

public class GaeaUtilTests {

    @Test
    public void testFormat(){
        String key = "tms:product:tracknode:${tenantCode}";
        System.out.println(GaeaUtils.replaceFormatString(key,"123","2234"));
        System.out.println(GaeaUtils.replaceFormatString(key,"123"));

        String f = "aaa,abb,ccc";
        String d = "aaa&abb&ccc";
        String seperator = ",|&";
        System.out.println(GaeaBeanUtils.getSeperator(f,seperator,"|"));
        System.out.println(GaeaBeanUtils.getSeperator(d,seperator,"|"));
        System.out.println(GaeaBeanUtils.getSeperator(f,",","|"));
        System.out.println(GaeaBeanUtils.getSeperator(d,"&","|"));
    }
}
