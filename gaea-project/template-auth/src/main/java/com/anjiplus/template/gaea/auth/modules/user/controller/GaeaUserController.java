package com.anjiplus.template.gaea.auth.modules.user.controller;

import com.anji.plus.gaea.annotation.log.GaeaAuditLog;
import com.anji.plus.gaea.bean.KeyValue;
import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.curd.controller.GaeaBaseController;
import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anjiplus.template.gaea.auth.modules.menu.controller.dto.TreeDTO;
import com.anjiplus.template.gaea.auth.modules.user.controller.dto.GaeaUserDTO;
import com.anjiplus.template.gaea.auth.modules.user.controller.param.GaeaUserParam;
import com.anjiplus.template.gaea.auth.modules.user.controller.param.GaeaUserPasswordParam;
import com.anjiplus.template.gaea.auth.modules.user.controller.param.GaeaUserQueryParam;
import com.anjiplus.template.gaea.auth.modules.user.controller.param.UserRoleOrgReqParam;
import com.anjiplus.template.gaea.auth.modules.user.dao.entity.GaeaUser;
import com.anjiplus.template.gaea.auth.modules.user.service.GaeaUserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户表(GaeaUser)实体类
 *
 * @author lirui
 * @since 2021-02-02 13:38:12
 */
@RestController
@RequestMapping("/user")
@Api(value = "/user", tags = "用户表")
public class GaeaUserController extends GaeaBaseController<GaeaUserParam, GaeaUser, GaeaUserDTO> {

    @Autowired
    private GaeaUserService gaeaUserService;

    @Override
    public GaeaBaseService<GaeaUserParam, GaeaUser> getService() {
        return gaeaUserService;
    }

    @Override
    public GaeaUser getEntity() {
        return new GaeaUser();
    }

    @Override
    public GaeaUserDTO getDTO() {
        return new GaeaUserDTO();
    }

    /**
     * 查询用户关联的机构角色树
     *
     * @param username
     * @return
     */
    @GetMapping("/queryRoleTree/{username}")
    public ResponseBean queryRoleTree(@PathVariable("username") String username) {
        TreeDTO data = gaeaUserService.queryRoleTree(username);
        return responseSuccessWithData(data);
    }

    /**
     * 保存用户角色机构关联关系
     *
     * @param reqParam
     * @return
     */
    @PostMapping("/saveRoleTree")
    @GaeaAuditLog(pageTitle = "分配用户角色")
    public ResponseBean saveRoleTree(@RequestBody UserRoleOrgReqParam reqParam) {
        Boolean data = gaeaUserService.saveRoleTree(reqParam);
        return responseSuccessWithData(data);
    }

    /**
     * 刷新用户名和用户真实姓名
     * @return
     */
    @PostMapping("/refresh/username")
    public ResponseBean refreshCache(@RequestBody List<String> usernameList) {
        gaeaUserService.refreshCache(usernameList);
        return responseSuccess();
    }

    /**
     * 用户修改密码
     * @param reqParam
     * @return
     */
    @PostMapping("/updatePassword")
    @GaeaAuditLog(pageTitle = "修改密码")
    public ResponseBean updatePassword(@Validated @RequestBody GaeaUserPasswordParam reqParam){
        return responseSuccessWithData(gaeaUserService.updatePassword(reqParam));
    }

    /**
     * 用户重置密码
     * @param usernames
     * @return
     */
    @PostMapping("/resetPwd")
    @GaeaAuditLog(pageTitle = "重置密码")
    public ResponseBean resetPassword(@RequestBody List<String> usernames){
        gaeaUserService.setDefaultPwd(usernames);
        return responseSuccess();
    }

    @PostMapping("/queryAdvanceUser")
    @GaeaAuditLog(pageTitle = "高级查询")
    public ResponseBean userAdvanceQuery(@RequestBody GaeaUserQueryParam requestParam){
        return responseSuccessWithData(gaeaUserService.queryAdvanceUserInfo(requestParam));
    }

    /**
     * 用户名与用户真实姓名的下拉列表
     * @return
     */
    @GetMapping("/select")
    public ResponseBean userSelect() {
        List<KeyValue> keyValues = gaeaUserService.userSelect();
        return responseSuccessWithData(keyValues);
    }

    @PostMapping("/unLock")
    public ResponseBean unLock(@RequestBody List<String> usernames) {
        gaeaUserService.unLock(usernames);
        return responseSuccess();
    }
}
