package com.anji.plus.gaea.security.plus.modules.menu.controller;

import com.anji.plus.gaea.annotation.Permission;
import com.anji.plus.gaea.annotation.log.GaeaAuditLog;
import com.anji.plus.gaea.bean.KeyValue;
import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.bean.TreeNode;
import com.anji.plus.gaea.curd.controller.GaeaBaseController;
import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anji.plus.gaea.holder.UserContentHolder;
import com.anji.plus.gaea.security.plus.modules.authority.service.GaeaAuthorityService;
import com.anji.plus.gaea.security.plus.modules.menu.controller.dto.GaeaLeftMenuDTO;
import com.anji.plus.gaea.security.plus.modules.menu.controller.dto.GaeaMenuAuthorityDTO;
import com.anji.plus.gaea.security.plus.modules.menu.controller.dto.GaeaMenuDTO;
import com.anji.plus.gaea.security.plus.modules.menu.controller.param.GaeaMenuParam;
import com.anji.plus.gaea.security.plus.modules.menu.controller.param.LeftMenuReqParam;
import com.anji.plus.gaea.security.plus.modules.menu.dao.entity.GaeaMenu;
import com.anji.plus.gaea.security.plus.modules.menu.service.GaeaMenuService;
import com.anji.plus.gaea.security.plus.modules.role.service.GaeaRoleService;
import com.anji.plus.gaea.security.plus.modules.user.dao.entity.GaeaUser;
import com.anji.plus.gaea.security.plus.modules.user.service.GaeaUserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 菜单表(GaeaMenu)实体类
 *
 * @author lr
 * @since 2021-02-02 13:36:43
 */
@RestController
@RequestMapping("/menu")
@Api(value = "/menu", tags = "菜单表")
@Permission(code = "menu",name = "菜单")
public class GaeaMenuController extends GaeaBaseController<GaeaMenuParam, GaeaMenu, GaeaMenuDTO> {
    @Autowired
    private GaeaMenuService gaeaMenuService;

    @Autowired
    private GaeaUserService gaeaUserService;

    @Autowired
    private GaeaRoleService gaeaRoleService;

    @Autowired
    private GaeaAuthorityService gaeaAuthorityService;

    @Override
    public GaeaBaseService<GaeaMenuParam, GaeaMenu> getService() {
        return gaeaMenuService;
    }

    @Override
    public GaeaMenu getEntity() {
        return new GaeaMenu();
    }

    @Override
    public GaeaMenuDTO getDTO() {
        return new GaeaMenuDTO();
    }

    @PostMapping("/menuUserInfoByOrg")
    @GaeaAuditLog(pageTitle="获取菜单")
    public ResponseBean getMenuInfoByOrg(@RequestBody LeftMenuReqParam reqParam){
        String username = UserContentHolder.getContext().getUsername();
        //获取当前用户所在机构
        Map<String,Object> userInfo = new HashMap<>(8);
        List<KeyValue> orgList = gaeaUserService.getOrgByUsername(username);
        if(!CollectionUtils.isEmpty(orgList)){
            String orgCode=reqParam.getOrgCode();
            if(StringUtils.isEmpty(orgCode)){
                orgCode=orgList.get(0).getId().toString();
            }
            List<String> userRoles =gaeaUserService.getRoleByUserOrg(username,orgCode);
            //获取当前用户所拥有的菜单
            List<GaeaLeftMenuDTO> userMenus = gaeaMenuService.getMenus(userRoles);

            List<String> roles = gaeaUserService.getRoleByUserOrg(username, orgCode);
            if(!CollectionUtils.isEmpty(roles)){
                //按钮权限
                Set<String> authorities  = gaeaAuthorityService.userAuthorities(orgCode, roles);
                userInfo.put("menus",userMenus);
                userInfo.put("roles",userRoles);
                userInfo.put("orgs",orgList);
                userInfo.put("currentOrgCode",orgCode);
                userInfo.put("authorities", authorities);
            }else{
                userInfo.put("menus",userMenus);
                userInfo.put("roles",null);
                userInfo.put("orgs",orgList);
                userInfo.put("currentOrgCode",orgCode);
                userInfo.put("authorities", null);
            }
        }else{
            userInfo.put("menus",null);
            userInfo.put("roles",null);
            userInfo.put("orgs",null);
        }
        GaeaUser gaeaUser = gaeaUserService.getUserByUsername(username);
        userInfo.put("username",username);
        userInfo.put("nickname",gaeaUser.getNickname());
        ResponseBean listResponseBean = ResponseBean.builder().data(userInfo).build();
        return listResponseBean;
    }

    /**
     * 获取权限树
     * @return
     */
    @GetMapping("/tree")
    public ResponseBean getTree() {
        List<TreeNode> tree = gaeaMenuService.getTree();
        return responseSuccessWithData(tree);
    }

    /**
     * 获取菜单树
     * @return
     */
    @GetMapping("/role/tree/{orgCode}/{roleCode}")
    public ResponseBean getRoleTree(@PathVariable("orgCode") String orgCode,@PathVariable("roleCode") String roleCode) {
        List<TreeNode> tree = gaeaMenuService.menuTree();

        Set<String> menuCodes = gaeaMenuService.getMenuCodes(orgCode, roleCode);
        Map<String,Object> result = new HashMap<>(2);
        result.put("menuTree", tree);
        result.put("menuCodes", menuCodes);
        return responseSuccessWithData(result);
    }

    /**
     * 菜单绑定权限
     * @param authorityDTO
     * @return
     */
    @PostMapping("/mapper/authorities")
    @Permission(code = "authority", name = "绑定权限")
    public ResponseBean menuAuthority(@RequestBody GaeaMenuAuthorityDTO authorityDTO) {
        gaeaMenuService.saveMenuAuthorities(authorityDTO.getMenuCode(), authorityDTO.getAuthCodes());
        return responseSuccess();
    }

    /**
     * 菜单绑定权限树
     * @return
     */
    @GetMapping("/authority/tree/{menuCode}")
    public ResponseBean authorityTree(@PathVariable("menuCode") String menuCode) {
        List<TreeNode> treeNodes = gaeaAuthorityService.authorityTree();

        List<String> gaeaRoleAuthorities = gaeaMenuService.menuOrgAuthorities(menuCode);

        Map<String, Object> result = new HashMap<>(2);
        result.put("has", gaeaRoleAuthorities);
        result.put("all", treeNodes);
        return responseSuccessWithData(result);
    }

    /**
     * 获取菜单下拉选项
     * @return
     */
    @GetMapping("/menuSelect")
    public ResponseBean menuSelect(){
        List<GaeaMenu> menusList=gaeaMenuService.findAll();
        List<KeyValue> data=null;
        if(!CollectionUtils.isEmpty(menusList)){
            data=menusList.stream().map(e->{
                KeyValue keyValue=new KeyValue();
                keyValue.setId(e.getMenuCode());
                keyValue.setText(e.getMenuName());
                return keyValue;
            }).collect(Collectors.toList());
        }
        return responseSuccessWithData(data);
    }

}
