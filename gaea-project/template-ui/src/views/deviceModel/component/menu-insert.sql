insert into gaea_menu(menu_code, menu_name, sys_code, parent_code, path,
                      menu_icon,sort,enabled,delete_flag, redirect_url, component,
                      create_by, update_by, create_time, update_time, version)values
('DeviceModel','设备类型','PC','SystemSet','deviceModel','',3060,1,	0,'','system-set/deviceModel/index','admin','管理员',now(),now(),1);

insert into gaea_authority(application_name, scan_annotation, auth_code, auth_name, parent_code, path,
                           create_time, update_time, version) values
('gaea-business', 0, 'DeviceModelController#pageList', 'uiI18n查询', 'DeviceModelController', 'GET#/deviceModel/pageList', now(), now(), 1),
('gaea-business', 0, 'DeviceModelController#detail', 'uiI18n详情', 'DeviceModelController', 'GET#/deviceModel/**', now(), now(), 1),
('gaea-business', 0, 'DeviceModelController#update', 'uiI18n修改', 'DeviceModelController', 'PUT#/deviceModel/', now(), now(), 1),
('gaea-business', 0, 'DeviceModelController#insert', 'uiI18n新增', 'DeviceModelController', 'POST#/deviceModel/', now(), now(), 1),
('gaea-business', 0, 'DeviceModelController#deleteById', 'uiI18n删除', 'DeviceModelController', 'DELETE#/deviceModel/**', now(), now(), 1)
;

insert into gaea_menu_authority(org_code, menu_code, auth_code, create_by,
                                update_by, create_time, update_time, version)VALUES
('3000004','DeviceModel','DeviceModelController#pageList','','',now(),now(),1),
('3000004','DeviceModel','DeviceModelController#detail','','',now(),now(),1),
('3000004','DeviceModel','DeviceModelController#update','','',now(),now(),1),
('3000004','DeviceModel','DeviceModelController#insert','','',now(),now(),1),
('3000004','DeviceModel','DeviceModelController#deleteById','','',now(),now(),1);