package com.anji.plus.gaea.job.trigger.config;

import com.anji.plus.gaea.job.trigger.scheduler.XxlJobScheduler;
import com.anji.plus.gaea.job.trigger.dao.DaoService;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * @desc 调度器自动装配
 * @author 木子李*de
 * @date 2023-05-17 15:25:35.961
 **/
public class TriggerConfiguration implements InitializingBean, DisposableBean {
    //初始化时，外部传入
    private ApplicationContext applicationContext;
    private JdbcTemplate jdbcTemplate;
    private RedissonClient redissonClient;
    private TriggerProperties triggerProperties;

    // 当前类初始化
    private XxlJobScheduler xxlJobScheduler;

    // 单例
    private static TriggerConfiguration instance = null;

    @Override
    public void afterPropertiesSet() throws Exception {
        this.instance = this;

        // 调度器中使用jdbcTemplate完成所有与数据库交互
        DaoService.loadJDBC(jdbcTemplate);

        // 调度器初始化
        xxlJobScheduler = new XxlJobScheduler();
        xxlJobScheduler.start();
    }

    @Override
    public void destroy() throws Exception {
        xxlJobScheduler.stop();
    }

    //*********************** getter and setter *************************

    public static TriggerConfiguration getInstance() {
        return instance;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public RedissonClient getRedissonClient() {
        return redissonClient;
    }

    public void setRedissonClient(RedissonClient redissonClient) {
        this.redissonClient = redissonClient;
    }

    public TriggerProperties getTriggerProperties() {
        return triggerProperties;
    }

    public void setTriggerProperties(TriggerProperties triggerProperties) {
        this.triggerProperties = triggerProperties;
    }
}
