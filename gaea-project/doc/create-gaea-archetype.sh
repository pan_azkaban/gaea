#!/usr/bin/env bash

root=`pwd`
echo 'build dependency'
git pull
#git reset --hard
#cd ../gaea-modules && mvn clean install
cd $root

echo 'create archetype,rm unused large files...'
if [ -d ./template-ui/node_modules ];then
	echo 'rm node_modules libs'
	rm -R ./template-ui/node_modules
fi
echo 'create archetype'

mvn clean && mvn archetype:create-from-project

echo 'install archetype'

#cp  gaea-refer-doc/archetype-metadata.xml  target/generated-sources/archetype/src/main/resources/META-INF/maven/archetype-metadata.xml
cd target/generated-sources/archetype
echo `pwd`
#mvn install

echo 'before deploy archetype,add distributionManagement node to generated-sources\archetype\pom.xml'
sed -i '11 a
	<distributionManagement>
	  <repository>
		  <id>nexus-releases</id>
		  <url>http://nexus.anji-plus.com:8081/repository/maven-releases/</url>
	  </repository>
	  <snapshotRepository>
		  <id>nexus-snapshots</id>
		  <url>http://nexus.anji-plus.com:8081/repository/maven-snapshots/</url>
	  </snapshotRepository>
	</distributionManagement>
	' target/generated-sources/archetype/pom.xml

echo 'deploy-path:'`pwd`
mvn deploy

echo 'view archetype-catalog'
echo 'http://nexus.anji-plus.com:8081/repository/maven-snapshots/archetype-catalog.xml'
cd $root

#