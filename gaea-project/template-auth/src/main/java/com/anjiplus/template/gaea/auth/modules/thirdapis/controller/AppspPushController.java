package com.anjiplus.template.gaea.auth.modules.thirdapis.controller;

import com.anji.plus.gaea.bean.ResponseBean;
import com.anjiplus.template.gaea.auth.modules.thirdapis.controller.param.AppspPushParam;
import com.anjiplus.template.gaea.auth.modules.thirdapis.service.AppPushMsgService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 功能描述：
 * 走Appsp推送平台接口进行app端推送
 * 跨服务接口
 * @Author: peiyanni
 * @Date: 2021/4/12 13:27
 */
@RestController
@RequestMapping("/apppush")
@Api(value = "/apppush", tags = "")
public class AppspPushController {
    @Autowired
    private AppPushMsgService appPushMsgService;

    /**
     * app推送测试
     * @param pushParam
     * @return
     */
    @PostMapping("/testpush")
    public ResponseBean testAppspPush(@Validated @RequestBody AppspPushParam pushParam){
        Boolean isTrue=appPushMsgService.testPush(pushParam);
        return ResponseBean.builder().data(isTrue).build();
    }


}
