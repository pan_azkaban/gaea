package com.anjiplus.template.gaea.business.modules.rules.controller.param;


import com.anji.plus.gaea.annotation.Query;
import com.anji.plus.gaea.constant.QueryEnum;
import com.anji.plus.gaea.curd.params.PageParam;

import java.io.Serializable;

/**
 * (GaeaRules)param
 *
 * @author lirui
 * @since 2021-03-12 10:38:19
 */
public class GaeaRulesParam extends PageParam implements Serializable {

    /**
     * 规则名称
     */
    @Query(QueryEnum.LIKE)
    private String ruleName;

    /**
     * 规则编码
     */
    @Query(QueryEnum.LIKE)
    private String ruleCode;

    /**
     * 规则状态
     */
    private Integer enabled;

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(String ruleCode) {
        this.ruleCode = ruleCode;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }
}
