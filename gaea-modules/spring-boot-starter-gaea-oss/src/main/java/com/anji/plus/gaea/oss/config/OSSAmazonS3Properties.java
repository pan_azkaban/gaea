package com.anji.plus.gaea.oss.config;

import java.io.Serializable;

/**
 * AmazonS3配置项
 *
 * @author 木子李·De
 * @since 2022/3/22 14:16
 */
public class OSSAmazonS3Properties implements Serializable {

    private String url;
    private String accessKey;
    private String secretKey;
    private String bucketName;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }
}
