package com.anji.plus.gaea.security.plus.modules.menu.controller.param;

import java.io.Serializable;

/**
 * 功能描述：
 *
 * @Author: peiyanni
 * @Date: 2021/2/7 13:03
 */
public class LeftMenuReqParam implements Serializable {
    /**
     * 用户当前所属机构
     */
    private String orgCode;

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }
}
