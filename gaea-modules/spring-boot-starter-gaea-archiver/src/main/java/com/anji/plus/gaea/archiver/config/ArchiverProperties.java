package com.anji.plus.gaea.archiver.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;
/**
 * 归档组件配置项
 *
 * @author 木子李·De
 * @since 2021/2/3 14:16
 */
@ConfigurationProperties(prefix = ArchiverProperties.COMPONENT_PREFIX + ArchiverProperties.COMPONENT_NAME)
public class ArchiverProperties {
    public final static String COMPONENT_PREFIX="spring.gaea.subscribes.";
    /**
     * 组件名称
     */
    public final static String COMPONENT_NAME = "archiver";

    private boolean enabled = true;

    /** 归档触发的定时器 */
    private String archiveScheduledCron;

    /** 将n天前的数据，移动到归档表，tables配置项中策略优先加载 */
    private Integer maxDaysBeforeArchive;

    /** 归档表中，已归档的数据保留期限，超过期限的数据将删除。tables配置项中策略优先加载 */
    private Integer maxDaysBeforeDelete;

    /** 删除历史数据表，是否需要存入本地文件 */
    private boolean historicalBackup = false;

    /** 生效要归档的库名 historicalBackup为true生效*/
    private String dbName;

    /** dump 归档host historicalBackup为true生效*/
    private String dumpHost;

    /** dump 归档port historicalBackup为true生效*/
    private String dumpPort;

    /** dump 归档用户 historicalBackup为true生效*/
    private String dumpUser;

    /** dump 归档密码 historicalBackup为true生效*/
    private String dumpPasswd;

    /** dump 归档本地路径 historicalBackup为true生效*/
    private String dumpFile;

    /** 限制执行节点ip */
    private String executeIp;

    /** 归档表配置 */
    private List<ArchiverTable> tables = new ArrayList<ArchiverTable>();

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isHistoricalBackup() {
        return historicalBackup;
    }

    public void setHistoricalBackup(boolean historicalBackup) {
        this.historicalBackup = historicalBackup;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getDumpHost() {
        return dumpHost;
    }

    public void setDumpHost(String dumpHost) {
        this.dumpHost = dumpHost;
    }

    public String getDumpPort() {
        return dumpPort;
    }

    public void setDumpPort(String dumpPort) {
        this.dumpPort = dumpPort;
    }

    public String getDumpUser() {
        return dumpUser;
    }

    public void setDumpUser(String dumpUser) {
        this.dumpUser = dumpUser;
    }

    public String getDumpPasswd() {
        return dumpPasswd;
    }

    public void setDumpPasswd(String dumpPasswd) {
        this.dumpPasswd = dumpPasswd;
    }

    public String getDumpFile() {
        return dumpFile;
    }

    public void setDumpFile(String dumpFile) {
        this.dumpFile = dumpFile;
    }

    public String getArchiveScheduledCron() {
        return archiveScheduledCron;
    }

    public void setArchiveScheduledCron(String archiveScheduledCron) {
        this.archiveScheduledCron = archiveScheduledCron;
    }

    public Integer getMaxDaysBeforeArchive() {
        return maxDaysBeforeArchive;
    }

    public void setMaxDaysBeforeArchive(Integer maxDaysBeforeArchive) {
        this.maxDaysBeforeArchive = maxDaysBeforeArchive;
    }

    public Integer getMaxDaysBeforeDelete() {
        return maxDaysBeforeDelete;
    }

    public void setMaxDaysBeforeDelete(Integer maxDaysBeforeDelete) {
        this.maxDaysBeforeDelete = maxDaysBeforeDelete;
    }

    public List<ArchiverTable> getTables() {
        return tables;
    }

    public void setTables(List<ArchiverTable> tables) {
        this.tables = tables;
    }

    public String getExecuteIp() {
        return executeIp;
    }

    public void setExecuteIp(String executeIp) {
        this.executeIp = executeIp;
    }
}
