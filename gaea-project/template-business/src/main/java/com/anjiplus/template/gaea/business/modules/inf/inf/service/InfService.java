package com.anjiplus.template.gaea.business.modules.inf.inf.service;


import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anjiplus.template.gaea.business.modules.inf.inf.controller.dto.InfDTO;
import com.anjiplus.template.gaea.business.modules.inf.inf.controller.params.InfParams;
import com.anjiplus.template.gaea.business.modules.inf.inf.dao.entity.InfEntity;

/**
 * 接口维护表(Inf)表服务接口
 *
 * @author ultrajiaming
 * @since 2021-03-17 10:17:24
 */
public interface InfService extends GaeaBaseService<InfParams, InfEntity> {

    /**
     * 通过接口名称查找
     **/
    InfEntity getByName(String infName);

    /**
     * 上线
     **/
    void online(Long id);

    /**
     * 下线
     **/
    void offline(Long id);

    /**
     * 审核
     **/
    void examine(Long id);

    /**
     * 授权应用
     **/
    void authorizeApps(String infName, String appIds);

    /**
     * 根据id查询
     * @param id
     * @return
     */
    InfDTO queryById(Long id);

}
