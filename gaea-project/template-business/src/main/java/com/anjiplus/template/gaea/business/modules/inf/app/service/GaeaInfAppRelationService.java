package com.anjiplus.template.gaea.business.modules.inf.app.service;

import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anjiplus.template.gaea.business.modules.inf.app.controller.dto.GaeaInfAppRelationDTO;
import com.anjiplus.template.gaea.business.modules.inf.app.controller.param.GaeaInfAppRelationParam;
import com.anjiplus.template.gaea.business.modules.inf.app.dao.entity.GaeaInfAppRelation;

import java.util.List;

/**
 * 接口应用关系表(GaeaInfAppRelation)Service
 *
 * @author ultrajiaming
 * @since 2021-04-08 17:37:08
 */
public interface GaeaInfAppRelationService extends GaeaBaseService<GaeaInfAppRelationParam, GaeaInfAppRelation> {

    /**
     * 查询应用接口
     **/
    List<GaeaInfAppRelationDTO> listInfApp(GaeaInfAppRelationDTO gaeaInfAppRelationDTO);

    /**
     * 修改应用接口调用次数
     **/
    void updateTimes(List<GaeaInfAppRelationDTO> dtoList);
}
