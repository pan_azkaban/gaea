package com.anji.plus.gaea.security.plus.modules.org.controller.param;


import com.anji.plus.gaea.annotation.Query;
import com.anji.plus.gaea.constant.QueryEnum;
import com.anji.plus.gaea.curd.params.PageParam;

import java.io.Serializable;

/**
 * 组织(GaeaOrg)param
 *
 * @author lr
 * @since 2021-02-02 13:37:33
 */
public class GaeaOrgParam extends PageParam implements Serializable {
    /**
     * 机构代码
     */
    @Query(QueryEnum.LIKE)
    private String orgCode;
    /**
     * 机构名称
     */
    @Query(QueryEnum.LIKE)
    private String orgName;
    /**
     * 上级组织code
     */
    private String orgParentCode;

    /**
     * 外部机构代码（从外系统同步过来得编码）
     */
    private String outOrgCode;
    /**
     * 外部机构父级编码（从外系统同步过来得父级编码）
     */
    private String outOrgParentCode;

    /**
     * 组织类型
     */
    private String orgType;
    /**
     * 联系人
     */
    private String linkman;

    /**
     * 0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG
     */
    private Integer enabled;

    /**
     * 左边树code
     */
    @Query(where = false)
    private String code;

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgParentCode() {
        return orgParentCode;
    }

    public void setOrgParentCode(String orgParentCode) {
        this.orgParentCode = orgParentCode;
    }

    public String getOutOrgCode() {
        return outOrgCode;
    }

    public void setOutOrgCode(String outOrgCode) {
        this.outOrgCode = outOrgCode;
    }

    public String getOutOrgParentCode() {
        return outOrgParentCode;
    }

    public void setOutOrgParentCode(String outOrgParentCode) {
        this.outOrgParentCode = outOrgParentCode;
    }

    public String getOrgType() {
        return orgType;
    }

    public void setOrgType(String orgType) {
        this.orgType = orgType;
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
