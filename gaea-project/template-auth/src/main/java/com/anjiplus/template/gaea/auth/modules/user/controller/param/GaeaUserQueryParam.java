package com.anjiplus.template.gaea.auth.modules.user.controller.param;

import com.anjiplus.template.gaea.common.dto.BaseQueryBO;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 功能描述：
 * 高级查询
 * @Author: peiyanni
 * @Date: 2021/3/9 13:08
 */
@Getter
@Setter
public class GaeaUserQueryParam extends BaseQueryBO implements Serializable {
    private String username;

}
