import request from '@/utils/request'

export function pageList(params) {
  return request({
    url: '/business/inf/pageList',
    method: 'get',
    params,
  })
}
export function businessApplication(params) {
  return request({
    url: '/business/inf/edit',
    method: 'post',
    data: params,
  })
}
export function addApplication(params) {
  return request({
    url: '/business/inf/add',
    method: 'post',
    data: params,
  })
}
export function editApplication(params) {
  return request({
    url: '/business/inf/edit',
    method: 'post',
    data: params,
  })
}
export function deleteApplication(id) {
  return request({
    url: '/business/inf/' + id,
    method: 'delete',
  })
}
export function online(id) {
  // 上线
  return request({
    url: '/business/inf/online/' + id,
    method: 'get',
  })
}
export function offline(id) {
  // 下线
  return request({
    url: '/business/inf/offline/' + id,
    method: 'get',
  })
}
export function audit(id) {
  // 下线
  return request({
    url: '/business/inf/examine/' + id,
    method: 'get',
  })
}
export function test(params) {
  // 测试
  return request({
    url: '/business/custom/dynamic/run/test',
    method: 'post',
    data: params,
  })
}

export function testDemo(params) {
  // 编辑页面接口调试
  return request({
    url: '/business/custom/dynamic/testDemo',
    method: 'post',
    data: params,
  })
}
export function authorization(params) {
  // 测试
  return request({
    url: '/business/inf/authorize/app/',
    method: 'post',
    data: params,
  })
}
export function queryPowerFun() {
  // 授权获取列表
  return request({
    url: '/business/inf/app/select/dropDown',
    method: 'get',
  })
}
export function submitPowerFun(id, params) {
  // 测试
  return request({
    url: '/business/inf/authorize/app/' + id,
    method: 'post',
    data: params,
  })
}
export function interfaceDetail(id) {
  // 接口详情
  return request({
    url: '/business/inf/id/' + id,
    method: 'get',
  })
}
export function getRelationListInfApp(params) {
  // 获取授权
  return request({
    url: '/business/inf/app/relation/listInfApp',
    method: 'post',
    data: params,
  })
}
