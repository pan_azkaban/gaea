package com.anji.plus.gaea.test;

import com.anji.plus.gaea.exception.BusinessException;
import com.anji.plus.gaea.exception.BusinessExceptionBuilder;

/**
 * @author WongBin
 * @date 2021/11/12
 */
public class ExceptionBuilderTest {

    public static void main(String[] args) {
        BusinessException ex = BusinessExceptionBuilder.build("1234");
        System.out.println(ex.getArgs());

        BusinessExceptionBuilder.build("12343","12323");
        BusinessExceptionBuilder.build("12343","12323","12323232");
    }
}
