package com.anjiplus.template.gaea.auth.modules.gaeaannot.dao;

import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.dao.entity.GaeaAnnouncement;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统公告信息表(GaeaAnnouncement)Mapper
 *
 * @author makejava
 * @since 2021-03-29 15:17:20
 */
@Mapper
public interface GaeaAnnouncementMapper extends GaeaBaseMapper<GaeaAnnouncement> {


}