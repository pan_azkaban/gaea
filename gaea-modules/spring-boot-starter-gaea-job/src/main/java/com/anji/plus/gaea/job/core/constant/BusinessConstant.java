package com.anji.plus.gaea.job.core.constant;
/**
 * @desc 常量
 * @author 木子李*de
 * @date 2023-05-12 15:25:35.961
 **/
public interface BusinessConstant {

    public static final int BEAT_TIMEOUT = 30;
    public static final int DEAD_TIMEOUT = BEAT_TIMEOUT * 3;

}
