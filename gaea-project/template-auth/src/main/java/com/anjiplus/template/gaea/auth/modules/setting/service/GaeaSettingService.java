package com.anjiplus.template.gaea.auth.modules.setting.service;

import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anjiplus.template.gaea.auth.modules.setting.controller.param.GaeaSettingParam;
import com.anjiplus.template.gaea.auth.modules.setting.entity.GaeaSetting;

/**
 * (GaeaSetting)Service
 *
 * @author makejava
 * @since 2021-02-05 16:58:53
 */
public interface GaeaSettingService extends GaeaBaseService<GaeaSettingParam, GaeaSetting> {

}
