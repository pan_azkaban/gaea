-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: 10.108.6.76    Database: otwb-meta
-- ------------------------------------------------------
-- Server version	5.7.29-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '92008da6-6cec-11ea-b31b-005056a0baac:1-439179,
d5d452f0-d0b4-11ea-a434-005056a4e37a:1-76113805';

--
-- Table structure for table `meta_job_info`
--

DROP TABLE IF EXISTS `meta_job_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `meta_job_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tenant_code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '租户编码，为空时表示系统级',
  `org_code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '机构编码，为空时表示系统级',
  `executor_code` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '执行器编码，取自执行器springboot的spring.application.name',
  `job_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '任务名称',
  `job_type` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '任务类型，BEAN/SQL',
  `job_handler` varchar(96) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'jobType=BEAN时，存放jobHandler的beanName',
  `job_source` mediumtext COLLATE utf8_unicode_ci COMMENT 'jobType=SQL时，存放sql',
  `job_source_update_time` datetime DEFAULT NULL COMMENT 'jobSource更新时间',
  `schedule_type` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NONE' COMMENT '任务--调度类型，NONE/CRON/FIX_RATE固定速度',
  `schedule_conf` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '任务--调度配置，时间表达式/固定速度',
  `job_misfire_strategy` varchar(56) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'DO_NOTHING' COMMENT '任务--调度过期策略，DO_NOTHING忽略/FIRE_ONCE_NOW立即执行一次',
  `job_route_strategy` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'FIRST' COMMENT '任务--路由策略，FIRST第一个/LAST最后一个/ROUND轮询/RANDOM随机/CONSISTENT_HASH一致性HASH/LEAST_FREQUENTLY_USED最不经常使用/LEAST_RECENTLY_USED最近最久未使用/FAILOVER故障转移/BUSYOVER忙碌转移',
  `job_block_strategy` varchar(56) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'DISCARD_LATER' COMMENT '任务--阻塞处理策略，SERIAL_EXECUTION单机串行/DISCARD_LATER丢弃后续调度/COVER_EARLY覆盖之前调度',
  `job_param` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '任务--执行器任务参数，Json字符串[{"key":"","value":"","desc":""}]',
  `job_batch` int(11) NOT NULL DEFAULT '100' COMMENT '在启用getParamList、getTotalCount下，分页处理页大小',
  `job_timeout` int(11) NOT NULL DEFAULT '0' COMMENT '任务--执行超时时间，单位秒',
  `job_fail_retry_count` int(11) NOT NULL DEFAULT '0' COMMENT '任务--失败重试次数',
  `trigger_status` int(11) NOT NULL COMMENT '任务--启用状态：0-禁用，1-启用',
  `trigger_last_time` bigint(20) NOT NULL COMMENT '上次调度时间，从1970年1月1日开始计算的总毫秒数',
  `trigger_next_time` bigint(20) NOT NULL COMMENT '下次调度时间，从1970年1月1日开始计算的总毫秒数',
  `child_job_ids` varchar(254) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '子任务ID，多个逗号分隔',
  `built_in` tinyint(1) NOT NULL DEFAULT '0' COMMENT '系统内置，YES_NO',
  `create_by` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '创建人',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '修改人',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `version` int(11) NOT NULL DEFAULT '1' COMMENT '版本号',
  PRIMARY KEY (`id`),
  KEY `meta_job_info_trigger_status_IDX` (`trigger_status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='定时任务表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `meta_job_log`
--

DROP TABLE IF EXISTS `meta_job_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `meta_job_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `job_id` bigint(20) NOT NULL COMMENT '定时任务ID',
  `tenant_code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '租户编码，为空时表示系统级',
  `org_code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '机构编码，为空时表示系统级',
  `executor_id` bigint(20) DEFAULT NULL COMMENT '执行器ID',
  `executor_code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '执行器编码，取自执行器springboot的spring.application.name',
  `executor_address` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '执行器地址，本次执行的地址',
  `job_handler` varchar(96) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'jobType=BEAN时，存放jobHandler的beanName',
  `job_param` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '任务--执行器任务参数',
  `job_fail_retry_count` int(11) NOT NULL DEFAULT '0' COMMENT '任务--失败重试次数',
  `trigger_time` datetime DEFAULT NULL COMMENT '调度-时间',
  `trigger_status` int(11) NOT NULL DEFAULT '0' COMMENT '调度-结果，0未处理，200成功，500失败',
  `trigger_msg` text COLLATE utf8_unicode_ci COMMENT '调度-日志',
  `executor_time` datetime DEFAULT NULL COMMENT '执行-时间',
  `executor_status` int(11) NOT NULL DEFAULT '0' COMMENT '执行-状态，0未处理，200成功，500失败',
  `executor_msg` text COLLATE utf8_unicode_ci COMMENT '执行-日志',
  `time_cost` bigint(20) DEFAULT '0' COMMENT '接口耗时毫秒(RT)',
  PRIMARY KEY (`id`),
  KEY `IDX1` (`executor_code`,`trigger_status`,`executor_status`,`trigger_time`) USING BTREE,
  KEY `IDX2` (`job_id`,`trigger_time`,`executor_status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='定时任务日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `meta_job_executor`
--

DROP TABLE IF EXISTS `meta_job_executor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `meta_job_executor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `executor_code` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '执行器编码，取自执行器springboot的spring.application.name',
  `executor_address` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT '执行器地址，http://ip:port/contextpath/',
  `registory_time` datetime DEFAULT NULL COMMENT '注册心跳时间，yyyy-MM-dd HH:mm:ss',
  `expire_timemillis` bigint(20) DEFAULT NULL COMMENT '心跳过期时间，从1970年1月1日开始计算的总毫秒数',
  `available` int(11) NOT NULL DEFAULT '0' COMMENT '可用状态，1--有效期 0--注册心跳60秒超时过期',
  PRIMARY KEY (`id`),
  UNIQUE KEY `meta_job_executor_pk` (`executor_code`,`executor_address`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='执行器注册表';
/*!40101 SET character_set_client = @saved_cs_client */;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-06-10 12:48:59
