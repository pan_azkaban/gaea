<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${package}.dao.${className}Mapper">

    <resultMap type="${package}.dao.entity.${className}" id="${className}Map">
        <!--jdbcType="{column.columnType}"-->
        <#if columns??>
          <#list columns as column>
        <result property="${column.changeColumnName}" column="${column.columnName}"  />
          </#list>
        </#if>

    </resultMap>

    <sql id="Base_Column_List">
        <#list columns as c><#if c_has_next>${c.columnName},<#else>${c.columnName}</#if></#list>
    </sql>

    <!--自定义sql -->

</mapper>