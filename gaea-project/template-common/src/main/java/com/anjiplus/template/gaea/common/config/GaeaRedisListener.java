package com.anjiplus.template.gaea.common.config;

import java.util.Map;

/**
 * 功能描述：
 * 定义redis监听
 * @Author: peiyanni
 * @Date: 2021/3/30 16:31
 */
public interface GaeaRedisListener {

    void onMessage(Map<String, String> message);
}

