package com.anji.plus.gaea.job.trigger.dao.entity;

import com.anji.plus.gaea.job.trigger.dao.BaseRowMapper;
import org.springframework.jdbc.core.RowMapper;

import java.util.Date;
/**
 * 定时任务日志表 entity
 * @author 木子李*de
 * @date 2023-05-18 09:50:46.038
 **/
public class MetaJobLog {

    /** 定时任务ID */
    private Long id;

    /** 定时任务ID */
    private Long jobId;

    /** 租户编码，为空时表示系统级 */
    private String tenantCode;

    /** 机构编码，为空时表示系统级 */
    private String orgCode;

    /** 执行器编码，取自执行器springboot的spring.application.name */
    private String executorCode;

    /** 执行器ID */
    private Long executorId;

    /** 执行器地址，本次执行的地址 */
    private String executorAddress;

    /** jobType=BEAN时，存放jobHandler的beanName */
    private String jobHandler;

    /** 任务--执行器任务参数 */
    private String jobParam;

    /** 任务--失败重试次数 */
    private Integer jobFailRetryCount;

    /** 调度-时间 */
    private Date triggerTime;

    /** 调度-结果，0未处理，200成功，500失败 */
    private Integer triggerStatus;

    /** 调度-日志 */
    private String triggerMsg;

    /** 执行-时间 */
    private Date executorTime;

    /** 执行-状态，0未处理，200成功，500失败 */
    private Integer executorStatus;

    /** 执行-日志 */
    private String executorMsg;

    /** 接口耗时毫秒(RT) */
    private Long timeCost;
    //*********************** getter and setter *************************


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getExecutorCode() {
        return executorCode;
    }

    public void setExecutorCode(String executorCode) {
        this.executorCode = executorCode;
    }

    public Long getExecutorId() {
        return executorId;
    }

    public void setExecutorId(Long executorId) {
        this.executorId = executorId;
    }

    public String getExecutorAddress() {
        return executorAddress;
    }

    public void setExecutorAddress(String executorAddress) {
        this.executorAddress = executorAddress;
    }

    public String getJobHandler() {
        return jobHandler;
    }

    public void setJobHandler(String jobHandler) {
        this.jobHandler = jobHandler;
    }

    public String getJobParam() {
        return jobParam;
    }

    public void setJobParam(String jobParam) {
        this.jobParam = jobParam;
    }

    public Integer getJobFailRetryCount() {
        return jobFailRetryCount;
    }

    public void setJobFailRetryCount(Integer jobFailRetryCount) {
        this.jobFailRetryCount = jobFailRetryCount;
    }

    public Date getTriggerTime() {
        return triggerTime;
    }

    public void setTriggerTime(Date triggerTime) {
        this.triggerTime = triggerTime;
    }

    public Integer getTriggerStatus() {
        return triggerStatus;
    }

    public void setTriggerStatus(Integer triggerStatus) {
        this.triggerStatus = triggerStatus;
    }

    public String getTriggerMsg() {
        return triggerMsg;
    }

    public void setTriggerMsg(String triggerMsg) {
        this.triggerMsg = triggerMsg;
    }

    public Date getExecutorTime() {
        return executorTime;
    }

    public void setExecutorTime(Date executorTime) {
        this.executorTime = executorTime;
    }

    public Integer getExecutorStatus() {
        return executorStatus;
    }

    public void setExecutorStatus(Integer executorStatus) {
        this.executorStatus = executorStatus;
    }

    public String getExecutorMsg() {
        return executorMsg;
    }

    public void setExecutorMsg(String executorMsg) {
        this.executorMsg = executorMsg;
    }

    public Long getTimeCost() {
        return timeCost;
    }

    public void setTimeCost(Long timeCost) {
        this.timeCost = timeCost;
    }

    //*********************** getRowMapper *************************
    public static final RowMapper<MetaJobLog> RowMapper = new BaseRowMapper(MetaJobLog.class);
}
