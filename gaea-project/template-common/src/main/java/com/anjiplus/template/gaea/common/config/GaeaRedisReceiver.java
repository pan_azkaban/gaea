package com.anjiplus.template.gaea.common.config;

import com.anji.plus.gaea.utils.ApplicationContextUtils;
import com.anjiplus.template.gaea.common.MagicValueConstants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

/**
 * 功能描述：
 * redis 订阅者
 *
 * @Author: peiyanni
 * @Date: 2021/3/30 16:20
 */
@Configuration
@Slf4j
public class GaeaRedisReceiver {

    /**
     * 接受消息并调用业务逻辑处理器
     *
     * @param map
     */
    public void onMessage(Map<String, String> map) {
        String handlerName = map.get(MagicValueConstants.HANDLER_NAME);
        GaeaRedisListener gaeaRedisListener = null;
        try {
            if (StringUtils.isNotBlank(handlerName)) {
                gaeaRedisListener = ApplicationContextUtils.getBean(handlerName, GaeaRedisListener.class);
            } else {
                gaeaRedisListener = ApplicationContextUtils.getBean(GaeaRedisListener.class);
            }
            if (null != gaeaRedisListener) {
                gaeaRedisListener.onMessage(map);
            }
        }catch (Exception e){
            log.info("redis listener getbean not foud");
        }

    }
}
