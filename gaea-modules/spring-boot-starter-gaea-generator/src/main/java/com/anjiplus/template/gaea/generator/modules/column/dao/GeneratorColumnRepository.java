/*
 *  Copyright 2019-2020 Zheng Jie
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.anjiplus.template.gaea.generator.modules.column.dao;

import com.anjiplus.template.gaea.generator.modules.column.dao.GeneratorColumn;
import org.apache.ibatis.annotations.Mapper;

import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;

/**
 * @author Zheng Jie
 * @date 2019-01-14
 */
@Mapper
public interface GeneratorColumnRepository extends GaeaBaseMapper<GeneratorColumn> {

    /**
     * 查询表信息
     * @param tableName 表格名
     * @return 表信息
     */
    //List<ColumnInfo> findByTableNameOrderByIdAsc(String tableName);
}
