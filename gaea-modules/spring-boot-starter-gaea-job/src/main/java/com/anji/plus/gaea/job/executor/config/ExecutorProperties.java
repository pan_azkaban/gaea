package com.anji.plus.gaea.job.executor.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @desc 定时任务--执行器配置文件
 * @author 木子李*De
 * @date 2023-05-12 15:25:35.961
 **/
@ConfigurationProperties(prefix = "spring.gaea.subscribes.executor")
public class ExecutorProperties {

    /** 默认激活job执行器组件 */
    private boolean enabled = true;

    /** 调度器地址，http://ip:port/contextpath/ */
    private String triggerAddresses;

    /** 调度器的访问令牌 */
    private String accessToken="AD6319843DDC5D6EDE5384858232BAF2";

    /** 执行器所在的springboot的application.name */
    private String executorCode;

    /** 执行器地址，http://ip:port/contextpath/ */
    private String executorAddress;

    /** 日志保留目录 */
    private String logHome="./logs/job-executor/";

    /** 日志保留天数 */
    private int logMaxDays = 30;

    /** 执行器注册心路间隔 */
    private int beatTimeoutSeconds = 30;

    /** 执行器注册心路失效时间 */
    private int beatDeadSeconds = 30 * 3;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getTriggerAddresses() {
        return triggerAddresses;
    }

    public void setTriggerAddresses(String triggerAddresses) {
        this.triggerAddresses = triggerAddresses;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getExecutorCode() {
        return executorCode;
    }

    public void setExecutorCode(String executorCode) {
        this.executorCode = executorCode;
    }

    public String getExecutorAddress() {
        return executorAddress;
    }

    public void setExecutorAddress(String executorAddress) {
        this.executorAddress = executorAddress;
    }

    public String getLogHome() {
        return logHome;
    }

    public void setLogHome(String logHome) {
        this.logHome = logHome;
    }

    public int getLogMaxDays() {
        return logMaxDays;
    }

    public void setLogMaxDays(int logMaxDays) {
        this.logMaxDays = logMaxDays;
    }

    public int getBeatTimeoutSeconds() {
        return beatTimeoutSeconds;
    }

    public void setBeatTimeoutSeconds(int beatTimeoutSeconds) {
        this.beatTimeoutSeconds = beatTimeoutSeconds;
    }

    public int getBeatDeadSeconds() {
        return beatDeadSeconds;
    }

    public void setBeatDeadSeconds(int beatDeadSeconds) {
        this.beatDeadSeconds = beatDeadSeconds;
    }
}
