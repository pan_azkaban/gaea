package com.anjiplus.template.gaea.business.modules.devicemodel.dao;

import org.apache.ibatis.annotations.Mapper;

import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import com.anjiplus.template.gaea.business.modules.devicemodel.dao.entity.DeviceModel;

/**
* DeviceModel Mapper
* @author dev-user
* @date 2021-04-07 17:27:35.130
**/
@Mapper
public interface DeviceModelMapper extends GaeaBaseMapper<DeviceModel> {

}