# 工作流

## 介绍
工作流组件**spring-boot-starter-gaea-flowable**需要结合盖亚底盘一起使用，采用flowable工作流框架。
主要提供工作流在线定制、动态表单定制、表单渲染、流程发起、审批等功能。

## 配置
#### 1.pom.xml中加入依赖如下
```xml
 <dependency>
        <groupId>com.anji-plus</groupId>
        <artifactId>spring-boot-gaea</artifactId>
 </dependency>

<dependency>
        <groupId>com.anji-plus</groupId>
        <artifactId>spring-boot-starter-gaea-flowable</artifactId>
 </dependency>
```

#### 2.添加配置如下
```yaml
spring:
  gaea:
    subscribes:
      flowable:
        enabled: true
```

*注：flowable 是工作流组件的名称，enabled：true表示启用工作流组件*

## 功能说明
#### 1.定制表单
![输入图片说明](https://images.gitee.com/uploads/images/2021/0407/141855_74597e47_477348.png "form1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0407/141450_5a793a3c_477348.png "flowable-form.png")
#### 2.绘制流程图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0407/141922_85800f7b_477348.png "flow.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0407/141938_404c53d3_477348.png "flow2.png")
#### 3.部署流程
![输入图片说明](https://images.gitee.com/uploads/images/2021/0407/142223_fe9eebd9_477348.png "在这里输入图片标题")
#### 4.发起流程
![输入图片说明](https://images.gitee.com/uploads/images/2021/0407/142305_3e11bd58_477348.png "start.png")
#### 5.审批流程
![输入图片说明](https://images.gitee.com/uploads/images/2021/0407/142323_f61af61c_477348.png "start1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0407/142335_398a94f9_477348.png "start2.png")

## 代码示例
工作流在线定制、动态表单定制属于组件内置功能，引入组件即可使用。

表单数据渲染、流程发起、审批等功能示例代码如下：
#### 1.获取定制的表单元数据，用于前端form-create表单框架进行表单渲染
```java
   /**
    * 获取表单数据
    *
    * @param processDefinitionId 流程定义ID
    * @return
    */
  public Model getFormModel(String processDefinitionId) {
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionId(processDefinitionId).latestVersion().singleResult();

        Model modelRes = modelRepository.findByKeyAndType(processDefinition.getKey(), 0).get(0);
        Model model = modelService.getModel(modelRes.getId());

        byte[] bytes = modelService.getBpmnXML(model);
        if (bytes == null) {
            throw BusinessExceptionBuilder.build("model bytes not exist");
        }

        BpmnModel bpmnModel = modelService.getBpmnModel(model);

        if (bpmnModel.getProcesses().size() == 0) {
            throw BusinessExceptionBuilder.build("bpmnModel.not.exist");
        }

        Process process = bpmnModel.getMainProcess();
        Collection<FlowElement> flowElements = process.getFlowElements();

        String formKey = null;
        for (FlowElement flowElement : flowElements) {

            if (flowElement instanceof StartEvent) {
                //位于开始节点上表单元素
                StartEvent startEvent = (StartEvent) flowElement;
                if (startEvent.getFormKey() != null && startEvent.getFormKey().length() > 0) {
                    formKey = startEvent.getFormKey();
                }
            } else if (flowElement instanceof UserTask) {
                //位于任务节点上表单元素
                UserTask userTask = (UserTask) flowElement;
                if (userTask.getFormKey() != null && userTask.getFormKey().length() > 0) {
                    formKey = userTask.getFormKey();
                }
            }
        }
        return modelRepository.findByKeyAndType(formKey, 2).get(0);
    }
```

#### 2.流程发起
```java
    /**
     * 启动流程
     *
     * @param processDefinitionId 流程定义ID
     * @param businessKey 流程关联的业务Key
     * @param map 前端填写的表单数据
     */
   public void startProcess(String processDefinitionId, String businessKey, Map<String,Object> map) {

        //生成唯一任务编号
        String processNo = GaeaUtils.formatDate(new Date(), DATE_PATTERN) + cacheHelper.increment(FLOWABLE_TASK_KEY);

        if(CollectionUtils.isEmpty(map)) {
            map = new HashMap<>(2);
        }

        map.put(PROCESS_NO, processNo);
        //设置发起人
        Authentication.setAuthenticatedUserId(UserContentHolder.getContext().getUsername());
        //启动流程
        runtimeService.startProcessInstanceById(processDefinitionId, businessKey, map);
    }
```

#### 3.流程审批
```java
   /**
     * 完成任务
     *
     * @param taskId 任务ID
     * @param requestBody 表单数据
     */
    public void completeTask(String taskId, Map<String, Object> requestBody) {
        String processInstanceId = taskService.createTaskQuery().taskId(taskId).singleResult().getProcessInstanceId();

        //添加当前处理的用户
        Authentication.setAuthenticatedUserId(UserContentHolder.getContext().getUsername());
        //添加备注
        taskService.addComment(taskId, processInstanceId, requestBody.get("comment").toString());

        //完成任务
        taskService.complete(taskId, requestBody);
    }
```
