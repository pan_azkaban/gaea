package com.anji.plus.gaea.job.executor.handler.impl;

import com.anji.plus.gaea.job.executor.handler.JobHandler;
import com.anji.plus.gaea.job.executor.handler.param.JobInfo;
import com.anji.plus.gaea.job.executor.util.XxlJobHelper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class SQLJobHandler implements JobHandler<Object, Object> {

    private String jobSource;

    private long jobSourceUpdateTime;

    private JdbcTemplate jdbcTemplate;

    public SQLJobHandler(JdbcTemplate jdbcTemplate, String jobSource, long jobSourceUpdateTime) {
        this.jobSourceUpdateTime = jobSourceUpdateTime;
        this.jobSource = jobSource;
        this.jdbcTemplate = jdbcTemplate;
    }

    public long getJobSourceUpdateTime() {
        return jobSourceUpdateTime;
    }

    @Override
    public void execute(JobInfo jobInfo, Object o, List<Object> pageList) throws Exception {
        XxlJobHelper.log("----------- SQL.source:"+ jobSource +" -----------");
        XxlJobHelper.log("----------- SQL.version:"+ jobSourceUpdateTime +" -----------");
        try{
            jdbcTemplate.execute(jobSource);
        }catch (Exception e){
            XxlJobHelper.log("SQLJobHandler execute sql:{} error:{}", jobSource, e);
        }
    }

}
