package com.anjiplus.template.gaea.business.modules.inf.event;

import com.anji.plus.gaea.inf.event.InfHistoryApplicationEvent;
import com.anji.plus.gaea.inf.event.InfHistoryParam;
import com.anjiplus.template.gaea.business.modules.inf.inf.dao.entity.InfHistoryEntity;
import com.anjiplus.template.gaea.business.modules.inf.inf.service.InfHistoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * @ClassName InfHistoryApplicationListener
 * @Description: l历史记录保存监听器
 * @Author dingkaiqiang
 * @Date 2021-03-26
 * @Version V1.0
 **/

@Slf4j
@Component
public class InfHistoryApplicationListener implements ApplicationListener<InfHistoryApplicationEvent> {


    @Autowired
    private InfHistoryService infHistoryService;
    @Override
    public void onApplicationEvent(InfHistoryApplicationEvent infHistoryApplicationEvent) {
        try {

            InfHistoryParam infHistoryParam = infHistoryApplicationEvent.getInfHistoryParam();
            InfHistoryEntity infHistoryEntity = new InfHistoryEntity();
            // SQL执行历史记录
            BeanUtils.copyProperties(infHistoryParam, infHistoryEntity);
            boolean save = infHistoryService.save(infHistoryEntity);
            log.info("SQL执行日志写入完成，执行结果：{}，入参：{}", save, infHistoryEntity);
        } catch (Exception e) {
//            e.printStackTrace();
            log.error("写入sql历史记录失败，{}", e.getMessage());
        }
    }
}
