package com.anjiplus.template.gaea.business.test;

import com.anjiplus.template.gaea.business.BusinessApplication;
import com.anjiplus.template.gaea.business.modules.data.dataSet.controller.dto.DataSetDto;
import com.anjiplus.template.gaea.business.modules.data.dataSet.controller.dto.OriginalDataDto;
import com.anjiplus.template.gaea.business.modules.data.dataSet.service.DataSetService;
import com.anjiplus.template.gaea.business.modules.data.dataSetParam.controller.dto.DataSetParamDto;
import com.anjiplus.template.gaea.business.modules.data.dataSetTransform.controller.dto.DataSetTransformDto;
import com.anjiplus.template.gaea.business.modules.data.reportexcel.controller.dto.ReportExcelDto;
import com.anjiplus.template.gaea.business.modules.data.reportexcel.service.ReportExcelService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by raodeming on 2021/3/22.
 */
@SpringBootTest(classes = BusinessApplication.class)
@RunWith(SpringRunner.class)
public class DataSetServiceImplTest {

    @Autowired
    private DataSetService dataSetService;

    @Autowired
    private ReportExcelService reportExcelService;

    @Test
    public void insertSet() {

        DataSetDto dataSetDto = new DataSetDto();
        dataSetDto.setSetCode("11");
        dataSetDto.setSetName("数据集1");
        dataSetService.insertSet(dataSetDto);
    }

    //deviceInfo  analysis-wifilogin
    //select org_code,count(org_code) AS countNum from t_device_info where create_time > '${createTime}' group by org_code ORDER BY countNum desc
    //{"query": "select HISTOGRAM(logTime,INTERVAL 1 MONTH) as h ,count(flag),flag from \"analysis-wifilogin\" where  logTime>='${startTime}' and logTime<'${endTime}' GROUP BY h,flag"}
    @Test
    public void transform(){
        DataSetDto dto = new DataSetDto();
        dto.setSetCode("deviceInfo");
        Map<String, Object> contextData = new HashMap<>();
        contextData.put("createTime", "2020-02-22 00:28:10");
        dto.setContextData(contextData);
        OriginalDataDto data = dataSetService.getData(dto);
        System.out.println(data.toString());
    }


    @Test
    public void testTransform(){
        DataSetDto dto = new DataSetDto();
        dto.setSourceCode("mysql_131");
        dto.setDynSentence("select org_code,count(org_code) AS countNum from t_device_info where create_time > '${createTime}' group by org_code ORDER BY countNum desc");
        /** 请求参数集合 */
        List<DataSetParamDto> dataSetParamDtoList = new ArrayList<>();
        DataSetParamDto dataSetParamDto = new DataSetParamDto();
        dataSetParamDto.setParamName("createTime");
        dataSetParamDto.setSampleItem("2020-02-22 00:28:10");
        //分页参数
        DataSetParamDto dataSetParamDto1 = new DataSetParamDto();
        dataSetParamDto1.setParamName("pageNumber");
        dataSetParamDto1.setSampleItem("1");
        DataSetParamDto dataSetParamDto2 = new DataSetParamDto();
        dataSetParamDto2.setParamName("pageSize");
        dataSetParamDto2.setSampleItem("2");
        dataSetParamDtoList.add(dataSetParamDto1);
        dataSetParamDtoList.add(dataSetParamDto2);

        dataSetParamDtoList.add(dataSetParamDto);
        /** 数据转换集合 */
        List<DataSetTransformDto> dataSetTransformDtoList = new ArrayList<>();

        dto.setDataSetParamDtoList(dataSetParamDtoList);
        dto.setDataSetTransformDtoList(dataSetTransformDtoList);
        OriginalDataDto originalDataDto = dataSetService.testTransform(dto);
        System.out.println(originalDataDto);
    }

    @Test
    public void pdf(){
        ReportExcelDto reportExcelDto = new ReportExcelDto();
        reportExcelDto.setReportType("gaea_template_pdf");
        reportExcelDto.setReportCode("gaea_excel_user");
        reportExcelDto.setSetParam("{\"gaea_user_info\":{\"username\":\"admin\"}}");
        reportExcelService.exportExcel(reportExcelDto);

    }
}
