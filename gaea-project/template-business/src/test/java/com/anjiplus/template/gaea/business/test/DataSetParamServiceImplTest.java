package com.anjiplus.template.gaea.business.test;

import com.alibaba.fastjson.JSONObject;
import com.anjiplus.template.gaea.business.modules.data.dataSetParam.controller.dto.DataSetParamDto;
import com.anjiplus.template.gaea.business.modules.data.dataSetParam.service.impl.DataSetParamServiceImpl;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.List;
import java.util.Set;

/**
 * Created by raodeming on 2021/3/23.
 */
//@SpringBootTest(classes = BusinessApplication.class)
//@RunWith(SpringRunner.class)
public class DataSetParamServiceImplTest {

    @Test
    public void test1(){
        DataSetParamServiceImpl impl = new DataSetParamServiceImpl();
        DataSetParamDto dataSetParamDto = new DataSetParamDto();
        String validationRules = "function verification(sampleItem){\n" +
//                "if(sampleItem ==  'aaaa1'){\n" +
//                "return true;;\n" +
//                "}\n" +
                "return sampleItem == 'aaaa';}";
        dataSetParamDto.setValidationRules(validationRules);
        dataSetParamDto.setSampleItem("aaaa");
        boolean verification = impl.verification(dataSetParamDto);
        System.out.println(verification);
    }


    @Test
    public void test2(){
        String transformScript = "{\n" +
                "    \"countNum\": {\n" +
                "        \"118\": 2,\n" +
                "        \"0\": \"女\"\n" +
                "    },\n" +
                "    \"org_code\": {\n" +
                "        \"2000001\": \"组织1\",\n" +
                "        \"3000115\": \"组织2\"\n" +
                "    }\n" +
                "}";

        String da = "[{\"countNum\":118,\"org_code\":\"2000001\"}, {\"countNum\":10,\"org_code\":\"3000115\"}, {\"countNum\":10,\"org_code\":\"700001\"}, {\"countNum\":9,\"org_code\":\"3000116\"}, {\"countNum\":5,\"org_code\":\"3000113\"}, {\"countNum\":3,\"org_code\":\"3000004\"}, {\"countNum\":3,\"org_code\":\"3000114\"}, {\"countNum\":2,\"org_code\":\"3000112\"}, {\"countNum\":1,\"org_code\":\"2000005\"}, {\"countNum\":1,\"org_code\":\"3000006\"}]";
        List<JSONObject> data = JSONObject.parseArray(da, JSONObject.class);
        JSONObject jsonObject = JSONObject.parseObject(transformScript);
        Set<String> keys = jsonObject.keySet();

        data.forEach(dataDetail -> {
            dataDetail.forEach((key, value) -> {
                if (keys.contains(key)) {
                    String string = jsonObject.getJSONObject(key).getString(value.toString());
                    if (StringUtils.isNotBlank(string)) {
                        dataDetail.put(key, string);
                    }
                }

            });

        });

        System.out.println(JSONObject.toJSONString(data));

    }




}