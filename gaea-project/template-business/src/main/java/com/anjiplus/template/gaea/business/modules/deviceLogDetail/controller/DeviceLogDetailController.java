
package com.anjiplus.template.gaea.business.modules.devicelogdetail.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.anji.plus.gaea.annotation.AccessKey;
import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.curd.controller.GaeaBaseController;
import com.anji.plus.gaea.curd.service.GaeaBaseService;

import com.anji.plus.gaea.utils.GaeaBeanUtils;
import com.anji.plus.gaea.utils.GaeaUtils;

import com.anjiplus.template.gaea.business.modules.devicelogdetail.dao.entity.DeviceLogDetail;
import com.anjiplus.template.gaea.business.modules.devicelogdetail.service.DeviceLogDetailService;
import com.anjiplus.template.gaea.business.modules.devicelogdetail.controller.dto.DeviceLogDetailDto;
import com.anjiplus.template.gaea.business.modules.devicelogdetail.controller.param.DeviceLogDetailParam;

import io.swagger.annotations.Api;

/**
* @desc 设备日志明细 controller
* @website https://gitee.com/anji-plus/gaea
* @author dev-user
* @date 2021-04-07 17:06:42.920
**/
@RestController
@Api(tags = "设备日志明细管理")
@RequestMapping("/deviceLogDetail")
public class DeviceLogDetailController extends GaeaBaseController<DeviceLogDetailParam, DeviceLogDetail, DeviceLogDetailDto> {

    @Autowired
    private DeviceLogDetailService deviceLogDetailService;

    @Override
    public GaeaBaseService<DeviceLogDetailParam, DeviceLogDetail> getService() {
        return deviceLogDetailService;
    }

    @Override
    public DeviceLogDetail getEntity() {
        return new DeviceLogDetail();
    }

    @Override
    public DeviceLogDetailDto getDTO() {
        return new DeviceLogDetailDto();
    }


    @GetMapping({"/{id}"})
    @AccessKey
    @Override
    public ResponseBean detail(@PathVariable("id") Long id) {
        this.logger.info("{}根据ID查询服务开始，id为：{}", this.getClass().getSimpleName(), id);
        DeviceLogDetail result = deviceLogDetailService.getDetail(id);
        DeviceLogDetailDto dto = this.getDTO();
        GaeaBeanUtils.copyAndFormatter(result, dto);
        ResponseBean responseBean = this.responseSuccessWithData(this.resultDtoHandle(dto));
        this.logger.info("{}根据ID查询结束，结果：{}", this.getClass().getSimpleName(), GaeaUtils.toJSONString(responseBean));
        return responseBean;
    }
}