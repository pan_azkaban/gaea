package com.anjiplus.gaea.oss.test;

import com.anji.plus.gaea.oss.ossbuilder.GaeaOSSTemplate;
import com.anjiplus.gaea.oss.GaeaOSSApplication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
/**
 * OSS组件测试示例
 * @author 木子李·De
 * @since 2022/3/22 14:16
 */
@SpringBootTest(classes = GaeaOSSApplication.class)
class ArchiverApplicationTests {

	@Autowired
	private GaeaOSSTemplate gaeaOSSTemplate;

	@Test
	void upload() {
		//gaeaOSSTemplate.uploadFileByInputStream()
		System.out.println("update finish");
	}

	@Test
	void download() {
		byte[] fileBytes = gaeaOSSTemplate.downloadFile("402b6193-e70e-40a9-bf5b-73a78ea1e8ab.png");
		System.out.println("download finish");
	}

	@Test
	void delete(){
		gaeaOSSTemplate.deleteFile("402b6193-e70e-40a9-bf5b-73a78ea1e8ab.png");
	}
}
