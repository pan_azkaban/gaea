/**/
package ${package}.controller.param;

import lombok.Data;
import java.io.Serializable;
import com.anji.plus.gaea.annotation.Query;
import com.anji.plus.gaea.constant.QueryEnum;
import com.anji.plus.gaea.curd.params.PageParam;

<#if queryHasTimestamp>
import java.sql.Timestamp;
</#if>
<#if queryHasBigDecimal>
import java.math.BigDecimal;
</#if>
<#if betweens??>
import java.util.List;
</#if>
<#if queryColumns??>

</#if>

/**
* @desc ${className} ${apiAlias}查询输入类
* @author ${author}
* @date ${date}
**/
@Data
public class ${className}Param extends PageParam implements Serializable{
<#if queryColumns??>
    <#list queryColumns as column>

<#if column.queryType = '='>
    /** 精确查询 */
    @Query
    private ${column.columnType} ${column.changeColumnName};
</#if>
<#if column.queryType = 'Like'>
    /** 模糊查询 */
    @Query(value = QueryEnum.LIKE)
    private ${column.columnType} ${column.changeColumnName};
</#if>
<#if column.queryType = 'In'>
    /** in */
    @Query(value = QueryEnum.IN)
    private ${column.columnType} ${column.changeColumnName};
</#if>
<#if column.queryType = 'NotNull'>
    /** 不为空 */
    //@Query(value = Query.Type.NOT_NULL)
    private ${column.columnType} ${column.changeColumnName};
</#if>
<#if column.queryType = '>='>
    /** 大于等于 */
    @Query(value = QueryEnum.GT)
    private ${column.columnType} ${column.changeColumnName};
</#if>
<#if column.queryType = '<='>
    /** 小于等于 */
    @Query(value = QueryEnum.LT)
    private ${column.columnType} ${column.changeColumnName};
</#if>
    </#list>
</#if>
<#if betweens??>
    <#list betweens as column>
    /** BETWEEN */
    @Query(value = QueryEnum.BWT)
    private List<${column.columnType}> ${column.changeColumnName};
    </#list>
</#if>
}