package com.anji.plus.gaea.security.plus.modules.user;

import com.anji.plus.gaea.constant.Enabled;
import com.anji.plus.gaea.security.plus.modules.user.dao.entity.GaeaUser;
import com.anji.plus.gaea.security.plus.modules.user.service.GaeaUserService;
import com.anji.plus.gaea.security.security.extension.GaeaUserDetail;
import com.anji.plus.gaea.security.security.extension.UserDetailsServiceHelper;
import com.anji.plus.gaea.utils.GaeaDateUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 实现security的抽象类
 * @author lr
 * @since 2021-02-01
 */
@Component
public class AppUserDetailsServiceHelper implements UserDetailsServiceHelper {

    @Autowired
    private GaeaUserService gaeaUserService;

    @Override
    public UserDetails findByUsername(String username) {
        LambdaQueryWrapper<GaeaUser> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(GaeaUser::getUsername, username);
        GaeaUser gaeaUser = gaeaUserService.selectOne(wrapper);
        if (gaeaUser == null) {
            return null;
        }

        //构建security的用户对象
        //账号是否可用
        boolean enabled = Enabled.YES.getValue().equals(gaeaUser.getEnabled());
        //账号是否未锁定
        boolean accountLocked = Enabled.YES.getValue().equals(gaeaUser.getAccountLocked());
        //账号是否过期
        boolean accountNonExpired = Enabled.YES.getValue().equals(gaeaUser.getAccountNonExpired());
        //密码是否过期
        boolean credentialsNonExpired = Enabled.YES.getValue().equals(gaeaUser.getCredentialsNonExpired());

        return User.builder().username(username)
                .password(gaeaUser.getPassword())
                .disabled(!enabled)
                .accountLocked(accountLocked)
                .accountExpired(!accountNonExpired)
                .authorities(new String[0])
                .credentialsExpired(!credentialsNonExpired).build();
    }

    @Override
    public Map<String, String> getUserRoles(String username) {
        return gaeaUserService.getUserOrgRoleMap(username);
    }

    /**
     * 获取指定用户组织编码列表
     * @param username
     * @return
     */
    @Override
    public List<String> getUserOrgCodes(String username) {
        return gaeaUserService.getOrgCodes(username);
    }

    /**
     * 是否允许多点登录
     * @param username
     * @return
     */
    @Override
    public Boolean isMultiLogin(String username) {
        GaeaUser gaeaUser = gaeaUserService.getUserByUsername(username);
        if (gaeaUser == null) {
            return false;
        }
        Integer multiLogin = gaeaUser.getMultiLogin();
        if (multiLogin == null) {
            return false;
        }
        return multiLogin > 0;
    }


    /**
     * getPasswordUpdateTime
     * @param username
     * @return
     */
    @Override
    public LocalDateTime getPasswordUpdateTime(String username) {
        GaeaUser gaeaUser = gaeaUserService.getUserByUsername(username);
        Date passwordUpdateTime = gaeaUser.getPasswordUpdateTime();
        return GaeaDateUtils.toLocalDateTime(passwordUpdateTime);
    }

    @Override
    public GaeaUserDetail getUserEntity(String username) {
        return gaeaUserService.getUserByUsername(username);
    }
}
