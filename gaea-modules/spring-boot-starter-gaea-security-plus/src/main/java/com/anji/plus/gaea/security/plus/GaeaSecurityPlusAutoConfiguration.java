package com.anji.plus.gaea.security.plus;

import com.anji.plus.gaea.security.plus.runner.ApplicationInitRunner;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * 盖亚自动装配类
 * @AutoConfigurationPackage 添加当前类所在包加入到AutoConfigurationPackages，保证该包及其子包下的实体类被扫描到
 * 并添加扫描controller的包
 * @author lr
 * @since 2021-01-11
 */

@ConditionalOnClass(WebSecurityConfigurerAdapter.class)
@AutoConfigurationPackage
@ComponentScan(basePackages = {"com.anji.plus.gaea.security.plus.modules"})
public class GaeaSecurityPlusAutoConfiguration {

    /**
     * 应用启动后执行，用于初始化请求信息到权限表中
     * @return
     */
    @Bean
    @ConditionalOnClass(DiscoveryClient.class)
    public ApplicationInitRunner applicationInitRunner() {
        return new ApplicationInitRunner();
    }

}
