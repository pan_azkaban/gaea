package com.anji.plus.gaea.test;

import com.anji.plus.gaea.constant.GaeaConstant;
import com.anji.plus.gaea.utils.GaeaDateUtils;
import com.anji.plus.gaea.utils.GaeaUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.anji.plus.gaea.utils.GaeaMaskUtils.mask;


public class UtilsTest {

    @Test
    public void testMatch(){
        String api = "{aa}/{bb}";
        System.out.println(api.replaceAll(GaeaConstant.URL_REGEX,"*"));

        api = "{cc}";
        System.out.println(api.replaceAll(GaeaConstant.URL_REGEX,"*"));
        api = "{a}/{b}/{c}";
        System.out.println(api.replaceAll(GaeaConstant.URL_REGEX,"*"));

    }

    @Test
    public void gaeaMaskTest(){
        String mask = "(\\w{1})\\w+(\\w{1})";
        String format = "(\\d{2,6}-?)\\d{4}";
        String mask1 = mask("121111", mask, "$1***$2");
        System.out.println(mask1);
        String s = StringUtils.leftPad("11", 4, '*');
        System.out.println(s);
    }

    @Test
    public void formatTest() {
        Map<String, Object> map = new HashMap<>();
        map.put("username", "admin");
        map.put("age", 24);
        String s = GaeaUtils.replaceFormatString("${username} is ${age}", map);
        System.out.println(s);

        String s1 = GaeaDateUtils.formatFromSecond(40000);
        System.out.println(s1);
    }

    @Test
    public void gaeaUtilTest() {
        Map<String,Object> map = new HashMap<>();
        map.put("one",1);
        map.put("two", 2);

        String key = "hello ${one} ${two} ${three}";
        String s = GaeaUtils.replaceFormatString(key, map);
        System.out.println(s);

        String demo = "/**/swgg.html/**";

        System.out.println(GaeaUtils.antPathMatcher.match(demo, "/swgg.html"));
    }

}
