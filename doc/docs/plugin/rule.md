# 规则引擎

## 介绍
规则引擎由规则编码唯一确定，支持新增、修改、执行。主要提供所需参数字段定义、动态规则定制、执行等功能。支持class类动态编译执行，使用了Aviator表达式求值引擎框架，支持各种表达式的动态求值。

## 配置
#### 1.pom.xml中加入依赖如下
```xml
 <dependency>
     <groupId>com.googlecode.aviator</groupId>
     <artifactId>aviator</artifactId>    
 </dependency>

```

## 功能说明
#### 1.规则引擎页面
![输入图片说明](https://images.gitee.com/uploads/images/2021/0409/102443_cbd8984f_8794239.png "规则引擎页面.png")

#### 2.规则新增、修改（参数字段定义、动态规则定制、测试数据Json）
![输入图片说明](https://images.gitee.com/uploads/images/2021/0409/102951_8b7cae79_8794239.png "规则新增.png")

#### 3.动态规则定制（class类、表达式）
![输入图片说明](https://images.gitee.com/uploads/images/2021/0409/104822_fb640eaf_8794239.png "class类.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0409/104839_2e503183_8794239.png "表达式.png")

#### 4.规则执行
![输入图片说明](https://images.gitee.com/uploads/images/2021/0409/103418_1b03b40d_8794239.png "规则执行.png")


## 代码示例
表达式的动态编译求值、class类动态编译执行等功能示例代码如下：
#### 1.参数字段校验（属性名、类型、必填）
```java
 /**
     * 校验规则
     * @param ruleCode 规则编码
     * @param ruleDataMap 测试数据Json
     * @return
     */
    private List<String> checkRules(String ruleCode, Map<String, Object> ruleDataMap) {
        // 获取编码对应规则
        LambdaQueryWrapper<GaeaRules> queryWrapper= Wrappers.lambdaQuery();
        queryWrapper.eq(GaeaRules::getRuleCode, ruleCode)
                .eq(GaeaRules::getEnabled, Enabled.YES.getValue());
        List<GaeaRules> gaeaRules = gaeaRulesMapper.selectList(queryWrapper);
        if (CollectionUtils.isEmpty(gaeaRules)) {
            throw BusinessExceptionBuilder.build(ResponseCode.RULE_CONTENT_NOT_EXIST);
        }
        String ruleFields = gaeaRules.get(0).getRuleFields();
        if (StringUtils.isBlank(ruleFields)) {
            throw BusinessExceptionBuilder.build(ResponseCode.RULE_FIELDS_NOT_EXIST);
        }
        List<GaeaRulesFieldsDTO> gaeaRulesFields = JSONObject.parseArray(ruleFields, GaeaRulesFieldsDTO.class);
        List<String> list = new ArrayList<>();
        gaeaRulesFields.forEach(e -> {
            StringBuilder stringBuilder = new StringBuilder();
            if (e.getRequire()) {
                Object fieldValue = ruleDataMap.get(e.getName());
                if (ObjectUtils.isEmpty(fieldValue)) {
                    stringBuilder.append(e.getName()).append(": ")
                        .append(messageSourceHolder.getMessage(ResponseCode.RULE_FIELD_VALUE_IS_REQUIRED));
                } else {
                    if ((e.getType().equalsIgnoreCase("Number") && !(fieldValue instanceof Number))
                    || (e.getType().equalsIgnoreCase("Boolean") && !(fieldValue instanceof Boolean))
                    || (e.getType().equalsIgnoreCase("String") && !(fieldValue instanceof String))) {
                        if (stringBuilder.length() == 0) {
                            stringBuilder.append(e.getName()).append(": ");
                        } else {
                            stringBuilder.append("，");
                        }
                        stringBuilder.append(messageSourceHolder.getMessage(ResponseCode.RULE_FIELD_VALUE_TYPE_ERROR));
                    }
                }
                if (stringBuilder.length() != 0) {
                    list.add(stringBuilder.toString());
                }
            }
        });
        return list;
    }
```

#### 2.class类动态编译执行
```java
    /**
     * class类动态编译执行
     * @param ruleContent 规则内容
     * @param ruleDataMap 测试数据Json
     * @return
     */
    private String getClassCompileValue(String ruleContent, Map<String, Object> ruleDataMap) {
        String result;
        CustomStringJavaCompiler compiler = new CustomStringJavaCompiler(ruleContent);
        boolean res = compiler.compiler();
        if (res) {
            logger.info("编译成功, compilerTakeTime：" + compiler.getCompilerTakeTime());
            try {
                compiler.runMethod(ruleDataMap);
                logger.info("runTakeTime：" + compiler.getRunTakeTime());
            } catch (Exception e) {
                logger.info("运行异常: " + e);
                throw BusinessExceptionBuilder.build(ResponseCode.RULE_CONTENT_EXECUTE_ERROR);
            }
            result = compiler.getRunResult();
            logger.info(result);
            logger.info("诊断信息：" + compiler.getCompilerMessage());
        } else {
            logger.info("编译失败: " + compiler.getCompilerMessage());
            throw BusinessExceptionBuilder.build(ResponseCode.RULE_CONTENT_EXECUTE_ERROR);
        }
        return result;
    }
```
#### 3.表达式动态求值
```java
    /**
     *  表达式动态求值
     * @param ruleContent 规则内容
     * @param ruleDataMap 测试数据Json
     * @return
     */
    private String getExpressionValue(String ruleContent, Map<String, Object> ruleDataMap) {
        // 数据转换
        ruleDataMap.forEach((k, v) -> {
            if (isNumeric(String.valueOf(v))) {
                ruleDataMap.put(k, new BigDecimal(String.valueOf(v)));
            }
        });
        Expression compileExp;
        try {
            compileExp = AviatorEvaluator.compile(ruleContent);
        } catch (Exception e) {
            logger.info("编译失败: " + e);
            throw BusinessExceptionBuilder.build(ResponseCode.RULE_CONTENT_COMPILE_ERROR);
        }
        String result;
        try {
            result = compileExp.execute(ruleDataMap).toString();
        } catch (Exception e) {
            logger.info("运行异常: " + e);
            throw BusinessExceptionBuilder.build(ResponseCode.RULE_CONTENT_EXECUTE_ERROR);
        }
        return result;
    }
```


