/*
package com.anjiplus.template.gaea.generator.service.impl;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.anjiplus.template.gaea.generator.modules.column.dao.Column;
import com.anjiplus.template.gaea.generator.modules.table.dao.TableInfo;
import com.anjiplus.template.gaea.generator.repository.ColumnInfoRepository;
import com.anjiplus.template.gaea.generator.utils.PageUtil;

import cn.hutool.core.util.ObjectUtil;

*/
/**
 * @author WongBin
 * @date 2021/4/26
 *//*

@Service(value = "jpaImpl")
public class GeneratorServiceJpaImpl extends GeneratorServiceImpl {


    @PersistenceContext
    private EntityManager em;

    @Autowired
    private ColumnInfoRepository columnInfoRepository;

    @Autowired
    private GaeaGeneratorProperties gaeaGeneratorProperties;

    @Override
    public Object getTables() {
        // 使用预编译防止sql注入
        String sql = "select table_name ,create_time , engine, "
                + "table_collation, table_comment from information_schema.tables " +
                "where table_schema = (select database()) " +
                "order by create_time desc";
        Query query = em.createNativeQuery(sql);
        return query.getResultList();
    }

    @Override
    public Object getTables(String name, int[] startEnd) {
        String excludeTablesSql = "";
        if(StringUtils.isNotBlank(gaeaGeneratorProperties.getExcludeTableSql())){
            excludeTablesSql = gaeaGeneratorProperties.getExcludeTableSql();
        }

        // 使用预编译防止sql注入
        String tableQry = StringUtils.isNotBlank(name) ? ("'%" + name + "%' ") : "'%%'";
        String sql = "select table_name ,create_time , engine, "
                + " table_collation, table_comment from information_schema.tables " +
                " where table_schema = (select database()) " +
                " and table_name like " + tableQry + excludeTablesSql+" order by create_time desc";
        Query query = em.createNativeQuery(sql);
        query.setFirstResult(startEnd[0]);
        query.setMaxResults(startEnd[1] - startEnd[0]);
        //query.setParameter(1, StringUtils.isNotBlank(name) ? ("%" + name + "%") : "%%");
        List result = query.getResultList();
        List<TableInfo> tableInfos = new ArrayList<>();
        for (Object obj : result) {
            Object[] arr = (Object[]) obj;
            tableInfos.add(new TableInfo(arr[0], arr[1], arr[2], arr[3],
                    ObjectUtil.isNotEmpty(arr[4]) ? arr[4] : "-"));
        }

        String countSql = "SELECT COUNT(*) from information_schema.tables " +
                " where table_schema = (select database()) and table_name like "+tableQry+excludeTablesSql;
        Query query1 = em.createNativeQuery(countSql);
        //query1.setParameter(1, tableQry);
        Object totalElements = query1.getSingleResult();

        return PageUtil.toPage(tableInfos, totalElements);
    }

    @Override
    public List<Column> queryColumns(String tableName) {
        // 使用预编译防止sql注入
        String sql = "select column_name, is_nullable, data_type, "
                + "column_comment, column_key, extra,character_maximum_length as maxLength," +
                " column_default as defaultValue" +
                " from information_schema.columns " +
                "where table_name = ? and table_schema = (select database()) order by ordinal_position";
        Query query = em.createNativeQuery(sql);
        query.setParameter(1, tableName);
        List result = query.getResultList();

        List<Column> columnInfos = new ArrayList<>();
        Column c = null;
        int idx = 0;
        for (Object obj : result) {
            Object[] arr = (Object[]) obj;
            idx = 0;
            c = new Column(tableName, arr[idx++].toString(),
                    "NO".equals(arr[idx++]), arr[idx++].toString(),
                    ObjectUtil.isNotNull(arr[idx]) ? arr[idx].toString() : null,
                    ObjectUtil.isNotNull(arr[++idx]) ? arr[idx].toString() : null,
                    ObjectUtil.isNotNull(arr[++idx]) ? arr[idx].toString() : null);
            if(arr[++idx]!=null) {
                c.setMaxLength("" + arr[idx]);
            }
            if(arr[++idx]!=null && "String,Integer".indexOf(c.getColumnType())>=0) {
                c.setDefaultValue(arr[idx]);
            }
            columnInfos.add(c);
        }
        return columnInfos;
    }

}
*/
