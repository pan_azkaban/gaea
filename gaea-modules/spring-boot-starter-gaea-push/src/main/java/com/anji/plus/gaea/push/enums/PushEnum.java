package com.anji.plus.gaea.push.enums;

import com.anji.plus.gaea.push.type.dingding.GaeaDingTalkTemplate;
import com.anji.plus.gaea.push.type.email.GaeaMailTemplate;
import com.anji.plus.gaea.push.type.sms.SmsPushDetails;
import com.anji.plus.gaea.push.type.wechat.GaeaWeChatMiniAppTemplate;
import org.apache.commons.lang3.EnumUtils;

import java.util.List;

/**
 * @ClassName PushEnum
 * @Description: 消息推送枚举类
 * @Author dingkaiqiang
 * @Date 2021-04-06
 * @Version V1.0
 **/
public enum PushEnum {

    /**
     * mail:邮件;
     */
    EMAIL("mail", GaeaMailTemplate.class),
    /**
     * dingtalk:钉钉
     */
    DING_DING("dingtalk", GaeaDingTalkTemplate.class),
    /**
     * sms:短信；
     */
    SMS("sms", SmsPushDetails.class),
    /**
     * miniapp 微信小程序
     */
    MINIAPP("miniapp", GaeaWeChatMiniAppTemplate.class);


    /**
     * 消息类型
     */
    private String type;


    /**
     * 发送消息类对应  的class
     */
    private Class aClass;


    PushEnum(String type, Class aClass) {
        this.type = type;
        this.aClass = aClass;
    }

    private static List<PushEnum> allSector = EnumUtils.getEnumList(PushEnum.class);


    /**
     * 根据class 获取到具体的 value
     * @param type
     * @return
     */
    public static Class getClassByType(String type) {
        for (PushEnum sectorEnum : allSector) {
            if (sectorEnum.getType().equals(type)) {
                return sectorEnum.getaClass();
            }
        }
        return null;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Class getaClass() {
        return aClass;
    }

    public void setaClass(Class aClass) {
        this.aClass = aClass;
    }
}
