<!-- **i18n版本-->
<template>
  <el-dialog :close-on-click-modal="false" :visible.sync="showDialog" :title="title" width="75%">
    <el-form ref="form" :model="form" size="small" label-width="120px">
    <#if columns??>
      <el-row :gutter="10">
      <#list columns as column>
          <#if column.columnShow>
        <el-col :xs="24" :sm="20" :md="cols" :lg="cols" :xl="cols">
            <!--<#if column.remark != ''>${column.remark}<#else>${column.changeColumnName}</#if> -->
          <el-form-item :label="tf('${column.changeColumnName}')"<#if column.isNotNull> prop="${column.changeColumnName}" </#if>>
            <#if column.formType = 'Input'>
                <el-input v-model="form.${column.changeColumnName}" />
            <#elseif column.formType = 'Textarea'>
            <el-input v-model="form.${column.changeColumnName}" :rows="3" type="textarea" />
            <#elseif column.formType = 'Radio'>
                <#if (column.dictName)?? && (column.dictName)!="">
            <el-radio v-model="form.${column.changeColumnName}" v-for="item in dict.${column.dictName}"
                      :key="item.id" :label="item.value">{{ item.label }}</el-radio>
                <#else>
                <!--未设置字典，请手动设置 Radio-->
                </#if>
            <#elseif column.formType = 'Select'>
                <#if (column.dictName)?? && (column.dictName)!="">
            <Dictionary v-model="form.${column.changeColumnName}"
                        :updata-dict="form.${column.changeColumnName}<#if column.columnType!='String'>+''</#if>" :dict-key="'${column.dictName}'" />
                <#else>
            <!--未设置字典，请手动设置 Select-->
                </#if>
            <#else>
            <el-date-picker v-model="form.${column.changeColumnName}" type="datetime" />
            </#if>
          </el-form-item>
        </el-col>
          </#if>
      </#list>
      </el-row>
    </#if>
    </el-form>

    <#if subEntity??>
       <#list subEntity as s>
    <${s.className}PreviewItem ref="${s.instance}" alias="r${s_index}" :module="module" />
       </#list>
    </#if>

    <div slot="footer" class="dialog-footer">
      <el-button type="primary" @click="close()">{{$t('btn.close')}}</el-button>
    </div>
  </el-dialog>
</template>

<script>
import { preview } from '@/api/${changeClassName}'
<#if subEntity??>
  <#list subEntity as s>
import ${s.className}PreviewItem from '@/views/${s.instance}/component/previewItem'
  </#list>
</#if>
<#if hasDict>
import Dictionary from '@/components/Dictionary/index'
</#if>
import i18nHandler from '@/mixins/i18nHandler'

export default {
  name: '${className}Detail',
  components:{
  <#if hasDict>Dictionary</#if>
  <#if subEntity??><#list subEntity as s>,${s.className}PreviewItem<#if s_has_next></#if></#list></#if>
  },
  mixins:[i18nHandler],
  props:{
      module:String
  },
  data() {
      return {
          cols:6,
          showDialog: false,title:'',<#if hasDict>dictionarys: [],</#if>
          form:{},
          i18nQry:{}
      }
  },
  created(){
      this.i18nQry.module = this.module
  },
  methods:{
      close(){
          this.showDialog = false;
          this.form = {};
      },

      init() {
          preview({id:this.form.id,accessKey:this.form.accessKey}).then(data => {
              this.form = data.data;
              <#if subEntity??>
              <#list subEntity as s>
              this.$refs.${s.instance}.detail = this.form.${s.instance}s
              </#list>
              </#if>
      }).catch(() => {})
      }
  }
}
</script>

<style scoped>

</style>