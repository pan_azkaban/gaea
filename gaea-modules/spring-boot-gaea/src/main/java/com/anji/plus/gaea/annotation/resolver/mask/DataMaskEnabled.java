package com.anji.plus.gaea.annotation.resolver.mask;

import com.anji.plus.gaea.annotation.DetailView;
import com.anji.plus.gaea.annotation.QueryView;
import com.anji.plus.gaea.holder.UserContext;

/**
 * @author WongBin
 * @date 2022/9/1
 */
public interface DataMaskEnabled {

    /***
     * 是否启用 数据脱敏逻辑
     * @param detailView
     * @param queryView
     * @param context
     * @return
     */
    default boolean enable(DetailView detailView, QueryView queryView, UserContext context){
      return false;
    }
}
