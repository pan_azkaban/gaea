package com.anjiplus.template.gaea.auth.modules.gaeaannot.dao;

import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.controller.dto.AnnotSendReadDTO;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.controller.param.GaeaAnnouncementSendParam;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.dao.entity.GaeaAnnouncementSend;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统公告发送阅读记录表(GaeaAnnouncementSend)Mapper
 *
 * @author peiyanni
 * @since 2021-03-29 15:31:28
 */
@Mapper
public interface GaeaAnnouncementSendMapper extends GaeaBaseMapper<GaeaAnnouncementSend> {

    /**
     * 查看我的消息
     * @param page
     * @param param
     * @return
     */
    List<AnnotSendReadDTO> myAnnotReadList(Page<AnnotSendReadDTO> page, @Param("bo")GaeaAnnouncementSendParam param);

    /**
     * 标记已读（全部标记，单条标记）
     * @param username
     * @param ids
     * @return
     */
    int updateReadFlag(@Param("username")String username,@Param("sendIds")List<Long> ids);
}