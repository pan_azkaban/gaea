package com.anjiplus.template.gaea.auth.modules.user.controller.param;


import com.anji.plus.gaea.annotation.Query;
import com.anji.plus.gaea.constant.QueryEnum;
import com.anji.plus.gaea.curd.params.PageParam;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 用户表(GaeaUser)param
 *
 * @author lr
 * @since 2021-02-02 13:38:12
 */
public class GaeaUserParam extends PageParam implements Serializable {
    @ApiModelProperty(value = "用户登录名")
    @Query(QueryEnum.LIKE)
    private String username;

    @ApiModelProperty(value = "真实姓名")
    @Query(QueryEnum.LIKE)
    private String nickname;

    @ApiModelProperty(value = "手机号")
    @Query(QueryEnum.LIKE)
    private String phone;

    @ApiModelProperty(value = "1：可用 0：禁用")
    private Integer enabled;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }
}
