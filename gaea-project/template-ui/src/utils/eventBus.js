import Vue from 'vue'

const eventBus = new Vue()

const EVENT_TYPE_SYSTEM_MSG = 'EVENT_TYPE_SYSTEM_MSG' // 系统公告通知

export { eventBus, EVENT_TYPE_SYSTEM_MSG }
