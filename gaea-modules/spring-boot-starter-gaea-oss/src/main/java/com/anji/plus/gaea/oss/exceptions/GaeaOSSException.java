package com.anji.plus.gaea.oss.exceptions;

/**
 * GaeaOSS异常
 */
public class GaeaOSSException extends RuntimeException{

    public GaeaOSSException(String message){
        super(message);
    }

    public GaeaOSSException(Throwable throwable) {
        super(throwable);
    }

    public GaeaOSSException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
