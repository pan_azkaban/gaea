package com.anjiplus.template.gaea.business.modules.deviceinfo.dao;

import org.apache.ibatis.annotations.Mapper;

import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import com.anjiplus.template.gaea.business.modules.deviceinfo.dao.entity.DeviceInfo;

/**
* DeviceInfo Mapper
* @author WongBin
* @date 2021-04-07 17:44:28.598
**/
@Mapper
public interface DeviceInfoMapper extends GaeaBaseMapper<DeviceInfo> {

}