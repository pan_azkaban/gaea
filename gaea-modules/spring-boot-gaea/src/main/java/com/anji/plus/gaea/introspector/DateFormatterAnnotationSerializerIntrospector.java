package com.anji.plus.gaea.introspector;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;

/**
 * @author: Raod
 * @since: 2021-12-20
 */
public class DateFormatterAnnotationSerializerIntrospector extends JacksonAnnotationIntrospector {

    private static final long serialVersionUID = 7368707128625539909L;


    @Override
    public Object findSerializer(Annotated annotated) {
        JsonFormat formatter = annotated.getAnnotation(JsonFormat.class);
        if (formatter != null) {
            return new DateSerializer(formatter.pattern(), formatter.timezone());
        }
        return super.findSerializer(annotated);
    }

}
