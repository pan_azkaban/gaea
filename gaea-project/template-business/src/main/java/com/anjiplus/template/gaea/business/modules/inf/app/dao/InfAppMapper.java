package com.anjiplus.template.gaea.business.modules.inf.app.dao;

import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import com.anjiplus.template.gaea.business.modules.inf.app.dao.entity.InfAppEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * (InfApp)表数据库访问层
 *
 * @author ultrajiaming
 * @since 2021-03-18 23:27:44
 */
@Mapper
public interface InfAppMapper extends GaeaBaseMapper<InfAppEntity> {

}
