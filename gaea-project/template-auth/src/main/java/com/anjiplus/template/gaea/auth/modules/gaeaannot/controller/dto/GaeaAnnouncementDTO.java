package com.anjiplus.template.gaea.auth.modules.gaeaannot.controller.dto;

import com.anji.plus.gaea.curd.dto.GaeaBaseDTO;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * 系统公告信息表(GaeaAnnouncement)实体类
 *
 * @author makejava
 * @since 2021-03-29 15:17:23
 */
@ApiModel(value = "系统公告信息表")
public class GaeaAnnouncementDTO extends GaeaBaseDTO implements Serializable {
    /**
     * 标题
     */
    @ApiModelProperty(value = "标题")
    @NotBlank(message = "title not empty")
    @Size(max = 30,message = "Title length must not exceed 30")
    private String title;
    /**
     * 内容
     */
    @ApiModelProperty(value = "内容")
    @NotBlank(message = "content not empty")
    private String msgContent;
    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date startTime;
    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date endTime;
    /**
     * 优先级 （优先级（L低，M中，H高））
     */
    @ApiModelProperty(value = "优先级 （优先级（L低，M中，H高））")
    private String priority;
    /**
     * 消息类型1:通知公告2:系统消息
     */
    @ApiModelProperty(value = "消息类型1:通知公告2:系统消息")
    private String msgCategory;
    /**
     * 通告对象类型（USER:指定用户，ALL:全体用户）
     */
    @ApiModelProperty(value = "通告对象类型（USER:指定用户，ALL:全体用户）")
    private String msgType;
    /**
     * 发布状态（0未发布，1已发布，2已撤销）
     */
    @ApiModelProperty(value = "发布状态（0未发布，1已发布，2已撤销）")
    private Integer sendStatus;
    /**
     * 发布人
     */
    @ApiModelProperty(value = "发布人")
    private String publisher;
    /**
     * 发布时间
     */
    @ApiModelProperty(value = "发布时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date sendTime;
    /**
     * 撤销时间
     */
    @ApiModelProperty(value = "撤销时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date cancelTime;
    /**
     * 指定用户，多个逗号隔开
     */
    @ApiModelProperty(value = "指定用户，多个逗号隔开")
    private String userNames;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getMsgCategory() {
        return msgCategory;
    }

    public void setMsgCategory(String msgCategory) {
        this.msgCategory = msgCategory;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public Integer getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(Integer sendStatus) {
        this.sendStatus = sendStatus;
    }

    public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    public Date getCancelTime() {
        return cancelTime;
    }

    public void setCancelTime(Date cancelTime) {
        this.cancelTime = cancelTime;
    }

    public String getUserNames() {
        return userNames;
    }

    public void setUserNames(String userNames) {
        this.userNames = userNames;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
}