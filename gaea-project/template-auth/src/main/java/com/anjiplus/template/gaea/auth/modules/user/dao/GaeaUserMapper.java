package com.anjiplus.template.gaea.auth.modules.user.dao;

import com.anjiplus.template.gaea.auth.modules.user.controller.dto.GaeaUserQueryDto;
import com.anjiplus.template.gaea.auth.modules.user.controller.param.GaeaUserQueryParam;
import com.anjiplus.template.gaea.auth.modules.user.dao.entity.GaeaUser;
import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户表(GaeaUser)Mapper
 *
 * @author lr
 * @since 2021-02-02 13:38:12
 */
@Mapper
public interface GaeaUserMapper extends GaeaBaseMapper<GaeaUser> {
    /**
     * 用户信息高级查询
     * @param page
     * @param bo
     * @param wrapper
     * @return
     */
    List<GaeaUserQueryDto> queryUserAdvance(Page<GaeaUserQueryDto> page, @Param("bo") GaeaUserQueryParam bo, @Param(Constants.WRAPPER) QueryWrapper wrapper);

    /**
     * 查询所有有效的用户
     * @return
     */
    List<GaeaUser> getAllUser();

}
