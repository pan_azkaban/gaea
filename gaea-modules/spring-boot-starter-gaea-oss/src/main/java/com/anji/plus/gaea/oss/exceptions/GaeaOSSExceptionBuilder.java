package com.anji.plus.gaea.oss.exceptions;

public class GaeaOSSExceptionBuilder {
    public GaeaOSSExceptionBuilder() {
    }

    public static GaeaOSSException build(String code) {
        return new GaeaOSSException(code);
    }

    public static GaeaOSSException build(Throwable throwable) {
        return new GaeaOSSException(throwable);
    }

    public static GaeaOSSException build(String code, Throwable throwable) {
        return new GaeaOSSException(code, throwable);
    }
}
