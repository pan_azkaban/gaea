package com.anji.plus.gaea.exception;

/**
 * 业务异常封装类
 * @author lr
 * @since 2021-01-12
 */

public class BusinessException extends RuntimeException{
    /**
     * 异常码
     */
    private String code;
    /*跨服务场景，msg需要透传*/
    private String msg;
    /**
     * 异常参数
     */
    private Object[] args;

    public BusinessException(String code) {
        this.code = code;
    }

    public BusinessException(String code, Object[] args) {
        this.code = code;
        this.args = args;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object[] getArgs() {
        if(args != null) {
            return args.clone();
        }else {
            return null;
        }

    }

    public void setArgs(Object[] args) {
        if(args != null) {
            this.args = args.clone();
        }
    }
}
