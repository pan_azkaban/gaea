package com.anji.plus.gaea.security.plus.modules.user.dao;

import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import com.anji.plus.gaea.security.plus.modules.user.dao.entity.GaeaUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户表(GaeaUser)Mapper
 *
 * @author lr
 * @since 2021-02-02 13:38:12
 */
@Mapper
public interface GaeaUserMapper extends GaeaBaseMapper<GaeaUser> {

}
