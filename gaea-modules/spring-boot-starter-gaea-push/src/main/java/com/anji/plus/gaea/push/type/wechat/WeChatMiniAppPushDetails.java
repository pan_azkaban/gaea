package com.anji.plus.gaea.push.type.wechat;

import cn.binarywang.wx.miniapp.bean.WxMaSubscribeMessage;
import cn.binarywang.wx.miniapp.bean.WxMaTemplateData;
import cn.binarywang.wx.miniapp.bean.WxMaUniformMessage;
import cn.binarywang.wx.miniapp.constant.WxMaConstants;
import com.anji.plus.gaea.push.type.base.BasePushDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 微信小程序明细
 * @author: Raod
 * @since: 2023-02-17
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WeChatMiniAppPushDetails extends BasePushDetails {
    //-----------------------------------小程序鉴权开始----------------------------------
    /**
     * 小程序ID
     */
    private String appid;

    /**
     * 小程序秘钥
     */
    private String secret;

    //-----------------------------------小程序鉴权结束----------------------------------

    /**
     * 是否发送公众号模版消息，否则发送小程序模版消息.
     */
    @Builder.Default
    private boolean isMpTemplateMsg = true;


    /**
     * 用户openid.
     * 可以是小程序的openid，也可以是mp_template_msg.appid对应的公众号的openid
     */
    private String toUser;

    /**
     * 公众号或小程序模板ID.
     */
    private String templateId;

    /**
     * 消息内容
     */
    private Map<String, Object> data;

    // --------------------------------发送小程序消息开始---------------------------

    /**
     * 小程序模板消息formid.
     */
    private String formId;

    /**
     * 小程序页面路径.
     */
    private String page;

    /**
     * 跳转小程序类型：developer为开发版；trial为体验版；formal为正式版；默认为正式版
     */
    @Builder.Default
    private String miniprogramState = WxMaConstants.MiniProgramState.FORMAL;

    /**
     * 进入小程序查看的语言类型，支持zh_CN(简体中文)、en_US(英文)、zh_HK(繁体中文)、zh_TW(繁体中文)，默认为zh_CN
     */
    @Builder.Default
    private String lang = WxMaConstants.MiniProgramLang.ZH_CN;

    /**
     * 转换小程序消息数据
     * @return
     */
    public List<WxMaSubscribeMessage.MsgData> convertMiniAppData() {
        List<WxMaSubscribeMessage.MsgData> result = new ArrayList<>();
        if (null != this.data) {
            this.data.forEach((key, value) -> {
                result.add(new WxMaSubscribeMessage.MsgData(String.valueOf(key), String.valueOf(value)));
            });
        }
        return result;
    }


    // --------------------------------发送小程序消息结束---------------------------


    // --------------------------------发送公众号消息开始---------------------------
    /**
     * 公众号appid，要求与小程序有绑定且同主体.
     */
    private String mpAppid;
    /**
     * 公众号模板消息所要跳转的url.
     */
    private String url;


    /**
     * 公众号模板消息所要跳转的小程序，小程序的必须与公众号具有绑定关系.
     * @param appid
     * @param pagePath
     * @return
     */
    public WxMaUniformMessage.MiniProgram buildMiniProgram(String appid, String pagePath) {
        return new WxMaUniformMessage.MiniProgram(appid, pagePath, false, false);
    }

    /**
     * 转换公众号消息数据
     * @return
     */
    public List<WxMaTemplateData> convertMpData() {
        List<WxMaTemplateData> result = new ArrayList<>();
        if (null != this.data) {
            this.data.forEach((key, value) -> {
                result.add(new WxMaTemplateData(String.valueOf(key), String.valueOf(value)));
            });
        }
        return result;
    }

    // --------------------------------发送公众号消息结束---------------------------

}
