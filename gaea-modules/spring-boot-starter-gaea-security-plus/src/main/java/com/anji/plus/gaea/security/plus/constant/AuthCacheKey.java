package com.anji.plus.gaea.security.plus.constant;

/**
 * 缓存key
 * @author lr
 * @since 2021-04-27
 */
public interface AuthCacheKey {

    /**
     * 机构code与名称对应的缓存
     */
    String ORG_CODE_NAME_KEY = "system:auth:org:code:name";

    /**
     * 菜单编码和名称
     */
    String MENU_CODE = "system:auth:menu:code:name";
}
