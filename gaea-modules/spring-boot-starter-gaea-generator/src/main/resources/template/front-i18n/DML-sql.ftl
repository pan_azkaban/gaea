/*step 1 初始化菜单，权限相关sql示例，parent_code,org_code 根据实际情况调整***/
insert into gaea_menu(menu_code, menu_name, sys_code, parent_code, path,
                      menu_icon,sort,enabled,delete_flag, redirect_url, component,
                      create_by, update_by, create_time, update_time, version)values
('${className}','${apiAlias}','PC','SystemSet','${changeClassName}','',3060,1,	0,'','system-set/${changeClassName}/index','admin','管理员',now(),now(),1);

insert into gaea_authority(application_name, scan_annotation, auth_code, auth_name, parent_code, path,
                           create_time, update_time, version) values
('${project}-business', 0, '${className}Controller#pageList', '${apiAlias}查询', '${className}Controller', 'GET#/${changeClassName}/pageList', now(), now(), 1),
('${project}-business', 0, '${className}Controller#detail', '${apiAlias}详情', '${className}Controller', 'GET#/${changeClassName}/**', now(), now(), 1),
('${project}-business', 0, '${className}Controller#update', '${apiAlias}修改', '${className}Controller', 'PUT#/${changeClassName}/', now(), now(), 1),
('${project}-business', 0, '${className}Controller#insert', '${apiAlias}新增', '${className}Controller', 'POST#/${changeClassName}/', now(), now(), 1),
('${project}-business', 0, '${className}Controller#deleteById', '${apiAlias}删除', '${className}Controller', 'DELETE#/${changeClassName}/**', now(), now(), 1)
;

insert into gaea_menu_authority(org_code, menu_code, auth_code, create_by,
                                update_by, create_time, update_time, version)VALUES
('3000004','${className}','${className}Controller#pageList','','',now(),now(),1),
('3000004','${className}','${className}Controller#detail','','',now(),now(),1),
('3000004','${className}','${className}Controller#update','','',now(),now(),1),
('3000004','${className}','${className}Controller#insert','','',now(),now(),1),
('3000004','${className}','${className}Controller#deleteById','','',now(),now(),1);

/*step 2 国际化配置***/
/**2.1 中文版本在系统页面操作*/
/**-在[系统设置/ui国际化设置] 【扫描】新表${apiAlias}:${tableName}的数据到gaea_ui_i18n***/

/**2.2 英文版本执行以下语句*/
insert into gaea_ui_i18n(locale, cata_type, system, module, code, name, remark, refer, enabled, create_by, create_time, update_by, update_time, version)
  SELECT 'en',cata_type, system, module, code, code, remark, refer, enabled, create_by, now(), update_by, now(), version
from gaea_ui_i18n
where refer='${tableName}' and locale='zh';

/**2.3 module名称国际化配置,用于新增，修改，详情页title字段***/
insert into gaea_ui_i18n(locale, cata_type, system, module, code, name, remark, refer, enabled, create_by, create_time, update_by, update_time, version)
values
('zh','','${project}-business','${className}','moduleName','${apiAlias}','${tableName}',1,'admin',now(),'admin',now(),1)
,('en','','${project}-business','${className}','moduleName','${className}','${tableName}',1,'admin',now(),'admin',now(),1)