package com.anji.plus.gaea.flowable.controller;

import org.flowable.idm.api.User;
import org.flowable.ui.common.model.RemoteUser;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

/**
 * @author lir
 * @since 2020-07-28
 */
@RequestMapping("/my/rest")
@RestController
public class BaseController {

    @GetMapping("/account")
    public User get() {
        User user = new RemoteUser();
        user.setFirstName("admin");
        user.setId("admin");
        user.setDisplayName("admin");

        String[] arr = new String[]{"access-idm", "access-rest-api", "access-task", "access-modeler", "access-admin"};
        ((RemoteUser) user).setPrivileges(Arrays.asList(arr));
        return user;
    }
}
