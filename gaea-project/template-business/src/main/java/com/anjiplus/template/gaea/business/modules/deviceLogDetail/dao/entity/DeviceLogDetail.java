
package com.anjiplus.template.gaea.business.modules.devicelogdetail.dao.entity;

import lombok.Data;
import io.swagger.annotations.ApiModelProperty;

import com.anji.plus.gaea.curd.entity.GaeaBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;

import javax.validation.constraints.*;
import java.sql.Timestamp;

/**
* @description 设备日志明细 entity
* @author dev-user
* @date 2021-04-07 17:06:42.920
**/
@TableName(value="t_device_log_detail")
@Data
public class DeviceLogDetail extends GaeaBaseEntity {
    @ApiModelProperty(value = "设备编号")
    private Long deviceId;

    @ApiModelProperty(value = "日志内容")
    private String log;


}