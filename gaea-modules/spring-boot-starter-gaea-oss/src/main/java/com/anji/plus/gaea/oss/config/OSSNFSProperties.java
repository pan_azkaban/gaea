package com.anji.plus.gaea.oss.config;

import java.io.Serializable;

public class OSSNFSProperties implements Serializable {

    private String path = "/app/disk/upload";

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
