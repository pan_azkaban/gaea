package com.anji.plus.gaea.oss.config;

import com.anji.plus.gaea.oss.ossbuilder.FileClientFactory;
import com.anji.plus.gaea.oss.ossbuilder.GaeaOSSTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
/**
 * springboot自动装配类
 *
 * @author 木子李·De
 * @since 2022/3/22 14:16
 */
@Configuration
@EnableConfigurationProperties(OSSProperties.class)
@ConditionalOnProperty(name= "spring.gaea.subscribes.oss.enabled", havingValue="true")
public class AutoConfiguration {
    private static Logger logger = LoggerFactory.getLogger(AutoConfiguration.class);

    @Autowired
    private OSSProperties ossProperties;

    @Bean
    @ConditionalOnMissingBean
    public GaeaOSSTemplate gaeaOSSTemplate() throws Exception{
        FileClientFactory fileClientFactory = new FileClientFactory(ossProperties);
        return fileClientFactory.getObject();
    }
}
