package com.anji.plus.gaea.oss.ossbuilder;

import java.util.Objects;

import com.anji.plus.gaea.oss.config.OSSAmazonS3Properties;
import com.anji.plus.gaea.oss.config.OSSMinioProperties;
import com.anji.plus.gaea.oss.config.OSSNFSProperties;
import com.anji.plus.gaea.oss.config.OSSProperties;
import com.anji.plus.gaea.oss.ossbuilder.builders.AmazonS3Client;
import com.anji.plus.gaea.oss.ossbuilder.builders.HuaweiOBSClient;
import com.anji.plus.gaea.oss.ossbuilder.builders.NFSClient;
import com.anji.plus.gaea.oss.ossbuilder.builders.MinioClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.util.StringUtils;

/**
 * 文件存储客户端工厂
 *
 * @Author: lide
 * @since 2022/3/22 14:16
 */
public class FileClientFactory implements FactoryBean<GaeaOSSTemplate> {

    private static Logger logger = LoggerFactory.getLogger(FileClientFactory.class);

    private GaeaOSSTemplate gaeaOSSTemplate;

    private OSSProperties ossProperties;

    public FileClientFactory(OSSProperties ossProperties){
        this.ossProperties = ossProperties;
        this.initFileClient();
    }

    @Override
    public GaeaOSSTemplate getObject() throws Exception {
        return gaeaOSSTemplate;
    }

    @Override
    public Class<?> getObjectType() {
        return GaeaOSSTemplate.class;
    }

    @Override
    public boolean isSingleton() {
        return FactoryBean.super.isSingleton();
    }

    /** 初始化文件存储，顺序如下：
     * 1.优先判断minio相关配置是否设置，如果有使用minio
     * 2.优先判断minio相关配置是否设置，如果有使用minio
     * 3.默认，以上没有启用时，使用本地存储代替
     */
    protected void initFileClient(){
        // 如果minio配置项存在时，使用minio文件存储
        OSSMinioProperties minio = this.ossProperties.getMinio();
        if(minio != null && !StringUtils.isEmpty(minio.getUrl())){
            this.gaeaOSSTemplate = new MinioClient(this.ossProperties);
            return;
        }
        // 如果AmazonS3配置项存在时，使用AmazonS3服务器
        OSSAmazonS3Properties amazonS3 = this.ossProperties.getAmazonS3();
        if(amazonS3 != null && !StringUtils.isEmpty(amazonS3.getUrl())){
            this.gaeaOSSTemplate = new AmazonS3Client(this.ossProperties);
            return;
        }
        if(Objects.nonNull(ossProperties.getHuaweiObs()) && !StringUtils.isEmpty(
                ossProperties.getHuaweiObs().getUrl())){
            this.gaeaOSSTemplate = new HuaweiOBSClient(ossProperties);
            return;
        }
        // 如果minio和AmazonS3配置项不存在时，使用本地存储，
        this.gaeaOSSTemplate = new NFSClient(this.ossProperties);
    }

}
