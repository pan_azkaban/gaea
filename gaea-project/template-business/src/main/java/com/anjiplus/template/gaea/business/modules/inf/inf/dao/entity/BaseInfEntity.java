package com.anjiplus.template.gaea.business.modules.inf.inf.dao.entity;

import com.anji.plus.gaea.curd.entity.GaeaBaseEntity;

/**
 * @ClassName BaseInfEntity
 * @Description: 接口维护表(Inf) 基础类，必须包含这三个字段
 * @Author dingkaiqiang
 * @Date 2021-03-26
 * @Version V1.0
 **/
public class BaseInfEntity extends GaeaBaseEntity {

    //接口名称
    private String infName;
    //接口描述
    private String infDescription;
    //sql语句
    private String sqlSentence;


    public String getInfName() {
        return infName;
    }

    public void setInfName(String infName) {
        this.infName = infName;
    }

    public String getInfDescription() {
        return infDescription;
    }

    public void setInfDescription(String infDescription) {
        this.infDescription = infDescription;
    }

    public String getSqlSentence() {
        return sqlSentence;
    }

    public void setSqlSentence(String sqlSentence) {
        this.sqlSentence = sqlSentence;
    }
}
