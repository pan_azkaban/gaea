package com.anji.plus.gaea.export.enums;

public enum ExportTypeEnum {
    /**easy_excel*/
    SIMPLE_EXCEL("excel", "easy_excel"),
    /**pasper_excel*/
    JASPER_TEMPLATE_EXCEL("jasper_template_excel", "jasper_template_excel"),
    /**jasper_pdf*/
    JASPER_TEMPLATE_PDF("jasper_template_pdf", "jasper_template_pdf"),

    /**gaea_excel
     * update by lide，该类型已经搬到aj-report，暂时未开发好
    GAEA_TEMPLATE_EXCEL("gaea_template_excel", "gaea_template_excel"),
    gaea_pdf
    GAEA_TEMPLATE_PDF("gaea_template_pdf", "gaea_template_pdf"),
     */
    ;

    private String codeValue;
    private String codeDesc;

    private ExportTypeEnum(String codeValue, String codeDesc) {
        this.codeValue = codeValue;
        this.codeDesc = codeDesc;
    }

    public String getCodeValue() {
        return this.codeValue;
    }

    public String getCodeDesc() {
        return this.codeDesc;
    }

}
