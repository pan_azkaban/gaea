package com.anji.plus.gaea.security.security.handler;


import com.alibaba.fastjson.JSONObject;
import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.cache.CacheHelper;
import com.anji.plus.gaea.constant.GaeaConstant;
import com.anji.plus.gaea.constant.GaeaKeyConstant;
import com.anji.plus.gaea.holder.UserContentHolder;
import com.anji.plus.gaea.security.code.UserResponseCode;
import com.anji.plus.gaea.security.event.UserLogoutEvent;
import com.anji.plus.gaea.security.i18.GaeaMessageSourceAccessor;
import com.anji.plus.gaea.security.i18.GaeaSecurityMessageSource;
import com.anji.plus.gaea.utils.ApplicationContextUtils;
import com.anji.plus.gaea.utils.JwtBean;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 登出成功
 * @author lr
 * @since 2021-01-27
 */
public class GaeaLogoutSuccessHandler implements LogoutSuccessHandler {

    @Autowired
    private CacheHelper cacheHelper;

    @Autowired
    private JwtBean jwtBean;

    private GaeaMessageSourceAccessor messages = GaeaSecurityMessageSource.getAccessor();

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.setCharacterEncoding(GaeaConstant.CHARSET_UTF8);
        response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        //用户名
        String username = UserContentHolder.getUsername();
        String token = request.getHeader(GaeaConstant.Authorization);
        String uuid = jwtBean.getUUID(token);
        if(StringUtils.isNotBlank(token)) {
            //删除指定客户端的缓存token
            cacheHelper.delete(GaeaKeyConstant.USER_LOGIN_TOKEN + username + GaeaConstant.REDIS_SPLIT + uuid);
        }

        String code = UserResponseCode.USER_LOGOUT_SUCCESS;
        ResponseBean responseBean = ResponseBean.builder().build();
        responseBean.setMessage(messages.getMessage(code,code));
        response.getWriter().print(JSONObject.toJSONString(responseBean));

        //发布登录事件
        ApplicationContextUtils.publishEvent(new UserLogoutEvent(username));
    }
}
