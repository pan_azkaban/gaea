package com.anji.plus.gaea.security.plus.modules.user.dao;

import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import com.anji.plus.gaea.security.plus.modules.user.dao.entity.GaeaUserRoleOrg;
import org.apache.ibatis.annotations.Mapper;

/**
 * (GaeaUserRoleOrg)Mapper
 *
 * @author makejava
 * @since 2021-02-03 18:01:05
 */
@Mapper
public interface GaeaUserRoleOrgMapper extends GaeaBaseMapper<GaeaUserRoleOrg> {
}
