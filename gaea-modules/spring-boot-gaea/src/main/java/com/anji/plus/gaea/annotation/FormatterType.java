package com.anji.plus.gaea.annotation;

import com.anji.plus.gaea.annotation.resolver.format.FormatterEnum;

import java.lang.annotation.*;

import javax.lang.model.type.NullType;

/**
 * 字段
 * @author lr
 * @since 2021-01-12
 */

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FormatterType {

    FormatterEnum type() default FormatterEnum.OBJECT;

    Class target() default Object.class;
}
