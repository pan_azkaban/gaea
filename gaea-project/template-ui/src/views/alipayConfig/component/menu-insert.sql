insert into gaea_menu(menu_code, menu_name, sys_code, parent_code, path,
                      menu_icon,sort,enabled,delete_flag, redirect_url, component,
                      create_by, update_by, create_time, update_time, version)values
('AlipayConfig','支付配置','PC','SystemSet','alipayConfig','',3060,1,	0,'','system-set/alipayConfig/index','admin','管理员',now(),now(),1);

insert into gaea_authority(application_name, scan_annotation, auth_code, auth_name, parent_code, path,
                           create_time, update_time, version) values
('gaea-business', 0, 'AlipayConfigController#pageList', 'uiI18n查询', 'AlipayConfigController', 'GET#/alipayConfig/pageList', now(), now(), 1),
('gaea-business', 0, 'AlipayConfigController#detail', 'uiI18n详情', 'AlipayConfigController', 'GET#/alipayConfig/**', now(), now(), 1),
('gaea-business', 0, 'AlipayConfigController#update', 'uiI18n修改', 'AlipayConfigController', 'PUT#/alipayConfig/', now(), now(), 1),
('gaea-business', 0, 'AlipayConfigController#insert', 'uiI18n新增', 'AlipayConfigController', 'POST#/alipayConfig/', now(), now(), 1),
('gaea-business', 0, 'AlipayConfigController#deleteById', 'uiI18n删除', 'AlipayConfigController', 'DELETE#/alipayConfig/**', now(), now(), 1)
;

insert into gaea_menu_authority(org_code, menu_code, auth_code, create_by,
                                update_by, create_time, update_time, version)VALUES
('3000004','AlipayConfig','AlipayConfigController#pageList','','',now(),now(),1),
('3000004','AlipayConfig','AlipayConfigController#detail','','',now(),now(),1),
('3000004','AlipayConfig','AlipayConfigController#update','','',now(),now(),1),
('3000004','AlipayConfig','AlipayConfigController#insert','','',now(),now(),1),
('3000004','AlipayConfig','AlipayConfigController#deleteById','','',now(),now(),1);