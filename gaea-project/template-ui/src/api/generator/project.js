import request from '@/utils/request'

export function pageList(params) {
  return request({
    url: '/business/project/pageList',
    method: 'get',
    params,
  })
}
export function businessProject(params) {
  return request({
    url: '/business/project',
    method: 'PUT',
    data: params,
  })
}
