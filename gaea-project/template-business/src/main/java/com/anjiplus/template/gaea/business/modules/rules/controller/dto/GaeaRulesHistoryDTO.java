package com.anjiplus.template.gaea.business.modules.rules.controller.dto;

import com.anji.plus.gaea.curd.dto.GaeaBaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * (GaeaRulesHistory)DTO
 *
 * @author why
 * @since 2021-03-17 13:52:55
 */
@ApiModel(value = "")
@Getter
@Setter
public class GaeaRulesHistoryDTO extends GaeaBaseDTO implements Serializable {
    /**
    * 规则编码
    */    
    @ApiModelProperty(value = "规则编码")
    private String ruleCode;
    /**
    * 规则测试值
    */
    @ApiModelProperty(value = "规则测试值")
    private String ruleDemo;
    /**
    * 规则内容
    */    
    @ApiModelProperty(value = "规则内容")
    private String ruleContent;
    /**
    * 执行结果
    */    
    @ApiModelProperty(value = "执行结果")
    private String ruleResult;

}