package com.anji.plus.gaea.security.plus.modules.role.dao;

import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import com.anji.plus.gaea.security.plus.modules.role.dao.entity.GaeaRoleMenuAuthority;
import org.apache.ibatis.annotations.Mapper;

/**
 * (GaeaRoleMenuAction)Mapper
 *
 * @author lr
 * @since 2021-02-02 13:43:52
 */
@Mapper
public interface GaeaRoleMenuAuthorityMapper extends GaeaBaseMapper<GaeaRoleMenuAuthority> {


}
