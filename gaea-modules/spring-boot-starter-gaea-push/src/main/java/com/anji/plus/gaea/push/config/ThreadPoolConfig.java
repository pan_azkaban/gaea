package com.anji.plus.gaea.push.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author anji gaea teams
 * @Title: spring 线程池配置，ThreadPoolTaskExecutor
 * @date 2020-10-11
 */
@Configuration
public class ThreadPoolConfig {
    private static int CORE_POOL_SIZE = 5;
    private static int MAX_POOL_SIZE = 50;

    /**
     * 后面将废弃
     * @return
     */
    @Bean(name = "threadPoolTaskExecutor")
    @Deprecated()
    public ThreadPoolTaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor poolTaskExecutor = new ThreadPoolTaskExecutor();

        //线程池维护线程的最少数量
        poolTaskExecutor.setCorePoolSize(CORE_POOL_SIZE);

        //线程池维护线程的最大数量
        poolTaskExecutor.setMaxPoolSize(MAX_POOL_SIZE);

        //线程池所使用的缓冲队列
        poolTaskExecutor.setQueueCapacity(100);

        //线程池维护线程所允许的空闲时间
        poolTaskExecutor.setKeepAliveSeconds(30000);

        //增加组名和子线程名前缀，方便排错
        poolTaskExecutor.setThreadGroupName("threadPoolTaskExecutor");
        poolTaskExecutor.setThreadNamePrefix("threadPoolTaskExecutor");

        //由调用线程处理该任务
        poolTaskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());

        poolTaskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        return poolTaskExecutor;
    }
}
