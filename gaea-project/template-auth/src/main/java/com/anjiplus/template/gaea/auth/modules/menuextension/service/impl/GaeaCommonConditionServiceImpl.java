package com.anjiplus.template.gaea.auth.modules.menuextension.service.impl;

import com.alibaba.fastjson.JSON;
import com.anji.plus.gaea.exception.BusinessExceptionBuilder;
import com.anji.plus.gaea.holder.UserContentHolder;
import com.anjiplus.template.gaea.common.MagicValueConstants;
import com.anjiplus.template.gaea.common.RespCommonCode;
import com.anjiplus.template.gaea.common.dto.DynamicQueryBo;
import com.anjiplus.template.gaea.auth.modules.menuextension.controller.dto.GaeaCommonConditionDTO;
import com.anjiplus.template.gaea.auth.modules.menuextension.controller.param.ComConditionQueryParam;
import com.anjiplus.template.gaea.auth.modules.menuextension.controller.param.CommonConditionInputBO;
import com.anjiplus.template.gaea.auth.modules.menuextension.controller.param.CommonConditionReqBO;
import com.anjiplus.template.gaea.auth.modules.menuextension.dao.GaeaCommonConditionMapper;
import com.anjiplus.template.gaea.auth.modules.menuextension.entity.GaeaCommonCondition;
import com.anjiplus.template.gaea.auth.modules.menuextension.service.GaeaCommonConditionService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * (GaeaCommonCondition)ServiceImpl
 *
 * @author makejava
 * @since 2021-02-02 14:42:40
 */
@Service
@Slf4j
public class GaeaCommonConditionServiceImpl implements GaeaCommonConditionService {
    @Autowired
    private GaeaCommonConditionMapper gaeaCommonConditionMapper;

    @Override
    public GaeaBaseMapper<GaeaCommonCondition> getMapper() {
        return gaeaCommonConditionMapper;
    }

    @Override
    public List<GaeaCommonConditionDTO> queryByCondition(ComConditionQueryParam queryParam) {
        return gaeaCommonConditionMapper.queryByCondition(queryParam);
    }

    @Override
    public List<DynamicQueryBo> getDynamicQueryBoListById(Long commonId) {
        List<DynamicQueryBo> result = new ArrayList<>(1);
        LambdaQueryWrapper<GaeaCommonCondition> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(GaeaCommonCondition::getId, commonId);
        try {
            GaeaCommonCondition commonCondition = gaeaCommonConditionMapper.selectOne(queryWrapper);
            if (null != commonCondition && StringUtils.isNotEmpty(commonCondition.getCommSql())) {
                result = JSON.parseArray(commonCondition.getCommSql(), DynamicQueryBo.class);
            }
        } catch (Exception e) {
            log.error("search error {}", e.getMessage());
        }
        return result;
    }

    @Override
    public boolean saveCommonCondition(CommonConditionInputBO t) {
        List<String> values = new ArrayList<>();
        List<DynamicQueryBo> dynamicQueryBos = new ArrayList<>();
        List<CommonConditionReqBO> commonConditionReqBOList = t.getCommonConditionReqBOList();
        GaeaCommonCondition commonCondition = t.inputBO2Entity();
        //判断同一个用户下的常用查询名称是否重复
        ComConditionQueryParam queryParam = new ComConditionQueryParam();
        queryParam.setCreateBy(UserContentHolder.getContext().getUsername());
        queryParam.setMenuCode(commonCondition.getMenuCode());
        queryParam.setTableCode(commonCondition.getTableCode());
        List<GaeaCommonConditionDTO> commList = gaeaCommonConditionMapper.queryByCondition(queryParam);
        if (!CollectionUtils.isEmpty(commList)) {
           List<String> searchNameList= commList.stream().map(e->e.getSearchName()).collect(Collectors.toList());
           if(searchNameList.contains(commonCondition.getSearchName())){
               throw BusinessExceptionBuilder.build(RespCommonCode.SEARCHNAME_ISEXIST);
           }
        }
        for (int i = 0; i < commonConditionReqBOList.size(); i++) {
            CommonConditionReqBO commonConditionReqBO = commonConditionReqBOList.get(i);
            String value = commonConditionReqBO.getValue();
            // 查询条件下拉框展示标签
            if (MagicValueConstants.TWO == commonConditionReqBO.getType()) {
                value = commonConditionReqBO.getValueName();
            }
            values.add(value);
            getDynamicList(dynamicQueryBos, commonConditionReqBO);
        }
        commonCondition.setCommSql(JSON.toJSONString(dynamicQueryBos));
        commonCondition.setLabel(JSON.toJSONString(values));
        if(null!=commonCondition.getId()){
            gaeaCommonConditionMapper.updateById(commonCondition);
        }else{
            gaeaCommonConditionMapper.insert(commonCondition);
        }
        // TODO 常用查询sql,List塞入缓存
        return true;
    }

    private void getDynamicList(List<DynamicQueryBo> dynamicQueryBos, CommonConditionReqBO commonConditionReqBO) {
        DynamicQueryBo dynamicQueryBo = new DynamicQueryBo();
        dynamicQueryBo.setDatePrecision(commonConditionReqBO.getDatePrecision());
        dynamicQueryBo.setName(commonConditionReqBO.getName());
        dynamicQueryBo.setOperator(commonConditionReqBO.getOperator());
        dynamicQueryBo.setValue(commonConditionReqBO.getValue());
        dynamicQueryBo.setValueType(commonConditionReqBO.getValueType());
        dynamicQueryBo.setType(commonConditionReqBO.getType());
        dynamicQueryBo.setDataSource(commonConditionReqBO.getDataSource());
        dynamicQueryBo.setDataSourceValue(commonConditionReqBO.getDataSourceValue());
        BeanUtils.copyProperties(commonConditionReqBO,dynamicQueryBo);
        dynamicQueryBos.add(dynamicQueryBo);
    }

}
