/** When your routing table is too long, you can split it into small modules **/
export const webList = [
  {
    path: '/generator',
    redirect: '/generator/project',
    alwaysShow: 0,
    meta: {
      title: 'generator',
      icon: 'list',
    },
    children: [
      {
        path: 'project',
        component: 'generator/project',
        name: 'generatorProject',
        meta: {
          title: 'generatorProject',
        },
      },
      {
        path: 'datasource',
        component: 'generator/index',
        name: 'index',
        hidden: 1,
        meta: {
          title: 'generatorSource',
        },
      },
      {
        path: 'initProject',
        component: 'generator/init',
        name: 'index',
        hidden: 1,
        meta: { title: 'initProject', icon: '', noCache: true, breadcrumb: false },
      },
      {
        path: 'config',
        component: 'generator/config',
        name: 'generatorConfig',
        hidden: 1,
        meta: {
          title: 'generatorConfig',
          noCache: true,
          breadcrumb: false,
        },
      },
      {
        path: 'preview',
        component: 'generator/preview',
        name: 'generatorPreview',
        hidden: 1,
        meta: {
          title: 'generatorPreview',
          noCache: true,
          breadcrumb: false,
        },
      },
    ],
  },
  {
    path: '/alipay_config',
    redirect: '/alipay_config/index',
    alwaysShow: 0,
    meta: {
      title: '代码生成示例',
      icon: 'list',
    },
    children: [
      {
        path: 'index',
        component: 'alipay_config/index',
        name: 'alipay-config',
        meta: {
          title: 'alipay-config',
        },
      },

      {
        path: 'preview',
        component: 'alipay_config/component/detail',
        name: 'preview',
        hidden: 1,
        meta: {
          title: 'generatorPreview',
          noCache: true,
          breadcrumb: false,
        },
      },
      {
        path: 'device',
        component: 'deviceInfo/index',
        name: 'deviceInfo',
        meta: {
          title: 'deviceInfo',
        },
      },

      {
        path: 'deviceInfoDetail',
        component: 'deviceInfo/component/detail',
        name: 'deviceInfoDetail',
        hidden: 1,
        meta: {
          title: 'deviceInfoDetail',
          noCache: true,
          breadcrumb: false,
        },
      },
      {
        path: 'deviceModel',
        component: 'deviceModel/index',
        name: 'deviceModel',
        meta: {
          title: 'deviceModel',
        },
      },

      {
        path: 'deviceModelDetail',
        component: 'deviceModel/component/detail',
        name: 'deviceModelDetail',
        hidden: 1,
        meta: {
          title: 'deviceModelDetail',
          noCache: true,
          breadcrumb: false,
        },
      },
      {
        path: 'deviceLog',
        component: 'deviceLogDetail/index',
        name: 'deviceLog',
        meta: {
          title: 'deviceLog',
        },
      },

      {
        path: 'deviceLogDetail',
        component: 'deviceLogDetail/component/detail',
        name: 'deviceLogDetail',
        hidden: 1,
        meta: {
          title: 'deviceLogDetail',
          noCache: true,
          breadcrumb: false,
        },
      },
    ],
  },
  // 系统设置
  {
    sort: '30',
    path: '/system-set',
    redirect: '/system-set/data-dictionary',
    name: 'systemSet',
    alwaysShow: 1,
    meta: {
      title: '系统设置',
      icon: 'systemSet',
    },
    children: [
      // 字典管理
      {
        sort: '3010',
        path: 'data-dictionary',
        component: 'system-set/data-dictionary/index',
        name: 'dataDictionary',
        meta: {
          title: '字典管理',
          icon: '',
        },
      },
      {
        sort: '3060',
        path: 'dict-item',
        component: 'system-set/data-dictionary/dict-item',
        hidden: 1,
        name: 'dictItem',
        meta: {
          title: '字典项',
          icon: '',
        },
      },
      // 参数管理
      {
        sort: '3020',
        path: 'parameter',
        component: 'system-set/parameter/index',
        name: 'parameter',
        meta: {
          title: '参数管理',
          icon: '',
        },
      },
      // 参数管理
      {
        sort: '3030',
        path: '/system-set/parameter/edit',
        // component: () => import('@/views/system-set/parameter/component/edit'),
        component: 'system-set/parameter/component/edit',
        // name: 'parameterEdit',
        hidden: 1,
        meta: { title: 'changePassword', icon: '', noCache: true, breadcrumb: false },
      },
      // 帮助中心
      {
        sort: '3040',
        path: 'support',
        component: 'system-set/support/index',
        name: 'support',
        meta: {
          title: '帮助中心',
          icon: '',
        },
      },
      // 操作日志
      {
        sort: '3050',
        path: 'operation-log',
        component: 'system-set/operation-log',
        name: 'operationLog',
        meta: {
          title: '操作日志',
          icon: '',
        },
      },
    ],
  },
  // 消息管理
  {
    sort: '40',
    path: '/push-notify',
    redirect: '/push-notify/history',
    name: 'pushNotify',
    alwaysShow: 1,
    meta: {
      title: '消息管理',
      icon: 'pushNotify',
    },
    children: [
      // 收发概况
      {
        sort: '4010',
        path: 'situation',
        component: 'push-notify/situation',
        name: 'situation',
        meta: {
          title: '收发概况',
          icon: '',
        },
      },
      // 推送模板
      {
        sort: '4020',
        path: 'template',
        component: 'push-notify/template/index',
        name: 'template',
        meta: {
          title: '推送模板',
          icon: '',
        },
      },
      // 推送历史
      {
        sort: '4030',
        path: 'history',
        component: 'push-notify/history/index',
        name: 'history',
        meta: {
          title: '推送历史',
          icon: '',
        },
      },
    ],
  },
  // 组件中心
  {
    sort: '50',
    path: '/component-center',
    redirect: '/component-center/advanced-list',
    name: 'componentCenter',
    meta: {
      title: '组件中心',
      icon: 'componentCenter',
    },
    alwaysShow: true,
    children: [
      {
        sort: '5010',
        path: 'advanced-list',
        component: 'component-center/advanced-list',
        name: 'advancedList',
        meta: {
          title: '高级查询/动态列',
          icon: '',
        },
      },
      {
        sort: '5020',
        path: 'demo',
        component: 'component-center/demo',
        name: 'demo',
        meta: {
          title: 'CRUD组件化案例',
          icon: '',
        },
      },
      {
        sort: '5030',
        path: 'list',
        component: 'list/index',
        name: 'list',
        meta: {
          title: 'mock数据',
        },
      },
    ],
  },
  // 导出中心
  {
    sort: '60',
    path: '/download',
    redirect: '/download/index',
    alwaysShow: 0,
    name: 'downloadMenu',
    children: [
      {
        sort: '6010',
        path: 'index',
        component: 'download/index',
        name: 'download',
        meta: {
          title: '导出中心',
          icon: 'download',
        },
      },
    ],
  },
]
