package com.anjiplus.template.gaea.generator.modules.project.dao.entity;

import java.io.Serializable;

import com.anji.plus.gaea.curd.entity.GaeaBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * (Project)实体类
 *
 * @author makejava
 * @since 2021-03-03 09:54:22
 */
@TableName("gaea_generator_project")
public class Project extends GaeaBaseEntity implements Serializable {
    /**
     * 项目名称
     */
    private String name;
    /**
     * 项目code
     */
    private String code;
    private String projectRoot;
    private String gitUser;

    private String gitPasswd;

    private String gitRepo;
    /**
     * 开发分支
     */
    private String devBranch;
    /**
     * 生成分支
     */
    private String genBranch;
    /**
     * db类型
     */
    private String dbType;

    private String dbSchema;
    /**
     *
     */
    private String dbConnectionUrl;
    /**
     *
     */
    private String dbUser;

    private String dbPasswd;

    private String status;

    public String getProjectRoot() {
        return projectRoot;
    }

    public void setProjectRoot(String projectRoot) {
        this.projectRoot = projectRoot;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getGitUser() {
        return gitUser;
    }

    public void setGitUser(String gitUser) {
        this.gitUser = gitUser;
    }

    public String getGitPasswd() {
        return gitPasswd;
    }

    public void setGitPasswd(String gitPasswd) {
        this.gitPasswd = gitPasswd;
    }

    public String getGitRepo() {
        return gitRepo;
    }

    public void setGitRepo(String gitRepo) {
        this.gitRepo = gitRepo;
    }

    public String getDbType() {
        return dbType;
    }

    public void setDbType(String dbType) {
        this.dbType = dbType;
    }

    public String getDbSchema() {
        return dbSchema;
    }

    public void setDbSchema(String dbSchema) {
        this.dbSchema = dbSchema;
    }

    public String getDbUser() {
        return dbUser;
    }

    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }

    public String getDbPasswd() {
        return dbPasswd;
    }

    public void setDbPasswd(String dbPasswd) {
        this.dbPasswd = dbPasswd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDevBranch() {
        return devBranch;
    }

    public void setDevBranch(String devBranch) {
        this.devBranch = devBranch;
    }

    public String getGenBranch() {
        return genBranch;
    }

    public void setGenBranch(String genBranch) {
        this.genBranch = genBranch;
    }

    public String getDbConnectionUrl() {
        return dbConnectionUrl;
    }

    public void setDbConnectionUrl(String dbConnectionUrl) {
        this.dbConnectionUrl = dbConnectionUrl;
    }
}
