package com.anji.plus.gaea.security.plus.modules.role.controller.param;

import java.io.Serializable;
import java.util.List;

/**
 * 功能描述：
 * 角色 机构关联
 * @Author: peiyanni
 * @Date: 2021/2/3 17:46
 */
public class RoleOrgReqParam implements Serializable {
    private String roleCode;
    private List<String> orgCodes;

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public List<String> getOrgCodes() {
        return orgCodes;
    }

    public void setOrgCodes(List<String> orgCodes) {
        this.orgCodes = orgCodes;
    }
}
