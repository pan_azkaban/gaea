<#--noinspection ALL-->
<template>
  <div class="">
      <header class="header">${apiAlias}</header>
      <el-table ref="table" :data="this.detail" size="small" style="width: 100%;" >
        <#--<el-table-column type="selection" width="55" />-->
  <#if columns??>
     <#list columns as column>
        <#if column.columnShow>
          <#if (column.dictName)?? && (column.dictName)!="">
        <el-table-column prop="${column.changeColumnName}Cn" label="<#if column.remark != ''>${column.remark}<#else>${column.changeColumnName}</#if>">
         <#-- <template slot-scope="scope">
            {{ dict.label.${column.dictName}[scope.row.${column.changeColumnName}] }}
          </template>-->
        </el-table-column>
          <#else>
        <el-table-column prop="${column.changeColumnName}" label="<#if column.remark != ''>${column.remark}<#else>${column.changeColumnName}</#if>" />
          </#if>
        </#if>
     </#list>
  </#if>
        <!-- v-if="checkPer(['admin','${changeClassName}:edit','${changeClassName}:del'])"-->
        <el-table-column  label="操作" width="200px" align="center">
          <template slot-scope="scope">

          </template>
        </el-table-column>
      </el-table>
  </div>
</template>

<script>

const defaultForm = { <#if columns??><#list columns as column>${column.changeColumnName}: null<#if column_has_next>, </#if></#list></#if> }

export default {
  name: '${className}PreviewItem',

  data() {
    return {
      detail:{}
    }
  },
  methods: {

  },
  created() {

  }
}
</script>

<style scoped>

</style>
