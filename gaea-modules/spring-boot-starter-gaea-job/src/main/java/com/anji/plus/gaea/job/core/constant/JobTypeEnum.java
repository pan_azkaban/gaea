package com.anji.plus.gaea.job.core.constant;
/**
 * @desc Job类型
 * @author 木子李*de
 * @date 2023-05-12 15:25:35.961
 **/
public enum JobTypeEnum {
    JOB_TYPE_BEAN("JavaBean"),
    JOB_TYPE_SQL("SQL");

    private String value;

    JobTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static JobTypeEnum match(String val){
        for (JobTypeEnum item: JobTypeEnum.values()) {
            if (item.value.equals(val)) {
                return item;
            }
        }
        return null;
    }
}
