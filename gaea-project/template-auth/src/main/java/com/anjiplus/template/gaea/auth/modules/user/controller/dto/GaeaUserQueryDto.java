package com.anjiplus.template.gaea.auth.modules.user.controller.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 功能描述：
 * 用户高级查询
 * @Author: peiyanni
 * @Date: 2021/3/9 12:37
 */
@Getter
@Setter
public class GaeaUserQueryDto extends GaeaUserDTO implements Serializable {
    /**
     * 机构
     */
    private String orgNames;
    /**
     * 角色
     */
    private String roleNames;
}
