insert into gaea_menu(menu_code, menu_name, sys_code, parent_code, path,
                      menu_icon,sort,enabled,delete_flag, redirect_url, component,
                      create_by, update_by, create_time, update_time, version)values
('GaeaUiI18n','ui文字国际化处理','PC','SystemSet','gaeaUiI18n','',3060,1,	0,'','system-set/gaeaUiI18n/index','admin','管理员',now(),now(),1);

insert into gaea_authority(application_name, scan_annotation, auth_code, auth_name, parent_code, path,
                           create_time, update_time, version) values
('gaea-business', 0, 'GaeaUiI18nController#pageList', 'uiI18n查询', 'GaeaUiI18nController', 'GET#/gaeaUiI18n/pageList', now(), now(), 1),
('gaea-business', 0, 'GaeaUiI18nController#detail', 'uiI18n详情', 'GaeaUiI18nController', 'GET#/gaeaUiI18n/**', now(), now(), 1),
('gaea-business', 0, 'GaeaUiI18nController#update', 'uiI18n修改', 'GaeaUiI18nController', 'PUT#/gaeaUiI18n/', now(), now(), 1),
('gaea-business', 0, 'GaeaUiI18nController#insert', 'uiI18n新增', 'GaeaUiI18nController', 'POST#/gaeaUiI18n/', now(), now(), 1),
('gaea-business', 0, 'GaeaUiI18nController#deleteById', 'uiI18n删除', 'GaeaUiI18nController', 'DELETE#/gaeaUiI18n/**', now(), now(), 1)
;

insert into gaea_menu_authority(org_code, menu_code, auth_code, create_by,
                                update_by, create_time, update_time, version)VALUES
('3000004','GaeaUiI18n','GaeaUiI18nController#pageList','','',now(),now(),1),
('3000004','GaeaUiI18n','GaeaUiI18nController#detail','','',now(),now(),1),
('3000004','GaeaUiI18n','GaeaUiI18nController#update','','',now(),now(),1),
('3000004','GaeaUiI18n','GaeaUiI18nController#insert','','',now(),now(),1),
('3000004','GaeaUiI18n','GaeaUiI18nController#deleteById','','',now(),now(),1);