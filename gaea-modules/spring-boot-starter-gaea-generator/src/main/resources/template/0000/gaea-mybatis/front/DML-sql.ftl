insert into gaea_menu(menu_code, menu_name, sys_code, parent_code, path,
                      menu_icon,sort,enabled,delete_flag, redirect_url, component,
                      create_by, update_by, create_time, update_time, version)values
('${className}','${apiAlias}','PC','SystemSet','${changeClassName}','',3060,1,	0,'','system-set/${changeClassName}/index','admin','管理员',now(),now(),1);

insert into gaea_authority(application_name, scan_annotation, auth_code, auth_name, parent_code, path,
                           create_time, update_time, version) values
('gaea-business', 0, '${className}Controller#pageList', 'uiI18n查询', '${className}Controller', 'GET#/${changeClassName}/pageList', now(), now(), 1),
('gaea-business', 0, '${className}Controller#detail', 'uiI18n详情', '${className}Controller', 'GET#/${changeClassName}/**', now(), now(), 1),
('gaea-business', 0, '${className}Controller#update', 'uiI18n修改', '${className}Controller', 'PUT#/${changeClassName}/', now(), now(), 1),
('gaea-business', 0, '${className}Controller#insert', 'uiI18n新增', '${className}Controller', 'POST#/${changeClassName}/', now(), now(), 1),
('gaea-business', 0, '${className}Controller#deleteById', 'uiI18n删除', '${className}Controller', 'DELETE#/${changeClassName}/**', now(), now(), 1)
;

insert into gaea_menu_authority(org_code, menu_code, auth_code, create_by,
                                update_by, create_time, update_time, version)VALUES
('3000004','${className}','${className}Controller#pageList','','',now(),now(),1),
('3000004','${className}','${className}Controller#detail','','',now(),now(),1),
('3000004','${className}','${className}Controller#update','','',now(),now(),1),
('3000004','${className}','${className}Controller#insert','','',now(),now(),1),
('3000004','${className}','${className}Controller#deleteById','','',now(),now(),1);