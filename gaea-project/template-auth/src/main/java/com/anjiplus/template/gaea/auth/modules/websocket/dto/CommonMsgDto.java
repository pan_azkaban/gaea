package com.anjiplus.template.gaea.auth.modules.websocket.dto;

import com.anjiplus.template.gaea.common.enums.RelationTypeEnums;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 功能描述：
 *
 * @Author: peiyanni
 * @Date: 2021/4/9 13:46
 */
@Getter
@Setter
public class CommonMsgDto implements Serializable {
    /**
     * 消息id
     */
    private String id;
    /**
     * 消息内容
     */
    private String message;
    /**
     * 关联业务Id，如果没有则不传
     */
    private String eventData;
    /**
     * 关联业务类型，如果没有则不传
     */
    private RelationTypeEnums eventType;

    public CommonMsgDto(String id, String message,RelationTypeEnums eventType) {
        this.id = id;
        this.message = message;
        this.eventType=eventType;
    }

    public CommonMsgDto(String id, String message,String eventData,RelationTypeEnums eventType) {
        this.id = id;
        this.message = message;
        this.eventData=eventData;
        this.eventType=eventType;
    }

}
