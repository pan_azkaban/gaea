package com.anjiplus.template.gaea.business.modules.inf.inf.dao.entity;

import com.anji.plus.gaea.curd.entity.GaeaBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 接口维护表(inf_history)表实体类
 *
 * @author ultrajiaming
 * @since 2021-03-17 10:17:22
 */
@TableName(value = "gaea_inf_history")
@Data
public class InfHistoryEntity extends GaeaBaseEntity {
    //接口名称
    private String infName;
    //接口描述
    private String infDescription;
    //sql语句
    private String sqlSentence;
    // 参数
    private String params;
    // 执行结果(0，成功；1，失败)
    private String executeResult;
    // 失败原因
    private String failureReason;
}
