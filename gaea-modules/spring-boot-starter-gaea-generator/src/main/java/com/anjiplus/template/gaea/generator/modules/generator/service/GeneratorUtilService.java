package com.anjiplus.template.gaea.generator.modules.generator.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.anjiplus.template.gaea.generator.modules.column.dao.GeneratorColumn;
import com.anjiplus.template.gaea.generator.modules.table.dao.GeneratorTable;

/**
 * 代码生成服务，业务系统可扩展自定义生成逻辑
 *
 * @author WongBin
 * @date 2021/4/29
 */
public interface GeneratorUtilService {
    /***
     * 预览代码
     * @param generatorColumns
     * @param generatorTable
     * @return
     */
    List<Map<String, Object>> preview(List<GeneratorColumn> generatorColumns, GeneratorTable generatorTable);

    /***
     * 生成代码
     * @param generatorColumnInfos
     * @param generatorTable
     * @throws IOException
     */
    void generatorCode(List<GeneratorColumn> generatorColumnInfos, GeneratorTable generatorTable) throws IOException;

    /***
     * 下载代码
     * @param generatorColumns
     * @param generatorTable
     * @return
     * @throws IOException
     */
    String download(List<GeneratorColumn> generatorColumns, GeneratorTable generatorTable) throws IOException;

    /**
     * column2java type
     * @param type
     * @return
     */
    String columnToJava(String type);
}
