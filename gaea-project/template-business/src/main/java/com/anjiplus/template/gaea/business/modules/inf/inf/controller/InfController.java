package com.anjiplus.template.gaea.business.modules.inf.inf.controller;

import com.alibaba.fastjson.JSONObject;
import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.curd.controller.GaeaBaseController;
import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anjiplus.template.gaea.business.modules.inf.inf.controller.dto.InfDTO;
import com.anjiplus.template.gaea.business.modules.inf.inf.controller.params.InfParams;
import com.anjiplus.template.gaea.business.modules.inf.inf.dao.entity.InfEntity;
import com.anjiplus.template.gaea.business.modules.inf.inf.service.InfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口维护表(Inf)表控制层
 *
 * @author ultrajiaming
 * @since 2021-03-17 10:17:23
 */
@RestController
@RequestMapping("inf")
public class InfController extends GaeaBaseController<InfParams, InfEntity, InfDTO> {

    @Autowired
    private InfService infService;

    @Override
    public GaeaBaseService<InfParams, InfEntity> getService() {
        return infService;
    }

    @Override
    public InfEntity getEntity() {
        return new InfEntity();
    }

    @Override
    public InfDTO getDTO() {
        return new InfDTO();
    }

    /**
     * 插入
     */
    @PostMapping("add")
    public ResponseBean add(@Validated @RequestBody InfDTO infDto) {
        return this.insert(infDto);
    }

    /**
     * 根据Id 查询
     */
    @GetMapping("id/{id}")
    public ResponseBean queryById(@PathVariable Long id) {
        return responseSuccessWithData(infService.queryById(id));
    }

    /**
     * 修改
     */
    @PostMapping("edit")
    public ResponseBean edit(@Validated @RequestBody InfDTO infDto) {
        return this.update(infDto);
    }


    /**
     * 上线
     */
    @GetMapping("online/{id}")
    public ResponseBean online(@PathVariable Long id) {
        infService.online(id);
        return responseSuccess();
    }

    /**
     * 下线
     */
    @GetMapping("offline/{id}")
    public ResponseBean offline(@PathVariable Long id) {
        infService.offline(id);
        return responseSuccess();
    }

    /**
     * 审核
     */
    @GetMapping("examine/{id}")
    public ResponseBean examine(@PathVariable Long id) {
        infService.examine(id);
        return responseSuccess();
    }

    /**
     * 授权应用
     **/
    @PostMapping("authorize/app/{infName}")
    public ResponseBean authorizeApps(@PathVariable("infName") String infName, @RequestBody String appIds) {
        String authorizeApps = JSONObject.parseObject(appIds).getString("appIds");
        infService.authorizeApps(infName, authorizeApps);
        return responseSuccess();
    }


}
