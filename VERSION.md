<center>版本说明</center>

### V2.1.2[开发中]
````
1.Gaea-push优化，微信、钉钉、邮件
2.fix --map context 值为空，或“”时从entity
3.删除组件spring-boot-starter-gaea-inf
4.QueryEnum.java添加新类型IS_NULL、BWT、NOT_NULL
5.ApplicationUtil增加判断bean是否存在
6.Controller异常处理切面优化javax.validation校验错误码提示
7.优化异常提示时，错误码中的字段名可自定义
  tentCode不能为空，优化后，租户不能为空
8.gaea-export中easyexcel升级到3.0.5
9.前后端代码生成器包名改成spring-boot-starter-gaea-generator

````

### V2.1.1[Release at 2022-3-23]
````
1.Gaea组件增加前后端代码生成器，支持多数据源切换
2.使用Redis scan替换keys，避免redis-server禁用keys而发生异常
3.优化分页bwt时区解析
4.操作日志增加租户
5.GaeaUtils增加replaceFormatString(String source, String... args)方法
  简化meta:goods:code_name:${tenantCode}:${goodsOwnerCode}:${goodsCode}格式化
6.增加gaea oss组件，支持上汽云S3和miniio、nfs；内置文件类型白名单
````

### V2.1.0[Release at 2022-01-13]
````
1.修复工程模板中gateway参数whileList命名错误，改成whiteList
2.GaeaBaseService 查询条件 wrapper可以外部传入
3.优化----时区feign调用时以时间戳交互
4.增加数据库0时区到客户端时区自动转换时间功能
5.修复GaeaDateUtils days format
6.cache数据批量获取，BeanUtils.copyList性能优化
7.token生成可扩展，生成异常，账号租户过期等由子类扩展实现
8.userDetail获取优化、authcode生成优化
9.GaeaBaseController日志输出优化
10.aj-captcha jar升级到1.3.0
11.refresh cache优化
````