package com.anjiplus.template.gaea.business.modules.inf.app.controller.dto;

import com.anji.plus.gaea.curd.dto.GaeaBaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
/**
 * 接口应用关系表(GaeaInfAppRelation)实体类
 *
 * @author ultrajiaming
 * @since 2021-04-08 17:37:09
 */
@ApiModel(value = "接口应用关系表")
public class GaeaInfAppRelationDTO extends GaeaBaseDTO implements Serializable {
            /**
    * 应用ID
    */
    @ApiModelProperty(value = "应用ID")
    private String appId;
        /**
    * 应用名称
    */
    @ApiModelProperty(value = "应用名称")
    private String appName;
        /**
    * 接口标识
    */
    @ApiModelProperty(value = "接口标识")
    private String infName;
        /**
    * 接口名称
    */
    @ApiModelProperty(value = "接口名称")
    private String infNameName;
        /**
    * 调用次数
    */
    @ApiModelProperty(value = "调用次数")
    private Integer times;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getInfName() {
        return infName;
    }

    public void setInfName(String infName) {
        this.infName = infName;
    }

    public String getInfNameName() {
        return infNameName;
    }

    public void setInfNameName(String infNameName) {
        this.infNameName = infNameName;
    }

    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }


}
