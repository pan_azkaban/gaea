package com.anjiplus.template.gaea.business.modules.rules.dao.entity;

import com.anji.plus.gaea.annotation.Unique;
import com.anji.plus.gaea.curd.entity.GaeaBaseEntity;
import com.anjiplus.template.gaea.business.code.ResponseCode;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
/**
 * (GaeaRules)实体类
 *
 * @author lirui
 * @since 2021-03-12 10:38:19
 */
@TableName("gaea_rules")
public class GaeaRules extends GaeaBaseEntity implements Serializable {

    private String ruleName;

    @Unique(code = ResponseCode.RULE_CODE_EXIST)
    private String ruleCode;

    private String ruleFields;

    private String ruleContent;

    private String ruleDemo;

    private Integer enabled;

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(String ruleCode) {
        this.ruleCode = ruleCode;
    }

    public String getRuleFields() {
        return ruleFields;
    }

    public void setRuleFields(String ruleFields) {
        this.ruleFields = ruleFields;
    }

    public String getRuleContent() {
        return ruleContent;
    }

    public void setRuleContent(String ruleContent) {
        this.ruleContent = ruleContent;
    }

    public String getRuleDemo() {
        return ruleDemo;
    }

    public void setRuleDemo(String ruleDemo) {
        this.ruleDemo = ruleDemo;
    }


}
