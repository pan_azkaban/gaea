package com.anjiplus.template.gaea.business.test;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.anjiplus.template.gaea.business.BusinessApplication;

/**
 * @author WongBin
 * @date 2021/3/26
 */
@SpringBootTest(classes = BusinessApplication.class)
@RunWith(SpringRunner.class)
public class ServiceTestBase {

}
