package com.anji.plus.gaea.job.trigger.dao.entity;

import com.anji.plus.gaea.job.trigger.dao.BaseRowMapper;
import org.springframework.jdbc.core.RowMapper;

import java.util.Date;

/**
 * 定时任务执行器注册表 entity
 * @author 木子李*de
 * @date 2023-05-12 15:25:35.961
 **/
public class MetaJobExecutor {

    private Long id;

    /** 执行器编码，取自执行器springboot的spring.application.name */
    private String executorCode;

    /** 执行器地址，http://ip:port/contextpath/ */
    private String executorAddress;

    /** 注册心跳时间，yyyy-MM-dd HH:mm:ss */
    private Date registoryTime;

    /** 心跳过期时间，从1970年1月1日开始计算的总毫秒数 */
    private Long expireTimemillis;

    /** 可用状态，1--有效期 0--注册心跳60秒超时过期 */
    private Integer available;

    //*********************** getter and setter *************************

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExecutorCode() {
        return executorCode;
    }

    public void setExecutorCode(String executorCode) {
        this.executorCode = executorCode;
    }

    public String getExecutorAddress() {
        return executorAddress;
    }

    public void setExecutorAddress(String executorAddress) {
        this.executorAddress = executorAddress;
    }

    public Date getRegistoryTime() {
        return registoryTime;
    }

    public void setRegistoryTime(Date registoryTime) {
        this.registoryTime = registoryTime;
    }

    public Long getExpireTimemillis() {
        return expireTimemillis;
    }

    public void setExpireTimemillis(Long expireTimemillis) {
        this.expireTimemillis = expireTimemillis;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    //*********************** getRowMapper *************************
    public static final RowMapper<MetaJobExecutor> RowMapper = new BaseRowMapper(MetaJobExecutor.class);
}
