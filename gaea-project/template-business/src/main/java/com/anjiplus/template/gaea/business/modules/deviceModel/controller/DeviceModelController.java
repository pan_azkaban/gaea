
package com.anjiplus.template.gaea.business.modules.devicemodel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.anji.plus.gaea.annotation.AccessKey;
import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.curd.controller.GaeaBaseController;
import com.anji.plus.gaea.curd.service.GaeaBaseService;

import com.anji.plus.gaea.utils.GaeaBeanUtils;
import com.anji.plus.gaea.utils.GaeaUtils;

import com.anjiplus.template.gaea.business.modules.devicemodel.dao.entity.DeviceModel;
import com.anjiplus.template.gaea.business.modules.devicemodel.service.DeviceModelService;
import com.anjiplus.template.gaea.business.modules.devicemodel.controller.dto.DeviceModelDto;
import com.anjiplus.template.gaea.business.modules.devicemodel.controller.param.DeviceModelParam;

import io.swagger.annotations.Api;

/**
* @desc 设备类型 controller
* @website https://gitee.com/anji-plus/gaea
* @author dev-user
* @date 2021-04-07 17:27:35.130
**/
@RestController
@Api(tags = "设备类型管理")
@RequestMapping("/deviceModel")
public class DeviceModelController extends GaeaBaseController<DeviceModelParam, DeviceModel, DeviceModelDto> {

    @Autowired
    private DeviceModelService deviceModelService;

    @Override
    public GaeaBaseService<DeviceModelParam, DeviceModel> getService() {
        return deviceModelService;
    }

    @Override
    public DeviceModel getEntity() {
        return new DeviceModel();
    }

    @Override
    public DeviceModelDto getDTO() {
        return new DeviceModelDto();
    }


    @GetMapping({"/{id}"})
    @AccessKey
    @Override
    public ResponseBean detail(@PathVariable("id") Long id) {
        this.logger.info("{}根据ID查询服务开始，id为：{}", this.getClass().getSimpleName(), id);
        DeviceModel result = deviceModelService.getDetail(id);
        DeviceModelDto dto = this.getDTO();
        GaeaBeanUtils.copyAndFormatter(result, dto);
        ResponseBean responseBean = this.responseSuccessWithData(this.resultDtoHandle(dto));
        this.logger.info("{}根据ID查询结束，结果：{}", this.getClass().getSimpleName(), GaeaUtils.toJSONString(responseBean));
        return responseBean;
    }
}