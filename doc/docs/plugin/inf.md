# 接口

## 介绍
接口**spring-boot-starter-gaea-inf**需要结合盖亚底盘一起使用。
主要动态提供接口，通过配置简单的sql语句，生成api接口，以供调用获取数据。

## 配置
#### 1.pom.xml中加入依赖如下，即可使用
```xml
 <dependency>
        <groupId>com.anji-plus</groupId>``
        <artifactId>spring-boot-gaea</artifactId>
 </dependency>

<dependency>
        <groupId>com.anji-plus</groupId>
        <artifactId>spring-boot-starter-gaea-inf</artifactId>
 </dependency>
```
## 功能说明
#### 1.创建应用，生成秘钥
![输入图片说明](https://images.gitee.com/uploads/images/2021/0408/134306_5f812373_1741092.png "应用.png")
#### 2.创建接口
![输入图片说明](https://images.gitee.com/uploads/images/2021/0408/134522_7619672a_1741092.png "创建接口.png")
#### 3.审核接口
![输入图片说明](https://images.gitee.com/uploads/images/2021/0408/134651_e054ac16_1741092.png "接口审核.png")
#### 4.接口上线
![输入图片说明](https://images.gitee.com/uploads/images/2021/0408/135257_1754b4f0_1741092.png "接口上线.png")
#### 5.接口授权应用
![输入图片说明](https://images.gitee.com/uploads/images/2021/0408/134825_9ac58b9f_1741092.png "接口授权.png")
#### 6.查看API文档
![输入图片说明](https://images.gitee.com/uploads/images/2021/0408/135306_72fa0c93_1741092.png "API文档.png")
#### 7.接口调用，请求头中放入app id和app secret
![输入图片说明](https://images.gitee.com/uploads/images/2021/0408/135029_6d4ed089_1741092.png "接口调用请求头.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0408/135040_adc0979f_1741092.png "接口调用.png")