package com.anji.plus.gaea.job.executor.handler.param;

import com.alibaba.fastjson.JSONObject;
import com.anji.plus.gaea.job.core.util.ObjectUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @desc Job信息
 * @author 木子李*de
 * @date 2023-05-12 15:25:35.961
 **/
public class JobInfo {

    private Long jobId;

    /** 租户编码，为空时表示系统级 */
    private String tenantCode;

    /** 机构编码，为空时表示系统级 */
    private String orgCode;

    /** 执行器编码，取自执行器springboot的spring.application.name */
    private String executorCode;

    /** 任务名称 */
    private String jobName;

    /** 任务--执行器任务参数 */
    private String jobParams;

    private Map<String,Object> jobParamsMap = new HashMap<String,Object>();

    /** 在启用getParamList、getTotalCount下，分页处理页大小 */
    private Integer jobBatch;

    //*********************** getter and setter *************************

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getExecutorCode() {
        return executorCode;
    }

    public void setExecutorCode(String executorCode) {
        this.executorCode = executorCode;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobParams() {
        return jobParams;
    }

    public void setJobParams(String jobParams) {
        this.jobParams = jobParams;
        try{
            if(ObjectUtil.isBlank(jobParams)){
                return;
            }
            JSONObject json = JSONObject.parseObject(jobParams);
            jobParamsMap.clear();
            jobParamsMap.putAll(json);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Object getJobParam(String key){
        return jobParamsMap.get(key);
    }

    public Map<String, Object> getJobParamsMap() {
        return jobParamsMap;
    }

    public Integer getJobBatch() {
        return jobBatch;
    }

    public void setJobBatch(Integer jobBatch) {
        this.jobBatch = jobBatch;
    }
}
