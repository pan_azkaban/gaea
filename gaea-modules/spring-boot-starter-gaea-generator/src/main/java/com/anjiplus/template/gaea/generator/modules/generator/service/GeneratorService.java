/*
 *  Copyright 2019-2020 Zheng Jie
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.anjiplus.template.gaea.generator.modules.generator.service;

import com.anji.plus.gaea.bean.KeyValue;
import com.anjiplus.template.gaea.generator.modules.column.dao.GeneratorColumn;
import com.anjiplus.template.gaea.generator.modules.generator.controller.param.GeneratorParam;
import com.anjiplus.template.gaea.generator.modules.table.dao.GeneratorTable;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @author Zheng Jie
 * @date 2019-01-02
 */
public interface GeneratorService {

    /**
     * 查询数据库元数据
     * @param generatorParam
     * @return /
     */
    Page getTables(GeneratorParam generatorParam);

    /**
     * 得到数据表的元数据
     * @param name 表名
     * @return /
     */
    List<GeneratorColumn> getColumns(String schema, String name);

    /**
     * 同步表数据
     * @param generatorColumnInfos /
     * @param generatorColumnInfoList /
     */
    void sync(List<GeneratorColumn> generatorColumnInfos, List<GeneratorColumn> generatorColumnInfoList);

    /**
     * 保持数据
     * @param generatorColumnInfos /
     */
    void saveColumnGenConfig(List<GeneratorColumn> generatorColumnInfos);

    /**
     * 获取所有table
     * @param schema
     * @return /
     */
    Object getTables(String schema);

    /**
     * 代码生成
     * @param generatorTable 配置信息
     * @param generatorColumns 字段信息
     */
    void generator(GeneratorTable generatorTable, List<GeneratorColumn> generatorColumns);

    /**
     * 预览
     * @param generatorTable 配置信息
     * @param generatorColumns 字段信息
     * @return /
     */
    List<Map<String, Object>> preview(GeneratorTable generatorTable, List<GeneratorColumn> generatorColumns);

    /**
     * 打包下载
     * @param generatorTable 配置信息
     * @param generatorColumns 字段信息
     * @param request /
     * @param response /
     */
    void download(GeneratorTable generatorTable, List<GeneratorColumn> generatorColumns,
                  HttpServletRequest request, HttpServletResponse response);

    /**
     * 查询数据库的表字段数据数据
     * @param table /
     * @return /
     */
    List<GeneratorColumn> queryColumns(String schema, String table);

    /***
     *
     * @return
     */
    List<KeyValue> getSchemas();
}
