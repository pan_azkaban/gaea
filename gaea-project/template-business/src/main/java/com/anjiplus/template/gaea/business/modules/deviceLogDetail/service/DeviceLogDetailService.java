
package com.anjiplus.template.gaea.business.modules.devicelogdetail.service;

import com.anjiplus.template.gaea.business.modules.devicelogdetail.dao.entity.DeviceLogDetail;
import com.anjiplus.template.gaea.business.modules.devicelogdetail.controller.param.DeviceLogDetailParam;
import com.anji.plus.gaea.curd.service.GaeaBaseService;

/**
* @desc DeviceLogDetail 设备日志明细服务接口
* @author dev-user
* @date 2021-04-07 17:06:42.920
**/
public interface DeviceLogDetailService extends GaeaBaseService<DeviceLogDetailParam, DeviceLogDetail> {

    /***
     * 查询详情
     *
     * @param id
     * @return
     */
    DeviceLogDetail getDetail(Long id);
}