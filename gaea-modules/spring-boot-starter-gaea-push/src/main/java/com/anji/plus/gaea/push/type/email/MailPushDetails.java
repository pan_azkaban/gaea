package com.anji.plus.gaea.push.type.email;

import com.anji.plus.gaea.push.type.base.BasePushDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.InputStream;
import java.util.Date;
import java.util.Map;

/**
 * 邮件发送明细
 *
 * @author lr
 * @since 2021-02-08
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MailPushDetails  extends BasePushDetails {


    /**
     * 发送主题
     */
    private String subject;

    /**
     * 收件人
     */
    private String to;

    /**
     * 发送人
     */
    private String from;

    /**
     * 抄送
     */
    private String cc;

    /**
     * 密送
     */
    private String bcc;

    /**
     * 邮件内容
     */
    private String text;

    /**
     * 是否是html
     */
    private boolean html;

    /**
     * 发送日期
     */
    private Date sentDate;

    private String param;

    private Map<String, InputStream> fileMap;

    private Map paramMap;

    private String secret;

    private String sign;
}
