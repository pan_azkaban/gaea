package com.anjiplus.template.gaea.business.modules.rules.service;

import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anjiplus.template.gaea.business.modules.rules.controller.param.GaeaRulesParam;
import com.anjiplus.template.gaea.business.modules.rules.dao.entity.GaeaRules;

/**
 * (GaeaRules)Service
 *
 * @author lirui
 * @since 2021-03-12 10:38:19
 */
public interface GaeaRulesService extends GaeaBaseService<GaeaRulesParam, GaeaRules> {

    /**
     * 执行规则
     * @param rulesCode
     * @param rulesContent
     * @param rulesDemo
     * @return
     */
    ResponseBean executeRules(String rulesCode, String rulesContent, String rulesDemo);
}
