import drawerTableItem from './drawerTableItem'
export default {
  name: 'DrawerTableBody',
  components: { drawerTableItem },
  props: {
    tableTitles: {
      type: Array,
      require: false,
      default: () => [],
    },
  },
  render(h) {
    const tableTitles = this.tableTitles || []
    const renderItem = this.renderItem
    return <div class='drawerTableBody'>{tableTitles.map((item) => renderItem(item))}</div>
  },
  methods: {
    renderItem(title) {
      return (
        <drawerTableItem name={title} key={title}>
          <slot />
        </drawerTableItem>
      )
    },
  },
}
