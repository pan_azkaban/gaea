/*
 *  Copyright 2019-2020 Zheng Jie
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.anjiplus.template.gaea.generator.modules.table.service;


import com.anjiplus.template.gaea.generator.modules.table.dao.GeneratorTable;

/**
 * @author Zheng Jie
 * @date 2019-01-14
 */
public interface TableService {

    /**
     * 查询表配置
     * @param schemaName schemaName
     * @param tableName 表名
     * @return 表配置
     */
    GeneratorTable find(String schemaName, String tableName);

    /**
     * 更新表配置
     * @param schemaName schemaName
     * @param tableName 表名
     * @param generatorTable 表配置
     * @return 表配置
     */
    GeneratorTable update(String schemaName, String tableName, GeneratorTable generatorTable);

    /**
     * 扩展接口，表配置初始化时逻辑可自定义
     * @param cfg
     */
    default void initGenConfig(GeneratorTable cfg){};
}
