package com.anji.plus.gaea.security.contants;

/**
 * 用户
 * @author lr
 * @since 2021-07-15
 */
public enum UserTypeEnum {
    /**
     * 平台
     */
    PLATFORM(0),
    /**
     * 租户
     */
    TENANT(1),
    /**
     * 其他普通用户
     */
    OTHER(2);

    private int type;

    UserTypeEnum(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
