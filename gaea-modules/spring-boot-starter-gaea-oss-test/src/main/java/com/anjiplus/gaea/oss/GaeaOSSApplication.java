package com.anjiplus.gaea.oss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * 文件存储示例
 *
 * @author 木子李·De
 * @since 2021/2/3 14:16
 */
@SpringBootApplication
public class GaeaOSSApplication {

	public static void main(String[] args) {
		SpringApplication.run(GaeaOSSApplication.class, args);
	}

}
