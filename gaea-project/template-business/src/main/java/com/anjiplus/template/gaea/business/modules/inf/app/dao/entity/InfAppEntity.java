package com.anjiplus.template.gaea.business.modules.inf.app.dao.entity;

import com.anji.plus.gaea.annotation.Unique;
import com.anji.plus.gaea.curd.entity.GaeaBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * (InfApp)表实体类
 *
 * @author ultrajiaming
 * @since 2021-03-18 23:27:35
 */
@TableName(value = "gaea_inf_app")
@Data
public class InfAppEntity extends GaeaBaseEntity {
    //应用名称
    private String appName;
    //应用ID
    @Unique
    private String appId;
    //应用秘钥
    private String appSecret;
    //1：有效 0：无效
    private Integer status;
    //描述
    private String remark;
}
