package com.anjiplus.template.gaea.auth.modules.thirdapis.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 功能描述：
 *
 * @Author: peiyanni
 * @Date: 2021/4/6 10:33
 */
@Getter
@Setter
public class PushMsgResponse implements Serializable {
    /**
     * 业务返回码，0000表示成功
     */
    private String repCode;
    /**
     * 业务日志、异常信息
     */
    private String repMsg;
    /**
     * 请求业务数据包、详情见下
     */
    private Object repData;
}
