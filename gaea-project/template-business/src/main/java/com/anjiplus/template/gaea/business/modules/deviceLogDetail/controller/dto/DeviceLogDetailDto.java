
package com.anjiplus.template.gaea.business.modules.devicelogdetail.controller.dto;

import java.io.Serializable;

import com.anji.plus.gaea.curd.dto.GaeaBaseDTO;
import lombok.Data;
import java.sql.Timestamp;


/**
*
* @description 设备日志明细 dto
* @author dev-user
* @date 2021-04-07 17:06:42.920
**/
@Data
public class DeviceLogDetailDto extends GaeaBaseDTO implements Serializable {
    /** 设备编号 */
    private Long deviceId;

    /** 日志内容 */
    private String log;

}