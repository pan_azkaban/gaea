package com.anjiplus.template.gaea.business.test;

import com.alibaba.fastjson.JSON;
import com.anjiplus.template.gaea.generator.modules.generator.controller.param.GeneratorParam;
import com.anjiplus.template.gaea.generator.modules.generator.service.GeneratorService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author WongBin
 * @date 2021/4/26
 */
public class CodeGeneratorTest extends ServiceTestBase {

    @Autowired
    GeneratorService generatorService;

    @Autowired
    //@Qualifier("jpaImpl")
    GeneratorService jpaImpl;

    @Test
    public void testGetTables(){
        Object ret = generatorService.getTables("");
        Object ret1 = jpaImpl.getTables("");
        System.out.println(JSON.toJSONString(ret));
        System.out.println(JSON.toJSONString(ret1));
        //assert JSON.toJSONString(ret).equals(JSON.toJSONString(ret1));
    }

    @Test
    public void testGetTablesPage(){
        GeneratorParam generatorParam = new GeneratorParam();
        generatorParam.setSchema("");
        generatorParam.setTableName("");

        Object ret = generatorService.getTables(generatorParam);
        //Object ret1 = jpaImpl.getTables("",name,page);
        System.out.println(JSON.toJSONString(ret));
//        System.out.println(JSON.toJSONString(ret1));
//        assert JSON.toJSONString(ret).equals(JSON.toJSONString(ret1));
    }

    @Test
    public void testGetColumns(){
        String name = "t_alipay_config";
        Object ret = generatorService.queryColumns("",name);
        Object ret1 = jpaImpl.queryColumns("",name);
        System.out.println(JSON.toJSONString(ret));
        System.out.println(JSON.toJSONString(ret1));
        assert JSON.toJSONString(ret).equals(JSON.toJSONString(ret1));
    }
}
