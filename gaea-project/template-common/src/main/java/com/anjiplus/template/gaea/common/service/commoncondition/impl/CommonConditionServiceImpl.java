package com.anjiplus.template.gaea.common.service.commoncondition.impl;

import com.alibaba.fastjson.JSON;
import com.anjiplus.template.gaea.common.dto.DynamicQueryBo;
import com.anjiplus.template.gaea.common.feign.AuthCommServiceClient;
import com.anji.plus.gaea.bean.ResponseBean;
import com.anjiplus.template.gaea.common.service.cache.CacheService;
import com.anjiplus.template.gaea.common.service.commoncondition.ICommonConditionService;
import com.anjiplus.template.gaea.common.CacheConstants;
import com.anjiplus.template.gaea.common.CacheTimeConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <pre>
 * CommonConditionServiceImpl
 * </pre>
 *
 * @author peiyanni
 * @version CommonConditionServiceImpl.java,
 */
@Service
public class CommonConditionServiceImpl implements ICommonConditionService {
    @Autowired
    private CacheService cacheService;

    @Autowired
    private AuthCommServiceClient authCommServiceClient;

    @Override
    public List<DynamicQueryBo> getDynamicQueryBoListById(Long id, List<DynamicQueryBo> paramList) {
        List<DynamicQueryBo> result = new ArrayList<>();
        if (!CollectionUtils.isEmpty(paramList)) {
            result.addAll(paramList);
        }
        // 先查缓存
        String key = CacheConstants.COMMON_QUERY_CONDITION + id;
        Serializable serializable = cacheService.get(key);
        if (null == serializable) {
            ResponseBean responseBean = authCommServiceClient.getDynamicQueryBoListById(id);
            if (null != responseBean && null != responseBean.getData()) {
                result.addAll((List<DynamicQueryBo>) responseBean.getData());
                cacheService.add(key, JSON.toJSONString(responseBean.getData()), CacheTimeConstants.QUERY_CONDITION_TIME_THIRTY_DAYS, TimeUnit.DAYS);
            }
        } else {
            result.addAll(JSON.parseArray((String) serializable, DynamicQueryBo.class));
        }
        return result;
    }

}
