package com.anji.plus.gaea.job.core.param;

import java.io.Serializable;

/**
 * @author xuxueli 2020-04-11 22:27
 *
 * Borrowed from xxljob v2.4.0
 */
public class IdleBeatParam implements Serializable {
    private static final long serialVersionUID = 42L;

    public IdleBeatParam() {
    }
    public IdleBeatParam(int jobId) {
        this.jobId = jobId;
    }

    private int jobId;


    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

}