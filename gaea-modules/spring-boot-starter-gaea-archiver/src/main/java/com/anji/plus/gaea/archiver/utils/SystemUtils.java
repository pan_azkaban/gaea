package com.anji.plus.gaea.archiver.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Properties;

/**
 * Created by raodeming on 2021/4/20.
 */
public class SystemUtils {
    private static final Logger log = LoggerFactory.getLogger(SystemUtils.class);
    public static boolean isLinux() {
        Properties prop = System.getProperties();

        String os = prop.getProperty("os.name");
        if (os != null && os.toLowerCase().indexOf("linux") > -1) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isWindows() {
        Properties prop = System.getProperties();

        String os = prop.getProperty("os.name");
        if (os != null && os.toLowerCase().indexOf("window") > -1) {
            return true;
        } else {
            return false;
        }
    }

    /***
     *  @param sb 命令脚本
     *
     */
    public static String runCmd(String sb) {
        boolean isLinux = SystemUtils.isLinux();
        Process process = null;
        String encoding = "UTF-8";
        try {
            if (isLinux) {
                encoding = "UTF-8";
                String[] cmds = new String[]{"/bin/sh", "-c", sb};
                process = Runtime.getRuntime().exec(cmds);
            } else {
                encoding = "GBK";
                String str = "cmd /c " + sb;
                process = Runtime.getRuntime().exec(str);
            }
            try (SequenceInputStream sis = new SequenceInputStream(process.getInputStream(),
                    process.getErrorStream());
                 InputStreamReader isr = new InputStreamReader(sis, encoding);
                 BufferedReader br = new BufferedReader(isr);
                 OutputStreamWriter osw = new OutputStreamWriter(process.getOutputStream());
                 BufferedWriter bw = new BufferedWriter(osw)
            ) {
                String line = null;
                while (null != (line = br.readLine())) {
                    log.info("=============>"+line);
                    if (!line.contains("Warning")) {
                        throw new RuntimeException(line);
                    }
                }
                bw.flush();
            }

            int ret = process.waitFor();
            log.info("---process-run-return:{}",ret);
            return ret+"";
        } catch (Exception ex) {
            log.error("cmd-exec-error:{}", sb, ex);
            return ex.getCause()==null?ex.getMessage():ex.getCause().getMessage();
        } finally {
            if (process != null) {
                process.destroy();
                log.info("cmd-exec:{},ret:{}", sb, process.exitValue());
            }
        }
    }

    public static String exeCmd(String commandStr) {
        boolean isLinux = SystemUtils.isLinux();
        String result = null;
        Process process;
        String encoding;
        try {
            if (isLinux) {
                encoding = "UTF-8";
                String[] cmd = new String[]{"/bin/sh", "-c",commandStr};
                process = Runtime.getRuntime().exec(cmd);
            } else {
                encoding = "GBK";
                String cmd = "cmd /c " + commandStr;
                process = Runtime.getRuntime().exec(cmd);
            }


            StringBuffer sb = new StringBuffer();
            try (SequenceInputStream sis = new SequenceInputStream(process.getInputStream(),
                    process.getErrorStream());
                 InputStreamReader isr = new InputStreamReader(sis, encoding);
                 BufferedReader br = new BufferedReader(isr);
                 OutputStreamWriter osw = new OutputStreamWriter(process.getOutputStream());
                 BufferedWriter bw = new BufferedWriter(osw)
            ) {
                String line;
                while (null != (line = br.readLine())) {
                    sb.append(line).append("</br>");
                }
                bw.flush();
            }
            result = sb.toString();
        } catch (Exception e) {
            log.error("exec error", e);
        }
        return result;
    }

}

