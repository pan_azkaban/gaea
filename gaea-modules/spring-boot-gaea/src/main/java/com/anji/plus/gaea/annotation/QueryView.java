package com.anji.plus.gaea.annotation;

import java.lang.annotation.*;

/**
 * 查询标记--需要脱敏的字段做掩码处理
 * @author wongbin
 * @date 2021/12/16
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD,ElementType.TYPE_PARAMETER,ElementType.METHOD})
@Documented
public @interface QueryView {

}
