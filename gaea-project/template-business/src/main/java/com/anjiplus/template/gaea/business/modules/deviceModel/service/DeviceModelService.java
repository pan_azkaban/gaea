
package com.anjiplus.template.gaea.business.modules.devicemodel.service;

import com.anjiplus.template.gaea.business.modules.devicemodel.dao.entity.DeviceModel;
import com.anjiplus.template.gaea.business.modules.devicemodel.controller.param.DeviceModelParam;
import com.anji.plus.gaea.curd.service.GaeaBaseService;

/**
* @desc DeviceModel 设备类型服务接口
* @author dev-user
* @date 2021-04-07 17:27:35.130
**/
public interface DeviceModelService extends GaeaBaseService<DeviceModelParam, DeviceModel> {

    /***
     * 查询详情
     *
     * @param id
     * @return
     */
    DeviceModel getDetail(Long id);
}