package com.anjiplus.template.gaea.business.modules.inf.constant;

/**
 * @author ultrajiaming
 * @since 2021/3/29 12:10
 */
public interface InfResCode {
    String FAIL = "500"; // 系统异常 // System error
    String INSERT_FAIL = "inf_1001"; // 插入失败 // Insert fail
    String AUTHORIZE_APP_FAIL = "inf_1002"; // 授权应用失败 // Authorize app fail
    String APP_NOT_EXIST = "inf_2001"; // 应用不存在 // App not exist
    String INF_NOT_EXIST = "inf_2002"; // 接口不存在 // Interface not exist
    String REQUEST_PARAM_NOT_NULL = "inf_2003"; // 请求参数不能为空 // Request param cannot be empty
    String ID_NOT_NULL = "inf_2004"; // ID不能为空 // Id cannot be empty
    String APP_ID_NOT_NULL = "inf_2005"; // 应用ID不能为空 // App id cannot be empty
    String APP_NAME_NOT_NULL = "inf_2006"; // 应用名称不能为空 // App name cannot be empty
    String INF_NAME_NOT_NULL = "inf_2007"; // 接口标识不能为空 // Interface identification cannot be empty
    String INF_DESC_NOT_NULL = "inf_2008"; // 接口描述不能为空 // Interface description cannot be empty
    String SQL_SEQUENCE_NOT_NULL = "inf_2009"; // SQL语句不能为空 // Sql sequence cannot be empty
    String APP_SECRET_NOT_NULL = "inf_2010"; // 请求秘钥不能为空 // App secret cannot be empty
    String DUPLICATE_APP_ID = "inf_3001"; // 应用ID不能重复 // App id cannot be repeated
    String DUPLICATE_INF_NAME = "inf_3002"; // 接口标识不能重复 // Interface name cannot be repeated
    String APP_ID_CANNOT_EDIT = "inf_3003"; // 应用ID不能修改 // App id cannot edit
    String INF_NAME_CANNOT_EDIT = "inf_3004"; // 接口标识不能修改 // Interface identification cannot edit
    String SQL_SEQUENCE_ERROR_$ = "inf_3005"; // SQL语句非法，禁止用${}填充参数 // Sql error, cannot use ${} fill parameters
    String SQL_SEQUENCE_SELECT_ERROR = "inf_3006"; // 查询语句非法 // Select sql error
    String SQL_SEQUENCE_ERROR_SEMICOLON = "inf_3007"; // SQL语句非法，不能包含分号 // Select sql error cannot contains semicolon
    String SQL_SEQUENCE_ERROR_NOTES = "inf_3008"; // SQL语句非法，不能包含注释 // Select sql error cannot contains notes
    String APP_UNUSEFUL_ERROR = "inf_3009"; // 该应用已禁用 // The app is unuseful
    String PUBLISHED_CANNOT_EDIT = "inf_4001"; // 已上线的接口不能编辑 // The online interface cannot be edited
    String PUBLISHED_CANNOT_DELETE = "inf_4002"; // 已上线的接口不能删除 // The online interface cannot delete
    String DUPLICATE_PUBLISHED = "inf_4003"; // 已经上线的接口不需要重复上线 // Interface that are already online do not need to be online repeatedly
    String ONLY_EXAMINE_CAN_PUBLISH = "inf_4004"; // 只有审核通过的接口可以上线 // Only the examined interfaces can go online
    String ONLY_PUBLISHED_CAN_OFFLINE = "inf_4005"; // 只有已上线的接口才可以下线 // Only the online interface can be offline
    String ONLY_PUBLISHED_CAN_EXECUTE = "inf_4006"; // 只有已上线的接口才可以调用 // Only the online interface can be executed
    String INTERFACE_NOT_AUTHENTICATED = "inf_4007"; // 接口未授权，请先授权该应用 // The interface is not authorized. Please authorize the application first
    String ONLY_TESTED_CAN_EXAMINE = "inf_4008"; // 只有测试通过的接口可以审核 // Only the tested interfaces can examine
    String PUBLISHED_CANNOT_TEST = "inf_4009"; // 已上线的接口不能测试 // The online interface cannot test
    String CALL_NUMBER_IS_OVER = "inf_4010"; // 调用次数已用完 // The number of calls has been exhausted
    String SECRET_AUTHENTICATED_FAIL = "inf_5001"; // 秘钥校验失败 // App secret verification failed
    String JSON_FORMAT_ERROR = "inf_5002"; // 请求参数不是JSON格式 // The request parameter is not in JSON format

}
