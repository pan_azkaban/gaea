package com.anji.plus.gaea.push.type.dingding;

import com.anji.plus.gaea.push.type.base.BasePushDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @ClassName DingDingPushDetails
 * @Author dingkaiqiang
 * @Date 2021-04-07
 * @Version V1.0
 **/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DingDingPushDetails extends BasePushDetails {

    private Long agentId;
    // 是否发送给所有人
    private Boolean toAllUser;
    // 消息类型
    private String msgType;
    // 内容
    private String content;
    // 手机号，多个用, 隔开
    private String mobles;


    /**
     * 标题
     */
    private String title;

    /**
     * 图片连接
     */
    private String imageUrl;
    /**
     * 二级标题
     */
    private String secondTitle;

    /**
     * 消息内容，支持markdown，语法参考标准markdown语法。建议1000个字符以内
     */
    private String markdown;

    /**
     * 简单标题
     */
    private String singleTitle;

}
