<#--noinspection ALL-->
<!-- **i18n版本-->
<template>
  <div class="app-container">
    <!--工具栏-->
    <div class="head-container">
    <#if hasQuery>
      <el-form v-if="crud.props.searchToggle" size="small" label-width="120px">
        <!-- 搜索 -->
        <el-row :gutter="10">
        <#if queryColumns??>
          <#list queryColumns as column>
            <#if column.queryType != 'BetWeen'>
          <el-col :xs="24" :sm="20" :md="cols" :lg="cols" :xl="cols">
            <!--<#if column.remark != ''>${column.remark}<#else>${column.changeColumnName}</#if>-->
            <el-form-item :label="tf('${column.changeColumnName}')">
        <#if column.formType =='Select'>
          <#if (column.dictName)?? && (column.dictName)!="">
              <Dictionary v-model="query.${column.changeColumnName}" :updata-dict="query.${column.changeColumnName}" :dict-key="'${column.dictName}'" />
          <#else>
            no Select options
          </#if>
        <#else>
              <el-input v-model="query.${column.changeColumnName}" clearable :placeholder="tf('${column.changeColumnName}')"
                        class="filter-item" @keyup.enter.native="crud.toQuery" />
        </#if>
            </el-form-item>
          </el-col>
            </#if>
           </#list>
        </#if>
  <#if betweens??>
          <el-col :xs="24" :sm="20" :md="cols" :lg="cols" :xl="cols">
    <#list betweens as column>
      <#if column.queryType = 'BetWeen'>
            <date-range-picker v-model="query.${column.changeColumnName}"
          start-placeholder="${column.changeColumnName}Start"
          end-placeholder="${column.changeColumnName}Start"
          class="date-item" />
      </#if>
    </#list>
         </el-col>
  </#if>
         <el-col :xs="24" :sm="20" :md="cols" :lg="cols" :xl="cols">
           <el-form-item label="" label-width="28px">
             <rrOperation :crud="crud" />
           </el-form-item>
         </el-col>
        </el-row>
      </el-form>
    </#if>
      <!--more-button ,slot = 'left' or 'right'-->
      <crudOperation :permission="permission" />

      <!--表单组件-->
      <el-dialog :close-on-click-modal="false" :before-close="crud.cancelCU"
                 :visible.sync="crud.status.cu > 0" :title="crud.status.title" width="75%">
        <el-form ref="form" :model="form" <#if isNotNullColumns??>:rules="rules"</#if> size="small" label-width="120px">
        <el-row :gutter="10">
    <#if columns??>
      <#list columns as column>
        <#if column.formShow>
        <el-col :xs="24" :sm="20" :md="editCols" :lg="editCols" :xl="editCols">
          <!--<#if column.remark != ''>${column.remark}<#else>${column.changeColumnName}</#if>" -->
          <el-form-item :label="tf('${column.changeColumnName}')"<#if column.isNotNull> prop="${column.changeColumnName}"</#if>>
            <#if column.formType = 'Input'>
            <el-input v-model="form.${column.changeColumnName}" />
            <#elseif column.formType = 'Textarea'>
            <el-input v-model="form.${column.changeColumnName}" :rows="3" type="textarea" />
            <#elseif column.formType = 'Radio'>
              <#if (column.dictName)?? && (column.dictName)!="">
            <el-radio v-model="form.${column.changeColumnName}" v-for="item in dict.${column.dictName}" :key="item.id" :label="item.value">{{ item.label }}</el-radio>
              <#else>
                未设置字典，请手动设置 Radio
              </#if>
            <#elseif column.formType = 'Select'>
              <#if (column.dictName)?? && (column.dictName)!="">
            <Dictionary v-model="form.${column.changeColumnName}" :updata-dict="form.${column.changeColumnName}<#if column.columnType!='String'>+''</#if>" :dict-key="'${column.dictName}'" />
              <#else>
            未设置字典，请手动设置 Select
              </#if>
            <#else>
            <el-date-picker v-model="form.${column.changeColumnName}" type="datetime" />
            </#if>
          </el-form-item>
        </el-col>
        </#if>
      </#list>
    </#if>
        </el-row>
      </el-form>
      <div slot="footer" class="dialog-footer">
          <el-button type="text" @click="crud.cancelCU">{{ $t('btn.cancel') }}</el-button>
          <el-button :loading="crud.status.cu === 2" type="primary"
                     @click="crud.submitCU">{{ $t('btn.confirm') }}</el-button>
      </div>
    </el-dialog>

      <!--表格渲染-->
      <el-table ref="table" v-loading="crud.loading" :data="crud.data" size="small"
                style="width: 100%;" @selection-change="crud.selectionChangeHandler">
        <el-table-column type="selection" width="55" />
        <#if columns??>
            <#list columns as column>
            <#if column.columnShow>
          <#if (column.dictName)?? && (column.dictName)!="">
        <#--<#if column.remark != ''>${column.remark}<#else>${column.changeColumnName}</#if> -->
        <el-table-column prop="${column.changeColumnName}Cn" :label="tf('${column.changeColumnName}')" />
                <#else>
        <#--<#if column.remark != ''>${column.remark}<#else>${column.changeColumnName}</#if> -->
        <el-table-column prop="${column.changeColumnName}" :label="tf('${column.changeColumnName}')" />
                </#if>
            </#if>
            </#list>
        </#if>
        <!-- v-if="checkPer(['admin','${changeClassName}:edit','${changeClassName}:del'])"-->
        <el-table-column  :label="$t('btn.operationCol')" width="200px" align="center">
          <template slot-scope="scope">
            <udOperation
              :data="scope.row"
              :permission="permission"
            />
          </template>
        </el-table-column>
      </el-table>
      <!--分页组件-->
      <pagination />

      <preview ref="previewForm" :module="i18nQry.module" />
    </div>
  </div>
</template>

<script>
import crud${className} from '@/api/${changeClassName}'
import CRUD, { presenter, header, form, crud } from '@crud/crud'
import rrOperation from '@crud/RR.operation'
import crudOperation from '@crud/CRUD.operation'
import udOperation from '@crud/UD.operation'
import pagination from '@crud/Pagination'
import preview from './component/detail'
<#if hasDict>
import Dictionary from '@/components/Dictionary/index'
</#if>
import i18nHandler from '@/mixins/i18nHandler'
import i18n from '@/lang'

const defaultForm = { <#if columns??><#list columns as c>${c.changeColumnName}: <#if c.defaultValue??>c.defaultValue<#else>null</#if> <#if c_has_next>, </#if></#list></#if> }

/**ui国际化初始化参数,菜单模块 menu_code**/
const i18nQry = {module: '${className}',refer: '${tableName}<#if subEntity??><#list subEntity as s>,${s.dbTableName}:r${s_index}</#list></#if>'}

export default {
  name: '${className}',
  components: {<#if hasDict>Dictionary,</#if> pagination, crudOperation, rrOperation, udOperation,preview },
  mixins: [presenter(), header(), form(defaultForm), crud(),i18nHandler],

  cruds() {
    return CRUD({ title: i18n.t('${className}.moduleName'),
        url: 'business/${changeClassName}/pageList', idField: '${pkChangeColName}',
        sort: '${pkChangeColName}',order:'DESC', crudMethod: { ...crud${className} }})
  },
  data() {
    return {
      cols:6,editCols:12,
      permission: {
        add: '${changeClassName}Controller#insert',
        edit: '${changeClassName}Controller#update',
        del: '${changeClassName}Controller#deleteById'
      },
      <#if hasDict>dictionarys: [],</#if>
      rules: {
        <#if isNotNullColumns??>
        <#list isNotNullColumns as column>
        <#if column.isNotNull>
        ${column.changeColumnName}: [
          { required: true, message: i18n.t('input.required',{t:i18n.t(i18nQry.module+'.${column.changeColumnName}')}), trigger: 'blur' }
        ]<#if column_has_next>,</#if>
        </#if>
        </#list>
        </#if>
      }<#--<#if hasQuery>,
      queryTypeOptions: [
        <#if queryColumns??>
        <#list queryColumns as column>
        <#if column.queryType != 'BetWeen'>
        { key: '${column.changeColumnName}', display_name: '<#if column.remark != ''>${column.remark}<#else>${column.changeColumnName}</#if>' }<#if column_has_next>,</#if>
        </#if>
        </#list>
        </#if>
      ]
      </#if>-->
    }
  },
  methods: {
    [CRUD.HOOK.beforeRefresh]() {
      return true
    }
  },
  created() {
     this.moduleI18nInit(i18nQry)
     this.crud.optShow = {add: true, edit: true,preview:true, del: false, download: false,reset:true}
  }
}
</script>

<style scoped>

</style>
