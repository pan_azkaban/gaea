# 数据归档
## 介绍
目前归档组件只支持mysql数据库（*后面会在补充其他数据库类型*）。
归档组件配置好后，会实现数据自动归档，不需要人为干预。归档的数据会存在一张新表中，对于历史的数据可以设置删除操作。
## 配置
#### 1.pom.xml中加入依赖如下，[查看Maven中央仓库版本](https://mvnrepository.com/artifact/com.anji-plus/spring-boot-starter-gaea-archiver)
```xml
	<dependency>
		<groupId>com.anji-plus</groupId>
		<artifactId>spring-boot-starter-gaea-archiver</artifactId>
    	<version>2.7.0.RELEASE</version>
	</dependency>
```
#### 2.添加配置
```yaml
spring:
  gaea:
    subscribes:
      archiver:
        enabled: true
        archive-scheduled-cron: 0 0 3 2 */1 ?  #归档触发的定时器默认每月2号，凌晨3:0:0执行归档
        max-days-before-archive: 30  #归档多久之前的数据，将n天前的数据，移动到归档表，tables配置项中策略优先加载
        max-days-before-delete: 720  #删除多久之前的历史数据，归档表中，已归档的数据保留期限，超过期限的数据将删除。tables配置项中策略优先加载
        execute-ip: '192.168.1.1' #多台机器，必须指定其中一台，这里配置执行的服务ip
        tables:
          - tablename: gaea_log
            timefield: request_time
            max-days-before-archive: 30
            max-days-before-delete: 720
```
*注：tablename表示归档哪张表，timeField 表示按照哪个时间字段进行归档操作*
#### 3.运行日志
```
2021-03-22 13:01:51.710  INFO ArchiverService : loading gaea archiver
2021-03-22 13:01:52.265  INFO ArchiverService : archive table gaea_log -> gaea_log_202003 success, row count=1
2021-03-22 13:01:52.374  INFO ArchiverService : archive table gaea_log -> gaea_log_202007 success, row count=1
2021-03-22 13:01:52.485  INFO ArchiverService : archive table gaea_log -> gaea_log_202008 success, row count=1
2021-03-22 13:01:52.600  INFO ArchiverService : archive table gaea_log -> gaea_log_202009 success, row count=1
2021-03-22 13:01:52.603  INFO ArchiverService : gaea archiver finish table count 1, time cost 0 second
```

#### 4.归档示例
对gaea_log进行归档，结果如下：
![归档结果](https://gaea.anji-plus.com/business/file/download/fe668a32-3f02-4d22-9431-1fbb7a9bc906)