package com.anji.plus.gaea.security.plus.modules.user.controller;

import com.anji.plus.gaea.annotation.Permission;
import com.anji.plus.gaea.annotation.log.GaeaAuditLog;
import com.anji.plus.gaea.bean.KeyValue;
import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.curd.controller.GaeaBaseController;
import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anji.plus.gaea.security.plus.modules.user.controller.dto.GaeaUserDTO;
import com.anji.plus.gaea.security.plus.modules.user.controller.param.GaeaUserParam;
import com.anji.plus.gaea.security.plus.modules.user.controller.param.GaeaUserPasswordParam;
import com.anji.plus.gaea.security.plus.modules.user.controller.param.UserRoleOrgReqParam;
import com.anji.plus.gaea.security.plus.modules.user.dao.entity.GaeaUser;
import com.anji.plus.gaea.security.plus.modules.user.service.GaeaUserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户表(GaeaUser)实体类
 *
 * @author lirui
 * @since 2021-02-02 13:38:12
 */
@RestController
@RequestMapping("/user")
@Api(value = "/user", tags = "用户表")
@Permission(code = "user", name = "用户")
public class GaeaUserController extends GaeaBaseController<GaeaUserParam, GaeaUser, GaeaUserDTO> {

    @Autowired
    private GaeaUserService gaeaUserService;

    @Override
    public GaeaBaseService<GaeaUserParam, GaeaUser> getService() {
        return gaeaUserService;
    }

    @Override
    public GaeaUser getEntity() {
        return new GaeaUser();
    }

    @Override
    public GaeaUserDTO getDTO() {
        return new GaeaUserDTO();
    }


    /**
     * 保存用户角色机构关联关系
     *
     * @param reqParam
     * @return
     */
    @PostMapping("/saveRoleTree")
    @GaeaAuditLog(pageTitle = "分配用户角色")
    @Permission(code = "role", name = "绑定角色")
    public ResponseBean saveRoleTree(@RequestBody UserRoleOrgReqParam reqParam) {
        Boolean data = gaeaUserService.saveRoleTree(reqParam);
        return responseSuccessWithData(data);
    }

    /**
     * 刷新用户名和用户真实姓名
     * @return
     */
    @PostMapping("/refresh/username")
    @Permission(code = "refreshName", name = "刷新用户名")
    public ResponseBean refreshCache(@RequestBody List<String> usernameList) {
        gaeaUserService.refreshCache(usernameList);
        return responseSuccess();
    }

    /**
     * 用户修改密码
     * @param reqParam
     * @return
     */
    @PostMapping("/updatePassword")
    @GaeaAuditLog(pageTitle = "修改密码")
    @Permission(code = "updatePassword", name = "修改密码")
    public ResponseBean updatePassword(@Validated @RequestBody GaeaUserPasswordParam reqParam){
        return responseSuccessWithData(gaeaUserService.updatePassword(reqParam));
    }

    /**
     * 用户重置密码
     * @param usernames
     * @return
     */
    @PostMapping("/resetPwd")
    @GaeaAuditLog(pageTitle = "重置密码")
    @Permission(code = "resetPassword", name = "重置密码")
    public ResponseBean resetPassword(@RequestBody List<String> usernames){
        gaeaUserService.setDefaultPwd(usernames);
        return responseSuccess();
    }

    /**
     * 用户名与用户真实姓名的下拉列表
     * @return
     */
    @GetMapping("/select")
    public ResponseBean userSelect() {
        List<KeyValue> keyValues = gaeaUserService.userSelect();
        return responseSuccessWithData(keyValues);
    }

    @PostMapping("/unLock")
    @Permission(code = "unLock", name = "解锁")
    public ResponseBean unLock(@RequestBody List<String> usernames) {
        gaeaUserService.unLock(usernames);
        return responseSuccess();
    }

    /**
     * 根据机构编号获取用户信息
     * @param orgCode
     * @return
     */
    @GetMapping("/org/{orgCode}")
    public ResponseBean orgUser(@PathVariable("orgCode") String orgCode) {
        List<GaeaUser> usersByOrgCode = gaeaUserService.getUsersByOrgCode(orgCode);
        return responseSuccessWithData(usersByOrgCode);
    }
}
