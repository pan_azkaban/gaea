package com.anjiplus.template.gaea.common;

/**
 * <pre>
 * MagicValueConstants
 * </pre>
 * 魔法值常量
 *
 * @author peiyanni
 * @version MagicValueConstants.java,
 */
public final class MagicValueConstants {


    private MagicValueConstants() {
    }
    public static final boolean FALSE = false;
    //接口返回结果成功状态
    public static final boolean TRUE = true;
    public static final String DEFAULT_PASSWORD="123456";

    public static final String GAEA = "GAEA";
    //接口返回结果成功状态
    public static final String SUCCESS = "SUCCESS";
    //魔法值
    public static final String STRING_ZERO = "0";


    public static final String STRING_ONE = "1";
    public static final String STRING_TWO = "2";
    //魔法值 0
    public static final int ZERO = 0;

    //魔法值 0
    public static final long ZEROL = 0;

    //魔法值 1
    public static final int ONE = 1;
    //魔法值 1
    public static final int TWO = 2;

    //魔法值 10
    public static final int TEN = 10;
    public static final int THOUSAND=1000;

    public static final String ADMIN = "admin";
    //高级查询相关
    public static final String COMMONID = "commonId";
    public static final String QUERYWRAPPER = "queryWrapper";

    //redis 消息监听
    public static final String HANDLER_NAME = "handlerName";
    public static final String REDIS_TOPIC_NAME="gaea_redis_topic";


}
