package com.anjiplus.gaea.archivertest;

import java.util.Date;

import com.anji.plus.gaea.archiver.service.ArchiverService;
import com.anji.plus.gaea.archiver.utils.DateUtil;
import com.anji.plus.gaea.archiver.utils.SystemUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
/**
 * 归档测试示例
 *
 * @author 木子李·De
 * @since 2021/2/3 14:16
 */
@SpringBootTest(classes = ArchiverApplication.class)
class ArchiverApplicationTests {

	@Autowired
	private ArchiverService archiverService;

	@Test
	void contextLoads() {
		archiverService.doArchiveTable();
		System.out.println("finish");
	}

	@Test
	void runExec(){
		String a = SystemUtils.exeCmd("mysqldump --set-gtid-purged=OFF --single-transaction -h10.108.26.197 -P3306 -uroot -pappuser@anji --databases gaea_auth --tables gaea_role gaea_org  > D:\\backup\\20210420171003852.sql");
		System.out.println("----");
		System.out.println(a);
	}


	@Test
	public void test(){
//		String lastest = "202112";
//		String fmt = "yyyyMMdd";
//		long nextArchive = DateUtil.addDays(DateUtil.parse(lastest+"01",fmt),365).getTime();
//		String next = DateUtil.format(new Date(nextArchive),fmt);
//		int n = Integer.valueOf(next);
//		System.out.println(n);
	}

}
