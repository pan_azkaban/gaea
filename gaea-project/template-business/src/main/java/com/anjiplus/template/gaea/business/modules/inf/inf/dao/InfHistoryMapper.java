package com.anjiplus.template.gaea.business.modules.inf.inf.dao;

import com.anjiplus.template.gaea.business.modules.inf.inf.dao.entity.InfHistoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface InfHistoryMapper extends BaseMapper<InfHistoryEntity> {

}
