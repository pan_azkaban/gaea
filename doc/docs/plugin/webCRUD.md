#  介绍
CRUD组件**page-template**是一个CRUD页面快速成型组件。传统的页面开发需要关注三个部分html,css,js，而此组件只需关注js部分，完全实现了以数据构建页面，更方便非专业前端开发基础CRUD页面。

组件源码地址: [AJ-Gaea\web-template\src\components\AnjiPlus\PageTemplate.vue](https://gitee.com/anji-plus/gaea/blob/master/web-template/src/components/AnjiPlus/PageTemplate.vue)
#  应用
## 组件属性值
### page-template组件共有5个属性值

| 参数 | 说明      |  类型 |
|:--------:|:-------------:|:-------------:|
|   prop-api   |  增删改查接口对象，具体选项参考[prop-api详细说明](#item1 "item1") | object
|   prop-btn   |    按钮权限对象，具体选项参考[prop-btn详细说明](#item2 "item2")  | object
|   prop-table |  列表相关对象，具体选项参考[prop-table详细说明](#item3 "item3")   | object
|   prop-dialog  |   弹窗form表单相关对象，具体选项参考[prop-from/prop-dialog 详细说明](#item4 "item4")   | object
|  prop-from | 查询条件form表单相关对象，具体选项参考[prop-from/prop-dialog 详细说明](#item4 "item4") | object
#### 1、prop-api 详细说明 <a id="item1"></a>
|参数|说明|必须|类型|可选值|默认值|
|:--:|:-------:|:-------:|:------:|:------:|:------:|
|query|查询接口 | 是 | function  |  - |   -   | 
 | add | 新增接口 | 否 |  function   |  - |   -   | 
 | edit | 编辑接口 | 否|function    |  - |   -   | 
 | delete | 删除接口 |  否| function   |  - |   -   | 
 #### 2、prop-btn 详细说明 <a id="item2"></a>
|参数|说明|必须|类型|可选值|默认值|
|:--:|:-------:|:-------:|:------:|:------:|:------:|
 | add | 新增按钮权限code,为假值时代表此按钮一直展示 | 否 |  string   |  - |   ''   | 
 | edit | 编辑按钮权限code,为假值时代表此按钮一直展示 | 否|string    |  - |   ''   | 
 | delete | 删除按钮权限code,为假值时代表此按钮一直展示 |  否| string   |  - |   ''   | 
#### 3、prop-table <a id="item3"></a>详细说明 
|参数|说明|必须|类型|可选值|默认值|
|:--:|:-------:|:-------:|:------:|:------:|:------:|
|border|表格是否带边框|否|boolean|-| true|
|stripe|表格是否斑马纹|否|boolean|-| false|
|height|Table 的高度| 否 |string/number|-| -|
|maxHeight|Table 的最大高度|否 |string/number| - | -|
|hasSelection|是否展示复选框|否|     boolean|   -|      true
|hasIndex|是否展示索引列|否|     boolean|   -|      false
|hasCreateAndupdate| 是否展示创建/更新者和创建时间|   否| boolean | -|  true
|list|所有列的详细信息列表,具体选项参考[list(object) 详细说明](#item31 "item31")|是|object[]|-|-

##### 3.1、list(object) <a id="item31"></a>详细说明 
|参数|说明|必须|类型|可选值|默认值|
|:--:|:-------:|:-------:|:------:|:------:|:------:|
label| 列名称| 是|  string|   -|-| 
| field| 字段名| 是|   string|    -|  -| 
| minWidth| 列最小宽度  | 否 | string|   -|110
|  operate| 第一列是否有查看的功能（高亮可操作）  否|      boolean|   true/false      true
|  custom| 是否需要自定义展示内容(处理字段后再展示)  否|  boolean|   true/false      false
|  renderer| 自定义的dom custom 字段为true时必有此值 否| function|    -| -| 
#### 4、prop-from/prop-dialog <a id="item31"></a>详细说明 
|参数|说明|必须|类型|可选值|默认值|
|:-:|:------:|:------:|:------:|:------:|:------:|
|labelWidth|表单域标签的宽度|否|string|- |'100px'|
|rules|所有表单的验证规则（配置可参照elementui)|否|object| - | -| 
|disabled|所有表单的禁用| 否|boolean |- | false| 
|list|所有查询条件列表,具体选项参考[list(object) 详细说明](#item41 "item41")|是|object[]|-|-| 
##### 4.1、list(object) <a id="item41"></a>详细说明
|参数|说明|必须|类型|可选值|默认值|
|:--:|:--:|:---:|:---:|:--:|:--:|
|formType|form表单类型|否|string |select/input|input|
|label|字段名称-支持国际化|是| string | - | -|
|field|字段名|是|string| - |   -|
|fieldValue|字段初始值|否|-|    -  |      -|
|disabled|该项表单的禁用|否 |boolean|- | false|
| clearable|是否显示清除按钮|否 |boolean|  - |true|
|placeholder|placeholder |否| - |  -  | -|
|rules|单项校验规则（配置可参照elementui)|否|object/array | -| -|
|options|下拉框的可选值| 否  |   object[]|   -| []|
|optionsconfig|下拉框options的配置，具体选项参考[optionsconfig 详细说明](#item411 "item411")|否|object| -|  {}
##### 4.1.1、optionsconfig <a id="item411"></a>详细说明
|参数|说明|必须|类型|可选值|默认值|
|:---:|:--------:|:--------:|:-------:|:-------:|:-------:|
| key|下拉框options的key绑定值|否|string|-|默认用下边label字段的值若label未传/则默值为'label'|
|label|options的label绑定值|否|string| - |'label'|
|value: 'value'|options的value绑定值|否|string |    - |'value'|

## 组件应用示例
demo地址：[AJ-Gaea\web-template\src\views\component-center\demo.vue](https://gitee.com/anji-plus/gaea/blob/master/web-template/src/views/component-center/demo.vue)