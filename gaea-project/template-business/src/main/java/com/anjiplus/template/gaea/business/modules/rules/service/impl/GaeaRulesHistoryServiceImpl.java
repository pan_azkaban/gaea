package com.anjiplus.template.gaea.business.modules.rules.service.impl;

import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import com.anjiplus.template.gaea.business.modules.rules.dao.GaeaRulesHistoryMapper;
import com.anjiplus.template.gaea.business.modules.rules.dao.entity.GaeaRulesHistory;
import com.anjiplus.template.gaea.business.modules.rules.service.GaeaRulesHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * (GaeaRulesHistory)ServiceImpl
 *
 * @author why
 * @since 2021-03-17 13:52:55
 */
@Service
public class GaeaRulesHistoryServiceImpl implements GaeaRulesHistoryService {
    @Autowired
    private GaeaRulesHistoryMapper gaeaRulesHistoryMapper;

    @Override
    public GaeaBaseMapper<GaeaRulesHistory> getMapper() {
        return gaeaRulesHistoryMapper;
    }

}
