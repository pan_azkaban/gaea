package com.anjiplus.template.gaea.auth.modules.thirdapis.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 功能描述：
 * 批量推送
 * @Author: peiyanni
 * @Date: 2021/4/6 10:22
 */
@Getter
@Setter
public class GaeaPushBatchDto extends GaeaPushBaseDto {
    /**
     * 推送标题
     */
    private String title;
    /**
     * 推送内容
     */
    private String content;
    /**
     * 推送类型 1透传消息 0 普通消息（默认0）
     */
    private String pushType;
    /**
     * 设备id列表（最大1000条）
     * 全部推送时，不需要此参数
     */
    private List<String> deviceIds;
}
