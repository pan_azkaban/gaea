package com.anji.plus.gaea.push.type.dingding.template;

import com.anji.plus.gaea.push.type.dingding.DingDingPushDetails;
import com.anji.plus.gaea.push.type.dingding.GaeaPushDingDingProperties;
import com.anji.plus.gaea.push.type.dingding.enums.DingDingMesEnums;
import com.anji.plus.gaea.push.type.param.PushParamVO;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiMessageCorpconversationAsyncsendV2Request;
import com.dingtalk.api.response.OapiMessageCorpconversationAsyncsendV2Response;
import com.taobao.api.ApiException;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

/**
 * @ClassName DingDingTemplate
 * @Description: TODO
 * @Author dingkaiqiang
 * @Date 2021-04-07
 * @Version V1.0
 **/
public class DingDingTemplate extends DingDingOperations {


    /** 应用代理ID*/
    protected static Long AGENTID;
    /** 应用key*/
    protected static String APPKEY;
    /** 应用秘钥*/
    protected static String APPSECRET;

    protected static String TOKEN_URL = "https://oapi.dingtalk.com/gettoken";

    protected static String GET_BY_MOBILE = "https://oapi.dingtalk.com/user/get_by_mobile";

    /** 工作通知消息*/
    protected final static String WORK_MESSAGE_URL = "https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2";


    @Autowired
    private GaeaPushDingDingProperties gaeaPushDingDingProperties;

    @PostConstruct
    public void init(){
        AGENTID = gaeaPushDingDingProperties.getAgentId();
        APPKEY = gaeaPushDingDingProperties.getAppkey();
        APPSECRET = gaeaPushDingDingProperties.getAppSecret();
    }


    public DingDingTemplate() {
    }

    /**
     * 数据转换
     * @param pushParamVO
     * @return
     */
    public DingDingPushDetails convertParam(PushParamVO pushParamVO) {
        DingDingPushDetails.DingDingPushDetailsBuilder builder = DingDingPushDetails.builder();
        builder.agentId(AGENTID);
        builder.toAllUser(false);
        builder.msgType(pushParamVO.getMsgType());
        builder.mobles(pushParamVO.getTo());
        builder.content(pushParamVO.getParam());
        builder.markdown(pushParamVO.getParam());

        builder.title(pushParamVO.getSubject());
        builder.secondTitle("二级标题");
        builder.imageUrl("http://10.108.11.33:8888/group1/M00/00/05/CmwLIV_FpT2AWcuFAACdCCzqMfg136.jpg");
        DingDingPushDetails pushDetails = builder.build();
        return pushDetails;
    }

    /**
     * 发送消息
     * @param pushDetails
     * @return
     */
    public Boolean doSendDingMsg(DingDingPushDetails pushDetails) {
        DingDingMesEnums mesEnums = DingDingMesEnums.valueOf(pushDetails.getMsgType());

        String userId = getUserInfoByMobleBatch(pushDetails.getMobles());
        OapiMessageCorpconversationAsyncsendV2Request request = new OapiMessageCorpconversationAsyncsendV2Request();
        request.setUseridList(userId);
        request.setAgentId(pushDetails.getAgentId());
        request.setToAllUser(pushDetails.getToAllUser());
        OapiMessageCorpconversationAsyncsendV2Request.Msg msg = new OapiMessageCorpconversationAsyncsendV2Request.Msg();
        pushDetails.setSingleTitle("sssssss");
        mesEnums.sendDingMes(pushDetails, msg);
        msg.setMsgtype(pushDetails.getMsgType());

        request.setMsg(msg);
        //发送历史记录保持
        DingTalkClient client = new DefaultDingTalkClient(WORK_MESSAGE_URL);
        String session = AccessTokenItils.getAccessToken();
        Boolean result = false;
        try {
            OapiMessageCorpconversationAsyncsendV2Response response = client.execute(request,session);
            result = response.isSuccess();
            if (result) {
                System.out.println("success--------");
            }
            System.out.println(response.getTaskId());
        } catch (ApiException e) {
            e.printStackTrace();
        }
        return result;
    }

}
