/*
 * @Author: zyk
 * @Date: 2020-07-10 14:23:29
 * @Last Modified by: zyk
 * @Last Modified time: 2021-03-01 12:41:39
 */
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'
import ApiListMain from '@/views/apiList/main'
/* Router 模块 */

/**
 * 路由配置说明
 *
 * hidden: 0                      如果设置为1 / true，项目将不会显示在侧边栏中(默认为0 / false)
 * alwaysShow: 0                  如果设置为1，将始终显示根菜单(默认为0)
 *                                如果没有设置alwaysShow，当项目有多个子路径时，
 *                                它将变成嵌套模式，否则不会显示根菜单
 * redirect: ''                   重定向路径，不设置则不做重定向
 * name:'router-name'             用<keep-alive>组件的时候必须设置
 * meta : {
    title: 'title'               侧边栏显示的名称（国际化时此值是国际化配置文件中的键）
    icon: 'svg-name'             侧边栏菜单的icon图标的名字（svg格式）
    noCache: true                如果设置为true，页面将不会被缓存(默认为false)
    affix: true                  如果设置为true，标签将固定在tags-view视图中
    breadcrumb: false            如果设置为false，项目将隐藏在面包屑中(默认为true)
    activeMenu: '/example/list'  如果设置路径，侧边栏将突出显示您设置的路径
  }
 */

/**
 * 固定的页面路由
 * 不需要任何权限控制的页面
 */
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index'),
      },
    ],
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true,
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true,
  },
  { path: '/report/bigscreen/designer', component: () => import('@/views/report/bigscreen/designer/index'), hidden: true }, // 大屏报表设计器
  { path: '/report/bigscreen/viewer', component: () => import('@/views/report/bigscreen/viewer/index'), hidden: true }, // 大屏报表查看器
  { path: '/report/excelreport/designer', component: () => import('@/views/report/excelreport/designer/index'), hidden: true }, // 表格报表设计器
  { path: '/report/excelreport/viewer', component: () => import('@/views/report/excelreport/viewer/index'), hidden: true }, // 表格报表查看器
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true,
  },
  // 报表设计器和查看器

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard',
        meta: { title: 'dashboard', icon: 'home', affix: true },
      },
    ],
  },
  // {
  //   path: '',
  //   component: Layout,
  //   name: 'SvgDemo',
  //   children: [
  //     {
  //       path: 'svg-demo',
  //       component: () => import('@/icons/demo'),
  //       name: 'SvgDemo',
  //       meta: { title: '图标库', icon: 'icons' },
  //     },
  //   ],
  // },
  {
    path: '',
    component: Layout,
    children: [
      {
        path: '/change-password',
        component: () => import('@/views/system-set/change-password'),
        name: 'ChangePassword',
        hidden: 1,
        meta: { title: 'changePassword', icon: '', noCache: true, breadcrumb: false },
      },
    ],
  },
  {
    path: '/helpCenList',
    component: () => import('@/views/help-center/list'),
    children: [
      { path: 'list', component: () => import('@/views/help-center/list-title'), hidden: true },
      { path: 'detail', component: () => import('@/views/help-center/list-detail'), hidden: true, name: 'helpDetails' },
      { path: 'search', component: () => import('@/views/help-center/list-search'), hidden: true },
    ],
  },
  /*
  {
    path: '/generator',
    name: 'Generator',
    component: Layout,
    meta: { title: '代码生成', icon: 'clipboard' },
    children: [
      { path: 'project', component: () => import('@/views/generator/project'), name: 'Project', meta: { title: '项目列表', icon: '' }},
      { path: 'index', component: () => import('@/views/generator/index'), hidden: 1, name: 'index', meta: { title: '项目数据源', icon: '' }},
      { path: 'config', hidden: 1, component: () => import('@/views/generator/config'), name: 'Config', meta: { title: '生成配置', icon: '' }},
      { path: 'init', component: () => import('@/views/generator/init'), name: 'Config', hidden: 1, meta: { title: '项目初始化', icon: '' }},
      { path: 'preview', component: () => import('@/views/generator/preview'), name: 'Config', hidden: 1, meta: { title: '代码预览', icon: '' }},
      { path: 'alipayConfig', component: () => import('@/views/alipayConfig/index'), name: 'AlipayConfig', meta: { title: '生成示例-单表', icon: '' }},
      { path: 'alipayConfigDetail', component: () => import('@/views/alipayConfig/component/detail'), name: 'alipayConfigDetail', hidden: 1, meta: { title: '生成示例-单表详情', icon: '' }},
      { path: 'deviceInfo', component: () => import('@/views/deviceInfo/index'), name: 'DeviceInfo', meta: { title: '设备信息-主表', icon: '' }},
      { path: 'deviceModel', component: () => import('@/views/deviceModel/index'), name: 'DeviceModel', meta: { title: '设备类型-子表', icon: '' }},
      { path: 'deviceLogDetail', component: () => import('@/views/deviceLogDetail/index'), name: 'DeviceLogDetail', meta: { title: '设备日志-子表', icon: '' }},
      { path: 'deviceInfoDetail', component: () => import('@/views/deviceInfo/component/detail'), name: 'deviceInfoDetail', hidden: 1, meta: { title: '设备信息-详情', icon: '' }},
      { path: 'deviceModelDetail', component: () => import('@/views/deviceModel/component/detail'), name: 'deviceModelDetail', hidden: 1, meta: { title: '设备类型-详情', icon: '' }},
      { path: 'deviceLogDetailInfo', component: () => import('@/views/deviceLogDetail/component/detail'), name: 'deviceLogDetailInfo', hidden: 1, meta: { title: '设备日志-详情', icon: '' }},
    ],
  },
  {
    path: '/open/view/api',
    component: ApiListMain,
    name: 'API',
    openView: true,
    meta: { title: 'API文档', icon: 'api' },
    children: [
      {
        path: 'index/:id',
        component: () => import('@/views/apiList/list'),
        name: 'OpenApiList',
        hidden: true,
        meta: { title: 'API列表', icon: 'api' },
      },
      {
        path: 'detail/:id',
        component: () => import('@/views/apiList/detail'),
        name: 'OpenApiDetail',
        hidden: true,
        meta: { title: '接口明细' },
      },
      {
        path: 'access',
        component: () => import('@/views/apiList/portal/access'),
        name: 'OpenApiAccess',
        hidden: true,
        meta: { title: '服务调用方' },
      },
      {
        path: 'guide',
        component: () => import('@/views/apiList/portal/guide'),
        name: 'OpenApiGuide',
        hidden: true,
        meta: { title: '服务提供方' },
      },
      {
        path: 'param',
        component: () => import('@/views/apiList/portal/param'),
        name: 'OpenApiParam',
        hidden: true,
        meta: { title: '接口参数' },
      },
      {
        path: 'sign',
        component: () => import('@/views/apiList/portal/sign'),
        name: 'OpenApiSign',
        hidden: true,
        meta: { title: '安全方式' },
      },
      {
        path: 'code',
        component: () => import('@/views/apiList/portal/code'),
        name: 'OpenApiCode',
        hidden: true,
        meta: { title: '响应码查询' },
      },
    ],
  },*/
  /*
  {
    path: '/announcement',
    component: Layout,
    meta: { title: '系统通告', icon: 'organization' },
    children: [{ path: 'index', component: () => import('@/views/announcement/index'), name: 'Announcement', meta: { title: '系统通告', icon: '' }}],
  },
  {
    path: '',
    component: Layout,
    children: [
      {
        path: '/my-massage',
        component: () => import('@/views/my-massage/index'),
        name: 'MyMassage',
        hidden: 1,
        meta: { title: '我的消息', icon: '', noCache: true, breadcrumb: false },
      },
      {
        path: '/my-massageDetails',
        component: () => import('@/views/my-massage/component/index'),
        name: 'MyMassage',
        hidden: 1,
        meta: { title: '我的消息', icon: '', noCache: true, breadcrumb: false },
      },
    ],
  },
  { path: '/my-massage/index', component: () => import('@/views/my-massage/index'), hidden: true }, // 我的消息
  */
]

const createRouter = () =>
  new Router({
    // mode: 'history', // 需要服务端支持
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes,
  })

const router = createRouter()

export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // 重置路由
}

export default router
