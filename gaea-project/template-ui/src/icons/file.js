const fs = require('fs')
const path = 'src/icons/'
function getFiles() {
  fs.readdir(path + 'svg', (error, files) => {
    if (files) {
      const list = files
        .filter((item) => item.includes('.svg'))
        .map((item) => {
          return item.split('.svg')[0]
        })
      if (checkedPath()) {
        createSvgList(list)
      } else {
        createFolder(list)
      }
    } else if (error) {
      console.error(error)
    }
  })
}
function checkedPath() {
  return fs.existsSync(path + 'assets/')
}
function createFolder(list) {
  fs.mkdir(path + 'assets/', function(err) {
    if (err) {
      console.error(err)
    } else {
      createSvgList(list)
    }
  })
}
function createSvgList(list) {
  const data = `export default ` + JSON.stringify(list)
  fs.writeFile(path + 'assets/filesName.js', data, (err) => {
    if (err) {
      console.error(err)
    } else {
      console.log('icon更新成功')
    }
  })
}
getFiles()
