
package com.anjiplus.template.gaea.business.modules.alipayconfig.dao.entity;

import lombok.Data;
import io.swagger.annotations.ApiModelProperty;

import com.anji.plus.gaea.curd.entity.GaeaBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;

import javax.validation.constraints.*;
import java.sql.Timestamp;

/**
* @description 支付配置 entity
* @author dev-user
* @date 2021-04-07 14:58:28.131
**/
@TableName(value="t_alipay_config")
@Data
public class AlipayConfig extends GaeaBaseEntity {
    @ApiModelProperty(value = "应用ID")
    private String appId;

    @ApiModelProperty(value = "编码")
    private String charset;

    @ApiModelProperty(value = "类型 ")
    private String format;

    @ApiModelProperty(value = "网关地址")
    private String gatewayUrl;

    @ApiModelProperty(value = "异步回调")
    private String notifyUrl;

    @ApiModelProperty(value = "私钥")
    private String privateKey;

    @ApiModelProperty(value = "公钥")
    private String publicKey;

    @ApiModelProperty(value = "回调地址")
    private String returnUrl;

    @ApiModelProperty(value = "签名方式")
    private String signType;

    @ApiModelProperty(value = "商户号")
    private String sysServiceProviderId;


}