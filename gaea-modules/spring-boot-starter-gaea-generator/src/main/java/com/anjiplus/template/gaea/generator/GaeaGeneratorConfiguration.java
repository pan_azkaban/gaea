package com.anjiplus.template.gaea.generator;

import com.anji.plus.gaea.annotation.condition.ConditionalOnGaeaComponent;
import com.anjiplus.template.gaea.generator.config.GaeaGeneratorProperties;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 代码生成配置
 * @author lr
 * @since 2021-04-15
 */
@Configuration
@EnableConfigurationProperties(GaeaGeneratorProperties.class)
@ConditionalOnGaeaComponent(GaeaGeneratorProperties.COMPONENT_NAME)
@ComponentScan(value = {"com.anjiplus.template.gaea.generator"})
@MapperScan(basePackages = {"com.anjiplus.template.gaea.generator.**.dao"})
public class GaeaGeneratorConfiguration {
}
