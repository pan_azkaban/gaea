package com.anji.plus.gaea.push.type.dingding.template;

import com.anji.plus.gaea.push.type.dingding.GaeaPushDingDingProperties;
import com.anji.plus.gaea.push.type.dingding.template.DingDingTemplate;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiGettokenRequest;
import com.dingtalk.api.request.OapiUserGetByMobileRequest;
import com.dingtalk.api.request.OapiUserGetRequest;
import com.dingtalk.api.response.OapiGettokenResponse;
import com.dingtalk.api.response.OapiUserGetByMobileResponse;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.taobao.api.ApiException;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

/**
 * @ClassName AccessTokenItils
 * @Description: AccessTokenItils
 * @Author dingkaiqiang
 * @Date 2021-04-07
 * @Version V1.0
 **/
public class AccessTokenItils {


    /**
     * 获取accessToken
     * @return
     */
    @PostConstruct
    public static String getAccessToken() {
        String appkey = DingDingTemplate.APPKEY;
        String appsecret = DingDingTemplate.APPSECRET;
        DefaultDingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/gettoken");
        OapiGettokenRequest request = new OapiGettokenRequest();
        request.setAppkey(appkey);
        request.setAppsecret(appsecret);
        request.setHttpMethod("GET");
        try {
            OapiGettokenResponse response = client.execute(request);
            System.out.println(response);
            return response.getAccessToken();

        } catch (ApiException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取用户详情
     * @return
     */
    public static OapiUserGetResponse getUserInfo() {
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
        OapiUserGetRequest request = new OapiUserGetRequest();
        request.setUserid("丁凯强");
        request.setHttpMethod("GET");
        try {
            OapiUserGetResponse response = client.execute(request, getAccessToken());
            System.out.println(response.getErrmsg());
            return response;
        } catch (ApiException e) {
            e.printStackTrace();
            return null;
        }
    }
    /**
     * 根据手机号查询用户id
     * 需要注意ip不在白名单
     * @return
     */
    public static OapiUserGetByMobileResponse getUserInfoByMoble() {
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get_by_mobile");
        OapiUserGetByMobileRequest request = new OapiUserGetByMobileRequest();
        request.setMobile("17621938880");

        try {
            OapiUserGetByMobileResponse execute = client.execute(request, getAccessToken());
            System.out.println("userId:=====  " + execute.getUserid());
            return execute;
        } catch (ApiException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void main(String[] args) {
        //获取用户信息
//        getUserInfo();
        // 根据部门id获取用户列表
//        DingDingUtil.getDDUsersByDeptId(142137434L);
        // 根据手机号获取信息
        getUserInfoByMoble();

    }
}
