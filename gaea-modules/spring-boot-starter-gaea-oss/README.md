
spring:
  gaea:
    subscribes:
      #================================ oss对象存储组件配置 ================================
      oss:
        enabled: true
        file-type-white-list: .png|.jpg|.gif|.icon|.pdf|.xlsx|.xls|.csv|.mp4|.avi #允许上传的文件后缀
        # 若要使用minio文件存储，请启用以下配置
        minio:
          url: http://10.108.3.133
          port: 9000
          access-key: ****
          secret-key: *****
          bucket-name: sili-otwb
        # 若要使用amazonS3文件存储，请启用以下配置
        #amazonS3:
        #  url: http://s3-sh-uat.fin-shine.com
        #  access-key: ***
        #  secret-key: ******
        #  bucket-name: ******
        # 华为云存储：
        huaweiObs:
          url: ''
          access-key: xx
          secret-key: xx
          bucket-name: xxxx
        # 若minio和amazonS3都没有，使用服务器高可用的nfs共享盘
        #nfs:
        #  path: /app/disk/upload/