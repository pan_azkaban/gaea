package com.anjiplus.template.gaea.business.modules.inf.inf.dao;

import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import com.anjiplus.template.gaea.business.modules.inf.inf.dao.entity.InfEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 接口维护表(Inf)表数据库访问层
 *
 * @author ultrajiaming
 * @since 2021-03-17 10:17:25
 */
@Mapper
public interface InfMapper extends GaeaBaseMapper<InfEntity> {

}
