package com.anji.plus.gaea.job.core.param;

import com.alibaba.fastjson.JSONObject;
import com.anji.plus.gaea.job.core.util.ObjectUtil;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by xuxueli on 16/7/22.
 *
 * Borrowed from xxljob v2.4.0
 */

public class TriggerParam implements Serializable {

    private static final long serialVersionUID = 42L;

    private Long jobId;

    /** 租户编码，为空时表示系统级 */
    private String tenantCode;

    /** 机构编码，为空时表示系统级 */
    private String orgCode;

    /** 执行器编码，取自执行器springboot的spring.application.name */
    private String executorCode;

    /** 任务名称 */
    private String jobName;

    /** 任务类型，BEAN/SQL */
    private String jobType;

    /** jobType=BEAN时，存放jobHandler的beanName */
    private String jobHandler;

    /** jobType=SQL时，存放sql */
    private String jobSource;

    /** jobSource更新时间 */
    private long jobSourceUpdateTime;

    /** 任务--阻塞处理策略，SERIAL_EXECUTION单机串行/DISCARD_LATER丢弃后续调度/COVER_EARLY覆盖之前调度 */
    private String jobBlockStrategy;

    /** 任务--执行器任务参数 */
    private String jobParams;

    /** 在启用getParamList、getTotalCount下，分页处理页大小 */
    private Integer jobBatch;

    /** 任务--执行超时时间，单位秒 */
    private int jobTimeout;

    private long logId;
    private long logDateTime;

    private Map<String,Object> jobParamsMap = new HashMap<String,Object>();

    //*********************** getter and setter *************************

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getExecutorCode() {
        return executorCode;
    }

    public void setExecutorCode(String executorCode) {
        this.executorCode = executorCode;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getJobHandler() {
        return jobHandler;
    }

    public void setJobHandler(String jobHandler) {
        this.jobHandler = jobHandler;
    }

    public String getJobSource() {
        return jobSource;
    }

    public void setJobSource(String jobSource) {
        this.jobSource = jobSource;
    }

    public long getJobSourceUpdateTime() {
        return jobSourceUpdateTime;
    }

    public void setJobSourceUpdateTime(long jobSourceUpdateTime) {
        this.jobSourceUpdateTime = jobSourceUpdateTime;
    }

    public String getJobBlockStrategy() {
        return jobBlockStrategy;
    }

    public void setJobBlockStrategy(String jobBlockStrategy) {
        this.jobBlockStrategy = jobBlockStrategy;
    }

    public String getJobParams() {
        return jobParams;
    }

    public void setJobParams(String jobParams) {
        this.jobParams = jobParams;
        try{
            if(ObjectUtil.isBlank(jobParams)){
                return;
            }
            JSONObject json = JSONObject.parseObject(jobParams);
            jobParamsMap.putAll(json);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Map<String, Object> getJobParamsMap() {
        return jobParamsMap;
    }

    public Integer getJobBatch() {
        return jobBatch;
    }

    public void setJobBatch(Integer jobBatch) {
        this.jobBatch = jobBatch;
    }

    public int getJobTimeout() {
        return jobTimeout;
    }

    public void setJobTimeout(int jobTimeout) {
        this.jobTimeout = jobTimeout;
    }

    public long getLogId() {
        return logId;
    }

    public void setLogId(long logId) {
        this.logId = logId;
    }

    public long getLogDateTime() {
        return logDateTime;
    }

    public void setLogDateTime(long logDateTime) {
        this.logDateTime = logDateTime;
    }

    @Override
    public String toString() {
        return "TriggerParam{" +
                "jobId=" + jobId +
                ", tenantCode='" + tenantCode + '\'' +
                ", orgCode='" + orgCode + '\'' +
                ", executorCode='" + executorCode + '\'' +
                ", jobName='" + jobName + '\'' +
                ", jobType='" + jobType + '\'' +
                ", jobSource='" + jobSource + '\'' +
                ", jobSourceUpdateTime=" + jobSourceUpdateTime +
                ", jobHandler='" + jobHandler + '\'' +
                ", jobParams='" + jobParams + '\'' +
                ", jobBatch=" + jobBatch +
                ", jobBlockStrategy='" + jobBlockStrategy + '\'' +
                ", jobTimeout=" + jobTimeout +
                ", logId=" + logId +
                ", logDateTime=" + logDateTime +
                '}';
    }

}

