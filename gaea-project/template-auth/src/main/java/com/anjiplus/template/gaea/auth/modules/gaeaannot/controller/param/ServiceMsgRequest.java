package com.anjiplus.template.gaea.auth.modules.gaeaannot.controller.param;

import com.anjiplus.template.gaea.common.enums.RelationTypeEnums;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 功能描述：
 * 业务相关发送消息,
 * @Author: peiyanni
 * @Date: 2021/4/15 13:27
 */
@Getter
@Setter
public class ServiceMsgRequest implements Serializable {
    /**
     * 消息标题
     */
    @NotBlank(message = "title not empty")
    @Size(max = 30,message = "Title length must not exceed 30")
    private String title;
    /**
     * 消息内容
     */
    @NotBlank(message = "msgContent not empty")
    private String msgContent;
    /**
     * 发布人
     */
    @NotBlank(message = "publisher not empty")
    private String publisher;

    /**
     * 消息接收人，多个用逗号隔开
     */
    @NotBlank(message = "userNames not empty")
    private String userNames;
    /**
     * 消息优先级 （优先级（L低，M中，H高））
     */
    private String priority;
    /**
     * 关联的业务数据（需要查看业务详情时使用）
     */
    private Object relationData;
    /**
     * 关联的业务类型（需要查看业务详情时使用）
     */
    private String relationType;
}
