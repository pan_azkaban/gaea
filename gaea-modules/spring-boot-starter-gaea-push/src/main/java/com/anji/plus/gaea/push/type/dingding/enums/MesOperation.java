package com.anji.plus.gaea.push.type.dingding.enums;

import com.anji.plus.gaea.push.type.dingding.DingDingPushDetails;
import com.dingtalk.api.request.OapiMessageCorpconversationAsyncsendV2Request;

/**
 * @ClassName MesOperation
 * @Description: TODO
 * @Author dingkaiqiang
 * @Date 2021-04-07
 * @Version V1.0
 **/
public interface MesOperation {


    String text = "text";
    String image = "image";
    String file = "file";
    String link = "link";
    String markdown = "markdown";
    String oa = "oa";
    String action_card = "action_card";
/*
    String TEST = "text";
    String IMAGES = "image";
    String FILE = "file";
    String LINK = "link";
    String MARK_DOWM = "markdown";
    String OA = "oa";
    String ACTION_CARD = "action_card";*/

    void sendDingMes(DingDingPushDetails pushDetails, OapiMessageCorpconversationAsyncsendV2Request.Msg msg);
}
