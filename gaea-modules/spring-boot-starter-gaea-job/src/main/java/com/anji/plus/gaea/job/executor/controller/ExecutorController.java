package com.anji.plus.gaea.job.executor.controller;

import com.anji.plus.gaea.job.core.dto.LogResult;
import com.anji.plus.gaea.job.core.dto.ReturnT;
import com.anji.plus.gaea.job.core.param.IdleBeatParam;
import com.anji.plus.gaea.job.core.param.KillParam;
import com.anji.plus.gaea.job.core.param.LogParam;
import com.anji.plus.gaea.job.core.param.TriggerParam;
import com.anji.plus.gaea.job.executor.service.ExecutorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @desc 执行器提供给调度器远程调用接口 controller
 * @author 木子李*de
 * @date 2023-05-12 15:25:35.961
 **/
@RestController
@RequestMapping("/jobexecutor")
public class ExecutorController {
    @Autowired
    private ExecutorService executorService;

    @ResponseBody
    @RequestMapping("/beat")
    public ReturnT<String> beat(){
        return executorService.beat();
    }

    @ResponseBody
    @RequestMapping("/idleBeat")
    public ReturnT<String> idleBeat(@RequestBody IdleBeatParam idleBeatParam){
        return executorService.idleBeat(idleBeatParam);
    }

    @ResponseBody
    @RequestMapping("/run")
    public ReturnT<String> run(@RequestBody TriggerParam triggerParam){
        return executorService.run(triggerParam);
    }

    @ResponseBody
    @RequestMapping("/kill")
    public ReturnT<String> kill(@RequestBody KillParam killParam){
        return executorService.kill(killParam);
    }

    @ResponseBody
    @RequestMapping("/log")
    public ReturnT<LogResult> log(@RequestBody LogParam logParam){
        return executorService.log(logParam);
    }
}
