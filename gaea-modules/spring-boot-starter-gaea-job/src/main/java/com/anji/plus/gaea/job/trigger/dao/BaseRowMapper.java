package com.anji.plus.gaea.job.trigger.dao;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
public class BaseRowMapper<T> implements RowMapper<T> {
    private static final Logger logger = LoggerFactory.getLogger(BaseRowMapper.class);

    private Class<?> targetClazz;
    private HashMap<String, Method> methodHashMap;

    public BaseRowMapper(Class<?> targetClazz) {
        this.targetClazz = targetClazz;
        methodHashMap = new HashMap<>();

        try{
            Field[] fields = targetClazz.getDeclaredFields();
            for (Field field : fields) {
                String fieldName = field.getName();
                if("RowMapper".equals(fieldName) || "logger".equals(fieldName)){
                    continue;
                }
                PropertyDescriptor propDesc = new PropertyDescriptor(fieldName, targetClazz);
                Method method = propDesc.getWriteMethod();
                if(method != null){
                    methodHashMap.put(field.getName(), method);
                } else{
                    logger.warn("not setmethod for field:{} find in class:{} ", fieldName, targetClazz.getName());
                }
            }
        }catch (Exception e){
            logger.warn("init RowMapper for class:{} error:{}", targetClazz.getName(), e);
        }
    }

    @Override
    public T mapRow(ResultSet rs, int arg1) throws SQLException {
        T obj = null;

        try {
            obj = (T) targetClazz.newInstance();

            final ResultSetMetaData metaData = rs.getMetaData();
            int columnLength = metaData.getColumnCount();
            String columnName = null;

            for (int i = 1; i <= columnLength; i++) {
                columnName = metaData.getColumnName(i);
                String fieldName = camel(columnName);
                if(methodHashMap.containsKey(fieldName) == false){
                    continue;
                }

                methodHashMap.get(fieldName).invoke(obj, rs.getObject(columnName));

                /*
                Class fieldClazz = fieldMap.get(fieldName).getType();
                Field field = fieldMap.get(fieldName);
                field.setAccessible(true);

                // fieldClazz == Character.class || fieldClazz == char.class
                if (fieldClazz == int.class || fieldClazz == Integer.class) { // int
                    field.set(obj, rs.getInt(columnName));
                } else if (fieldClazz == boolean.class || fieldClazz == Boolean.class) { // boolean
                    field.set(obj, rs.getBoolean(columnName));
                } else if (fieldClazz == String.class) { // string
                    field.set(obj, rs.getString(columnName));
                } else if (fieldClazz == float.class) { // float
                    field.set(obj, rs.getFloat(columnName));
                } else if (fieldClazz == double.class || fieldClazz == Double.class) { // double
                    field.set(obj, rs.getDouble(columnName));
                } else if (fieldClazz == BigDecimal.class) { // bigdecimal
                    field.set(obj, rs.getBigDecimal(columnName));
                } else if (fieldClazz == short.class || fieldClazz == Short.class) { // short
                    field.set(obj, rs.getShort(columnName));
                } else if (fieldClazz == Date.class) { // date
                    field.set(obj, rs.getDate(columnName));
                } else if (fieldClazz == Timestamp.class) { // timestamp
                    field.set(obj, rs.getTimestamp(columnName));
                } else if (fieldClazz == Long.class || fieldClazz == long.class) { // long
                    field.set(obj, rs.getLong(columnName));
                }

                field.setAccessible(false);
                */
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return obj;
    }

    /**
     * 下划线转驼峰
     * @param str
     * @return
     */
    public static String camel(String str) {
        Pattern pattern = Pattern.compile("_(\\w)");
        Matcher matcher = pattern.matcher(str);
        StringBuffer sb = new StringBuffer(str);
        if(matcher.find()) {
            sb = new StringBuffer();
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
            matcher.appendTail(sb);
        }else {
            return sb.toString();
        }
        return camel(sb.toString());
    }
}
