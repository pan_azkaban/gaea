package com.anji.plus.gaea.curd.dto;

import com.anji.plus.gaea.curd.entity.Converter;

/**
 * 最上层类
 * @author lr
 * @since 2021-03-17
 */
public interface BaseDTO extends Converter {

}
