package com.anjiplus.template.gaea.auth.modules.thirdapis.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 功能描述：
 *
 * @Author: peiyanni
 * @Date: 2021/4/6 11:00
 */
@Getter
@Setter
public class GaeaPushBaseDto implements Serializable {
    /**
     * 应用唯一key
     */
    private String appKey;
    /**
     * 应用秘钥
     */
    private String secretKey;
    /**
     * 厂商通道的token或者regId(华为、oppo、vivo、小米传值)
     */
    private String manuToken;
    /**
     * 极光的用户id
     */
    private String registrationId;
}
