package com.anji.plus.gaea.security.plus.modules.role.controller;

import com.anji.plus.gaea.annotation.Permission;
import com.anji.plus.gaea.annotation.log.GaeaAuditLog;
import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.curd.controller.GaeaBaseController;
import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anji.plus.gaea.security.plus.modules.role.controller.dto.GaeaRoleDTO;
import com.anji.plus.gaea.security.plus.modules.role.controller.dto.RoleMenuAuthorityDTO;
import com.anji.plus.gaea.security.plus.modules.role.controller.param.GaeaRoleParam;
import com.anji.plus.gaea.security.plus.modules.role.dao.entity.GaeaRole;
import com.anji.plus.gaea.security.plus.modules.role.service.GaeaRoleService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 角色(GaeaRole)实体类
 *
 * @author lr
 * @since 2021-02-02 13:37:54
 */
@RestController
@RequestMapping("/role")
@Api(value = "/role", tags = "角色")
@Permission(code = "role", name = "角色")
public class GaeaRoleController extends GaeaBaseController<GaeaRoleParam, GaeaRole, GaeaRoleDTO> {
    @Autowired
    private GaeaRoleService gaeaRoleService;

    @Override
    public GaeaBaseService<GaeaRoleParam, GaeaRole> getService() {
        return gaeaRoleService;
    }

    @Override
    public GaeaRole getEntity() {
        return new GaeaRole();
    }

    @Override
    public GaeaRoleDTO getDTO() {
        return new GaeaRoleDTO();
    }


    /**
     * 保存菜单权限
     * @param dto
     * @return
     */
    @PostMapping("/roleMenuAuthorities")
    @GaeaAuditLog(pageTitle="绑定权限")
    @Permission(code = "auth", name = "绑定权限")
    public ResponseBean saveMenuActionTreeForRole(@RequestBody RoleMenuAuthorityDTO dto){
        //保存角色和权限
        Boolean data=gaeaRoleService.saveMenuAuthority(dto);

        return responseSuccessWithData(data);
    }


    /**
     * 保存菜单
     * @param dto
     * @return
     */
    @PostMapping("/roleMenus")
    @GaeaAuditLog(pageTitle="绑定菜单")
    @Permission(code = "roleMenu", name = "绑定菜单")
    public ResponseBean saveMenuForRole(@RequestBody RoleMenuAuthorityDTO dto){
        //保存角色和权限
        Boolean data=gaeaRoleService.saveMenu(dto);

        return responseSuccessWithData(data);
    }


    /**
     * 获取当前角色拥有的权限按钮
     * @return
     */
    @GetMapping("/tree/authorities/{orgCode}/{roleCode}")
    public ResponseBean roleAuthorities(@PathVariable("orgCode")String orgCode, @PathVariable("roleCode")String roleCode) {
        return responseSuccessWithData(gaeaRoleService.getSelectAuthorities(orgCode,roleCode));
    }
}
