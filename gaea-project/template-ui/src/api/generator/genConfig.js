import request from '@/utils/request'

export function getTableGenConfig(project, tableName) {
  return request({
    url: '/business/api/genConfig/' + project + '/' + tableName,
    method: 'get',
  })
}

export function update(data) {
  return request({
    url: '/business/api/genConfig',
    data,
    method: 'put',
  })
}
export function generatorColumns(data) {
  return request({
    url: '/business/api/generator/columns?pageNumber=' + data.pageNumber + '&pageSize=' + data.pageSize + '&tableName=' + data.tableName,
    method: 'get',
  })
}
