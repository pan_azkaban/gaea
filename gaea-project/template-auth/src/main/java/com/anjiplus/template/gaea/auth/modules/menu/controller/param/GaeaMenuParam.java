package com.anjiplus.template.gaea.auth.modules.menu.controller.param;


import com.anji.plus.gaea.annotation.Query;
import com.anji.plus.gaea.constant.QueryEnum;
import com.anji.plus.gaea.curd.params.PageParam;

import java.io.Serializable;

/**
 * 菜单表(GaeaMenu)param
 *
 * @author lr
 * @since 2021-02-02 13:36:43
 */
public class GaeaMenuParam extends PageParam implements Serializable {
    /**
     * 菜单代码
     */
    private String menuCode;
    /**
     * 菜单名称
     */
    @Query(QueryEnum.LIKE)
    private String menuName;

    /**
     * 系统代码
     */
    private String sysCode;

    /**
     * 启用状态
     */
    private Integer enabled;

    /**
     * 菜单路径
     */
    private String path;


    /**
     * 左边树code
     */
    @Query(where = false)
    private String code;


    public String getMenuCode() {
        return menuCode;
    }

    public void setMenuCode(String menuCode) {
        this.menuCode = menuCode;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getSysCode() {
        return sysCode;
    }

    public void setSysCode(String sysCode) {
        this.sysCode = sysCode;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
