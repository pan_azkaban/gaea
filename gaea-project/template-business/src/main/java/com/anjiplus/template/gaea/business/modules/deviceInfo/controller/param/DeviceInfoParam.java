/**/
package com.anjiplus.template.gaea.business.modules.deviceinfo.controller.param;

import lombok.Data;
import java.io.Serializable;
import com.anji.plus.gaea.annotation.Query;
import com.anji.plus.gaea.constant.QueryEnum;
import com.anji.plus.gaea.curd.params.PageParam;

import java.util.List;


/**
* @desc DeviceInfo 设备信息查询输入类
* @author WongBin
* @date 2021-04-07 17:44:28.598
**/
@Data
public class DeviceInfoParam extends PageParam implements Serializable{

    /** 精确查询 */
    @Query
    private Long deviceModelId;

    /** 模糊查询 */
    @Query(value = QueryEnum.LIKE)
    private String deviceName;

    /** 精确查询 */
    @Query
    private String orgCode;

    /** 精确查询 */
    @Query
    private String orgName;

    /** 精确查询 */
    @Query
    private String deviceIp;

    /** 精确查询 */
    @Query
    private String macAddress;

    /** 精确查询 */
    @Query
    private Integer enableFlag;

    /** 精确查询 */
    @Query
    private Integer deleteFlag;
}