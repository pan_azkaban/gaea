
package ${package}.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.anji.plus.gaea.annotation.AccessKey;
import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.curd.controller.GaeaBaseController;
import com.anji.plus.gaea.curd.service.GaeaBaseService;

import com.anji.plus.gaea.utils.GaeaBeanUtils;
import com.anji.plus.gaea.utils.GaeaUtils;

import ${package}.dao.entity.${className};
import ${package}.service.${className}Service;
import ${package}.controller.dto.${className}Dto;
import ${package}.controller.param.${className}Param;

import io.swagger.annotations.Api;

/**
* @desc ${apiAlias} controller
* @website https://gitee.com/anji-plus/gaea
* @author ${author}
* @date ${date}
**/
@RestController
@Api(tags = "${apiAlias}管理")
@RequestMapping("/${changeClassName}")
public class ${className}Controller extends GaeaBaseController<${className}Param, ${className}, ${className}Dto> {

    @Autowired
    private ${className}Service ${changeClassName}Service;

    @Override
    public GaeaBaseService<${className}Param, ${className}> getService() {
        return ${changeClassName}Service;
    }

    @Override
    public ${className} getEntity() {
        return new ${className}();
    }

    @Override
    public ${className}Dto getDTO() {
        return new ${className}Dto();
    }


    @GetMapping({"/{id}"})
    @AccessKey
    @Override
    public ResponseBean detail(@PathVariable("id") Long id) {
        this.logger.info("{}根据ID查询服务开始，id为：{}", this.getClass().getSimpleName(), id);
        ${className} result = ${changeClassName}Service.getDetail(id);
        ${className}Dto dto = this.getDTO();
        GaeaBeanUtils.copyAndFormatter(result, dto);
        ResponseBean responseBean = this.responseSuccessWithData(this.resultDtoHandle(dto));
        this.logger.info("{}根据ID查询结束，结果：{}", this.getClass().getSimpleName(), GaeaUtils.toJSONString(responseBean));
        return responseBean;
    }
}