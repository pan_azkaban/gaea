package com.anjiplus.template.gaea.auth.modules.role.controller;

import com.anjiplus.template.gaea.auth.modules.menu.controller.dto.TreeDTO;
import com.anjiplus.template.gaea.auth.modules.role.controller.dto.RoleMenuAuthorityDTO;
import com.anjiplus.template.gaea.auth.modules.role.controller.param.RoleOrgReqParam;
import com.anjiplus.template.gaea.auth.modules.role.dao.entity.GaeaRole;
import com.anjiplus.template.gaea.auth.modules.role.service.GaeaRoleService;
import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.curd.controller.GaeaBaseController;
import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anjiplus.template.gaea.auth.modules.role.controller.dto.GaeaRoleDTO;
import com.anjiplus.template.gaea.auth.modules.role.controller.param.GaeaRoleParam;
import com.anji.plus.gaea.annotation.log.GaeaAuditLog;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 角色(GaeaRole)实体类
 *
 * @author lr
 * @since 2021-02-02 13:37:54
 */
@RestController
@RequestMapping("/role")
@Api(value = "/role", tags = "角色")
public class GaeaRoleController extends GaeaBaseController<GaeaRoleParam, GaeaRole, GaeaRoleDTO> {
    @Autowired
    private GaeaRoleService gaeaRoleService;

    @Override
    public GaeaBaseService<GaeaRoleParam, GaeaRole> getService() {
        return gaeaRoleService;
    }

    @Override
    public GaeaRole getEntity() {
        return new GaeaRole();
    }

    @Override
    public GaeaRoleDTO getDTO() {
        return new GaeaRoleDTO();
    }

    /**
     * 查询角色跟组织
     * @param roleCode
     * @return
     */
    @GetMapping("/org/{code}")
    @GaeaAuditLog(pageTitle="查询角色组织")
    public ResponseBean queryOrgTreeForRole(@PathVariable("code") String roleCode) {
        TreeDTO data = gaeaRoleService.queryRoleOrgTree(roleCode);
        return responseSuccessWithData(data);
    }

    /**
     * 保存角色跟组织
     * @param reqParam
     * @return
     */
    @PostMapping("/mapping/org")
    @GaeaAuditLog(pageTitle="保存角色组织")
    public ResponseBean saveOrgTreeForRole(@RequestBody RoleOrgReqParam reqParam){
        Boolean data=gaeaRoleService.saveOrgTreeForRole(reqParam);
        return responseSuccessWithData(data);
    }


    /**
     * 保存菜单权限
     * @param dto
     * @return
     */
    @PostMapping("/roleMenuAuthorities")
    @GaeaAuditLog(pageTitle="保存角色菜单权限")
    public ResponseBean saveMenuActionTreeForRole(@RequestBody RoleMenuAuthorityDTO dto){
        //保存角色和权限
        Boolean data=gaeaRoleService.saveMenuAuthority(dto);

        return responseSuccessWithData(data);
    }


    /**
     * 获取当前角色拥有的权限按钮
     * @return
     */
    @GetMapping("/tree/authorities/{orgCode}/{roleCode}")
    public ResponseBean roleAuthorities(@PathVariable("orgCode")String orgCode, @PathVariable("roleCode")String roleCode) {
        return responseSuccessWithData(gaeaRoleService.getSelectAuthorities(orgCode,roleCode));
    }
}
