package com.anjiplus.template.gaea.business.modules.inf.app.controller;

import com.anji.plus.gaea.annotation.log.GaeaAuditLog;
import com.anji.plus.gaea.bean.ResponseBean;
import com.anji.plus.gaea.curd.controller.GaeaBaseController;
import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anjiplus.template.gaea.business.modules.inf.app.controller.dto.GaeaInfAppRelationDTO;
import com.anjiplus.template.gaea.business.modules.inf.app.controller.param.GaeaInfAppRelationParam;
import com.anjiplus.template.gaea.business.modules.inf.app.dao.entity.GaeaInfAppRelation;
import com.anjiplus.template.gaea.business.modules.inf.app.service.GaeaInfAppRelationService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 接口应用关系表(GaeaInfAppRelation)实体类
 *
 * @author ultrajiaming
 * @since 2021-04-08 18:10:13
 */
@RestController
@RequestMapping("/inf/app/relation")
@Api(value = "/inf/app/relation", tags = "接口应用关系表")
public class GaeaInfAppRelationController extends GaeaBaseController<GaeaInfAppRelationParam, GaeaInfAppRelation, GaeaInfAppRelationDTO> {
    @Autowired
    private GaeaInfAppRelationService gaeaInfAppRelationService;

    @Override
    public GaeaBaseService<GaeaInfAppRelationParam, GaeaInfAppRelation> getService() {
        return gaeaInfAppRelationService;
    }

    @Override
    public GaeaInfAppRelation getEntity() {
        return new GaeaInfAppRelation();
    }

    @Override
    public GaeaInfAppRelationDTO getDTO() {
        return new GaeaInfAppRelationDTO();
    }

    @PostMapping("listInfApp")
    public ResponseBean listInfApp(@RequestBody GaeaInfAppRelationDTO gaeaInfAppRelationDTO) {
        List<GaeaInfAppRelationDTO> results = gaeaInfAppRelationService.listInfApp(gaeaInfAppRelationDTO);
        return responseSuccessWithData(results);
    }

    @PutMapping("times")
    @GaeaAuditLog(pageTitle = "修改")
    public ResponseBean update(@Validated @RequestBody List<GaeaInfAppRelationDTO> dtoList) {
        gaeaInfAppRelationService.updateTimes(dtoList);
        return responseSuccess();
    }

}
