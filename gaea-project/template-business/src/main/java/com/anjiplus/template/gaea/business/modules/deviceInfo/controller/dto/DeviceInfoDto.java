
package com.anjiplus.template.gaea.business.modules.deviceinfo.controller.dto;

import com.anji.plus.gaea.annotation.Formatter;
import com.anji.plus.gaea.annotation.FormatterType;
import com.anji.plus.gaea.annotation.resolver.format.FormatterEnum;
import com.anji.plus.gaea.curd.dto.GaeaBaseDTO;
import com.anjiplus.template.gaea.business.modules.devicelogdetail.controller.dto.DeviceLogDetailDto;
import com.anjiplus.template.gaea.business.modules.devicemodel.controller.dto.DeviceModelDto;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
*
* @description 设备信息 dto
* @author WongBin
* @date 2021-04-07 17:44:28.598
**/
@Data
public class DeviceInfoDto extends GaeaBaseDTO implements Serializable {
    /** 设备型号ID */
    private Long deviceModelId;

    /** 设备名称 */
    private String deviceName;

    /** 机构代码 */
    private String orgCode;

    /** 所属机构 */
    private String orgName;

    /** 设备ip */
    private String deviceIp;

    /** 设备mac地址 */
    private String macAddress;

    /** 日志级别 */
    private String logLevel;

    /** 启用标志 */
    @Formatter(dictCode = "FILTER_FLAG",targetField = "enableFlagCn")
    private Integer enableFlag;
    private String enableFlagCn;

    /** 删除标志 */
    @Formatter(dictCode = "DELETE_FLAG",targetField = "deleteFlagCn")
    private Integer deleteFlag;
    private String deleteFlagCn;

    private List<DeviceLogDetailDto> deviceLogDetails;

    @FormatterType(type = FormatterEnum.LIST)
    private List<DeviceModelDto> deviceModels;

}
