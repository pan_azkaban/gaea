package com.anjiplus.template.gaea.gateway.filter;

import com.anji.plus.gaea.constant.Enabled;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.Ordered;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * 设置跳过校验标识
 * @author lr
 * @since 2021-03-18
 */
public class SkipAuthenticationGatewayFilterFactory extends
        AbstractGatewayFilterFactory<SkipAuthenticationGatewayFilterFactory.Config> {

    /**
     * 跳过过滤器
     */
    public final static String SKIP = "skipFilter";

    public SkipAuthenticationGatewayFilterFactory() {
        super(Config.class);
    }

    @Override
    public GatewayFilter apply(Config config) {
        return new SkipGatewayFilter();
    }

    /**
     * 跳过过滤器
     */
    public class SkipGatewayFilter implements GatewayFilter , Ordered {
        @Override
        public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
            //设置跳过标识
            exchange.getAttributes().put(SKIP, Enabled.YES);
            return chain.filter(exchange);
        }

        @Override
        public int getOrder() {
            return Ordered.LOWEST_PRECEDENCE -3;
        }
    }


    public static class Config {

    }
}
