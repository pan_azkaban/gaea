package com.anjiplus.template.gaea.auth.listener;

import com.anji.plus.gaea.constant.Enabled;
import com.anji.plus.gaea.security.event.EventEnum;
import com.anji.plus.gaea.security.event.UserApplicationEvent;
import com.anjiplus.template.gaea.auth.modules.user.dao.entity.GaeaUser;
import com.anjiplus.template.gaea.auth.modules.user.service.GaeaUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;

/**
 * 监听用户事件
 * @author lr
 * @since 2021-03-26
 */
public class UserEventListener implements ApplicationListener<UserApplicationEvent> {

    @Autowired
    private GaeaUserService gaeaUserService;

    @Override
    public void onApplicationEvent(UserApplicationEvent event) {

        GaeaUser gaeaUser = gaeaUserService.getUserByUsername(event.getUsername());
        EventEnum eventEnum = event.getEventEnum();
        switch (eventEnum) {
            //锁定
            case LOCKED:
                gaeaUser.setAccountLocked(Enabled.YES.getValue());
                break;
            //密码过期
            case CERDENTIALS_EXPIRE:
                gaeaUser.setCredentialsNonExpired(Enabled.NO.getValue());
                break;
            default:
        }

        //更新用户状态
        gaeaUserService.update(gaeaUser);
    }
}
