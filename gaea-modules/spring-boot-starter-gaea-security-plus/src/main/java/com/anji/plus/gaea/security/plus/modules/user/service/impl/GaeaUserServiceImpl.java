package com.anji.plus.gaea.security.plus.modules.user.service.impl;

import com.anji.plus.gaea.bean.KeyValue;
import com.anji.plus.gaea.cache.CacheHelper;
import com.anji.plus.gaea.constant.BaseOperationEnum;
import com.anji.plus.gaea.constant.Enabled;
import com.anji.plus.gaea.constant.GaeaConstant;
import com.anji.plus.gaea.constant.GaeaKeyConstant;
import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import com.anji.plus.gaea.exception.BusinessException;
import com.anji.plus.gaea.exception.BusinessExceptionBuilder;
import com.anji.plus.gaea.holder.UserContentHolder;
import com.anji.plus.gaea.security.GaeaSecurityProperties;
import com.anji.plus.gaea.security.plus.code.RespCommonCode;
import com.anji.plus.gaea.security.plus.modules.org.dao.GaeaOrgMapper;
import com.anji.plus.gaea.security.plus.modules.org.dao.entity.GaeaOrg;
import com.anji.plus.gaea.security.plus.modules.org.service.GaeaOrgService;
import com.anji.plus.gaea.security.plus.modules.user.controller.param.GaeaUserParam;
import com.anji.plus.gaea.security.plus.modules.user.controller.param.GaeaUserPasswordParam;
import com.anji.plus.gaea.security.plus.modules.user.controller.param.UserRoleOrgReqParam;
import com.anji.plus.gaea.security.plus.modules.user.dao.GaeaUserMapper;
import com.anji.plus.gaea.security.plus.modules.user.dao.GaeaUserRoleOrgMapper;
import com.anji.plus.gaea.security.plus.modules.user.dao.entity.GaeaUser;
import com.anji.plus.gaea.security.plus.modules.user.dao.entity.GaeaUserRoleOrg;
import com.anji.plus.gaea.security.plus.modules.user.service.GaeaUserService;
import com.anji.plus.gaea.security.utils.MD5Util;
import com.anji.plus.gaea.security.utils.SecurityUtils;
import com.anji.plus.gaea.utils.GaeaUtils;
import com.anji.plus.gaea.utils.JwtBean;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

import static com.anji.plus.gaea.security.plus.code.RespCommonCode.ORG_ROLE_INVALID;
import static com.anji.plus.gaea.security.plus.code.RespCommonCode.ROLE_NOT_BIND_ANY_ROLE;

/**
 * 用户表(GaeaUser)ServiceImpl
 *
 * @author lr
 * @since 2021-02-02 13:38:12
 */
@Service
public class GaeaUserServiceImpl implements GaeaUserService {

    @Autowired
    private GaeaUserMapper gaeaUserMapper;

    @Autowired
    private GaeaUserRoleOrgMapper gaeaUserRoleOrgMapper;

    @Autowired
    private GaeaOrgMapper gaeaOrgMapper;

    @Autowired
    private GaeaOrgService gaeaOrgService;

    @Autowired
    private CacheHelper cacheHelper;

    @Autowired
    private JwtBean jwtBean;

    @Autowired
    private GaeaSecurityProperties gaeaSecurityProperties;


    @Override
    public GaeaBaseMapper<GaeaUser> getMapper() {
        return gaeaUserMapper;
    }


    @Override
    public Wrapper<GaeaUser> extensionWrapper(GaeaUserParam param, QueryWrapper<GaeaUser> wrapper) {

        Set<String> usernameSet = new HashSet<>();
        String roleCode = param.getOrgRoleCode();
        if (StringUtils.isNotBlank(roleCode)) {
            if (!roleCode.contains(GaeaConstant.REDIS_SPLIT)) {
                return wrapper;
            }
            String[] split = roleCode.split(GaeaConstant.REDIS_SPLIT);

            LambdaQueryWrapper<GaeaUserRoleOrg> lambdaQueryWrapper = Wrappers.lambdaQuery();
            lambdaQueryWrapper.eq(GaeaUserRoleOrg::getOrgCode, split[0]);
            lambdaQueryWrapper.eq(GaeaUserRoleOrg::getRoleCode, split[1]);
            usernameSet = gaeaUserRoleOrgMapper.selectList(lambdaQueryWrapper).stream()
                    .map(GaeaUserRoleOrg::getUsername)
                    .collect(Collectors.toSet());
            if (!CollectionUtils.isEmpty(usernameSet)) {
                wrapper.lambda().in(GaeaUser::getUsername, usernameSet);
            } else {
                wrapper.lambda().isNull(GaeaUser::getUsername);
            }
        }

        if (CollectionUtils.isEmpty(usernameSet)) {
            List<String> orgCodes = gaeaOrgService.getCurrentUserOrgCodes();
            if (!CollectionUtils.isEmpty(orgCodes)) {
                LambdaQueryWrapper<GaeaUserRoleOrg> lambdaQueryWrapper = Wrappers.lambdaQuery();
                lambdaQueryWrapper.in(GaeaUserRoleOrg::getOrgCode, orgCodes);
                List<GaeaUserRoleOrg> gaeaUserRoleOrgs = gaeaUserRoleOrgMapper.selectList(lambdaQueryWrapper);
                List<String> userList = gaeaUserRoleOrgs.stream().map(GaeaUserRoleOrg::getUsername).collect(Collectors.toList());
                if (!CollectionUtils.isEmpty(gaeaUserRoleOrgs)) {
                    wrapper.lambda().in(GaeaUser::getUsername, userList);
                }
            } else {
                wrapper.lambda().isNull(GaeaUser::getUsername);
            }
        }

        return wrapper;
    }

    @Override
    public GaeaUser getUserByUsername(String username) {
        LambdaQueryWrapper<GaeaUser> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(GaeaUser::getUsername, username);
        return gaeaUserMapper.selectOne(queryWrapper);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveRoleTree(UserRoleOrgReqParam reqParam) {
        //获取当前用户所拥有的角色，他只能管理这些角色，也就只能删除这些角色
        String roleOperationUser = UserContentHolder.getUsername();
        LambdaQueryWrapper<GaeaUserRoleOrg> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(GaeaUserRoleOrg::getUsername, roleOperationUser);
        List<GaeaUserRoleOrg> gaeaUserRoleOrgs = gaeaUserRoleOrgMapper.selectList(wrapper);
        //判断是否有超级管理员角色
        Optional<GaeaUserRoleOrg> exist = gaeaUserRoleOrgs.stream().filter(gaeaUserRoleOrg -> GaeaConstant.SUPER_ADMIN_ROLE.equals(gaeaUserRoleOrg.getRoleCode()))
                .findFirst();

        String username = reqParam.getUsername().toLowerCase();
        List<String> orgRoleCodes = reqParam.getRoleOrgCodes();

        List<String> paramOrgs = orgRoleCodes.stream().map(item -> item.split(GaeaConstant.REDIS_SPLIT)[0])
                .collect(Collectors.toList());

        //该用户已经关联的组织角色
        LambdaQueryWrapper<GaeaUserRoleOrg> userRoleOrgWrapper = Wrappers.lambdaQuery();
        userRoleOrgWrapper.eq(GaeaUserRoleOrg::getUsername, username);

        //没有超级管理员
        if (!exist.isPresent()) {
            Map<String, List<String>> hasOrgRole = gaeaUserRoleOrgs.stream()
                    .collect(Collectors.groupingBy(GaeaUserRoleOrg::getOrgCode, Collectors.mapping(GaeaUserRoleOrg::getRoleCode, Collectors.toList())));
            paramOrgs.stream().forEach(orgCode -> {
                List<String> orgHashRoles = hasOrgRole.get(orgCode);
                userRoleOrgWrapper.eq(GaeaUserRoleOrg::getOrgCode, orgCode);
                if (!orgHashRoles.contains(GaeaConstant.SUPER_USER_NAME)) {
                    userRoleOrgWrapper.in(GaeaUserRoleOrg::getRoleCode, orgHashRoles);
                }
                gaeaUserRoleOrgMapper.delete(userRoleOrgWrapper);
            });

        } else {
            userRoleOrgWrapper.in(GaeaUserRoleOrg::getOrgCode, paramOrgs);
            gaeaUserRoleOrgMapper.delete(userRoleOrgWrapper);
        }

        //删除缓存
        String key = GaeaKeyConstant.USER_ROLE_SET_PREFIX + username;
        cacheHelper.delete(key);

        if (CollectionUtils.isEmpty(orgRoleCodes)) {
            return true;
        }
        List<GaeaUserRoleOrg> list = new ArrayList<>();


        orgRoleCodes.stream()
                .filter(codeStr -> StringUtils.isNotBlank(codeStr) && codeStr.contains(GaeaConstant.REDIS_SPLIT))
                .forEach(codesStr -> {
                    GaeaUserRoleOrg userRoleOrg = new GaeaUserRoleOrg();
                    String[] info = codesStr.split(GaeaConstant.REDIS_SPLIT);
                    userRoleOrg.setOrgCode(info[0]);
                    userRoleOrg.setRoleCode(info[1]);
                    userRoleOrg.setUsername(username);
                    list.add(userRoleOrg);
                });
        gaeaUserRoleOrgMapper.insertBatch(list);

        //刷新用户、机构、角色GaeaKeyConstant.USER_ROLE_SET_PREFIX + : + orgCode => map
        Map<String, String> userOrgRoleMap = list.stream()
                .collect(Collectors.groupingBy(GaeaUserRoleOrg::getOrgCode,
                        Collectors.mapping(GaeaUserRoleOrg::getRoleCode, Collectors.joining(GaeaConstant.SPLIT))));


        cacheHelper.hashSet(key, userOrgRoleMap);
        return true;
    }

    @Override
    public Boolean updatePassword(GaeaUserPasswordParam requestParam) {
        //参数校验
        if (!requestParam.getConfirmPassword().equals(requestParam.getPassword())) {
            //密码和确认密码不一致
            throw BusinessExceptionBuilder.build(RespCommonCode.AUTH_PASSWORD_NOTSAME);
        }
        //新密码不能与老密码一样
        if (StringUtils.equals(requestParam.getOldPassword(), requestParam.getPassword())) {
            throw BusinessExceptionBuilder.build(RespCommonCode.USER_PASSWORD_CONFIG_PASSWORD_CANOT_EQUAL);
        }
        String password = SecurityUtils.getPassword(requestParam.getPassword());
        String oldPassword = SecurityUtils.getPassword(requestParam.getOldPassword());
        String username = UserContentHolder.getContext().getUsername();

        LambdaQueryWrapper<GaeaUser> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.select(GaeaUser::getId, GaeaUser::getPassword)
                .eq(GaeaUser::getUsername, username);
        GaeaUser gaeaUser = gaeaUserMapper.selectOne(queryWrapper);

        if (SecurityUtils.matches(gaeaUser.getPassword(), oldPassword)) {
            throw BusinessExceptionBuilder.build(RespCommonCode.OLD_PASSWORD_ERROR);
        }
        gaeaUser.setPassword(password);
        int flag = gaeaUserMapper.updateById(gaeaUser);

        if (flag > 0) {
            //token
            String tokenKey = GaeaKeyConstant.USER_LOGIN_TOKEN + username + GaeaConstant.REDIS_SPLIT + GaeaConstant.PATTERN_SIGN;
            Set<String> keys = cacheHelper.keys(tokenKey);
            cacheHelper.delete(new ArrayList<>(keys));
        }
        return flag > 0;
    }


    @Override
    public List<KeyValue> getOrgByUsername(String username) {
        LambdaQueryWrapper<GaeaUserRoleOrg> wrapper = Wrappers.lambdaQuery();

        wrapper.eq(GaeaUserRoleOrg::getUsername,username);
        List<GaeaUserRoleOrg> gaeaUserRoleOrgs = gaeaUserRoleOrgMapper.selectList(wrapper);

        //角色
        List<String> roles = gaeaUserRoleOrgs.stream().map(GaeaUserRoleOrg::getRoleCode)
                .collect(Collectors.toList());

        LambdaQueryWrapper<GaeaOrg> orgWrapper = Wrappers.lambdaQuery();
        if (!roles.contains(GaeaConstant.SUPER_ADMIN_ROLE)) {
            //组织
            Set<String> orgCodes = gaeaUserRoleOrgs.stream()
                    .map(GaeaUserRoleOrg::getOrgCode)
                    .collect(Collectors.toSet());
            orgWrapper.in(GaeaOrg::getOrgCode, orgCodes);
        }

        List<GaeaOrg> gaeaOrgs = gaeaOrgMapper.selectList(orgWrapper);
        return gaeaOrgs.stream().map(gaeaOrg -> {
            KeyValue keyValue = new KeyValue();
            keyValue.setText(gaeaOrg.getOrgName());
            keyValue.setId(gaeaOrg.getOrgCode());
            keyValue.setExtend(gaeaOrg);
            return keyValue;
        }).collect(Collectors.toList());
    }

    @Override
    public List<String> getRoleByUserOrg(String username, String orgCode) {
        LambdaQueryWrapper<GaeaUserRoleOrg> wrapper = Wrappers.lambdaQuery();
        wrapper.select(GaeaUserRoleOrg::getRoleCode, GaeaUserRoleOrg::getId)
                .eq(GaeaUserRoleOrg::getUsername, username)
                .and(StringUtils.isNotBlank(orgCode), e -> e.eq(GaeaUserRoleOrg::getOrgCode, orgCode));
        List<GaeaUserRoleOrg> gaeaUserRoleOrgList = gaeaUserRoleOrgMapper.selectList(wrapper);
        return gaeaUserRoleOrgList.stream().map(GaeaUserRoleOrg::getRoleCode).collect(Collectors.toList());
    }

    /**
     * 新增用户，需要设置默认密码
     *
     * @param entity        前端传递的对象
     * @param operationEnum 操作类型
     * @throws BusinessException
     */
    @Override
    public void processBeforeOperation(GaeaUser entity, BaseOperationEnum operationEnum) throws BusinessException {
        switch (operationEnum) {
            case INSERT:
                setDefault(entity);
                break;
            default:
        }
    }

    @Override
    public void processAfterOperation(GaeaUser entity, BaseOperationEnum operationEnum) throws BusinessException {
        String key = GaeaUtils.replaceFormatString(GaeaKeyConstant.USER_NICKNAME_KEY, UserContentHolder.getContext().getParams());
        String uuid = UserContentHolder.getUuid();
        switch (operationEnum) {
            case INSERT:
                //用户创建未绑定角色时异常
                if (StringUtils.isBlank(entity.getRoleOrgCode())) {
                    throw BusinessExceptionBuilder.build(ROLE_NOT_BIND_ANY_ROLE, entity.getUsername());
                }

                String roleOrgCode = entity.getRoleOrgCode();
                if (!roleOrgCode.contains(GaeaConstant.REDIS_SPLIT)) {
                    throw BusinessExceptionBuilder.build(ORG_ROLE_INVALID, roleOrgCode);
                }
                String[] split = roleOrgCode.split(GaeaConstant.REDIS_SPLIT);

                //保存角色与用户的对应关系
                GaeaUserRoleOrg gaeaUserRoleOrg = new GaeaUserRoleOrg();
                gaeaUserRoleOrg.setOrgCode(split[0]);
                gaeaUserRoleOrg.setRoleCode(split[1]);
                gaeaUserRoleOrg.setUsername(entity.getUsername());
                gaeaUserRoleOrgMapper.insert(gaeaUserRoleOrg);
            case UPDATE:
                cacheHelper.hashSet(key, entity.getUsername().toLowerCase(), entity.getNickname());
                if (Enabled.NO.getValue().equals(entity.getEnabled())) {
                    //清空登录状态
                    cacheHelper.delete(GaeaKeyConstant.USER_LOGIN_TOKEN + entity.getUsername() + GaeaConstant.REDIS_SPLIT + uuid);
                }
                break;

            case DELETE:
                cacheHelper.hashDel(key, entity.getUsername().toLowerCase());
                cacheHelper.delete(GaeaKeyConstant.USER_LOGIN_TOKEN + entity.getUsername() + GaeaConstant.REDIS_SPLIT + uuid);
                break;
            default:
        }
    }


    @Override
    public void processBatchAfterOperation(List<GaeaUser> entities, BaseOperationEnum operationEnum) throws BusinessException {
        String uuid = UserContentHolder.getUuid();
        List<String> loginTokenKeys = entities.stream()
                .map(gaeaUser -> GaeaKeyConstant.USER_LOGIN_TOKEN + gaeaUser.getUsername() + GaeaConstant.REDIS_SPLIT + uuid)
                .collect(Collectors.toList());
        switch (operationEnum) {
            case DELETE_BATCH:
                //清空登录状态
                cacheHelper.delete(loginTokenKeys);

                Set<String> usernameSet = entities.stream().map(GaeaUser::getUsername).map(String::toLowerCase).collect(Collectors.toSet());
                String key = GaeaUtils.replaceFormatString(GaeaKeyConstant.USER_NICKNAME_KEY, UserContentHolder.getContext().getParams());
                cacheHelper.hashBatchDel(key, usernameSet);
                break;
            default:
        }

    }

    /**
     * 设置默认值
     *
     * @param entity
     */
    private void setDefault(GaeaUser entity) {
        String md5Pwd = MD5Util.encryptBySalt(gaeaSecurityProperties.getDefaultPassword());
        String defaultPwd = SecurityUtils.getPassword(md5Pwd);
        entity.setPassword(defaultPwd);
        if (entity.getMultiLogin() == null) {
            entity.setMultiLogin(Enabled.NO.getValue());
        }
        entity.setAccountNonExpired(Enabled.YES.getValue());
        entity.setCredentialsNonExpired(Enabled.YES.getValue());
        entity.setAccountLocked(Enabled.NO.getValue());
    }

    @Override
    public void refreshCache(List<String> usernameList) {

        LambdaQueryWrapper<GaeaUser> wrapper = Wrappers.lambdaQuery();

        if (CollectionUtils.isEmpty(usernameList)) {
            String orgCode = UserContentHolder.getOrgCode();
            LambdaQueryWrapper<GaeaUserRoleOrg> lambdaQueryWrapper = Wrappers.lambdaQuery();
            lambdaQueryWrapper.eq(GaeaUserRoleOrg::getOrgCode, orgCode);
            List<GaeaUserRoleOrg> gaeaUserRoleOrgs = gaeaUserRoleOrgMapper.selectList(lambdaQueryWrapper);
            usernameList = gaeaUserRoleOrgs.stream().map(GaeaUserRoleOrg::getUsername).distinct().collect(Collectors.toList());
        }

        wrapper.in(GaeaUser::getUsername, usernameList);

        if (usernameList.isEmpty()) {
            return;
        }
        List<GaeaUser> gaeaUsers = gaeaUserMapper.selectList(wrapper);

        Map<String, String> map = gaeaUsers.stream().collect(Collectors.toMap(gaeaUser -> gaeaUser.getUsername().toLowerCase(), GaeaUser::getNickname, (v1, v2) -> v2));

        String key = GaeaUtils.replaceFormatString(GaeaKeyConstant.USER_NICKNAME_KEY, UserContentHolder.getContext().getParams());
        cacheHelper.delete(key);
        cacheHelper.hashSet(key, map);
    }

    @Override
    public List<String> getOrgCodes(String username) {
        LambdaQueryWrapper<GaeaUserRoleOrg> wrapper = Wrappers.lambdaQuery();
        wrapper.select(GaeaUserRoleOrg::getOrgCode);
        wrapper.eq(GaeaUserRoleOrg::getUsername, username);
        return gaeaUserRoleOrgMapper.selectList(wrapper).stream()
                .map(GaeaUserRoleOrg::getOrgCode).distinct()
                .collect(Collectors.toList());
    }

    @Override
    public List<String> getOrgRoleMappings(String username) {
        LambdaQueryWrapper<GaeaUserRoleOrg> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(GaeaUserRoleOrg::getUsername, username);
        return gaeaUserRoleOrgMapper.selectList(wrapper)
                .stream()
                .map(gaeaUserRoleOrg -> gaeaUserRoleOrg.getOrgCode() + GaeaConstant.REDIS_SPLIT + gaeaUserRoleOrg.getRoleCode())
                .collect(Collectors.toList());
    }

    @Override
    public List<KeyValue> userSelect() {

        String key = GaeaUtils.replaceFormatString(GaeaKeyConstant.USER_NICKNAME_KEY, UserContentHolder.getContext().getParams());
        Map<String, String> userMap = cacheHelper.hashGet(key);

        List<KeyValue> userSelect = userMap.entrySet().stream().map(entry -> {
            KeyValue keyValue = new KeyValue();
            keyValue.setId(entry.getKey());
            keyValue.setText(entry.getValue());
            return keyValue;
        }).collect(Collectors.toList());

        return userSelect;
    }


    @Override
    public void setDefaultPwd(List<String> usernameList) {
        LambdaQueryWrapper<GaeaUser> wrapper = Wrappers.lambdaQuery();
        wrapper.in(GaeaUser::getUsername, usernameList);
        List<GaeaUser> gaeaUsers = list(wrapper);

        String md5Pwd = MD5Util.encryptBySalt(gaeaSecurityProperties.getDefaultPassword());
        String defaultPwd = SecurityUtils.getPassword(md5Pwd);
        Map<String, Object> map = new HashMap<>();
        map.put("password", defaultPwd);
        map.put("account_locked", Enabled.NO.getValue());
        map.put("account_non_expired", Enabled.YES.getValue());
        map.put("credentials_non_expired", Enabled.YES.getValue());
        map.put("password_update_time", new Date());
        int i = gaeaUserMapper.updateFieldsBatch(map, gaeaUsers);

        if (i > 0) {
            List<String> deleteKeys = new ArrayList<>();
            usernameList.stream().forEach(username -> {
                String tokenKey = GaeaKeyConstant.USER_LOGIN_TOKEN + username + GaeaConstant.REDIS_SPLIT + GaeaConstant.PATTERN_SIGN;
                Set<String> keys = cacheHelper.keys(tokenKey);
                deleteKeys.addAll(keys);
            });
            cacheHelper.delete(deleteKeys);

        }

    }

    @Override
    public void unLock(List<String> usernameList) {

        LambdaQueryWrapper<GaeaUser> wrapper = Wrappers.lambdaQuery();
        wrapper.in(GaeaUser::getUsername, usernameList);
        List<GaeaUser> gaeaUsers = list(wrapper);

        Map<String, Object> map = new HashMap<>();
        map.put("account_locked", Enabled.NO.getValue());
        gaeaUserMapper.updateFieldsBatch(map, gaeaUsers);
    }

    @Override
    public Map<String, String> getUserOrgRoleMap(String username) {

        LambdaQueryWrapper<GaeaUserRoleOrg> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(GaeaUserRoleOrg::getUsername, username);

        List<GaeaUserRoleOrg> gaeaUserRoleOrgList = gaeaUserRoleOrgMapper.selectList(wrapper);

        return gaeaUserRoleOrgList.stream().collect(Collectors
                .groupingBy(GaeaUserRoleOrg::getOrgCode,
                        Collectors.mapping(GaeaUserRoleOrg::getRoleCode, Collectors.joining(GaeaConstant.SPLIT))));
    }

    @Override
    public List<GaeaUser> getUserByNames(List<String> usernames) {
        LambdaQueryWrapper<GaeaUser> wrapper = Wrappers.lambdaQuery();
        wrapper.in(GaeaUser::getUsername, usernames)
                .and(e -> e.eq(GaeaUser::getEnabled, Enabled.YES.getValue()));
        return gaeaUserMapper.selectList(wrapper);
    }


    @Override
    public List<GaeaUser> getUsersByOrgCode(String orgCode) {
        LambdaQueryWrapper<GaeaUserRoleOrg> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(GaeaUserRoleOrg::getOrgCode, orgCode);

        List<GaeaUserRoleOrg> gaeaUserRoleOrgs = gaeaUserRoleOrgMapper.selectList(wrapper);

        List<String> usernames = gaeaUserRoleOrgs.stream().map(GaeaUserRoleOrg::getUsername).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(usernames)) {
            return Collections.emptyList();
        }
        return getUserByNames(usernames);
    }
}
