import com.anji.plus.gaea.security.utils.SecurityUtils;
import com.anjiplus.template.gaea.common.util.Md5Util;
import org.junit.jupiter.api.Test;

/**
 * 说明
 *
 * @author 木子李*muzili
 * @since 2022/1/13 18:22
 */
public class UserTest {

    @Test
    public void generatePassword(){
        String md5Pwd = Md5Util.encryptBySalt("123456");
        String password = SecurityUtils.getPassword(md5Pwd);
        System.out.println(password);
    }
}
