/*
Navicat MySQL Data Transfer

Source Server Version : 50728
Source Database       : gaea_business

Target Server Type    : MYSQL
Target Server Version : 50728
File Encoding         : 65001

Date: 2021-04-13 12:58:14
*/
-- ----------------------------
-- Table structure for gaea_application
-- ----------------------------
DROP TABLE IF EXISTS `gaea_application`;
CREATE TABLE `gaea_application` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `app_name` varchar(255) DEFAULT NULL COMMENT '应用名称',
  `app_code` varchar(255) DEFAULT NULL COMMENT '应用ID',
  `app_secret` varchar(255) DEFAULT NULL COMMENT '应用秘钥',
  `linkman_email` varchar(64) DEFAULT NULL COMMENT '应用通知邮箱',
  `status` tinyint(1) DEFAULT NULL COMMENT '1：有效 2：无效',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述',
  `create_by` varchar(64) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(64) DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for gaea_dict
-- ----------------------------
DROP TABLE IF EXISTS `gaea_dict`;
CREATE TABLE `gaea_dict` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dict_name` varchar(64) NOT NULL COMMENT '字典名称',
  `dict_code` varchar(64) NOT NULL COMMENT '字典编码',
  `remark` varchar(64) DEFAULT NULL COMMENT '描述',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新用户',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `version` int(11) DEFAULT NULL COMMENT '版本',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='数组字典';

-- ----------------------------
-- Table structure for gaea_dict_item
-- ----------------------------
DROP TABLE IF EXISTS `gaea_dict_item`;
CREATE TABLE `gaea_dict_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dict_code` varchar(64) DEFAULT NULL COMMENT '数据字典编码',
  `item_name` varchar(64) NOT NULL COMMENT '字典项名称',
  `item_value` varchar(64) NOT NULL COMMENT '字典项值',
  `item_extend` varchar(2048) DEFAULT NULL COMMENT '字典扩展项',
  `enabled` int(1) DEFAULT '1' COMMENT '1:启用 0:禁用',
  `locale` varchar(16) DEFAULT NULL COMMENT '语言标识',
  `remark` varchar(64) DEFAULT NULL COMMENT '描述',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新用户',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `version` int(11) DEFAULT NULL COMMENT '版本',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='数据字典项';

-- ----------------------------
-- Table structure for gaea_export
-- ----------------------------
DROP TABLE IF EXISTS `gaea_export`;
CREATE TABLE `gaea_export` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_id` varchar(128) NOT NULL COMMENT '文件在t_file中的id，前端传它来读流接口显示，http://auth/file/download/fileId',
  `file_title` varchar(128) NOT NULL COMMENT '文件标题，比如:对账单报表6月份报表',
  `result_start_time` datetime DEFAULT NULL COMMENT '导出前，查询的数据开始时间',
  `result_end_time` datetime DEFAULT NULL COMMENT '导出前，查询的数据结束时间',
  `result_size` bigint(20) NOT NULL DEFAULT '0' COMMENT '导出查询结果，数据总条数',
  `file_create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '文件导出触发时间',
  `file_finish_time` datetime DEFAULT NULL COMMENT '文件生成完成时间',
  `file_status` varchar(16) NOT NULL DEFAULT '0' COMMENT '文件状态，creating生成中，success生成成功,failed生成失败',
  `create_by` varchar(64) NOT NULL COMMENT '创建人',
  `update_by` varchar(64) DEFAULT NULL COMMENT '修改人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `version` int(8) DEFAULT NULL COMMENT '版本号',
  `remark` varchar(1024) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='导出中心';

-- ----------------------------
-- Table structure for gaea_file
-- ----------------------------
DROP TABLE IF EXISTS `gaea_file`;
CREATE TABLE `gaea_file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_id` varchar(64) DEFAULT NULL COMMENT '生成的唯一uuid',
  `file_path` varchar(1024) DEFAULT NULL COMMENT '文件在linux中的完整目录，比如/app/dist/export/excel/${fileid}.xlsx',
  `url_path` varchar(1024) DEFAULT NULL COMMENT '通过接口的下载完整http路径',
  `file_instruction` varchar(1024) DEFAULT NULL COMMENT '文件内容说明，比如 对账单(202001~202012)',
  `create_by` varchar(64) CHARACTER SET utf8mb4 DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_by` varchar(64) CHARACTER SET utf8mb4 DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gaea_help
-- ----------------------------
DROP TABLE IF EXISTS `gaea_help`;
CREATE TABLE `gaea_help` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `help_category` varchar(32) NOT NULL COMMENT '帮助分类，字典=HELP_CATEGORY',
  `help_title` varchar(64) NOT NULL COMMENT '标题',
  `help_content` longtext COMMENT '文本内容',
  `enabled` int(1) NOT NULL DEFAULT '1' COMMENT '0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序号',
  `remark` varchar(512) DEFAULT NULL COMMENT '备注',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `version` int(8) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='帮助中心表';

-- ----------------------------
-- Table structure for gaea_inf
-- ----------------------------
DROP TABLE IF EXISTS `gaea_inf`;
CREATE TABLE `gaea_inf` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `inf_name` varchar(64) NOT NULL COMMENT '接口标识',
  `inf_name_name` varchar(255) NOT NULL COMMENT '接口名称',
  `inf_description` varchar(1000) DEFAULT NULL COMMENT '接口描述',
  `sql_sentence` text NOT NULL COMMENT 'sql语句',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态 (0，编辑中；1，测试通过；2，已上线；3，已下线)',
  `with_group` varchar(32) DEFAULT NULL COMMENT '所属分组',
  `interaction_type` varchar(32) DEFAULT NULL COMMENT '交互类型',
  `dynamic_condition` text COMMENT '动态参数，逗号隔开',
  `request_params` text COMMENT '请求参数',
  `response_demo` text COMMENT '返回结果示例',
  `response_params` text COMMENT '响应参数',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  `version` int(8) DEFAULT NULL COMMENT '版本号',
  `authorized_apps` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_infName` (`inf_name`) USING BTREE COMMENT '接口唯一'
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='接口维护表';

-- ----------------------------
-- Table structure for gaea_inf_app
-- ----------------------------
DROP TABLE IF EXISTS `gaea_inf_app`;
CREATE TABLE `gaea_inf_app` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) DEFAULT NULL COMMENT '应用名称',
  `app_id` varchar(255) DEFAULT NULL COMMENT '应用ID',
  `app_secret` varchar(255) DEFAULT NULL COMMENT '应用秘钥',
  `status` char(1) DEFAULT '1' COMMENT '1：有效 0：无效',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  `version` int(8) DEFAULT NULL COMMENT '版本号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_app_id` (`app_id`) USING BTREE COMMENT '应用唯一'
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='应用维护表';

-- ----------------------------
-- Table structure for gaea_inf_app_relation
-- ----------------------------
DROP TABLE IF EXISTS `gaea_inf_app_relation`;
CREATE TABLE `gaea_inf_app_relation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `app_id` varchar(255) NOT NULL COMMENT '应用ID',
  `app_name` varchar(255) DEFAULT NULL COMMENT '应用名称',
  `inf_name` varchar(64) NOT NULL COMMENT '接口标识',
  `inf_name_name` varchar(255) DEFAULT NULL COMMENT '接口名称',
  `times` int(8) DEFAULT NULL COMMENT '调用次数',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  `version` int(8) DEFAULT NULL COMMENT '版本号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_uni_app_inf` (`app_id`,`inf_name`) USING BTREE,
  KEY `idx_app` (`app_id`) USING BTREE,
  KEY `idx_inf` (`inf_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='接口应用关系表';

-- ----------------------------
-- Table structure for gaea_inf_history
-- ----------------------------
DROP TABLE IF EXISTS `gaea_inf_history`;
CREATE TABLE `gaea_inf_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `inf_name` varchar(64) NOT NULL COMMENT '接口名称',
  `inf_description` varchar(128) NOT NULL COMMENT '接口描述',
  `sql_sentence` text NOT NULL COMMENT 'sql语句',
  `params` text COMMENT '参数',
  `execute_result` char(1) DEFAULT NULL COMMENT '执行结果(0，成功；1，失败)',
  `failure_reason` text COMMENT '失败原因',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  `version` int(8) DEFAULT NULL COMMENT '版本号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='接口调用历史记录表';

-- ----------------------------
-- Table structure for gaea_inf_param
-- ----------------------------
DROP TABLE IF EXISTS `gaea_inf_param`;
CREATE TABLE `gaea_inf_param` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `inf_id` bigint(20) DEFAULT NULL COMMENT '接口维护表Id',
  `param_name` varchar(32) DEFAULT NULL COMMENT '参数名',
  `description` varchar(64) DEFAULT NULL COMMENT '描述',
  `data_type` varchar(64) DEFAULT NULL COMMENT '数据类型',
  `param_demo` varchar(255) DEFAULT NULL COMMENT '示例值',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `check_rule` varchar(255) DEFAULT NULL COMMENT '校验',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  `version` int(8) DEFAULT NULL COMMENT '版本号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='接口参数明细维护表';

-- ----------------------------
-- Table structure for gaea_push_history
-- ----------------------------
DROP TABLE IF EXISTS `gaea_push_history`;
CREATE TABLE `gaea_push_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `template_type` varchar(20) NOT NULL COMMENT '模板类型，sms：短信；mail：邮件; dingtalk:',
  `template_code` varchar(128) DEFAULT NULL COMMENT '模板code',
  `content` longtext COMMENT '内容',
  `push_title` varchar(512) DEFAULT NULL COMMENT 'sms:短信消息, mail:邮件标题, dingtalk: 钉钉消息',
  `push_from` varchar(512) DEFAULT NULL COMMENT '发送者',
  `push_to` varchar(512) DEFAULT NULL COMMENT '接受者,以,或;隔开',
  `mobiles` varchar(255) DEFAULT NULL COMMENT '发送的手机号（钉钉，短信）,以,隔开',
  `mail_copy` varchar(255) DEFAULT NULL COMMENT '邮件抄送人,以,或;隔开',
  `mail_bcc` varchar(255) DEFAULT NULL COMMENT '邮件密送人,以,或;隔开',
  `operator` varchar(50) DEFAULT NULL COMMENT '操作者(短信-运营商、钉钉、邮箱)',
  `send_time` datetime DEFAULT NULL COMMENT '发送时间',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态,1,成功;0,失败',
  `send_result` longtext COMMENT '发送结果',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int(8) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for gaea_push_template
-- ----------------------------
DROP TABLE IF EXISTS `gaea_push_template`;
CREATE TABLE `gaea_push_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(255) DEFAULT NULL COMMENT '模板名称',
  `template_code` varchar(255) DEFAULT NULL COMMENT '模板代码',
  `template_type` varchar(20) NOT NULL COMMENT '模板类型，sms：短信；mail：邮件',
  `template` varchar(2048) DEFAULT NULL COMMENT '模板',
  `template_show` varchar(2048) DEFAULT NULL COMMENT '模板预览',
  `template_param` varchar(2048) DEFAULT NULL COMMENT '模板参数',
  `template_info` varchar(500) DEFAULT NULL COMMENT '三方应用模板相关信息，极光，阿里',
  `enable_flag` int(1) NOT NULL DEFAULT '1' COMMENT '0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG',
  `delete_flag` int(1) DEFAULT '0' COMMENT '0--未删除 1--已删除 DIC_NAME=DELETE_FLAG',
  `create_by` varchar(255) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(255) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int(8) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `unique_template_code` (`template_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for gaea_report
-- ----------------------------
DROP TABLE IF EXISTS `gaea_report`;
CREATE TABLE `gaea_report` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `report_name` varchar(100) DEFAULT NULL COMMENT '名称',
  `report_code` varchar(100) DEFAULT NULL COMMENT '报表编码',
  `set_codes` varchar(255) DEFAULT NULL COMMENT '数据集编码，以|分割',
  `report_group` varchar(100) DEFAULT NULL COMMENT '分组',
  `set_param` varchar(1024) DEFAULT NULL COMMENT '数据集查询参数',
  `json_str` text COMMENT '报表json串',
  `report_type` varchar(20) DEFAULT NULL COMMENT '报表类型',
  `enable_flag` int(1) DEFAULT '1' COMMENT '0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG',
  `delete_flag` int(1) DEFAULT '0' COMMENT '0--未删除 1--已删除 DIC_NAME=DELETE_FLAG',
  `create_by` varchar(255) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(255) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int(8) DEFAULT NULL COMMENT '版本号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE_REPORT_CODE` (`report_code`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gaea_report_dashboard
-- ----------------------------
DROP TABLE IF EXISTS `gaea_report_dashboard`;
CREATE TABLE `gaea_report_dashboard` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '看板id',
  `report_code` varchar(50) NOT NULL COMMENT '报表编码',
  `title` varchar(254) DEFAULT NULL COMMENT '看板标题',
  `width` bigint(20) NOT NULL COMMENT '宽度px',
  `height` bigint(20) NOT NULL COMMENT '高度px',
  `background_color` varchar(24) DEFAULT NULL COMMENT '背景色',
  `background_image` varchar(254) DEFAULT NULL COMMENT '背景图片',
  `preset_line` varchar(4096) DEFAULT NULL COMMENT '工作台中的辅助线',
  `refresh_seconds` int(11) DEFAULT NULL COMMENT '自动刷新间隔秒，数据字典REFRESH_TYPE',
  `enable_flag` int(1) NOT NULL DEFAULT '1' COMMENT '0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG',
  `delete_flag` int(1) NOT NULL DEFAULT '0' COMMENT ' 0--未删除 1--已删除 DIC_NAME=DEL_FLAG',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序，降序',
  `create_by` varchar(64) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `UNIQUE_REPORT_CODE` (`report_code`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for gaea_report_dashboard_widget
-- ----------------------------
DROP TABLE IF EXISTS `gaea_report_dashboard_widget`;
CREATE TABLE `gaea_report_dashboard_widget` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '组件id',
  `report_code` varchar(50) NOT NULL COMMENT '报表编码',
  `type` varchar(24) NOT NULL COMMENT '组件类型参考字典DASHBOARD_PANEL_TYPE',
  `setup` varchar(4096) NOT NULL COMMENT '组件的渲染属性json',
  `data` varchar(4096) DEFAULT NULL COMMENT '组件的数据属性json',
  `set_code` varchar(50) DEFAULT NULL COMMENT '组件的数据来源数据集的编码',
  `position` varchar(4096) NOT NULL COMMENT '组件的大小位置属性json',
  `enable_flag` int(1) NOT NULL DEFAULT '1' COMMENT '0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG',
  `delete_flag` int(1) NOT NULL DEFAULT '0' COMMENT ' 0--未删除 1--已删除 DIC_NAME=DEL_FLAG',
  `sort` bigint(20) NOT NULL DEFAULT '0' COMMENT '排序，图层的概念',
  `create_by` varchar(64) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for gaea_report_data_set
-- ----------------------------
DROP TABLE IF EXISTS `gaea_report_data_set`;
CREATE TABLE `gaea_report_data_set` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `set_code` varchar(50) DEFAULT NULL COMMENT '数据集编码',
  `set_name` varchar(100) DEFAULT NULL COMMENT '数据集名称',
  `set_desc` varchar(255) DEFAULT NULL COMMENT '数据集描述',
  `source_code` varchar(50) DEFAULT NULL COMMENT '数据源编码',
  `dyn_sentence` varchar(2048) DEFAULT NULL COMMENT '动态查询sql或者接口中的请求体',
  `case_result` text COMMENT '结果案例',
  `enable_flag` int(1) DEFAULT '1' COMMENT '0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG',
  `delete_flag` int(1) DEFAULT '0' COMMENT '0--未删除 1--已删除 DIC_NAME=DELETE_FLAG',
  `create_by` varchar(255) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(255) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int(8) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `unique_set_code` (`set_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='数据集管理';

-- ----------------------------
-- Table structure for gaea_report_data_set_param
-- ----------------------------
DROP TABLE IF EXISTS `gaea_report_data_set_param`;
CREATE TABLE `gaea_report_data_set_param` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `set_code` varchar(50) DEFAULT NULL COMMENT '数据集编码',
  `param_name` varchar(50) DEFAULT NULL COMMENT '参数名',
  `param_desc` varchar(100) DEFAULT NULL COMMENT '参数描述',
  `param_type` varchar(255) DEFAULT NULL COMMENT '参数类型，字典=',
  `sample_item` varchar(1080) DEFAULT NULL COMMENT '参数示例项',
  `required_flag` int(1) DEFAULT '1' COMMENT '0--非必填 1--必填 DIC_NAME=REQUIRED_FLAG',
  `validation_rules` varchar(2048) DEFAULT NULL COMMENT 'js校验字段值规则，满足校验返回 true',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  `enable_flag` int(1) DEFAULT '1' COMMENT '0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG',
  `delete_flag` int(1) DEFAULT '0' COMMENT '0--未删除 1--已删除 DIC_NAME=DELETE_FLAG',
  `create_by` varchar(255) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(255) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int(8) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='数据集查询参数';

-- ----------------------------
-- Table structure for gaea_report_data_set_transform
-- ----------------------------
DROP TABLE IF EXISTS `gaea_report_data_set_transform`;
CREATE TABLE `gaea_report_data_set_transform` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `set_code` varchar(50) DEFAULT NULL COMMENT '数据集编码',
  `transform_type` varchar(50) DEFAULT NULL COMMENT '数据转换类型，DIC_NAME=TRANSFORM_TYPE; js，javaBean，字典转换',
  `transform_script` varchar(10800) DEFAULT NULL COMMENT '数据转换script,处理逻辑',
  `order_num` int(2) DEFAULT NULL COMMENT '排序,执行数据转换顺序',
  `enable_flag` int(1) DEFAULT '1' COMMENT '0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG',
  `delete_flag` int(1) DEFAULT '0' COMMENT '0--未删除 1--已删除 DIC_NAME=DELETE_FLAG',
  `create_by` varchar(255) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(255) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int(8) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='数据集数据转换';

-- ----------------------------
-- Table structure for gaea_report_data_source
-- ----------------------------
DROP TABLE IF EXISTS `gaea_report_data_source`;
CREATE TABLE `gaea_report_data_source` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `source_code` varchar(100) DEFAULT NULL COMMENT '数据源编码',
  `source_name` varchar(100) DEFAULT NULL COMMENT '数据源名称',
  `source_desc` varchar(255) DEFAULT NULL COMMENT '数据源描述',
  `source_type` varchar(50) DEFAULT NULL COMMENT '数据源类型 DIC_NAME=SOURCE_TYPE; mysql，orace，sqlserver，elasticsearch，接口，javaBean，数据源类型字典中item-extend动态生成表单',
  `source_config` varchar(2048) DEFAULT NULL COMMENT '数据源连接配置json：关系库{ jdbcUrl:'''', username:'''', password:'''' } ES{ hostList:''ip1:9300,ip2:9300,ip3:9300'', clusterName:''elasticsearch_cluster'' }  接口{ apiUrl:''http://ip:port/url'', method:'''' } javaBean{ beanNamw:''xxx'' }',
  `enable_flag` int(1) DEFAULT '1' COMMENT '0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG',
  `delete_flag` int(1) DEFAULT '0' COMMENT '0--未删除 1--已删除 DIC_NAME=DELETE_FLAG',
  `create_by` varchar(255) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(255) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int(8) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `unique_source_code` (`source_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='数据源管理';

-- ----------------------------
-- Table structure for gaea_rules
-- ----------------------------
DROP TABLE IF EXISTS `gaea_rules`;
CREATE TABLE `gaea_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_name` varchar(64) DEFAULT NULL COMMENT '规则名称',
  `rule_code` varchar(64) DEFAULT NULL COMMENT '规则编码',
  `rule_fields` text COMMENT '规则字段值',
  `rule_content` text COMMENT '规则内容',
  `rule_demo` varchar(255) DEFAULT NULL COMMENT '规则测试值',
  `enabled` tinyint(1) DEFAULT NULL COMMENT '启用状态：0：禁用 1：启用',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int(8) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_rule_code` (`rule_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gaea_rules_history
-- ----------------------------
DROP TABLE IF EXISTS `gaea_rules_history`;
CREATE TABLE `gaea_rules_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_code` varchar(64) DEFAULT NULL COMMENT '规则编码',
  `rule_demo` varchar(255) DEFAULT NULL COMMENT '规则测试值',
  `rule_content` text COMMENT '规则内容',
  `rule_result` varchar(64) DEFAULT NULL COMMENT '执行结果',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `version` int(8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gaea_ui_i18n
-- ----------------------------
DROP TABLE IF EXISTS `gaea_ui_i18n`;
CREATE TABLE `gaea_ui_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` varchar(10) NOT NULL DEFAULT 'zh' COMMENT '语言标识',
  `cata_type` varchar(32) DEFAULT NULL COMMENT '行业标识',
  `system` varchar(32) DEFAULT NULL COMMENT '所属系统',
  `module` varchar(20) DEFAULT NULL,
  `code` varchar(64) NOT NULL COMMENT '字段编码',
  `name` varchar(64) NOT NULL COMMENT '字段名称',
  `remark` varchar(64) NOT NULL COMMENT '业务描述',
  `refer` varchar(50) DEFAULT NULL COMMENT '关联表名',
  `enabled` int(1) NOT NULL DEFAULT '1' COMMENT '启用状态：1:启用 0:禁用',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新用户',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `version` int(11) DEFAULT NULL COMMENT '版本',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='字段命名国际化管理';
