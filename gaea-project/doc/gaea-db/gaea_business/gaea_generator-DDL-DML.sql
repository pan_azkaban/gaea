
create table t_lc_project(
	id bigint auto_increment comment 'ID' primary key,
	name varchar(55) not null comment '项目名称',
	project_root varchar(30) null comment '项目根目录名',
	code varchar(50) not null comment '项目code',
	git_user varchar(20) null,
	git_passwd varchar(20) null,
	git_repo varchar(120) null,
	dev_branch varchar(20) default 'dev' null,
	gen_branch varchar(20) default 'dev' null,
	db_type varchar(10) default 'mysql' null,
	db_connection_url varchar(120) null,
	db_schema varchar(10) null,
	db_user varchar(20) null,
	db_passwd varchar(20) null,
	status char not null,
	create_time datetime null,
	update_time datetime null,
	create_by varchar(20) null,
	update_by varchar(20) null,
	version int null
)comment '代码生成-项目级配置表';

create table code_column_config(
	id bigint auto_increment comment 'ID'	primary key,
	table_name varchar(25) null,
	column_name varchar(25) null,
	column_type varchar(25) null,
	dict_name varchar(25) null,
	extra varchar(255) null,
	form_show bit null,
	form_type varchar(25) null,
	key_type varchar(25) null,
	list_show bit null,
	not_null bit null,
	query_type varchar(25) null,
	remark varchar(255) null,
	date_annotation varchar(25) null,
	sub_entity varchar(50) null,
	sub_entity_field varchar(50) null,
	create_by varchar(20) null,
	create_time datetime null,
	update_time datetime null,
	update_by varchar(20) null,
	version int null
)
comment '代码生成-字段级配置';

create index idx_table_name on code_column_config (table_name);

create table code_gen_config(
	id bigint auto_increment comment 'ID'	primary key,
	project varchar(50) null,
	project_root varchar(30) null,
	table_name varchar(255) null comment '表名',
	author varchar(255) null comment '作者',
	cover bit null comment '是否覆盖',
	module_name varchar(255) null comment '模块名称',
	pack varchar(255) null comment '至于哪个包下',
	path varchar(255) null comment '前端代码生成的路径',
	api_path varchar(255) null comment '前端Api文件路径',
	prefix varchar(255) null comment '表前缀',
	api_alias varchar(255) null comment '接口名称',
	catatype varchar(10) default 'Mybatis' null,
	gen_ui bit default b'1' null,
	create_by varchar(20) null,
	create_time datetime null,
	update_time datetime null,
	update_by varchar(20) null,
	version int null
)
comment '代码生成-表配置';

create index idx_table_name on code_gen_config (table_name);

/**以下是代码生成示例表*/
create table t_alipay_config(
	id bigint auto_increment comment '主键'	primary key,
	app_id varchar(255) null comment '应用ID',
	charset varchar(255) null comment '编码',
	format varchar(255) null comment '响应格式',
	gateway_url varchar(255) null comment '网关地址',
	notify_url varchar(255) null comment '异步回调',
	private_key text null comment '私钥',
	public_key text null comment '公钥',
	return_url varchar(255) null comment '回调地址',
	sign_type varchar(255) null comment '签名方式',
	sys_service_provider_id varchar(255) null comment '商户号',
	create_by varchar(20) null,
	create_time datetime null,
	update_by varchar(20) null,
	update_time datetime null,
	version int null
)
;

create table t_device_info
(
	id bigint auto_increment
		primary key,
	device_model_id bigint not null comment '设备型号ID',
	device_name varchar(50) null comment '设备名称',
	org_code varchar(30) null comment '机构代码',
	org_name varchar(50) null comment '所属机构',
	device_ip varchar(20) null comment '设备ip',
	mac_address varchar(50) not null comment '设备mac地址',
	log_level varchar(255) null comment '日志级别',
	enable_flag int(1) default '1' not null comment '启用标志',
	delete_flag int(1) default '0' null comment '删除标志',
	create_by varchar(64) null comment '创建人',
	create_time datetime not null comment '创建时间',
	update_by varchar(64) null comment '更新人',
	update_time datetime null comment '更新时间',
	version int(10) null
)
comment '设备信息表'
;

create index index_deviceModelId
	on t_device_info (device_model_id)
;

create table t_device_log_detail
(
	id bigint auto_increment
		primary key,
	device_id bigint not null,
	log text null,
	create_by varchar(50) not null comment '创建人',
	create_time datetime not null comment '创建时间',
	update_by varchar(50) null comment '更新人',
	update_time datetime null comment '更新时间',
	version int(3) null
)
comment '设备日志详情表'
;

create table t_device_model
(
	id bigint auto_increment
		primary key,
	device_type varchar(50) null comment '设备类型 DIC_NAME=DEVICE_TYPE',
	manufacturer varchar(50) null comment '厂商名称DIC_NAME=MANUFACTURER',
	device_model varchar(50) null comment '设备型号DIC_NAME=device_cataType',
	enable_flag int(1) default '1' not null comment '启用标志',
	delete_flag int(1) default '0' null comment '删除标志',
	create_by varchar(50) not null comment '创建人',
	create_time datetime not null comment '创建时间',
	update_by varchar(50) null comment '更新人',
	update_time datetime null comment '更新时间',
	version int(3) null
)
comment '设备型号表'
;

ALTER TABLE code_gen_config ADD gen_type varchar(10) default 'i18n' after gen_ui;

ALTER TABLE code_column_config ADD default_value varchar(10) null after query_type;

ALTER TABLE code_column_config ADD max_length int after default_value;

INSERT INTO t_lc_project (name, project_root, code, git_user, git_passwd, git_repo, dev_branch, gen_branch, db_type, db_connection_url, db_schema, db_user, db_passwd, status, create_time, update_time, create_by, update_by, version) 
VALUES ('template-gaea', 'gaea-project-template', 'template-gaea', 'gituser', 'gitpasswd', 'daddf', 'dev', 'dev', 'mysql', 'aa', null, 'sd', 'sss', '1', null, '2021-04-06 10:40:08', null, 'lixiaoyan', null);

create table code_gen_project
(
  id bigint auto_increment comment 'ID'
    primary key,
  name varchar(55) not null comment '名称',
  project_root varchar(30) null,
  code varchar(50) not null comment 'code',
  git_user varchar(20) null,
  git_passwd varchar(20) null,
  git_repo varchar(120) null,
  dev_branch varchar(20) default 'dev' null,
  gen_branch varchar(20) default 'dev' null,
  db_type varchar(10) default 'mysql' null,
  db_connection_url varchar(120) null,
  db_schema varchar(10) null,
  db_user varchar(20) null,
  db_passwd varchar(20) null,
  status char not null,
  create_time datetime null,
  update_time datetime null,
  create_by varchar(20) null,
  update_by varchar(20) null,
  version int null
) comment '代码生成-项目级配置'
;

insert into code_gen_project select * from t_lc_project;
