package com.anjiplus.template.gaea.business.modules.inf.inf.controller.dto;

import com.anji.plus.gaea.curd.dto.GaeaBaseDTO;
import lombok.Data;

/**
 * @ClassName InfParamDTO
 * @Description: 接口参数DTO
 * @Author dingkaiqiang
 * @Date 2021-03-22
 * @Version V1.0
 **/

@Data
public class InfParamDTO extends GaeaBaseDTO {


    /**
     * 接口维护表Id
     */
    private Long infId;
    /**
     * 参数名
     */
    private String paramName;
    /**
     * 描述
     */
    private String description;
    /**
     * 数据类型
     */
    private String dataType;
    /**
     * 示例值
     */
    private String paramDemo;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 校验
     */
    private String checkRule;
}
