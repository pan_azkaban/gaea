package com.anji.plus.gaea.job.executor.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @desc 执行器自动装配
 * @author 木子李*de
 * @date 2023-05-12 15:25:35.961
 **/
@Configuration
@EnableConfigurationProperties(ExecutorProperties.class)
@ComponentScan(value = {"com.anji.plus.gaea.job.executor"})
@ConditionalOnProperty(name= "spring.gaea.subscribes.executor.enabled", havingValue="true")
public class AutoConfiguration {
    private static Logger logger = LoggerFactory.getLogger(AutoConfiguration.class);

    @Autowired
    private ExecutorProperties executorProperties;

    @Autowired
    private ApplicationContext applicationContext;

    @Bean
    public ExecutorConfiguration executorConfiguration(){
        ExecutorConfiguration executorConfiguration = new ExecutorConfiguration();
        executorConfiguration.setApplicationContext(applicationContext);
        executorConfiguration.setExecutorProperties(executorProperties);
        return executorConfiguration;
    }

}
