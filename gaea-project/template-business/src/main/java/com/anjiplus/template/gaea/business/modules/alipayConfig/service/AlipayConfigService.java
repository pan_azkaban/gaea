
package com.anjiplus.template.gaea.business.modules.alipayconfig.service;

import com.anjiplus.template.gaea.business.modules.alipayconfig.dao.entity.AlipayConfig;
import com.anjiplus.template.gaea.business.modules.alipayconfig.controller.param.AlipayConfigParam;
import com.anji.plus.gaea.curd.service.GaeaBaseService;

/**
* @desc AlipayConfig 支付配置服务接口
* @author dev-user
* @date 2021-04-07 14:58:28.131
**/
public interface AlipayConfigService extends GaeaBaseService<AlipayConfigParam, AlipayConfig> {

    /***
     * 查询详情
     *
     * @param id
     * @return
     */
    AlipayConfig getDetail(Long id);
}