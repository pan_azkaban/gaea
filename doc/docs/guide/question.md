# 常见问题

## 应用类
### 跨服务Feign接口调用失效
当程序中出现定义的FeignClient接口失效，可以看下启动类中的@EnableFeignClients中的扫描包路径是否包含FeignClient接口所在的包。例如：
```java
@SpringBootApplication(scanBasePackages = {
        "com.anjiplus.template.gaea",
        "com.anji.plus"
})
@EnableFeignClients(basePackages = {"com.anjiplus.template.gaea"})
@EnabledGaeaConfiguration
public class AuthApplication {
    public static void main( String[] args ) {
        SpringApplication.run(AuthApplication.class);
    }

}

```
## 工具类
### IDEA中Properties文件如何显示中文
在IDEA中打开*Settings*  ->*Editor*  ->*File Encodings*后，勾选*Transparent native-to-ascii convertion*


## 其他

## 