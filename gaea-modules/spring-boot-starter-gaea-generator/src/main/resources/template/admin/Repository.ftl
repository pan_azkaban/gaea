package ${package}.dao;

import org.apache.ibatis.annotations.Mapper;

import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import ${package}.dao.entity.${className};

/**
* ${className} Mapper
* @author ${author}
* @date ${date}
**/
@Mapper
public interface ${className}Mapper extends GaeaBaseMapper<${className}> {

}