package com.anji.plus.gaea.push;

import com.anji.plus.gaea.annotation.condition.ConditionalOnGaeaComponent;
import com.anji.plus.gaea.push.type.dingding.GaeaDingTalkTemplate;
import com.anji.plus.gaea.push.type.dingding.GaeaPushDingDingProperties;
import com.anji.plus.gaea.push.type.dingding.template.DingDingTemplate;
import com.anji.plus.gaea.push.type.email.GaeaMailTemplate;
import com.anji.plus.gaea.push.type.email.GaeaPushMailProperties;
import com.anji.plus.gaea.push.type.sms.GaeaPushSmsProperties;
import com.anji.plus.gaea.push.type.sms.GaeaSmsTemplate;
import com.anji.plus.gaea.push.type.wechat.GaeaPushWeChatMiniAppProperties;
import com.anji.plus.gaea.push.type.wechat.GaeaWeChatMiniAppTemplate;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 推送组件
 * @author lr
 * @since 2021-01-15
 */
@Configuration
@EnableConfigurationProperties({GaeaPushProperties.class,GaeaPushSmsProperties.class, GaeaPushDingDingProperties.class, GaeaPushMailProperties.class, GaeaPushWeChatMiniAppProperties.class})
@ConditionalOnGaeaComponent(GaeaPushProperties.COMPONENT_NAME)
public class GaeaPushConfiguration {

    /**
     * 发送
     * @return
     */
    @Bean
    public GaeaSmsTemplate gaeaSmsTemplate() {
        return new GaeaSmsTemplate();
    }

    /**
     * 邮件发送
     * @return
     */
    @Bean
    public GaeaMailTemplate gaeaMailTemplate() {
        return new GaeaMailTemplate();
    }

    /**
     * 钉钉推送
     * @return
     */
    @Bean
    public GaeaDingTalkTemplate gaeaDingTalkTemplate(){
        return new GaeaDingTalkTemplate();
    }

    @Bean
    public DingDingTemplate dingDingTemplate() {
        return new DingDingTemplate();
    }

    /**
     * 微信小程序推送
     * @return
     */
    @Bean
    public GaeaWeChatMiniAppTemplate gaeaWeChatMiniAppTemplate(){
        return new GaeaWeChatMiniAppTemplate();
    }

}
