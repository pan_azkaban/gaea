
package com.anjiplus.template.gaea.business.modules.devicemodel.dao.entity;

import lombok.Data;
import io.swagger.annotations.ApiModelProperty;

import com.anji.plus.gaea.curd.entity.GaeaBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;

import javax.validation.constraints.*;
import java.sql.Timestamp;

/**
* @description 设备类型 entity
* @author dev-user
* @date 2021-04-07 17:27:35.130
**/
@TableName(value="t_device_model")
@Data
public class DeviceModel extends GaeaBaseEntity {
    @ApiModelProperty(value = "设备类型")
    private String deviceType;

    @ApiModelProperty(value = "厂商名称")
    private String manufacturer;

    @ApiModelProperty(value = "设备型号")
    private String deviceModel;

    @ApiModelProperty(value = "启用标志")
    private Integer enableFlag;

    @ApiModelProperty(value = "删除标志")
    private Integer deleteFlag;


}