package com.anjiplus.template.gaea.business.modules.inf.app.dao;

import com.anjiplus.template.gaea.business.modules.inf.app.dao.entity.GaeaInfAppRelation;
import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 接口应用关系表(GaeaInfAppRelation)Mapper
 *
 * @author ultrajiaming
 * @since 2021-04-08 17:37:07
 */
@Mapper
public interface GaeaInfAppRelationMapper extends GaeaBaseMapper<GaeaInfAppRelation> {


}