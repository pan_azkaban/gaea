/*
 * @Author: zyk
 * @Date: 2020-07-13 15:14:13
 * @Last Modified by: zyk
 * @Last Modified time: 2021-03-15 13:28:19
 */
import request from '@/utils/request'
// 权限管理菜单中几个页面的接口

// 菜单配置页面接口

// 菜单自定义列查询
export function queryMenuExtension(menuCode, params) {
  return request({
    url: `auth/menuextension/queryMenuExtension/${menuCode}`,
    method: 'GET',
    params,
  })
}
// 菜单自定义列查询
export function queryconditionList(data) {
  return request({
    url: `/auth/querycondition/list`,
    method: 'POST',
    data,
  })
}
// 新增自定义列
export function addMenuextension(data) {
  return request({
    url: `/auth/menuextension`,
    method: 'POST',
    data,
  })
}
// 编辑自定义列
export function editMenuextension(data) {
  return request({
    url: `/auth/menuextension`,
    method: 'PUT',
    data,
  })
}
// 新增高级搜索
export function addCondition(data) {
  return request({
    url: `/auth/querycondition`,
    method: 'POST',
    data,
  })
}
// 编辑高级搜索
export function eidtCondition(data) {
  return request({
    url: `/auth/querycondition`,
    method: 'PUT',
    data,
  })
}
// 高级搜索删除
export function delCondition(id) {
  return request({
    url: `/auth/querycondition/${id}`,
    method: 'DELETE',
  })
}
// 自定义列删除
export function delMenuextension(id) {
  return request({
    url: `/auth/menuextension/${id}`,
    method: 'DELETE',
  })
}
// 列表查询接口
export function getMenuList(params) {
  return request({
    url: '/auth/menu/pageList',
    method: 'GET',
    params,
  })
}
// 新增接口
export function addMenu(data) {
  return request({
    url: '/auth/menu',
    method: 'POST',
    data,
  })
}
// 编辑接口
export function editMenu(data) {
  return request({
    url: '/auth/menu',
    method: 'PUT',
    data,
  })
}
// 删除接口
export function deleteMenu(id) {
  return request({
    url: `/auth/menu/${id}`,
    method: 'DELETE',
  })
}
// 多删除接口
export function deleteMenus(data) {
  return request({
    url: `/auth/menu/delete/batch`,
    method: 'POST',
    data,
  })
}

// 获取父级菜单list
export function getParentMenuList(id) {
  return request({
    url: 'auth/menu/menuSelect',
    method: 'GET',
  })
}

// 菜单和按钮关联情况查询
export function queryActionTreeForMenu(code) {
  return request({
    url: `/auth/menu/queryActionTreeForMenu/${code}`,
    method: 'GET',
  })
}

// 菜单和按钮关联情况保存
export function saveActionTreeForMenu(data) {
  return request({
    url: '/auth/menu/saveActionTreeForMenu',
    method: 'POST',
    data,
  })
}

// 查询菜单树
export function getMenuTree() {
  return request({
    url: '/auth/menu/role/tree',
    method: 'GET',
  })
}
// 查询角色树
export function getRoleTree() {
  return request({
    url: '/auth/org/role/tree',
    method: 'GET',
  })
}

// // 用户页面接口

export function getUserList(params) {
  return request({
    url: '/auth/user/pageList',
    method: 'GET',
    params,
  })
}
// 新增接口
export function addUser(data) {
  return request({
    // url: '/auth/user/insertUser',
    url: '/auth/user',
    method: 'POST',
    data,
  })
}

// 编辑接口
export function editUser(data) {
  return request({
    url: '/auth/user',
    method: 'PUT',
    data,
  })
}

// 用户详情
export function userDetail(id, accessKey) {
  return request({
    url: `/auth/user/${id}?accessKey=${accessKey}`,
    method: 'GET',
  })
}
// 删除接口
export function deleteUser(id) {
  return request({
    url: `/auth/user/${id}`,
    method: 'DELETE',
  })
}
// 多删除接口
export function deleteUsers(data) {
  return request({
    url: `/auth/user/delete/batch`,
    // headers: { 'Content-Type': 'application/text;charset=UTF-8' },
    method: 'POST',
    data,
  })
}
// 获取角色树和被选中的节点
export function userGetRoleTree(username) {
  return request({
    url: `/auth/org/user/role/tree/${username}`,
    // url: `/auth/user/queryRoleTree/${username}`,
    method: 'GET',
  })
}
// 提交选中的菜单节点
export function saveRoleTree(data) {
  return request({
    url: '/auth/user/saveRoleTree',
    method: 'POST',
    data,
  })
}

// 提交选中的菜单节点
export function resetPwd(data) {
  return request({
    url: '/auth/user/resetPwd',
    method: 'POST',
    data,
  })
}

// // 权限管理

// 权限配置接口
export function getPermissionList(params) {
  return request({
    url: '/auth/gaeaAuthority/pageList',
    method: 'GET',
    params,
  })
}
// 删除接口
export function permissionDelete(id) {
  return request({
    url: `/auth/gaeaAuthority/${id}`,
    method: 'DELETE',
    params: { noTip: false },
  })
}
// 多删除接口
export function permissionsDelete(data) {
  return request({
    url: `/auth/gaeaAuthority/delete/batch`,
    method: 'POST',
    data,
  })
}
// 编辑接口
export function permissionEdit(data) {
  return request({
    url: `/auth/gaeaAuthority`,
    method: 'PUT',
    data,
  })
}
// 新增接口
export function permissionAdd(data) {
  return request({
    url: `/auth/gaeaAuthority/`,
    method: 'POST',
    data,
  })
}

// // 角色管理

export function getRoleList(params) {
  return request({
    url: '/auth/role/pageList',
    method: 'GET',
    params,
  })
}
// 新增接口
export function addRole(data) {
  return request({
    url: '/auth/role',
    method: 'POST',
    data,
  })
}

// 编辑接口
export function editRole(data) {
  return request({
    url: '/auth/role',
    method: 'PUT',
    data,
  })
}
// 查询接口
export function queryRoleByPrimarykey(row) {
  return request({
    url: `/auth/role/${row.id}?accessKey=${row.accessKey}`,
    method: 'GET',
  })
}
// 删除接口
export function deleteRole(id) {
  return request({
    url: `/auth/role/${id}`,
    method: 'DELETE',
  })
}

// 多删除接口
export function deleteRoles(data) {
  return request({
    url: `/auth/role/delete/batch`,
    method: 'POST',
    data,
  })
}
// 获取全部菜单
export function allMenu() {
  return request({
    url: `/auth/menu/tree`,
    method: 'GET',
  })
}
// 获取选中的菜单
export function actionTree(orgCode, code) {
  return request({
    url: `/auth/role/tree/authorities/${orgCode}/${code}`,
    method: 'GET',
  })
}

// 新增接口
export function saveMenuTree(data) {
  return request({
    url: '/auth/role/roleMenuAuthorities',
    method: 'POST',
    data,
  })
}

// 角色关联组织弹窗查询接口
export function getOrgTree(code) {
  return request({
    url: `/auth/role/org/${code}`,
    method: 'GET',
  })
}
// 提交选中的菜单节点
export function saveOrgTree(data) {
  return request({
    url: '/auth/role/mapping/org',
    method: 'POST',
    data,
  })
}

// 角色关联组织弹窗查询接口
export function getAuthTree(orgCode, roleCode) {
  return request({
    url: `auth/menu/authority/tree/${orgCode}/${roleCode}`,
    method: 'GET',
  })
}
// 提交选中的菜单节点
export function saveAuthTree(data) {
  return request({
    url: '/auth/menu/mapper/authorities',
    method: 'POST',
    data,
  })
}
// 解锁
export function unLock(data) {
  return request({
    url: '/auth/user/unLock',
    method: 'POST',
    data,
  })
}
/**
 * 刷新数据字典
 * @param data
 */
export function freshUsername(data) {
  return request({
    url: '/auth/user/refresh/username',
    method: 'POST',
    data,
  })
}
