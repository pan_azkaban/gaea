package com.anji.plus.gaea.log.aspect;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * 日志相关数据
 * @author lr
 */
public class LogOperation implements Serializable {

    /**
     * 租户
     */
    private String tenantCode;

    private String userName;
    private String createBy;

    /**
     * 请求路径
     */
    private String requestUrl;

    /**
     * 页面或按钮标题
     */
    private String pageTitle;

    /**
     * 请求参数
     */
    private String requestParam;

    /**
     * 响应参数
     */
    private String responseParam;

    /**
     * 来源IP
     */
    private String sourceIp;

    /**
     * 请求方式get/post/put/delete
     */
    private String requestMethod;

    /**
     * 访问时间
     */
    private Date requestTime;
    /*上游请求时间戳，从header里获取*/
    private String reqTimeStamp;
    private Date responseTime;

    private String orgCode;
    private String terminal;
    private String locale;

    private Map extend;

    @Override
    public String toString() {
        return "LogOperation{" +
                "tenantCode='" + tenantCode + '\'' +
                ", userName='" + userName + '\'' +
                ", createBy='" + createBy + '\'' +
                ", requestUrl='" + requestUrl + '\'' +
                ", pageTitle='" + pageTitle + '\'' +
                ", requestParam='" + requestParam + '\'' +
                ", responseParam='" + responseParam + '\'' +
                ", sourceIp='" + sourceIp + '\'' +
                ", requestMethod='" + requestMethod + '\'' +
                ", requestTime=" + requestTime +
                ", responseTime=" + responseTime +
                ", reqTimeStamp='" + reqTimeStamp + '\'' +
                ", orgCode='" + orgCode + '\'' +
                ", terminal='" + terminal + '\'' +
                ", locale='" + locale + '\'' +
                '}';
    }

    public Map getExtend() {
        return extend;
    }

    public void setExtend(Map extend) {
        this.extend = extend;
    }

    public String getReqTimeStamp() {
        return reqTimeStamp;
    }

    public void setReqTimeStamp(String reqTimeStamp) {
        this.reqTimeStamp = reqTimeStamp;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getRequestParam() {
        return requestParam;
    }

    public void setRequestParam(String requestParam) {
        this.requestParam = requestParam;
    }

    public String getResponseParam() {
        return responseParam;
    }

    public void setResponseParam(String responseParam) {
        this.responseParam = responseParam;
    }

    public String getSourceIp() {
        return sourceIp;
    }

    public void setSourceIp(String sourceIp) {
        this.sourceIp = sourceIp;
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    public Date getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Date requestTime) {
        this.requestTime = requestTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getLocale() {
        return locale;
    }

    public Date getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Date responseTime) {
        this.responseTime = responseTime;
    }
}
