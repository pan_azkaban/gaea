package com.anjiplus.template.gaea.common;

/**
 * 功能描述：
 * 异常code ,具体的提示信息见 i18n/messages.properties
 *
 * @Author: peiyanni
 * @Date: 2021/2/7 10:52
 */
public class RespCommonCode {

    /**
     * 用户相关
     */
    public static final String AUTH_PASSWORD_NOTSAME = "1001";
    public static final String USER_PASSWORD_CONFIG_PASSWORD_CANOT_EQUAL = "1002";
    public static final String OLD_PASSWORD_ERROR = "1003";
    public static final String USER_ONTEXIST_ORGINFO = "1004";
    public static final String USER_ONTEXIST_ROLEINFO = "1005";
    public static final String MENU_TABLE_CODE_EXIST = "1006";
    public static final String USER_CODE_ISEXIST = "1007";
    public static final String ROLE_CODE_ISEXIST = "1008";
    public static final String MENU_CODE_ISEXIST = "1009";
    public static final String ORG_CODE_ISEXIST = "1010";
    //高级查询搜索
    public static final String SEARCHNAME_ISEXIST = "1011";
    //参数管理
    public static final String SETTINGNAME_ISEXIST = "1012";
    public static final String DICCODE_ISEXIST="1013";
    //app端推送deviceId
    public static final String DEVICEID_LENGTH="1014";
    public static final String USERINFO_EMPTY="1015";

    /**
     * 文件相关
     */
    public static final String FILE_EMPTY_FILENAME = "2001";
    public static final String FILE_SUFFIX_UNSUPPORTED = "2002";
    public static final String FILE_UPLOAD_ERROR = "2003";
    public static final String FILE_ONT_EXSIT = "2004";
    public static final String LIST_IS_EMPTY = "2005";


    /**
     * 推送模板
     */
    public static final String PUSHCODE_NEED_UNIQUE = "3001";
    public static final String RECEIVER_IS_EMPTY="3002";
    /**
     * 数据源相关
     */
    public static final String DATA_SOURCE_CONNECTION_FAILED = "4001";
    public static final String DATA_SOURCE_TYPE_DOES_NOT_MATCH_TEMPORARILY = "4002";
    public static final String EXECUTE_SQL_ERROR = "4003";
    public static final String INCOMPLETE_PARAMETER_REPLACEMENT_VALUES = "4004";
    public static final String EXECUTE_JS_ERROR = "4005";
    public static final String ANALYSIS_DATA_ERROR = "4006";
    public static final String REPORT_CODE_ISEXIST = "4007";
    public static final String SET_CODE_ISEXIST = "4008";
    public static final String SOURCE_CODE_ISEXIST = "4009";
    public static final String CLASS_NOT_FOUND = "4010";
}
