package com.anji.plus.gaea.oss.config;

import java.io.Serializable;

/**
 * 云储存 公共配置项
 * @author WongBin
 * @date 2023/8/24
 */

public class OSSBaseProperties implements Serializable {
    private String type;
    private String url;
    private String accessKey;
    private String secretKey;
    private String bucketName;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }
}
