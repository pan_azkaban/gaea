package com.anjiplus.template.gaea.business.feign;

import com.anji.plus.gaea.bean.ResponseBean;
import com.anjiplus.template.gaea.business.feign.dto.AppspPushParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * <pre>
 * AuthServiceClient
 * </pre>
 * , fallback = CommonBaseServiceFallback.class
 *
 * @author peiyanni
 * @version AuthServiceClient.java
 */
@FeignClient(name = "gaea-auth",contextId ="gaea-auth01" ,fallback = AuthServiceClientFallback.class)
public interface AuthServiceClient {
    @RequestMapping(value = "/apppush/testpush", method = RequestMethod.POST)
    ResponseBean testAppspPush(@RequestBody AppspPushParam pushParam);
}
