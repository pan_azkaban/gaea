/**/
package com.anjiplus.template.gaea.business.modules.devicemodel.controller.param;

import lombok.Data;
import java.io.Serializable;
import com.anji.plus.gaea.annotation.Query;
import com.anji.plus.gaea.constant.QueryEnum;
import com.anji.plus.gaea.curd.params.PageParam;

import java.util.List;


/**
* @desc DeviceModel 设备类型查询输入类
* @author dev-user
* @date 2021-04-07 17:27:35.130
**/
@Data
public class DeviceModelParam extends PageParam implements Serializable{

    /** 精确查询 */
    @Query
    private String deviceType;

    /** 精确查询 */
    @Query
    private String manufacturer;

    /** 精确查询 */
    @Query
    private String deviceModel;

    /** 精确查询 */
    @Query
    private Integer enableFlag;

    /** 精确查询 */
    @Query
    private Integer deleteFlag;
}