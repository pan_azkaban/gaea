package com.anjiplus.template.gaea.business.modules.rules.controller.param;

import com.anji.plus.gaea.curd.params.PageParam;
import java.io.Serializable;

/**
 * (GaeaRulesHistory)param
 *
 * @author why
 * @since 2021-03-17 13:52:55
 */
public class GaeaRulesHistoryParam extends PageParam implements Serializable {

}