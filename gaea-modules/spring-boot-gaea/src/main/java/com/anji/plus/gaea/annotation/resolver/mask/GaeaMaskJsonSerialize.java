package com.anji.plus.gaea.annotation.resolver.mask;

import com.anji.plus.gaea.utils.GaeaMaskUtils;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.fasterxml.jackson.databind.ser.ResolvableSerializer;
import com.fasterxml.jackson.databind.ser.std.NullSerializer;
import com.anji.plus.gaea.annotation.GaeaMask;

import java.io.IOException;

/**
 * 数据脱敏注解解析
 * @author lr
 * @since 2021-02-05
 * @see JsonSerialize#using()
 */
public class GaeaMaskJsonSerialize extends JsonSerializer<String> implements ContextualSerializer{

    /**
     * 脱敏注解
     */
    private GaeaMask gaeaMask;

    public GaeaMaskJsonSerialize(){}

    public GaeaMaskJsonSerialize(GaeaMask gaeaMask) {
        this.gaeaMask = gaeaMask;
    }

    /**
     * Jackson序列化时会调用该方法，自定义属性值
     * @param value
     * @param gen
     * @param serializers
     * @throws IOException
     */
    @Override
    public void serialize(String value, JsonGenerator gen, SerializerProvider serializers) throws IOException {

        String result = value;
        //脱敏注解
        if (gaeaMask != null) {
            MaskEnum type = gaeaMask.type();

            result = GaeaMaskUtils.getMaskValue(result,type,gaeaMask);
        }
        gen.writeString(result);
    }

    /**
     * Jackson序列化时会调用该方法创建JsonSerializer
     * @param prov
     * @param property
     * @return
     * @throws JsonMappingException
     */
    @Override
    public JsonSerializer<?> createContextual(SerializerProvider prov, BeanProperty property) throws JsonMappingException {
        if (property != null) {
            gaeaMask = property.getAnnotation(GaeaMask.class);
            if (gaeaMask == null) {
                gaeaMask = property.getContextAnnotation(GaeaMask.class);
            }
            if (gaeaMask != null) {
                return new GaeaMaskJsonSerialize(gaeaMask);
            }
            return prov.findValueSerializer(property.getType(), property);
        }

        return NullSerializer.instance;
    }
}
