package com.anjiplus.template.gaea.auth.modules.log.service.impl;

import com.anji.plus.gaea.constant.GaeaConstant;
import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import com.anji.plus.gaea.exception.BusinessExceptionBuilder;
import com.anji.plus.gaea.export.enums.ExportTypeEnum;
import com.anji.plus.gaea.export.utils.ExportUtil;
import com.anji.plus.gaea.export.vo.ExportOperation;
import com.anji.plus.gaea.holder.UserContentHolder;
import com.anji.plus.gaea.log.aspect.LogOperation;
import com.anji.plus.gaea.utils.GaeaDateUtils;
import com.anjiplus.template.gaea.auth.modules.log.controller.param.GaeaLogParam;
import com.anjiplus.template.gaea.auth.modules.log.dao.GaeaLogMapper;
import com.anjiplus.template.gaea.auth.modules.log.entity.GaeaLog;
import com.anjiplus.template.gaea.auth.modules.log.service.GaeaLogService;
import com.anjiplus.template.gaea.common.RespCommonCode;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * (GaeaLog)ServiceImpl
 *
 * @author peiyanni
 * @since 2021-02-18 16:30:23
 */
@Service
public class GaeaLogServiceImpl implements GaeaLogService {
    @Autowired
    private GaeaLogMapper gaeaLogMapper;

    @Resource(name="threadPoolGaeaLogExecutor")
    private ThreadPoolTaskExecutor threadPoolGaeaLogExecutor;

    @Value("${file.dist-path}")
    private String dictPath;

    private static final String fileTitleDefault = "Export.log";

    @Override
    public GaeaBaseMapper<GaeaLog> getMapper() {
        return gaeaLogMapper;
    }

    @Override
    public void saveCallbackInfo(LogOperation logOperation) {
        GaeaLog gaeaLog = new GaeaLog();
        BeanUtils.copyProperties(logOperation, gaeaLog);
        if (StringUtils.isEmpty(gaeaLog.getUserName())) {
            gaeaLog.setUserName(UserContentHolder.getContext().getUsername());
        }
        gaeaLogMapper.insert(gaeaLog);
    }

    @Override
    public Boolean exportLogToFile(GaeaLogParam gaeaLogParam) {
        ExportOperation exportOperation = new ExportOperation();
        //指明导出数据查询到结果开始时间
        exportOperation.setResultStartTime(LocalDateTime.now());
        String[] timeArr = StringUtils.isNotEmpty(gaeaLogParam.getRequestTime()) ? gaeaLogParam.getRequestTime().split(",") : null;
        String startTime = (null != timeArr) ? timeArr[0] : GaeaDateUtils.toString(new Date(), GaeaConstant.DATE_PATTERN);
        String endTime = (null != timeArr) ? timeArr[1] : null;
        List<GaeaLog> list = gaeaLogMapper.queryLogInfo(gaeaLogParam, startTime, endTime);
        if (CollectionUtils.isEmpty(list)) {
            throw BusinessExceptionBuilder.build(RespCommonCode.LIST_IS_EMPTY);
        }
        //指明导出数据查询到结果结束时间
        exportOperation.setResultEndTime(LocalDateTime.now());
        //指明导出数据查询到结果条数
        exportOperation.setResultSize(Long.parseLong(list.size() + ""));
        //指明采用什么模式导出
        exportOperation.setExportType(ExportTypeEnum.SIMPLE_EXCEL.getCodeValue());
        //设置导出的文件名
        exportOperation.setFileTitle(StringUtils.isEmpty(gaeaLogParam.getFileTitle()) ? fileTitleDefault : gaeaLogParam.getFileTitle());
        //设置导出的文件存放目录
        exportOperation.setFilePath(dictPath);
        //设置导出的数据集合
        exportOperation.setList(list);
        //保存当前操作人
        exportOperation.setCreaterUsername(UserContentHolder.getContext().getUsername());
        //调用盖亚组件实现导出文件
        threadPoolGaeaLogExecutor.execute(() -> {
            ExportUtil.getInstance().exportByFilePathSimple(exportOperation, GaeaLog.class);
        });
        return true;
    }
}
