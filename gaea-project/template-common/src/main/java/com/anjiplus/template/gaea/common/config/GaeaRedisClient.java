package com.anjiplus.template.gaea.common.config;

import com.anjiplus.template.gaea.common.MagicValueConstants;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.util.Map;

/**
 * redis客户端
 */
@Configuration
public class GaeaRedisClient {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;


    /**
     * 发送消息
     *
     * @param params
     */
    public void sendMessage(String handerName, Map<String,String> params) {
        params.put(MagicValueConstants.HANDLER_NAME,handerName);
        redisTemplate.convertAndSend(MagicValueConstants.REDIS_TOPIC_NAME, params);
    }


}
