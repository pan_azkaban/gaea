/*
 * @Author: zyk
 * @Date: 2020-07-13 15:21:58
 * @Last Modified by: zyk
 * @Last Modified time: 2021-03-15 13:39:53
 */
import Vue from 'vue'

import Cookies from 'js-cookie'

import 'normalize.css/normalize.css' // 各浏览器样式统一化

import Element from 'element-ui'
import './styles/element-variables.scss'

import '@/styles/index.scss' // 全局 css
import formCreate from '@form-create/element-ui'
import anjiSelect from '@/components/AnjiPlus/anji-select'

Vue.use(formCreate)
import App from './App'
import store from './store'
import router from './router'

import i18n from './lang' // 国际化
import './icons' // icon
import './permission' // 菜单权限控制

import mixin from './mixins/index'
Vue.mixin(mixin)

import VueSuperSlide from 'vue-superslide'
Vue.use(VueSuperSlide)

// 全局定义echarts
import ECharts from 'vue-echarts'
import 'echarts/lib/chart/bar'
import 'echarts/lib/component/tooltip'
// import 'echarts-gl'
Vue.component('v-chart', ECharts)

// 按钮权限的指令
import permission from '@/directive/permission/index'
Vue.use(permission)

Vue.component('anji-select', anjiSelect)

/**
 * 如果不使用模拟服务器
 * 可用MockJs来模拟api
 * 可以执行:mockXHR()
 *
 * 请在上线前删除!!!
 */
if (process.env.NODE_ENV === 'development') {
  const { mockXHR } = require('../mock')
  mockXHR()
}

/**
 * 后续自行添加的模块和全局变量
 */
import '@/utils/common'

Vue.use(Element, {
  size: Cookies.get('size') || 'mini', // 设置 element-ui的默认size为mini
  i18n: (key, value) => i18n.t(key, value),
})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: (h) => h(App),
})
