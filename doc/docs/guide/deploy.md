# 系统运行
微服务版本的系统运行,可以在本地开发环境下跑起来。

## 准备工作
1. 环境准备
- JDK >= 1.8 (推荐1.8版本)
- Mysql >= 5.7.0 (推荐5.7版本)
- Redis >= 3.0
- Maven >= 3.0
- Node >= 10
- Nacos >= 1.1.0

2. 从gitee上拉取下载项目源码,并解压到工作目录;

前端:<https://gitee.com/anji-plus/gaea/tree/master/gaea-project-template/template-gaea-ui>

后端:<https://gitee.com/anji-plus/gaea/tree/master/gaea-project-template>

## 运行系统
### 后端运行
1. 项目解压到工作目录后，需要导入到对应的开发工具中。
2. 创建数据库gaea_auth,并执行对应的sql脚本--[gaea_auth数据库脚本](https://gitee.com/anji-plus/gaea/tree/master/gaea-project-template/gaea-refer-doc/gaea-db);
3. 创建数据库gaea_business,并执行对应的sql脚本--[gaea_business数据库脚本](https://gitee.com/anji-plus/gaea/tree/master/gaea-project-template/gaea-refer-doc/gaea-db);
4. 导入nacos配置文件，并修改对应的数据源地址，redis地址--[nacos配置文件](https://gitee.com/anji-plus/gaea/tree/master/gaea-project-template/gaea-refer-doc/gaea-nacos);
5. 修改文件bootstap-dev.xml中的nacos服务器地址；
6. 启动如下项目;
	- gaea-gateway   //网关服务（必选）
	- gaea-auth      //权限基础服务（必选）
	- gaea-business  //业务服务（可选）


### 前端运行
1. 安装
如果不使用git，可以下载源码后解压，直接进入到解压后的项目目录，并修改对应的项目接口地址后操作;
```
# 克隆项目
git clone https://gitee.com/anji-plus/gaea.git
# 或者
git clone git@gitee.com:anji-plus/gaea.git

# 进入项目目录
cd web-template

# 安装依赖
npm install


# 启动服务
npm run dev
```
2. 浏览器访问
http://localhost:8080,出现界面，并输入默认用户名 admin/p@ss1234登录成功，则前后端运行成功;

3. 发布
```
# 构建生产环境
npm run build:prod
```

4. 其他
```
# 预览发布环境效果
npm run preview

# 预览发布环境效果 + 静态资源分析
npm run preview -- --report

# 代码格式检查
npm run lint

# 代码格式检查并自动修复
npm run lint -- --fix
```
