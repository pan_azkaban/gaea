package com.anjiplus.template.gaea.business.modules.inf.app.controller.param;


import com.anji.plus.gaea.annotation.Query;
import com.anji.plus.gaea.constant.QueryEnum;
import com.anji.plus.gaea.curd.params.PageParam;
import lombok.Data;

import java.io.Serializable;

/**
 * 接口应用关系表(GaeaInfAppRelation)param
 *
 * @author ultrajiaming
 * @since 2021-04-08 17:38:25
 */
@Data
public class GaeaInfAppRelationParam extends PageParam implements Serializable {
    @Query(QueryEnum.LIKE)
    private String appId;
    @Query(QueryEnum.LIKE)
    private String appName;
    @Query(QueryEnum.LIKE)
    private String infName;
    @Query(QueryEnum.LIKE)
    private String infNameName;

}
