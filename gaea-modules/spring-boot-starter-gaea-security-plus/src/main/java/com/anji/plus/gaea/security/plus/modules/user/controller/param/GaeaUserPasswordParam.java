package com.anji.plus.gaea.security.plus.modules.user.controller.param;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 用户表(GaeaUser)param
 *
 * @author peiyanni
 * @since 2021-02-07 13:38:12
 */

public class GaeaUserPasswordParam implements Serializable {
    private String username;
    /**
     * 新密码
     */
    @NotBlank(message = "password not empty")
    @Size(min = 8,message ="Password at least 8 characters" )
    private String password;
    /**修改密码时旧密码*/
    @NotBlank(message = "oldPassword not empty")
    private String oldPassword;

    /**修改密码时确认密码*/
    @NotBlank(message = "confirmPassword not empty")
    private String confirmPassword;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
