package com.anjiplus.template.gaea.business.modules.rules.dao.entity;

import com.anji.plus.gaea.curd.entity.GaeaBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * (GaeaRulesHistory)实体类
 *
 * @author why
 * @since 2021-03-17 13:52:55
 */
@TableName("gaea_rules_history")
@Getter
@Setter
public class GaeaRulesHistory extends GaeaBaseEntity implements Serializable {
    /**
    * 规则编码
    */    
    @ApiModelProperty(value = "规则编码")
    private String ruleCode;
    /**
    * 规则测试值
    */    
    @ApiModelProperty(value = "规则测试值")
    private String ruleDemo;
    /**
    * 规则内容
    */    
    @ApiModelProperty(value = "规则内容")
    private String ruleContent;
    /**
    * 执行结果
    */    
    @ApiModelProperty(value = "执行结果")
    private String ruleResult;

}