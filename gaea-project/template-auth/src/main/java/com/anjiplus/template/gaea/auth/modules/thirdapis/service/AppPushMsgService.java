package com.anjiplus.template.gaea.auth.modules.thirdapis.service;


import com.anjiplus.template.gaea.auth.modules.thirdapis.controller.param.AppspPushParam;
import com.anjiplus.template.gaea.auth.modules.thirdapis.dto.GaeaPushBatchDto;
import com.anjiplus.template.gaea.auth.modules.thirdapis.dto.GaeaPushInitDto;

/**
 * 功能描述：
 * app端推送
 * @Author: peiyanni
 * @Date: 2021/4/6 10:07
 */
public interface AppPushMsgService {
    /**
     * 推送消息-初始化接口
     * 用户登录时获取设备号并异步调用此接口进行推送初始化
     * @param initDto
     * @return
     */
    Boolean pushMsgInit(GaeaPushInitDto initDto);

    /**
     * 推送消息-批量推送接口
     * @param batchDto
     * @return
     */
    Boolean pushMsgBatch(GaeaPushBatchDto batchDto);

    /**
     * 推送消息=全部推送接口
     * @param allDto
     * @return
     */
    Boolean pushMsgAll(GaeaPushBatchDto allDto);

    /**
     * 推送测试
     * @param pushParam
     * @return
     */
    Boolean testPush(AppspPushParam pushParam);
}
