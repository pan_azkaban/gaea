/*
 * @Author: zyk
 * @Date: 2020-07-13 15:34:51
 * @Last Modified by: zyk
 * @Last Modified time: 2021-03-15 13:58:25
 */
import { baseList } from './modules/baseList'
import { webList } from './modules/webList'
// 模拟从后端获取的动态路由
export const routerList = [...baseList, ...webList]
