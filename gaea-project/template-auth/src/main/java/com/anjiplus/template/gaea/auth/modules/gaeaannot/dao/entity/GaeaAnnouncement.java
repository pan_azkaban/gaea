package com.anjiplus.template.gaea.auth.modules.gaeaannot.dao.entity;

import com.anji.plus.gaea.curd.entity.GaeaBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统公告信息表(GaeaAnnouncement)实体类
 *
 * @author makejava
 * @since 2021-03-29 15:17:17
 */
@TableName("gaea_announcement")
public class GaeaAnnouncement extends GaeaBaseEntity implements Serializable {
    /**
     * 标题
     */
    private String title;
    /**
     * 内容
     */
    private String msgContent;
    /**
     * 开始时间
     */
    private Date startTime;
    /**
     * 结束时间
     */
    private Date endTime;
    /**
     * 优先级 （优先级（L低，M中，H高））
     */
    private String priority;
    /**
     * 消息类型1:通知公告2:系统消息
     */
    private String msgCategory;
    /**
     * 通告对象类型（USER:指定用户，ALL:全体用户）
     */
    private String msgType;
    /**
     * 发布状态（0未发布，1已发布，2已撤销）
     */
    private Integer sendStatus;

    /**
     * 发布人
     */
    private String publisher;

    /**
     * 发布时间
     */
    private Date sendTime;
    /**
     * 撤销时间
     */
    private Date cancelTime;
    /**
     * 指定用户，多个逗号隔开
     */
    private String userNames;
    /**
     * 关联的业务数据
     */
    private String relationInfo;
    /**
     * 关联的业务类型
     */
    private String relationType;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getMsgCategory() {
        return msgCategory;
    }

    public void setMsgCategory(String msgCategory) {
        this.msgCategory = msgCategory;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public Integer getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(Integer sendStatus) {
        this.sendStatus = sendStatus;
    }

    public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    public Date getCancelTime() {
        return cancelTime;
    }

    public void setCancelTime(Date cancelTime) {
        this.cancelTime = cancelTime;
    }

    public String getUserNames() {
        return userNames;
    }

    public void setUserNames(String userNames) {
        this.userNames = userNames;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getRelationInfo() {
        return relationInfo;
    }

    public void setRelationInfo(String relationInfo) {
        this.relationInfo = relationInfo;
    }

    public String getRelationType() {
        return relationType;
    }

    public void setRelationType(String relationType) {
        this.relationType = relationType;
    }
}