package com.anjiplus.template.gaea.business.test;

import com.alibaba.fastjson.JSONObject;
import com.anjiplus.template.gaea.business.modules.data.dataSource.controller.dto.DataSourceDto;
import com.anjiplus.template.gaea.business.modules.data.dataSource.pool.datasource.PooledDataSource;
import com.anjiplus.template.gaea.business.modules.data.dataSource.pool.util.JdbcUtil;
import com.anjiplus.template.gaea.business.modules.data.dataSource.service.impl.DataSourceServiceImpl;

import org.junit.Test;

import java.sql.*;
import java.util.List;

/**
 * Created by raodeming on 2021/3/18.
 */
public class DataSourceServiceImplTest {

    @Test
    public void mysql() {
        try {
            // 1.注册驱动，与数据库建立连接
            //不建议使用： DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            //导致驱动被注册两次，强烈依赖数据库的驱动jar
            //1.加载驱动
            Class.forName("com.mysql.cj.jdbc.Driver");
            // 2.获取连接Connection
            //主机:端口号/数据库名
            String conn = "jdbc:mysql://10.108.6.131:3306/test_db?useUnicode=true&characterEncoding=UTF-8&serverTimezone=GMT%2B8";
            Connection connection = DriverManager.getConnection(conn, "root", "p@ss1234");
            if (connection != null) {
                System.out.println("数据库连接成功");
            }

            // 3.得到执行sql语句的对象Statement
            Statement stmt = connection.createStatement();
            // 4.执行sql语句，并返回结果
            ResultSet rs = stmt.executeQuery("select 1");
            // 5.处理结果
            while(rs.next()){
                System.out.println(rs.getObject(1));
                System.out.println("---------------------");
            }
            // 6.关闭资源
            rs.close();
            stmt.close();
            connection.close();

        } catch (ClassNotFoundException e) {
            System.out.println("数据库连接失败：驱动类不存在");
            e.printStackTrace();
        } catch (Exception throwables) {
            System.out.println("数据库连接失败:" + throwables.getMessage());
            throwables.printStackTrace();
        }

    }


    @Test
    public void simpleTest() throws SQLException, ClassNotFoundException {
        PooledDataSource source = new PooledDataSource();
        source.setJdbcUrl("jdbc:mysql://10.108.6.131:3306/test_db?useUnicode=true&characterEncoding=UTF-8&serverTimezone=GMT%2B8");
        source.setUser("root");
        source.setPassword("p@ss1234");
        source.setDriverClass("com.mysql.cj.jdbc.Driver");
        source.setMinSize(5);
        // 初始化
        source.init();
        Connection connection = source.getConnection();
        System.out.println(connection.getCatalog());

        Connection connection2 = source.getConnection();
        System.out.println(connection2.getCatalog());


        for (int i = 0; i < 10; i++) {
            Connection connection3 = source.getConnection();
            System.out.println(connection3.getCatalog());
//            connection.close();
        }
    }

    @Test
    public void jdbcTest() throws SQLException {
        DataSourceDto dto = new DataSourceDto();
        dto.setUsername("root");
        dto.setPassword("p@ss1234");
        dto.setDriverName("com.mysql.cj.jdbc.Driver");
        dto.setJdbcUrl("jdbc:mysql://10.108.6.131:3306/test_db?useUnicode=true&characterEncoding=UTF-8&serverTimezone=GMT%2B8");

        for (long i = 0L; i < 10L; i++) {
            dto.setId(i);
            Connection connection = JdbcUtil.getPooledConnection(dto);
            System.out.println(connection.getCatalog() + "---" + i);
        }
        System.out.println("--------------------------");
        dto.setId(1L);

        for (int i = 0; i < 10; i++) {
            Connection pooledConnection = JdbcUtil.getPooledConnection(dto);
            System.out.println(pooledConnection+"---------------------");
            pooledConnection.close();

        }


    }

    @Test
    public void unPool() {
        DataSourceDto dto = new DataSourceDto();
        dto.setUsername("root");
        dto.setPassword("p@ss1234");
        dto.setDriverName("com.mysql.cj.jdbc.Driver");
        dto.setJdbcUrl("jdbc:mysql://10.108.6.131:3306/test_db?useUnicode=true&characterEncoding=UTF-8&serverTimezone=GMT%2B8");

        try {
            Connection unPooledConnection = JdbcUtil.getUnPooledConnection(dto);
            System.out.println(unPooledConnection.getCatalog());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


    }

    @Test
    public void testExecuteSql() {
        DataSourceServiceImpl dataSourceService = new DataSourceServiceImpl();
        DataSourceDto dto = new DataSourceDto();
        dto.setSourceType("mysql");
        dto.setSourceConfig("{\"jdbcUrl\":\"jdbc:mysql://10.108.6.131:3306/test_db?useUnicode=true&characterEncoding=UTF-8&serverTimezone=GMT%2B8\",\"username\":\"root\",\"password\":\"p@ss1234\",\"driverName\":\"com.mysql.cj.jdbc.Driver\"}");
        String sql = "select * from t_device_info";
//        sql = "select count(1) as count from (" + sql + ") as gaeaExecute";
//        sql = "select org_code,count(org_code) AS countNum from t_device_info group by org_code ORDER BY countNum desc;";
        dto.setDynSentence(sql);
        List<JSONObject> jsonObjects = dataSourceService.execute(dto);
        System.out.println(jsonObjects.toString());

    }


    @Test
    public void kudu() {
        DataSourceDto dto = new DataSourceDto();
        dto.setUsername("root");
        dto.setPassword("p@ss1234");
        dto.setDriverName("org.apache.hive.jdbc.HiveDriver");
        dto.setJdbcUrl("jdbc:impala://10.108.3.111:21050/ods");

        try {
            Connection unPooledConnection = JdbcUtil.getUnPooledConnection(dto);
            System.out.println(unPooledConnection.getCatalog());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


    }

}