/*
 *  Copyright 2019-2020 Zheng Jie
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.anjiplus.template.gaea.generator.utils;

import com.anji.plus.gaea.curd.params.PageParam;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 分页工具
 * @author Zheng Jie
 * @date 2018-12-10
 */
public class PageUtil extends cn.hutool.core.util.PageUtil {

    /**
     * List 分页
     */
    public static List toPage(int page, int size , List list) {
        int fromIndex = page * size;
        int toIndex = page * size + size;
        if(fromIndex > list.size()){
            return new ArrayList();
        } else if(toIndex >= list.size()) {
            return list.subList(fromIndex,list.size());
        } else {
            return list.subList(fromIndex,toIndex);
        }
    }

    /**
     * 自定义分页
     */
    public static Page toPage(List object, Integer totalCount) {
        return toPage(object, totalCount, null);
    }

    public static int[] getLimit(PageParam pageParam, Integer totalCount){
        int current = 1;
        int pageSize = 10;
        int totalRecords = 0; // 共多少条
        if(pageParam != null){
            if(pageParam.getPageNumber() != null && pageParam.getPageNumber().intValue() >=1 ){
                current = pageParam.getPageNumber();
            }
            if(pageParam.getPageSize() != null && pageParam.getPageSize().intValue() > 0){
                pageSize = pageParam.getPageSize();
            }
        }
        if(totalCount != null && totalCount.intValue() >= 0){
            totalRecords = totalCount.intValue();
        }
        int maxPageNum = 0;
        if(totalCount%pageSize != 0){
            maxPageNum = totalCount/pageSize + 1;
        }else{
            maxPageNum = totalCount/pageSize;
        }
        if(current > maxPageNum){
            current = maxPageNum;
        }

        int[] limit = new int[2];
        int start = (current - 1) * pageSize;
        limit[0] = start <0 ? 0:start;
        limit[1] = pageSize;
        return limit;
    }

    public static Page toPage(List records, Integer totalCount, PageParam pageParam) {
        int current = 1;
        int pageSize = 10;
        int total=0;
        if(pageParam != null){
            if(pageParam.getPageNumber() != null){
                current = pageParam.getPageNumber();
            }
            if(pageParam.getPageSize() != null){
                pageSize = pageParam.getPageSize();
            }
        }
        if(totalCount != null){
            total = totalCount.intValue();
        }
        int totalPage = totalPage(totalCount, pageSize);

        Page page = new Page();
        page.setCurrent(current);
        page.setTotal(total);
        page.setPages(totalPage);// 多少页
        page.setSize(pageSize); // 一页多少条
        if(records == null){
            page.setRecords(new ArrayList());
        }else{
            page.setRecords(records);
        }
        return page;
    }

}
