package com.anjiplus.template.gaea.auth.modules.gaeaannot.controller.param;


import com.anji.plus.gaea.annotation.Query;
import com.anji.plus.gaea.constant.QueryEnum;
import com.anji.plus.gaea.curd.params.PageParam;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 系统公告信息表(GaeaAnnouncement)param
 *
 * @author makejava
 * @since 2021-03-29 15:17:26
 */
@Getter
@Setter
public class GaeaAnnouncementParam extends PageParam implements Serializable {
    @Query(QueryEnum.LIKE)
    private String title;
}