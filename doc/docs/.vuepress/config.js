module.exports = {
  base: '/doc/',
  title: '复用性底盘',
  description: '使用[盖亚]快速构建业务系统',
  dest: 'dist',
  lastUpdated: 'Last Updated',
  theme: '',
  themeConfig: {
    logo: '/logo.png',
    smoothScroll: true,
    sidebarDepth: 2,
    nav: [
      { text: '首页', link: '/' },
      { text: '文档', link: '/guide/' },
      { text: '插件', link: '/plugin/' },
      { text: 'GitHub', link: 'https://github.com/anji-plus/gaea' },
      { text: 'Gitee', link: 'https://gitee.com/anji-plus/gaea' },
    ],
    sidebar:{
      '/guide/':[
				{
					title:'文档',
					collapsable:false,
					children:[
					{title:'介绍',path:'/guide/'},
					{title:'快速了解',path:'/guide/quickly'},
					{title:'项目介绍',path:'/guide/productintro'},
					{title:'系统运行',path:'/guide/deploy'},
					{title:'开发手册',path:'/guide/devdoc'}
					]
				},
				{	
					title:'盖亚底盘',
					collapsable:false,
					children:[
						
					]
				},
				{
					title:'其他',
					collapsable:false,
					children:[
					{title:'常见问题',path:'/guide/question'}	
					]
			}],
			'/plugin/': [
				{
					title:'后端组件',
					collapsable:false,
					children:[
					{title:'归档',path:'/plugin/archiver'},
					{title:'推送',path:'/plugin/push'},
					{title:'导出',path:'/plugin/export'},
					{title:'权限',path:'/plugin/security'},
					{title:'审计日志',path:'/plugin/behaviorAudit'},
					{title:'高级查询/自定义列',path:'/plugin/queryAdvance'}
					]
			},{
				title:'前端组件',
				collapsable:false,
				children:[
					{title:'CRUD组件',path:'/plugin/webCRUD'},
					{title:'mock数据',path:'/plugin/webMock'},
					{title:'高级查询/自定义列',path:'/plugin/webQueryAdvanced'},
				]
		}]
    },
  },
  plugins: [
    ['@vuepress/back-to-top', true],
  ],
	configureWebpack: {
    resolve: {
      alias: {
        '@': '/.vuepress/public'
      }
    }
  }
}