package com.anji.plus.gaea.push.type.dingding.enums;

import com.anji.plus.gaea.push.type.dingding.DingDingPushDetails;
import com.dingtalk.api.request.OapiMessageCorpconversationAsyncsendV2Request;

/**
 * @ClassName DingDingMesEnums
 * @Description: TODO
 * @Author dingkaiqiang
 * @Date 2021-04-07
 * @Version V1.0
 **/
public enum DingDingMesEnums implements MesOperation{

    text {
        @Override
        public void sendDingMes(DingDingPushDetails pushDetails, OapiMessageCorpconversationAsyncsendV2Request.Msg msg) {
            msg.setText(new OapiMessageCorpconversationAsyncsendV2Request.Text());
            msg.getText().setContent(pushDetails.getContent());
        }
    },
    image {
        @Override
        public void sendDingMes(DingDingPushDetails pushDetails, OapiMessageCorpconversationAsyncsendV2Request.Msg msg) {
                msg.setImage(new OapiMessageCorpconversationAsyncsendV2Request.Image());
                msg.getImage().setMediaId("@lADOdvRYes0CbM0CbA");
        }
    },
    file {
        @Override
        public void sendDingMes(DingDingPushDetails pushDetails, OapiMessageCorpconversationAsyncsendV2Request.Msg msg) {
            msg.setFile(new OapiMessageCorpconversationAsyncsendV2Request.File());
            msg.getFile().setMediaId("@lADOdvRYes0CbM0CbA");
        }
    },
    link {
        @Override
        public void sendDingMes(DingDingPushDetails pushDetails, OapiMessageCorpconversationAsyncsendV2Request.Msg msg) {
            msg.setLink(new OapiMessageCorpconversationAsyncsendV2Request.Link());
            msg.getLink().setTitle(pushDetails.getTitle());
            msg.getLink().setText(pushDetails.getContent());
            msg.getLink().setMessageUrl("http://s.dingtalk.com/market/dingtalk/error_code.php");
            msg.getLink().setPicUrl("http://10.108.11.33:8888/group1/M00/00/05/CmwLIV_EgiWAICVoAAGdaRolaBE364.png?attName=4ca254f0cb29c68daab2a4d07c78825.png");
        }
    },
    markdown {
        @Override
        public void sendDingMes(DingDingPushDetails pushDetails, OapiMessageCorpconversationAsyncsendV2Request.Msg msg) {
            msg.setMarkdown(new OapiMessageCorpconversationAsyncsendV2Request.Markdown());
            msg.getMarkdown().setTitle(pushDetails.getTitle());
            msg.getMarkdown().setText(pushDetails.getMarkdown());
        }
    },
    oa {
        @Override
        public void sendDingMes(DingDingPushDetails pushDetails, OapiMessageCorpconversationAsyncsendV2Request.Msg msg) {
            msg.setOa(new OapiMessageCorpconversationAsyncsendV2Request.OA());
            msg.getOa().setHead(new OapiMessageCorpconversationAsyncsendV2Request.Head());
//            msg.getOa().setMessageUrl(pushDetails.getOaMessageUrl());
//            msg.getOa().getHead().setText(pushDetails.getOaHeadText());
//            msg.getOa().getHead().setBgcolor(pushDetails.getOaHeadBgcolor());
//            msg.getOa().setBody(new OapiMessageCorpconversationAsyncsendV2Request.Body());
//            msg.getOa().getBody().setTitle(pushDetails.getOaBodyTitle());
//
//            msg.getOa().getBody().setForm(pushDetails.getOaBodyForms());
//            msg.getOa().getBody().setRich(pushDetails.getOaBodyRich());
////            msg.getOa().getBody().setContent("大段文本大段文本大段文本大段文本大段文本大段文本大段文本大段文本大段文本大段文本大段文本大段文本");
//            msg.getOa().getBody().setFileCount(pushDetails.getOaBodyFileCount());
//            msg.getOa().getBody().setAuthor(pushDetails.getOaBodyAuthor());
//            msg.getOa().getBody().setImage(pushDetails.getOaBodyImage());

/*            msg.setOa(new OapiMessageCorpconversationAsyncsendV2Request.OA());
            msg.getOa().setHead(new OapiMessageCorpconversationAsyncsendV2Request.Head());
            msg.getOa().setMessageUrl("http://www.baidu.com");
            msg.getOa().getHead().setText("智能客户管理");
            msg.getOa().getHead().setBgcolor("FFBBBBBB");
            msg.getOa().setBody(new OapiMessageCorpconversationAsyncsendV2Request.Body());
            msg.getOa().getBody().setTitle("钉钉推出了全新客户管理方式");
            List<OapiMessageCorpconversationAsyncsendV2Request.Form> formList = new ArrayList<>();
            OapiMessageCorpconversationAsyncsendV2Request.Form form1 = new OapiMessageCorpconversationAsyncsendV2Request.Form();
            form1.setKey("1.客户通讯录：");
            form1.setValue("统一管理");
            OapiMessageCorpconversationAsyncsendV2Request.Form form2 = new OapiMessageCorpconversationAsyncsendV2Request.Form();
            form2.setKey("2.客户群：");
            form2.setValue("一站式沟通和协同");
            OapiMessageCorpconversationAsyncsendV2Request.Form form3 = new OapiMessageCorpconversationAsyncsendV2Request.Form();
            form3.setKey("3.客户工具：");
            form3.setValue("高效系统同流程透明");
            OapiMessageCorpconversationAsyncsendV2Request.Form form4 = new OapiMessageCorpconversationAsyncsendV2Request.Form();
            form4.setKey("4.服务工具：");
            form4.setValue("智能客服、群发");
            OapiMessageCorpconversationAsyncsendV2Request.Form form5 = new OapiMessageCorpconversationAsyncsendV2Request.Form();
            form5.setKey("爱好：");
            form5.setValue("打球、听音乐");
            OapiMessageCorpconversationAsyncsendV2Request.Form form6 = new OapiMessageCorpconversationAsyncsendV2Request.Form();
            form6.setKey("学历：");
            form6.setValue("本科");
            formList.add(form1);
            formList.add(form2);
            formList.add(form3);
            formList.add(form4);
            formList.add(form5);
            formList.add(form6);
            msg.getOa().getBody().setForm(formList);
            OapiMessageCorpconversationAsyncsendV2Request.Rich rich = new OapiMessageCorpconversationAsyncsendV2Request.Rich();
            rich.setNum("15.6");
            rich.setUnit("元");
            msg.getOa().getBody().setRich(rich);
//            msg.getOa().getBody().setContent("大段文本大段文本大段文本大段文本大段文本大段文本大段文本大段文本大段文本大段文本大段文本大段文本");
            msg.getOa().getBody().setFileCount("3");
            msg.getOa().getBody().setAuthor("李四");
            msg.getOa().getBody().setImage("http://10.108.11.33:8888/group1/M00/00/05/CmwLIV_F1MKAPyhxAAE6ywFkrlY889.png");
            msg.setMsgtype("oa");*/
        }
    },
    action_card {
        @Override
        public void sendDingMes(DingDingPushDetails pushDetails, OapiMessageCorpconversationAsyncsendV2Request.Msg msg) {
            msg.setActionCard(new OapiMessageCorpconversationAsyncsendV2Request.ActionCard());
            msg.getActionCard().setTitle(pushDetails.getTitle());
            msg.getActionCard().setMarkdown(pushDetails.getMarkdown());
            msg.getActionCard().setSingleTitle(pushDetails.getSingleTitle());
            // 跳转的内部url 详细url
//            msg.getActionCard().setSingleUrl("http://10.108.11.33:8888/group1/M00/00/05/CmwLIV_EgiWAICVoAAGdaRolaBE364.png?attName=4ca254f0cb29c68daab2a4d07c78825.png");
            msg.getActionCard().setSingleUrl("http://www.baidu.com");
        }
    },
    ;
}
