package com.anjiplus.template.gaea.business.modules.rules.controller;

import com.anji.plus.gaea.curd.controller.GaeaBaseController;
import com.anjiplus.template.gaea.business.modules.rules.dao.entity.GaeaRulesHistory;
import com.anjiplus.template.gaea.business.modules.rules.controller.dto.GaeaRulesHistoryDTO;
import com.anjiplus.template.gaea.business.modules.rules.controller.param.GaeaRulesHistoryParam;
import com.anjiplus.template.gaea.business.modules.rules.service.GaeaRulesHistoryService;
import com.anji.plus.gaea.curd.service.GaeaBaseService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * (GaeaRulesHistory)实体类
 *
 * @author why
 * @since 2021-03-17 13:52:55
 */
@RestController
@RequestMapping("/gaeaRulesHistory")
@Api(value = "/gaeaRulesHistory", tags = "")
public class GaeaRulesHistoryController extends GaeaBaseController<GaeaRulesHistoryParam, GaeaRulesHistory, GaeaRulesHistoryDTO> {
    @Autowired
    private GaeaRulesHistoryService gaeaRulesHistoryService;
    
    @Override
    public GaeaBaseService<GaeaRulesHistoryParam, GaeaRulesHistory> getService() {
        return gaeaRulesHistoryService;
    }

    @Override
    public GaeaRulesHistory getEntity() {
        return new GaeaRulesHistory();
    }

    @Override
    public GaeaRulesHistoryDTO getDTO() {
        return new GaeaRulesHistoryDTO();
    }
}