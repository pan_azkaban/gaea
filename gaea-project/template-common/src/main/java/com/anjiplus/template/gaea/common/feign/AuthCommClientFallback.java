package com.anjiplus.template.gaea.common.feign;

import com.anji.plus.gaea.bean.ResponseBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


/**
 * 功能描述：
 *
 * @Author: peiyanni
 * @Date: 2021/2/19 14:32
 */
@Service
@Slf4j
public class AuthCommClientFallback implements AuthCommServiceClient {
    @Override
    public ResponseBean getDynamicQueryBoListById(Long commonId) {
        log.info("--AuthCommClientFallback--");
        return null;
    }
}
