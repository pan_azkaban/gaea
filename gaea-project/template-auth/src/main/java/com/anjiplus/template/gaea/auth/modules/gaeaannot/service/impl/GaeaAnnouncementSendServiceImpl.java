package com.anjiplus.template.gaea.auth.modules.gaeaannot.service.impl;

import com.anji.plus.gaea.curd.mapper.GaeaBaseMapper;
import com.anji.plus.gaea.holder.UserContentHolder;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.controller.dto.GaeaAnnouncementDTO;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.dao.GaeaAnnouncementMapper;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.dao.GaeaAnnouncementSendMapper;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.dao.entity.GaeaAnnouncement;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.dao.entity.GaeaAnnouncementSend;
import com.anjiplus.template.gaea.auth.modules.gaeaannot.service.GaeaAnnouncementSendService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 系统公告发送阅读记录表(GaeaAnnouncementSend)ServiceImpl
 *
 * @author makejava
 * @since 2021-03-29 15:31:29
 */
@Service
public class GaeaAnnouncementSendServiceImpl implements GaeaAnnouncementSendService {
    @Autowired
    private GaeaAnnouncementSendMapper gaeaAnnouncementSendMapper;
    @Autowired
    private GaeaAnnouncementMapper gaeaAnnouncementMapper;

    @Override
    public GaeaBaseMapper<GaeaAnnouncementSend> getMapper() {
        return gaeaAnnouncementSendMapper;
    }

    @Override
    public GaeaAnnouncementDTO getAnnotReadDetailInfo(Long id){
        String username= UserContentHolder.getContext().getUsername();
        GaeaAnnouncementSend annotSend=gaeaAnnouncementSendMapper.selectById(id);
        if(annotSend.getReadFlag()==0){
            //变更消息阅读记录
            List<Long> idList=new ArrayList<>(1);
            idList.add(id);
            gaeaAnnouncementSendMapper.updateReadFlag(username,idList);
        }
        GaeaAnnouncement annot=gaeaAnnouncementMapper.selectById(annotSend.getAnntId());
        GaeaAnnouncementDTO annotDto=new GaeaAnnouncementDTO();
        BeanUtils.copyProperties(annot,annotDto);
        return annotDto;
    }

    @Override
    public Boolean tagAllRead(List<Long> ids) {
        String username =UserContentHolder.getContext().getUsername();
        gaeaAnnouncementSendMapper.updateReadFlag(username,ids);
        return true;
    }
}