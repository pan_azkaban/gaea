rem @echo off
echo %~dp0
set path=%~dp0 
set root =%path%/ 
echo 'build dependency'
git pull
rem cd ../gaea-modules && mvn clean install

cd %root%

echo 'create archetype'

if exist ./template-ui/node_modules (
	echo 'rm node_modules'
	rd /s/q ./template-ui/node_modules
)

mvn clean && mvn archetype:create-from-project 

echo 'install archetype'

copy  doc/archetype-metadata.xml  target\generated-sources\archetype\src\main\resources\META-INF\maven\archetype-metadata.xml

cd target\generated-sources\archetype && mvn install
echo 'deploy archetype'
rem mvn deploy

echo 'view archetype-catatype'
cd %root%

pause