package com.anji.plus.gaea.job.core.constant;
/**
 * @desc 缓存key
 * @author 木子李*de
 * @date 2023-05-12 15:25:35.961
 **/
public interface KeyConstant {
    /**
     * 定时任务，JobScheduleHelper线程，每5秒预读5秒内需要执行的job分布式锁
     */
    String KEY_TRIGGER_JOB_PRE_READ_LOCK = "meta:job:trigger:job_pre_read_lock";
}
