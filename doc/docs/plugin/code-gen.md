# 代码生成 
## 实现原理

  - 数据模型驱动代码设计开发
  - 代码生成模块整合了el-admin的相关实现逻辑，添加了子母表详情功能和国际化支持。
  - 代码生成基于freemarker模板，通过修改前后端代码模板可以自定义适配各种技术栈。
  - 目前gaea内置维护的[模板,详情参考这里](https://gitee.com/anji-plus/gaea/tree/master/gaea-project-template/template-gaea-generator/src/main/resources/template/gaea-mybatis)
## 交互流程

1、需求分析，创建业务模型，DDL完整规范化：
  - 1.1 表名，列名都需要添加comment,该comment默认作为对应字段的UI的label
  - 1.2 遵循1.1的约定，后续代码生成，UI国际化配置就会很简单快捷。
  
2、代码生成的**项目级配置**,相关表定义参考[代码生成db初始化](https://gitee.com/anji-plus/gaea/blob/master/gaea-project-template/gaea-refer-doc/gaea-db/gaea_generator-DDL-DML.sql)
  ```sql
  select code as project_code,project_root from t_lc_project;
  ```
  根据实际情况，修改code,project_root，通过gaea-project-template模板生成的项目，code和project_root应该是一样的。
  
  gaea-project-template项目的配置如下：
  
  project_root=gaea-project-template
  code=template-gaea

3、代码生成的**表级别、字段级配置**
  详见菜单【代码生成】->选择当前项目->查看数据源->选择目标表->配置，进入如下页面
  - 配置主页面
  
  - 保存表级别的配置
  
  - 保存字段生成相关的配置
  
  - 测试、预览
  
  - 生成、下载
  
4、UI国际化配置
  
  国际化配置默认从代码生成配置表获取字段配置信息
  
  【系统设置 / UI国际化设置】
  
  国际化配置采用 语言属性(locale)+行业属性(cataType)+表名称(refer)+菜单模块(module) 4个维度来控制UI字段的展示名称。
  
## 操作步骤

## 配置说明
  - 代码生成服务端配置
```yaml
generator:
  enabled: true # 代码生成服务开关，为false时 相关service不实例化。
  workspace: /workspace/gaea #代码workspace=${workspace}/${prjoectRoot}
  templatePath: template # freemarker模板目录，可以自定义新的模板
```   
## 功能表说明
  - t_lc_project 代码生成-项目级配置表
  - code_gen_config  代码生成-表级别配置
  - code_column_config 代码生成-字段级配置
  - t_alipay_config 代码生成示例表-单表生成
  - t_device_info 代码生成示例表-子母表功能
  - t_device_model
  - t_device_log_detail
  
## 生成示例
  单表列表页
  ![单表列表页](./resources/gen-example-zh-index.png)
  
  [单表详情页]()
  
  子母表详情页
  ![子母表详情页](./resources/gen-example-deviceInfo-zh.png)
  [UI国际化]()
  
## 单表增删改查

## 母子表增删改查