package com.anjiplus.template.gaea.generator.modules.project.service;

import com.anjiplus.template.gaea.generator.modules.project.controller.param.ProjectParam;
import com.anjiplus.template.gaea.generator.modules.project.dao.entity.Project;
import com.anji.plus.gaea.curd.service.GaeaBaseService;

/**
 * (Project)Service
 *
 * @author makejava
 * @since 2021-03-03 09:54:42
 */
public interface ProjectService extends GaeaBaseService<ProjectParam, Project> {

}
