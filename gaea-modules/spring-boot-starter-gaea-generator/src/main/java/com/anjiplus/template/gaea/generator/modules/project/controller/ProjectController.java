package com.anjiplus.template.gaea.generator.modules.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.anji.plus.gaea.curd.controller.GaeaBaseController;
import com.anji.plus.gaea.curd.service.GaeaBaseService;
import com.anjiplus.template.gaea.generator.modules.project.controller.dto.ProjectDTO;
import com.anjiplus.template.gaea.generator.modules.project.controller.param.ProjectParam;
import com.anjiplus.template.gaea.generator.modules.project.dao.entity.Project;
import com.anjiplus.template.gaea.generator.modules.project.service.ProjectService;

import io.swagger.annotations.Api;

/**
 * (Project)实体类
 *
 * @author makejava
 * @since 2021-03-03 09:54:57
 */
@RestController
@RequestMapping("/project")
@Api(value = "/project", tags = "")
public class ProjectController extends GaeaBaseController<ProjectParam, Project, ProjectDTO> {
    @Autowired
    private ProjectService projectService;

    @Override
    public GaeaBaseService<ProjectParam, Project> getService() {
        return projectService;
    }

    @Override
    public Project getEntity() {
        return new Project();
    }

    @Override
    public ProjectDTO getDTO() {
        return new ProjectDTO();
    }
}
