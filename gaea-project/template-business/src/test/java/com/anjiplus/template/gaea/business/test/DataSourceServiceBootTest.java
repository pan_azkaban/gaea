package com.anjiplus.template.gaea.business.test;

import com.alibaba.fastjson.JSONObject;
import com.anjiplus.template.gaea.business.BusinessApplication;
import com.anjiplus.template.gaea.business.modules.data.dataSource.controller.dto.DataSourceDto;
import com.anjiplus.template.gaea.business.modules.data.dataSource.controller.param.ConnectionParam;
import com.anjiplus.template.gaea.business.modules.data.dataSource.service.DataSourceService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Created by raodeming on 2021/3/24.
 */
@SpringBootTest(classes = BusinessApplication.class)
@RunWith(SpringRunner.class)
public class DataSourceServiceBootTest {

    @Autowired
    private DataSourceService dataSourceService;

    @Test
    public void executeElasticsearchSql(){
        DataSourceDto dto = new DataSourceDto();
        dto.setSourceType("elasticsearch_sql");
        dto.setSourceConfig("{\"apiUrl\":\"http://10.108.26.164:9200/_xpack/sql?format=json\",\"method\":\"POST\",\"body\":\"1\"}");
        dto.setDynSentence("{\"query\": \"select HISTOGRAM(logTime,INTERVAL 1 MONTH) as h ,count(flag),flag from \\\"analysis-wifilogin\\\" where  logTime>='2021-02-22 00:28:10.000' and logTime<'2021-03-22 00:28:10.000' GROUP BY h,flag\"}");
        List<JSONObject> execute = dataSourceService.execute(dto);
        System.out.println(execute);

    }


    @Test
    public void testElasticsearchSqlConnection(){
        ConnectionParam dto = new ConnectionParam();
        dto.setSourceType("elasticsearch_sql");
        dto.setSourceConfig("{\"apiUrl\":\"http://10.108.26.164:9200/_xpack/sql?format=json\",\"method\":\"POST\",\"body\":\"1\"}");

        Boolean aBoolean = dataSourceService.testConnection(dto);
        System.out.println(aBoolean);

    }

}