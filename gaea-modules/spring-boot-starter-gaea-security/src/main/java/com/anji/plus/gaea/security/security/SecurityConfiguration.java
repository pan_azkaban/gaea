package com.anji.plus.gaea.security.security;

import com.anji.plus.gaea.security.GaeaSecurityProperties;
import com.anji.plus.gaea.security.security.handler.*;
import com.anji.plus.gaea.security.security.url.UrlAccessDecisionManager;
import com.anji.plus.gaea.security.security.url.UrlFilterInvocationSecurityMetadataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

/**
 * security 安全配置
 *
 * @author lr
 * @since 2021-01-25
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtTokenAuthenticationFilter jwtTokenAuthenticationFilter;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UrlAccessDecisionManager urlAccessDecisionManager;

    @Autowired
    private GaeaSecurityProperties gaeaSecurityProperties;

    @Autowired
    private UrlFilterInvocationSecurityMetadataSource urlFilterInvocationSecurityMetadataSource;

    /**
     * 自定义userDetailService
     *
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(new BCryptPasswordEncoder());
    }

    /**
     * 未登录
     *
     * @return
     */
    @Bean
    public AccessDeniedHandler accessDeniedHandler() {
        return new GaeaAccessDeniedHandler();
    }

    /**
     * 权限校验未通过处理
     *
     * @return
     */
    @Bean
    public AuthenticationEntryPoint authenticationEntryPoint() {
        return new GaeaAuthenticationEntryPoint();
    }

    /**
     * 登录成功处理
     *
     * @return
     */
    @Bean
    @ConditionalOnMissingBean
    public GaeaLoginSuccessHandler gaeaLoginSuccessHandler() {
        return new GaeaLoginSuccessHandler();
    }

    /**
     * 登录失败处理
     * @return
     */
    @Bean
    @ConditionalOnMissingBean
    public GaeaLoginFailureHandler gaeaLoginFailureHandler() {
        return new GaeaLoginFailureHandler();
    }

    /**
     * 登出成功处理
     *
     * @return
     */
    @Bean
    @ConditionalOnMissingBean
    public GaeaLogoutSuccessHandler gaeaLogoutSuccessHandler() {
        return new GaeaLogoutSuccessHandler();
    }


    /**
     * 登录逻辑处理
     * @return
     */
    @Bean
    @ConditionalOnMissingBean
    public GaeaUsernamePasswordAuthenticationFilter gaeaUsernamePasswordAuthenticationFilter() {

        AuthenticationManager authenticationManager = null;
        try {
            authenticationManager = this.authenticationManager();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new GaeaUsernamePasswordAuthenticationFilter(authenticationManager,
                gaeaLoginSuccessHandler(), gaeaLoginFailureHandler());
    }

    /**
     * 解决单体服务跨域
     *
     * @author du
     * @since 2021-05-07
     */
    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        org.springframework.web.cors.CorsConfiguration configuration = new CorsConfiguration();
        configuration.addAllowedOrigin("*");
        configuration.addAllowedMethod("*");
        configuration.addAllowedHeader("*");
        configuration.setAllowCredentials(true);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry = http.antMatcher("/**").authorizeRequests();

        registry.withObjectPostProcessor(new ObjectPostProcessor<FilterSecurityInterceptor>() {
            @Override
            public <O extends FilterSecurityInterceptor> O postProcess(O o) {
                o.setSecurityMetadataSource(urlFilterInvocationSecurityMetadataSource);
                o.setAccessDecisionManager(urlAccessDecisionManager);
                return o;
            }
        });

        http.exceptionHandling()
                .accessDeniedHandler(accessDeniedHandler())
                .authenticationEntryPoint(authenticationEntryPoint())
                .and()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/login/**","/logout", "/health", "/user/loginCode/**").permitAll()
                .mvcMatchers(HttpMethod.GET,"/dict/item/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin().successHandler(gaeaLoginSuccessHandler())
                .failureHandler(gaeaLoginFailureHandler())
                .and()
                .logout().logoutSuccessHandler(gaeaLogoutSuccessHandler());

        http.addFilterBefore(
                gaeaUsernamePasswordAuthenticationFilter(),
                UsernamePasswordAuthenticationFilter.class);

        if (gaeaSecurityProperties.isCorsEnabled()) {
            http.cors(Customizer.withDefaults());
        }

        if (!gaeaSecurityProperties.getAuthDisabled()) {

            http.addFilterBefore(jwtTokenAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
        }
    }
}
